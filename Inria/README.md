* Un premier retour a été formulé suite à la réunion COMP du centre de Saclay le 4 novembre 2024, c'est le [pdf](https://gitlab.inria.fr/sgilles/homepage/-/blob/master/Inria/Retour_COMP_devtech_2024_2028_SebastienGilles.pdf?ref_type=heads) dans ce dossier.

* Quelques correctifs très mineurs ont été fait dans le texte du COMP, mais ils n'adressaient fondamentalement aucun des points que j'avais soulevés. Je l'ai écrit à Jean-Philippe Lagrange, qui a invité à donner des propositions de modification "clé en main". J'ai donc produit [ce notes](https://notes.inria.fr/ZhIF25t9S-2NDCMOo7KJIQ#); je mets également [le markdown](PropositionReformulationPage27.md) dans le répertoire au cas où il disparaîtrait de notes mais il s'appuie sur codimd et aura un meilleur rendu sur l'instance notes.


