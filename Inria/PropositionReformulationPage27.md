---
type: document

title: Proposition de reformulation de la page 27 du COMP

description: Cette note fait suite à la proposition de Jean-Philippe Lagrange de proposer des reformulations pour le COMP. Elle fait suite à [cette note](https://notes.inria.fr/soSOSFIOQ3q7Ct7gJy06Gw#) qui a déjà été remontée..

tags: comp, sed
---
[TOC]

# Code couleur

- Texte normal: proposition de nouveau contenu, qui remplace en général le bloc rouge du dessus (+ nouvelle proposition en fin de document)
- Bloc rouge: le texte du COMP actuel, laissé à titre indicatif. La proposition est de remplacer ces blocs par le reste.
- Bloc bleu: nouveaux jalons internes proposés.
- Bloc jaune: le seul jalon non interne, laissé tel quel.


# Proposition


Pour l’activité du développement technologique, des modalités et priorités opérationnelles sont ainsi identifiées pour la période 2024-2028 :


:::danger
* Faire du mode projet un outil opérationnel central notamment en s’appuyant sur la cartographie des compétences des ingénieurs : la formalisation, la qualification, l’affectation des ressources et le suivi au cours du temps sont des enjeux majeurs de performance, en particulier pour la réalisation des logiciels et infrastructures de l’institut. L’affectation des ressources de développement se fera donc sur des projets clairement identifiés et pilotés avec une cartographie des compétences comme pierre angulaire du mode projet.
:::

* Améliorer la vision d'ensemble des projets logiciels et des infrastructures de l'institut, en s'appuyant notamment sur un nouvel outil de cartographie des projets et des compétences des ingénieurs actuellement en cours de développement.


:::info
Jalons internes: 

- Première version de l'outil déployée au plus tard à la fin 1er trimestre 2025.
- Complétion de la cartographie des projets dans lesquels des ingénieurs permanents sont impliqués fin 2025.
- Définition d'une procédure pour cartographier l'ensemble des projets de développement technologique sur lesquels des ingénieurs sont impliqués quel que soit leur statut fin 2026.
- Cartographie fonctionnelle complète avec mise à jour dynamique des données fin 2027.
:::

> [name=Sébastien] _Pourquoi la proposition de changement ci-dessus?_
> * Le mode projet est essentiellement le fonctionnement historique
> * La cartographie des projets me semble vraiment l'élément-clé.



:::danger
* Renforcer le savoir-faire technologique : Les compétences en développement technologique vont devoir être renforcées et mises à jour régulièrement, en particulier pour être efficaces sur les domaines émergents.
:::

* Mieux accompagner via un plan formation ambitieux, flexible et dynamique les ingénieurs souhaitant s'investir à court ou moyen terme dans des domaines émergents (par exemple informatique quantique).


> [name=Sébastien] _Pourquoi la proposition de changement ci-dessus?_
> * La précédente formulation laissait entendre que la ligne métier ne faisait pas de veille technologique, ce qui est très inexact. Je trouve intéressant de souligner également le travail fait ces deux dernières années côté DGD-I pour mettre en place un plan formation digne de ce nom.



:::danger
* Renforcer les dynamiques collectives et les effets induits par la disponibilité d’une masse critique de compétences dans des domaines spécifiques, en particulier par le développement de réseaux thématiques d’ingénieurs au niveau national.
:::

* Renforcer et mieux structurer les réseaux thématiques mis en place en 2022, et étudier avec l'Agence de Programmes comment en créer de nouveaux sur les thématiques non couvertes actuellement (cybersécurité, IA, quantique, etc...)

:::info
Jalons internes: 
* A l'horizon 2026, la majorité des ingénieurs de l'Institut devrait être affiliés dans l'outil de cartographie à au moins un réseau thématique, et être encouragé à y participer activement (participation voire organisation des séminaires réguliers, veille partagée, échanges de bonnes pratiques, etc...)
* Au cas par cas, étudier comment ouvrir un réseau donné au reste de l'ESR, en s'appuyant également sur les réseaux existants (DevLog, groupe Calcul, GDR, etc...)
:::


> [name=Sébastien] _Pourquoi la proposition de changement ci-dessus?_
> * Indique plus précisément ce qu'il y a déjà, et le chemin qu'il reste à faire.
> * Souligne notamment le fait que des thèmes ne sont pas couverts.



:::danger
* Formaliser et partager des méthodologies de développement et d’expérimentation.
:::

* Renforcer l'activité de formation et de diffusion des bonnes pratiques de génie logiciel en interne, avec notamment une réflexion sur comment accompagner les ingénieurs junior présents dans les équipes de recherche.

:::info
Jalons internes: 
* A l'horizon 2026, meilleur partage de l'offre de formations internes à l'échelle de l'Institut, pour favoriser les échanges et le transfert de compétences inter-centres.
* A l'horizon 2027, proposer un parcours d'accompagnement pour les nouveaux arrivants autour de l'activité de développement technologique :
    * guide détaillé pointant vers les ressources utiles pour le développement (galeries d'exemple pour l'intégration continue développées depuis 2023, pointeurs vers les infrastructures de calcul et de développement disponibles, etc..)
    * parcours de formation / mise en place d'un mentorat si besoin pour les ingénieurs junior pour acquérir les compétences nécessaires pour réussir le projet sur lequel ils sont impliqués.
:::


> [name=Sébastien] _Pourquoi la proposition de changement ci-dessus?_
> * Je ne comprends pas ce que voulait couvrir l'item
> * Il me semble important de souligner l'activité transverse des SED - élever le niveau global de qualité logicielle des projets de l'Institut.



* Préparer une vision long terme pour tous les logiciels structurants et impactants d'Inria. Cette démarche a été entamée avec succès pour plusieurs logiciels existants:
    *  par le dispositif InriaSoft (scikit-learn, Coq, Vidjil, Shanoir, Pharo, PlantNet, FedBioMed)
    *  par la création d'une Fondation ad hoc (OCaml)
    *  plus récemment par l'Agence de Programmes (plan P16 / probabl pour scikit-learn, skrub et corese)
    
mais d'autres logiciels avec un fort rayonnement n'ont pas de feuille de route aussi définie. On peut citer par exemple Eigen (librairie d'algèbre linéaire de référence qui est une brique de nombre de logiciels de calcul scientifique), Scotch (seule librairie développée activement de décomposition de domaines) ou Guix-HPC (pour fournir un environnement reproductible dans le contexte de calcul HPC) rien que dans le domaine du calcul scientifique.

:::info
Jalons internes: 
* A l'horizon 2026, cartographie de tous les logiciels impactants pour lesquels une feuille de route long terme est à établir.
* Avec l'Agence de Programmes, étudier la création de modalités similaires à celles du programme P16 pour soutenir les logiciels des thèmes ne relevant pas du thème de l'IA.
:::

> [name=Sébastien] _Pourquoi la proposition d'ajout ci-dessus?_
> * C'est un point qui me semble important, est une continuité / extension d'un point du précédent COP et suit une des recommendations de l'HCERES.

:::danger
Jalon interne : À horizon 2025, Inria aura mis en place un outil dynamique de cartographie des projets de développement technologique avec une priorité de gestion des projets à l’échelle de l’Agence de programme (ex : P16, Slices, Catala…) pour assurer leur diffusion, leur appropriation et leur impact.
Une attention particulière sera portée aux projets d’intérêt national (en lien avec l’Agence).
:::

> [name=Sébastien] _Pourquoi la proposition de suppression ci-dessus?_
> * Déjà couvert par le premier point

:::warning
Jalon : Au premier semestre 2025, Inria présentera à son conseil d’administration une cartographie des enjeux de standardisation et des initiatives en cours, et un plan d’action pour soutenir les dynamiques de standardisation en lien avec les priorités identifiées par l’Agence.
:::