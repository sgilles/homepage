// In C++
#include <string>
#include <map>
#include <vector>

int a = 5;
double x = 7.;
std::string s = "My string";
std::map<unsigned int, std::vector<std::string>> my_object;
std::map<unsigned int, std::vector<std::string>>::iterator it = my_object.begin();

// New in C++ 11: type inference
auto a2 = 5;
auto x2 = 7.;
auto s2 = "My string";
auto it2 = my_object.begin();