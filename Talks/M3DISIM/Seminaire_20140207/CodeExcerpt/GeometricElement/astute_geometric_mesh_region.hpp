// GeometricMeshRegion.hpp
class GeometricMeshRegion
{
public:
    
    /*!
     * First element of the pair: index in the vector of the first element that fits the bill.
     * Second element of the pair: number of geometric elements that matches.
    */
    std::pair<std::size_t, std::size_t> GetSubsetGeometricElementList(GeometricElementEnum geometric_type,
                                                                      unsigned int label_index) const;
                                                                      
    //! Returns the \a i-th geometric element in the vector.
    GeometricElement* GetGeometricElement(std::size_t i) const;                                                                  
    
private:
    
    //! Class in which the implementation is truly implemented.
    GeometricElementList geometric_element_list_;
};


// GeometricMeshRegion.hpp
class GeometricMeshRegion
{
public:
    
    /*!
     * First element of the pair: index in the vector of the first element that fits the bill.
     * Second element of the pair: number of geometric elements that matches.
    */
    std::pair<std::size_t, std::size_t> GetSubsetGeometricElementList(GeometricElementEnum geometric_type,
                                                                      unsigned int label_index) const;
                                                                      
    //! Returns the \a i-th geometric element in the vector.
    GeometricElement* GetGeometricElement(std::size_t i) const;                                                                  
    
private:
    
    //! Class in which the implementation is truly implemented.
    Private::GeometricElementList geometric_element_list_;
};


// GeometricMeshRegion.cpp
std::pair<std::size_t, std::size_t> GeometricMeshRegion::GetSubsetGeometricElementList(GeometricElementEnum geometric_type,
                                                                                       unsigned int label_index) const
{
    return geometric_element_list_.GetSubsetGeometricElementList(geometric_type, label_index);
}


GeometricElement* GeometricMeshRegion::GetGeometricElement(std::size_t i) const
{
    return geometric_element_list_.GetGeometricElement(i);
}


// main.cpp
int main()
{
    GeometricMeshRegion mesh;
    ...
    std::size_t position_first_element, Nelement;
    std::tie(position_first_element, Nelement) = mesh.GetSubsetGeometricElementList(GeometricElementEnum::Tria3, 1);

    for (std::size_t index = position_first_element; index <= position_first_element + Nelement; ++index)
        ...
}
            