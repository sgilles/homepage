typedef std::unordered_map<unsigned int, std::pair<unsigned int, unsigned int> > GeometricElementPerLabelType;

/*!
* \brief A minimalist container to be able to fetch quickly all the geometric elements of a given kind.
*
* Key of the first map: identifier of the GeometricElement. (for instance 'Triangle3').
* Value of the first map is a second map.
* Key of the second map is index of the label.
* Value of the second map is a pair which elements are:
*  - first: starting index to the elements having this label
*  - second: the number of elements having this label.
*
* This quite ugly container is supposed to be purely internal; it shouldn't be left accessible to
* the public interface!
*
*/
typedef std::unordered_map<GeometricElementEnum, GeometricElementPerLabelType> GeometricElementLookupHelperType;


