#include <vector>

// GeometricMeshRegion.hpp
class GeometricMeshRegion
{
public:
    
    std::vector<GeometricElement*> GetGeometricElementList(GeometricElementEnum type, unsigned int label) const;
        
private:

    std::vector<GeometricElement*> geometric_element_list_;

};


// GeometricMeshRegion.cpp
std::vector<GeometricElement*> GeometricMeshRegion::GetGeometricElementList(GeometricElementEnum type, unsigned int label) const
{
    std::vector<GeometricElement*> ret;

    for (auto geometric_element: geometric_element_list_)
    {
        assert(geometric_element);
        if (geometric_element->GetLabel() == label && geometric_element->GetIdentifier() == type)
            ret.push_back(geometric_element);    
    }
    
    return std::move(ret);
}


// main.cpp
int main()
{
    GeometricMeshRegion mesh; 
    ...
    auto triangle_3_label_1 = std::move(mesh.GetGeometricElementList(GeometricElementEnum::Tria3, 1));
}