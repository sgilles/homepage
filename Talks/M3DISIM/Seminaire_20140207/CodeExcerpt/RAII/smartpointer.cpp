#include <memory>



class GeometricElement
{
public:
    
    explicit GeometricElement(std::shared_ptr<Label> label)
    : label_(label)
    { }
    
    ~GeometricElement() = default;
    
    GeometricElement& operator=(const GeometricElement&) = default;
        
private:
    std::shared_ptr<Label> label_;    
};


int main()
{
    {
        std::shared_ptr label = std::make_shared<Label>(2); // internal count = 1
        
        GeometricElement element1(label); // internal count = 2
        {
            GeometricElement element2 = element1; // internal count = 3
        }
        // element2 goes out of scope; internal count = 2
        
    } // underlying object is destroyed here!
        
}


class GeometricMeshRegion
{
    ...
private:
    std::vector<std::shared_ptr<Vertex>> vertice_list_;
};


class Vertex
{
    typedef std::vector<std::shared_ptr<Vertex>> vector_shared_ptr;
};


class GeometricMeshRegion
{
    ...
private:
    Vertex::vector_shared_ptr vertice_list_; // syntactic sugar!
};
