class Label 
{
public:
    explicit Label(int label)
    : label_(label)
    { }
    
private:
    
    int label_;
};


class GeometricElement
{
public:
    
    explicit GeometricElement(Label* label)
    : label_(label)
    { }
    
    ~GeometricElement() { delete label_; }
    
    GeometricElement& operator=(const GeometricElement&) = default;
        
private:
    Label* label_;    
};


// Patched version.
class GeometricElement
{
public:
    
    explicit GeometricElement(Label* label)
    : label_(label)
    { }
    
    ~GeometricElement() = default;
    
    GeometricElement& operator=(const GeometricElement&) = default;
        
private:
    Label* label_;    
};



// main.cpp
int main()
{
    Label* label = new Label(2);
    GeometricElement element1(label);
    GeometricElement element2 = element1; // ERROR: double delete!    
}


// Patched version.
int main()
{
    Label* label = new Label(2);
    GeometricElement element1(label);
    GeometricElement element2 = element1; // ERROR: double delete!    
    delete label;
}