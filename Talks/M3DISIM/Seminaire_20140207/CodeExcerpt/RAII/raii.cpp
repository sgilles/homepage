
int Function()
{
    double* array = new array[5];
    // ...       
    if (condition) 
    {
        delete[] array;
        return 1;
    }
    // ...
    try
    {
        OtherFunctionThatMightThrow();
    }
    catch(...)
    {
        delete[] array;
        throw;
    }
    
    delete[] array;
    
    return 0;
}


// In ArrayRaii.hpp
class ArrayRaii
{
public:
    //! Constructor.
    explicit ArrayRaii(std::size_t size);
    
    //! Destructor.
    ~ArrayRaii();
    
    //! Accessor to the underlying array.
    double* GetArray() const;
    
private:
    
    //! Underlying array.
    double* array_;  
};


// In ArrayRaii.cpp
ArrayRaii::ArrayRaii(std::size_t size)
{
    array_ = new double(size);
}


ArrayRaii::~ArrayRaii()
{
    delete[] array_;
}


double* ArrayRaii::GetArray() const
{
    return array_;
}


int Function()
{
    ArrayRaii array(5);

    if (condition) 
        return 1;
    // ...
    OtherFunctionThatMightThrow();
    
    return 0;
}


