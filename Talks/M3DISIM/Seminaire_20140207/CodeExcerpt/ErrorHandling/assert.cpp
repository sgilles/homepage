GeometricMeshRegion mesh;
auto ptr = mesh.GetGeometricElementPtr(geometric_type, i);
assert(ptr != nullptr);
unsigned int index = ptr->GetIndex();


inline const QuadraturePoint& CurrentFiniteElement::QuadraturePoint(unsigned int i) const
{
   assert(i < current_quadrature_point_.size());
   assert(!(!current_quadrature_point_[i]));
   return *current_quadrature_point_[i];
}


assert(std::is_sorted(ghosted_dof.cbegin(), ghosted_dof.cend()));
 

 
 
#ifndef NDEBUG
const auto& mpi = MpiHappyHeart();
        
if (mpi.Nprocessor<int>() == 1)
    assert(Nprocessor_wise_dof_ == Nprogram_wise_dof_);
else
    assert(mpi.AllReduce(Nprocessor_wise_dof_, MPI_SUM) == Nprogram_wise_dof_);
#endif // NDEBUG