#include <exception>
#include <vector>
#include <iostream>

int main()
{
    std::vector<int> vector(5);
   
    try
    {        
        vector.at(7);
    }
    catch(const std::exception& e)
    {
        std::cerr << "Exception found!" << std::endl;
    }
    
    
}