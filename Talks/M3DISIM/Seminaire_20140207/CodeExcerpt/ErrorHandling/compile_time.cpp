template<class T>
constexpr T UninitializedIndex()
{
    static_assert(std::is_integral<T>(), "Only relevant for integral types.");
    return std::numeric_limits<T>::max();
}



template<class InterfaceT>
unsigned int Nvertice(const InterfaceT& interface)
{
    return interface.Nvertice();
}