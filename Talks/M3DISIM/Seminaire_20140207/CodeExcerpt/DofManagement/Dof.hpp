class Dof final
{
  public:    
     ...
    
    ///@}
    
    /*!
     * \brief Processor-wise or ghost index.
     *
     * ...
     *
     * This index should typically be used when an element must be fetched in a parallel Petsc::Vector.
     */
    unsigned int GetProcessorWiseOrGhostIndex() const;
    
    /*!
     * \brief Program-wise index.
     *
     * This index is useful when you deal with a parallel matrix: for instance if you need to zero a row
     * for a boundary condition, Petsc expects a program-wise index to do so.
     */
    unsigned int GetProgramWiseIndex() const;
    
    ...
};

    


