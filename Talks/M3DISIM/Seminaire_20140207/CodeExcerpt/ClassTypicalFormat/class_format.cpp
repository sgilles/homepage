#include <ostream>

class GeometricElement
{

public:
    
    //! Constructor(s).
    explicit GeometricElement(*arguments*);
    
    //! Destructor.
    ~GeometricElement() = default;
    
    //! Recopy constructor.
    GeometricElement(const GeometricElement&) = default; // = delete to forbid it.
    
    //! Operator=.
    GeometricElement& operator=(const GeometricElement&) = default;
    
    //! Move constructor (C++ 11).
    GeometricElement(GeometricElement&&) = default;
    
    // Public methods.
    
protected:
    
    // Protected methods.
    
private:
    
    // Private methods.
    
private:
    
    // Private data attributes. All data attributes should be private!
        
};

//! Overload operator<.
bool operator<(const GeometricElement& element1, const GeometricElement& element2);

//! Overload operator==.
bool operator==(const GeometricElement& element1, const GeometricElement& element2);

// Other non member functions.


namespace std
{
    //! Defines what is written when element is sent to a stream.
    std::ostream& operator<<(std::ostream& stream, const GeometricElement& element);    
}
