void LinearProblem::assembleMatrixRHSBD(int rank, int flagMatrixRHS)
{
IGNORE_UNUSED_RANK;
ElementType eltType;           //geometric element type in the mesh.
int numPointPerElt = 0;        //number of points per geometric element.
int currentLabel = 0;          //number of label domain.
felInt numEltPerLabel = 0;        //number of element for one label and one eltType.

// use to define a "global" numbering of element in the mesh.
felInt numElement[ _meshLocal._numTypesOfElement ];
for (int ityp=0; ityp< _meshLocal._numTypesOfElement; ityp++ ) {
  eltType = (ElementType)ityp;
  numElement[eltType] = 0;
}

// contains points of the current element.
vector<Point*> elemPoint;

// contains ids of point of the current element.
vector<felInt> elemIdPoint;

// contains the ids of the support elements of a mesh element
vector<felInt> vectorIdSupport;

// use to get element number in support dof mesh ordering.
felInt ielSupportDof;

//Assembly loop.
//First loop on geometric type.
const vector<ElementType>& bagElementTypeDomain = _meshLocal.bagElementTypeDomain();
for (size_t i = 0; i < bagElementTypeDomain.size(); ++i){
  eltType =  bagElementTypeDomain[i];
  
  //resize array.
  numPointPerElt = _meshLocal._numPointsPerElt[eltType];
  elemPoint.resize(numPointPerElt, 0);
  elemIdPoint.resize(numPointPerElt, 0);
  
  //define all current finite element use in the problem from data
  //file configuration and allocate elemMat and elemVec (question: virtual ?).
  defineCurvilinearFiniteElement(eltType);
  
  //allocate array use for assemble with petsc.
  allocateArrayForAssembleMatrixRHSBD(flagMatrixRHS);
  
  //virtual function use in derived problem to allocate elemenField necessary.
  initPerElementTypeBD(eltType, flagMatrixRHS);
  
  // use by user to add specific term (term source for example with elemField).
  userElementInitBD();
  
  //second loop on region of the mesh.
  for ( GeometricMeshRegion::IntRefToBegEndIndex_type::const_iterator itRef = _meshLocal.intRefToBegEndMaps[eltType].begin();
       itRef != _meshLocal.intRefToBegEndMaps[eltType].end(); itRef++) {
    currentLabel = itRef->first;
    numEltPerLabel = itRef->second.second;
    
    //By default this virtual defnie variable _currentLabel: (_currentLabel=label).
    //We can switch on label region with that and define some parameters.
    initPerDomainBD(currentLabel, flagMatrixRHS);
    
    //Third loop on element in the region with the type: eltType. ("real" loop on elements).
    for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
      // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
      setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);
      
      // loop over all the support elements
      for (size_t it = 0; it < vectorIdSupport.size(); it++){
        // get the id of the support
        ielSupportDof = vectorIdSupport[it];
        
        //todo Jeremie 01 02 2012: assemble multiple matrix
        for (size_t j = 0; j < _Matrix.size();j++)
          _elementMatBD[j]->zero();
        
        // clear elementary vector.
        for (size_t j = 0; j < _Vector.size();j++)
          _elementVectorBD[j]->zero();
        
        // function call in derived problem to compute specific operators of the  problem (Heat, N-S,...).
        computeElementArrayBD(elemPoint, elemIdPoint, ielSupportDof,flagMatrixRHS);

        // compute specific term of users.
        userElementComputeBD(elemPoint, elemIdPoint, ielSupportDof);
        
        // add values of elemMat in the global matrix: _Matrix[0].
        setValueMatrixRHSBD(&ielSupportDof, flagMatrixRHS);
      }
      numElement[eltType]++;
    }
  }
  //allocate array use for assemble with petsc.
  desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);
}

if( (flagMatrixRHS==0) || (flagMatrixRHS==1)) {
  //real assembling of the matrix manage by PETsC.
  //todo Jeremie 01 02 2012: assemble multiple matrix
  for (size_t i = 0; i < _Matrix.size();i++){
    MatAssemblyBegin(_Matrix[i],MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(_Matrix[i],MAT_FINAL_ASSEMBLY);
  }
}
//call with high level of verbose to print matrix in matlab format.
writeMatrixForMatlab(_verbose);

if( (flagMatrixRHS==0) || (flagMatrixRHS==2)){
  //real assembling of the right hand side (RHS).
  for (size_t index = 0; index < _Vector.size();index++){
    VecAssemblyBegin(_Vector[index]);
    VecAssemblyEnd(_Vector[index]);
  }
}
//call with high level of verbose to print right hand side in matlab format.
writeRHSForMatlab(_verbose);

elemPoint.clear();
elemIdPoint.clear();
}
