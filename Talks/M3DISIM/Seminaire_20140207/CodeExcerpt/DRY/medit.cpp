void Medit::readerMedit(GeometricMeshRegion & mesh, double spaceUnit)
{
    ...
    //------------
    // Triangles3:
    eltType = _eltFelNameToMeditPair["Triangles3"].first;
    keyWord = _eltFelNameToMeditPair["Triangles3"].second;
    numElems = mesh.numElements(eltType);
    nVpEl = GeometricMeshRegion::_numPointsPerElt[eltType];
    if ( numElems != 0 ) {
      // read the Triangles3 (3 points)
      GmfGotoKwd(InpMsh, keyWord);
      elem.resize(nVpEl, 0);
      for(felInt iel = 0; iel < numElems; iel++) {
        GmfGetLin(InpMsh, keyWord, 
                  &elem[0], &elem[1], &elem[2],
                  &the_ref);
        mesh.setOneElement(eltType, iel, elem, decr_vert_id);
        RefToElementsMaps[ eltType ][ the_ref ].push_back( iel );
        intRefToEnum[ the_ref ].insert( eltType );
      }
    }
        
    //------------
    // Tetrahedra4:
    eltType = _eltFelNameToMeditPair["Tetrahedra4"].first;
    keyWord = _eltFelNameToMeditPair["Tetrahedra4"].second;
    numElems = mesh.numElements(eltType);
    nVpEl = GeometricMeshRegion::_numPointsPerElt[eltType];
    if ( numElems != 0 ) {
      // read the Tetrahedra4 (4 points)
      GmfGotoKwd(InpMsh, keyWord);
      elem.resize(nVpEl, 0);
      for(felInt iel = 0; iel < numElems; iel++) {
        GmfGetLin(InpMsh, keyWord, 
                  &elem[0], &elem[1], &elem[2], &elem[3], 
                  &the_ref);
        mesh.setOneElement(eltType, iel, elem, decr_vert_id);
        RefToElementsMaps[ eltType ][ the_ref ].push_back( iel );
        intRefToEnum[ the_ref ].insert( eltType );
      }
    }
    
    
    // Hexahedra27
    // Hexahedra8
    // Prisms6
    // Tetrahedra10
    // Tetrahedra4
    // Quadrangles9
    // Quadrangles4
    // Triangles6
    // Segments2
  
}




void MeditRead::ReadGeometricElements(const GeometricMeshRegion& mesh, LabelHelper& label_helper)
{
    // Iterate through all registered geometric elements
    const auto& geometric_element_factory = Private::GeometricElementFactory::Instance();
    const auto& medit_type_list = geometric_element_factory.MeditGeometricElementTypeList();

    auto Nvertice = static_cast<unsigned int>(GmfStatKwd(mesh_index_, GmfVertices));
    
    unsigned int geometric_element_index = 1; // Medit convention is to make indexes begin at 1.
    
    for (auto type : medit_type_list)
    {
        const auto& typeCode = type.first; // alias

        GmfGotoKwd(mesh_index_, typeCode);

        // Check whether there are geometric elements of this kind in the file
        auto number = static_cast<unsigned int>(GmfStatKwd(mesh_index_, typeCode));
        if (number == 0)
            continue;

        const auto& createGeometricElementFunction = type.second; // alias

        for (unsigned int i = 0; i < number; ++i)
        {
            GeometricElement::shared_ptr new_geometric_element_ptr(createGeometricElementFunction());
            GeometricElement& new_geometric_element = *new_geometric_element_ptr;

            int label_int;
            new_geometric_element.ReadMeditFormat(mesh, mesh_index_, Nvertice, label_int);

            auto label = label_helper.FetchLabel(static_cast<unsigned int>(label_int));

            new_geometric_element.SetLabel(label);
            new_geometric_element.SetIndex(geometric_element_index++);

            geometric_element_list_.push_back(new_geometric_element_ptr);
        }
    }
}


