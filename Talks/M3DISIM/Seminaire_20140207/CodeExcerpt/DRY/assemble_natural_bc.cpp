void LinearProblem::assembleMatrixRHSNaturalBoundaryCondition(int rank, int flagMatrixRHS, const int idBCforLinCombMethod)
{
    IGNORE_UNUSED_RANK;
    ElementType eltType;           //geometric element type in the mesh.
    int numPointPerElt = 0;        //number of points per geometric element.
    int currentLabel = 0;          //number of label domain.
    felInt numEltPerLabel = 0;     //number of element for one label and one eltType.
    // use to define a "global" numbering of element in the mesh.
    felInt numElement[ _meshLocal._numTypesOfElement ];
    for (int ityp=0; ityp< _meshLocal._numTypesOfElement; ityp++ ) {
        eltType = (ElementType)ityp;
        numElement[eltType] = 0;
    }
    
    // contains points of the current element.
    vector<Point*> elemPoint;
    
    // contains ids of point of the current element.
    vector<felInt> elemIdPoint;
    
    // contains ids of all the support elements associated to a mesh element
    vector<felInt> vectorIdSupport;
    
    // use to get element number in support dof mesh ordering.
    felInt ielSupportDof;
    
    allocateVectorBoundaryConditionDerivedLinPb();
    
    //First loop on geometric type.
    const vector<ElementType>& bagElementTypeDomainBoundary = _meshLocal.bagElementTypeDomainBoundary();
    for (size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i){
        eltType =  bagElementTypeDomainBoundary[i];
        //resize array.
        numPointPerElt = _meshLocal._numPointsPerElt[eltType];
        elemPoint.resize(numPointPerElt, 0);
        elemIdPoint.resize(numPointPerElt, 0);
        
        //define all current finite element use in the problem from data
        //file configuration and allocate elemMat and elemVec (question: virtual ?).
        defineCurvilinearFiniteElement(eltType);
        
        //allocate array use for assemble with petsc.
        allocateArrayForAssembleMatrixRHSBD(flagMatrixRHS);
        
        //Function (1), (2), (3) doing the same stuff for different use: allocate/initialize elementField
        
        // (1) virtual function use in derived problem to allocate elemenField necessary.
        initPerElementTypeBoundaryCondition(eltType, flagMatrixRHS);
        
        // (2) use by user to add specific term (term source for example with elemField).
        userElementInitNaturalBoundaryCondition();
        
        // (3) allocate Elemfield and setValue if BC = constant in space
        // if BC = function : setValue in users
        allocateElemFieldBoundaryCondition(idBCforLinCombMethod);
        
        //second loop on region of the mesh.
        for ( GeometricMeshRegion::IntRefToBegEndIndex_type::const_iterator itRef = _meshLocal.intRefToBegEndMaps[eltType].begin();
             itRef != _meshLocal.intRefToBegEndMaps[eltType].end(); itRef++) {
            currentLabel = itRef->first;
            numEltPerLabel = itRef->second.second;
            
            //By default this virtual define variable _currentLabel: (_currentLabel=label).
            //We can switch on label region with that and define some parameters.
            //We can also fill the elemField allocated in initPerElementTypeBoundaryCondition()
            initPerDomainBoundaryCondition(elemPoint, elemIdPoint, currentLabel, numEltPerLabel, &ielSupportDof, eltType, numElement[eltType], flagMatrixRHS);
            
            //Third loop on element in the region with the type: eltType. ("real" loop on elements).
            for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
                // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
                setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);
                
                // loop over all the support elements
                for (size_t it = 0; it < vectorIdSupport.size(); it++){
                    // get the id of the support
                    ielSupportDof = vectorIdSupport[it];
                    
                    
                    // clear elementary matrix.
                    // todo Jeremie 01 02 2012: assemble multiple matrix
                    for (size_t j = 0; j < _Matrix.size();j++)
                        _elementMatBD[j]->zero();
                    
                    // clear elementary vector.
                    for (size_t j = 0; j < _Vector.size();j++)
                        _elementVectorBD[j]->zero();
                    
                    // function call in derived problem to compute specific operators of the  problem (Heat, N-S,...).initialized in (1)
                    computeElementArrayBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof,flagMatrixRHS);
                    
                    // compute specific term of users (Neumann transient for example) initialized in (2)
                    userElementComputeNaturalBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof, currentLabel);
                    
                    // apply bc initialized in (3) if constant or initialized in (1) or (2) if functions
                    applyNaturalBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof, flagMatrixRHS);
                    
                    // add values of elemMat in the global matrix: _Matrix[0].
                    setValueMatrixRHSBD(&ielSupportDof, flagMatrixRHS);
                }
                numElement[eltType]++;
            }
        }
        //allocate array use for assemble with petsc.
        desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);
    }
    
    if( (flagMatrixRHS==0) || (flagMatrixRHS==1)) {
        //real assembling of the matrix manage by PETsC.
        for (size_t i = 0; i < _Matrix.size();i++){
            MatAssemblyBegin(_Matrix[i],MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd(_Matrix[i],MAT_FINAL_ASSEMBLY);
        }
    }
    
    //call with high level of verbose to print matrix in matlab format.
    writeMatrixForMatlab(_verbose);
    
    if( (flagMatrixRHS==0) || (flagMatrixRHS==2)){
        //real assembling of the right hand side (RHS).
        for (size_t index = 0; index < _Vector.size();index++){
            VecAssemblyBegin(_Vector[index]);
            VecAssemblyEnd(_Vector[index]);
        }
    }
    
    //call with high level of verbose to print right hand side in matlab format.
    writeRHSForMatlab(_verbose);
    
    elemPoint.clear();
    elemIdPoint.clear();
}