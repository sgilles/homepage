template <class LawPolicyT, HyperelasticityNS::TimeScheme TimeSchemeT>
class LinearProblemHyperElasticity : public LinearProblem, public LawPolicyT
{ ... };



class Edge : public Private::Interface::Impl<Edge>
{
public: 
       
   /*!
    * \brief Constructor.
    *
    * This constructor only attributes a dummy value to the identifier; the true construction of the 
    * object requires the call to InitInterface().
    */
   Edge() = default;
   
   //! Destructor.
   ~Edge() = default;
};


class Face : public Private::Interface::Impl<Face>
{
public:
 
   /*!
    * \brief Constructor.
    *
    * This constructor only attributes a dummy value to the identifier; the true construction of the 
    * object requires the call to InitInterface().
    */
   Face() = default;
   
   //! Destructor.
   ~Face() = default;
};



typedef std::tuple
<
   InputParameter::Variable::Variable,
   InputParameter::Variable::DegreeOfExactness,
   InputParameter::Variable::TypeOfFiniteElement,
   ...

   InputParameter::Solid::Poisson,
   InputParameter::Solid::Young,
   InputParameter::Solid::VolumicMass,
> HyperelasticityParameters;
