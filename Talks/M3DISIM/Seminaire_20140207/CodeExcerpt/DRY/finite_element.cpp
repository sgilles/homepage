class ElementMatrix
{
    // Constructor for CurrentFiniteElement.
    ElementMatrix(const CurrentFiniteElement& fe, size_t nbr1, size_t nbc1);
    
    // Constructor for CurvilinearFiniteElement; implementation is exactly the same
    // as the above constructor!
    ElementMatrix(const CurvilinearFiniteElement& fe, size_t nbr1, size_t nbc1);
;


class CurBaseFiniteElement 
{ ... };

class CurrentFiniteElement : public CurBaseFiniteElement 
{ ... };

class CurvilinearFiniteElement : public CurBaseFiniteElement
{ ... };


class ElementMatrix
{
    ElementMatrix(const CurBaseFiniteElement& fe, size_t nbr1, size_t nbc1);
};