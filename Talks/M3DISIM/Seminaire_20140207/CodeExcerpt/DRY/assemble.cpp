/*!
 \brief elementary loop to build matrix.
 This function uses specific functions which are defined in derived problem classes.
 */
void LinearProblem::assembleMatrixRHS(int rank, int flagMatrixRHS)
{
  IGNORE_UNUSED_RANK;
  ElementType eltType;           // geometric element type in the mesh.
  int numPointPerElt = 0;        // number of points per geometric element.
  int currentLabel = 0;          // number of label domain.
  felInt numEltPerLabel = 0;     // number of element for one label and one eltType.

  vector<felInt> vectorIdSupport; // for one element, list of all the associated support elements.
                                  // it assumes that the number of support elements for a given element is
                                  // the same for all unknown. If it is not the case, one need to implement
                                  // an assembling of the matrix by block ?

  // use to define a "global" numbering of element in the mesh.
  felInt numElement[ _meshLocal._numTypesOfElement ];
  for (int ityp=0; ityp< _meshLocal._numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // contains points of the current element.
  vector<Point*> elemPoint;
  // contains ids of point of the current element.
  vector<felInt> elemIdPoint;
  // use to get element number in support dof mesh ordering.
  felInt ielSupportDof;

  // Assembly loop.
  // First loop on geometric type.
  const vector<ElementType>& bagElementTypeDomain = _meshLocal.bagElementTypeDomain();
  for (size_t i = 0; i < bagElementTypeDomain.size(); ++i){
    eltType =  bagElementTypeDomain[i];
    
    // resize array.
    numPointPerElt = _meshLocal._numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, 0);
    elemIdPoint.resize(numPointPerElt, 0);
    
    // define all current finite element use in the problem from data
    // file cnfiguration and allocate    1 and elemVec (question: virtual ?).
    defineFiniteElement(eltType);

    // allocate array use for assemble with petsc.
    allocateArrayForAssembleMatrixRHS(flagMatrixRHS);

    // virtual function use in derived problem to allocate elemenField necessary.
    initPerElementType(eltType, flagMatrixRHS);

    // used by user to add specific term (source term for example with elemField).
    userElementInit();

    // second loop on region of the mesh.
    for(GeometricMeshRegion::IntRefToBegEndIndex_type::const_iterator itRef = _meshLocal.intRefToBegEndMaps[eltType].begin();
        itRef != _meshLocal.intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;


      // By default this virtual defines variable _currentLabel: (_currentLabel=label).
      // We can switch on label region with that and define some parameters.
      initPerDomain(currentLabel, flagMatrixRHS);
      
      // Third loop on element in the region with the type: eltType. ("real" loop on elements).
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
        assert(!_elementVector.empty());

        // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
        setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);
       
        // Fourth loop over all the support elements
        for (size_t it = 0; it < vectorIdSupport.size(); it++){
          // get the id of the support
          ielSupportDof = vectorIdSupport[it];

          // clear elementary matrices 
          for (size_t j = 0, size = _Matrix.size(); j < size ;j++)
            _elementMat[j]->zero();

          // clear elementary vector.
          for (size_t j = 0, size = _Vector.size(); j < size ;j++)
            _elementVector[j]->zero();
        
          // function call in derived problem to compute specific operators of the  problem (Heat, N-S,...).
          if (_fsparam->NSequationFlag == 1 && _fsparam->characteristicMethod != 0){
            chronoAssembleCharact.start() ;
          
            // use method of characteristics for N-S
            computeElementArrayCharact(elemPoint, elemIdPoint, ielSupportDof, eltType, numElement[eltType], flagMatrixRHS);
          
            chronoAssembleCharact.stop();
            _assembleCharactTime += chronoAssembleCharact.diff();
          }
          else if(_fsparam->duplicateSupportDof)
            computeElementArray(elemPoint, elemIdPoint, ielSupportDof, eltType, numElement[eltType], flagMatrixRHS);
          else
            computeElementArray(elemPoint, elemIdPoint, ielSupportDof, flagMatrixRHS);
        
          // compute specific term of users.
          userElementCompute(elemPoint, elemIdPoint, ielSupportDof);

          // add values of elemMat in the global matrix: _Matrix[0].
          setValueMatrixRHS(&ielSupportDof, flagMatrixRHS);
        }
        numElement[eltType]++;
      }
    }
    // desallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);
  }

  // Assembly loop for LumpedModelBC in case of NS model with implicit implementation
  if (_fsparam->lumpedModelBCLabel.size()>0 &&  _fsparam->model == "NS") {
    for (size_t iLabel = 0; iLabel < _fsparam->lumpedModelBCLabel.size(); iLabel++) {
      if(_fsparam->lumpedModelBCAlgo[iLabel] == 2) // use a Enum here... 08/13 VM
        assembleMatrixImplLumpedModelBC(rank, iLabel);
    }
  }

  if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_matrix)) {
    // real assembling of the matrix manage by PETsC.
    // todo Jeremie 01 02 2012: assemble multiple matrix
    for (size_t i = 0; i < _Matrix.size();i++){
      MatAssemblyBegin(_Matrix[i],MAT_FINAL_ASSEMBLY);
      MatAssemblyEnd(_Matrix[i],MAT_FINAL_ASSEMBLY);
    }
  }

  // call with high level of verbose to print matrix in matlab format.
  writeMatrixForMatlab(_verbose);
  if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_rhs)){
    // real assembling of the right hand side (RHS).
    for (size_t i = 0; i < _Vector.size();i++){
      VecAssemblyBegin(_Vector[i]);
      VecAssemblyEnd(_Vector[i]);
    }
  }
  // call with high level of verbose to print right hand side in matlab format.
  writeRHSForMatlab(_verbose);

  elemPoint.clear();
  elemIdPoint.clear();
}