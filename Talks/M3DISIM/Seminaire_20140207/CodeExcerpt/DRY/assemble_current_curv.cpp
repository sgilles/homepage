void LinearProblem::assembleMatrixRHSCurrentAndCurvilinearElement(int rank, int flagMatrixRHS)
{
    IGNORE_UNUSED_RANK;
    
    ElementType eltType;           //geometric element type in the mesh.
    int numPointPerElt = 0;        //number of points per geometric element.
    int currentLabel = 0;          //number of label domain.
    felInt numEltPerLabel = 0;     //number of element for one label and one eltType.
    
    // use to define a "global" numbering of element in the mesh.
    felInt numElement[ _meshLocal._numTypesOfElement ];
    for (int ityp=0; ityp< _meshLocal._numTypesOfElement; ityp++ ) {
        eltType = (ElementType)ityp;
        numElement[eltType] = 0;
    }
    
    // contains points of the current element.
    vector<Point*> elemPoint;
    
    // contains ids of point of the current element.
    vector<felInt> elemIdPoint;
    
    // contains ids of support elements of a mesh element
    vector<felInt> vectorIdSupport;
    
    // use to get element number in support dof mesh ordering.
    felInt ielSupportDof;
    felInt ielSupportDofCurv;
    
    //Assembly loop current.
    const vector<ElementType>& bagElementTypeDomain = _meshLocal.bagElementTypeDomain();
    for (size_t i = 0; i < bagElementTypeDomain.size(); ++i){
        eltType =  bagElementTypeDomain[i];
        //resize array.
        numPointPerElt = _meshLocal._numPointsPerElt[eltType];
        elemPoint.resize(numPointPerElt, 0);
        elemIdPoint.resize(numPointPerElt, 0);
        
        //define all current finite element use in the problem from data
        //file cnfiguration and allocate    1 and elemVec (question: virtual ?).
        defineFiniteElement(eltType);
        
        //allocate array use for assemble with petsc.
        allocateArrayForAssembleMatrixRHS(flagMatrixRHS);
        
        //virtual function use in derived problem to allocate elemenField necessary.
        initPerElementType(eltType, flagMatrixRHS);
        
        // use by user to add specific term (source term for example with elemField).
        userElementInit();
        
        //second loop on region of the mesh.
        for(GeometricMeshRegion::IntRefToBegEndIndex_type::const_iterator itRef = _meshLocal.intRefToBegEndMaps[eltType].begin();
            itRef != _meshLocal.intRefToBegEndMaps[eltType].end(); itRef++) {
            currentLabel = itRef->first;
            numEltPerLabel = itRef->second.second;
            
            //By default this virtual defnie variable _currentLabel: (_currentLabel=label).
            //We can switch on label region with that and define some parameters.
            initPerDomain(currentLabel, flagMatrixRHS);
            
            //Third loop on element in the region with the type: eltType. ("real" loop on elements).
            for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
                
                // Sebastien
                // If assert is always passed as I believe it is it means numElement is just code obfuscation...
                // As a matter of fact acording to its prototype computeElementArray(Charact) would be able
                // to modify it so that it differs from iel, but practically
                // . Both cases implemented don't actually use the argument
                // . It would be a lousy way to proceed, as the loop would go on the expected number of
                // iterations no matter what is the new index, with risks of skipping some elements
                // of the array
                // It seems to be the case; but I'd rather not change it until I have time to look at it in detail and be 100 %
                // sure I can tackle with this code
                //FEL_ASSERT(iel == numElement[eltType]);
                
                // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
                setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);
                
                // loop over all the support elements
                for (size_t it = 0; it < vectorIdSupport.size(); it++){
                    // get the id of the support
                    ielSupportDof = vectorIdSupport[it];
                    
                    //todo Jeremie 01 02 2012: assemble multiple matrix
                    for (size_t j = 0; j < _Matrix.size();j++)
                        _elementMat[j]->zero();
                    
                    // clear elementary vector.
                    for (size_t j = 0; j < _Vector.size();j++)
                        _elementVector[j]->zero();
                    
                    // function call in derived problem to compute specific operators of the  problem (Heat, N-S,...).
                    computeElementArray(elemPoint, elemIdPoint, ielSupportDof, flagMatrixRHS);
                    
                    // compute specific term of users.
                    //userElementCompute(elemPoint, elemIdPoint, ielSupportDof);
                    
                    // add values of elemMat in the global matrix: _Matrix[0].
                    setValueMatrixRHS(&ielSupportDof, flagMatrixRHS);
                }
                numElement[eltType]++;
            }
        }
        //desallocate array use for assemble with petsc.
        desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);
    }
    
    //Assembly loop curvilinear.
    const vector<ElementType>& bagElementTypeDomainBoundary = _meshLocal.bagElementTypeDomainBoundary();
    for (size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i){
        eltType =  bagElementTypeDomainBoundary[i];
        
        //resize array.
        numPointPerElt = _meshLocal._numPointsPerElt[eltType];
        elemPoint.resize(numPointPerElt, 0);
        elemIdPoint.resize(numPointPerElt, 0);
        
        //define all current finite element use in the problem from data
        //file configuration and allocate elemMat and elemVec (question: virtual ?).
        defineCurvilinearFiniteElement(eltType);
        
        //allocate array use for assemble with petsc.
        allocateArrayForAssembleMatrixRHSBD(flagMatrixRHS);
        
        //virtual function use in derived problem to allocate elemenField necessary.
        initPerElementTypeBoundaryCondition(eltType, flagMatrixRHS);
        
        // use by user to add specific term (term source for example with elemField).
        userElementInitNaturalBoundaryCondition();
        
        assert(!_elementVectorBD.empty());
        
        // allocate elem field and setValue if BC = constant in space
        // if BC = function : setValue in users
        //      allocateElemFieldBoundaryCondition(idBCforLinCombMethod);
        //second loop on region of the mesh.
        for ( GeometricMeshRegion::IntRefToBegEndIndex_type::const_iterator itRef = _meshLocal.intRefToBegEndMaps[eltType].begin();
             itRef != _meshLocal.intRefToBegEndMaps[eltType].end(); itRef++) {
            currentLabel = itRef->first;
            numEltPerLabel = itRef->second.second;
            
            //By default this virtual define variable _currentLabel: (_currentLabel=label).
            //We can switch on label region with that and define some parameters.
            //We can also fill the elemField allocated in initPerElementTypeBoundaryCondition()
            initPerDomainBoundaryCondition(elemPoint, elemIdPoint, currentLabel, numEltPerLabel, &ielSupportDofCurv, eltType, numElement[eltType], flagMatrixRHS);
            
            //Third loop on element in the region with the type: eltType. ("real" loop on elements).
            for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
                // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
                setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);
                
                // loop over all the support elements
                for (size_t it = 0; it < vectorIdSupport.size(); it++){
                    // get the id of the support
                    ielSupportDofCurv = vectorIdSupport[it];
                    
                    // clear elementary matrix.
                    //todo Jeremie 01 02 2012: assemble multiple matrix
                    for (size_t j = 0; j < _Matrix.size();j++)
                        _elementMatBD[j]->zero();
                    
                    // clear elementary vector.
                    for (size_t j = 0; j < _Vector.size();j++)
                        _elementVectorBD[j]->zero();
                    
                    // function call in derived problem to compute specific operators of the  problem (Heat, N-S,...).
                    computeElementArrayBoundaryCondition(elemPoint, elemIdPoint, ielSupportDofCurv,flagMatrixRHS);
                    
                    // compute specific term of users (Neumann transient for example).
                    //userElementComputeNaturalBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof, currentLabel);
                    
                    //applyNaturalBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof);
                    // add values of elemMat in the global matrix: _Matrix[0].
                    if ((currentLabel >= 30) && (currentLabel <= 42)){
                        //computeElementArrayBoundaryCondition(elemPoint, elemIdPoint, ielSupportDofCurv,flagMatrixRHS);
                        setValueMatrixRHSBD(&ielSupportDofCurv, flagMatrixRHS);
                    }
                }
                numElement[eltType]++;
            }
        }
        //allocate array use for assemble with petsc.
        desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);
    }
    
    if( (flagMatrixRHS==0) || (flagMatrixRHS==1)) {
        //real assembling of the matrix manage by PETsC.
        for (size_t i = 0; i < _Matrix.size();i++){
            MatAssemblyBegin(_Matrix[i],MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd(_Matrix[i],MAT_FINAL_ASSEMBLY);
        }
    }
    //call with high level of verbose to print matrix in matlab format.
    writeMatrixForMatlab(_verbose);
    if( (flagMatrixRHS==0) || (flagMatrixRHS==2)){
        //real assembling of the right hand side (RHS).
        for (size_t index = 0; index < _Vector.size();index++){
            VecAssemblyBegin(_Vector[index]);
            VecAssemblyEnd(_Vector[index]);
        }
    }
    //call with high level of verbose to print right hand side in matlab format.
    writeRHSForMatlab(_verbose);
    
    elemPoint.clear();
}