#include <stdio.h>
#include <assert.h>

struct Square
{
    float side_length;
    float area;
};

void Print(struct Square* square)
{
    assert(square);
    printf("The length of a side is %f so the area is %f\n", square->side_length, square->area);
}


int main()
{
    struct Square square;
    square.side_length = 5.f;
    square.area = square.side_length * square.side_length;       
    Print(&square);
    
    square.side_length = 10.f;
    Print(&square);
}