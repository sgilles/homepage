// In Square.hpp
class Square
{
public:
    
    //! Constructor.
    explicit Square(float side_length);
    
    //! Change the length of a side.
    void SetSideLength(float side_length);
    
    //! Returns the length of a side.
    float GetSideLength() const;
    
    //! Returns the area.
    float GetArea() const;
        
private:

    //! Length of a side.
    float side_length_;
    
    //! Area.
    float area_;
};

//! Print informations related to \a square.
void Print(const Square& square);


// In Square.cpp
void Print(const Square& square)
{
    std::cout << "The length of a side is " << square.GetSideLength() << " so the area is " << square.GetArea() << std::endl;
}


Square::Square(float side_length)
{
    SetSideLength(side_length);
}

void Square::SetSideLength(float side_length)
{
    side_length_ = side_length;
    area_ = side_length * side_length;
}

float Square::GetSideLength() const
{
    return side_length_;
}


float Square::GetArea() const
{
    return area_;
}


// In main.cpp
int main()
{
    Square square(5.f);
    Print(square);
    
    square.SetSideLength(10.f);
    Print(square);
    
    square.side_length = 15.f; // compilation error!     
}