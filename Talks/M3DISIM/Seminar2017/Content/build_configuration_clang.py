# Build and intermediate build directories.
# Subdirectories for each type of compilation will be created there.
BUILD_DIR = '/Volumes/Data/sebastien/HappyHeart/Build'
INTERMEDIATE_BUILD_DIR =  '/Volumes/Data/sebastien/HappyHeart/IntermediateBuild'

# Choose C and C++ compilers. You might also specifies here clang static analyzer (paths to ccc-analyzer and c++-analyzer respectively) to perform static analysis of the code.
COMPILER= 'clang' 
COMPILER_DIRECTORY = 'clang'
CC = '/Users/Shared/LibraryVersions/clang/Openmpi/bin/mpicc'
CXX = '/Users/Shared/LibraryVersions/clang/Openmpi/bin/mpic++'

# Choose either 'full_debug', 'debug' or 'release'. 'full_debug' defines additional macros that activate even more checks and warning prints.
# (so far only HAPPY_HEART_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE).
MODE='debug'

# Choose either 'static' or 'shared'.
LIBRARY_TYPE='static'

# Whether specific macros should be activated or not.

    # If true, add a (costly) method that gives an hint whether an UpdateGhost() call was relevant or not.
HAPPY_HEART_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE = False

    # If true, TimeKeep gains the ability to track times between each call of PrintTimeElapsed(). If not, PrintTimeElapsed() is flatly ignored.
HAPPY_HEART_EXTENDED_TIME_KEEP = False

    # If true, there are additional checks that no nan and inf appears in the code. Even if False, solver always check for the validity of its solution (if a nan or an inf is present the SolveLinear() or SolveNonLinear() operation throws with a dedicated Petsc error). Advised in debug mode and up to you in release mode.
HAPPY_HEART_CHECK_NAN_AND_INF = True

# OpenMPI libary.
OPEN_MPI_INCL_DIR='/Users/Shared/LibraryVersions/clang/Openmpi/include'
OPEN_MPI_LIB_DIR='/Users/Shared/LibraryVersions/clang/Openmpi/lib'

# BLAS Library.
BLAS_CUSTOM_LINKER=True
BLAS_LIB_DIR=None
BLAS_LIB='-framework Accelerate'

# Petsc library.
PETSC_GENERAL_INCL_DIR='/Users/Shared/LibraryVersions/clang/Petsc/include'
PETSC_DEBUG_INCL_DIR='/Users/Shared/LibraryVersions/clang/Petsc/debug/include'
PETSC_RELEASE_INCL_DIR='/Users/Shared/LibraryVersions/clang/Petsc/release/include'

PETSC_DEBUG_LIB_DIR='/Users/Shared/LibraryVersions/clang/Petsc/debug/lib'
PETSC_RELEASE_LIB_DIR='/Users/Shared/LibraryVersions/clang/Petsc/release/lib'

# Parmetis library.
PARMETIS_INCL_DIR='/Users/Shared/LibraryVersions/clang/Parmetis/include'
PARMETIS_LIB_DIR='/Users/Shared/LibraryVersions/clang/Parmetis/lib'

# Lua library.
LUA_INCL_DIR='/Users/Shared/LibraryVersions/clang/Lua/include'
LUA_LIB_DIR='/Users/Shared/LibraryVersions/clang/Lua/lib'

# Yuni library.
YUNI_INCL_DIR='/Users/Shared/LibraryVersions/clang/Yuni/src'
YUNI_LIB_DIR='/Users/Shared/LibraryVersions/clang/Yuni/src/build/release/lib'

# Ops directory (source files are looked there and library is compiled by HappyHeart).
OPS_DIR='/Users/Shared/LibraryVersions/clang/Ops'

# Seldon directory (source files are looked there and library is compiled by HappyHeart).
SELDON_DIR='/Users/Shared/LibraryVersions/clang/Seldon'
