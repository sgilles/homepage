//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//
#include "ModelInstances/___VARIABLE_groupName:identifier___/___FILEBASENAME___.hpp"

namespace HappyHeart
{
    namespace ___VARIABLE_problemName:identifier___NS
    {
        Model::Model(const Wrappers::Mpi& mpi_ptr,
                     const InputParameterList& input_parameter_data)
        : parent(mpi_ptr, input_parameter_data)
        { }

        void Model::SupplInitialize(const InputParameterList& input_parameter_data)
        {
            // TODO: Fill the content with whatever initialization is required by the problem.
            // What is already done in base method Initialize() (and therefore must not be repeated here) is:           
            // - Creation of the geometric mesh regions.
            // - Definitions of the GodOfDof
            // What typically might be added is initialization of variational formulation(s), print on screen,
            // run of the static case, ...
        }

        void Model::Forward()
        {
            // TODO: Define the forward operations. In case of a variational problem; it probably means delegating 
            // work to an underlying variational formulation object.
        }
        
        ...

    } // namespace ___VARIABLE_problemName:identifier___NS

} // namespace HappyHeart
