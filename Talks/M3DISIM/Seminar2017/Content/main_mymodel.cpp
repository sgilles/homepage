#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Core/InitHappyHeart.hpp"
#include "ModelInstances/ModelName/Model.hpp"
#include "ModelInstances/ModelName/InputParameterList.hpp"

using namespace HappyHeart;

int main(int argc, char** argv)
{
	using InputParameterList = MyModelNS::InputParameterList;
    
	InitHappyHeart<InputParameterList> happy_heart(argc, argv);
            
	decltype(auto) input_parameter_data = happy_heart.GetInputParameterList();
	decltype(auto) mpi = happy_heart.GetMpi();
    
	MyModelNS::Model model(mpi, input_parameter_data);
	model.Run(input_parameter_data);
        
	input_parameter_data.PrintUnused(std::cout);    
	return EXIT_SUCCESS;
}

