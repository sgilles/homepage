decltype(auto) velocity_ns = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::velocity));
decltype(auto) pressure_ns = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::pressure));

GlobalMatrix velocity_velocity_matrix(velocity_ns, velocity_ns);
GlobalMatrix velocity_pressure_matrix(velocity_ns, velocity_ns);
... // initialization of Stokes matrix

stokes_operator.Assemble(std::make_tuple(std::ref(velocity_velocity_matrix)));

