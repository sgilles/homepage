decltype(auto) numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));

GlobalMatrix stokes_matrix(numbering_subset, numbering_subset);
... // initialization of Stokes matrix

stokes_operator.Assemble(std::make_tuple(std::ref(stokes_matrix))); // slightly simplified

