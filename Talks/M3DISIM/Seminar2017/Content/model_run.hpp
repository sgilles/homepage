template<class DerivedT, ...>
template<class InputParameterDataT>
void Model<DerivedT>::Run(const InputParameterDataT& input_parameter_data)
{
    auto& derived = static_cast<DerivedT&>(*this);
    
    derived.Initialize(input_parameter_data, ...);
    //< Does most of the initialization work of the library

    while (!derived.HasFinished())
    {
        derived.InitializeStep();
        derived.Forward();
        derived.FinalizeStep();
    }
    
    derived.Finalize();
}