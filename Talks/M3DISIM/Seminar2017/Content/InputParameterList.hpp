# Header guards, includes...

namespace HappyHeart
{

    namespace ElasticityNS
    {
        
		enum class ...
   
        using InputParameterTuple = std::tuple
        <
            InputParameter::TimeManager,
		
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>,
			
            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::solid_displacement)>,
			
            InputParameter::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,
			
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::neumann)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,
			
            InputParameter::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::sole)>,
			
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>,
			
            InputParameter::Petsc<EnumUnderlyingType(SolverIndex::solver)>,
			
            InputParameter::Solid::VolumicMass,
            InputParameter::Solid::YoungModulus,
            InputParameter::Solid::PoissonRatio,
            InputParameter::Solid::PlaneStressStrain,
			
            InputParameter::VectorialTransientSource<EnumUnderlyingType(SourceIndex::volumic)>,
            InputParameter::VectorialTransientSource<EnumUnderlyingType(SourceIndex::surfacic)>,
			
            InputParameter::Result
        >;

        using InputParameterList = InputParameterList<InputParameterTuple>;

    } // namespace ElasticityNS
    
} // namespace HappyHeart
