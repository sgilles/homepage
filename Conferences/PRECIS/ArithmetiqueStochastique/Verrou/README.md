# Session sur Verrou à l'école thématique PRECIS

## Cours

- [support sur verrou](https://github.com/edf-hpc/verrou/raw/ecole-precis/cours-verrou.pdf)
- [présentation rapide de Verificarlo](https://github.com/edf-hpc/verrou/raw/ecole-precis/cours-verificarlo.pdf)
  
## TP

- [énoncé](https://github.com/edf-hpc/verrou/raw/ecole-precis/TP.pdf)
- archive de code source, au
  format [tar.gz](https://github.com/edf-hpc/verrou/raw/ecole-precis/TP.tgz)
  ou [zip](https://github.com/edf-hpc/verrou/raw/ecole-precis/TP.zip)
