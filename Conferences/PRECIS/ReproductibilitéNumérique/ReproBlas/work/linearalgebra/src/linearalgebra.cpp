/*
 *************************************************************************************
 Copyright (c) 2015, University of Perpignan
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************
*/

#include <immintrin.h>
#include <stdio.h>
#include <string.h>
#include <mkl_cblas.h>
#include <summation.h>
#include <rtnblas.h>
#include <reprodblas.h>
#include <linearalgebra.h>
#include <malloc.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>

#define BLOCK 200

#ifdef STATS
static unsigned long exponentExtractCycles = 0;

void getLinearAlgebraStatistics(unsigned long *paramExponentExtractCycles) {
	*paramExponentExtractCycles = exponentExtractCycles;
}
#endif

#ifdef MPI_PARALLEL
#include <mpi.h>
static int MPI_NUMBER_OF_THREADS;
static int myRank;
static MPI_Op MPI_QUAD_SUM;
#endif

#ifdef OMP_PARALLEL
#include <omp.h>
static unsigned int OMP_NUMBER_OF_THREADS;
#endif

#ifdef SSE
#define PDSIZE 2
#define PDVECTOR __m128d
#define addvector _mm_add_pd
#define subvector _mm_sub_pd
#define mulvector _mm_mul_pd
#define andvector _mm_and_pd
#define maxvector _mm_max_pd
#define broadcast(adress) _mm_broadcastsd_pd(*(__m128d*) adress)
#define ZEROES _mm_setzero_pd()
#elif defined(AVX) || defined(AVX2)
#define PDSIZE 4
#define PDVECTOR __m256d
#define addvector _mm256_add_pd
#define subvector _mm256_sub_pd
#define mulvector _mm256_mul_pd
#define andvector _mm256_and_pd
#define maxvector _mm256_max_pd
#define broadcast(adress) _mm256_broadcast_sd(adress)
#define ZEROES _mm256_setzero_pd()
#elif defined(AVX512)
#define PDSIZE 8
#define PDVECTOR __m512d
#define addvector _mm512_add_pd
#define subvector _mm512_sub_pd
#define mulvector _mm512_mul_pd
#define absvector _mm512_abs_pd
#define maxvector _mm512_max_pd
PDVECTOR _mm512_broadcastsd_pd(__m128d);
#ifdef MIC
#define broadcast(adress) _mm512_extload_pd (adress, _MM_UPCONV_PD_NONE, _MM_BROADCAST_1X8, 0)
#else
#define broadcast(adress) _mm512_broadcastsd_pd(*(__m128d*) adress)
#endif
#define ZEROES _mm512_setzero_pd()
#endif

#if defined(AVX2)
#define fmaddvector _mm256_fmadd_pd
#define fmsubvector _mm256_fmsub_pd
#elif defined(AVX512)
#define fmaddvector _mm512_fmadd_pd
#define fmsubvector _mm512_fmsub_pd
#endif

#define L1CACHE 32768
#define L2CACHE 262144
#define CACHELINE 8
#define FPSIZE 8
#define HYBRIDSUMSIZE 2048
#define ONLINEEXACTSIZE 4096
#define MAXTRANSSIZE 40

#ifndef AVX512
static unsigned long ABS_MASK = 0x7FFFFFFFFFFFFFFF;
static PDVECTOR ABS_MASK_VECTOR;
#endif

static unsigned long SPLIT_MASK = 0xFFFFFFFFFC000000;
static double SPLIT_FACTOR, eps, eta;
static PDVECTOR SPLIT_FACTOR_VECTOR;

void linearAlgebraInitialise() {
	summationInitialise();
	rtnblasInitialise();
	reprodblasInitialise();
	eps = pow(2, -53);
	SPLIT_FACTOR = pow(2, 27) + 1;
	SPLIT_FACTOR_VECTOR = broadcast(&SPLIT_FACTOR);

#ifndef AVX512
	ABS_MASK_VECTOR = broadcast((double*) &ABS_MASK);
#endif

#ifdef OMP_PARALLEL
#ifdef MIC
	OMP_NUMBER_OF_THREADS = 240;
#else
	OMP_NUMBER_OF_THREADS = atoi(getenv("OMP_NUM_THREADS"));
#endif
#endif

#ifdef MPI_PARALLEL
	MPI_Comm_size(MPI_COMM_WORLD, &MPI_NUMBER_OF_THREADS);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
#endif

}

#ifdef SSE
#define splitScallers(X,High,Low,Temp)\
*(PDVECTOR*) &Temp[0] = mulvector(*(PDVECTOR*) &X[0], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[0] = subvector(*(PDVECTOR*) &Temp[0], subvector(*(PDVECTOR*) &Temp[0], *(PDVECTOR*) &X[0]));\
*(PDVECTOR*) &Low[0] = subvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &High[0]);\
*(PDVECTOR*) &Temp[2] = mulvector(*(PDVECTOR*) &X[2], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[2] = subvector(*(PDVECTOR*) &Temp[2], subvector(*(PDVECTOR*) &Temp[2], *(PDVECTOR*) &X[2]));\
*(PDVECTOR*) &Low[2] = subvector(*(PDVECTOR*) &X[2], *(PDVECTOR*) &High[2]);\
*(PDVECTOR*) &Temp[4] = mulvector(*(PDVECTOR*) &X[4], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[4] = subvector(*(PDVECTOR*) &Temp[4], subvector(*(PDVECTOR*) &Temp[4], *(PDVECTOR*) &X[4]));\
*(PDVECTOR*) &Low[4] = subvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &High[4]);\
*(PDVECTOR*) &Temp[6] = mulvector(*(PDVECTOR*) &X[6], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[6] = subvector(*(PDVECTOR*) &Temp[6], subvector(*(PDVECTOR*) &Temp[6], *(PDVECTOR*) &X[6]));\
*(PDVECTOR*) &Low[6] = subvector(*(PDVECTOR*) &X[6], *(PDVECTOR*) &High[6]);
#define twoProd(X,Y,mul,error,XH,XL,XT,YH,YL,YT,Temp)\
*(PDVECTOR*) &mul[0] = mulvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &mul[2] = mulvector(*(PDVECTOR*) &X[2], *(PDVECTOR*) &Y[2]);\
*(PDVECTOR*) &mul[4] = mulvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4]);\
*(PDVECTOR*) &mul[6] = mulvector(*(PDVECTOR*) &X[6], *(PDVECTOR*) &Y[6]);\
splitScallers(X,XH,XL,Temp)\
splitScallers(Y,YH,YL,Temp)\
*(PDVECTOR*) &XT[0] = subvector(mulvector(*(PDVECTOR*) &XH[0], *(PDVECTOR*) &YH[0]), *(PDVECTOR*) &mul[0]);\
*(PDVECTOR*) &YT[0] = addvector(mulvector(*(PDVECTOR*) &XH[0], *(PDVECTOR*) &YL[0]), *(PDVECTOR*) &XT[0]);\
*(PDVECTOR*) &XT[0] = addvector(mulvector(*(PDVECTOR*) &XL[0], *(PDVECTOR*) &YH[0]), *(PDVECTOR*) &YT[0]);\
*(PDVECTOR*) &error[0] = addvector(mulvector(*(PDVECTOR*) &XL[0], *(PDVECTOR*) &YL[0]), *(PDVECTOR*) &XT[0]);\
*(PDVECTOR*) &XT[2] = subvector(mulvector(*(PDVECTOR*) &XH[2], *(PDVECTOR*) &YH[2]), *(PDVECTOR*) &mul[2]);\
*(PDVECTOR*) &YT[2] = addvector(mulvector(*(PDVECTOR*) &XH[2], *(PDVECTOR*) &YL[2]), *(PDVECTOR*) &XT[2]);\
*(PDVECTOR*) &XT[2] = addvector(mulvector(*(PDVECTOR*) &XL[2], *(PDVECTOR*) &YH[2]), *(PDVECTOR*) &YT[2]);\
*(PDVECTOR*) &error[2] = addvector(mulvector(*(PDVECTOR*) &XL[2], *(PDVECTOR*) &YL[2]), *(PDVECTOR*) &XT[2]);\
*(PDVECTOR*) &XT[4] = subvector(mulvector(*(PDVECTOR*) &XH[4], *(PDVECTOR*) &YH[4]), *(PDVECTOR*) &mul[4]);\
*(PDVECTOR*) &YT[4] = addvector(mulvector(*(PDVECTOR*) &XH[4], *(PDVECTOR*) &YL[4]), *(PDVECTOR*) &XT[4]);\
*(PDVECTOR*) &XT[4] = addvector(mulvector(*(PDVECTOR*) &XL[4], *(PDVECTOR*) &YH[4]), *(PDVECTOR*) &YT[4]);\
*(PDVECTOR*) &error[4] = addvector(mulvector(*(PDVECTOR*) &XL[4], *(PDVECTOR*) &YL[4]), *(PDVECTOR*) &XT[4]);\
*(PDVECTOR*) &XT[6] = subvector(mulvector(*(PDVECTOR*) &XH[6], *(PDVECTOR*) &YH[6]), *(PDVECTOR*) &mul[6]);\
*(PDVECTOR*) &YT[6] = addvector(mulvector(*(PDVECTOR*) &XH[6], *(PDVECTOR*) &YL[6]), *(PDVECTOR*) &XT[6]);\
*(PDVECTOR*) &XT[6] = addvector(mulvector(*(PDVECTOR*) &XL[6], *(PDVECTOR*) &YH[6]), *(PDVECTOR*) &YT[6]);\
*(PDVECTOR*) &error[6] = addvector(mulvector(*(PDVECTOR*) &XL[6], *(PDVECTOR*) &YL[6]), *(PDVECTOR*) &XT[6]);
#elif defined(AVX)
#define splitScallers(X,High,Low,Temp)\
*(PDVECTOR*) &Temp[0] = mulvector(*(PDVECTOR*) &X[0], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[0] = subvector(*(PDVECTOR*) &Temp[0], subvector(*(PDVECTOR*) &Temp[0], *(PDVECTOR*) &X[0]));\
*(PDVECTOR*) &Low[0] = subvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &High[0]);\
*(PDVECTOR*) &Temp[4] = mulvector(*(PDVECTOR*) &X[4], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[4] = subvector(*(PDVECTOR*) &Temp[4], subvector(*(PDVECTOR*) &Temp[4], *(PDVECTOR*) &X[4]));\
*(PDVECTOR*) &Low[4] = subvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &High[4]);
#define twoProd(X,Y,mul,error,XH,XL,XT,YH,YL,YT,Temp)\
*(PDVECTOR*) &mul[0] = mulvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &mul[4] = mulvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4]);\
splitScallers(X,XH,XL,Temp)\
splitScallers(Y,YH,YL,Temp)\
*(PDVECTOR*) &XT[0] = subvector(mulvector(*(PDVECTOR*) &XH[0], *(PDVECTOR*) &YH[0]), *(PDVECTOR*) &mul[0]);\
*(PDVECTOR*) &YT[0] = addvector(mulvector(*(PDVECTOR*) &XH[0], *(PDVECTOR*) &YL[0]), *(PDVECTOR*) &XT[0]);\
*(PDVECTOR*) &XT[0] = addvector(mulvector(*(PDVECTOR*) &XL[0], *(PDVECTOR*) &YH[0]), *(PDVECTOR*) &YT[0]);\
*(PDVECTOR*) &error[0] = addvector(mulvector(*(PDVECTOR*) &XL[0], *(PDVECTOR*) &YL[0]), *(PDVECTOR*) &XT[0]);\
*(PDVECTOR*) &XT[4] = subvector(mulvector(*(PDVECTOR*) &XH[4], *(PDVECTOR*) &YH[4]), *(PDVECTOR*) &mul[4]);\
*(PDVECTOR*) &YT[4] = addvector(mulvector(*(PDVECTOR*) &XH[4], *(PDVECTOR*) &YL[4]), *(PDVECTOR*) &XT[4]);\
*(PDVECTOR*) &XT[4] = addvector(mulvector(*(PDVECTOR*) &XL[4], *(PDVECTOR*) &YH[4]), *(PDVECTOR*) &YT[4]);\
*(PDVECTOR*) &error[4] = addvector(mulvector(*(PDVECTOR*) &XL[4], *(PDVECTOR*) &YL[4]), *(PDVECTOR*) &XT[4]);
#elif defined(AVX2)
#define splitScallers(X,High,Low,Temp)\
*(PDVECTOR*) &Temp[0] = mulvector(*(PDVECTOR*) &X[0], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[0] = subvector(*(PDVECTOR*) &Temp[0], subvector(*(PDVECTOR*) &Temp[0], *(PDVECTOR*) &X[0]));\
*(PDVECTOR*) &Low[0] = subvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &High[0]);\
*(PDVECTOR*) &Temp[4] = mulvector(*(PDVECTOR*) &X[4], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[4] = subvector(*(PDVECTOR*) &Temp[4], subvector(*(PDVECTOR*) &Temp[4], *(PDVECTOR*) &X[4]));\
*(PDVECTOR*) &Low[4] = subvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &High[4]);
#define twoProd(X,Y,mul,error)\
*(PDVECTOR*) &mul[0] = mulvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &error[0] = fmsubvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0], *(PDVECTOR*) &mul[0]);\
*(PDVECTOR*) &mul[4] = mulvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4]);\
*(PDVECTOR*) &error[4] = fmsubvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4], *(PDVECTOR*) &mul[4]);
#elif defined(AVX512)
#define splitScallers(X,High,Low,Temp)\
*(PDVECTOR*) &Temp[0] = mulvector(*(PDVECTOR*) &X[0], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[0] = subvector(*(PDVECTOR*) &Temp[0], subvector(*(PDVECTOR*) &Temp[0], *(PDVECTOR*) &X[0]));\
*(PDVECTOR*) &Low[0] = subvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &High[0]);
#define twoProd(X,Y,mul,error)\
*(PDVECTOR*) &mul[0] = mulvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &error[0] = fmsubvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0], *(PDVECTOR*) &mul[0]);
#endif

#define extractExponent(X,bin,exponent)\
bin.data = fabs(X);\
exponent = bin.binary >> 52;
#define hybridexponentaccumulate(H,L,index1,index2)\
extractExponent(H,exponent[index1],index[index1])\
index[index2] = index[index1] - 27;\
index[index2] = (index[index2] > 0) ? index[index2] : 0;\
C[index[index1]] += H;\
C[index[index2]] += L;

#define splitScaller(X,High,Low,Temp)\
Temp[0] = X[0] * SPLIT_FACTOR;\
High[0] = Temp[0] - (Temp[0] - X[0]);\
Low[0] = X[0] - High[0];

#if not defined(AVX2) && not defined(AVX512)
#define twoProdScaller(X,Y,mul,error,XH,XL,XT,YH,YL,YT,Temp)\
mul[0] = X[0] * Y[0];\
splitScaller(X,XH,XL,Temp)\
splitScaller(Y,YH,YL,Temp)\
XT[0] = (XH[0] * YH[0]) - mul[0];\
YT[0] = (XH[0] * YL[0]) + XT[0];\
XT[0] = (XL[0] * YH[0]) + YT[0];\
error[0] = (XL[0] * YL[0]) + XT[0];
#else
#define twoProdScaller(X,Y,mul,error)\
mul[0] = X[0] * Y[0];\
error[0] = X[0];\
*(__m128d*) &error[0] = _mm_fmadd_sd(*(__m128d*) &error[0], *(__m128d*) &Y[0], *(__m128d*) &mul[0]);
#endif

#define fastTwoAcc(var,i)\
temp[i] = C[index[i]] + var;\
C[index[i] + 1] += (var - (temp[i] - C[index[i]]));\
C[index[i]] = temp[i];

#define onlineexponentfasttwosum(index1,index2)\
exponent[index1].data = fabs(res1[index1]);\
index[index1] = (exponent[index1].binary >> 52) * 2;\
index[index2] = index[index1] - 106;\
index[index2] = (index[index2] > 0) ? index[index2] : 0;\
fastTwoAcc(res1[index1],index1)\
fastTwoAcc(res2[index1],index2)

#if defined(SSE)
#define TwoSum(X,Y,Xt,Yt)\
*(PDVECTOR*) &Xt[0] = addvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &Yt[0] = subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &X[0]);\
*(PDVECTOR*) &Y[0] = addvector(subvector(*(PDVECTOR*) &X[0], subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &Yt[0])),\
			subvector(*(PDVECTOR*) &Y[0], *(PDVECTOR*) &Yt[0]));\
*(PDVECTOR*) &X[0] = *(PDVECTOR*) &Xt[0];\
*(PDVECTOR*) &Xt[2] = addvector(*(PDVECTOR*) &X[2], *(PDVECTOR*) &Y[2]);\
*(PDVECTOR*) &Yt[2] = subvector(*(PDVECTOR*) &Xt[2], *(PDVECTOR*) &X[2]);\
*(PDVECTOR*) &Y[2] = addvector(subvector(*(PDVECTOR*) &X[2], subvector(*(PDVECTOR*) &Xt[2], *(PDVECTOR*) &Yt[2])),\
			subvector(*(PDVECTOR*) &Y[2], *(PDVECTOR*) &Yt[2]));\
*(PDVECTOR*) &X[2] = *(PDVECTOR*) &Xt[2];\
*(PDVECTOR*) &Xt[4] = addvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4]);\
*(PDVECTOR*) &Yt[4] = subvector(*(PDVECTOR*) &Xt[4], *(PDVECTOR*) &X[4]);\
*(PDVECTOR*) &Y[4] = addvector(subvector(*(PDVECTOR*) &X[4], subvector(*(PDVECTOR*) &Xt[4], *(PDVECTOR*) &Yt[4])),\
			subvector(*(PDVECTOR*) &Y[4], *(PDVECTOR*) &Yt[4]));\
*(PDVECTOR*) &X[4] = *(PDVECTOR*) &Xt[4];\
*(PDVECTOR*) &Xt[6] = addvector(*(PDVECTOR*) &X[6], *(PDVECTOR*) &Y[6]);\
*(PDVECTOR*) &Yt[6] = subvector(*(PDVECTOR*) &Xt[6], *(PDVECTOR*) &X[6]);\
*(PDVECTOR*) &Y[6] = addvector(subvector(*(PDVECTOR*) &X[6], subvector(*(PDVECTOR*) &Xt[6], *(PDVECTOR*) &Yt[6])),\
			subvector(*(PDVECTOR*) &Y[6], *(PDVECTOR*) &Yt[6]));\
*(PDVECTOR*) &X[6] = *(PDVECTOR*) &Xt[6];
#elif defined(AVX) || defined(AVX2)
#define TwoSum(X,Y,Xt,Yt)\
*(PDVECTOR*) &Xt[0] = addvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &Yt[0] = subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &X[0]);\
*(PDVECTOR*) &Y[0] = addvector(subvector(*(PDVECTOR*) &X[0], subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &Yt[0])),\
			subvector(*(PDVECTOR*) &Y[0], *(PDVECTOR*) &Yt[0]));\
*(PDVECTOR*) &X[0] = *(PDVECTOR*) &Xt[0];\
*(PDVECTOR*) &Xt[4] = addvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4]);\
*(PDVECTOR*) &Yt[4] = subvector(*(PDVECTOR*) &Xt[4], *(PDVECTOR*) &X[4]);\
*(PDVECTOR*) &Y[4] = addvector(subvector(*(PDVECTOR*) &X[4], subvector(*(PDVECTOR*) &Xt[4], *(PDVECTOR*) &Yt[4])),\
			subvector(*(PDVECTOR*) &Y[4], *(PDVECTOR*) &Yt[4]));\
*(PDVECTOR*) &X[4] = *(PDVECTOR*) &Xt[4];
#elif defined(AVX512)
#define TwoSum(X,Y,Xt,Yt)\
*(PDVECTOR*) &Xt[0] = addvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &Yt[0] = subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &X[0]);\
*(PDVECTOR*) &Y[0] = addvector(subvector(*(PDVECTOR*) &X[0], subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &Yt[0])),\
			subvector(*(PDVECTOR*) &Y[0], *(PDVECTOR*) &Yt[0]));\
*(PDVECTOR*) &X[0] = *(PDVECTOR*) &Xt[0];
#endif

#define reduceVector(vector,result)\
result = 0;\
for (unsigned int cnt = 0; cnt < PDSIZE; cnt++) result += ((double*) &vector)[cnt];

void factorizationLUBlock(unsigned long n, double* A, unsigned long lda) {
	for (unsigned long j = 0; j < n - 1; j++) {
		if (A[j * lda + j] != 0) {
			cblas_dscal(n - j - 1, 1 / A[j * lda + j], &A[(j + 1) * lda + j], lda);
			cblas_dger(
				CblasRowMajor, n - j - 1, n - j - 1, -1, &A[(j + 1) * lda + j],
				lda, &A[j * lda + j + 1], 1, &A[(j + 1) * lda + j + 1], lda
			);
		}
		else printf("j = %ld\n", j);
	}
}

void factorizationLU(int n, double* A) {
	for (unsigned long jt = 0; jt < n; jt += BLOCK) {
		factorizationLUBlock(BLOCK, &A[jt * n + jt], n);
		cblas_dtrsm(
			CblasRowMajor, CblasRight, CblasUpper, CblasNoTrans, CblasNonUnit, n - BLOCK - jt,
			BLOCK, 1, &A[jt * n + jt], n, &A[(jt + BLOCK) * n + jt], n
		);
		cblas_dtrsm(
			CblasRowMajor, CblasLeft, CblasLower, CblasNoTrans, CblasUnit, BLOCK,
			n - BLOCK - jt, 1, &A[jt * n + jt], n, &A[jt * n + jt + BLOCK], n
		);
		cblas_dgemm(
			CblasRowMajor, CblasNoTrans, CblasNoTrans, n - jt - BLOCK, n - jt - BLOCK, BLOCK, -1, &A[(jt + BLOCK) * n + jt],
			n, &A[jt * n + jt + BLOCK], n, 1, &A[(jt + BLOCK) * n + jt + BLOCK], n
		);
	}
}

void solveSystem(int n, double* A, double* b) {
	factorizationLU(n, A);
	cblas_dtrsv(CblasRowMajor, CblasLower, CblasNoTrans, CblasUnit, n, A, n, b, 1);
	cblas_dtrsv(CblasRowMajor, CblasUpper, CblasNoTrans, CblasNonUnit, n, A, n, b, 1);
}

