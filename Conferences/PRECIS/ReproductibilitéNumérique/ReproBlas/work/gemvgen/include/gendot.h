/*
 *************************************************************************************
 Copyright (c) 2015, University of Perpignan
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************
*/

#ifndef __GENSUM__
#define __GENSUM__
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include "dp_tools.h"
#include "sum.h"

/* ------------------------------------------------------------- */
// Basic utilities
/* ------------------------------------------------------------- */

double rand_double(double M);
double rand_unif(double a, double b);
unsigned int rand_uint(unsigned int M);
/* ----------------------------------------------------------------- */
/* --- Generator of ill-conditionned sums -------------------------- */
/* ----------------------------------------------------------------- */
// Conditionnning oriented : Ogita-Rump-Oishi's
double GenSum(double *x, double *C, unsigned int n, double c);
// comme GenSum mais parametre = exposant du cond 
double GenSum2(double *x, double *C, unsigned int n, double exp_c);
// comme GenSum2 mais optimise pour n grand (>10^6) 
//TODO double GenSumHuge2(double *x, double *C, unsigned int n, double exp_c);

// Exponent range oriented 
// random exponent in [-delta/2, delta/2] but not necessary ill conditionned sums
// x and 1/x are stored 
double GenSumDeltaUnif(double *x, double *C, unsigned int n, int delta);
// NO random exponent in [-delta/2, delta/2]: (n-1) are -delta/2 and 1 is delta/2 
double GenSumDeltaDirac(double *x, double *C, unsigned int n, int delta);
// a la Anderson
double GenSumAndersonDelta(double *x, int *delta_act, double *C, int n, int delta);
// a la GenSum with a larger exponent range
double GenSumDelta(double *x, double *C, unsigned int n, double exp_c, int delta);

/* ------------------------------------------------------------- */
// Utilities
/* ------------------------------------------------------------- */
double **rand_nbs(char * nfich, unsigned int n_voulu);
double **rand_nbs_delta(char * nfich, unsigned int n_voulu);
void free_tab_nbs(double ** tab, unsigned int nb_vect);
int lect_param_fich_data(char *nfich, unsigned int *n, unsigned int *nb_vect, double *cond);
int lect_param_fich_data_delta(char *nfich, unsigned int *n, unsigned int *nb_vect, unsigned int * delta, double *cond);


#endif //__GENSUM__

