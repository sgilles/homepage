#
# *************************************************************************************
# Copyright (c) 2015, University of Perpignan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.
# *************************************************************************************
#


export COMPILER=intel
export LIBRARY=static

export INST_SET_EXT=AVX

export MPCC=mpicc
export MPCXX=mpic++
export MPI_FLAGS=-DMPI_PARALLEL

export DFLAGS=-D$(INST_SET_EXT)

export SRC_DIR=src
export INCLUDE_DIR=include

export KMP_AFFINITY=granularity=thread,scatter
export STATSFLAGS=-DSTATS

export MICCXX=icpc
export MICCFLAGS=-O3 -mmic -fp-model double -funroll-all-loops -Wall
export MICDFLAGS=-DAVX512 -DMIC

export MIC_BUILD_DIR=build/bin/icc/static/mic
export MIC_LIB_DIR=lib/bin/icc/static/mic

ifeq ($(COMPILER),intel)
	export CXX=icpc
	export CFLAGS=-O3 -xHost -fp-model double -fp-model strict -funroll-all-loops -Wall
	export SEQUENTIAL_MKL=-mkl=sequential
	export PARALLEL_MKL=-mkl=parallel
	export OMP_FLAGS=-DOMP_PARALLEL -openmp
	export OMPI_MPICC=icc
	export OMPI_MPICXX=icpc
	export LIBPREFIX=lib/bin/icc/static/
	export STATSLIBPREFIX=lib/stats/icc/static/
	export BUILDPREFIX=build/bin/icc/static/
	export STATSBUILDPREFIX=build/stats/icc/static/
	export FIX=/icc/static/
else
	export CXX=g++
	export CFLAGS=-O3 -march=native -fp-model double -fp-model strict -funroll-all-loops -Wall
	export SEQUENTIAL_MKL=-mkl=sequential
	export PARALLEL_MKL=-mkl=parallel
	export OMP_FLAGS=-DOMP_PARALLEL -openmp
	export OMPI_MPICC=icc
	export OMPI_MPICXX=icpc
	export LIBPREFIX=lib/bin/icc/static/
	export STATSLIBPREFIX=lib/stats/icc/static/
	export BUILDPREFIX=build/bin/icc/static/
	export STATSBUILDPREFIX=build/stats/icc/static/
	export FIX=/icc/static/
endif

export SEQUENTIAL_BUILD_DIR=$(BUILDPREFIX)sequential
export OMP_BUILD_DIR=$(BUILDPREFIX)omp
export MPI_BUILD_DIR=$(BUILDPREFIX)mpi
export HYBRID_BUILD_DIR=$(BUILDPREFIX)hybrid
export SEQUENTIAL_LIB_DIR=$(LIBPREFIX)sequential
export OMP_LIB_DIR=$(LIBPREFIX)omp
export MPI_LIB_DIR=$(LIBPREFIX)mpi
export HYBRID_LIB_DIR=$(LIBPREFIX)hybrid

export SEQUENTIAL_STATSBUILD_DIR=$(STATSBUILDPREFIX)sequential
export OMP_STATSBUILD_DIR=$(STATSBUILDPREFIX)omp
export MPI_STATSBUILD_DIR=$(STATSBUILDPREFIX)mpi
export HYBRID_STATSBUILD_DIR=$(STATSBUILDPREFIX)hybrid
export SEQUENTIAL_STATSLIB_DIR=$(STATSLIBPREFIX)sequential
export OMP_STATSLIB_DIR=$(STATSLIBPREFIX)omp
export MPI_STATSLIB_DIR=$(STATSLIBPREFIX)mpi
export HYBRID_STATSLIB_DIR=$(STATSLIBPREFIX)hybrid

