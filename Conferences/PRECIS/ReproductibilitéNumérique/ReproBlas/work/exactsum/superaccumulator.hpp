#ifndef SUPERACCUMULATOR_HPP_INCLUDED
#define SUPERACCUMULATOR_HPP_INCLUDED

#include <vector>
#include <stdint.h>
#include <iosfwd>
#include "mylibm.hpp"
#include <cassert>
#include <cmath>
#include <cstdio>

struct Superaccumulator
{
    Superaccumulator(int e_bits = 1023, int f_bits = 1023 + 52);
    
    void Accumulate(int64_t x, int exp);
    void Accumulate(double x);
    void Accumulate(Superaccumulator & other);   // May modify (normalize) other member
    double Round();
    
    enum Status
    {
        Exact,
        Inexact,
        MinusInfinity,
        PlusInfinity,
        Overflow,
        sNaN,
        qNaN
    };
    bool Normalize();
    void Dump(std::ostream & os);
private:
    void AccumulateWord(int64_t x, int i);

    static constexpr unsigned int K = 8;    // High-radix carry-save bits
    static constexpr int digits = 64 - K;
    static constexpr double deltaScale = double(1ull << digits); // Assumes K>0


    int f_words, e_words;
    std::vector<int64_t> accumulator;
    int imin, imax;
    Status status;
    
    int64_t overflow_counter;
};


inline void Superaccumulator::AccumulateWord(int64_t x, int i)
{
    // With atomic accumulator updates
    // accumulation and carry propagation can happen in any order,
    // as long as addition is atomic
    // only constraint is: never forget an overflow bit
    //assert(i >= 0 && i < e_words + f_words);
    int64_t carry = x;
    int64_t carrybit;
    unsigned char overflow;
    int64_t oldword = xadd(accumulator[i], x, overflow);
    while(unlikely(overflow))
    {
        // Carry or borrow
        // oldword has sign S
        // x has sign S
        // accumulator[i] has sign !S (just after update)
        // carry has sign !S
        // carrybit has sign S
        carry = (oldword + carry) >> digits;    // Arithmetic shift
        bool s = oldword > 0;
        carrybit = (s ? 1ll << K : -1ll << K);
        
        // Cancel carry-save bits
        xadd(accumulator[i], -(carry << digits), overflow);
        if(TSAFE && unlikely(s ^ overflow)) {
            // (Another) overflow of sign S
            carrybit *= 2;
        }
        
        carry += carrybit;

        ++i;
        if(i >= f_words + e_words) {
            status = Overflow;
            return;
        }
        oldword = xadd(accumulator[i], carry, overflow);
    }
}

inline void Superaccumulator::Accumulate(double x)
{
    if(x == 0) return;
    
    int e = exponent(x);
    int exp_word = e / digits;  // Word containing MSbit (upper bound)
    int iup = exp_word + f_words;
    
    double xscaled = myldexp(x, -digits * exp_word);

    int i;
    for(i = iup; xscaled != 0; --i) {
        double xrounded = myrint(xscaled);
        int64_t xint = myllrint(xscaled);
        AccumulateWord(xint, i);
        
        xscaled -= xrounded;
        xscaled *= deltaScale;
    }
}

#endif
