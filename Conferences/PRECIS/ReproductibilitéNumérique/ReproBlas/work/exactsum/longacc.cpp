/*
 *  Copyright (c) 2014 Inria
 *  All rights reserved.
 */

// Tests for library functions

#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include "superaccumulator.hpp"
#include <mpfr.h>
#include "fpexpansionvect.hpp"
#include <omp.h>
#include <random>
#include <malloc.h>
#include <string.h>
#include <string>

double const freq = 2.890; // GHz

int constexpr e_bits = 1023;    // 319
int constexpr f_bits = 1023 + 52;   // 300

double randDoubleUniform()
{
    // Uniform distribution for now
    return double(rand() - RAND_MAX/4) * 12345.678901234;
}

double randDouble(int emin, int emax, int neg_ratio)
{
    // Uniform mantissa
    double x = double(rand()) / double(RAND_MAX * .99) + 1.;
    // Uniform exponent
    int e = (rand() % (emax - emin)) + emin;
    // Sign
    if(neg_ratio > 1 && rand() % neg_ratio == 0) {
        x = -x;
    }
    return ldexp(x, e);
}

void init_fpuniform(double * a, int n, int range, int emax)
{
    for(int i = 0; i != n; ++i) a[i] = randDouble(emax-range, emax, 2);
}

void init_lognormal(double * a, int n, double mean, double stddev)
{
    std::random_device rd;
    std::default_random_engine gen(rd());
    std::lognormal_distribution<> d(mean, stddev);
    for(int i = 0; i != n; ++i) {
        a[i] = d(gen);
    }
}

double TimeMPFR(double * a, int N)
{
    mpfr_t mpaccum;
    mpfr_init2(mpaccum, 619);
    mpfr_set_zero(mpaccum, 0);
    for(int i = 0; i != N; ++i) {
        mpfr_add_d(mpaccum, mpaccum, a[i], MPFR_RNDN);
    }
    double dacc = mpfr_get_d(mpaccum, MPFR_RNDN);
    mpfr_printf("%Ra\n", mpaccum);
    double result = mpfr_get_d(mpaccum, MPFR_RNDN);
    mpfr_clear(mpaccum);
    printf("mpfr=%a\n", dacc);
	return result;
}

inline static void ReductionStep(int step, int tid1, int tid2, Superaccumulator * acc1, Superaccumulator * acc2,
    int volatile * ready1, int volatile * ready2)
{
    _mm_prefetch((char const*)ready2, _MM_HINT_T0);
    // Wait for thread 2
    while(*ready2 < step) {
        // wait
        _mm_pause();
    }
    acc1->Accumulate(*acc2);
}

inline static void Reduction(unsigned int tid, unsigned int tnum, std::vector<int32_t>& ready,
    std::vector<Superaccumulator>& acc, int const linesize)
{
    // Custom reduction
    for(unsigned int s = 1; (1 << (s-1)) < tnum; ++s) 
    {
        int32_t volatile * c = &ready[tid * linesize];
        ++*c;
        if(tid % (1 << s) == 0) {
            unsigned int tid2 = tid | (1 << (s-1));
            if(tid2 < tnum) {
                //acc[tid2].Prefetch(); // No effect...
                ReductionStep(s, tid, tid2, &acc[tid], &acc[tid2],
                    &ready[tid * linesize], &ready[tid2 * linesize]);
            }
        }
    }
}

template<typename CACHE>
double TimeOMPCache(double * a, int N, unsigned long* time)
{
    int const linesize = 16;    // * sizeof(int32_t)
    int maxthreads = omp_get_max_threads();
    std::vector<Superaccumulator> acc(maxthreads);
    std::vector<int32_t> ready(maxthreads * linesize);
    uint64_t tstart = rdtsc();
#pragma omp parallel
    {
        unsigned int tid = omp_get_thread_num();
        unsigned int tnum = omp_get_num_threads();

        CACHE cache(acc[tid]);
        *(int32_t volatile *)(&ready[tid * linesize]) = 0;
        
        int l = ((tid * int64_t(N)) / tnum) & ~7ul;
        int r = ((((tid+1) * int64_t(N)) / tnum) & ~7ul) - 1;

        for(int i = l; i < r; i+=8) {
            asm ("# myloop");
            cache.Accumulate(Vec4d().load_a(a + i), Vec4d().load_a(a + i + 4));
        }
        cache.Flush();
    	acc[tid].Normalize();
        
        Reduction(tid, tnum, ready, acc, linesize);
    }
    uint64_t tend = rdtsc();
    double dacc = acc[0].Round();
    *time = tend - tstart;
    return dacc;
}

template<typename CACHE>
double TimeOMPCacheBlock(double * a, int N)
{
    // OpenMP sum+reduction
    int const linesize = 16;    // * sizeof(int32_t)
    int maxthreads = omp_get_max_threads();
    std::vector<Superaccumulator> acc(maxthreads);
    std::vector<int32_t> ready(maxthreads * linesize);
    uint64_t tstart = rdtsc();
    
#pragma omp parallel
    {
        unsigned int tid = omp_get_thread_num();
        unsigned int tnum = omp_get_num_threads();

        CACHE cache(acc[tid]);
        *(int32_t volatile *)(&ready[tid * linesize]) = 0;  // Race here, who cares?
        
        int l = ((tid * int64_t(N)) / tnum) & ~7ul;
        int r = ((((tid+1) * int64_t(N)) / tnum) & ~7ul) - 1;

        cache.Accumulate(a + l, r - l);
        cache.Flush();
    	acc[tid].Normalize();
        
        Reduction(tid, tnum, ready, acc, linesize);
    }
    uint64_t tend = rdtsc();
    double dacc = acc[0].Round();
    double t = double(tend - tstart)/N;
    return t;
}

#if 1
#define TIME_FUNC(func) \
    mincachecycles = 2 << 30; \
	meancachecycles = 0;\
	double s;\
    for(int i = 0; i != iterations; ++i) {\
		unsigned long cycles;\
        s = func(a, N, &cycles);\
		meancachecycles += cycles;\
        mincachecycles = (cycles < mincachecycles) ? cycles : mincachecycles; \
    }\
	meancachecycles /= iterations;\
    printf("%d, %.16g, %.16g, %ld, %ld, 0, 0, ", N, s, result, mincachecycles, meancachecycles);
#endif

void initGeneratedSumVector(double* X, char* filename, unsigned long size, double* sum) {
	int r;
	unsigned long fileSize;
	double cond;
	FILE* file;
	file = fopen(filename, "rb");
	if (file == NULL) {
		printf("Can not open file %s\n", filename);
		exit(1);
	}
	r = fread(&fileSize, sizeof(unsigned long), 1, file);
	if (r == 0) {
		printf("Can not read file %s\n", filename);
		exit(1);
	}
	r = fread(X, sizeof(double), fileSize, file);
	if (r == 0) {
		printf("Can not read file %s\n", filename);
		exit(1);
	}
	r = fread(sum, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read file %s\n", filename);
		exit(1);
	}
	r = fread(&cond, sizeof(double), 1, file);  
	if (r == 0) {
		printf("Can not read file %s\n", filename);
		exit(1);
	}
	unsigned long times = size / fileSize;
	*sum *= times;
	for (unsigned long index = fileSize; index < size; index++) {
		unsigned long firstIndex = index % fileSize;
		X[index] = X[firstIndex];
	}
	fclose(file);
}

int main(int argc, char * argv[]) {
    double result;
    int expansSize = atoi(argv[1]);
    int earlyexit = atoi(argv[2]);
    int N = atoi(argv[3]);
    int iterations = atoi(argv[5]);
    double* a = (double*) memalign(64, N * sizeof(double));
	char filename[100] = "";
	if (N > 100000) sprintf(filename, "./datasum/c%ss100000.dat", argv[4]);
	else sprintf(filename, "./datasum/c%ss%d.dat", argv[4], N);
    initGeneratedSumVector(a, filename, N, &result);
	printf("%s, ", argv[6]);
    unsigned long mincachecycles, meancachecycles;
	if (earlyexit) {
		if (expansSize == 2) {
			TIME_FUNC((TimeOMPCache<FPExpansionVect<Vec4d, 2, FPExpansionTraits<true> > >));
		}
		if (expansSize == 3) {
			TIME_FUNC((TimeOMPCache<FPExpansionVect<Vec4d, 3, FPExpansionTraits<true> > >));
		}
		if (expansSize == 4) {
			TIME_FUNC((TimeOMPCache<FPExpansionVect<Vec4d, 4, FPExpansionTraits<true> > >));
		}
		if (expansSize == 5) {
			TIME_FUNC((TimeOMPCache<FPExpansionVect<Vec4d, 5, FPExpansionTraits<true> > >));
		}
		if (expansSize == 6) {
			TIME_FUNC((TimeOMPCache<FPExpansionVect<Vec4d, 6, FPExpansionTraits<true> > >));
		}
		if (expansSize == 7) {
			TIME_FUNC((TimeOMPCache<FPExpansionVect<Vec4d, 7, FPExpansionTraits<true> > >));
		}
		if (expansSize == 8) {
			TIME_FUNC((TimeOMPCache<FPExpansionVect<Vec4d, 8, FPExpansionTraits<true> > >));
		}
	}
	else {
		if (expansSize == 2) {
			TIME_FUNC((TimeOMPCache<FPExpansionVect<Vec4d, 2> >));
		}
		if (expansSize == 3) {
			TIME_FUNC((TimeOMPCache<FPExpansionVect<Vec4d, 3> >));
		}
		if (expansSize == 4) {
			TIME_FUNC((TimeOMPCache<FPExpansionVect<Vec4d, 4> >));
		}
		if (expansSize == 5) {
			TIME_FUNC((TimeOMPCache<FPExpansionVect<Vec4d, 5> >));
		}
		if (expansSize == 6) {
			TIME_FUNC((TimeOMPCache<FPExpansionVect<Vec4d, 6> >));
		}
		if (expansSize == 7) {
			TIME_FUNC((TimeOMPCache<FPExpansionVect<Vec4d, 7> >));
		}
		if (expansSize == 8) {
			TIME_FUNC((TimeOMPCache<FPExpansionVect<Vec4d, 8> >));
		}
	}
	printf("%s\n", argv[7]);
//    TIME_FUNC((TimeOMPCache<FPExpansionVect<Vec4d, 3> >));
//    TIME_FUNC((TimeOMPCache<FPExpansionVect<Vec4d, 4> >));
//    TIME_FUNC((TimeOMPCache<FPExpansionVect<Vec4d, 8, FPExpansionTraits<true> > >));
    return 0;
}

