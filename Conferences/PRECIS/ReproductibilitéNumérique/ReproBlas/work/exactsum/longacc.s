	.file	"longacc.cpp"
	.intel_syntax noprefix
	.text
	.globl	_Z17randDoubleUniformv
	.type	_Z17randDoubleUniformv, @function
_Z17randDoubleUniformv:
.LFB6963:
	.cfi_startproc
	sub	rsp, 8
	.cfi_def_cfa_offset 16
	call	rand
	sub	eax, 536870911
	vxorpd	xmm0, xmm0, xmm0
	vcvtsi2sd	xmm0, xmm0, eax
	vmulsd	xmm0, xmm0, QWORD PTR .LC0[rip]
	add	rsp, 8
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE6963:
	.size	_Z17randDoubleUniformv, .-_Z17randDoubleUniformv
	.globl	_Z10randDoubleiii
	.type	_Z10randDoubleiii, @function
_Z10randDoubleiii:
.LFB6964:
	.cfi_startproc
	push	r13
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	push	r12
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	push	rbp
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	push	rbx
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	sub	rsp, 8
	.cfi_def_cfa_offset 48
	mov	ebp, edi
	mov	r12d, esi
	mov	r13d, edx
	call	rand
	vxorpd	xmm0, xmm0, xmm0
	vcvtsi2sd	xmm0, xmm0, eax
	vdivsd	xmm0, xmm0, QWORD PTR .LC1[rip]
	vaddsd	xmm1, xmm0, QWORD PTR .LC2[rip]
	vmovq	rbx, xmm1
	call	rand
	sub	r12d, ebp
	cdq
	idiv	r12d
	add	ebp, edx
	cmp	r13d, 1
	jle	.L4
	call	rand
	cdq
	idiv	r13d
	test	edx, edx
	jne	.L4
	btc	rbx, 63
.L4:
	mov	edi, ebp
	vmovq	xmm0, rbx
	call	ldexp
	add	rsp, 8
	.cfi_def_cfa_offset 40
	pop	rbx
	.cfi_def_cfa_offset 32
	pop	rbp
	.cfi_def_cfa_offset 24
	pop	r12
	.cfi_def_cfa_offset 16
	pop	r13
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE6964:
	.size	_Z10randDoubleiii, .-_Z10randDoubleiii
	.globl	_Z14init_fpuniformPdiii
	.type	_Z14init_fpuniformPdiii, @function
_Z14init_fpuniformPdiii:
.LFB6965:
	.cfi_startproc
	test	esi, esi
	je	.L12
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 8
	.cfi_def_cfa_offset 64
	mov	r13d, ecx
	mov	r14, rdi
	lea	r12d, [rsi-1]
	add	r12, 1
	mov	ebx, 0
	mov	r15d, ecx
	sub	r15d, edx
.L8:
	movsx	rax, ebx
	lea	rbp, [r14+rax*8]
	mov	edx, 2
	mov	esi, r13d
	mov	edi, r15d
	call	_Z10randDoubleiii
	vmovsd	QWORD PTR [rbp+0], xmm0
	add	rbx, 1
	cmp	rbx, r12
	jne	.L8
	add	rsp, 8
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_restore 3
	.cfi_def_cfa_offset 48
	pop	rbp
	.cfi_restore 6
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_restore 12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_restore 13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_restore 14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_restore 15
	.cfi_def_cfa_offset 8
.L12:
	ret
	.cfi_endproc
.LFE6965:
	.size	_Z14init_fpuniformPdiii, .-_Z14init_fpuniformPdiii
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"%Ra\n"
.LC5:
	.string	"mpfr=%a\n"
	.text
	.globl	_Z8TimeMPFRPdi
	.type	_Z8TimeMPFRPdi, @function
_Z8TimeMPFRPdi:
.LFB6967:
	.cfi_startproc
	push	r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	push	rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	push	rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	sub	rsp, 48
	.cfi_def_cfa_offset 80
	mov	r12, rdi
	mov	ebp, esi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+40], rax
	xor	eax, eax
	mov	esi, 619
	mov	rdi, rsp
	call	mpfr_init2
	mov	esi, 0
	mov	rdi, rsp
	call	mpfr_set_zero
	test	ebp, ebp
	je	.L14
	sub	ebp, 1
	add	rbp, 1
	mov	ebx, 0
.L15:
	movsx	rax, ebx
	vmovsd	xmm0, QWORD PTR [r12+rax*8]
	mov	edx, 0
	mov	rsi, rsp
	mov	rdi, rsp
	call	mpfr_add_d
	add	rbx, 1
	cmp	rbx, rbp
	jne	.L15
.L14:
	mov	esi, 0
	mov	rdi, rsp
	call	mpfr_get_d
	vmovq	rbp, xmm0
	mov	rsi, rsp
	mov	edi, OFFSET FLAT:.LC4
	mov	eax, 0
	call	mpfr_printf
	mov	esi, 0
	mov	rdi, rsp
	call	mpfr_get_d
	vmovq	rbx, xmm0
	mov	rdi, rsp
	call	mpfr_clear
	vmovq	xmm0, rbp
	mov	esi, OFFSET FLAT:.LC5
	mov	edi, 1
	mov	eax, 1
	call	__printf_chk
	vmovq	xmm0, rbx
	mov	rax, QWORD PTR [rsp+40]
	xor	rax, QWORD PTR fs:40
	je	.L16
	call	__stack_chk_fail
.L16:
	add	rsp, 48
	.cfi_def_cfa_offset 32
	pop	rbx
	.cfi_def_cfa_offset 24
	pop	rbp
	.cfi_def_cfa_offset 16
	pop	r12
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE6967:
	.size	_Z8TimeMPFRPdi, .-_Z8TimeMPFRPdi
	.section	.rodata.str1.1
.LC6:
	.string	"rb"
.LC7:
	.string	"Can not open file %s\n"
.LC8:
	.string	"Can not read file %s\n"
	.text
	.globl	_Z22initGeneratedSumVectorPdPcmS_
	.type	_Z22initGeneratedSumVectorPdPcmS_, @function
_Z22initGeneratedSumVectorPdPcmS_:
.LFB6972:
	.cfi_startproc
	push	r14
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	push	r13
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
	push	r12
	.cfi_def_cfa_offset 32
	.cfi_offset 12, -32
	push	rbp
	.cfi_def_cfa_offset 40
	.cfi_offset 6, -40
	push	rbx
	.cfi_def_cfa_offset 48
	.cfi_offset 3, -48
	sub	rsp, 32
	.cfi_def_cfa_offset 80
	mov	rbx, rdi
	mov	r14, rsi
	mov	rbp, rdx
	mov	r13, rcx
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+24], rax
	xor	eax, eax
	mov	esi, OFFSET FLAT:.LC6
	mov	rdi, r14
	call	fopen
	mov	r12, rax
	test	rax, rax
	jne	.L20
	mov	rdx, r14
	mov	esi, OFFSET FLAT:.LC7
	mov	edi, 1
	mov	eax, 0
	call	__printf_chk
	mov	edi, 1
	call	exit
.L20:
	mov	rcx, rax
	mov	edx, 1
	mov	esi, 8
	lea	rdi, [rsp+8]
	call	fread
	test	eax, eax
	jne	.L21
	mov	rdx, r14
	mov	esi, OFFSET FLAT:.LC8
	mov	edi, 1
	call	__printf_chk
	mov	edi, 1
	call	exit
.L21:
	mov	rcx, r12
	mov	rdx, QWORD PTR [rsp+8]
	mov	esi, 8
	mov	rdi, rbx
	call	fread
	test	eax, eax
	jne	.L22
	mov	rdx, r14
	mov	esi, OFFSET FLAT:.LC8
	mov	edi, 1
	call	__printf_chk
	mov	edi, 1
	call	exit
.L22:
	mov	rcx, r12
	mov	edx, 1
	mov	esi, 8
	mov	rdi, r13
	call	fread
	test	eax, eax
	jne	.L23
	mov	rdx, r14
	mov	esi, OFFSET FLAT:.LC8
	mov	edi, 1
	call	__printf_chk
	mov	edi, 1
	call	exit
.L23:
	mov	rcx, r12
	mov	edx, 1
	mov	esi, 8
	lea	rdi, [rsp+16]
	call	fread
	test	eax, eax
	jne	.L24
	mov	rdx, r14
	mov	esi, OFFSET FLAT:.LC8
	mov	edi, 1
	call	__printf_chk
	mov	edi, 1
	call	exit
.L24:
	mov	r8, QWORD PTR [rsp+8]
	mov	rax, rbp
	mov	edx, 0
	div	r8
	test	rax, rax
	js	.L25
	vxorpd	xmm0, xmm0, xmm0
	vcvtsi2sdq	xmm0, xmm0, rax
	jmp	.L26
.L25:
	mov	rdx, rax
	shr	rdx
	and	eax, 1
	or	rdx, rax
	vxorpd	xmm0, xmm0, xmm0
	vcvtsi2sdq	xmm0, xmm0, rdx
	vaddsd	xmm0, xmm0, xmm0
.L26:
	vmulsd	xmm0, xmm0, QWORD PTR [r13+0]
	vmovsd	QWORD PTR [r13+0], xmm0
	cmp	r8, rbp
	jae	.L27
	mov	rsi, r8
.L28:
	mov	rax, rsi
	mov	edx, 0
	div	r8
	vmovsd	xmm0, QWORD PTR [rbx+rdx*8]
	vmovsd	QWORD PTR [rbx+rsi*8], xmm0
	add	rsi, 1
	cmp	rsi, rbp
	jne	.L28
.L27:
	mov	rdi, r12
	call	fclose
	mov	rax, QWORD PTR [rsp+24]
	xor	rax, QWORD PTR fs:40
	je	.L29
	call	__stack_chk_fail
.L29:
	add	rsp, 32
	.cfi_def_cfa_offset 48
	pop	rbx
	.cfi_def_cfa_offset 40
	pop	rbp
	.cfi_def_cfa_offset 32
	pop	r12
	.cfi_def_cfa_offset 24
	pop	r13
	.cfi_def_cfa_offset 16
	pop	r14
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE6972:
	.size	_Z22initGeneratedSumVectorPdPcmS_, .-_Z22initGeneratedSumVectorPdPcmS_
	.section	.text._ZNSt6vectorI16SuperaccumulatorSaIS0_EED2Ev,"axG",@progbits,_ZNSt6vectorI16SuperaccumulatorSaIS0_EED5Ev,comdat
	.align 2
	.weak	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED2Ev
	.type	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED2Ev, @function
_ZNSt6vectorI16SuperaccumulatorSaIS0_EED2Ev:
.LFB7187:
	.cfi_startproc
	push	r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	push	rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	push	rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	mov	r12, rdi
	mov	rbp, QWORD PTR [rdi+8]
	mov	rbx, QWORD PTR [rdi]
	cmp	rbp, rbx
	je	.L33
.L37:
	mov	rdi, QWORD PTR [rbx+8]
	test	rdi, rdi
	je	.L34
	call	_ZdlPv
.L34:
	add	rbx, 56
	cmp	rbp, rbx
	jne	.L37
.L33:
	mov	rdi, QWORD PTR [r12]
	test	rdi, rdi
	je	.L32
	call	_ZdlPv
.L32:
	pop	rbx
	.cfi_def_cfa_offset 24
	pop	rbp
	.cfi_def_cfa_offset 16
	pop	r12
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE7187:
	.size	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED2Ev, .-_ZNSt6vectorI16SuperaccumulatorSaIS0_EED2Ev
	.weak	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED1Ev
	.set	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED1Ev,_ZNSt6vectorI16SuperaccumulatorSaIS0_EED2Ev
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.weak	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm:
.LFB7085:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7085
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 136
	.cfi_def_cfa_offset 192
	mov	QWORD PTR [rsp+8], rdi
	mov	r14d, esi
	mov	r15, rdx
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+120], rax
	xor	eax, eax
	call	omp_get_max_threads
	mov	r12d, eax
	movsx	rbp, eax
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+24], 0
	mov	QWORD PTR [rsp+32], 0
	test	rbp, rbp
	je	.L41
	movabs	rax, 329406144173384850
	cmp	rbp, rax
	jbe	.L42
.LEHB0:
	call	_ZSt17__throw_bad_allocv
.L42:
	lea	rax, [0+rbp*8]
	mov	rbx, rbp
	sal	rbx, 6
	sub	rbx, rax
	mov	rdi, rbx
	call	_Znwm
.LEHE0:
	mov	r13, rax
	mov	QWORD PTR [rsp+16], rax
	mov	QWORD PTR [rsp+24], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+32], rbx
	mov	rbx, rax
.L44:
	test	rbx, rbx
	je	.L43
	mov	edx, 1075
	mov	esi, 1023
	mov	rdi, rbx
.LEHB1:
	call	_ZN16SuperaccumulatorC1Eii
.LEHE1:
.L43:
	add	rbx, 56
	sub	rbp, 1
	jne	.L44
	jmp	.L77
.L69:
	mov	rbx, rax
	call	__cxa_end_catch
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	jne	.L47
	jmp	.L48
.L70:
	mov	rdi, rax
	call	__cxa_begin_catch
	cmp	rbx, r13
	je	.L50
.L72:
	mov	rdi, QWORD PTR [r13+8]
	test	rdi, rdi
	je	.L51
	call	_ZdlPv
.L51:
	add	r13, 56
	cmp	rbx, r13
	jne	.L72
.L50:
.LEHB2:
	call	__cxa_rethrow
.LEHE2:
.L77:
	mov	rax, QWORD PTR [rsp+32]
	mov	QWORD PTR [rsp+24], rax
	sal	r12d, 4
	movsx	r12, r12d
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+56], 0
	mov	QWORD PTR [rsp+64], 0
	test	r12, r12
	jne	.L53
	jmp	.L54
.L47:
	call	_ZdlPv
.L48:
	mov	rdi, rbx
.LEHB3:
	call	_Unwind_Resume
.LEHE3:
.L53:
	movabs	rax, 4611686018427387903
	cmp	r12, rax
	jbe	.L55
.LEHB4:
	call	_ZSt17__throw_bad_allocv
.L55:
	lea	rbx, [0+r12*4]
	mov	rdi, rbx
	call	_Znwm
.LEHE4:
	mov	QWORD PTR [rsp+48], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+64], rbx
	mov	edx, 0
.L56:
	mov	DWORD PTR [rax+rdx*4], 0
	add	rdx, 1
	cmp	r12, rdx
	jne	.L56
.L65:
	mov	rax, QWORD PTR [rsp+64]
	mov	QWORD PTR [rsp+56], rax
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbp, rdx
	mov	rax, QWORD PTR [rsp+8]
	mov	QWORD PTR [rsp+80], rax
	lea	rax, [rsp+16]
	mov	QWORD PTR [rsp+88], rax
	lea	rax, [rsp+48]
	mov	QWORD PTR [rsp+96], rax
	mov	DWORD PTR [rsp+104], r14d
	mov	ecx, 0
	mov	edx, 0
	lea	rsi, [rsp+80]
	mov	edi, OFFSET FLAT:_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.13
	call	GOMP_parallel
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbx, rdx
	mov	rdi, QWORD PTR [rsp+16]
.LEHB5:
	call	_ZN16Superaccumulator5RoundEv
.LEHE5:
	vmovq	r12, xmm0
	sub	rbx, rbp
	mov	QWORD PTR [r15], rbx
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L57
	call	_ZdlPv
.L57:
	mov	rbp, QWORD PTR [rsp+24]
	mov	rbx, QWORD PTR [rsp+16]
	cmp	rbp, rbx
	je	.L58
.L71:
	mov	rdi, QWORD PTR [rbx+8]
	test	rdi, rdi
	je	.L59
	call	_ZdlPv
.L59:
	add	rbx, 56
	cmp	rbp, rbx
	jne	.L71
.L58:
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	je	.L61
	call	_ZdlPv
.L61:
	vmovq	xmm0, r12
	mov	rax, QWORD PTR [rsp+120]
	xor	rax, QWORD PTR fs:40
	je	.L66
	jmp	.L78
.L68:
	mov	rbx, rax
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L64
	call	_ZdlPv
	jmp	.L64
.L67:
	mov	rbx, rax
.L64:
	lea	rdi, [rsp+16]
	call	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED1Ev
	mov	rdi, rbx
.LEHB6:
	call	_Unwind_Resume
.LEHE6:
.L41:
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+32], 0
	mov	QWORD PTR [rsp+24], 0
.L54:
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+64], 0
	jmp	.L65
.L78:
	call	__stack_chk_fail
.L66:
	add	rsp, 136
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_def_cfa_offset 48
	pop	rbp
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE7085:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"aG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.align 4
.LLSDA7085:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT7085-.LLSDATTD7085
.LLSDATTD7085:
	.byte	0x1
	.uleb128 .LLSDACSE7085-.LLSDACSB7085
.LLSDACSB7085:
	.uleb128 .LEHB0-.LFB7085
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB7085
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L70-.LFB7085
	.uleb128 0x1
	.uleb128 .LEHB2-.LFB7085
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L69-.LFB7085
	.uleb128 0
	.uleb128 .LEHB3-.LFB7085
	.uleb128 .LEHE3-.LEHB3
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB4-.LFB7085
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L67-.LFB7085
	.uleb128 0
	.uleb128 .LEHB5-.LFB7085
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L68-.LFB7085
	.uleb128 0
	.uleb128 .LEHB6-.LFB7085
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
.LLSDACSE7085:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT7085:
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.weak	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm:
.LFB7084:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7084
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 136
	.cfi_def_cfa_offset 192
	mov	QWORD PTR [rsp+8], rdi
	mov	r14d, esi
	mov	r15, rdx
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+120], rax
	xor	eax, eax
	call	omp_get_max_threads
	mov	r12d, eax
	movsx	rbp, eax
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+24], 0
	mov	QWORD PTR [rsp+32], 0
	test	rbp, rbp
	je	.L81
	movabs	rax, 329406144173384850
	cmp	rbp, rax
	jbe	.L82
.LEHB7:
	call	_ZSt17__throw_bad_allocv
.L82:
	lea	rax, [0+rbp*8]
	mov	rbx, rbp
	sal	rbx, 6
	sub	rbx, rax
	mov	rdi, rbx
	call	_Znwm
.LEHE7:
	mov	r13, rax
	mov	QWORD PTR [rsp+16], rax
	mov	QWORD PTR [rsp+24], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+32], rbx
	mov	rbx, rax
.L84:
	test	rbx, rbx
	je	.L83
	mov	edx, 1075
	mov	esi, 1023
	mov	rdi, rbx
.LEHB8:
	call	_ZN16SuperaccumulatorC1Eii
.LEHE8:
.L83:
	add	rbx, 56
	sub	rbp, 1
	jne	.L84
	jmp	.L117
.L109:
	mov	rbx, rax
	call	__cxa_end_catch
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	jne	.L87
	jmp	.L88
.L110:
	mov	rdi, rax
	call	__cxa_begin_catch
	cmp	rbx, r13
	je	.L90
.L112:
	mov	rdi, QWORD PTR [r13+8]
	test	rdi, rdi
	je	.L91
	call	_ZdlPv
.L91:
	add	r13, 56
	cmp	rbx, r13
	jne	.L112
.L90:
.LEHB9:
	call	__cxa_rethrow
.LEHE9:
.L117:
	mov	rax, QWORD PTR [rsp+32]
	mov	QWORD PTR [rsp+24], rax
	sal	r12d, 4
	movsx	r12, r12d
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+56], 0
	mov	QWORD PTR [rsp+64], 0
	test	r12, r12
	jne	.L93
	jmp	.L94
.L87:
	call	_ZdlPv
.L88:
	mov	rdi, rbx
.LEHB10:
	call	_Unwind_Resume
.LEHE10:
.L93:
	movabs	rax, 4611686018427387903
	cmp	r12, rax
	jbe	.L95
.LEHB11:
	call	_ZSt17__throw_bad_allocv
.L95:
	lea	rbx, [0+r12*4]
	mov	rdi, rbx
	call	_Znwm
.LEHE11:
	mov	QWORD PTR [rsp+48], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+64], rbx
	mov	edx, 0
.L96:
	mov	DWORD PTR [rax+rdx*4], 0
	add	rdx, 1
	cmp	r12, rdx
	jne	.L96
.L105:
	mov	rax, QWORD PTR [rsp+64]
	mov	QWORD PTR [rsp+56], rax
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbp, rdx
	mov	rax, QWORD PTR [rsp+8]
	mov	QWORD PTR [rsp+80], rax
	lea	rax, [rsp+16]
	mov	QWORD PTR [rsp+88], rax
	lea	rax, [rsp+48]
	mov	QWORD PTR [rsp+96], rax
	mov	DWORD PTR [rsp+104], r14d
	mov	ecx, 0
	mov	edx, 0
	lea	rsi, [rsp+80]
	mov	edi, OFFSET FLAT:_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.12
	call	GOMP_parallel
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbx, rdx
	mov	rdi, QWORD PTR [rsp+16]
.LEHB12:
	call	_ZN16Superaccumulator5RoundEv
.LEHE12:
	vmovq	r12, xmm0
	sub	rbx, rbp
	mov	QWORD PTR [r15], rbx
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L97
	call	_ZdlPv
.L97:
	mov	rbp, QWORD PTR [rsp+24]
	mov	rbx, QWORD PTR [rsp+16]
	cmp	rbp, rbx
	je	.L98
.L111:
	mov	rdi, QWORD PTR [rbx+8]
	test	rdi, rdi
	je	.L99
	call	_ZdlPv
.L99:
	add	rbx, 56
	cmp	rbp, rbx
	jne	.L111
.L98:
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	je	.L101
	call	_ZdlPv
.L101:
	vmovq	xmm0, r12
	mov	rax, QWORD PTR [rsp+120]
	xor	rax, QWORD PTR fs:40
	je	.L106
	jmp	.L118
.L108:
	mov	rbx, rax
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L104
	call	_ZdlPv
	jmp	.L104
.L107:
	mov	rbx, rax
.L104:
	lea	rdi, [rsp+16]
	call	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED1Ev
	mov	rdi, rbx
.LEHB13:
	call	_Unwind_Resume
.LEHE13:
.L81:
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+32], 0
	mov	QWORD PTR [rsp+24], 0
.L94:
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+64], 0
	jmp	.L105
.L118:
	call	__stack_chk_fail
.L106:
	add	rsp, 136
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_def_cfa_offset 48
	pop	rbp
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE7084:
	.section	.gcc_except_table._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"aG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.align 4
.LLSDA7084:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT7084-.LLSDATTD7084
.LLSDATTD7084:
	.byte	0x1
	.uleb128 .LLSDACSE7084-.LLSDACSB7084
.LLSDACSB7084:
	.uleb128 .LEHB7-.LFB7084
	.uleb128 .LEHE7-.LEHB7
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB8-.LFB7084
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L110-.LFB7084
	.uleb128 0x1
	.uleb128 .LEHB9-.LFB7084
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L109-.LFB7084
	.uleb128 0
	.uleb128 .LEHB10-.LFB7084
	.uleb128 .LEHE10-.LEHB10
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB11-.LFB7084
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L107-.LFB7084
	.uleb128 0
	.uleb128 .LEHB12-.LFB7084
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L108-.LFB7084
	.uleb128 0
	.uleb128 .LEHB13-.LFB7084
	.uleb128 .LEHE13-.LEHB13
	.uleb128 0
	.uleb128 0
.LLSDACSE7084:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT7084:
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.weak	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm:
.LFB7083:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7083
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 136
	.cfi_def_cfa_offset 192
	mov	QWORD PTR [rsp+8], rdi
	mov	r14d, esi
	mov	r15, rdx
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+120], rax
	xor	eax, eax
	call	omp_get_max_threads
	mov	r12d, eax
	movsx	rbp, eax
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+24], 0
	mov	QWORD PTR [rsp+32], 0
	test	rbp, rbp
	je	.L121
	movabs	rax, 329406144173384850
	cmp	rbp, rax
	jbe	.L122
.LEHB14:
	call	_ZSt17__throw_bad_allocv
.L122:
	lea	rax, [0+rbp*8]
	mov	rbx, rbp
	sal	rbx, 6
	sub	rbx, rax
	mov	rdi, rbx
	call	_Znwm
.LEHE14:
	mov	r13, rax
	mov	QWORD PTR [rsp+16], rax
	mov	QWORD PTR [rsp+24], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+32], rbx
	mov	rbx, rax
.L124:
	test	rbx, rbx
	je	.L123
	mov	edx, 1075
	mov	esi, 1023
	mov	rdi, rbx
.LEHB15:
	call	_ZN16SuperaccumulatorC1Eii
.LEHE15:
.L123:
	add	rbx, 56
	sub	rbp, 1
	jne	.L124
	jmp	.L157
.L149:
	mov	rbx, rax
	call	__cxa_end_catch
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	jne	.L127
	jmp	.L128
.L150:
	mov	rdi, rax
	call	__cxa_begin_catch
	cmp	rbx, r13
	je	.L130
.L152:
	mov	rdi, QWORD PTR [r13+8]
	test	rdi, rdi
	je	.L131
	call	_ZdlPv
.L131:
	add	r13, 56
	cmp	rbx, r13
	jne	.L152
.L130:
.LEHB16:
	call	__cxa_rethrow
.LEHE16:
.L157:
	mov	rax, QWORD PTR [rsp+32]
	mov	QWORD PTR [rsp+24], rax
	sal	r12d, 4
	movsx	r12, r12d
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+56], 0
	mov	QWORD PTR [rsp+64], 0
	test	r12, r12
	jne	.L133
	jmp	.L134
.L127:
	call	_ZdlPv
.L128:
	mov	rdi, rbx
.LEHB17:
	call	_Unwind_Resume
.LEHE17:
.L133:
	movabs	rax, 4611686018427387903
	cmp	r12, rax
	jbe	.L135
.LEHB18:
	call	_ZSt17__throw_bad_allocv
.L135:
	lea	rbx, [0+r12*4]
	mov	rdi, rbx
	call	_Znwm
.LEHE18:
	mov	QWORD PTR [rsp+48], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+64], rbx
	mov	edx, 0
.L136:
	mov	DWORD PTR [rax+rdx*4], 0
	add	rdx, 1
	cmp	r12, rdx
	jne	.L136
.L145:
	mov	rax, QWORD PTR [rsp+64]
	mov	QWORD PTR [rsp+56], rax
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbp, rdx
	mov	rax, QWORD PTR [rsp+8]
	mov	QWORD PTR [rsp+80], rax
	lea	rax, [rsp+16]
	mov	QWORD PTR [rsp+88], rax
	lea	rax, [rsp+48]
	mov	QWORD PTR [rsp+96], rax
	mov	DWORD PTR [rsp+104], r14d
	mov	ecx, 0
	mov	edx, 0
	lea	rsi, [rsp+80]
	mov	edi, OFFSET FLAT:_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.11
	call	GOMP_parallel
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbx, rdx
	mov	rdi, QWORD PTR [rsp+16]
.LEHB19:
	call	_ZN16Superaccumulator5RoundEv
.LEHE19:
	vmovq	r12, xmm0
	sub	rbx, rbp
	mov	QWORD PTR [r15], rbx
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L137
	call	_ZdlPv
.L137:
	mov	rbp, QWORD PTR [rsp+24]
	mov	rbx, QWORD PTR [rsp+16]
	cmp	rbp, rbx
	je	.L138
.L151:
	mov	rdi, QWORD PTR [rbx+8]
	test	rdi, rdi
	je	.L139
	call	_ZdlPv
.L139:
	add	rbx, 56
	cmp	rbp, rbx
	jne	.L151
.L138:
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	je	.L141
	call	_ZdlPv
.L141:
	vmovq	xmm0, r12
	mov	rax, QWORD PTR [rsp+120]
	xor	rax, QWORD PTR fs:40
	je	.L146
	jmp	.L158
.L148:
	mov	rbx, rax
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L144
	call	_ZdlPv
	jmp	.L144
.L147:
	mov	rbx, rax
.L144:
	lea	rdi, [rsp+16]
	call	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED1Ev
	mov	rdi, rbx
.LEHB20:
	call	_Unwind_Resume
.LEHE20:
.L121:
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+32], 0
	mov	QWORD PTR [rsp+24], 0
.L134:
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+64], 0
	jmp	.L145
.L158:
	call	__stack_chk_fail
.L146:
	add	rsp, 136
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_def_cfa_offset 48
	pop	rbp
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE7083:
	.section	.gcc_except_table._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"aG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.align 4
.LLSDA7083:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT7083-.LLSDATTD7083
.LLSDATTD7083:
	.byte	0x1
	.uleb128 .LLSDACSE7083-.LLSDACSB7083
.LLSDACSB7083:
	.uleb128 .LEHB14-.LFB7083
	.uleb128 .LEHE14-.LEHB14
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB15-.LFB7083
	.uleb128 .LEHE15-.LEHB15
	.uleb128 .L150-.LFB7083
	.uleb128 0x1
	.uleb128 .LEHB16-.LFB7083
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L149-.LFB7083
	.uleb128 0
	.uleb128 .LEHB17-.LFB7083
	.uleb128 .LEHE17-.LEHB17
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB18-.LFB7083
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L147-.LFB7083
	.uleb128 0
	.uleb128 .LEHB19-.LFB7083
	.uleb128 .LEHE19-.LEHB19
	.uleb128 .L148-.LFB7083
	.uleb128 0
	.uleb128 .LEHB20-.LFB7083
	.uleb128 .LEHE20-.LEHB20
	.uleb128 0
	.uleb128 0
.LLSDACSE7083:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT7083:
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.weak	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm:
.LFB7082:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7082
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 136
	.cfi_def_cfa_offset 192
	mov	QWORD PTR [rsp+8], rdi
	mov	r14d, esi
	mov	r15, rdx
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+120], rax
	xor	eax, eax
	call	omp_get_max_threads
	mov	r12d, eax
	movsx	rbp, eax
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+24], 0
	mov	QWORD PTR [rsp+32], 0
	test	rbp, rbp
	je	.L161
	movabs	rax, 329406144173384850
	cmp	rbp, rax
	jbe	.L162
.LEHB21:
	call	_ZSt17__throw_bad_allocv
.L162:
	lea	rax, [0+rbp*8]
	mov	rbx, rbp
	sal	rbx, 6
	sub	rbx, rax
	mov	rdi, rbx
	call	_Znwm
.LEHE21:
	mov	r13, rax
	mov	QWORD PTR [rsp+16], rax
	mov	QWORD PTR [rsp+24], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+32], rbx
	mov	rbx, rax
.L164:
	test	rbx, rbx
	je	.L163
	mov	edx, 1075
	mov	esi, 1023
	mov	rdi, rbx
.LEHB22:
	call	_ZN16SuperaccumulatorC1Eii
.LEHE22:
.L163:
	add	rbx, 56
	sub	rbp, 1
	jne	.L164
	jmp	.L197
.L189:
	mov	rbx, rax
	call	__cxa_end_catch
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	jne	.L167
	jmp	.L168
.L190:
	mov	rdi, rax
	call	__cxa_begin_catch
	cmp	rbx, r13
	je	.L170
.L192:
	mov	rdi, QWORD PTR [r13+8]
	test	rdi, rdi
	je	.L171
	call	_ZdlPv
.L171:
	add	r13, 56
	cmp	rbx, r13
	jne	.L192
.L170:
.LEHB23:
	call	__cxa_rethrow
.LEHE23:
.L197:
	mov	rax, QWORD PTR [rsp+32]
	mov	QWORD PTR [rsp+24], rax
	sal	r12d, 4
	movsx	r12, r12d
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+56], 0
	mov	QWORD PTR [rsp+64], 0
	test	r12, r12
	jne	.L173
	jmp	.L174
.L167:
	call	_ZdlPv
.L168:
	mov	rdi, rbx
.LEHB24:
	call	_Unwind_Resume
.LEHE24:
.L173:
	movabs	rax, 4611686018427387903
	cmp	r12, rax
	jbe	.L175
.LEHB25:
	call	_ZSt17__throw_bad_allocv
.L175:
	lea	rbx, [0+r12*4]
	mov	rdi, rbx
	call	_Znwm
.LEHE25:
	mov	QWORD PTR [rsp+48], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+64], rbx
	mov	edx, 0
.L176:
	mov	DWORD PTR [rax+rdx*4], 0
	add	rdx, 1
	cmp	r12, rdx
	jne	.L176
.L185:
	mov	rax, QWORD PTR [rsp+64]
	mov	QWORD PTR [rsp+56], rax
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbp, rdx
	mov	rax, QWORD PTR [rsp+8]
	mov	QWORD PTR [rsp+80], rax
	lea	rax, [rsp+16]
	mov	QWORD PTR [rsp+88], rax
	lea	rax, [rsp+48]
	mov	QWORD PTR [rsp+96], rax
	mov	DWORD PTR [rsp+104], r14d
	mov	ecx, 0
	mov	edx, 0
	lea	rsi, [rsp+80]
	mov	edi, OFFSET FLAT:_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.10
	call	GOMP_parallel
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbx, rdx
	mov	rdi, QWORD PTR [rsp+16]
.LEHB26:
	call	_ZN16Superaccumulator5RoundEv
.LEHE26:
	vmovq	r12, xmm0
	sub	rbx, rbp
	mov	QWORD PTR [r15], rbx
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L177
	call	_ZdlPv
.L177:
	mov	rbp, QWORD PTR [rsp+24]
	mov	rbx, QWORD PTR [rsp+16]
	cmp	rbp, rbx
	je	.L178
.L191:
	mov	rdi, QWORD PTR [rbx+8]
	test	rdi, rdi
	je	.L179
	call	_ZdlPv
.L179:
	add	rbx, 56
	cmp	rbp, rbx
	jne	.L191
.L178:
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	je	.L181
	call	_ZdlPv
.L181:
	vmovq	xmm0, r12
	mov	rax, QWORD PTR [rsp+120]
	xor	rax, QWORD PTR fs:40
	je	.L186
	jmp	.L198
.L188:
	mov	rbx, rax
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L184
	call	_ZdlPv
	jmp	.L184
.L187:
	mov	rbx, rax
.L184:
	lea	rdi, [rsp+16]
	call	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED1Ev
	mov	rdi, rbx
.LEHB27:
	call	_Unwind_Resume
.LEHE27:
.L161:
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+32], 0
	mov	QWORD PTR [rsp+24], 0
.L174:
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+64], 0
	jmp	.L185
.L198:
	call	__stack_chk_fail
.L186:
	add	rsp, 136
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_def_cfa_offset 48
	pop	rbp
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE7082:
	.section	.gcc_except_table._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"aG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.align 4
.LLSDA7082:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT7082-.LLSDATTD7082
.LLSDATTD7082:
	.byte	0x1
	.uleb128 .LLSDACSE7082-.LLSDACSB7082
.LLSDACSB7082:
	.uleb128 .LEHB21-.LFB7082
	.uleb128 .LEHE21-.LEHB21
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB22-.LFB7082
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L190-.LFB7082
	.uleb128 0x1
	.uleb128 .LEHB23-.LFB7082
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L189-.LFB7082
	.uleb128 0
	.uleb128 .LEHB24-.LFB7082
	.uleb128 .LEHE24-.LEHB24
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB25-.LFB7082
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L187-.LFB7082
	.uleb128 0
	.uleb128 .LEHB26-.LFB7082
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L188-.LFB7082
	.uleb128 0
	.uleb128 .LEHB27-.LFB7082
	.uleb128 .LEHE27-.LEHB27
	.uleb128 0
	.uleb128 0
.LLSDACSE7082:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT7082:
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.weak	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm:
.LFB7081:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7081
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 136
	.cfi_def_cfa_offset 192
	mov	QWORD PTR [rsp+8], rdi
	mov	r14d, esi
	mov	r15, rdx
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+120], rax
	xor	eax, eax
	call	omp_get_max_threads
	mov	r12d, eax
	movsx	rbp, eax
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+24], 0
	mov	QWORD PTR [rsp+32], 0
	test	rbp, rbp
	je	.L201
	movabs	rax, 329406144173384850
	cmp	rbp, rax
	jbe	.L202
.LEHB28:
	call	_ZSt17__throw_bad_allocv
.L202:
	lea	rax, [0+rbp*8]
	mov	rbx, rbp
	sal	rbx, 6
	sub	rbx, rax
	mov	rdi, rbx
	call	_Znwm
.LEHE28:
	mov	r13, rax
	mov	QWORD PTR [rsp+16], rax
	mov	QWORD PTR [rsp+24], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+32], rbx
	mov	rbx, rax
.L204:
	test	rbx, rbx
	je	.L203
	mov	edx, 1075
	mov	esi, 1023
	mov	rdi, rbx
.LEHB29:
	call	_ZN16SuperaccumulatorC1Eii
.LEHE29:
.L203:
	add	rbx, 56
	sub	rbp, 1
	jne	.L204
	jmp	.L237
.L229:
	mov	rbx, rax
	call	__cxa_end_catch
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	jne	.L207
	jmp	.L208
.L230:
	mov	rdi, rax
	call	__cxa_begin_catch
	cmp	rbx, r13
	je	.L210
.L232:
	mov	rdi, QWORD PTR [r13+8]
	test	rdi, rdi
	je	.L211
	call	_ZdlPv
.L211:
	add	r13, 56
	cmp	rbx, r13
	jne	.L232
.L210:
.LEHB30:
	call	__cxa_rethrow
.LEHE30:
.L237:
	mov	rax, QWORD PTR [rsp+32]
	mov	QWORD PTR [rsp+24], rax
	sal	r12d, 4
	movsx	r12, r12d
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+56], 0
	mov	QWORD PTR [rsp+64], 0
	test	r12, r12
	jne	.L213
	jmp	.L214
.L207:
	call	_ZdlPv
.L208:
	mov	rdi, rbx
.LEHB31:
	call	_Unwind_Resume
.LEHE31:
.L213:
	movabs	rax, 4611686018427387903
	cmp	r12, rax
	jbe	.L215
.LEHB32:
	call	_ZSt17__throw_bad_allocv
.L215:
	lea	rbx, [0+r12*4]
	mov	rdi, rbx
	call	_Znwm
.LEHE32:
	mov	QWORD PTR [rsp+48], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+64], rbx
	mov	edx, 0
.L216:
	mov	DWORD PTR [rax+rdx*4], 0
	add	rdx, 1
	cmp	r12, rdx
	jne	.L216
.L225:
	mov	rax, QWORD PTR [rsp+64]
	mov	QWORD PTR [rsp+56], rax
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbp, rdx
	mov	rax, QWORD PTR [rsp+8]
	mov	QWORD PTR [rsp+80], rax
	lea	rax, [rsp+16]
	mov	QWORD PTR [rsp+88], rax
	lea	rax, [rsp+48]
	mov	QWORD PTR [rsp+96], rax
	mov	DWORD PTR [rsp+104], r14d
	mov	ecx, 0
	mov	edx, 0
	lea	rsi, [rsp+80]
	mov	edi, OFFSET FLAT:_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.9
	call	GOMP_parallel
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbx, rdx
	mov	rdi, QWORD PTR [rsp+16]
.LEHB33:
	call	_ZN16Superaccumulator5RoundEv
.LEHE33:
	vmovq	r12, xmm0
	sub	rbx, rbp
	mov	QWORD PTR [r15], rbx
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L217
	call	_ZdlPv
.L217:
	mov	rbp, QWORD PTR [rsp+24]
	mov	rbx, QWORD PTR [rsp+16]
	cmp	rbp, rbx
	je	.L218
.L231:
	mov	rdi, QWORD PTR [rbx+8]
	test	rdi, rdi
	je	.L219
	call	_ZdlPv
.L219:
	add	rbx, 56
	cmp	rbp, rbx
	jne	.L231
.L218:
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	je	.L221
	call	_ZdlPv
.L221:
	vmovq	xmm0, r12
	mov	rax, QWORD PTR [rsp+120]
	xor	rax, QWORD PTR fs:40
	je	.L226
	jmp	.L238
.L228:
	mov	rbx, rax
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L224
	call	_ZdlPv
	jmp	.L224
.L227:
	mov	rbx, rax
.L224:
	lea	rdi, [rsp+16]
	call	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED1Ev
	mov	rdi, rbx
.LEHB34:
	call	_Unwind_Resume
.LEHE34:
.L201:
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+32], 0
	mov	QWORD PTR [rsp+24], 0
.L214:
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+64], 0
	jmp	.L225
.L238:
	call	__stack_chk_fail
.L226:
	add	rsp, 136
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_def_cfa_offset 48
	pop	rbp
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE7081:
	.section	.gcc_except_table._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"aG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.align 4
.LLSDA7081:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT7081-.LLSDATTD7081
.LLSDATTD7081:
	.byte	0x1
	.uleb128 .LLSDACSE7081-.LLSDACSB7081
.LLSDACSB7081:
	.uleb128 .LEHB28-.LFB7081
	.uleb128 .LEHE28-.LEHB28
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB29-.LFB7081
	.uleb128 .LEHE29-.LEHB29
	.uleb128 .L230-.LFB7081
	.uleb128 0x1
	.uleb128 .LEHB30-.LFB7081
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L229-.LFB7081
	.uleb128 0
	.uleb128 .LEHB31-.LFB7081
	.uleb128 .LEHE31-.LEHB31
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB32-.LFB7081
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L227-.LFB7081
	.uleb128 0
	.uleb128 .LEHB33-.LFB7081
	.uleb128 .LEHE33-.LEHB33
	.uleb128 .L228-.LFB7081
	.uleb128 0
	.uleb128 .LEHB34-.LFB7081
	.uleb128 .LEHE34-.LEHB34
	.uleb128 0
	.uleb128 0
.LLSDACSE7081:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT7081:
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.weak	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm:
.LFB7080:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7080
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 136
	.cfi_def_cfa_offset 192
	mov	QWORD PTR [rsp+8], rdi
	mov	r14d, esi
	mov	r15, rdx
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+120], rax
	xor	eax, eax
	call	omp_get_max_threads
	mov	r12d, eax
	movsx	rbp, eax
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+24], 0
	mov	QWORD PTR [rsp+32], 0
	test	rbp, rbp
	je	.L241
	movabs	rax, 329406144173384850
	cmp	rbp, rax
	jbe	.L242
.LEHB35:
	call	_ZSt17__throw_bad_allocv
.L242:
	lea	rax, [0+rbp*8]
	mov	rbx, rbp
	sal	rbx, 6
	sub	rbx, rax
	mov	rdi, rbx
	call	_Znwm
.LEHE35:
	mov	r13, rax
	mov	QWORD PTR [rsp+16], rax
	mov	QWORD PTR [rsp+24], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+32], rbx
	mov	rbx, rax
.L244:
	test	rbx, rbx
	je	.L243
	mov	edx, 1075
	mov	esi, 1023
	mov	rdi, rbx
.LEHB36:
	call	_ZN16SuperaccumulatorC1Eii
.LEHE36:
.L243:
	add	rbx, 56
	sub	rbp, 1
	jne	.L244
	jmp	.L277
.L269:
	mov	rbx, rax
	call	__cxa_end_catch
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	jne	.L247
	jmp	.L248
.L270:
	mov	rdi, rax
	call	__cxa_begin_catch
	cmp	rbx, r13
	je	.L250
.L272:
	mov	rdi, QWORD PTR [r13+8]
	test	rdi, rdi
	je	.L251
	call	_ZdlPv
.L251:
	add	r13, 56
	cmp	rbx, r13
	jne	.L272
.L250:
.LEHB37:
	call	__cxa_rethrow
.LEHE37:
.L277:
	mov	rax, QWORD PTR [rsp+32]
	mov	QWORD PTR [rsp+24], rax
	sal	r12d, 4
	movsx	r12, r12d
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+56], 0
	mov	QWORD PTR [rsp+64], 0
	test	r12, r12
	jne	.L253
	jmp	.L254
.L247:
	call	_ZdlPv
.L248:
	mov	rdi, rbx
.LEHB38:
	call	_Unwind_Resume
.LEHE38:
.L253:
	movabs	rax, 4611686018427387903
	cmp	r12, rax
	jbe	.L255
.LEHB39:
	call	_ZSt17__throw_bad_allocv
.L255:
	lea	rbx, [0+r12*4]
	mov	rdi, rbx
	call	_Znwm
.LEHE39:
	mov	QWORD PTR [rsp+48], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+64], rbx
	mov	edx, 0
.L256:
	mov	DWORD PTR [rax+rdx*4], 0
	add	rdx, 1
	cmp	r12, rdx
	jne	.L256
.L265:
	mov	rax, QWORD PTR [rsp+64]
	mov	QWORD PTR [rsp+56], rax
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbp, rdx
	mov	rax, QWORD PTR [rsp+8]
	mov	QWORD PTR [rsp+80], rax
	lea	rax, [rsp+16]
	mov	QWORD PTR [rsp+88], rax
	lea	rax, [rsp+48]
	mov	QWORD PTR [rsp+96], rax
	mov	DWORD PTR [rsp+104], r14d
	mov	ecx, 0
	mov	edx, 0
	lea	rsi, [rsp+80]
	mov	edi, OFFSET FLAT:_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.8
	call	GOMP_parallel
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbx, rdx
	mov	rdi, QWORD PTR [rsp+16]
.LEHB40:
	call	_ZN16Superaccumulator5RoundEv
.LEHE40:
	vmovq	r12, xmm0
	sub	rbx, rbp
	mov	QWORD PTR [r15], rbx
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L257
	call	_ZdlPv
.L257:
	mov	rbp, QWORD PTR [rsp+24]
	mov	rbx, QWORD PTR [rsp+16]
	cmp	rbp, rbx
	je	.L258
.L271:
	mov	rdi, QWORD PTR [rbx+8]
	test	rdi, rdi
	je	.L259
	call	_ZdlPv
.L259:
	add	rbx, 56
	cmp	rbp, rbx
	jne	.L271
.L258:
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	je	.L261
	call	_ZdlPv
.L261:
	vmovq	xmm0, r12
	mov	rax, QWORD PTR [rsp+120]
	xor	rax, QWORD PTR fs:40
	je	.L266
	jmp	.L278
.L268:
	mov	rbx, rax
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L264
	call	_ZdlPv
	jmp	.L264
.L267:
	mov	rbx, rax
.L264:
	lea	rdi, [rsp+16]
	call	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED1Ev
	mov	rdi, rbx
.LEHB41:
	call	_Unwind_Resume
.LEHE41:
.L241:
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+32], 0
	mov	QWORD PTR [rsp+24], 0
.L254:
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+64], 0
	jmp	.L265
.L278:
	call	__stack_chk_fail
.L266:
	add	rsp, 136
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_def_cfa_offset 48
	pop	rbp
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE7080:
	.section	.gcc_except_table._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"aG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.align 4
.LLSDA7080:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT7080-.LLSDATTD7080
.LLSDATTD7080:
	.byte	0x1
	.uleb128 .LLSDACSE7080-.LLSDACSB7080
.LLSDACSB7080:
	.uleb128 .LEHB35-.LFB7080
	.uleb128 .LEHE35-.LEHB35
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB36-.LFB7080
	.uleb128 .LEHE36-.LEHB36
	.uleb128 .L270-.LFB7080
	.uleb128 0x1
	.uleb128 .LEHB37-.LFB7080
	.uleb128 .LEHE37-.LEHB37
	.uleb128 .L269-.LFB7080
	.uleb128 0
	.uleb128 .LEHB38-.LFB7080
	.uleb128 .LEHE38-.LEHB38
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB39-.LFB7080
	.uleb128 .LEHE39-.LEHB39
	.uleb128 .L267-.LFB7080
	.uleb128 0
	.uleb128 .LEHB40-.LFB7080
	.uleb128 .LEHE40-.LEHB40
	.uleb128 .L268-.LFB7080
	.uleb128 0
	.uleb128 .LEHB41-.LFB7080
	.uleb128 .LEHE41-.LEHB41
	.uleb128 0
	.uleb128 0
.LLSDACSE7080:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT7080:
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.weak	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm:
.LFB7079:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7079
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 136
	.cfi_def_cfa_offset 192
	mov	QWORD PTR [rsp+8], rdi
	mov	r14d, esi
	mov	r15, rdx
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+120], rax
	xor	eax, eax
	call	omp_get_max_threads
	mov	r12d, eax
	movsx	rbp, eax
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+24], 0
	mov	QWORD PTR [rsp+32], 0
	test	rbp, rbp
	je	.L281
	movabs	rax, 329406144173384850
	cmp	rbp, rax
	jbe	.L282
.LEHB42:
	call	_ZSt17__throw_bad_allocv
.L282:
	lea	rax, [0+rbp*8]
	mov	rbx, rbp
	sal	rbx, 6
	sub	rbx, rax
	mov	rdi, rbx
	call	_Znwm
.LEHE42:
	mov	r13, rax
	mov	QWORD PTR [rsp+16], rax
	mov	QWORD PTR [rsp+24], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+32], rbx
	mov	rbx, rax
.L284:
	test	rbx, rbx
	je	.L283
	mov	edx, 1075
	mov	esi, 1023
	mov	rdi, rbx
.LEHB43:
	call	_ZN16SuperaccumulatorC1Eii
.LEHE43:
.L283:
	add	rbx, 56
	sub	rbp, 1
	jne	.L284
	jmp	.L317
.L309:
	mov	rbx, rax
	call	__cxa_end_catch
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	jne	.L287
	jmp	.L288
.L310:
	mov	rdi, rax
	call	__cxa_begin_catch
	cmp	rbx, r13
	je	.L290
.L312:
	mov	rdi, QWORD PTR [r13+8]
	test	rdi, rdi
	je	.L291
	call	_ZdlPv
.L291:
	add	r13, 56
	cmp	rbx, r13
	jne	.L312
.L290:
.LEHB44:
	call	__cxa_rethrow
.LEHE44:
.L317:
	mov	rax, QWORD PTR [rsp+32]
	mov	QWORD PTR [rsp+24], rax
	sal	r12d, 4
	movsx	r12, r12d
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+56], 0
	mov	QWORD PTR [rsp+64], 0
	test	r12, r12
	jne	.L293
	jmp	.L294
.L287:
	call	_ZdlPv
.L288:
	mov	rdi, rbx
.LEHB45:
	call	_Unwind_Resume
.LEHE45:
.L293:
	movabs	rax, 4611686018427387903
	cmp	r12, rax
	jbe	.L295
.LEHB46:
	call	_ZSt17__throw_bad_allocv
.L295:
	lea	rbx, [0+r12*4]
	mov	rdi, rbx
	call	_Znwm
.LEHE46:
	mov	QWORD PTR [rsp+48], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+64], rbx
	mov	edx, 0
.L296:
	mov	DWORD PTR [rax+rdx*4], 0
	add	rdx, 1
	cmp	r12, rdx
	jne	.L296
.L305:
	mov	rax, QWORD PTR [rsp+64]
	mov	QWORD PTR [rsp+56], rax
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbp, rdx
	mov	rax, QWORD PTR [rsp+8]
	mov	QWORD PTR [rsp+80], rax
	lea	rax, [rsp+16]
	mov	QWORD PTR [rsp+88], rax
	lea	rax, [rsp+48]
	mov	QWORD PTR [rsp+96], rax
	mov	DWORD PTR [rsp+104], r14d
	mov	ecx, 0
	mov	edx, 0
	lea	rsi, [rsp+80]
	mov	edi, OFFSET FLAT:_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.7
	call	GOMP_parallel
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbx, rdx
	mov	rdi, QWORD PTR [rsp+16]
.LEHB47:
	call	_ZN16Superaccumulator5RoundEv
.LEHE47:
	vmovq	r12, xmm0
	sub	rbx, rbp
	mov	QWORD PTR [r15], rbx
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L297
	call	_ZdlPv
.L297:
	mov	rbp, QWORD PTR [rsp+24]
	mov	rbx, QWORD PTR [rsp+16]
	cmp	rbp, rbx
	je	.L298
.L311:
	mov	rdi, QWORD PTR [rbx+8]
	test	rdi, rdi
	je	.L299
	call	_ZdlPv
.L299:
	add	rbx, 56
	cmp	rbp, rbx
	jne	.L311
.L298:
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	je	.L301
	call	_ZdlPv
.L301:
	vmovq	xmm0, r12
	mov	rax, QWORD PTR [rsp+120]
	xor	rax, QWORD PTR fs:40
	je	.L306
	jmp	.L318
.L308:
	mov	rbx, rax
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L304
	call	_ZdlPv
	jmp	.L304
.L307:
	mov	rbx, rax
.L304:
	lea	rdi, [rsp+16]
	call	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED1Ev
	mov	rdi, rbx
.LEHB48:
	call	_Unwind_Resume
.LEHE48:
.L281:
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+32], 0
	mov	QWORD PTR [rsp+24], 0
.L294:
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+64], 0
	jmp	.L305
.L318:
	call	__stack_chk_fail
.L306:
	add	rsp, 136
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_def_cfa_offset 48
	pop	rbp
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE7079:
	.section	.gcc_except_table._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"aG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.align 4
.LLSDA7079:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT7079-.LLSDATTD7079
.LLSDATTD7079:
	.byte	0x1
	.uleb128 .LLSDACSE7079-.LLSDACSB7079
.LLSDACSB7079:
	.uleb128 .LEHB42-.LFB7079
	.uleb128 .LEHE42-.LEHB42
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB43-.LFB7079
	.uleb128 .LEHE43-.LEHB43
	.uleb128 .L310-.LFB7079
	.uleb128 0x1
	.uleb128 .LEHB44-.LFB7079
	.uleb128 .LEHE44-.LEHB44
	.uleb128 .L309-.LFB7079
	.uleb128 0
	.uleb128 .LEHB45-.LFB7079
	.uleb128 .LEHE45-.LEHB45
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB46-.LFB7079
	.uleb128 .LEHE46-.LEHB46
	.uleb128 .L307-.LFB7079
	.uleb128 0
	.uleb128 .LEHB47-.LFB7079
	.uleb128 .LEHE47-.LEHB47
	.uleb128 .L308-.LFB7079
	.uleb128 0
	.uleb128 .LEHB48-.LFB7079
	.uleb128 .LEHE48-.LEHB48
	.uleb128 0
	.uleb128 0
.LLSDACSE7079:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT7079:
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.weak	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm:
.LFB7078:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7078
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 136
	.cfi_def_cfa_offset 192
	mov	QWORD PTR [rsp+8], rdi
	mov	r14d, esi
	mov	r15, rdx
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+120], rax
	xor	eax, eax
	call	omp_get_max_threads
	mov	r12d, eax
	movsx	rbp, eax
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+24], 0
	mov	QWORD PTR [rsp+32], 0
	test	rbp, rbp
	je	.L321
	movabs	rax, 329406144173384850
	cmp	rbp, rax
	jbe	.L322
.LEHB49:
	call	_ZSt17__throw_bad_allocv
.L322:
	lea	rax, [0+rbp*8]
	mov	rbx, rbp
	sal	rbx, 6
	sub	rbx, rax
	mov	rdi, rbx
	call	_Znwm
.LEHE49:
	mov	r13, rax
	mov	QWORD PTR [rsp+16], rax
	mov	QWORD PTR [rsp+24], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+32], rbx
	mov	rbx, rax
.L324:
	test	rbx, rbx
	je	.L323
	mov	edx, 1075
	mov	esi, 1023
	mov	rdi, rbx
.LEHB50:
	call	_ZN16SuperaccumulatorC1Eii
.LEHE50:
.L323:
	add	rbx, 56
	sub	rbp, 1
	jne	.L324
	jmp	.L357
.L349:
	mov	rbx, rax
	call	__cxa_end_catch
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	jne	.L327
	jmp	.L328
.L350:
	mov	rdi, rax
	call	__cxa_begin_catch
	cmp	rbx, r13
	je	.L330
.L352:
	mov	rdi, QWORD PTR [r13+8]
	test	rdi, rdi
	je	.L331
	call	_ZdlPv
.L331:
	add	r13, 56
	cmp	rbx, r13
	jne	.L352
.L330:
.LEHB51:
	call	__cxa_rethrow
.LEHE51:
.L357:
	mov	rax, QWORD PTR [rsp+32]
	mov	QWORD PTR [rsp+24], rax
	sal	r12d, 4
	movsx	r12, r12d
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+56], 0
	mov	QWORD PTR [rsp+64], 0
	test	r12, r12
	jne	.L333
	jmp	.L334
.L327:
	call	_ZdlPv
.L328:
	mov	rdi, rbx
.LEHB52:
	call	_Unwind_Resume
.LEHE52:
.L333:
	movabs	rax, 4611686018427387903
	cmp	r12, rax
	jbe	.L335
.LEHB53:
	call	_ZSt17__throw_bad_allocv
.L335:
	lea	rbx, [0+r12*4]
	mov	rdi, rbx
	call	_Znwm
.LEHE53:
	mov	QWORD PTR [rsp+48], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+64], rbx
	mov	edx, 0
.L336:
	mov	DWORD PTR [rax+rdx*4], 0
	add	rdx, 1
	cmp	r12, rdx
	jne	.L336
.L345:
	mov	rax, QWORD PTR [rsp+64]
	mov	QWORD PTR [rsp+56], rax
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbp, rdx
	mov	rax, QWORD PTR [rsp+8]
	mov	QWORD PTR [rsp+80], rax
	lea	rax, [rsp+16]
	mov	QWORD PTR [rsp+88], rax
	lea	rax, [rsp+48]
	mov	QWORD PTR [rsp+96], rax
	mov	DWORD PTR [rsp+104], r14d
	mov	ecx, 0
	mov	edx, 0
	lea	rsi, [rsp+80]
	mov	edi, OFFSET FLAT:_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.6
	call	GOMP_parallel
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbx, rdx
	mov	rdi, QWORD PTR [rsp+16]
.LEHB54:
	call	_ZN16Superaccumulator5RoundEv
.LEHE54:
	vmovq	r12, xmm0
	sub	rbx, rbp
	mov	QWORD PTR [r15], rbx
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L337
	call	_ZdlPv
.L337:
	mov	rbp, QWORD PTR [rsp+24]
	mov	rbx, QWORD PTR [rsp+16]
	cmp	rbp, rbx
	je	.L338
.L351:
	mov	rdi, QWORD PTR [rbx+8]
	test	rdi, rdi
	je	.L339
	call	_ZdlPv
.L339:
	add	rbx, 56
	cmp	rbp, rbx
	jne	.L351
.L338:
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	je	.L341
	call	_ZdlPv
.L341:
	vmovq	xmm0, r12
	mov	rax, QWORD PTR [rsp+120]
	xor	rax, QWORD PTR fs:40
	je	.L346
	jmp	.L358
.L348:
	mov	rbx, rax
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L344
	call	_ZdlPv
	jmp	.L344
.L347:
	mov	rbx, rax
.L344:
	lea	rdi, [rsp+16]
	call	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED1Ev
	mov	rdi, rbx
.LEHB55:
	call	_Unwind_Resume
.LEHE55:
.L321:
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+32], 0
	mov	QWORD PTR [rsp+24], 0
.L334:
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+64], 0
	jmp	.L345
.L358:
	call	__stack_chk_fail
.L346:
	add	rsp, 136
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_def_cfa_offset 48
	pop	rbp
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE7078:
	.section	.gcc_except_table._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"aG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.align 4
.LLSDA7078:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT7078-.LLSDATTD7078
.LLSDATTD7078:
	.byte	0x1
	.uleb128 .LLSDACSE7078-.LLSDACSB7078
.LLSDACSB7078:
	.uleb128 .LEHB49-.LFB7078
	.uleb128 .LEHE49-.LEHB49
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB50-.LFB7078
	.uleb128 .LEHE50-.LEHB50
	.uleb128 .L350-.LFB7078
	.uleb128 0x1
	.uleb128 .LEHB51-.LFB7078
	.uleb128 .LEHE51-.LEHB51
	.uleb128 .L349-.LFB7078
	.uleb128 0
	.uleb128 .LEHB52-.LFB7078
	.uleb128 .LEHE52-.LEHB52
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB53-.LFB7078
	.uleb128 .LEHE53-.LEHB53
	.uleb128 .L347-.LFB7078
	.uleb128 0
	.uleb128 .LEHB54-.LFB7078
	.uleb128 .LEHE54-.LEHB54
	.uleb128 .L348-.LFB7078
	.uleb128 0
	.uleb128 .LEHB55-.LFB7078
	.uleb128 .LEHE55-.LEHB55
	.uleb128 0
	.uleb128 0
.LLSDACSE7078:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT7078:
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.weak	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm:
.LFB7077:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7077
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 136
	.cfi_def_cfa_offset 192
	mov	QWORD PTR [rsp+8], rdi
	mov	r14d, esi
	mov	r15, rdx
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+120], rax
	xor	eax, eax
	call	omp_get_max_threads
	mov	r12d, eax
	movsx	rbp, eax
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+24], 0
	mov	QWORD PTR [rsp+32], 0
	test	rbp, rbp
	je	.L361
	movabs	rax, 329406144173384850
	cmp	rbp, rax
	jbe	.L362
.LEHB56:
	call	_ZSt17__throw_bad_allocv
.L362:
	lea	rax, [0+rbp*8]
	mov	rbx, rbp
	sal	rbx, 6
	sub	rbx, rax
	mov	rdi, rbx
	call	_Znwm
.LEHE56:
	mov	r13, rax
	mov	QWORD PTR [rsp+16], rax
	mov	QWORD PTR [rsp+24], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+32], rbx
	mov	rbx, rax
.L364:
	test	rbx, rbx
	je	.L363
	mov	edx, 1075
	mov	esi, 1023
	mov	rdi, rbx
.LEHB57:
	call	_ZN16SuperaccumulatorC1Eii
.LEHE57:
.L363:
	add	rbx, 56
	sub	rbp, 1
	jne	.L364
	jmp	.L397
.L389:
	mov	rbx, rax
	call	__cxa_end_catch
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	jne	.L367
	jmp	.L368
.L390:
	mov	rdi, rax
	call	__cxa_begin_catch
	cmp	rbx, r13
	je	.L370
.L392:
	mov	rdi, QWORD PTR [r13+8]
	test	rdi, rdi
	je	.L371
	call	_ZdlPv
.L371:
	add	r13, 56
	cmp	rbx, r13
	jne	.L392
.L370:
.LEHB58:
	call	__cxa_rethrow
.LEHE58:
.L397:
	mov	rax, QWORD PTR [rsp+32]
	mov	QWORD PTR [rsp+24], rax
	sal	r12d, 4
	movsx	r12, r12d
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+56], 0
	mov	QWORD PTR [rsp+64], 0
	test	r12, r12
	jne	.L373
	jmp	.L374
.L367:
	call	_ZdlPv
.L368:
	mov	rdi, rbx
.LEHB59:
	call	_Unwind_Resume
.LEHE59:
.L373:
	movabs	rax, 4611686018427387903
	cmp	r12, rax
	jbe	.L375
.LEHB60:
	call	_ZSt17__throw_bad_allocv
.L375:
	lea	rbx, [0+r12*4]
	mov	rdi, rbx
	call	_Znwm
.LEHE60:
	mov	QWORD PTR [rsp+48], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+64], rbx
	mov	edx, 0
.L376:
	mov	DWORD PTR [rax+rdx*4], 0
	add	rdx, 1
	cmp	r12, rdx
	jne	.L376
.L385:
	mov	rax, QWORD PTR [rsp+64]
	mov	QWORD PTR [rsp+56], rax
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbp, rdx
	mov	rax, QWORD PTR [rsp+8]
	mov	QWORD PTR [rsp+80], rax
	lea	rax, [rsp+16]
	mov	QWORD PTR [rsp+88], rax
	lea	rax, [rsp+48]
	mov	QWORD PTR [rsp+96], rax
	mov	DWORD PTR [rsp+104], r14d
	mov	ecx, 0
	mov	edx, 0
	lea	rsi, [rsp+80]
	mov	edi, OFFSET FLAT:_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.5
	call	GOMP_parallel
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbx, rdx
	mov	rdi, QWORD PTR [rsp+16]
.LEHB61:
	call	_ZN16Superaccumulator5RoundEv
.LEHE61:
	vmovq	r12, xmm0
	sub	rbx, rbp
	mov	QWORD PTR [r15], rbx
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L377
	call	_ZdlPv
.L377:
	mov	rbp, QWORD PTR [rsp+24]
	mov	rbx, QWORD PTR [rsp+16]
	cmp	rbp, rbx
	je	.L378
.L391:
	mov	rdi, QWORD PTR [rbx+8]
	test	rdi, rdi
	je	.L379
	call	_ZdlPv
.L379:
	add	rbx, 56
	cmp	rbp, rbx
	jne	.L391
.L378:
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	je	.L381
	call	_ZdlPv
.L381:
	vmovq	xmm0, r12
	mov	rax, QWORD PTR [rsp+120]
	xor	rax, QWORD PTR fs:40
	je	.L386
	jmp	.L398
.L388:
	mov	rbx, rax
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L384
	call	_ZdlPv
	jmp	.L384
.L387:
	mov	rbx, rax
.L384:
	lea	rdi, [rsp+16]
	call	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED1Ev
	mov	rdi, rbx
.LEHB62:
	call	_Unwind_Resume
.LEHE62:
.L361:
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+32], 0
	mov	QWORD PTR [rsp+24], 0
.L374:
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+64], 0
	jmp	.L385
.L398:
	call	__stack_chk_fail
.L386:
	add	rsp, 136
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_def_cfa_offset 48
	pop	rbp
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE7077:
	.section	.gcc_except_table._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"aG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.align 4
.LLSDA7077:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT7077-.LLSDATTD7077
.LLSDATTD7077:
	.byte	0x1
	.uleb128 .LLSDACSE7077-.LLSDACSB7077
.LLSDACSB7077:
	.uleb128 .LEHB56-.LFB7077
	.uleb128 .LEHE56-.LEHB56
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB57-.LFB7077
	.uleb128 .LEHE57-.LEHB57
	.uleb128 .L390-.LFB7077
	.uleb128 0x1
	.uleb128 .LEHB58-.LFB7077
	.uleb128 .LEHE58-.LEHB58
	.uleb128 .L389-.LFB7077
	.uleb128 0
	.uleb128 .LEHB59-.LFB7077
	.uleb128 .LEHE59-.LEHB59
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB60-.LFB7077
	.uleb128 .LEHE60-.LEHB60
	.uleb128 .L387-.LFB7077
	.uleb128 0
	.uleb128 .LEHB61-.LFB7077
	.uleb128 .LEHE61-.LEHB61
	.uleb128 .L388-.LFB7077
	.uleb128 0
	.uleb128 .LEHB62-.LFB7077
	.uleb128 .LEHE62-.LEHB62
	.uleb128 0
	.uleb128 0
.LLSDACSE7077:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT7077:
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.weak	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm:
.LFB7076:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7076
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 136
	.cfi_def_cfa_offset 192
	mov	QWORD PTR [rsp+8], rdi
	mov	r14d, esi
	mov	r15, rdx
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+120], rax
	xor	eax, eax
	call	omp_get_max_threads
	mov	r12d, eax
	movsx	rbp, eax
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+24], 0
	mov	QWORD PTR [rsp+32], 0
	test	rbp, rbp
	je	.L401
	movabs	rax, 329406144173384850
	cmp	rbp, rax
	jbe	.L402
.LEHB63:
	call	_ZSt17__throw_bad_allocv
.L402:
	lea	rax, [0+rbp*8]
	mov	rbx, rbp
	sal	rbx, 6
	sub	rbx, rax
	mov	rdi, rbx
	call	_Znwm
.LEHE63:
	mov	r13, rax
	mov	QWORD PTR [rsp+16], rax
	mov	QWORD PTR [rsp+24], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+32], rbx
	mov	rbx, rax
.L404:
	test	rbx, rbx
	je	.L403
	mov	edx, 1075
	mov	esi, 1023
	mov	rdi, rbx
.LEHB64:
	call	_ZN16SuperaccumulatorC1Eii
.LEHE64:
.L403:
	add	rbx, 56
	sub	rbp, 1
	jne	.L404
	jmp	.L437
.L429:
	mov	rbx, rax
	call	__cxa_end_catch
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	jne	.L407
	jmp	.L408
.L430:
	mov	rdi, rax
	call	__cxa_begin_catch
	cmp	rbx, r13
	je	.L410
.L432:
	mov	rdi, QWORD PTR [r13+8]
	test	rdi, rdi
	je	.L411
	call	_ZdlPv
.L411:
	add	r13, 56
	cmp	rbx, r13
	jne	.L432
.L410:
.LEHB65:
	call	__cxa_rethrow
.LEHE65:
.L437:
	mov	rax, QWORD PTR [rsp+32]
	mov	QWORD PTR [rsp+24], rax
	sal	r12d, 4
	movsx	r12, r12d
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+56], 0
	mov	QWORD PTR [rsp+64], 0
	test	r12, r12
	jne	.L413
	jmp	.L414
.L407:
	call	_ZdlPv
.L408:
	mov	rdi, rbx
.LEHB66:
	call	_Unwind_Resume
.LEHE66:
.L413:
	movabs	rax, 4611686018427387903
	cmp	r12, rax
	jbe	.L415
.LEHB67:
	call	_ZSt17__throw_bad_allocv
.L415:
	lea	rbx, [0+r12*4]
	mov	rdi, rbx
	call	_Znwm
.LEHE67:
	mov	QWORD PTR [rsp+48], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+64], rbx
	mov	edx, 0
.L416:
	mov	DWORD PTR [rax+rdx*4], 0
	add	rdx, 1
	cmp	r12, rdx
	jne	.L416
.L425:
	mov	rax, QWORD PTR [rsp+64]
	mov	QWORD PTR [rsp+56], rax
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbp, rdx
	mov	rax, QWORD PTR [rsp+8]
	mov	QWORD PTR [rsp+80], rax
	lea	rax, [rsp+16]
	mov	QWORD PTR [rsp+88], rax
	lea	rax, [rsp+48]
	mov	QWORD PTR [rsp+96], rax
	mov	DWORD PTR [rsp+104], r14d
	mov	ecx, 0
	mov	edx, 0
	lea	rsi, [rsp+80]
	mov	edi, OFFSET FLAT:_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.4
	call	GOMP_parallel
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbx, rdx
	mov	rdi, QWORD PTR [rsp+16]
.LEHB68:
	call	_ZN16Superaccumulator5RoundEv
.LEHE68:
	vmovq	r12, xmm0
	sub	rbx, rbp
	mov	QWORD PTR [r15], rbx
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L417
	call	_ZdlPv
.L417:
	mov	rbp, QWORD PTR [rsp+24]
	mov	rbx, QWORD PTR [rsp+16]
	cmp	rbp, rbx
	je	.L418
.L431:
	mov	rdi, QWORD PTR [rbx+8]
	test	rdi, rdi
	je	.L419
	call	_ZdlPv
.L419:
	add	rbx, 56
	cmp	rbp, rbx
	jne	.L431
.L418:
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	je	.L421
	call	_ZdlPv
.L421:
	vmovq	xmm0, r12
	mov	rax, QWORD PTR [rsp+120]
	xor	rax, QWORD PTR fs:40
	je	.L426
	jmp	.L438
.L428:
	mov	rbx, rax
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L424
	call	_ZdlPv
	jmp	.L424
.L427:
	mov	rbx, rax
.L424:
	lea	rdi, [rsp+16]
	call	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED1Ev
	mov	rdi, rbx
.LEHB69:
	call	_Unwind_Resume
.LEHE69:
.L401:
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+32], 0
	mov	QWORD PTR [rsp+24], 0
.L414:
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+64], 0
	jmp	.L425
.L438:
	call	__stack_chk_fail
.L426:
	add	rsp, 136
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_def_cfa_offset 48
	pop	rbp
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE7076:
	.section	.gcc_except_table._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"aG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.align 4
.LLSDA7076:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT7076-.LLSDATTD7076
.LLSDATTD7076:
	.byte	0x1
	.uleb128 .LLSDACSE7076-.LLSDACSB7076
.LLSDACSB7076:
	.uleb128 .LEHB63-.LFB7076
	.uleb128 .LEHE63-.LEHB63
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB64-.LFB7076
	.uleb128 .LEHE64-.LEHB64
	.uleb128 .L430-.LFB7076
	.uleb128 0x1
	.uleb128 .LEHB65-.LFB7076
	.uleb128 .LEHE65-.LEHB65
	.uleb128 .L429-.LFB7076
	.uleb128 0
	.uleb128 .LEHB66-.LFB7076
	.uleb128 .LEHE66-.LEHB66
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB67-.LFB7076
	.uleb128 .LEHE67-.LEHB67
	.uleb128 .L427-.LFB7076
	.uleb128 0
	.uleb128 .LEHB68-.LFB7076
	.uleb128 .LEHE68-.LEHB68
	.uleb128 .L428-.LFB7076
	.uleb128 0
	.uleb128 .LEHB69-.LFB7076
	.uleb128 .LEHE69-.LEHB69
	.uleb128 0
	.uleb128 0
.LLSDACSE7076:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT7076:
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.weak	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm:
.LFB7075:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7075
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 136
	.cfi_def_cfa_offset 192
	mov	QWORD PTR [rsp+8], rdi
	mov	r14d, esi
	mov	r15, rdx
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+120], rax
	xor	eax, eax
	call	omp_get_max_threads
	mov	r12d, eax
	movsx	rbp, eax
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+24], 0
	mov	QWORD PTR [rsp+32], 0
	test	rbp, rbp
	je	.L441
	movabs	rax, 329406144173384850
	cmp	rbp, rax
	jbe	.L442
.LEHB70:
	call	_ZSt17__throw_bad_allocv
.L442:
	lea	rax, [0+rbp*8]
	mov	rbx, rbp
	sal	rbx, 6
	sub	rbx, rax
	mov	rdi, rbx
	call	_Znwm
.LEHE70:
	mov	r13, rax
	mov	QWORD PTR [rsp+16], rax
	mov	QWORD PTR [rsp+24], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+32], rbx
	mov	rbx, rax
.L444:
	test	rbx, rbx
	je	.L443
	mov	edx, 1075
	mov	esi, 1023
	mov	rdi, rbx
.LEHB71:
	call	_ZN16SuperaccumulatorC1Eii
.LEHE71:
.L443:
	add	rbx, 56
	sub	rbp, 1
	jne	.L444
	jmp	.L477
.L469:
	mov	rbx, rax
	call	__cxa_end_catch
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	jne	.L447
	jmp	.L448
.L470:
	mov	rdi, rax
	call	__cxa_begin_catch
	cmp	rbx, r13
	je	.L450
.L472:
	mov	rdi, QWORD PTR [r13+8]
	test	rdi, rdi
	je	.L451
	call	_ZdlPv
.L451:
	add	r13, 56
	cmp	rbx, r13
	jne	.L472
.L450:
.LEHB72:
	call	__cxa_rethrow
.LEHE72:
.L477:
	mov	rax, QWORD PTR [rsp+32]
	mov	QWORD PTR [rsp+24], rax
	sal	r12d, 4
	movsx	r12, r12d
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+56], 0
	mov	QWORD PTR [rsp+64], 0
	test	r12, r12
	jne	.L453
	jmp	.L454
.L447:
	call	_ZdlPv
.L448:
	mov	rdi, rbx
.LEHB73:
	call	_Unwind_Resume
.LEHE73:
.L453:
	movabs	rax, 4611686018427387903
	cmp	r12, rax
	jbe	.L455
.LEHB74:
	call	_ZSt17__throw_bad_allocv
.L455:
	lea	rbx, [0+r12*4]
	mov	rdi, rbx
	call	_Znwm
.LEHE74:
	mov	QWORD PTR [rsp+48], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+64], rbx
	mov	edx, 0
.L456:
	mov	DWORD PTR [rax+rdx*4], 0
	add	rdx, 1
	cmp	r12, rdx
	jne	.L456
.L465:
	mov	rax, QWORD PTR [rsp+64]
	mov	QWORD PTR [rsp+56], rax
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbp, rdx
	mov	rax, QWORD PTR [rsp+8]
	mov	QWORD PTR [rsp+80], rax
	lea	rax, [rsp+16]
	mov	QWORD PTR [rsp+88], rax
	lea	rax, [rsp+48]
	mov	QWORD PTR [rsp+96], rax
	mov	DWORD PTR [rsp+104], r14d
	mov	ecx, 0
	mov	edx, 0
	lea	rsi, [rsp+80]
	mov	edi, OFFSET FLAT:_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.3
	call	GOMP_parallel
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbx, rdx
	mov	rdi, QWORD PTR [rsp+16]
.LEHB75:
	call	_ZN16Superaccumulator5RoundEv
.LEHE75:
	vmovq	r12, xmm0
	sub	rbx, rbp
	mov	QWORD PTR [r15], rbx
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L457
	call	_ZdlPv
.L457:
	mov	rbp, QWORD PTR [rsp+24]
	mov	rbx, QWORD PTR [rsp+16]
	cmp	rbp, rbx
	je	.L458
.L471:
	mov	rdi, QWORD PTR [rbx+8]
	test	rdi, rdi
	je	.L459
	call	_ZdlPv
.L459:
	add	rbx, 56
	cmp	rbp, rbx
	jne	.L471
.L458:
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	je	.L461
	call	_ZdlPv
.L461:
	vmovq	xmm0, r12
	mov	rax, QWORD PTR [rsp+120]
	xor	rax, QWORD PTR fs:40
	je	.L466
	jmp	.L478
.L468:
	mov	rbx, rax
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L464
	call	_ZdlPv
	jmp	.L464
.L467:
	mov	rbx, rax
.L464:
	lea	rdi, [rsp+16]
	call	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED1Ev
	mov	rdi, rbx
.LEHB76:
	call	_Unwind_Resume
.LEHE76:
.L441:
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+32], 0
	mov	QWORD PTR [rsp+24], 0
.L454:
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+64], 0
	jmp	.L465
.L478:
	call	__stack_chk_fail
.L466:
	add	rsp, 136
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_def_cfa_offset 48
	pop	rbp
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE7075:
	.section	.gcc_except_table._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"aG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.align 4
.LLSDA7075:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT7075-.LLSDATTD7075
.LLSDATTD7075:
	.byte	0x1
	.uleb128 .LLSDACSE7075-.LLSDACSB7075
.LLSDACSB7075:
	.uleb128 .LEHB70-.LFB7075
	.uleb128 .LEHE70-.LEHB70
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB71-.LFB7075
	.uleb128 .LEHE71-.LEHB71
	.uleb128 .L470-.LFB7075
	.uleb128 0x1
	.uleb128 .LEHB72-.LFB7075
	.uleb128 .LEHE72-.LEHB72
	.uleb128 .L469-.LFB7075
	.uleb128 0
	.uleb128 .LEHB73-.LFB7075
	.uleb128 .LEHE73-.LEHB73
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB74-.LFB7075
	.uleb128 .LEHE74-.LEHB74
	.uleb128 .L467-.LFB7075
	.uleb128 0
	.uleb128 .LEHB75-.LFB7075
	.uleb128 .LEHE75-.LEHB75
	.uleb128 .L468-.LFB7075
	.uleb128 0
	.uleb128 .LEHB76-.LFB7075
	.uleb128 .LEHE76-.LEHB76
	.uleb128 0
	.uleb128 0
.LLSDACSE7075:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT7075:
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.weak	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm:
.LFB7074:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7074
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 136
	.cfi_def_cfa_offset 192
	mov	QWORD PTR [rsp+8], rdi
	mov	r14d, esi
	mov	r15, rdx
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+120], rax
	xor	eax, eax
	call	omp_get_max_threads
	mov	r12d, eax
	movsx	rbp, eax
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+24], 0
	mov	QWORD PTR [rsp+32], 0
	test	rbp, rbp
	je	.L481
	movabs	rax, 329406144173384850
	cmp	rbp, rax
	jbe	.L482
.LEHB77:
	call	_ZSt17__throw_bad_allocv
.L482:
	lea	rax, [0+rbp*8]
	mov	rbx, rbp
	sal	rbx, 6
	sub	rbx, rax
	mov	rdi, rbx
	call	_Znwm
.LEHE77:
	mov	r13, rax
	mov	QWORD PTR [rsp+16], rax
	mov	QWORD PTR [rsp+24], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+32], rbx
	mov	rbx, rax
.L484:
	test	rbx, rbx
	je	.L483
	mov	edx, 1075
	mov	esi, 1023
	mov	rdi, rbx
.LEHB78:
	call	_ZN16SuperaccumulatorC1Eii
.LEHE78:
.L483:
	add	rbx, 56
	sub	rbp, 1
	jne	.L484
	jmp	.L517
.L509:
	mov	rbx, rax
	call	__cxa_end_catch
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	jne	.L487
	jmp	.L488
.L510:
	mov	rdi, rax
	call	__cxa_begin_catch
	cmp	rbx, r13
	je	.L490
.L512:
	mov	rdi, QWORD PTR [r13+8]
	test	rdi, rdi
	je	.L491
	call	_ZdlPv
.L491:
	add	r13, 56
	cmp	rbx, r13
	jne	.L512
.L490:
.LEHB79:
	call	__cxa_rethrow
.LEHE79:
.L517:
	mov	rax, QWORD PTR [rsp+32]
	mov	QWORD PTR [rsp+24], rax
	sal	r12d, 4
	movsx	r12, r12d
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+56], 0
	mov	QWORD PTR [rsp+64], 0
	test	r12, r12
	jne	.L493
	jmp	.L494
.L487:
	call	_ZdlPv
.L488:
	mov	rdi, rbx
.LEHB80:
	call	_Unwind_Resume
.LEHE80:
.L493:
	movabs	rax, 4611686018427387903
	cmp	r12, rax
	jbe	.L495
.LEHB81:
	call	_ZSt17__throw_bad_allocv
.L495:
	lea	rbx, [0+r12*4]
	mov	rdi, rbx
	call	_Znwm
.LEHE81:
	mov	QWORD PTR [rsp+48], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+64], rbx
	mov	edx, 0
.L496:
	mov	DWORD PTR [rax+rdx*4], 0
	add	rdx, 1
	cmp	r12, rdx
	jne	.L496
.L505:
	mov	rax, QWORD PTR [rsp+64]
	mov	QWORD PTR [rsp+56], rax
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbp, rdx
	mov	rax, QWORD PTR [rsp+8]
	mov	QWORD PTR [rsp+80], rax
	lea	rax, [rsp+16]
	mov	QWORD PTR [rsp+88], rax
	lea	rax, [rsp+48]
	mov	QWORD PTR [rsp+96], rax
	mov	DWORD PTR [rsp+104], r14d
	mov	ecx, 0
	mov	edx, 0
	lea	rsi, [rsp+80]
	mov	edi, OFFSET FLAT:_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.2
	call	GOMP_parallel
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbx, rdx
	mov	rdi, QWORD PTR [rsp+16]
.LEHB82:
	call	_ZN16Superaccumulator5RoundEv
.LEHE82:
	vmovq	r12, xmm0
	sub	rbx, rbp
	mov	QWORD PTR [r15], rbx
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L497
	call	_ZdlPv
.L497:
	mov	rbp, QWORD PTR [rsp+24]
	mov	rbx, QWORD PTR [rsp+16]
	cmp	rbp, rbx
	je	.L498
.L511:
	mov	rdi, QWORD PTR [rbx+8]
	test	rdi, rdi
	je	.L499
	call	_ZdlPv
.L499:
	add	rbx, 56
	cmp	rbp, rbx
	jne	.L511
.L498:
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	je	.L501
	call	_ZdlPv
.L501:
	vmovq	xmm0, r12
	mov	rax, QWORD PTR [rsp+120]
	xor	rax, QWORD PTR fs:40
	je	.L506
	jmp	.L518
.L508:
	mov	rbx, rax
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L504
	call	_ZdlPv
	jmp	.L504
.L507:
	mov	rbx, rax
.L504:
	lea	rdi, [rsp+16]
	call	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED1Ev
	mov	rdi, rbx
.LEHB83:
	call	_Unwind_Resume
.LEHE83:
.L481:
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+32], 0
	mov	QWORD PTR [rsp+24], 0
.L494:
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+64], 0
	jmp	.L505
.L518:
	call	__stack_chk_fail
.L506:
	add	rsp, 136
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_def_cfa_offset 48
	pop	rbp
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE7074:
	.section	.gcc_except_table._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"aG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.align 4
.LLSDA7074:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT7074-.LLSDATTD7074
.LLSDATTD7074:
	.byte	0x1
	.uleb128 .LLSDACSE7074-.LLSDACSB7074
.LLSDACSB7074:
	.uleb128 .LEHB77-.LFB7074
	.uleb128 .LEHE77-.LEHB77
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB78-.LFB7074
	.uleb128 .LEHE78-.LEHB78
	.uleb128 .L510-.LFB7074
	.uleb128 0x1
	.uleb128 .LEHB79-.LFB7074
	.uleb128 .LEHE79-.LEHB79
	.uleb128 .L509-.LFB7074
	.uleb128 0
	.uleb128 .LEHB80-.LFB7074
	.uleb128 .LEHE80-.LEHB80
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB81-.LFB7074
	.uleb128 .LEHE81-.LEHB81
	.uleb128 .L507-.LFB7074
	.uleb128 0
	.uleb128 .LEHB82-.LFB7074
	.uleb128 .LEHE82-.LEHB82
	.uleb128 .L508-.LFB7074
	.uleb128 0
	.uleb128 .LEHB83-.LFB7074
	.uleb128 .LEHE83-.LEHB83
	.uleb128 0
	.uleb128 0
.LLSDACSE7074:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT7074:
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.weak	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm:
.LFB7073:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7073
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 136
	.cfi_def_cfa_offset 192
	mov	QWORD PTR [rsp+8], rdi
	mov	r14d, esi
	mov	r15, rdx
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+120], rax
	xor	eax, eax
	call	omp_get_max_threads
	mov	r12d, eax
	movsx	rbp, eax
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+24], 0
	mov	QWORD PTR [rsp+32], 0
	test	rbp, rbp
	je	.L521
	movabs	rax, 329406144173384850
	cmp	rbp, rax
	jbe	.L522
.LEHB84:
	call	_ZSt17__throw_bad_allocv
.L522:
	lea	rax, [0+rbp*8]
	mov	rbx, rbp
	sal	rbx, 6
	sub	rbx, rax
	mov	rdi, rbx
	call	_Znwm
.LEHE84:
	mov	r13, rax
	mov	QWORD PTR [rsp+16], rax
	mov	QWORD PTR [rsp+24], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+32], rbx
	mov	rbx, rax
.L524:
	test	rbx, rbx
	je	.L523
	mov	edx, 1075
	mov	esi, 1023
	mov	rdi, rbx
.LEHB85:
	call	_ZN16SuperaccumulatorC1Eii
.LEHE85:
.L523:
	add	rbx, 56
	sub	rbp, 1
	jne	.L524
	jmp	.L557
.L549:
	mov	rbx, rax
	call	__cxa_end_catch
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	jne	.L527
	jmp	.L528
.L550:
	mov	rdi, rax
	call	__cxa_begin_catch
	cmp	rbx, r13
	je	.L530
.L552:
	mov	rdi, QWORD PTR [r13+8]
	test	rdi, rdi
	je	.L531
	call	_ZdlPv
.L531:
	add	r13, 56
	cmp	rbx, r13
	jne	.L552
.L530:
.LEHB86:
	call	__cxa_rethrow
.LEHE86:
.L557:
	mov	rax, QWORD PTR [rsp+32]
	mov	QWORD PTR [rsp+24], rax
	sal	r12d, 4
	movsx	r12, r12d
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+56], 0
	mov	QWORD PTR [rsp+64], 0
	test	r12, r12
	jne	.L533
	jmp	.L534
.L527:
	call	_ZdlPv
.L528:
	mov	rdi, rbx
.LEHB87:
	call	_Unwind_Resume
.LEHE87:
.L533:
	movabs	rax, 4611686018427387903
	cmp	r12, rax
	jbe	.L535
.LEHB88:
	call	_ZSt17__throw_bad_allocv
.L535:
	lea	rbx, [0+r12*4]
	mov	rdi, rbx
	call	_Znwm
.LEHE88:
	mov	QWORD PTR [rsp+48], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+64], rbx
	mov	edx, 0
.L536:
	mov	DWORD PTR [rax+rdx*4], 0
	add	rdx, 1
	cmp	r12, rdx
	jne	.L536
.L545:
	mov	rax, QWORD PTR [rsp+64]
	mov	QWORD PTR [rsp+56], rax
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbp, rdx
	mov	rax, QWORD PTR [rsp+8]
	mov	QWORD PTR [rsp+80], rax
	lea	rax, [rsp+16]
	mov	QWORD PTR [rsp+88], rax
	lea	rax, [rsp+48]
	mov	QWORD PTR [rsp+96], rax
	mov	DWORD PTR [rsp+104], r14d
	mov	ecx, 0
	mov	edx, 0
	lea	rsi, [rsp+80]
	mov	edi, OFFSET FLAT:_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.1
	call	GOMP_parallel
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbx, rdx
	mov	rdi, QWORD PTR [rsp+16]
.LEHB89:
	call	_ZN16Superaccumulator5RoundEv
.LEHE89:
	vmovq	r12, xmm0
	sub	rbx, rbp
	mov	QWORD PTR [r15], rbx
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L537
	call	_ZdlPv
.L537:
	mov	rbp, QWORD PTR [rsp+24]
	mov	rbx, QWORD PTR [rsp+16]
	cmp	rbp, rbx
	je	.L538
.L551:
	mov	rdi, QWORD PTR [rbx+8]
	test	rdi, rdi
	je	.L539
	call	_ZdlPv
.L539:
	add	rbx, 56
	cmp	rbp, rbx
	jne	.L551
.L538:
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	je	.L541
	call	_ZdlPv
.L541:
	vmovq	xmm0, r12
	mov	rax, QWORD PTR [rsp+120]
	xor	rax, QWORD PTR fs:40
	je	.L546
	jmp	.L558
.L548:
	mov	rbx, rax
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L544
	call	_ZdlPv
	jmp	.L544
.L547:
	mov	rbx, rax
.L544:
	lea	rdi, [rsp+16]
	call	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED1Ev
	mov	rdi, rbx
.LEHB90:
	call	_Unwind_Resume
.LEHE90:
.L521:
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+32], 0
	mov	QWORD PTR [rsp+24], 0
.L534:
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+64], 0
	jmp	.L545
.L558:
	call	__stack_chk_fail
.L546:
	add	rsp, 136
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_def_cfa_offset 48
	pop	rbp
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE7073:
	.section	.gcc_except_table._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"aG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.align 4
.LLSDA7073:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT7073-.LLSDATTD7073
.LLSDATTD7073:
	.byte	0x1
	.uleb128 .LLSDACSE7073-.LLSDACSB7073
.LLSDACSB7073:
	.uleb128 .LEHB84-.LFB7073
	.uleb128 .LEHE84-.LEHB84
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB85-.LFB7073
	.uleb128 .LEHE85-.LEHB85
	.uleb128 .L550-.LFB7073
	.uleb128 0x1
	.uleb128 .LEHB86-.LFB7073
	.uleb128 .LEHE86-.LEHB86
	.uleb128 .L549-.LFB7073
	.uleb128 0
	.uleb128 .LEHB87-.LFB7073
	.uleb128 .LEHE87-.LEHB87
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB88-.LFB7073
	.uleb128 .LEHE88-.LEHB88
	.uleb128 .L547-.LFB7073
	.uleb128 0
	.uleb128 .LEHB89-.LFB7073
	.uleb128 .LEHE89-.LEHB89
	.uleb128 .L548-.LFB7073
	.uleb128 0
	.uleb128 .LEHB90-.LFB7073
	.uleb128 .LEHE90-.LEHB90
	.uleb128 0
	.uleb128 0
.LLSDACSE7073:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT7073:
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.weak	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm:
.LFB7072:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7072
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 136
	.cfi_def_cfa_offset 192
	mov	QWORD PTR [rsp+8], rdi
	mov	r14d, esi
	mov	r15, rdx
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+120], rax
	xor	eax, eax
	call	omp_get_max_threads
	mov	r12d, eax
	movsx	rbp, eax
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+24], 0
	mov	QWORD PTR [rsp+32], 0
	test	rbp, rbp
	je	.L561
	movabs	rax, 329406144173384850
	cmp	rbp, rax
	jbe	.L562
.LEHB91:
	call	_ZSt17__throw_bad_allocv
.L562:
	lea	rax, [0+rbp*8]
	mov	rbx, rbp
	sal	rbx, 6
	sub	rbx, rax
	mov	rdi, rbx
	call	_Znwm
.LEHE91:
	mov	r13, rax
	mov	QWORD PTR [rsp+16], rax
	mov	QWORD PTR [rsp+24], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+32], rbx
	mov	rbx, rax
.L564:
	test	rbx, rbx
	je	.L563
	mov	edx, 1075
	mov	esi, 1023
	mov	rdi, rbx
.LEHB92:
	call	_ZN16SuperaccumulatorC1Eii
.LEHE92:
.L563:
	add	rbx, 56
	sub	rbp, 1
	jne	.L564
	jmp	.L597
.L589:
	mov	rbx, rax
	call	__cxa_end_catch
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	jne	.L567
	jmp	.L568
.L590:
	mov	rdi, rax
	call	__cxa_begin_catch
	cmp	rbx, r13
	je	.L570
.L592:
	mov	rdi, QWORD PTR [r13+8]
	test	rdi, rdi
	je	.L571
	call	_ZdlPv
.L571:
	add	r13, 56
	cmp	rbx, r13
	jne	.L592
.L570:
.LEHB93:
	call	__cxa_rethrow
.LEHE93:
.L597:
	mov	rax, QWORD PTR [rsp+32]
	mov	QWORD PTR [rsp+24], rax
	sal	r12d, 4
	movsx	r12, r12d
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+56], 0
	mov	QWORD PTR [rsp+64], 0
	test	r12, r12
	jne	.L573
	jmp	.L574
.L567:
	call	_ZdlPv
.L568:
	mov	rdi, rbx
.LEHB94:
	call	_Unwind_Resume
.LEHE94:
.L573:
	movabs	rax, 4611686018427387903
	cmp	r12, rax
	jbe	.L575
.LEHB95:
	call	_ZSt17__throw_bad_allocv
.L575:
	lea	rbx, [0+r12*4]
	mov	rdi, rbx
	call	_Znwm
.LEHE95:
	mov	QWORD PTR [rsp+48], rax
	add	rbx, rax
	mov	QWORD PTR [rsp+64], rbx
	mov	edx, 0
.L576:
	mov	DWORD PTR [rax+rdx*4], 0
	add	rdx, 1
	cmp	r12, rdx
	jne	.L576
.L585:
	mov	rax, QWORD PTR [rsp+64]
	mov	QWORD PTR [rsp+56], rax
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbp, rdx
	mov	rax, QWORD PTR [rsp+8]
	mov	QWORD PTR [rsp+80], rax
	lea	rax, [rsp+16]
	mov	QWORD PTR [rsp+88], rax
	lea	rax, [rsp+48]
	mov	QWORD PTR [rsp+96], rax
	mov	DWORD PTR [rsp+104], r14d
	mov	ecx, 0
	mov	edx, 0
	lea	rsi, [rsp+80]
	mov	edi, OFFSET FLAT:_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.0
	call	GOMP_parallel
#APP
# 45 "mylibm.hpp" 1
	rdtsc
# 0 "" 2
#NO_APP
	sal	rdx, 32
	mov	eax, eax
	or	rdx, rax
	mov	rbx, rdx
	mov	rdi, QWORD PTR [rsp+16]
.LEHB96:
	call	_ZN16Superaccumulator5RoundEv
.LEHE96:
	vmovq	r12, xmm0
	sub	rbx, rbp
	mov	QWORD PTR [r15], rbx
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L577
	call	_ZdlPv
.L577:
	mov	rbp, QWORD PTR [rsp+24]
	mov	rbx, QWORD PTR [rsp+16]
	cmp	rbp, rbx
	je	.L578
.L591:
	mov	rdi, QWORD PTR [rbx+8]
	test	rdi, rdi
	je	.L579
	call	_ZdlPv
.L579:
	add	rbx, 56
	cmp	rbp, rbx
	jne	.L591
.L578:
	mov	rdi, QWORD PTR [rsp+16]
	test	rdi, rdi
	je	.L581
	call	_ZdlPv
.L581:
	vmovq	xmm0, r12
	mov	rax, QWORD PTR [rsp+120]
	xor	rax, QWORD PTR fs:40
	je	.L586
	jmp	.L598
.L588:
	mov	rbx, rax
	mov	rdi, QWORD PTR [rsp+48]
	test	rdi, rdi
	je	.L584
	call	_ZdlPv
	jmp	.L584
.L587:
	mov	rbx, rax
.L584:
	lea	rdi, [rsp+16]
	call	_ZNSt6vectorI16SuperaccumulatorSaIS0_EED1Ev
	mov	rdi, rbx
.LEHB97:
	call	_Unwind_Resume
.LEHE97:
.L561:
	mov	QWORD PTR [rsp+16], 0
	mov	QWORD PTR [rsp+32], 0
	mov	QWORD PTR [rsp+24], 0
.L574:
	mov	QWORD PTR [rsp+48], 0
	mov	QWORD PTR [rsp+64], 0
	jmp	.L585
.L598:
	call	__stack_chk_fail
.L586:
	add	rsp, 136
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_def_cfa_offset 48
	pop	rbp
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE7072:
	.section	.gcc_except_table._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"aG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.align 4
.LLSDA7072:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT7072-.LLSDATTD7072
.LLSDATTD7072:
	.byte	0x1
	.uleb128 .LLSDACSE7072-.LLSDACSB7072
.LLSDACSB7072:
	.uleb128 .LEHB91-.LFB7072
	.uleb128 .LEHE91-.LEHB91
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB92-.LFB7072
	.uleb128 .LEHE92-.LEHB92
	.uleb128 .L590-.LFB7072
	.uleb128 0x1
	.uleb128 .LEHB93-.LFB7072
	.uleb128 .LEHE93-.LEHB93
	.uleb128 .L589-.LFB7072
	.uleb128 0
	.uleb128 .LEHB94-.LFB7072
	.uleb128 .LEHE94-.LEHB94
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB95-.LFB7072
	.uleb128 .LEHE95-.LEHB95
	.uleb128 .L587-.LFB7072
	.uleb128 0
	.uleb128 .LEHB96-.LFB7072
	.uleb128 .LEHE96-.LEHB96
	.uleb128 .L588-.LFB7072
	.uleb128 0
	.uleb128 .LEHB97-.LFB7072
	.uleb128 .LEHE97-.LEHB97
	.uleb128 0
	.uleb128 0
.LLSDACSE7072:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT7072:
	.section	.text._Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,"axG",@progbits,_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm,comdat
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	.section	.rodata.str1.1
.LC9:
	.string	"./datasum/c%ss100000.dat"
.LC10:
	.string	"./datasum/c%ss%d.dat"
.LC11:
	.string	"%s, "
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"%d, %.16g, %.16g, %ld, %ld, 0, 0, "
	.section	.rodata.str1.1
.LC13:
	.string	"%s\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB6973:
	.cfi_startproc
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 280
	.cfi_def_cfa_offset 336
	mov	rbx, rsi
	mov	QWORD PTR [rsp+16], rsi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+264], rax
	xor	eax, eax
	mov	rdi, QWORD PTR [rsi+8]
	mov	edx, 10
	mov	esi, 0
	call	strtol
	mov	QWORD PTR [rsp+8], rax
	mov	rdi, QWORD PTR [rbx+16]
	mov	edx, 10
	mov	esi, 0
	call	strtol
	mov	r14, rax
	mov	rdi, QWORD PTR [rbx+24]
	mov	edx, 10
	mov	esi, 0
	call	strtol
	mov	r15, rax
	mov	QWORD PTR [rsp+24], rax
	mov	ebp, eax
	mov	rdi, QWORD PTR [rbx+40]
	mov	edx, 10
	mov	esi, 0
	call	strtol
	mov	QWORD PTR [rsp], rax
	mov	r12d, eax
	movsx	r13, r15d
	lea	rsi, [0+r13*8]
	mov	edi, 64
	call	memalign
	mov	rbx, rax
	mov	QWORD PTR [rsp+160], 0
	lea	rdi, [rsp+168]
	mov	ecx, 11
	mov	eax, 0
	rep stosq
	mov	DWORD PTR [rdi], 0
	cmp	r15d, 100000
	jle	.L601
	mov	rax, QWORD PTR [rsp+16]
	mov	r8, QWORD PTR [rax+32]
	mov	ecx, OFFSET FLAT:.LC9
	mov	edx, 100
	mov	esi, 1
	lea	rdi, [rsp+160]
	mov	eax, 0
	call	__sprintf_chk
	jmp	.L602
.L601:
	mov	r9d, DWORD PTR [rsp+24]
	mov	rax, QWORD PTR [rsp+16]
	mov	r8, QWORD PTR [rax+32]
	mov	ecx, OFFSET FLAT:.LC10
	mov	edx, 100
	mov	esi, 1
	lea	rdi, [rsp+160]
	mov	eax, 0
	call	__sprintf_chk
.L602:
	lea	rcx, [rsp+144]
	mov	rdx, r13
	lea	rsi, [rsp+160]
	mov	rdi, rbx
	call	_Z22initGeneratedSumVectorPdPcmS_
	mov	rax, QWORD PTR [rsp+16]
	mov	rdx, QWORD PTR [rax+48]
	mov	esi, OFFSET FLAT:.LC11
	mov	edi, 1
	mov	eax, 0
	call	__printf_chk
	test	r14d, r14d
	je	.L603
	cmp	DWORD PTR [rsp+8], 2
	jne	.L604
	cmp	DWORD PTR [rsp], 0
	je	.L646
	mov	r14d, 0
	mov	r15d, 0
	mov	r13, -2147483648
.L606:
	lea	rdx, [rsp+152]
	mov	esi, ebp
	mov	rdi, rbx
	call	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	mov	rax, QWORD PTR [rsp+152]
	add	r15, rax
	cmp	r13, rax
	cmova	r13, rax
	add	r14d, 1
	cmp	r14d, r12d
	jne	.L606
	vmovsd	QWORD PTR [rsp+48], xmm0
	jmp	.L605
.L646:
	mov	r15d, 0
	mov	r13, -2147483648
.L605:
	movsx	rcx, DWORD PTR [rsp]
	mov	rax, r15
	mov	edx, 0
	div	rcx
	mov	r8, rax
	mov	rcx, r13
	vmovsd	xmm1, QWORD PTR [rsp+144]
	vmovsd	xmm0, QWORD PTR [rsp+48]
	mov	edx, DWORD PTR [rsp+24]
	mov	esi, OFFSET FLAT:.LC12
	mov	edi, 1
	mov	eax, 2
	call	__printf_chk
	jmp	.L607
.L604:
	cmp	DWORD PTR [rsp+8], 3
	jne	.L608
	cmp	DWORD PTR [rsp], 0
	je	.L647
	mov	r14d, 0
	mov	r15d, 0
	mov	r13, -2147483648
.L610:
	lea	rdx, [rsp+152]
	mov	esi, ebp
	mov	rdi, rbx
	call	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	mov	rax, QWORD PTR [rsp+152]
	add	r15, rax
	cmp	r13, rax
	cmova	r13, rax
	add	r14d, 1
	cmp	r14d, r12d
	jne	.L610
	vmovsd	QWORD PTR [rsp+56], xmm0
	jmp	.L609
.L647:
	mov	r15d, 0
	mov	r13, -2147483648
.L609:
	movsx	rcx, DWORD PTR [rsp]
	mov	rax, r15
	mov	edx, 0
	div	rcx
	mov	r8, rax
	mov	rcx, r13
	vmovsd	xmm1, QWORD PTR [rsp+144]
	vmovsd	xmm0, QWORD PTR [rsp+56]
	mov	edx, DWORD PTR [rsp+24]
	mov	esi, OFFSET FLAT:.LC12
	mov	edi, 1
	mov	eax, 2
	call	__printf_chk
	jmp	.L611
.L608:
	cmp	DWORD PTR [rsp+8], 4
	jne	.L607
	cmp	DWORD PTR [rsp], 0
	je	.L648
	mov	r14d, 0
	mov	r15d, 0
	mov	r13, -2147483648
.L613:
	lea	rdx, [rsp+152]
	mov	esi, ebp
	mov	rdi, rbx
	call	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	mov	rax, QWORD PTR [rsp+152]
	add	r15, rax
	cmp	r13, rax
	cmova	r13, rax
	add	r14d, 1
	cmp	r14d, r12d
	jne	.L613
	vmovsd	QWORD PTR [rsp+64], xmm0
	jmp	.L612
.L648:
	mov	r15d, 0
	mov	r13, -2147483648
.L612:
	movsx	rcx, DWORD PTR [rsp]
	mov	rax, r15
	mov	edx, 0
	div	rcx
	mov	r8, rax
	mov	rcx, r13
	vmovsd	xmm1, QWORD PTR [rsp+144]
	vmovsd	xmm0, QWORD PTR [rsp+64]
	mov	edx, DWORD PTR [rsp+24]
	mov	esi, OFFSET FLAT:.LC12
	mov	edi, 1
	mov	eax, 2
	call	__printf_chk
	jmp	.L614
.L607:
	cmp	DWORD PTR [rsp+8], 5
	jne	.L611
	cmp	DWORD PTR [rsp], 0
	je	.L649
	mov	r14d, 0
	mov	r15d, 0
	mov	r13, -2147483648
.L616:
	lea	rdx, [rsp+152]
	mov	esi, ebp
	mov	rdi, rbx
	call	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	mov	rax, QWORD PTR [rsp+152]
	add	r15, rax
	cmp	r13, rax
	cmova	r13, rax
	add	r14d, 1
	cmp	r14d, r12d
	jne	.L616
	vmovsd	QWORD PTR [rsp+72], xmm0
	jmp	.L615
.L649:
	mov	r15d, 0
	mov	r13, -2147483648
.L615:
	movsx	rcx, DWORD PTR [rsp]
	mov	rax, r15
	mov	edx, 0
	div	rcx
	mov	r8, rax
	mov	rcx, r13
	vmovsd	xmm1, QWORD PTR [rsp+144]
	vmovsd	xmm0, QWORD PTR [rsp+72]
	mov	edx, DWORD PTR [rsp+24]
	mov	esi, OFFSET FLAT:.LC12
	mov	edi, 1
	mov	eax, 2
	call	__printf_chk
	jmp	.L617
.L611:
	cmp	DWORD PTR [rsp+8], 6
	jne	.L614
	cmp	DWORD PTR [rsp], 0
	je	.L650
	mov	r14d, 0
	mov	r15d, 0
	mov	r13, -2147483648
.L619:
	lea	rdx, [rsp+152]
	mov	esi, ebp
	mov	rdi, rbx
	call	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	mov	rax, QWORD PTR [rsp+152]
	add	r15, rax
	cmp	r13, rax
	cmova	r13, rax
	add	r14d, 1
	cmp	r14d, r12d
	jne	.L619
	vmovsd	QWORD PTR [rsp+80], xmm0
	jmp	.L618
.L650:
	mov	r15d, 0
	mov	r13, -2147483648
.L618:
	movsx	rcx, DWORD PTR [rsp]
	mov	rax, r15
	mov	edx, 0
	div	rcx
	mov	r8, rax
	mov	rcx, r13
	vmovsd	xmm1, QWORD PTR [rsp+144]
	vmovsd	xmm0, QWORD PTR [rsp+80]
	mov	edx, DWORD PTR [rsp+24]
	mov	esi, OFFSET FLAT:.LC12
	mov	edi, 1
	mov	eax, 2
	call	__printf_chk
	jmp	.L620
.L614:
	cmp	DWORD PTR [rsp+8], 7
	jne	.L617
	cmp	DWORD PTR [rsp], 0
	je	.L651
	mov	r14d, 0
	mov	r15d, 0
	mov	r13, -2147483648
.L622:
	lea	rdx, [rsp+152]
	mov	esi, ebp
	mov	rdi, rbx
	call	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	mov	rax, QWORD PTR [rsp+152]
	add	r15, rax
	cmp	r13, rax
	cmova	r13, rax
	add	r14d, 1
	cmp	r14d, r12d
	jne	.L622
	vmovsd	QWORD PTR [rsp+88], xmm0
	jmp	.L621
.L651:
	mov	r15d, 0
	mov	r13, -2147483648
.L621:
	movsx	rcx, DWORD PTR [rsp]
	mov	rax, r15
	mov	edx, 0
	div	rcx
	mov	r8, rax
	mov	rcx, r13
	vmovsd	xmm1, QWORD PTR [rsp+144]
	vmovsd	xmm0, QWORD PTR [rsp+88]
	mov	edx, DWORD PTR [rsp+24]
	mov	esi, OFFSET FLAT:.LC12
	mov	edi, 1
	mov	eax, 2
	call	__printf_chk
	jmp	.L620
.L617:
	cmp	DWORD PTR [rsp+8], 8
	jne	.L620
	cmp	DWORD PTR [rsp], 0
	je	.L652
	mov	r14d, 0
	mov	r15d, 0
	mov	r13, -2147483648
.L624:
	lea	rdx, [rsp+152]
	mov	esi, ebp
	mov	rdi, rbx
	call	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	mov	rax, QWORD PTR [rsp+152]
	add	r15, rax
	cmp	r13, rax
	cmova	r13, rax
	add	r14d, 1
	cmp	r14d, r12d
	jne	.L624
	vmovsd	QWORD PTR [rsp+32], xmm0
	jmp	.L623
.L652:
	mov	r15d, 0
	mov	r13, -2147483648
.L623:
	movsx	rcx, DWORD PTR [rsp]
	mov	rax, r15
	mov	edx, 0
	div	rcx
	mov	r8, rax
	mov	rcx, r13
	vmovsd	xmm1, QWORD PTR [rsp+144]
	vmovsd	xmm0, QWORD PTR [rsp+32]
	mov	edx, DWORD PTR [rsp+24]
	mov	esi, OFFSET FLAT:.LC12
	mov	edi, 1
	mov	eax, 2
	call	__printf_chk
	jmp	.L620
.L603:
	cmp	DWORD PTR [rsp+8], 2
	jne	.L625
	cmp	DWORD PTR [rsp], 0
	je	.L653
	mov	r14d, 0
	mov	r15d, 0
	mov	r13, -2147483648
.L627:
	lea	rdx, [rsp+152]
	mov	esi, ebp
	mov	rdi, rbx
	call	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	mov	rax, QWORD PTR [rsp+152]
	add	r15, rax
	cmp	r13, rax
	cmova	r13, rax
	add	r14d, 1
	cmp	r14d, r12d
	jne	.L627
	vmovsd	QWORD PTR [rsp+96], xmm0
	jmp	.L626
.L653:
	mov	r15d, 0
	mov	r13, -2147483648
.L626:
	movsx	rcx, DWORD PTR [rsp]
	mov	rax, r15
	mov	edx, 0
	div	rcx
	mov	r8, rax
	mov	rcx, r13
	vmovsd	xmm1, QWORD PTR [rsp+144]
	vmovsd	xmm0, QWORD PTR [rsp+96]
	mov	edx, DWORD PTR [rsp+24]
	mov	esi, OFFSET FLAT:.LC12
	mov	edi, 1
	mov	eax, 2
	call	__printf_chk
	jmp	.L628
.L625:
	cmp	DWORD PTR [rsp+8], 3
	jne	.L629
	cmp	DWORD PTR [rsp], 0
	je	.L654
	mov	r14d, 0
	mov	r15d, 0
	mov	r13, -2147483648
.L631:
	lea	rdx, [rsp+152]
	mov	esi, ebp
	mov	rdi, rbx
	call	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	mov	rax, QWORD PTR [rsp+152]
	add	r15, rax
	cmp	r13, rax
	cmova	r13, rax
	add	r14d, 1
	cmp	r14d, r12d
	jne	.L631
	vmovsd	QWORD PTR [rsp+104], xmm0
	jmp	.L630
.L654:
	mov	r15d, 0
	mov	r13, -2147483648
.L630:
	movsx	rcx, DWORD PTR [rsp]
	mov	rax, r15
	mov	edx, 0
	div	rcx
	mov	r8, rax
	mov	rcx, r13
	vmovsd	xmm1, QWORD PTR [rsp+144]
	vmovsd	xmm0, QWORD PTR [rsp+104]
	mov	edx, DWORD PTR [rsp+24]
	mov	esi, OFFSET FLAT:.LC12
	mov	edi, 1
	mov	eax, 2
	call	__printf_chk
	jmp	.L632
.L629:
	cmp	DWORD PTR [rsp+8], 4
	jne	.L628
	cmp	DWORD PTR [rsp], 0
	je	.L655
	mov	r14d, 0
	mov	r15d, 0
	mov	r13, -2147483648
.L634:
	lea	rdx, [rsp+152]
	mov	esi, ebp
	mov	rdi, rbx
	call	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	mov	rax, QWORD PTR [rsp+152]
	add	r15, rax
	cmp	r13, rax
	cmova	r13, rax
	add	r14d, 1
	cmp	r14d, r12d
	jne	.L634
	vmovsd	QWORD PTR [rsp+112], xmm0
	jmp	.L633
.L655:
	mov	r15d, 0
	mov	r13, -2147483648
.L633:
	movsx	rcx, DWORD PTR [rsp]
	mov	rax, r15
	mov	edx, 0
	div	rcx
	mov	r8, rax
	mov	rcx, r13
	vmovsd	xmm1, QWORD PTR [rsp+144]
	vmovsd	xmm0, QWORD PTR [rsp+112]
	mov	edx, DWORD PTR [rsp+24]
	mov	esi, OFFSET FLAT:.LC12
	mov	edi, 1
	mov	eax, 2
	call	__printf_chk
	jmp	.L635
.L628:
	cmp	DWORD PTR [rsp+8], 5
	jne	.L632
	cmp	DWORD PTR [rsp], 0
	je	.L656
	mov	r14d, 0
	mov	r15d, 0
	mov	r13, -2147483648
.L637:
	lea	rdx, [rsp+152]
	mov	esi, ebp
	mov	rdi, rbx
	call	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	mov	rax, QWORD PTR [rsp+152]
	add	r15, rax
	cmp	r13, rax
	cmova	r13, rax
	add	r14d, 1
	cmp	r14d, r12d
	jne	.L637
	vmovsd	QWORD PTR [rsp+120], xmm0
	jmp	.L636
.L656:
	mov	r15d, 0
	mov	r13, -2147483648
.L636:
	movsx	rcx, DWORD PTR [rsp]
	mov	rax, r15
	mov	edx, 0
	div	rcx
	mov	r8, rax
	mov	rcx, r13
	vmovsd	xmm1, QWORD PTR [rsp+144]
	vmovsd	xmm0, QWORD PTR [rsp+120]
	mov	edx, DWORD PTR [rsp+24]
	mov	esi, OFFSET FLAT:.LC12
	mov	edi, 1
	mov	eax, 2
	call	__printf_chk
	jmp	.L638
.L632:
	cmp	DWORD PTR [rsp+8], 6
	jne	.L635
	cmp	DWORD PTR [rsp], 0
	je	.L657
	mov	r14d, 0
	mov	r15d, 0
	mov	r13, -2147483648
.L640:
	lea	rdx, [rsp+152]
	mov	esi, ebp
	mov	rdi, rbx
	call	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	mov	rax, QWORD PTR [rsp+152]
	add	r15, rax
	cmp	r13, rax
	cmova	r13, rax
	add	r14d, 1
	cmp	r14d, r12d
	jne	.L640
	vmovsd	QWORD PTR [rsp+128], xmm0
	jmp	.L639
.L657:
	mov	r15d, 0
	mov	r13, -2147483648
.L639:
	movsx	rcx, DWORD PTR [rsp]
	mov	rax, r15
	mov	edx, 0
	div	rcx
	mov	r8, rax
	mov	rcx, r13
	vmovsd	xmm1, QWORD PTR [rsp+144]
	vmovsd	xmm0, QWORD PTR [rsp+128]
	mov	edx, DWORD PTR [rsp+24]
	mov	esi, OFFSET FLAT:.LC12
	mov	edi, 1
	mov	eax, 2
	call	__printf_chk
	jmp	.L620
.L635:
	cmp	DWORD PTR [rsp+8], 7
	jne	.L638
	cmp	DWORD PTR [rsp], 0
	je	.L658
	mov	r14d, 0
	mov	r15d, 0
	mov	r13, -2147483648
.L642:
	lea	rdx, [rsp+152]
	mov	esi, ebp
	mov	rdi, rbx
	call	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	mov	rax, QWORD PTR [rsp+152]
	add	r15, rax
	cmp	r13, rax
	cmova	r13, rax
	add	r14d, 1
	cmp	r14d, r12d
	jne	.L642
	vmovsd	QWORD PTR [rsp+136], xmm0
	jmp	.L641
.L658:
	mov	r15d, 0
	mov	r13, -2147483648
.L641:
	movsx	rcx, DWORD PTR [rsp]
	mov	rax, r15
	mov	edx, 0
	div	rcx
	mov	r8, rax
	mov	rcx, r13
	vmovsd	xmm1, QWORD PTR [rsp+144]
	vmovsd	xmm0, QWORD PTR [rsp+136]
	mov	edx, DWORD PTR [rsp+24]
	mov	esi, OFFSET FLAT:.LC12
	mov	edi, 1
	mov	eax, 2
	call	__printf_chk
	jmp	.L620
.L638:
	cmp	DWORD PTR [rsp+8], 8
	jne	.L620
	cmp	DWORD PTR [rsp], 0
	je	.L659
	mov	r14d, 0
	mov	r15d, 0
	mov	r13, -2147483648
.L644:
	lea	rdx, [rsp+152]
	mov	esi, ebp
	mov	rdi, rbx
	call	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm
	mov	rax, QWORD PTR [rsp+152]
	add	r15, rax
	cmp	r13, rax
	cmova	r13, rax
	add	r14d, 1
	cmp	r14d, r12d
	jne	.L644
	vmovsd	QWORD PTR [rsp+40], xmm0
	jmp	.L643
.L659:
	mov	r15d, 0
	mov	r13, -2147483648
.L643:
	movsx	rcx, DWORD PTR [rsp]
	mov	rax, r15
	mov	edx, 0
	div	rcx
	mov	r8, rax
	mov	rcx, r13
	vmovsd	xmm1, QWORD PTR [rsp+144]
	vmovsd	xmm0, QWORD PTR [rsp+40]
	mov	edx, DWORD PTR [rsp+24]
	mov	esi, OFFSET FLAT:.LC12
	mov	edi, 1
	mov	eax, 2
	call	__printf_chk
.L620:
	mov	rax, QWORD PTR [rsp+16]
	mov	rdx, QWORD PTR [rax+56]
	mov	esi, OFFSET FLAT:.LC13
	mov	edi, 1
	mov	eax, 0
	call	__printf_chk
	mov	eax, 0
	mov	rcx, QWORD PTR [rsp+264]
	xor	rcx, QWORD PTR fs:40
	je	.L645
	call	__stack_chk_fail
.L645:
	add	rsp, 280
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_def_cfa_offset 48
	pop	rbp
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE6973:
	.size	main, .-main
	.section	.text._ZN15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,"axG",@progbits,_ZN15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,comdat
	.align 2
	.weak	_ZN15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.type	_ZN15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, @function
_ZN15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv:
.LFB7205:
	.cfi_startproc
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 64
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	lea	r12, [rdi+32]
	lea	rax, [rdi+96]
	mov	QWORD PTR [rbp-112], rax
	lea	rax, [rbp-96]
	lea	r13, [rax+32]
	vxorpd	xmm6, xmm6, xmm6
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm6
	vmovapd	xmm4, xmm6
.L685:
	mov	QWORD PTR [rbp-104], r12
	vmovapd	ymm0, YMMWORD PTR [r12]
	vmovupd	YMMWORD PTR [rbp-96], ymm0
	vzeroupper
	lea	r10, [rbp-96]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm8, xmm7
	vmovapd	xmm5, xmm7
.L684:
	vmovsd	xmm0, QWORD PTR [r10]
	mov	rsi, QWORD PTR [rdi]
	vucomisd	xmm0, xmm6
	jp	.L688
	vucomisd	xmm0, xmm5
	je	.L676
.L688:
	vmovq	rcx, xmm0
	vmovq	r8, xmm0
	shr	r8, 52
	and	r8d, 2047
	sub	r8, 1023
	mov	edx, -1840700269
	mov	eax, r8d
	imul	edx
	add	edx, r8d
	sar	edx, 5
	sar	r8d, 31
	sub	edx, r8d
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm7
	jp	.L678
	vucomisd	xmm0, xmm8
	je	.L676
.L678:
	mov	r14d, 256
.L692:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r9, xmm0
	movsx	rdx, eax
	mov	r8, QWORD PTR [rsi+8]
	mov	rcx, r9
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r8+rdx*8], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	je	.L679
	mov	r11d, eax
.L682:
	add	r9, rcx
	sar	r9, 56
	test	rcx, rcx
	mov	rcx, -256
	cmovg	rcx, r14
	movsx	r8, r11d
	sal	r8, 3
	mov	rbx, r8
	add	rbx, QWORD PTR [rsi+8]
	mov	rdx, r9
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rbx], rdx
seto r15b
# 0 "" 2
#NO_APP
	add	r9, rcx
	add	r11d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r11d, edx
	jl	.L681
	mov	DWORD PTR [rsi+40], 4
	jmp	.L679
.L681:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r9
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+r8], rcx
seto bl
# 0 "" 2
#NO_APP
	test	bl, bl
	jne	.L682
.L679:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L692
	vucomisd	xmm0, xmm4
	jne	.L692
.L676:
	add	r10, 8
	cmp	r10, r13
	jne	.L684
	vxorpd	xmm0, xmm0, xmm0
	mov	rax, QWORD PTR [rbp-104]
	vmovapd	YMMWORD PTR [rax], ymm0
	add	r12, 32
	cmp	r12, QWORD PTR [rbp-112]
	jne	.L685
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L686
	call	__stack_chk_fail
.L686:
	add	rsp, 64
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7205:
	.size	_ZN15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, .-_ZN15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.text
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.0, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.0:
.LFB7603:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7603
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 256
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r15, rdi
	mov	QWORD PTR [rbp-248], rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	call	omp_get_thread_num
	mov	ebx, eax
	mov	DWORD PTR [rbp-256], eax
	call	omp_get_num_threads
	mov	r14d, eax
	mov	DWORD PTR [rbp-268], eax
	mov	edx, ebx
	lea	rax, [0+rdx*8]
	mov	rcx, rdx
	sal	rcx, 6
	sub	rcx, rax
	mov	QWORD PTR [rbp-288], rcx
	mov	QWORD PTR [rbp-280], rcx
	mov	rax, QWORD PTR [r15+8]
	add	rcx, QWORD PTR [rax]
	mov	QWORD PTR [rbp-208], rcx
	vxorpd	xmm0, xmm0, xmm0
	vmovapd	YMMWORD PTR [rbp-112], ymm0
	vmovapd	YMMWORD PTR [rbp-176], ymm0
	vmovapd	YMMWORD PTR [rbp-144], ymm0
	mov	eax, ebx
	sal	eax, 4
	mov	eax, eax
	lea	rcx, [0+rax*4]
	mov	QWORD PTR [rbp-264], rcx
	mov	rax, QWORD PTR [r15+16]
	add	rcx, QWORD PTR [rax]
	mov	DWORD PTR [rcx], 0
	movsx	rsi, DWORD PTR [r15+24]
	mov	ecx, r14d
	mov	rax, rdx
	imul	rax, rsi
	cqo
	idiv	rcx
	mov	r9d, eax
	and	r9d, -8
	mov	eax, ebx
	add	eax, 1
	imul	rax, rsi
	cqo
	idiv	rcx
	and	eax, -8
	sub	eax, 1
	mov	DWORD PTR [rbp-252], eax
	cmp	r9d, eax
	jl	.L695
.L726:
	lea	rdi, [rbp-208]
	call	_ZN15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	mov	rbx, QWORD PTR [rbp-248]
	mov	rax, QWORD PTR [rbx+8]
	mov	rdi, QWORD PTR [rbp-288]
	add	rdi, QWORD PTR [rax]
	call	_ZN16Superaccumulator9NormalizeEv
	mov	r14, QWORD PTR [rbx+8]
	mov	r13, QWORD PTR [rbx+16]
	mov	eax, DWORD PTR [rbp-268]
	cmp	eax, 1
	jbe	.L694
	mov	ebx, 1
	mov	edx, 1
	mov	r15d, 1
	mov	QWORD PTR [rbp-248], r14
	mov	r14d, eax
.L700:
	mov	rcx, QWORD PTR [rbp-264]
	add	rcx, QWORD PTR [r13+0]
	mov	eax, DWORD PTR [rcx]
	add	eax, 1
	mov	DWORD PTR [rcx], eax
	mov	r8d, ebx
	mov	r12d, r15d
	mov	ecx, ebx
	sal	r12d, cl
	lea	eax, [r12-1]
	test	DWORD PTR [rbp-256], eax
	jne	.L697
	or	edx, DWORD PTR [rbp-256]
	cmp	r14d, edx
	jbe	.L697
	mov	eax, edx
	sal	eax, 4
	mov	eax, eax
	mov	rcx, QWORD PTR [r13+0]
	lea	rcx, [rcx+rax*4]
	mov	rax, QWORD PTR [rbp-248]
	mov	rax, QWORD PTR [rax]
	mov	edx, edx
	lea	rsi, [0+rdx*8]
	sal	rdx, 6
	sub	rdx, rsi
	mov	rsi, rdx
	add	rsi, rax
	mov	rdi, QWORD PTR [rbp-280]
	add	rdi, rax
	prefetcht0	[rcx]
	mov	eax, DWORD PTR [rcx]
	cmp	ebx, eax
	jle	.L698
.L734:
	rep nop
	mov	eax, DWORD PTR [rcx]
	cmp	r8d, eax
	jg	.L734
.L698:
	call	_ZN16Superaccumulator10AccumulateERS_
.L697:
	mov	edx, r12d
	add	ebx, 1
	cmp	r14d, r12d
	ja	.L700
	jmp	.L694
.L695:
	vmovapd	ymm7, YMMWORD PTR _ZZ10constant8fILin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647EEDv8_fvE1u[rip]
	lea	rax, [rbp-240]
	lea	r8, [rax+32]
	vmovsd	xmm5, QWORD PTR .LC15[rip]
	vmovapd	xmm4, xmm5
.L735:
#APP
# 127 "longacc.cpp" 1
	# myloop
# 0 "" 2
#NO_APP
	movsx	rax, r9d
	add	rax, 4
	mov	rbx, QWORD PTR [rbp-248]
	mov	rdx, QWORD PTR [rbx]
	vmovapd	ymm6, YMMWORD PTR [rdx+rax*8]
	vmovapd	ymm1, YMMWORD PTR [rdx-32+rax*8]
	lea	rax, [rbp-208]
	mov	edx, 0
	vxorpd	xmm9, xmm9, xmm9
.L705:
	mov	rcx, rax
	vmovapd	ymm2, YMMWORD PTR [rax+32]
	vaddpd	ymm0, ymm2, ymm1
	vandpd	ymm3, ymm2, ymm7
	vandpd	ymm8, ymm1, ymm7
	vcmppd	ymm3, ymm3, ymm8, 1
	vptest	ymm3, ymm1
	je	.L701
	vblendvpd	ymm8, ymm2, ymm1, ymm3
	vblendvpd	ymm1, ymm1, ymm2, ymm3
	vmovapd	ymm2, ymm8
.L701:
	vsubpd	ymm2, ymm2, ymm0
	vaddpd	ymm1, ymm2, ymm1
	vaddpd	ymm8, ymm0, ymm6
	vandpd	ymm2, ymm0, ymm7
	vandpd	ymm3, ymm6, ymm7
	vcmppd	ymm2, ymm2, ymm3, 1
	vptest	ymm2, ymm6
	je	.L702
	vblendvpd	ymm3, ymm0, ymm6, ymm2
	vblendvpd	ymm6, ymm6, ymm0, ymm2
	vmovapd	ymm0, ymm3
.L702:
	vsubpd	ymm0, ymm0, ymm8
	vaddpd	ymm6, ymm0, ymm6
	vmovapd	YMMWORD PTR [rcx+32], ymm8
	test	edx, edx
	je	.L703
	vorpd	ymm0, ymm1, ymm6
	vcmppd	ymm0, ymm0, ymm9, 4
	vtestpd	ymm0, ymm0
	je	.L707
.L703:
	add	edx, 1
	add	rax, 32
	cmp	edx, 2
	jne	.L705
	vmovupd	YMMWORD PTR [rbp-240], ymm1
	vzeroupper
	lea	r11, [rbp-240]
	vxorpd	xmm8, xmm8, xmm8
	vmovapd	xmm10, xmm8
	vmovapd	xmm2, xmm8
	vmovapd	xmm3, xmm8
	vmovapd	xmm11, xmm8
	vmovapd	xmm9, xmm8
.L716:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rbx, QWORD PTR [rbp-208]
	vucomisd	xmm0, xmm8
	jp	.L732
	vucomisd	xmm0, xmm9
	je	.L708
.L732:
	vmovq	rcx, xmm0
	vmovq	rsi, xmm0
	shr	rsi, 52
	and	esi, 2047
	sub	rsi, 1023
	mov	edx, -1840700269
	mov	eax, esi
	imul	edx
	add	edx, esi
	sar	edx, 5
	sar	esi, 31
	sub	edx, esi
	mov	esi, edx
	add	esi, DWORD PTR [rbx]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm10
	jp	.L710
	vucomisd	xmm0, xmm11
	je	.L708
.L710:
	mov	r13d, 256
	mov	r12, -256
.L744:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	rdi, xmm0
	movsx	rax, esi
	mov	rcx, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx+rax*8], rdx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L711
	mov	r10d, esi
.L714:
	add	rdi, rdx
	sar	rdi, 56
	test	rdx, rdx
	mov	rdx, r12
	cmovg	rdx, r13
	movsx	rcx, r10d
	sal	rcx, 3
	mov	r14, rcx
	add	r14, QWORD PTR [rbx+8]
	mov	rax, rdi
	sal	rax, 56
	neg	rax
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r14], rax
seto r15b
# 0 "" 2
#NO_APP
	add	rdi, rdx
	add	r10d, 1
	mov	eax, DWORD PTR [rbx+4]
	add	eax, DWORD PTR [rbx]
	cmp	r10d, eax
	jl	.L713
	mov	DWORD PTR [rbx+40], 4
	jmp	.L711
.L713:
	mov	rax, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax+8+rcx], rdx
seto r14b
# 0 "" 2
#NO_APP
	test	r14b, r14b
	jne	.L714
.L711:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm5
	sub	esi, 1
	vucomisd	xmm0, xmm2
	jp	.L744
	vucomisd	xmm0, xmm3
	jne	.L744
.L708:
	add	r11, 8
	cmp	r11, r8
	jne	.L716
	vmovupd	YMMWORD PTR [rbp-240], ymm6
	vzeroupper
	lea	r11, [rbp-240]
	vxorpd	xmm6, xmm6, xmm6
	vmovapd	xmm9, xmm6
	vmovapd	xmm2, xmm6
	vmovapd	xmm3, xmm6
	vmovapd	xmm10, xmm6
	vmovapd	xmm8, xmm6
.L725:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rbx, QWORD PTR [rbp-208]
	vucomisd	xmm0, xmm6
	jp	.L733
	vucomisd	xmm0, xmm8
	je	.L717
.L733:
	vmovq	rcx, xmm0
	vmovq	rsi, xmm0
	shr	rsi, 52
	and	esi, 2047
	sub	rsi, 1023
	mov	edx, -1840700269
	mov	eax, esi
	imul	edx
	add	edx, esi
	sar	edx, 5
	sar	esi, 31
	sub	edx, esi
	mov	esi, edx
	add	esi, DWORD PTR [rbx]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm9
	jp	.L719
	vucomisd	xmm0, xmm10
	je	.L717
.L719:
	mov	r13d, 256
	mov	r12, -256
.L745:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	rdi, xmm0
	movsx	rax, esi
	mov	rcx, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx+rax*8], rdx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L720
	mov	r10d, esi
.L723:
	add	rdi, rdx
	sar	rdi, 56
	test	rdx, rdx
	mov	rdx, r12
	cmovg	rdx, r13
	movsx	rcx, r10d
	sal	rcx, 3
	mov	r14, rcx
	add	r14, QWORD PTR [rbx+8]
	mov	rax, rdi
	sal	rax, 56
	neg	rax
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r14], rax
seto r15b
# 0 "" 2
#NO_APP
	add	rdi, rdx
	add	r10d, 1
	mov	eax, DWORD PTR [rbx+4]
	add	eax, DWORD PTR [rbx]
	cmp	r10d, eax
	jl	.L722
	mov	DWORD PTR [rbx+40], 4
	jmp	.L720
.L722:
	mov	rax, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax+8+rcx], rdx
seto r14b
# 0 "" 2
#NO_APP
	test	r14b, r14b
	jne	.L723
.L720:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm4
	sub	esi, 1
	vucomisd	xmm0, xmm2
	jp	.L745
	vucomisd	xmm0, xmm3
	jne	.L745
.L717:
	add	r11, 8
	cmp	r11, r8
	jne	.L725
.L707:
	add	r9d, 8
	cmp	DWORD PTR [rbp-252], r9d
	jg	.L735
	jmp	.L726
.L731:
.L694:
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L728
	call	__stack_chk_fail
.L728:
	add	rsp, 256
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7603:
	.section	.gcc_except_table,"a",@progbits
.LLSDA7603:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7603-.LLSDACSB7603
.LLSDACSB7603:
.LLSDACSE7603:
	.text
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.0, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.0
	.section	.text._ZN15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,"axG",@progbits,_ZN15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,comdat
	.align 2
	.weak	_ZN15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.type	_ZN15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, @function
_ZN15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv:
.LFB7210:
	.cfi_startproc
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 64
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r12, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	lea	rbx, [rdi+32]
	lea	rax, [rdi+128]
	mov	QWORD PTR [rbp-112], rax
	lea	rax, [rbp-96]
	lea	r13, [rax+32]
	vxorpd	xmm6, xmm6, xmm6
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm6
	vmovapd	xmm4, xmm6
.L758:
	mov	QWORD PTR [rbp-104], rbx
	vmovapd	ymm0, YMMWORD PTR [rbx]
	vmovupd	YMMWORD PTR [rbp-96], ymm0
	vzeroupper
	lea	r9, [rbp-96]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm8, xmm7
	vmovapd	xmm5, xmm7
.L757:
	vmovsd	xmm0, QWORD PTR [r9]
	mov	rsi, QWORD PTR [r12]
	vucomisd	xmm0, xmm6
	jp	.L761
	vucomisd	xmm0, xmm5
	je	.L749
.L761:
	vmovq	rcx, xmm0
	vmovq	rdi, xmm0
	shr	rdi, 52
	and	edi, 2047
	sub	rdi, 1023
	mov	edx, -1840700269
	mov	eax, edi
	imul	edx
	add	edx, edi
	sar	edx, 5
	sar	edi, 31
	sub	edx, edi
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm7
	jp	.L751
	vucomisd	xmm0, xmm8
	je	.L749
.L751:
	mov	r14d, 256
.L765:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r8, xmm0
	movsx	rdx, eax
	mov	rdi, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdi+rdx*8], rcx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L752
	mov	r10d, eax
.L755:
	add	r8, rcx
	sar	r8, 56
	test	rcx, rcx
	mov	rcx, -256
	cmovg	rcx, r14
	movsx	rdi, r10d
	sal	rdi, 3
	mov	r11, rdi
	add	r11, QWORD PTR [rsi+8]
	mov	rdx, r8
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r11], rdx
seto r15b
# 0 "" 2
#NO_APP
	add	r8, rcx
	add	r10d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r10d, edx
	jl	.L754
	mov	DWORD PTR [rsi+40], 4
	jmp	.L752
.L754:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+rdi], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	jne	.L755
.L752:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L765
	vucomisd	xmm0, xmm4
	jne	.L765
.L749:
	add	r9, 8
	cmp	r9, r13
	jne	.L757
	vxorpd	xmm0, xmm0, xmm0
	mov	rax, QWORD PTR [rbp-104]
	vmovapd	YMMWORD PTR [rax], ymm0
	add	rbx, 32
	cmp	rbx, QWORD PTR [rbp-112]
	jne	.L758
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L759
	call	__stack_chk_fail
.L759:
	add	rsp, 64
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7210:
	.size	_ZN15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, .-_ZN15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.text
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.1, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.1:
.LFB7604:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7604
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 288
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r15, rdi
	mov	QWORD PTR [rbp-280], rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	call	omp_get_thread_num
	mov	ebx, eax
	mov	DWORD PTR [rbp-288], eax
	call	omp_get_num_threads
	mov	r14d, eax
	mov	DWORD PTR [rbp-300], eax
	mov	edx, ebx
	lea	rax, [0+rdx*8]
	mov	rcx, rdx
	sal	rcx, 6
	sub	rcx, rax
	mov	QWORD PTR [rbp-320], rcx
	mov	QWORD PTR [rbp-312], rcx
	mov	rax, QWORD PTR [r15+8]
	add	rcx, QWORD PTR [rax]
	mov	QWORD PTR [rbp-240], rcx
	vxorpd	xmm0, xmm0, xmm0
	vmovapd	YMMWORD PTR [rbp-112], ymm0
	vmovapd	YMMWORD PTR [rbp-208], ymm0
	vmovapd	YMMWORD PTR [rbp-176], ymm0
	vmovapd	YMMWORD PTR [rbp-144], ymm0
	mov	eax, ebx
	sal	eax, 4
	mov	eax, eax
	lea	rcx, [0+rax*4]
	mov	QWORD PTR [rbp-296], rcx
	mov	rax, QWORD PTR [r15+16]
	add	rcx, QWORD PTR [rax]
	mov	DWORD PTR [rcx], 0
	movsx	rsi, DWORD PTR [r15+24]
	mov	ecx, r14d
	mov	rax, rdx
	imul	rax, rsi
	cqo
	idiv	rcx
	mov	r9d, eax
	and	r9d, -8
	mov	eax, ebx
	add	eax, 1
	imul	rax, rsi
	cqo
	idiv	rcx
	and	eax, -8
	sub	eax, 1
	mov	DWORD PTR [rbp-284], eax
	cmp	r9d, eax
	jl	.L768
.L799:
	lea	rdi, [rbp-240]
	call	_ZN15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	mov	rbx, QWORD PTR [rbp-280]
	mov	rax, QWORD PTR [rbx+8]
	mov	rdi, QWORD PTR [rbp-320]
	add	rdi, QWORD PTR [rax]
	call	_ZN16Superaccumulator9NormalizeEv
	mov	r14, QWORD PTR [rbx+8]
	mov	r13, QWORD PTR [rbx+16]
	mov	eax, DWORD PTR [rbp-300]
	cmp	eax, 1
	jbe	.L767
	mov	ebx, 1
	mov	edx, 1
	mov	r15d, 1
	mov	QWORD PTR [rbp-280], r14
	mov	r14d, eax
.L773:
	mov	rcx, QWORD PTR [rbp-296]
	add	rcx, QWORD PTR [r13+0]
	mov	eax, DWORD PTR [rcx]
	add	eax, 1
	mov	DWORD PTR [rcx], eax
	mov	r8d, ebx
	mov	r12d, r15d
	mov	ecx, ebx
	sal	r12d, cl
	lea	eax, [r12-1]
	test	DWORD PTR [rbp-288], eax
	jne	.L770
	or	edx, DWORD PTR [rbp-288]
	cmp	r14d, edx
	jbe	.L770
	mov	eax, edx
	sal	eax, 4
	mov	eax, eax
	mov	rcx, QWORD PTR [r13+0]
	lea	rcx, [rcx+rax*4]
	mov	rax, QWORD PTR [rbp-280]
	mov	rax, QWORD PTR [rax]
	mov	edx, edx
	lea	rsi, [0+rdx*8]
	sal	rdx, 6
	sub	rdx, rsi
	mov	rsi, rdx
	add	rsi, rax
	mov	rdi, QWORD PTR [rbp-312]
	add	rdi, rax
	prefetcht0	[rcx]
	mov	eax, DWORD PTR [rcx]
	cmp	ebx, eax
	jle	.L771
.L807:
	rep nop
	mov	eax, DWORD PTR [rcx]
	cmp	r8d, eax
	jg	.L807
.L771:
	call	_ZN16Superaccumulator10AccumulateERS_
.L770:
	mov	edx, r12d
	add	ebx, 1
	cmp	r14d, r12d
	ja	.L773
	jmp	.L767
.L768:
	vmovapd	ymm6, YMMWORD PTR _ZZ10constant8fILin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647EEDv8_fvE1u[rip]
	lea	rax, [rbp-272]
	lea	r8, [rax+32]
	vmovsd	xmm5, QWORD PTR .LC15[rip]
	vmovapd	xmm4, xmm5
.L808:
#APP
# 127 "longacc.cpp" 1
	# myloop
# 0 "" 2
#NO_APP
	movsx	rax, r9d
	add	rax, 4
	mov	rbx, QWORD PTR [rbp-280]
	mov	rdx, QWORD PTR [rbx]
	vmovapd	ymm2, YMMWORD PTR [rdx+rax*8]
	vmovapd	ymm1, YMMWORD PTR [rdx-32+rax*8]
	lea	rax, [rbp-240]
	mov	edx, 0
	vxorpd	xmm9, xmm9, xmm9
.L778:
	mov	rcx, rax
	vmovapd	ymm3, YMMWORD PTR [rax+32]
	vaddpd	ymm0, ymm3, ymm1
	vandpd	ymm7, ymm3, ymm6
	vandpd	ymm8, ymm1, ymm6
	vcmppd	ymm7, ymm7, ymm8, 1
	vptest	ymm7, ymm1
	je	.L774
	vblendvpd	ymm8, ymm3, ymm1, ymm7
	vblendvpd	ymm1, ymm1, ymm3, ymm7
	vmovapd	ymm3, ymm8
.L774:
	vsubpd	ymm3, ymm3, ymm0
	vaddpd	ymm1, ymm3, ymm1
	vaddpd	ymm8, ymm0, ymm2
	vandpd	ymm3, ymm0, ymm6
	vandpd	ymm7, ymm2, ymm6
	vcmppd	ymm3, ymm3, ymm7, 1
	vptest	ymm3, ymm2
	je	.L775
	vblendvpd	ymm7, ymm0, ymm2, ymm3
	vblendvpd	ymm2, ymm2, ymm0, ymm3
	vmovapd	ymm0, ymm7
.L775:
	vsubpd	ymm0, ymm0, ymm8
	vaddpd	ymm2, ymm0, ymm2
	vmovapd	YMMWORD PTR [rcx+32], ymm8
	test	edx, edx
	je	.L776
	vorpd	ymm0, ymm1, ymm2
	vcmppd	ymm0, ymm0, ymm9, 4
	vtestpd	ymm0, ymm0
	je	.L780
.L776:
	add	edx, 1
	add	rax, 32
	cmp	edx, 3
	jne	.L778
	vmovupd	YMMWORD PTR [rbp-272], ymm1
	vzeroupper
	lea	r11, [rbp-272]
	vxorpd	xmm8, xmm8, xmm8
	vmovapd	xmm10, xmm8
	vmovapd	xmm3, xmm8
	vmovapd	xmm7, xmm8
	vmovapd	xmm11, xmm8
	vmovapd	xmm9, xmm8
.L789:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rbx, QWORD PTR [rbp-240]
	vucomisd	xmm0, xmm8
	jp	.L805
	vucomisd	xmm0, xmm9
	je	.L781
.L805:
	vmovq	rcx, xmm0
	vmovq	rsi, xmm0
	shr	rsi, 52
	and	esi, 2047
	sub	rsi, 1023
	mov	edx, -1840700269
	mov	eax, esi
	imul	edx
	add	edx, esi
	sar	edx, 5
	sar	esi, 31
	sub	edx, esi
	mov	esi, edx
	add	esi, DWORD PTR [rbx]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm10
	jp	.L783
	vucomisd	xmm0, xmm11
	je	.L781
.L783:
	mov	r13d, 256
	mov	r12, -256
.L817:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	rdi, xmm0
	movsx	rax, esi
	mov	rcx, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx+rax*8], rdx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L784
	mov	r10d, esi
.L787:
	add	rdi, rdx
	sar	rdi, 56
	test	rdx, rdx
	mov	rdx, r12
	cmovg	rdx, r13
	movsx	rcx, r10d
	sal	rcx, 3
	mov	r14, rcx
	add	r14, QWORD PTR [rbx+8]
	mov	rax, rdi
	sal	rax, 56
	neg	rax
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r14], rax
seto r15b
# 0 "" 2
#NO_APP
	add	rdi, rdx
	add	r10d, 1
	mov	eax, DWORD PTR [rbx+4]
	add	eax, DWORD PTR [rbx]
	cmp	r10d, eax
	jl	.L786
	mov	DWORD PTR [rbx+40], 4
	jmp	.L784
.L786:
	mov	rax, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax+8+rcx], rdx
seto r14b
# 0 "" 2
#NO_APP
	test	r14b, r14b
	jne	.L787
.L784:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm5
	sub	esi, 1
	vucomisd	xmm0, xmm3
	jp	.L817
	vucomisd	xmm0, xmm7
	jne	.L817
.L781:
	add	r11, 8
	cmp	r11, r8
	jne	.L789
	vmovupd	YMMWORD PTR [rbp-272], ymm2
	vzeroupper
	lea	r11, [rbp-272]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm9, xmm7
	vmovapd	xmm2, xmm7
	vmovapd	xmm3, xmm7
	vmovapd	xmm10, xmm7
	vmovapd	xmm8, xmm7
.L798:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rbx, QWORD PTR [rbp-240]
	vucomisd	xmm0, xmm7
	jp	.L806
	vucomisd	xmm0, xmm8
	je	.L790
.L806:
	vmovq	rcx, xmm0
	vmovq	rsi, xmm0
	shr	rsi, 52
	and	esi, 2047
	sub	rsi, 1023
	mov	edx, -1840700269
	mov	eax, esi
	imul	edx
	add	edx, esi
	sar	edx, 5
	sar	esi, 31
	sub	edx, esi
	mov	esi, edx
	add	esi, DWORD PTR [rbx]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm9
	jp	.L792
	vucomisd	xmm0, xmm10
	je	.L790
.L792:
	mov	r13d, 256
	mov	r12, -256
.L818:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	rdi, xmm0
	movsx	rax, esi
	mov	rcx, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx+rax*8], rdx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L793
	mov	r10d, esi
.L796:
	add	rdi, rdx
	sar	rdi, 56
	test	rdx, rdx
	mov	rdx, r12
	cmovg	rdx, r13
	movsx	rcx, r10d
	sal	rcx, 3
	mov	r14, rcx
	add	r14, QWORD PTR [rbx+8]
	mov	rax, rdi
	sal	rax, 56
	neg	rax
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r14], rax
seto r15b
# 0 "" 2
#NO_APP
	add	rdi, rdx
	add	r10d, 1
	mov	eax, DWORD PTR [rbx+4]
	add	eax, DWORD PTR [rbx]
	cmp	r10d, eax
	jl	.L795
	mov	DWORD PTR [rbx+40], 4
	jmp	.L793
.L795:
	mov	rax, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax+8+rcx], rdx
seto r14b
# 0 "" 2
#NO_APP
	test	r14b, r14b
	jne	.L796
.L793:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm4
	sub	esi, 1
	vucomisd	xmm0, xmm2
	jp	.L818
	vucomisd	xmm0, xmm3
	jne	.L818
.L790:
	add	r11, 8
	cmp	r11, r8
	jne	.L798
.L780:
	add	r9d, 8
	cmp	DWORD PTR [rbp-284], r9d
	jg	.L808
	jmp	.L799
.L804:
.L767:
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L801
	call	__stack_chk_fail
.L801:
	add	rsp, 288
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7604:
	.section	.gcc_except_table
.LLSDA7604:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7604-.LLSDACSB7604
.LLSDACSB7604:
.LLSDACSE7604:
	.text
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.1, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.1
	.section	.text._ZN15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,"axG",@progbits,_ZN15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,comdat
	.align 2
	.weak	_ZN15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.type	_ZN15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, @function
_ZN15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv:
.LFB7215:
	.cfi_startproc
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 64
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r12, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	lea	rbx, [rdi+32]
	lea	rax, [rdi+160]
	mov	QWORD PTR [rbp-112], rax
	lea	rax, [rbp-96]
	lea	r13, [rax+32]
	vxorpd	xmm6, xmm6, xmm6
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm6
	vmovapd	xmm4, xmm6
.L831:
	mov	QWORD PTR [rbp-104], rbx
	vmovapd	ymm0, YMMWORD PTR [rbx]
	vmovupd	YMMWORD PTR [rbp-96], ymm0
	vzeroupper
	lea	r9, [rbp-96]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm8, xmm7
	vmovapd	xmm5, xmm7
.L830:
	vmovsd	xmm0, QWORD PTR [r9]
	mov	rsi, QWORD PTR [r12]
	vucomisd	xmm0, xmm6
	jp	.L834
	vucomisd	xmm0, xmm5
	je	.L822
.L834:
	vmovq	rcx, xmm0
	vmovq	rdi, xmm0
	shr	rdi, 52
	and	edi, 2047
	sub	rdi, 1023
	mov	edx, -1840700269
	mov	eax, edi
	imul	edx
	add	edx, edi
	sar	edx, 5
	sar	edi, 31
	sub	edx, edi
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm7
	jp	.L824
	vucomisd	xmm0, xmm8
	je	.L822
.L824:
	mov	r14d, 256
.L838:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r8, xmm0
	movsx	rdx, eax
	mov	rdi, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdi+rdx*8], rcx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L825
	mov	r10d, eax
.L828:
	add	r8, rcx
	sar	r8, 56
	test	rcx, rcx
	mov	rcx, -256
	cmovg	rcx, r14
	movsx	rdi, r10d
	sal	rdi, 3
	mov	r11, rdi
	add	r11, QWORD PTR [rsi+8]
	mov	rdx, r8
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r11], rdx
seto r15b
# 0 "" 2
#NO_APP
	add	r8, rcx
	add	r10d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r10d, edx
	jl	.L827
	mov	DWORD PTR [rsi+40], 4
	jmp	.L825
.L827:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+rdi], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	jne	.L828
.L825:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L838
	vucomisd	xmm0, xmm4
	jne	.L838
.L822:
	add	r9, 8
	cmp	r9, r13
	jne	.L830
	vxorpd	xmm0, xmm0, xmm0
	mov	rax, QWORD PTR [rbp-104]
	vmovapd	YMMWORD PTR [rax], ymm0
	add	rbx, 32
	cmp	rbx, QWORD PTR [rbp-112]
	jne	.L831
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L832
	call	__stack_chk_fail
.L832:
	add	rsp, 64
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7215:
	.size	_ZN15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, .-_ZN15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.text
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.2, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.2:
.LFB7605:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7605
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 320
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r15, rdi
	mov	QWORD PTR [rbp-312], rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	call	omp_get_thread_num
	mov	ebx, eax
	mov	DWORD PTR [rbp-320], eax
	call	omp_get_num_threads
	mov	r14d, eax
	mov	DWORD PTR [rbp-332], eax
	mov	edx, ebx
	lea	rax, [0+rdx*8]
	mov	rcx, rdx
	sal	rcx, 6
	sub	rcx, rax
	mov	QWORD PTR [rbp-352], rcx
	mov	QWORD PTR [rbp-344], rcx
	mov	rax, QWORD PTR [r15+8]
	add	rcx, QWORD PTR [rax]
	mov	QWORD PTR [rbp-272], rcx
	vxorpd	xmm0, xmm0, xmm0
	vmovapd	YMMWORD PTR [rbp-112], ymm0
	vmovapd	YMMWORD PTR [rbp-240], ymm0
	vmovapd	YMMWORD PTR [rbp-208], ymm0
	vmovapd	YMMWORD PTR [rbp-176], ymm0
	vmovapd	YMMWORD PTR [rbp-144], ymm0
	mov	eax, ebx
	sal	eax, 4
	mov	eax, eax
	lea	rcx, [0+rax*4]
	mov	QWORD PTR [rbp-328], rcx
	mov	rax, QWORD PTR [r15+16]
	add	rcx, QWORD PTR [rax]
	mov	DWORD PTR [rcx], 0
	movsx	rsi, DWORD PTR [r15+24]
	mov	ecx, r14d
	mov	rax, rdx
	imul	rax, rsi
	cqo
	idiv	rcx
	mov	r9d, eax
	and	r9d, -8
	mov	eax, ebx
	add	eax, 1
	imul	rax, rsi
	cqo
	idiv	rcx
	and	eax, -8
	sub	eax, 1
	mov	DWORD PTR [rbp-316], eax
	cmp	r9d, eax
	jl	.L841
.L872:
	lea	rdi, [rbp-272]
	call	_ZN15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	mov	rbx, QWORD PTR [rbp-312]
	mov	rax, QWORD PTR [rbx+8]
	mov	rdi, QWORD PTR [rbp-352]
	add	rdi, QWORD PTR [rax]
	call	_ZN16Superaccumulator9NormalizeEv
	mov	r14, QWORD PTR [rbx+8]
	mov	r13, QWORD PTR [rbx+16]
	mov	eax, DWORD PTR [rbp-332]
	cmp	eax, 1
	jbe	.L840
	mov	ebx, 1
	mov	edx, 1
	mov	r15d, 1
	mov	QWORD PTR [rbp-312], r14
	mov	r14d, eax
.L846:
	mov	rcx, QWORD PTR [rbp-328]
	add	rcx, QWORD PTR [r13+0]
	mov	eax, DWORD PTR [rcx]
	add	eax, 1
	mov	DWORD PTR [rcx], eax
	mov	r8d, ebx
	mov	r12d, r15d
	mov	ecx, ebx
	sal	r12d, cl
	lea	eax, [r12-1]
	test	DWORD PTR [rbp-320], eax
	jne	.L843
	or	edx, DWORD PTR [rbp-320]
	cmp	r14d, edx
	jbe	.L843
	mov	eax, edx
	sal	eax, 4
	mov	eax, eax
	mov	rcx, QWORD PTR [r13+0]
	lea	rcx, [rcx+rax*4]
	mov	rax, QWORD PTR [rbp-312]
	mov	rax, QWORD PTR [rax]
	mov	edx, edx
	lea	rsi, [0+rdx*8]
	sal	rdx, 6
	sub	rdx, rsi
	mov	rsi, rdx
	add	rsi, rax
	mov	rdi, QWORD PTR [rbp-344]
	add	rdi, rax
	prefetcht0	[rcx]
	mov	eax, DWORD PTR [rcx]
	cmp	ebx, eax
	jle	.L844
.L880:
	rep nop
	mov	eax, DWORD PTR [rcx]
	cmp	r8d, eax
	jg	.L880
.L844:
	call	_ZN16Superaccumulator10AccumulateERS_
.L843:
	mov	edx, r12d
	add	ebx, 1
	cmp	r14d, r12d
	ja	.L846
	jmp	.L840
.L841:
	vmovapd	ymm6, YMMWORD PTR _ZZ10constant8fILin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647EEDv8_fvE1u[rip]
	lea	rax, [rbp-304]
	lea	r8, [rax+32]
	vmovsd	xmm5, QWORD PTR .LC15[rip]
	vmovapd	xmm4, xmm5
.L881:
#APP
# 127 "longacc.cpp" 1
	# myloop
# 0 "" 2
#NO_APP
	movsx	rax, r9d
	add	rax, 4
	mov	rbx, QWORD PTR [rbp-312]
	mov	rdx, QWORD PTR [rbx]
	vmovapd	ymm2, YMMWORD PTR [rdx+rax*8]
	vmovapd	ymm1, YMMWORD PTR [rdx-32+rax*8]
	lea	rax, [rbp-272]
	mov	edx, 0
	vxorpd	xmm9, xmm9, xmm9
.L851:
	mov	rcx, rax
	vmovapd	ymm3, YMMWORD PTR [rax+32]
	vaddpd	ymm0, ymm3, ymm1
	vandpd	ymm7, ymm3, ymm6
	vandpd	ymm8, ymm1, ymm6
	vcmppd	ymm7, ymm7, ymm8, 1
	vptest	ymm7, ymm1
	je	.L847
	vblendvpd	ymm8, ymm3, ymm1, ymm7
	vblendvpd	ymm1, ymm1, ymm3, ymm7
	vmovapd	ymm3, ymm8
.L847:
	vsubpd	ymm3, ymm3, ymm0
	vaddpd	ymm1, ymm3, ymm1
	vaddpd	ymm8, ymm0, ymm2
	vandpd	ymm3, ymm0, ymm6
	vandpd	ymm7, ymm2, ymm6
	vcmppd	ymm3, ymm3, ymm7, 1
	vptest	ymm3, ymm2
	je	.L848
	vblendvpd	ymm7, ymm0, ymm2, ymm3
	vblendvpd	ymm2, ymm2, ymm0, ymm3
	vmovapd	ymm0, ymm7
.L848:
	vsubpd	ymm0, ymm0, ymm8
	vaddpd	ymm2, ymm0, ymm2
	vmovapd	YMMWORD PTR [rcx+32], ymm8
	test	edx, edx
	je	.L849
	vorpd	ymm0, ymm1, ymm2
	vcmppd	ymm0, ymm0, ymm9, 4
	vtestpd	ymm0, ymm0
	je	.L853
.L849:
	add	edx, 1
	add	rax, 32
	cmp	edx, 4
	jne	.L851
	vmovupd	YMMWORD PTR [rbp-304], ymm1
	vzeroupper
	lea	r11, [rbp-304]
	vxorpd	xmm8, xmm8, xmm8
	vmovapd	xmm10, xmm8
	vmovapd	xmm3, xmm8
	vmovapd	xmm7, xmm8
	vmovapd	xmm11, xmm8
	vmovapd	xmm9, xmm8
.L862:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rbx, QWORD PTR [rbp-272]
	vucomisd	xmm0, xmm8
	jp	.L878
	vucomisd	xmm0, xmm9
	je	.L854
.L878:
	vmovq	rcx, xmm0
	vmovq	rsi, xmm0
	shr	rsi, 52
	and	esi, 2047
	sub	rsi, 1023
	mov	edx, -1840700269
	mov	eax, esi
	imul	edx
	add	edx, esi
	sar	edx, 5
	sar	esi, 31
	sub	edx, esi
	mov	esi, edx
	add	esi, DWORD PTR [rbx]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm10
	jp	.L856
	vucomisd	xmm0, xmm11
	je	.L854
.L856:
	mov	r13d, 256
	mov	r12, -256
.L890:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	rdi, xmm0
	movsx	rax, esi
	mov	rcx, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx+rax*8], rdx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L857
	mov	r10d, esi
.L860:
	add	rdi, rdx
	sar	rdi, 56
	test	rdx, rdx
	mov	rdx, r12
	cmovg	rdx, r13
	movsx	rcx, r10d
	sal	rcx, 3
	mov	r14, rcx
	add	r14, QWORD PTR [rbx+8]
	mov	rax, rdi
	sal	rax, 56
	neg	rax
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r14], rax
seto r15b
# 0 "" 2
#NO_APP
	add	rdi, rdx
	add	r10d, 1
	mov	eax, DWORD PTR [rbx+4]
	add	eax, DWORD PTR [rbx]
	cmp	r10d, eax
	jl	.L859
	mov	DWORD PTR [rbx+40], 4
	jmp	.L857
.L859:
	mov	rax, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax+8+rcx], rdx
seto r14b
# 0 "" 2
#NO_APP
	test	r14b, r14b
	jne	.L860
.L857:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm5
	sub	esi, 1
	vucomisd	xmm0, xmm3
	jp	.L890
	vucomisd	xmm0, xmm7
	jne	.L890
.L854:
	add	r11, 8
	cmp	r11, r8
	jne	.L862
	vmovupd	YMMWORD PTR [rbp-304], ymm2
	vzeroupper
	lea	r11, [rbp-304]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm9, xmm7
	vmovapd	xmm2, xmm7
	vmovapd	xmm3, xmm7
	vmovapd	xmm10, xmm7
	vmovapd	xmm8, xmm7
.L871:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rbx, QWORD PTR [rbp-272]
	vucomisd	xmm0, xmm7
	jp	.L879
	vucomisd	xmm0, xmm8
	je	.L863
.L879:
	vmovq	rcx, xmm0
	vmovq	rsi, xmm0
	shr	rsi, 52
	and	esi, 2047
	sub	rsi, 1023
	mov	edx, -1840700269
	mov	eax, esi
	imul	edx
	add	edx, esi
	sar	edx, 5
	sar	esi, 31
	sub	edx, esi
	mov	esi, edx
	add	esi, DWORD PTR [rbx]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm9
	jp	.L865
	vucomisd	xmm0, xmm10
	je	.L863
.L865:
	mov	r13d, 256
	mov	r12, -256
.L891:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	rdi, xmm0
	movsx	rax, esi
	mov	rcx, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx+rax*8], rdx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L866
	mov	r10d, esi
.L869:
	add	rdi, rdx
	sar	rdi, 56
	test	rdx, rdx
	mov	rdx, r12
	cmovg	rdx, r13
	movsx	rcx, r10d
	sal	rcx, 3
	mov	r14, rcx
	add	r14, QWORD PTR [rbx+8]
	mov	rax, rdi
	sal	rax, 56
	neg	rax
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r14], rax
seto r15b
# 0 "" 2
#NO_APP
	add	rdi, rdx
	add	r10d, 1
	mov	eax, DWORD PTR [rbx+4]
	add	eax, DWORD PTR [rbx]
	cmp	r10d, eax
	jl	.L868
	mov	DWORD PTR [rbx+40], 4
	jmp	.L866
.L868:
	mov	rax, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax+8+rcx], rdx
seto r14b
# 0 "" 2
#NO_APP
	test	r14b, r14b
	jne	.L869
.L866:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm4
	sub	esi, 1
	vucomisd	xmm0, xmm2
	jp	.L891
	vucomisd	xmm0, xmm3
	jne	.L891
.L863:
	add	r11, 8
	cmp	r11, r8
	jne	.L871
.L853:
	add	r9d, 8
	cmp	DWORD PTR [rbp-316], r9d
	jg	.L881
	jmp	.L872
.L877:
.L840:
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L874
	call	__stack_chk_fail
.L874:
	add	rsp, 320
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7605:
	.section	.gcc_except_table
.LLSDA7605:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7605-.LLSDACSB7605
.LLSDACSB7605:
.LLSDACSE7605:
	.text
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.2, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.2
	.section	.text._ZN15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,"axG",@progbits,_ZN15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,comdat
	.align 2
	.weak	_ZN15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.type	_ZN15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, @function
_ZN15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv:
.LFB7220:
	.cfi_startproc
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 64
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r12, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	lea	rbx, [rdi+32]
	lea	rax, [rdi+192]
	mov	QWORD PTR [rbp-112], rax
	lea	rax, [rbp-96]
	lea	r13, [rax+32]
	vxorpd	xmm6, xmm6, xmm6
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm6
	vmovapd	xmm4, xmm6
.L904:
	mov	QWORD PTR [rbp-104], rbx
	vmovapd	ymm0, YMMWORD PTR [rbx]
	vmovupd	YMMWORD PTR [rbp-96], ymm0
	vzeroupper
	lea	r9, [rbp-96]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm8, xmm7
	vmovapd	xmm5, xmm7
.L903:
	vmovsd	xmm0, QWORD PTR [r9]
	mov	rsi, QWORD PTR [r12]
	vucomisd	xmm0, xmm6
	jp	.L907
	vucomisd	xmm0, xmm5
	je	.L895
.L907:
	vmovq	rcx, xmm0
	vmovq	rdi, xmm0
	shr	rdi, 52
	and	edi, 2047
	sub	rdi, 1023
	mov	edx, -1840700269
	mov	eax, edi
	imul	edx
	add	edx, edi
	sar	edx, 5
	sar	edi, 31
	sub	edx, edi
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm7
	jp	.L897
	vucomisd	xmm0, xmm8
	je	.L895
.L897:
	mov	r14d, 256
.L911:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r8, xmm0
	movsx	rdx, eax
	mov	rdi, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdi+rdx*8], rcx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L898
	mov	r10d, eax
.L901:
	add	r8, rcx
	sar	r8, 56
	test	rcx, rcx
	mov	rcx, -256
	cmovg	rcx, r14
	movsx	rdi, r10d
	sal	rdi, 3
	mov	r11, rdi
	add	r11, QWORD PTR [rsi+8]
	mov	rdx, r8
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r11], rdx
seto r15b
# 0 "" 2
#NO_APP
	add	r8, rcx
	add	r10d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r10d, edx
	jl	.L900
	mov	DWORD PTR [rsi+40], 4
	jmp	.L898
.L900:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+rdi], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	jne	.L901
.L898:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L911
	vucomisd	xmm0, xmm4
	jne	.L911
.L895:
	add	r9, 8
	cmp	r9, r13
	jne	.L903
	vxorpd	xmm0, xmm0, xmm0
	mov	rax, QWORD PTR [rbp-104]
	vmovapd	YMMWORD PTR [rax], ymm0
	add	rbx, 32
	cmp	rbx, QWORD PTR [rbp-112]
	jne	.L904
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L905
	call	__stack_chk_fail
.L905:
	add	rsp, 64
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7220:
	.size	_ZN15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, .-_ZN15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.text
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.3, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.3:
.LFB7606:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7606
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 352
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r15, rdi
	mov	QWORD PTR [rbp-344], rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	call	omp_get_thread_num
	mov	ebx, eax
	mov	DWORD PTR [rbp-352], eax
	call	omp_get_num_threads
	mov	r14d, eax
	mov	DWORD PTR [rbp-364], eax
	mov	edx, ebx
	lea	rax, [0+rdx*8]
	mov	rcx, rdx
	sal	rcx, 6
	sub	rcx, rax
	mov	QWORD PTR [rbp-384], rcx
	mov	QWORD PTR [rbp-376], rcx
	mov	rax, QWORD PTR [r15+8]
	add	rcx, QWORD PTR [rax]
	mov	QWORD PTR [rbp-304], rcx
	vxorpd	xmm0, xmm0, xmm0
	vmovapd	YMMWORD PTR [rbp-112], ymm0
	vmovapd	YMMWORD PTR [rbp-272], ymm0
	vmovapd	YMMWORD PTR [rbp-240], ymm0
	vmovapd	YMMWORD PTR [rbp-208], ymm0
	vmovapd	YMMWORD PTR [rbp-176], ymm0
	vmovapd	YMMWORD PTR [rbp-144], ymm0
	mov	eax, ebx
	sal	eax, 4
	mov	eax, eax
	lea	rcx, [0+rax*4]
	mov	QWORD PTR [rbp-360], rcx
	mov	rax, QWORD PTR [r15+16]
	add	rcx, QWORD PTR [rax]
	mov	DWORD PTR [rcx], 0
	movsx	rsi, DWORD PTR [r15+24]
	mov	ecx, r14d
	mov	rax, rdx
	imul	rax, rsi
	cqo
	idiv	rcx
	mov	r9d, eax
	and	r9d, -8
	mov	eax, ebx
	add	eax, 1
	imul	rax, rsi
	cqo
	idiv	rcx
	and	eax, -8
	sub	eax, 1
	mov	DWORD PTR [rbp-348], eax
	cmp	r9d, eax
	jl	.L914
.L945:
	lea	rdi, [rbp-304]
	call	_ZN15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	mov	rbx, QWORD PTR [rbp-344]
	mov	rax, QWORD PTR [rbx+8]
	mov	rdi, QWORD PTR [rbp-384]
	add	rdi, QWORD PTR [rax]
	call	_ZN16Superaccumulator9NormalizeEv
	mov	r14, QWORD PTR [rbx+8]
	mov	r13, QWORD PTR [rbx+16]
	mov	eax, DWORD PTR [rbp-364]
	cmp	eax, 1
	jbe	.L913
	mov	ebx, 1
	mov	edx, 1
	mov	r15d, 1
	mov	QWORD PTR [rbp-344], r14
	mov	r14d, eax
.L919:
	mov	rcx, QWORD PTR [rbp-360]
	add	rcx, QWORD PTR [r13+0]
	mov	eax, DWORD PTR [rcx]
	add	eax, 1
	mov	DWORD PTR [rcx], eax
	mov	r8d, ebx
	mov	r12d, r15d
	mov	ecx, ebx
	sal	r12d, cl
	lea	eax, [r12-1]
	test	DWORD PTR [rbp-352], eax
	jne	.L916
	or	edx, DWORD PTR [rbp-352]
	cmp	r14d, edx
	jbe	.L916
	mov	eax, edx
	sal	eax, 4
	mov	eax, eax
	mov	rcx, QWORD PTR [r13+0]
	lea	rcx, [rcx+rax*4]
	mov	rax, QWORD PTR [rbp-344]
	mov	rax, QWORD PTR [rax]
	mov	edx, edx
	lea	rsi, [0+rdx*8]
	sal	rdx, 6
	sub	rdx, rsi
	mov	rsi, rdx
	add	rsi, rax
	mov	rdi, QWORD PTR [rbp-376]
	add	rdi, rax
	prefetcht0	[rcx]
	mov	eax, DWORD PTR [rcx]
	cmp	ebx, eax
	jle	.L917
.L953:
	rep nop
	mov	eax, DWORD PTR [rcx]
	cmp	r8d, eax
	jg	.L953
.L917:
	call	_ZN16Superaccumulator10AccumulateERS_
.L916:
	mov	edx, r12d
	add	ebx, 1
	cmp	r14d, r12d
	ja	.L919
	jmp	.L913
.L914:
	vmovapd	ymm6, YMMWORD PTR _ZZ10constant8fILin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647EEDv8_fvE1u[rip]
	lea	rax, [rbp-336]
	lea	r8, [rax+32]
	vmovsd	xmm5, QWORD PTR .LC15[rip]
	vmovapd	xmm4, xmm5
.L954:
#APP
# 127 "longacc.cpp" 1
	# myloop
# 0 "" 2
#NO_APP
	movsx	rax, r9d
	add	rax, 4
	mov	rbx, QWORD PTR [rbp-344]
	mov	rdx, QWORD PTR [rbx]
	vmovapd	ymm2, YMMWORD PTR [rdx+rax*8]
	vmovapd	ymm1, YMMWORD PTR [rdx-32+rax*8]
	lea	rax, [rbp-304]
	mov	edx, 0
	vxorpd	xmm9, xmm9, xmm9
.L924:
	mov	rcx, rax
	vmovapd	ymm3, YMMWORD PTR [rax+32]
	vaddpd	ymm0, ymm3, ymm1
	vandpd	ymm7, ymm3, ymm6
	vandpd	ymm8, ymm1, ymm6
	vcmppd	ymm7, ymm7, ymm8, 1
	vptest	ymm7, ymm1
	je	.L920
	vblendvpd	ymm8, ymm3, ymm1, ymm7
	vblendvpd	ymm1, ymm1, ymm3, ymm7
	vmovapd	ymm3, ymm8
.L920:
	vsubpd	ymm3, ymm3, ymm0
	vaddpd	ymm1, ymm3, ymm1
	vaddpd	ymm8, ymm0, ymm2
	vandpd	ymm3, ymm0, ymm6
	vandpd	ymm7, ymm2, ymm6
	vcmppd	ymm3, ymm3, ymm7, 1
	vptest	ymm3, ymm2
	je	.L921
	vblendvpd	ymm7, ymm0, ymm2, ymm3
	vblendvpd	ymm2, ymm2, ymm0, ymm3
	vmovapd	ymm0, ymm7
.L921:
	vsubpd	ymm0, ymm0, ymm8
	vaddpd	ymm2, ymm0, ymm2
	vmovapd	YMMWORD PTR [rcx+32], ymm8
	test	edx, edx
	je	.L922
	vorpd	ymm0, ymm1, ymm2
	vcmppd	ymm0, ymm0, ymm9, 4
	vtestpd	ymm0, ymm0
	je	.L926
.L922:
	add	edx, 1
	add	rax, 32
	cmp	edx, 5
	jne	.L924
	vmovupd	YMMWORD PTR [rbp-336], ymm1
	vzeroupper
	lea	r11, [rbp-336]
	vxorpd	xmm8, xmm8, xmm8
	vmovapd	xmm10, xmm8
	vmovapd	xmm3, xmm8
	vmovapd	xmm7, xmm8
	vmovapd	xmm11, xmm8
	vmovapd	xmm9, xmm8
.L935:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rbx, QWORD PTR [rbp-304]
	vucomisd	xmm0, xmm8
	jp	.L951
	vucomisd	xmm0, xmm9
	je	.L927
.L951:
	vmovq	rcx, xmm0
	vmovq	rsi, xmm0
	shr	rsi, 52
	and	esi, 2047
	sub	rsi, 1023
	mov	edx, -1840700269
	mov	eax, esi
	imul	edx
	add	edx, esi
	sar	edx, 5
	sar	esi, 31
	sub	edx, esi
	mov	esi, edx
	add	esi, DWORD PTR [rbx]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm10
	jp	.L929
	vucomisd	xmm0, xmm11
	je	.L927
.L929:
	mov	r13d, 256
	mov	r12, -256
.L963:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	rdi, xmm0
	movsx	rax, esi
	mov	rcx, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx+rax*8], rdx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L930
	mov	r10d, esi
.L933:
	add	rdi, rdx
	sar	rdi, 56
	test	rdx, rdx
	mov	rdx, r12
	cmovg	rdx, r13
	movsx	rcx, r10d
	sal	rcx, 3
	mov	r14, rcx
	add	r14, QWORD PTR [rbx+8]
	mov	rax, rdi
	sal	rax, 56
	neg	rax
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r14], rax
seto r15b
# 0 "" 2
#NO_APP
	add	rdi, rdx
	add	r10d, 1
	mov	eax, DWORD PTR [rbx+4]
	add	eax, DWORD PTR [rbx]
	cmp	r10d, eax
	jl	.L932
	mov	DWORD PTR [rbx+40], 4
	jmp	.L930
.L932:
	mov	rax, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax+8+rcx], rdx
seto r14b
# 0 "" 2
#NO_APP
	test	r14b, r14b
	jne	.L933
.L930:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm5
	sub	esi, 1
	vucomisd	xmm0, xmm3
	jp	.L963
	vucomisd	xmm0, xmm7
	jne	.L963
.L927:
	add	r11, 8
	cmp	r11, r8
	jne	.L935
	vmovupd	YMMWORD PTR [rbp-336], ymm2
	vzeroupper
	lea	r11, [rbp-336]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm9, xmm7
	vmovapd	xmm2, xmm7
	vmovapd	xmm3, xmm7
	vmovapd	xmm10, xmm7
	vmovapd	xmm8, xmm7
.L944:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rbx, QWORD PTR [rbp-304]
	vucomisd	xmm0, xmm7
	jp	.L952
	vucomisd	xmm0, xmm8
	je	.L936
.L952:
	vmovq	rcx, xmm0
	vmovq	rsi, xmm0
	shr	rsi, 52
	and	esi, 2047
	sub	rsi, 1023
	mov	edx, -1840700269
	mov	eax, esi
	imul	edx
	add	edx, esi
	sar	edx, 5
	sar	esi, 31
	sub	edx, esi
	mov	esi, edx
	add	esi, DWORD PTR [rbx]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm9
	jp	.L938
	vucomisd	xmm0, xmm10
	je	.L936
.L938:
	mov	r13d, 256
	mov	r12, -256
.L964:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	rdi, xmm0
	movsx	rax, esi
	mov	rcx, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx+rax*8], rdx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L939
	mov	r10d, esi
.L942:
	add	rdi, rdx
	sar	rdi, 56
	test	rdx, rdx
	mov	rdx, r12
	cmovg	rdx, r13
	movsx	rcx, r10d
	sal	rcx, 3
	mov	r14, rcx
	add	r14, QWORD PTR [rbx+8]
	mov	rax, rdi
	sal	rax, 56
	neg	rax
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r14], rax
seto r15b
# 0 "" 2
#NO_APP
	add	rdi, rdx
	add	r10d, 1
	mov	eax, DWORD PTR [rbx+4]
	add	eax, DWORD PTR [rbx]
	cmp	r10d, eax
	jl	.L941
	mov	DWORD PTR [rbx+40], 4
	jmp	.L939
.L941:
	mov	rax, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax+8+rcx], rdx
seto r14b
# 0 "" 2
#NO_APP
	test	r14b, r14b
	jne	.L942
.L939:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm4
	sub	esi, 1
	vucomisd	xmm0, xmm2
	jp	.L964
	vucomisd	xmm0, xmm3
	jne	.L964
.L936:
	add	r11, 8
	cmp	r11, r8
	jne	.L944
.L926:
	add	r9d, 8
	cmp	DWORD PTR [rbp-348], r9d
	jg	.L954
	jmp	.L945
.L950:
.L913:
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L947
	call	__stack_chk_fail
.L947:
	add	rsp, 352
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7606:
	.section	.gcc_except_table
.LLSDA7606:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7606-.LLSDACSB7606
.LLSDACSB7606:
.LLSDACSE7606:
	.text
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.3, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.3
	.section	.text._ZN15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,"axG",@progbits,_ZN15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,comdat
	.align 2
	.weak	_ZN15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.type	_ZN15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, @function
_ZN15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv:
.LFB7225:
	.cfi_startproc
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 64
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r12, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	lea	rbx, [rdi+32]
	lea	rax, [rdi+224]
	mov	QWORD PTR [rbp-112], rax
	lea	rax, [rbp-96]
	lea	r13, [rax+32]
	vxorpd	xmm6, xmm6, xmm6
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm6
	vmovapd	xmm4, xmm6
.L977:
	mov	QWORD PTR [rbp-104], rbx
	vmovapd	ymm0, YMMWORD PTR [rbx]
	vmovupd	YMMWORD PTR [rbp-96], ymm0
	vzeroupper
	lea	r9, [rbp-96]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm8, xmm7
	vmovapd	xmm5, xmm7
.L976:
	vmovsd	xmm0, QWORD PTR [r9]
	mov	rsi, QWORD PTR [r12]
	vucomisd	xmm0, xmm6
	jp	.L980
	vucomisd	xmm0, xmm5
	je	.L968
.L980:
	vmovq	rcx, xmm0
	vmovq	rdi, xmm0
	shr	rdi, 52
	and	edi, 2047
	sub	rdi, 1023
	mov	edx, -1840700269
	mov	eax, edi
	imul	edx
	add	edx, edi
	sar	edx, 5
	sar	edi, 31
	sub	edx, edi
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm7
	jp	.L970
	vucomisd	xmm0, xmm8
	je	.L968
.L970:
	mov	r14d, 256
.L984:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r8, xmm0
	movsx	rdx, eax
	mov	rdi, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdi+rdx*8], rcx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L971
	mov	r10d, eax
.L974:
	add	r8, rcx
	sar	r8, 56
	test	rcx, rcx
	mov	rcx, -256
	cmovg	rcx, r14
	movsx	rdi, r10d
	sal	rdi, 3
	mov	r11, rdi
	add	r11, QWORD PTR [rsi+8]
	mov	rdx, r8
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r11], rdx
seto r15b
# 0 "" 2
#NO_APP
	add	r8, rcx
	add	r10d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r10d, edx
	jl	.L973
	mov	DWORD PTR [rsi+40], 4
	jmp	.L971
.L973:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+rdi], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	jne	.L974
.L971:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L984
	vucomisd	xmm0, xmm4
	jne	.L984
.L968:
	add	r9, 8
	cmp	r9, r13
	jne	.L976
	vxorpd	xmm0, xmm0, xmm0
	mov	rax, QWORD PTR [rbp-104]
	vmovapd	YMMWORD PTR [rax], ymm0
	add	rbx, 32
	cmp	rbx, QWORD PTR [rbp-112]
	jne	.L977
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L978
	call	__stack_chk_fail
.L978:
	add	rsp, 64
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7225:
	.size	_ZN15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, .-_ZN15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.text
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.4, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.4:
.LFB7607:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7607
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 384
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r15, rdi
	mov	QWORD PTR [rbp-376], rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	call	omp_get_thread_num
	mov	ebx, eax
	mov	DWORD PTR [rbp-384], eax
	call	omp_get_num_threads
	mov	r14d, eax
	mov	DWORD PTR [rbp-396], eax
	mov	edx, ebx
	lea	rax, [0+rdx*8]
	mov	rcx, rdx
	sal	rcx, 6
	sub	rcx, rax
	mov	QWORD PTR [rbp-416], rcx
	mov	QWORD PTR [rbp-408], rcx
	mov	rax, QWORD PTR [r15+8]
	add	rcx, QWORD PTR [rax]
	mov	QWORD PTR [rbp-336], rcx
	vxorpd	xmm0, xmm0, xmm0
	vmovapd	YMMWORD PTR [rbp-112], ymm0
	vmovapd	YMMWORD PTR [rbp-304], ymm0
	vmovapd	YMMWORD PTR [rbp-272], ymm0
	vmovapd	YMMWORD PTR [rbp-240], ymm0
	vmovapd	YMMWORD PTR [rbp-208], ymm0
	vmovapd	YMMWORD PTR [rbp-176], ymm0
	vmovapd	YMMWORD PTR [rbp-144], ymm0
	mov	eax, ebx
	sal	eax, 4
	mov	eax, eax
	lea	rcx, [0+rax*4]
	mov	QWORD PTR [rbp-392], rcx
	mov	rax, QWORD PTR [r15+16]
	add	rcx, QWORD PTR [rax]
	mov	DWORD PTR [rcx], 0
	movsx	rsi, DWORD PTR [r15+24]
	mov	ecx, r14d
	mov	rax, rdx
	imul	rax, rsi
	cqo
	idiv	rcx
	mov	r9d, eax
	and	r9d, -8
	mov	eax, ebx
	add	eax, 1
	imul	rax, rsi
	cqo
	idiv	rcx
	and	eax, -8
	sub	eax, 1
	mov	DWORD PTR [rbp-380], eax
	cmp	r9d, eax
	jl	.L987
.L1018:
	lea	rdi, [rbp-336]
	call	_ZN15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	mov	rbx, QWORD PTR [rbp-376]
	mov	rax, QWORD PTR [rbx+8]
	mov	rdi, QWORD PTR [rbp-416]
	add	rdi, QWORD PTR [rax]
	call	_ZN16Superaccumulator9NormalizeEv
	mov	r14, QWORD PTR [rbx+8]
	mov	r13, QWORD PTR [rbx+16]
	mov	eax, DWORD PTR [rbp-396]
	cmp	eax, 1
	jbe	.L986
	mov	ebx, 1
	mov	edx, 1
	mov	r15d, 1
	mov	QWORD PTR [rbp-376], r14
	mov	r14d, eax
.L992:
	mov	rcx, QWORD PTR [rbp-392]
	add	rcx, QWORD PTR [r13+0]
	mov	eax, DWORD PTR [rcx]
	add	eax, 1
	mov	DWORD PTR [rcx], eax
	mov	r8d, ebx
	mov	r12d, r15d
	mov	ecx, ebx
	sal	r12d, cl
	lea	eax, [r12-1]
	test	DWORD PTR [rbp-384], eax
	jne	.L989
	or	edx, DWORD PTR [rbp-384]
	cmp	r14d, edx
	jbe	.L989
	mov	eax, edx
	sal	eax, 4
	mov	eax, eax
	mov	rcx, QWORD PTR [r13+0]
	lea	rcx, [rcx+rax*4]
	mov	rax, QWORD PTR [rbp-376]
	mov	rax, QWORD PTR [rax]
	mov	edx, edx
	lea	rsi, [0+rdx*8]
	sal	rdx, 6
	sub	rdx, rsi
	mov	rsi, rdx
	add	rsi, rax
	mov	rdi, QWORD PTR [rbp-408]
	add	rdi, rax
	prefetcht0	[rcx]
	mov	eax, DWORD PTR [rcx]
	cmp	ebx, eax
	jle	.L990
.L1026:
	rep nop
	mov	eax, DWORD PTR [rcx]
	cmp	r8d, eax
	jg	.L1026
.L990:
	call	_ZN16Superaccumulator10AccumulateERS_
.L989:
	mov	edx, r12d
	add	ebx, 1
	cmp	r14d, r12d
	ja	.L992
	jmp	.L986
.L987:
	vmovapd	ymm6, YMMWORD PTR _ZZ10constant8fILin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647EEDv8_fvE1u[rip]
	lea	rax, [rbp-368]
	lea	r8, [rax+32]
	vmovsd	xmm5, QWORD PTR .LC15[rip]
	vmovapd	xmm4, xmm5
.L1027:
#APP
# 127 "longacc.cpp" 1
	# myloop
# 0 "" 2
#NO_APP
	movsx	rax, r9d
	add	rax, 4
	mov	rbx, QWORD PTR [rbp-376]
	mov	rdx, QWORD PTR [rbx]
	vmovapd	ymm2, YMMWORD PTR [rdx+rax*8]
	vmovapd	ymm1, YMMWORD PTR [rdx-32+rax*8]
	lea	rax, [rbp-336]
	mov	edx, 0
	vxorpd	xmm9, xmm9, xmm9
.L997:
	mov	rcx, rax
	vmovapd	ymm3, YMMWORD PTR [rax+32]
	vaddpd	ymm0, ymm3, ymm1
	vandpd	ymm7, ymm3, ymm6
	vandpd	ymm8, ymm1, ymm6
	vcmppd	ymm7, ymm7, ymm8, 1
	vptest	ymm7, ymm1
	je	.L993
	vblendvpd	ymm8, ymm3, ymm1, ymm7
	vblendvpd	ymm1, ymm1, ymm3, ymm7
	vmovapd	ymm3, ymm8
.L993:
	vsubpd	ymm3, ymm3, ymm0
	vaddpd	ymm1, ymm3, ymm1
	vaddpd	ymm8, ymm0, ymm2
	vandpd	ymm3, ymm0, ymm6
	vandpd	ymm7, ymm2, ymm6
	vcmppd	ymm3, ymm3, ymm7, 1
	vptest	ymm3, ymm2
	je	.L994
	vblendvpd	ymm7, ymm0, ymm2, ymm3
	vblendvpd	ymm2, ymm2, ymm0, ymm3
	vmovapd	ymm0, ymm7
.L994:
	vsubpd	ymm0, ymm0, ymm8
	vaddpd	ymm2, ymm0, ymm2
	vmovapd	YMMWORD PTR [rcx+32], ymm8
	test	edx, edx
	je	.L995
	vorpd	ymm0, ymm1, ymm2
	vcmppd	ymm0, ymm0, ymm9, 4
	vtestpd	ymm0, ymm0
	je	.L999
.L995:
	add	edx, 1
	add	rax, 32
	cmp	edx, 6
	jne	.L997
	vmovupd	YMMWORD PTR [rbp-368], ymm1
	vzeroupper
	lea	r11, [rbp-368]
	vxorpd	xmm8, xmm8, xmm8
	vmovapd	xmm10, xmm8
	vmovapd	xmm3, xmm8
	vmovapd	xmm7, xmm8
	vmovapd	xmm11, xmm8
	vmovapd	xmm9, xmm8
.L1008:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rbx, QWORD PTR [rbp-336]
	vucomisd	xmm0, xmm8
	jp	.L1024
	vucomisd	xmm0, xmm9
	je	.L1000
.L1024:
	vmovq	rcx, xmm0
	vmovq	rsi, xmm0
	shr	rsi, 52
	and	esi, 2047
	sub	rsi, 1023
	mov	edx, -1840700269
	mov	eax, esi
	imul	edx
	add	edx, esi
	sar	edx, 5
	sar	esi, 31
	sub	edx, esi
	mov	esi, edx
	add	esi, DWORD PTR [rbx]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm10
	jp	.L1002
	vucomisd	xmm0, xmm11
	je	.L1000
.L1002:
	mov	r13d, 256
	mov	r12, -256
.L1036:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	rdi, xmm0
	movsx	rax, esi
	mov	rcx, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx+rax*8], rdx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L1003
	mov	r10d, esi
.L1006:
	add	rdi, rdx
	sar	rdi, 56
	test	rdx, rdx
	mov	rdx, r12
	cmovg	rdx, r13
	movsx	rcx, r10d
	sal	rcx, 3
	mov	r14, rcx
	add	r14, QWORD PTR [rbx+8]
	mov	rax, rdi
	sal	rax, 56
	neg	rax
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r14], rax
seto r15b
# 0 "" 2
#NO_APP
	add	rdi, rdx
	add	r10d, 1
	mov	eax, DWORD PTR [rbx+4]
	add	eax, DWORD PTR [rbx]
	cmp	r10d, eax
	jl	.L1005
	mov	DWORD PTR [rbx+40], 4
	jmp	.L1003
.L1005:
	mov	rax, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax+8+rcx], rdx
seto r14b
# 0 "" 2
#NO_APP
	test	r14b, r14b
	jne	.L1006
.L1003:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm5
	sub	esi, 1
	vucomisd	xmm0, xmm3
	jp	.L1036
	vucomisd	xmm0, xmm7
	jne	.L1036
.L1000:
	add	r11, 8
	cmp	r11, r8
	jne	.L1008
	vmovupd	YMMWORD PTR [rbp-368], ymm2
	vzeroupper
	lea	r11, [rbp-368]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm9, xmm7
	vmovapd	xmm2, xmm7
	vmovapd	xmm3, xmm7
	vmovapd	xmm10, xmm7
	vmovapd	xmm8, xmm7
.L1017:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rbx, QWORD PTR [rbp-336]
	vucomisd	xmm0, xmm7
	jp	.L1025
	vucomisd	xmm0, xmm8
	je	.L1009
.L1025:
	vmovq	rcx, xmm0
	vmovq	rsi, xmm0
	shr	rsi, 52
	and	esi, 2047
	sub	rsi, 1023
	mov	edx, -1840700269
	mov	eax, esi
	imul	edx
	add	edx, esi
	sar	edx, 5
	sar	esi, 31
	sub	edx, esi
	mov	esi, edx
	add	esi, DWORD PTR [rbx]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm9
	jp	.L1011
	vucomisd	xmm0, xmm10
	je	.L1009
.L1011:
	mov	r13d, 256
	mov	r12, -256
.L1037:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	rdi, xmm0
	movsx	rax, esi
	mov	rcx, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx+rax*8], rdx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L1012
	mov	r10d, esi
.L1015:
	add	rdi, rdx
	sar	rdi, 56
	test	rdx, rdx
	mov	rdx, r12
	cmovg	rdx, r13
	movsx	rcx, r10d
	sal	rcx, 3
	mov	r14, rcx
	add	r14, QWORD PTR [rbx+8]
	mov	rax, rdi
	sal	rax, 56
	neg	rax
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r14], rax
seto r15b
# 0 "" 2
#NO_APP
	add	rdi, rdx
	add	r10d, 1
	mov	eax, DWORD PTR [rbx+4]
	add	eax, DWORD PTR [rbx]
	cmp	r10d, eax
	jl	.L1014
	mov	DWORD PTR [rbx+40], 4
	jmp	.L1012
.L1014:
	mov	rax, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax+8+rcx], rdx
seto r14b
# 0 "" 2
#NO_APP
	test	r14b, r14b
	jne	.L1015
.L1012:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm4
	sub	esi, 1
	vucomisd	xmm0, xmm2
	jp	.L1037
	vucomisd	xmm0, xmm3
	jne	.L1037
.L1009:
	add	r11, 8
	cmp	r11, r8
	jne	.L1017
.L999:
	add	r9d, 8
	cmp	DWORD PTR [rbp-380], r9d
	jg	.L1027
	jmp	.L1018
.L1023:
.L986:
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1020
	call	__stack_chk_fail
.L1020:
	add	rsp, 384
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7607:
	.section	.gcc_except_table
.LLSDA7607:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7607-.LLSDACSB7607
.LLSDACSB7607:
.LLSDACSE7607:
	.text
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.4, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.4
	.section	.text._ZN15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,"axG",@progbits,_ZN15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,comdat
	.align 2
	.weak	_ZN15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.type	_ZN15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, @function
_ZN15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv:
.LFB7230:
	.cfi_startproc
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 64
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r12, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	lea	rbx, [rdi+32]
	lea	rax, [rdi+256]
	mov	QWORD PTR [rbp-112], rax
	lea	rax, [rbp-96]
	lea	r13, [rax+32]
	vxorpd	xmm6, xmm6, xmm6
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm6
	vmovapd	xmm4, xmm6
.L1050:
	mov	QWORD PTR [rbp-104], rbx
	vmovapd	ymm0, YMMWORD PTR [rbx]
	vmovupd	YMMWORD PTR [rbp-96], ymm0
	vzeroupper
	lea	r9, [rbp-96]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm8, xmm7
	vmovapd	xmm5, xmm7
.L1049:
	vmovsd	xmm0, QWORD PTR [r9]
	mov	rsi, QWORD PTR [r12]
	vucomisd	xmm0, xmm6
	jp	.L1053
	vucomisd	xmm0, xmm5
	je	.L1041
.L1053:
	vmovq	rcx, xmm0
	vmovq	rdi, xmm0
	shr	rdi, 52
	and	edi, 2047
	sub	rdi, 1023
	mov	edx, -1840700269
	mov	eax, edi
	imul	edx
	add	edx, edi
	sar	edx, 5
	sar	edi, 31
	sub	edx, edi
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm7
	jp	.L1043
	vucomisd	xmm0, xmm8
	je	.L1041
.L1043:
	mov	r14d, 256
.L1057:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r8, xmm0
	movsx	rdx, eax
	mov	rdi, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdi+rdx*8], rcx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L1044
	mov	r10d, eax
.L1047:
	add	r8, rcx
	sar	r8, 56
	test	rcx, rcx
	mov	rcx, -256
	cmovg	rcx, r14
	movsx	rdi, r10d
	sal	rdi, 3
	mov	r11, rdi
	add	r11, QWORD PTR [rsi+8]
	mov	rdx, r8
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r11], rdx
seto r15b
# 0 "" 2
#NO_APP
	add	r8, rcx
	add	r10d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r10d, edx
	jl	.L1046
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1044
.L1046:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+rdi], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	jne	.L1047
.L1044:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1057
	vucomisd	xmm0, xmm4
	jne	.L1057
.L1041:
	add	r9, 8
	cmp	r9, r13
	jne	.L1049
	vxorpd	xmm0, xmm0, xmm0
	mov	rax, QWORD PTR [rbp-104]
	vmovapd	YMMWORD PTR [rax], ymm0
	add	rbx, 32
	cmp	rbx, QWORD PTR [rbp-112]
	jne	.L1050
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1051
	call	__stack_chk_fail
.L1051:
	add	rsp, 64
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7230:
	.size	_ZN15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, .-_ZN15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.text
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.5, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.5:
.LFB7608:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7608
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 416
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r15, rdi
	mov	QWORD PTR [rbp-408], rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	call	omp_get_thread_num
	mov	ebx, eax
	mov	DWORD PTR [rbp-416], eax
	call	omp_get_num_threads
	mov	DWORD PTR [rbp-428], eax
	mov	eax, ebx
	lea	rdx, [0+rax*8]
	sal	rax, 6
	sub	rax, rdx
	mov	rbx, rax
	mov	QWORD PTR [rbp-448], rax
	mov	QWORD PTR [rbp-440], rax
	mov	rax, QWORD PTR [r15+8]
	add	rbx, QWORD PTR [rax]
	mov	QWORD PTR [rbp-368], rbx
	vxorpd	xmm0, xmm0, xmm0
	vmovapd	YMMWORD PTR [rbp-112], ymm0
	lea	rax, [rbp-336]
	lea	rdx, [rbp-112]
.L1060:
	vmovapd	YMMWORD PTR [rax], ymm0
	add	rax, 32
	cmp	rax, rdx
	jne	.L1060
	mov	ebx, DWORD PTR [rbp-416]
	mov	eax, ebx
	sal	eax, 4
	mov	eax, eax
	sal	rax, 2
	mov	rdi, rax
	mov	QWORD PTR [rbp-424], rax
	mov	rcx, QWORD PTR [rbp-408]
	mov	rax, QWORD PTR [rcx+16]
	add	rdi, QWORD PTR [rax]
	mov	DWORD PTR [rdi], 0
	movsx	rsi, DWORD PTR [rcx+24]
	mov	ecx, DWORD PTR [rbp-428]
	mov	edx, ebx
	mov	rax, rdx
	imul	rax, rsi
	cqo
	idiv	rcx
	mov	r9d, eax
	and	r9d, -8
	mov	eax, ebx
	add	eax, 1
	imul	rax, rsi
	cqo
	idiv	rcx
	and	eax, -8
	sub	eax, 1
	mov	DWORD PTR [rbp-412], eax
	cmp	r9d, eax
	jl	.L1061
.L1092:
	lea	rdi, [rbp-368]
	call	_ZN15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	mov	rbx, QWORD PTR [rbp-408]
	mov	rax, QWORD PTR [rbx+8]
	mov	rdi, QWORD PTR [rbp-448]
	add	rdi, QWORD PTR [rax]
	call	_ZN16Superaccumulator9NormalizeEv
	mov	r14, QWORD PTR [rbx+8]
	mov	r13, QWORD PTR [rbx+16]
	mov	eax, DWORD PTR [rbp-428]
	cmp	eax, 1
	jbe	.L1059
	mov	ebx, 1
	mov	edx, 1
	mov	r15d, 1
	mov	QWORD PTR [rbp-408], r14
	mov	r14d, eax
.L1066:
	mov	rcx, QWORD PTR [rbp-424]
	add	rcx, QWORD PTR [r13+0]
	mov	eax, DWORD PTR [rcx]
	add	eax, 1
	mov	DWORD PTR [rcx], eax
	mov	r8d, ebx
	mov	r12d, r15d
	mov	ecx, ebx
	sal	r12d, cl
	lea	eax, [r12-1]
	test	DWORD PTR [rbp-416], eax
	jne	.L1063
	or	edx, DWORD PTR [rbp-416]
	cmp	r14d, edx
	jbe	.L1063
	mov	eax, edx
	sal	eax, 4
	mov	eax, eax
	mov	rcx, QWORD PTR [r13+0]
	lea	rcx, [rcx+rax*4]
	mov	rax, QWORD PTR [rbp-408]
	mov	rax, QWORD PTR [rax]
	mov	edx, edx
	lea	rsi, [0+rdx*8]
	sal	rdx, 6
	sub	rdx, rsi
	mov	rsi, rdx
	add	rsi, rax
	mov	rdi, QWORD PTR [rbp-440]
	add	rdi, rax
	prefetcht0	[rcx]
	mov	eax, DWORD PTR [rcx]
	cmp	ebx, eax
	jle	.L1064
.L1100:
	rep nop
	mov	eax, DWORD PTR [rcx]
	cmp	r8d, eax
	jg	.L1100
.L1064:
	call	_ZN16Superaccumulator10AccumulateERS_
.L1063:
	mov	edx, r12d
	add	ebx, 1
	cmp	r14d, r12d
	ja	.L1066
	jmp	.L1059
.L1061:
	vmovapd	ymm4, YMMWORD PTR _ZZ10constant8fILin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647EEDv8_fvE1u[rip]
	lea	rax, [rbp-400]
	lea	r8, [rax+32]
	vmovsd	xmm6, QWORD PTR .LC15[rip]
	vmovapd	xmm5, xmm6
.L1101:
#APP
# 127 "longacc.cpp" 1
	# myloop
# 0 "" 2
#NO_APP
	movsx	rax, r9d
	add	rax, 4
	mov	rbx, QWORD PTR [rbp-408]
	mov	rdx, QWORD PTR [rbx]
	vmovapd	ymm2, YMMWORD PTR [rdx+rax*8]
	vmovapd	ymm1, YMMWORD PTR [rdx-32+rax*8]
	lea	rax, [rbp-368]
	mov	edx, 0
	vxorpd	xmm9, xmm9, xmm9
.L1071:
	mov	rcx, rax
	vmovapd	ymm3, YMMWORD PTR [rax+32]
	vaddpd	ymm0, ymm3, ymm1
	vandpd	ymm7, ymm3, ymm4
	vandpd	ymm8, ymm1, ymm4
	vcmppd	ymm7, ymm7, ymm8, 1
	vptest	ymm7, ymm1
	je	.L1067
	vblendvpd	ymm8, ymm3, ymm1, ymm7
	vblendvpd	ymm1, ymm1, ymm3, ymm7
	vmovapd	ymm3, ymm8
.L1067:
	vsubpd	ymm3, ymm3, ymm0
	vaddpd	ymm1, ymm3, ymm1
	vaddpd	ymm8, ymm0, ymm2
	vandpd	ymm3, ymm0, ymm4
	vandpd	ymm7, ymm2, ymm4
	vcmppd	ymm3, ymm3, ymm7, 1
	vptest	ymm3, ymm2
	je	.L1068
	vblendvpd	ymm7, ymm0, ymm2, ymm3
	vblendvpd	ymm2, ymm2, ymm0, ymm3
	vmovapd	ymm0, ymm7
.L1068:
	vsubpd	ymm0, ymm0, ymm8
	vaddpd	ymm2, ymm0, ymm2
	vmovapd	YMMWORD PTR [rcx+32], ymm8
	test	edx, edx
	je	.L1069
	vorpd	ymm0, ymm1, ymm2
	vcmppd	ymm0, ymm0, ymm9, 4
	vtestpd	ymm0, ymm0
	je	.L1073
.L1069:
	add	edx, 1
	add	rax, 32
	cmp	edx, 7
	jne	.L1071
	vmovupd	YMMWORD PTR [rbp-400], ymm1
	vzeroupper
	lea	r11, [rbp-400]
	vxorpd	xmm8, xmm8, xmm8
	vmovapd	xmm10, xmm8
	vmovapd	xmm3, xmm8
	vmovapd	xmm7, xmm8
	vmovapd	xmm11, xmm8
	vmovapd	xmm9, xmm8
.L1082:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rbx, QWORD PTR [rbp-368]
	vucomisd	xmm0, xmm8
	jp	.L1098
	vucomisd	xmm0, xmm9
	je	.L1074
.L1098:
	vmovq	rcx, xmm0
	vmovq	rsi, xmm0
	shr	rsi, 52
	and	esi, 2047
	sub	rsi, 1023
	mov	edx, -1840700269
	mov	eax, esi
	imul	edx
	add	edx, esi
	sar	edx, 5
	sar	esi, 31
	sub	edx, esi
	mov	esi, edx
	add	esi, DWORD PTR [rbx]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm10
	jp	.L1076
	vucomisd	xmm0, xmm11
	je	.L1074
.L1076:
	mov	r13d, 256
	mov	r12, -256
.L1111:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	rdi, xmm0
	movsx	rax, esi
	mov	rcx, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx+rax*8], rdx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L1077
	mov	r10d, esi
.L1080:
	add	rdi, rdx
	sar	rdi, 56
	test	rdx, rdx
	mov	rdx, r12
	cmovg	rdx, r13
	movsx	rcx, r10d
	sal	rcx, 3
	mov	r14, rcx
	add	r14, QWORD PTR [rbx+8]
	mov	rax, rdi
	sal	rax, 56
	neg	rax
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r14], rax
seto r15b
# 0 "" 2
#NO_APP
	add	rdi, rdx
	add	r10d, 1
	mov	eax, DWORD PTR [rbx+4]
	add	eax, DWORD PTR [rbx]
	cmp	r10d, eax
	jl	.L1079
	mov	DWORD PTR [rbx+40], 4
	jmp	.L1077
.L1079:
	mov	rax, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax+8+rcx], rdx
seto r14b
# 0 "" 2
#NO_APP
	test	r14b, r14b
	jne	.L1080
.L1077:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm6
	sub	esi, 1
	vucomisd	xmm0, xmm3
	jp	.L1111
	vucomisd	xmm0, xmm7
	jne	.L1111
.L1074:
	add	r11, 8
	cmp	r11, r8
	jne	.L1082
	vmovupd	YMMWORD PTR [rbp-400], ymm2
	vzeroupper
	lea	r11, [rbp-400]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm9, xmm7
	vmovapd	xmm2, xmm7
	vmovapd	xmm3, xmm7
	vmovapd	xmm10, xmm7
	vmovapd	xmm8, xmm7
.L1091:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rbx, QWORD PTR [rbp-368]
	vucomisd	xmm0, xmm7
	jp	.L1099
	vucomisd	xmm0, xmm8
	je	.L1083
.L1099:
	vmovq	rcx, xmm0
	vmovq	rsi, xmm0
	shr	rsi, 52
	and	esi, 2047
	sub	rsi, 1023
	mov	edx, -1840700269
	mov	eax, esi
	imul	edx
	add	edx, esi
	sar	edx, 5
	sar	esi, 31
	sub	edx, esi
	mov	esi, edx
	add	esi, DWORD PTR [rbx]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm9
	jp	.L1085
	vucomisd	xmm0, xmm10
	je	.L1083
.L1085:
	mov	r13d, 256
	mov	r12, -256
.L1112:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	rdi, xmm0
	movsx	rax, esi
	mov	rcx, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx+rax*8], rdx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L1086
	mov	r10d, esi
.L1089:
	add	rdi, rdx
	sar	rdi, 56
	test	rdx, rdx
	mov	rdx, r12
	cmovg	rdx, r13
	movsx	rcx, r10d
	sal	rcx, 3
	mov	r14, rcx
	add	r14, QWORD PTR [rbx+8]
	mov	rax, rdi
	sal	rax, 56
	neg	rax
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r14], rax
seto r15b
# 0 "" 2
#NO_APP
	add	rdi, rdx
	add	r10d, 1
	mov	eax, DWORD PTR [rbx+4]
	add	eax, DWORD PTR [rbx]
	cmp	r10d, eax
	jl	.L1088
	mov	DWORD PTR [rbx+40], 4
	jmp	.L1086
.L1088:
	mov	rax, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax+8+rcx], rdx
seto r14b
# 0 "" 2
#NO_APP
	test	r14b, r14b
	jne	.L1089
.L1086:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm5
	sub	esi, 1
	vucomisd	xmm0, xmm2
	jp	.L1112
	vucomisd	xmm0, xmm3
	jne	.L1112
.L1083:
	add	r11, 8
	cmp	r11, r8
	jne	.L1091
.L1073:
	add	r9d, 8
	cmp	DWORD PTR [rbp-412], r9d
	jg	.L1101
	jmp	.L1092
.L1097:
.L1059:
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1094
	call	__stack_chk_fail
.L1094:
	add	rsp, 416
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7608:
	.section	.gcc_except_table
.LLSDA7608:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7608-.LLSDACSB7608
.LLSDACSB7608:
.LLSDACSE7608:
	.text
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.5, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.5
	.section	.text._ZN15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,"axG",@progbits,_ZN15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,comdat
	.align 2
	.weak	_ZN15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.type	_ZN15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, @function
_ZN15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv:
.LFB7235:
	.cfi_startproc
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 64
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r12, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	lea	rbx, [rdi+32]
	lea	rax, [rdi+288]
	mov	QWORD PTR [rbp-112], rax
	lea	rax, [rbp-96]
	lea	r13, [rax+32]
	vxorpd	xmm6, xmm6, xmm6
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm6
	vmovapd	xmm4, xmm6
.L1125:
	mov	QWORD PTR [rbp-104], rbx
	vmovapd	ymm0, YMMWORD PTR [rbx]
	vmovupd	YMMWORD PTR [rbp-96], ymm0
	vzeroupper
	lea	r9, [rbp-96]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm8, xmm7
	vmovapd	xmm5, xmm7
.L1124:
	vmovsd	xmm0, QWORD PTR [r9]
	mov	rsi, QWORD PTR [r12]
	vucomisd	xmm0, xmm6
	jp	.L1128
	vucomisd	xmm0, xmm5
	je	.L1116
.L1128:
	vmovq	rcx, xmm0
	vmovq	rdi, xmm0
	shr	rdi, 52
	and	edi, 2047
	sub	rdi, 1023
	mov	edx, -1840700269
	mov	eax, edi
	imul	edx
	add	edx, edi
	sar	edx, 5
	sar	edi, 31
	sub	edx, edi
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm7
	jp	.L1118
	vucomisd	xmm0, xmm8
	je	.L1116
.L1118:
	mov	r14d, 256
.L1132:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r8, xmm0
	movsx	rdx, eax
	mov	rdi, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdi+rdx*8], rcx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L1119
	mov	r10d, eax
.L1122:
	add	r8, rcx
	sar	r8, 56
	test	rcx, rcx
	mov	rcx, -256
	cmovg	rcx, r14
	movsx	rdi, r10d
	sal	rdi, 3
	mov	r11, rdi
	add	r11, QWORD PTR [rsi+8]
	mov	rdx, r8
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r11], rdx
seto r15b
# 0 "" 2
#NO_APP
	add	r8, rcx
	add	r10d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r10d, edx
	jl	.L1121
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1119
.L1121:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+rdi], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	jne	.L1122
.L1119:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1132
	vucomisd	xmm0, xmm4
	jne	.L1132
.L1116:
	add	r9, 8
	cmp	r9, r13
	jne	.L1124
	vxorpd	xmm0, xmm0, xmm0
	mov	rax, QWORD PTR [rbp-104]
	vmovapd	YMMWORD PTR [rax], ymm0
	add	rbx, 32
	cmp	rbx, QWORD PTR [rbp-112]
	jne	.L1125
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1126
	call	__stack_chk_fail
.L1126:
	add	rsp, 64
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7235:
	.size	_ZN15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, .-_ZN15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.text
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.6, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.6:
.LFB7609:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7609
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 448
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r15, rdi
	mov	QWORD PTR [rbp-440], rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	call	omp_get_thread_num
	mov	ebx, eax
	mov	DWORD PTR [rbp-448], eax
	call	omp_get_num_threads
	mov	DWORD PTR [rbp-460], eax
	mov	eax, ebx
	lea	rdx, [0+rax*8]
	sal	rax, 6
	sub	rax, rdx
	mov	rbx, rax
	mov	QWORD PTR [rbp-480], rax
	mov	QWORD PTR [rbp-472], rax
	mov	rax, QWORD PTR [r15+8]
	add	rbx, QWORD PTR [rax]
	mov	QWORD PTR [rbp-400], rbx
	vxorpd	xmm0, xmm0, xmm0
	vmovapd	YMMWORD PTR [rbp-112], ymm0
	lea	rax, [rbp-368]
	lea	rdx, [rbp-112]
.L1135:
	vmovapd	YMMWORD PTR [rax], ymm0
	add	rax, 32
	cmp	rax, rdx
	jne	.L1135
	mov	ebx, DWORD PTR [rbp-448]
	mov	eax, ebx
	sal	eax, 4
	mov	eax, eax
	sal	rax, 2
	mov	rdi, rax
	mov	QWORD PTR [rbp-456], rax
	mov	rcx, QWORD PTR [rbp-440]
	mov	rax, QWORD PTR [rcx+16]
	add	rdi, QWORD PTR [rax]
	mov	DWORD PTR [rdi], 0
	movsx	rsi, DWORD PTR [rcx+24]
	mov	ecx, DWORD PTR [rbp-460]
	mov	edx, ebx
	mov	rax, rdx
	imul	rax, rsi
	cqo
	idiv	rcx
	mov	r9d, eax
	and	r9d, -8
	mov	eax, ebx
	add	eax, 1
	imul	rax, rsi
	cqo
	idiv	rcx
	and	eax, -8
	sub	eax, 1
	mov	DWORD PTR [rbp-444], eax
	cmp	r9d, eax
	jl	.L1136
.L1167:
	lea	rdi, [rbp-400]
	call	_ZN15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	mov	rbx, QWORD PTR [rbp-440]
	mov	rax, QWORD PTR [rbx+8]
	mov	rdi, QWORD PTR [rbp-480]
	add	rdi, QWORD PTR [rax]
	call	_ZN16Superaccumulator9NormalizeEv
	mov	r14, QWORD PTR [rbx+8]
	mov	r13, QWORD PTR [rbx+16]
	mov	edi, DWORD PTR [rbp-460]
	cmp	edi, 1
	jbe	.L1134
	mov	ebx, 1
	mov	eax, 1
	mov	r15d, 1
	mov	QWORD PTR [rbp-440], r14
	mov	r14d, edi
.L1141:
	mov	rcx, QWORD PTR [rbp-456]
	add	rcx, QWORD PTR [r13+0]
	mov	edx, DWORD PTR [rcx]
	add	edx, 1
	mov	DWORD PTR [rcx], edx
	mov	edx, ebx
	mov	r12d, r15d
	mov	ecx, ebx
	sal	r12d, cl
	lea	ecx, [r12-1]
	test	DWORD PTR [rbp-448], ecx
	jne	.L1138
	or	eax, DWORD PTR [rbp-448]
	cmp	r14d, eax
	jbe	.L1138
	mov	ecx, eax
	sal	ecx, 4
	mov	ecx, ecx
	mov	rsi, QWORD PTR [r13+0]
	lea	rcx, [rsi+rcx*4]
	mov	rdi, QWORD PTR [rbp-440]
	mov	rdi, QWORD PTR [rdi]
	mov	eax, eax
	lea	rsi, [0+rax*8]
	sal	rax, 6
	sub	rax, rsi
	lea	rsi, [rdi+rax]
	add	rdi, QWORD PTR [rbp-472]
	prefetcht0	[rcx]
	mov	eax, DWORD PTR [rcx]
	cmp	ebx, eax
	jle	.L1139
.L1175:
	rep nop
	mov	eax, DWORD PTR [rcx]
	cmp	edx, eax
	jg	.L1175
.L1139:
	call	_ZN16Superaccumulator10AccumulateERS_
.L1138:
	mov	eax, r12d
	add	ebx, 1
	cmp	r14d, r12d
	ja	.L1141
	jmp	.L1134
.L1136:
	vmovapd	ymm4, YMMWORD PTR _ZZ10constant8fILin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647EEDv8_fvE1u[rip]
	lea	rax, [rbp-432]
	lea	r8, [rax+32]
	vmovsd	xmm6, QWORD PTR .LC15[rip]
	vmovapd	xmm5, xmm6
.L1176:
#APP
# 127 "longacc.cpp" 1
	# myloop
# 0 "" 2
#NO_APP
	movsx	rax, r9d
	add	rax, 4
	mov	rbx, QWORD PTR [rbp-440]
	mov	rdx, QWORD PTR [rbx]
	vmovapd	ymm2, YMMWORD PTR [rdx+rax*8]
	vmovapd	ymm1, YMMWORD PTR [rdx-32+rax*8]
	lea	rax, [rbp-400]
	mov	edx, 0
	vxorpd	xmm9, xmm9, xmm9
.L1146:
	mov	rcx, rax
	vmovapd	ymm3, YMMWORD PTR [rax+32]
	vaddpd	ymm0, ymm3, ymm1
	vandpd	ymm7, ymm3, ymm4
	vandpd	ymm8, ymm1, ymm4
	vcmppd	ymm7, ymm7, ymm8, 1
	vptest	ymm7, ymm1
	je	.L1142
	vblendvpd	ymm8, ymm3, ymm1, ymm7
	vblendvpd	ymm1, ymm1, ymm3, ymm7
	vmovapd	ymm3, ymm8
.L1142:
	vsubpd	ymm3, ymm3, ymm0
	vaddpd	ymm1, ymm3, ymm1
	vaddpd	ymm8, ymm0, ymm2
	vandpd	ymm3, ymm0, ymm4
	vandpd	ymm7, ymm2, ymm4
	vcmppd	ymm3, ymm3, ymm7, 1
	vptest	ymm3, ymm2
	je	.L1143
	vblendvpd	ymm7, ymm0, ymm2, ymm3
	vblendvpd	ymm2, ymm2, ymm0, ymm3
	vmovapd	ymm0, ymm7
.L1143:
	vsubpd	ymm0, ymm0, ymm8
	vaddpd	ymm2, ymm0, ymm2
	vmovapd	YMMWORD PTR [rcx+32], ymm8
	test	edx, edx
	je	.L1144
	vorpd	ymm0, ymm1, ymm2
	vcmppd	ymm0, ymm0, ymm9, 4
	vtestpd	ymm0, ymm0
	je	.L1148
.L1144:
	add	edx, 1
	add	rax, 32
	cmp	edx, 8
	jne	.L1146
	vmovupd	YMMWORD PTR [rbp-432], ymm1
	vzeroupper
	lea	r11, [rbp-432]
	vxorpd	xmm8, xmm8, xmm8
	vmovapd	xmm10, xmm8
	vmovapd	xmm3, xmm8
	vmovapd	xmm7, xmm8
	vmovapd	xmm11, xmm8
	vmovapd	xmm9, xmm8
.L1157:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rbx, QWORD PTR [rbp-400]
	vucomisd	xmm0, xmm8
	jp	.L1173
	vucomisd	xmm0, xmm9
	je	.L1149
.L1173:
	vmovq	rcx, xmm0
	vmovq	rsi, xmm0
	shr	rsi, 52
	and	esi, 2047
	sub	rsi, 1023
	mov	edx, -1840700269
	mov	eax, esi
	imul	edx
	add	edx, esi
	sar	edx, 5
	sar	esi, 31
	sub	edx, esi
	mov	esi, edx
	add	esi, DWORD PTR [rbx]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm10
	jp	.L1151
	vucomisd	xmm0, xmm11
	je	.L1149
.L1151:
	mov	r13d, 256
	mov	r12, -256
.L1186:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	rdi, xmm0
	movsx	rax, esi
	mov	rcx, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx+rax*8], rdx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L1152
	mov	r10d, esi
.L1155:
	add	rdi, rdx
	sar	rdi, 56
	test	rdx, rdx
	mov	rdx, r12
	cmovg	rdx, r13
	movsx	rcx, r10d
	sal	rcx, 3
	mov	r14, rcx
	add	r14, QWORD PTR [rbx+8]
	mov	rax, rdi
	sal	rax, 56
	neg	rax
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r14], rax
seto r15b
# 0 "" 2
#NO_APP
	add	rdi, rdx
	add	r10d, 1
	mov	eax, DWORD PTR [rbx+4]
	add	eax, DWORD PTR [rbx]
	cmp	r10d, eax
	jl	.L1154
	mov	DWORD PTR [rbx+40], 4
	jmp	.L1152
.L1154:
	mov	rax, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax+8+rcx], rdx
seto r14b
# 0 "" 2
#NO_APP
	test	r14b, r14b
	jne	.L1155
.L1152:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm6
	sub	esi, 1
	vucomisd	xmm0, xmm3
	jp	.L1186
	vucomisd	xmm0, xmm7
	jne	.L1186
.L1149:
	add	r11, 8
	cmp	r11, r8
	jne	.L1157
	vmovupd	YMMWORD PTR [rbp-432], ymm2
	vzeroupper
	lea	r11, [rbp-432]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm9, xmm7
	vmovapd	xmm2, xmm7
	vmovapd	xmm3, xmm7
	vmovapd	xmm10, xmm7
	vmovapd	xmm8, xmm7
.L1166:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rbx, QWORD PTR [rbp-400]
	vucomisd	xmm0, xmm7
	jp	.L1174
	vucomisd	xmm0, xmm8
	je	.L1158
.L1174:
	vmovq	rcx, xmm0
	vmovq	rsi, xmm0
	shr	rsi, 52
	and	esi, 2047
	sub	rsi, 1023
	mov	edx, -1840700269
	mov	eax, esi
	imul	edx
	add	edx, esi
	sar	edx, 5
	sar	esi, 31
	sub	edx, esi
	mov	esi, edx
	add	esi, DWORD PTR [rbx]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm9
	jp	.L1160
	vucomisd	xmm0, xmm10
	je	.L1158
.L1160:
	mov	r13d, 256
	mov	r12, -256
.L1187:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	rdi, xmm0
	movsx	rax, esi
	mov	rcx, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx+rax*8], rdx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L1161
	mov	r10d, esi
.L1164:
	add	rdi, rdx
	sar	rdi, 56
	test	rdx, rdx
	mov	rdx, r12
	cmovg	rdx, r13
	movsx	rcx, r10d
	sal	rcx, 3
	mov	r14, rcx
	add	r14, QWORD PTR [rbx+8]
	mov	rax, rdi
	sal	rax, 56
	neg	rax
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r14], rax
seto r15b
# 0 "" 2
#NO_APP
	add	rdi, rdx
	add	r10d, 1
	mov	eax, DWORD PTR [rbx+4]
	add	eax, DWORD PTR [rbx]
	cmp	r10d, eax
	jl	.L1163
	mov	DWORD PTR [rbx+40], 4
	jmp	.L1161
.L1163:
	mov	rax, QWORD PTR [rbx+8]
	mov	rdx, rdi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax+8+rcx], rdx
seto r14b
# 0 "" 2
#NO_APP
	test	r14b, r14b
	jne	.L1164
.L1161:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm5
	sub	esi, 1
	vucomisd	xmm0, xmm2
	jp	.L1187
	vucomisd	xmm0, xmm3
	jne	.L1187
.L1158:
	add	r11, 8
	cmp	r11, r8
	jne	.L1166
.L1148:
	add	r9d, 8
	cmp	DWORD PTR [rbp-444], r9d
	jg	.L1176
	jmp	.L1167
.L1172:
.L1134:
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1169
	call	__stack_chk_fail
.L1169:
	add	rsp, 448
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7609:
	.section	.gcc_except_table
.LLSDA7609:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7609-.LLSDACSB7609
.LLSDACSB7609:
.LLSDACSE7609:
	.text
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.6, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb1ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.6
	.section	.text._ZN15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,"axG",@progbits,_ZN15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,comdat
	.align 2
	.weak	_ZN15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.type	_ZN15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, @function
_ZN15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv:
.LFB7240:
	.cfi_startproc
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 64
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	lea	r12, [rdi+32]
	lea	rax, [rdi+96]
	mov	QWORD PTR [rbp-112], rax
	lea	rax, [rbp-96]
	lea	r13, [rax+32]
	vxorpd	xmm6, xmm6, xmm6
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm6
	vmovapd	xmm4, xmm6
.L1200:
	mov	QWORD PTR [rbp-104], r12
	vmovapd	ymm0, YMMWORD PTR [r12]
	vmovupd	YMMWORD PTR [rbp-96], ymm0
	vzeroupper
	lea	r10, [rbp-96]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm8, xmm7
	vmovapd	xmm5, xmm7
.L1199:
	vmovsd	xmm0, QWORD PTR [r10]
	mov	rsi, QWORD PTR [rdi]
	vucomisd	xmm0, xmm6
	jp	.L1203
	vucomisd	xmm0, xmm5
	je	.L1191
.L1203:
	vmovq	rcx, xmm0
	vmovq	r8, xmm0
	shr	r8, 52
	and	r8d, 2047
	sub	r8, 1023
	mov	edx, -1840700269
	mov	eax, r8d
	imul	edx
	add	edx, r8d
	sar	edx, 5
	sar	r8d, 31
	sub	edx, r8d
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm7
	jp	.L1193
	vucomisd	xmm0, xmm8
	je	.L1191
.L1193:
	mov	r14d, 256
.L1207:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r9, xmm0
	movsx	rdx, eax
	mov	r8, QWORD PTR [rsi+8]
	mov	rcx, r9
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r8+rdx*8], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	je	.L1194
	mov	r11d, eax
.L1197:
	add	r9, rcx
	sar	r9, 56
	test	rcx, rcx
	mov	rcx, -256
	cmovg	rcx, r14
	movsx	r8, r11d
	sal	r8, 3
	mov	rbx, r8
	add	rbx, QWORD PTR [rsi+8]
	mov	rdx, r9
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rbx], rdx
seto r15b
# 0 "" 2
#NO_APP
	add	r9, rcx
	add	r11d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r11d, edx
	jl	.L1196
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1194
.L1196:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r9
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+r8], rcx
seto bl
# 0 "" 2
#NO_APP
	test	bl, bl
	jne	.L1197
.L1194:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1207
	vucomisd	xmm0, xmm4
	jne	.L1207
.L1191:
	add	r10, 8
	cmp	r10, r13
	jne	.L1199
	vxorpd	xmm0, xmm0, xmm0
	mov	rax, QWORD PTR [rbp-104]
	vmovapd	YMMWORD PTR [rax], ymm0
	add	r12, 32
	cmp	r12, QWORD PTR [rbp-112]
	jne	.L1200
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1201
	call	__stack_chk_fail
.L1201:
	add	rsp, 64
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7240:
	.size	_ZN15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, .-_ZN15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.text
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.7, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.7:
.LFB7610:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7610
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 256
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	rbx, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	call	omp_get_thread_num
	mov	r14d, eax
	call	omp_get_num_threads
	mov	ecx, eax
	mov	DWORD PTR [rbp-244], eax
	mov	r9d, r14d
	lea	rax, [0+r9*8]
	mov	rdi, r9
	sal	rdi, 6
	sub	rdi, rax
	mov	QWORD PTR [rbp-280], rdi
	mov	QWORD PTR [rbp-272], rdi
	mov	rax, QWORD PTR [rbx+8]
	add	rdi, QWORD PTR [rax]
	mov	QWORD PTR [rbp-208], rdi
	vxorpd	xmm0, xmm0, xmm0
	vmovapd	YMMWORD PTR [rbp-112], ymm0
	vmovapd	YMMWORD PTR [rbp-176], ymm0
	vmovapd	YMMWORD PTR [rbp-144], ymm0
	mov	eax, r14d
	sal	eax, 4
	mov	eax, eax
	lea	rsi, [0+rax*4]
	mov	QWORD PTR [rbp-256], rsi
	mov	rax, QWORD PTR [rbx+16]
	add	rsi, QWORD PTR [rax]
	mov	DWORD PTR [rsi], 0
	movsx	rsi, DWORD PTR [rbx+24]
	mov	ecx, ecx
	mov	rax, r9
	imul	rax, rsi
	cqo
	idiv	rcx
	mov	r9d, eax
	and	r9d, -8
	lea	eax, [r14+1]
	imul	rax, rsi
	cqo
	idiv	rcx
	and	eax, -8
	lea	r13d, [rax-1]
	cmp	r9d, r13d
	jl	.L1210
.L1239:
	lea	rdi, [rbp-208]
	call	_ZN15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	mov	rax, QWORD PTR [rbx+8]
	mov	rdi, QWORD PTR [rbp-280]
	add	rdi, QWORD PTR [rax]
	call	_ZN16Superaccumulator9NormalizeEv
	mov	rax, QWORD PTR [rbx+8]
	mov	QWORD PTR [rbp-264], rax
	mov	r13, QWORD PTR [rbx+16]
	cmp	DWORD PTR [rbp-244], 1
	jbe	.L1209
	mov	ebx, 1
	mov	eax, 1
	mov	r15d, 1
.L1215:
	mov	rcx, QWORD PTR [rbp-256]
	add	rcx, QWORD PTR [r13+0]
	mov	edx, DWORD PTR [rcx]
	add	edx, 1
	mov	DWORD PTR [rcx], edx
	mov	edx, ebx
	mov	r12d, r15d
	mov	ecx, ebx
	sal	r12d, cl
	lea	ecx, [r12-1]
	test	ecx, r14d
	jne	.L1212
	or	eax, r14d
	cmp	DWORD PTR [rbp-244], eax
	jbe	.L1212
	mov	ecx, eax
	sal	ecx, 4
	mov	ecx, ecx
	mov	rsi, QWORD PTR [r13+0]
	lea	rcx, [rsi+rcx*4]
	mov	rsi, QWORD PTR [rbp-264]
	mov	rdi, QWORD PTR [rsi]
	mov	eax, eax
	lea	rsi, [0+rax*8]
	sal	rax, 6
	sub	rax, rsi
	lea	rsi, [rdi+rax]
	add	rdi, QWORD PTR [rbp-272]
	prefetcht0	[rcx]
	mov	eax, DWORD PTR [rcx]
	cmp	ebx, eax
	jle	.L1213
.L1245:
	rep nop
	mov	eax, DWORD PTR [rcx]
	cmp	edx, eax
	jg	.L1245
.L1213:
	call	_ZN16Superaccumulator10AccumulateERS_
.L1212:
	mov	eax, r12d
	add	ebx, 1
	cmp	DWORD PTR [rbp-244], r12d
	ja	.L1215
	jmp	.L1209
.L1210:
	lea	rax, [rbp-208]
	lea	r15, [rax+64]
	vmovapd	ymm4, YMMWORD PTR _ZZ10constant8fILin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647EEDv8_fvE1u[rip]
	lea	rax, [rbp-240]
	add	rax, 32
	mov	QWORD PTR [rbp-264], rax
	vxorpd	xmm6, xmm6, xmm6
	vmovsd	xmm5, QWORD PTR .LC15[rip]
	mov	DWORD PTR [rbp-248], r14d
	mov	r14, r15
.L1246:
#APP
# 127 "longacc.cpp" 1
	# myloop
# 0 "" 2
#NO_APP
	movsx	rax, r9d
	add	rax, 4
	mov	rdx, QWORD PTR [rbx]
	vmovapd	ymm2, YMMWORD PTR [rdx+rax*8]
	vmovapd	ymm1, YMMWORD PTR [rdx-32+rax*8]
	lea	rax, [rbp-208]
.L1218:
	mov	rdx, rax
	vmovapd	ymm3, YMMWORD PTR [rax+32]
	vaddpd	ymm0, ymm3, ymm1
	vandpd	ymm7, ymm3, ymm4
	vandpd	ymm8, ymm1, ymm4
	vcmppd	ymm7, ymm7, ymm8, 1
	vptest	ymm7, ymm1
	je	.L1216
	vblendvpd	ymm8, ymm3, ymm1, ymm7
	vblendvpd	ymm1, ymm1, ymm3, ymm7
	vmovapd	ymm3, ymm8
.L1216:
	vsubpd	ymm3, ymm3, ymm0
	vaddpd	ymm1, ymm3, ymm1
	vaddpd	ymm8, ymm0, ymm2
	vandpd	ymm3, ymm0, ymm4
	vandpd	ymm7, ymm2, ymm4
	vcmppd	ymm3, ymm3, ymm7, 1
	vptest	ymm3, ymm2
	je	.L1217
	vblendvpd	ymm7, ymm0, ymm2, ymm3
	vblendvpd	ymm2, ymm2, ymm0, ymm3
	vmovapd	ymm0, ymm7
.L1217:
	vsubpd	ymm0, ymm0, ymm8
	vaddpd	ymm2, ymm0, ymm2
	vmovapd	YMMWORD PTR [rdx+32], ymm8
	add	rax, 32
	cmp	rax, r14
	jne	.L1218
	vxorpd	xmm0, xmm0, xmm0
	vcmppd	ymm0, ymm1, ymm0, 4
	vtestpd	ymm0, ymm0
	je	.L1219
	vmovupd	YMMWORD PTR [rbp-240], ymm1
	vzeroupper
	lea	r10, [rbp-240]
	vxorpd	xmm9, xmm9, xmm9
	vmovapd	xmm3, xmm9
	vmovapd	xmm7, xmm9
	vmovapd	xmm10, xmm9
	vmovapd	xmm8, xmm9
.L1228:
	vmovsd	xmm0, QWORD PTR [r10]
	mov	rsi, QWORD PTR [rbp-208]
	vucomisd	xmm0, xmm6
	jp	.L1243
	vucomisd	xmm0, xmm8
	je	.L1220
.L1243:
	vmovq	rcx, xmm0
	vmovq	rdi, xmm0
	shr	rdi, 52
	and	edi, 2047
	sub	rdi, 1023
	mov	edx, -1840700269
	mov	eax, edi
	imul	edx
	add	edx, edi
	sar	edx, 5
	sar	edi, 31
	sub	edx, edi
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm9
	jp	.L1255
	vucomisd	xmm0, xmm10
	je	.L1220
.L1255:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r8, xmm0
	movsx	rdx, eax
	mov	rdi, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdi+rdx*8], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	je	.L1223
	mov	r11d, eax
.L1226:
	add	r8, rcx
	sar	r8, 56
	test	rcx, rcx
	mov	rcx, -256
	mov	edi, 256
	cmovg	rcx, rdi
	movsx	rdi, r11d
	sal	rdi, 3
	mov	r12, rdi
	add	r12, QWORD PTR [rsi+8]
	mov	rdx, r8
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r12], rdx
seto r15b
# 0 "" 2
#NO_APP
	add	r8, rcx
	add	r11d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r11d, edx
	jl	.L1225
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1223
.L1225:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+rdi], rcx
seto r12b
# 0 "" 2
#NO_APP
	test	r12b, r12b
	jne	.L1226
.L1223:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm5
	sub	eax, 1
	vucomisd	xmm0, xmm3
	jp	.L1255
	vucomisd	xmm0, xmm7
	jne	.L1255
.L1220:
	add	r10, 8
	cmp	r10, QWORD PTR [rbp-264]
	jne	.L1228
.L1219:
	vxorpd	xmm0, xmm0, xmm0
	vcmppd	ymm0, ymm2, ymm0, 4
	vtestpd	ymm0, ymm0
	je	.L1229
	vmovupd	YMMWORD PTR [rbp-240], ymm2
	vzeroupper
	lea	r10, [rbp-240]
	vxorpd	xmm8, xmm8, xmm8
	vmovapd	xmm10, xmm8
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm8
	vmovapd	xmm7, xmm8
	vmovapd	xmm11, xmm8
	vmovapd	xmm9, xmm8
.L1238:
	vmovsd	xmm0, QWORD PTR [r10]
	mov	rsi, QWORD PTR [rbp-208]
	vucomisd	xmm0, xmm8
	jp	.L1244
	vucomisd	xmm0, xmm9
	je	.L1230
.L1244:
	vmovq	rcx, xmm0
	vmovq	rdi, xmm0
	shr	rdi, 52
	and	edi, 2047
	sub	rdi, 1023
	mov	edx, -1840700269
	mov	eax, edi
	imul	edx
	add	edx, edi
	sar	edx, 5
	sar	edi, 31
	sub	edx, edi
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm10
	jp	.L1256
	vucomisd	xmm0, xmm11
	je	.L1230
.L1256:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r8, xmm0
	movsx	rdx, eax
	mov	rdi, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdi+rdx*8], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	je	.L1233
	mov	r11d, eax
.L1236:
	add	r8, rcx
	sar	r8, 56
	test	rcx, rcx
	mov	rcx, -256
	mov	edi, 256
	cmovg	rcx, rdi
	movsx	rdi, r11d
	sal	rdi, 3
	mov	r12, rdi
	add	r12, QWORD PTR [rsi+8]
	mov	rdx, r8
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r12], rdx
seto r15b
# 0 "" 2
#NO_APP
	add	r8, rcx
	add	r11d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r11d, edx
	jl	.L1235
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1233
.L1235:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+rdi], rcx
seto r12b
# 0 "" 2
#NO_APP
	test	r12b, r12b
	jne	.L1236
.L1233:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1256
	vucomisd	xmm0, xmm7
	jne	.L1256
.L1230:
	add	r10, 8
	cmp	r10, QWORD PTR [rbp-264]
	jne	.L1238
.L1229:
	add	r9d, 8
	cmp	r13d, r9d
	jg	.L1246
	mov	r14d, DWORD PTR [rbp-248]
	jmp	.L1239
.L1209:
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1240
	call	__stack_chk_fail
.L1240:
	add	rsp, 256
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7610:
	.section	.gcc_except_table
.LLSDA7610:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7610-.LLSDACSB7610
.LLSDACSB7610:
.LLSDACSE7610:
	.text
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.7, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi2E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.7
	.section	.text._ZN15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,"axG",@progbits,_ZN15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,comdat
	.align 2
	.weak	_ZN15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.type	_ZN15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, @function
_ZN15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv:
.LFB7245:
	.cfi_startproc
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 64
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r12, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	lea	rbx, [rdi+32]
	lea	rax, [rdi+128]
	mov	QWORD PTR [rbp-112], rax
	lea	rax, [rbp-96]
	lea	r13, [rax+32]
	vxorpd	xmm6, xmm6, xmm6
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm6
	vmovapd	xmm4, xmm6
.L1268:
	mov	QWORD PTR [rbp-104], rbx
	vmovapd	ymm0, YMMWORD PTR [rbx]
	vmovupd	YMMWORD PTR [rbp-96], ymm0
	vzeroupper
	lea	r9, [rbp-96]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm8, xmm7
	vmovapd	xmm5, xmm7
.L1267:
	vmovsd	xmm0, QWORD PTR [r9]
	mov	rsi, QWORD PTR [r12]
	vucomisd	xmm0, xmm6
	jp	.L1271
	vucomisd	xmm0, xmm5
	je	.L1259
.L1271:
	vmovq	rcx, xmm0
	vmovq	rdi, xmm0
	shr	rdi, 52
	and	edi, 2047
	sub	rdi, 1023
	mov	edx, -1840700269
	mov	eax, edi
	imul	edx
	add	edx, edi
	sar	edx, 5
	sar	edi, 31
	sub	edx, edi
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm7
	jp	.L1261
	vucomisd	xmm0, xmm8
	je	.L1259
.L1261:
	mov	r14d, 256
.L1275:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r8, xmm0
	movsx	rdx, eax
	mov	rdi, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdi+rdx*8], rcx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L1262
	mov	r10d, eax
.L1265:
	add	r8, rcx
	sar	r8, 56
	test	rcx, rcx
	mov	rcx, -256
	cmovg	rcx, r14
	movsx	rdi, r10d
	sal	rdi, 3
	mov	r11, rdi
	add	r11, QWORD PTR [rsi+8]
	mov	rdx, r8
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r11], rdx
seto r15b
# 0 "" 2
#NO_APP
	add	r8, rcx
	add	r10d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r10d, edx
	jl	.L1264
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1262
.L1264:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+rdi], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	jne	.L1265
.L1262:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1275
	vucomisd	xmm0, xmm4
	jne	.L1275
.L1259:
	add	r9, 8
	cmp	r9, r13
	jne	.L1267
	vxorpd	xmm0, xmm0, xmm0
	mov	rax, QWORD PTR [rbp-104]
	vmovapd	YMMWORD PTR [rax], ymm0
	add	rbx, 32
	cmp	rbx, QWORD PTR [rbp-112]
	jne	.L1268
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1269
	call	__stack_chk_fail
.L1269:
	add	rsp, 64
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7245:
	.size	_ZN15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, .-_ZN15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.text
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.8, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.8:
.LFB7611:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7611
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 288
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	rbx, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	call	omp_get_thread_num
	mov	r14d, eax
	call	omp_get_num_threads
	mov	edx, eax
	mov	DWORD PTR [rbp-276], eax
	mov	ecx, r14d
	lea	rax, [0+rcx*8]
	mov	rdi, rcx
	sal	rdi, 6
	sub	rdi, rax
	mov	QWORD PTR [rbp-312], rdi
	mov	QWORD PTR [rbp-304], rdi
	mov	rax, QWORD PTR [rbx+8]
	add	rdi, QWORD PTR [rax]
	mov	QWORD PTR [rbp-240], rdi
	vxorpd	xmm0, xmm0, xmm0
	vmovapd	YMMWORD PTR [rbp-112], ymm0
	vmovapd	YMMWORD PTR [rbp-208], ymm0
	vmovapd	YMMWORD PTR [rbp-176], ymm0
	vmovapd	YMMWORD PTR [rbp-144], ymm0
	mov	eax, r14d
	sal	eax, 4
	mov	eax, eax
	lea	rsi, [0+rax*4]
	mov	QWORD PTR [rbp-288], rsi
	mov	rax, QWORD PTR [rbx+16]
	add	rsi, QWORD PTR [rax]
	mov	DWORD PTR [rsi], 0
	movsx	r8, DWORD PTR [rbx+24]
	mov	esi, edx
	mov	rax, rcx
	imul	rax, r8
	cqo
	idiv	rsi
	mov	edi, eax
	and	edi, -8
	lea	eax, [r14+1]
	imul	rax, r8
	cqo
	idiv	rsi
	and	eax, -8
	lea	r13d, [rax-1]
	cmp	edi, r13d
	jl	.L1278
.L1307:
	lea	rdi, [rbp-240]
	call	_ZN15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	mov	rax, QWORD PTR [rbx+8]
	mov	rdi, QWORD PTR [rbp-312]
	add	rdi, QWORD PTR [rax]
	call	_ZN16Superaccumulator9NormalizeEv
	mov	rax, QWORD PTR [rbx+8]
	mov	QWORD PTR [rbp-296], rax
	mov	r13, QWORD PTR [rbx+16]
	cmp	DWORD PTR [rbp-276], 1
	jbe	.L1277
	mov	ebx, 1
	mov	eax, 1
	mov	r15d, 1
.L1283:
	mov	rcx, QWORD PTR [rbp-288]
	add	rcx, QWORD PTR [r13+0]
	mov	edx, DWORD PTR [rcx]
	add	edx, 1
	mov	DWORD PTR [rcx], edx
	mov	edx, ebx
	mov	r12d, r15d
	mov	ecx, ebx
	sal	r12d, cl
	lea	ecx, [r12-1]
	test	ecx, r14d
	jne	.L1280
	or	eax, r14d
	cmp	DWORD PTR [rbp-276], eax
	jbe	.L1280
	mov	ecx, eax
	sal	ecx, 4
	mov	ecx, ecx
	mov	rsi, QWORD PTR [r13+0]
	lea	rcx, [rsi+rcx*4]
	mov	rsi, QWORD PTR [rbp-296]
	mov	rdi, QWORD PTR [rsi]
	mov	eax, eax
	lea	rsi, [0+rax*8]
	sal	rax, 6
	sub	rax, rsi
	lea	rsi, [rdi+rax]
	add	rdi, QWORD PTR [rbp-304]
	prefetcht0	[rcx]
	mov	eax, DWORD PTR [rcx]
	cmp	ebx, eax
	jle	.L1281
.L1313:
	rep nop
	mov	eax, DWORD PTR [rcx]
	cmp	edx, eax
	jg	.L1313
.L1281:
	call	_ZN16Superaccumulator10AccumulateERS_
.L1280:
	mov	eax, r12d
	add	ebx, 1
	cmp	DWORD PTR [rbp-276], r12d
	ja	.L1283
	jmp	.L1277
.L1278:
	lea	rax, [rbp-240]
	lea	r15, [rax+96]
	vmovapd	ymm4, YMMWORD PTR _ZZ10constant8fILin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647EEDv8_fvE1u[rip]
	lea	rax, [rbp-272]
	add	rax, 32
	mov	QWORD PTR [rbp-296], rax
	vxorpd	xmm6, xmm6, xmm6
	vmovsd	xmm5, QWORD PTR .LC15[rip]
	mov	DWORD PTR [rbp-280], r14d
	mov	r12d, r13d
	mov	r13, rbx
.L1314:
#APP
# 127 "longacc.cpp" 1
	# myloop
# 0 "" 2
#NO_APP
	movsx	rax, edi
	add	rax, 4
	mov	rdx, QWORD PTR [r13+0]
	vmovapd	ymm2, YMMWORD PTR [rdx+rax*8]
	vmovapd	ymm3, YMMWORD PTR [rdx-32+rax*8]
	lea	rax, [rbp-240]
.L1286:
	mov	rdx, rax
	vmovapd	ymm1, YMMWORD PTR [rax+32]
	vaddpd	ymm0, ymm1, ymm3
	vandpd	ymm7, ymm1, ymm4
	vandpd	ymm8, ymm3, ymm4
	vcmppd	ymm7, ymm7, ymm8, 1
	vptest	ymm7, ymm3
	je	.L1284
	vblendvpd	ymm8, ymm1, ymm3, ymm7
	vblendvpd	ymm3, ymm3, ymm1, ymm7
	vmovapd	ymm1, ymm8
.L1284:
	vsubpd	ymm1, ymm1, ymm0
	vaddpd	ymm3, ymm1, ymm3
	vaddpd	ymm8, ymm0, ymm2
	vandpd	ymm1, ymm0, ymm4
	vandpd	ymm7, ymm2, ymm4
	vcmppd	ymm1, ymm1, ymm7, 1
	vptest	ymm1, ymm2
	je	.L1285
	vblendvpd	ymm7, ymm0, ymm2, ymm1
	vblendvpd	ymm2, ymm2, ymm0, ymm1
	vmovapd	ymm0, ymm7
.L1285:
	vsubpd	ymm0, ymm0, ymm8
	vaddpd	ymm2, ymm0, ymm2
	vmovapd	YMMWORD PTR [rdx+32], ymm8
	add	rax, 32
	cmp	rax, r15
	jne	.L1286
	vxorpd	xmm0, xmm0, xmm0
	vcmppd	ymm0, ymm3, ymm0, 4
	vtestpd	ymm0, ymm0
	je	.L1287
	vmovupd	YMMWORD PTR [rbp-272], ymm3
	vzeroupper
	lea	r10, [rbp-272]
	vxorpd	xmm9, xmm9, xmm9
	vmovapd	xmm3, xmm9
	vmovapd	xmm7, xmm9
	vmovapd	xmm10, xmm9
	vmovapd	xmm8, xmm9
.L1296:
	vmovsd	xmm0, QWORD PTR [r10]
	mov	rsi, QWORD PTR [rbp-240]
	vucomisd	xmm0, xmm6
	jp	.L1311
	vucomisd	xmm0, xmm8
	je	.L1288
.L1311:
	vmovq	rcx, xmm0
	vmovq	r8, xmm0
	shr	r8, 52
	and	r8d, 2047
	sub	r8, 1023
	mov	edx, -1840700269
	mov	eax, r8d
	imul	edx
	add	edx, r8d
	sar	edx, 5
	sar	r8d, 31
	sub	edx, r8d
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm9
	jp	.L1323
	vucomisd	xmm0, xmm10
	je	.L1288
.L1323:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r9, xmm0
	movsx	rdx, eax
	mov	r8, QWORD PTR [rsi+8]
	mov	rcx, r9
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r8+rdx*8], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	je	.L1291
	mov	r11d, eax
.L1294:
	add	r9, rcx
	sar	r9, 56
	test	rcx, rcx
	mov	rcx, -256
	mov	ebx, 256
	cmovg	rcx, rbx
	movsx	r8, r11d
	sal	r8, 3
	mov	rbx, r8
	add	rbx, QWORD PTR [rsi+8]
	mov	rdx, r9
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rbx], rdx
seto r14b
# 0 "" 2
#NO_APP
	add	r9, rcx
	add	r11d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r11d, edx
	jl	.L1293
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1291
.L1293:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r9
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+r8], rcx
seto bl
# 0 "" 2
#NO_APP
	test	bl, bl
	jne	.L1294
.L1291:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm5
	sub	eax, 1
	vucomisd	xmm0, xmm3
	jp	.L1323
	vucomisd	xmm0, xmm7
	jne	.L1323
.L1288:
	add	r10, 8
	cmp	r10, QWORD PTR [rbp-296]
	jne	.L1296
.L1287:
	vxorpd	xmm0, xmm0, xmm0
	vcmppd	ymm0, ymm2, ymm0, 4
	vtestpd	ymm0, ymm0
	je	.L1297
	vmovupd	YMMWORD PTR [rbp-272], ymm2
	vzeroupper
	lea	r10, [rbp-272]
	vxorpd	xmm8, xmm8, xmm8
	vmovapd	xmm10, xmm8
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm8
	vmovapd	xmm7, xmm8
	vmovapd	xmm11, xmm8
	vmovapd	xmm9, xmm8
.L1306:
	vmovsd	xmm0, QWORD PTR [r10]
	mov	rsi, QWORD PTR [rbp-240]
	vucomisd	xmm0, xmm8
	jp	.L1312
	vucomisd	xmm0, xmm9
	je	.L1298
.L1312:
	vmovq	rcx, xmm0
	vmovq	r8, xmm0
	shr	r8, 52
	and	r8d, 2047
	sub	r8, 1023
	mov	edx, -1840700269
	mov	eax, r8d
	imul	edx
	add	edx, r8d
	sar	edx, 5
	sar	r8d, 31
	sub	edx, r8d
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm10
	jp	.L1324
	vucomisd	xmm0, xmm11
	je	.L1298
.L1324:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r9, xmm0
	movsx	rdx, eax
	mov	r8, QWORD PTR [rsi+8]
	mov	rcx, r9
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r8+rdx*8], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	je	.L1301
	mov	r11d, eax
.L1304:
	add	r9, rcx
	sar	r9, 56
	test	rcx, rcx
	mov	rcx, -256
	mov	ebx, 256
	cmovg	rcx, rbx
	movsx	r8, r11d
	sal	r8, 3
	mov	rbx, r8
	add	rbx, QWORD PTR [rsi+8]
	mov	rdx, r9
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rbx], rdx
seto r14b
# 0 "" 2
#NO_APP
	add	r9, rcx
	add	r11d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r11d, edx
	jl	.L1303
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1301
.L1303:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r9
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+r8], rcx
seto bl
# 0 "" 2
#NO_APP
	test	bl, bl
	jne	.L1304
.L1301:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1324
	vucomisd	xmm0, xmm7
	jne	.L1324
.L1298:
	add	r10, 8
	cmp	r10, QWORD PTR [rbp-296]
	jne	.L1306
.L1297:
	add	edi, 8
	cmp	r12d, edi
	jg	.L1314
	mov	r14d, DWORD PTR [rbp-280]
	mov	rbx, r13
	jmp	.L1307
.L1277:
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1308
	call	__stack_chk_fail
.L1308:
	add	rsp, 288
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7611:
	.section	.gcc_except_table
.LLSDA7611:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7611-.LLSDACSB7611
.LLSDACSB7611:
.LLSDACSE7611:
	.text
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.8, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi3E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.8
	.section	.text._ZN15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,"axG",@progbits,_ZN15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,comdat
	.align 2
	.weak	_ZN15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.type	_ZN15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, @function
_ZN15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv:
.LFB7250:
	.cfi_startproc
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 64
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r12, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	lea	rbx, [rdi+32]
	lea	rax, [rdi+160]
	mov	QWORD PTR [rbp-112], rax
	lea	rax, [rbp-96]
	lea	r13, [rax+32]
	vxorpd	xmm6, xmm6, xmm6
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm6
	vmovapd	xmm4, xmm6
.L1336:
	mov	QWORD PTR [rbp-104], rbx
	vmovapd	ymm0, YMMWORD PTR [rbx]
	vmovupd	YMMWORD PTR [rbp-96], ymm0
	vzeroupper
	lea	r9, [rbp-96]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm8, xmm7
	vmovapd	xmm5, xmm7
.L1335:
	vmovsd	xmm0, QWORD PTR [r9]
	mov	rsi, QWORD PTR [r12]
	vucomisd	xmm0, xmm6
	jp	.L1339
	vucomisd	xmm0, xmm5
	je	.L1327
.L1339:
	vmovq	rcx, xmm0
	vmovq	rdi, xmm0
	shr	rdi, 52
	and	edi, 2047
	sub	rdi, 1023
	mov	edx, -1840700269
	mov	eax, edi
	imul	edx
	add	edx, edi
	sar	edx, 5
	sar	edi, 31
	sub	edx, edi
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm7
	jp	.L1329
	vucomisd	xmm0, xmm8
	je	.L1327
.L1329:
	mov	r14d, 256
.L1343:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r8, xmm0
	movsx	rdx, eax
	mov	rdi, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdi+rdx*8], rcx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L1330
	mov	r10d, eax
.L1333:
	add	r8, rcx
	sar	r8, 56
	test	rcx, rcx
	mov	rcx, -256
	cmovg	rcx, r14
	movsx	rdi, r10d
	sal	rdi, 3
	mov	r11, rdi
	add	r11, QWORD PTR [rsi+8]
	mov	rdx, r8
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r11], rdx
seto r15b
# 0 "" 2
#NO_APP
	add	r8, rcx
	add	r10d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r10d, edx
	jl	.L1332
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1330
.L1332:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+rdi], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	jne	.L1333
.L1330:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1343
	vucomisd	xmm0, xmm4
	jne	.L1343
.L1327:
	add	r9, 8
	cmp	r9, r13
	jne	.L1335
	vxorpd	xmm0, xmm0, xmm0
	mov	rax, QWORD PTR [rbp-104]
	vmovapd	YMMWORD PTR [rax], ymm0
	add	rbx, 32
	cmp	rbx, QWORD PTR [rbp-112]
	jne	.L1336
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1337
	call	__stack_chk_fail
.L1337:
	add	rsp, 64
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7250:
	.size	_ZN15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, .-_ZN15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.text
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.9, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.9:
.LFB7612:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7612
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 320
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r13, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	call	omp_get_thread_num
	mov	r14d, eax
	call	omp_get_num_threads
	mov	ebx, eax
	mov	DWORD PTR [rbp-308], eax
	mov	ecx, r14d
	lea	rax, [0+rcx*8]
	mov	rdi, rcx
	sal	rdi, 6
	sub	rdi, rax
	mov	QWORD PTR [rbp-344], rdi
	mov	QWORD PTR [rbp-336], rdi
	mov	rax, QWORD PTR [r13+8]
	add	rdi, QWORD PTR [rax]
	mov	QWORD PTR [rbp-272], rdi
	vxorpd	xmm0, xmm0, xmm0
	vmovapd	YMMWORD PTR [rbp-112], ymm0
	vmovapd	YMMWORD PTR [rbp-240], ymm0
	vmovapd	YMMWORD PTR [rbp-208], ymm0
	vmovapd	YMMWORD PTR [rbp-176], ymm0
	vmovapd	YMMWORD PTR [rbp-144], ymm0
	mov	eax, r14d
	sal	eax, 4
	mov	eax, eax
	lea	rsi, [0+rax*4]
	mov	QWORD PTR [rbp-320], rsi
	mov	rax, QWORD PTR [r13+16]
	add	rsi, QWORD PTR [rax]
	mov	DWORD PTR [rsi], 0
	movsx	r8, DWORD PTR [r13+24]
	mov	esi, ebx
	mov	rax, rcx
	imul	rax, r8
	cqo
	idiv	rsi
	mov	edi, eax
	and	edi, -8
	lea	eax, [r14+1]
	imul	rax, r8
	cqo
	idiv	rsi
	and	eax, -8
	lea	r15d, [rax-1]
	cmp	edi, r15d
	jl	.L1346
.L1375:
	lea	rdi, [rbp-272]
	call	_ZN15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	mov	rax, QWORD PTR [r13+8]
	mov	rdi, QWORD PTR [rbp-344]
	add	rdi, QWORD PTR [rax]
	call	_ZN16Superaccumulator9NormalizeEv
	mov	rax, QWORD PTR [r13+8]
	mov	QWORD PTR [rbp-328], rax
	mov	r13, QWORD PTR [r13+16]
	cmp	DWORD PTR [rbp-308], 1
	jbe	.L1345
	mov	ebx, 1
	mov	eax, 1
	mov	r15d, 1
.L1351:
	mov	rcx, QWORD PTR [rbp-320]
	add	rcx, QWORD PTR [r13+0]
	mov	edx, DWORD PTR [rcx]
	add	edx, 1
	mov	DWORD PTR [rcx], edx
	mov	edx, ebx
	mov	r12d, r15d
	mov	ecx, ebx
	sal	r12d, cl
	lea	ecx, [r12-1]
	test	ecx, r14d
	jne	.L1348
	or	eax, r14d
	cmp	DWORD PTR [rbp-308], eax
	jbe	.L1348
	mov	ecx, eax
	sal	ecx, 4
	mov	ecx, ecx
	mov	rsi, QWORD PTR [r13+0]
	lea	rcx, [rsi+rcx*4]
	mov	rsi, QWORD PTR [rbp-328]
	mov	rdi, QWORD PTR [rsi]
	mov	eax, eax
	lea	rsi, [0+rax*8]
	sal	rax, 6
	sub	rax, rsi
	lea	rsi, [rdi+rax]
	add	rdi, QWORD PTR [rbp-336]
	prefetcht0	[rcx]
	mov	eax, DWORD PTR [rcx]
	cmp	ebx, eax
	jle	.L1349
.L1381:
	rep nop
	mov	eax, DWORD PTR [rcx]
	cmp	edx, eax
	jg	.L1381
.L1349:
	call	_ZN16Superaccumulator10AccumulateERS_
.L1348:
	mov	eax, r12d
	add	ebx, 1
	cmp	DWORD PTR [rbp-308], r12d
	ja	.L1351
	jmp	.L1345
.L1346:
	lea	rax, [rbp-272]
	lea	r9, [rax+128]
	vmovapd	ymm4, YMMWORD PTR _ZZ10constant8fILin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647EEDv8_fvE1u[rip]
	lea	rax, [rbp-304]
	add	rax, 32
	mov	QWORD PTR [rbp-328], rax
	vxorpd	xmm6, xmm6, xmm6
	vmovsd	xmm5, QWORD PTR .LC15[rip]
	mov	DWORD PTR [rbp-312], r14d
	mov	r14, r13
	mov	r13d, r15d
.L1382:
#APP
# 127 "longacc.cpp" 1
	# myloop
# 0 "" 2
#NO_APP
	movsx	rax, edi
	add	rax, 4
	mov	rdx, QWORD PTR [r14]
	vmovapd	ymm2, YMMWORD PTR [rdx+rax*8]
	vmovapd	ymm3, YMMWORD PTR [rdx-32+rax*8]
	lea	rax, [rbp-272]
.L1354:
	mov	rdx, rax
	vmovapd	ymm1, YMMWORD PTR [rax+32]
	vaddpd	ymm0, ymm1, ymm3
	vandpd	ymm7, ymm1, ymm4
	vandpd	ymm8, ymm3, ymm4
	vcmppd	ymm7, ymm7, ymm8, 1
	vptest	ymm7, ymm3
	je	.L1352
	vblendvpd	ymm8, ymm1, ymm3, ymm7
	vblendvpd	ymm3, ymm3, ymm1, ymm7
	vmovapd	ymm1, ymm8
.L1352:
	vsubpd	ymm1, ymm1, ymm0
	vaddpd	ymm3, ymm1, ymm3
	vaddpd	ymm8, ymm0, ymm2
	vandpd	ymm1, ymm0, ymm4
	vandpd	ymm7, ymm2, ymm4
	vcmppd	ymm1, ymm1, ymm7, 1
	vptest	ymm1, ymm2
	je	.L1353
	vblendvpd	ymm7, ymm0, ymm2, ymm1
	vblendvpd	ymm2, ymm2, ymm0, ymm1
	vmovapd	ymm0, ymm7
.L1353:
	vsubpd	ymm0, ymm0, ymm8
	vaddpd	ymm2, ymm0, ymm2
	vmovapd	YMMWORD PTR [rdx+32], ymm8
	add	rax, 32
	cmp	rax, r9
	jne	.L1354
	vxorpd	xmm0, xmm0, xmm0
	vcmppd	ymm0, ymm3, ymm0, 4
	vtestpd	ymm0, ymm0
	je	.L1355
	vmovupd	YMMWORD PTR [rbp-304], ymm3
	vzeroupper
	lea	r11, [rbp-304]
	vxorpd	xmm10, xmm10, xmm10
	vmovapd	xmm3, xmm10
	vmovapd	xmm7, xmm10
	vmovapd	xmm9, xmm10
	vmovapd	xmm8, xmm10
.L1364:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rsi, QWORD PTR [rbp-272]
	vucomisd	xmm0, xmm6
	jp	.L1379
	vucomisd	xmm0, xmm8
	je	.L1356
.L1379:
	vmovq	rcx, xmm0
	vmovq	r8, xmm0
	shr	r8, 52
	and	r8d, 2047
	sub	r8, 1023
	mov	edx, -1840700269
	mov	eax, r8d
	imul	edx
	add	edx, r8d
	sar	edx, 5
	sar	r8d, 31
	sub	edx, r8d
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm10
	jp	.L1391
	vucomisd	xmm0, xmm9
	je	.L1356
.L1391:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r10, xmm0
	movsx	rdx, eax
	mov	r8, QWORD PTR [rsi+8]
	mov	rcx, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r8+rdx*8], rcx
seto bl
# 0 "" 2
#NO_APP
	test	bl, bl
	je	.L1359
	mov	ebx, eax
.L1362:
	lea	r12, [rcx+r10]
	sar	r12, 56
	test	rcx, rcx
	mov	rcx, -256
	mov	edx, 256
	cmovg	rcx, rdx
	movsx	r8, ebx
	sal	r8, 3
	mov	r10, r8
	add	r10, QWORD PTR [rsi+8]
	mov	rdx, r12
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r10], rdx
seto r15b
# 0 "" 2
#NO_APP
	lea	r10, [r12+rcx]
	add	ebx, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	ebx, edx
	jl	.L1361
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1359
.L1361:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+r8], rcx
seto r12b
# 0 "" 2
#NO_APP
	test	r12b, r12b
	jne	.L1362
.L1359:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm5
	sub	eax, 1
	vucomisd	xmm0, xmm3
	jp	.L1391
	vucomisd	xmm0, xmm7
	jne	.L1391
.L1356:
	add	r11, 8
	cmp	r11, QWORD PTR [rbp-328]
	jne	.L1364
.L1355:
	vxorpd	xmm0, xmm0, xmm0
	vcmppd	ymm0, ymm2, ymm0, 4
	vtestpd	ymm0, ymm0
	je	.L1365
	vmovupd	YMMWORD PTR [rbp-304], ymm2
	vzeroupper
	lea	r11, [rbp-304]
	vxorpd	xmm8, xmm8, xmm8
	vmovapd	xmm11, xmm8
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm8
	vmovapd	xmm7, xmm8
	vmovapd	xmm10, xmm8
	vmovapd	xmm9, xmm8
.L1374:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rsi, QWORD PTR [rbp-272]
	vucomisd	xmm0, xmm8
	jp	.L1380
	vucomisd	xmm0, xmm9
	je	.L1366
.L1380:
	vmovq	rcx, xmm0
	vmovq	r8, xmm0
	shr	r8, 52
	and	r8d, 2047
	sub	r8, 1023
	mov	edx, -1840700269
	mov	eax, r8d
	imul	edx
	add	edx, r8d
	sar	edx, 5
	sar	r8d, 31
	sub	edx, r8d
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm11
	jp	.L1392
	vucomisd	xmm0, xmm10
	je	.L1366
.L1392:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r10, xmm0
	movsx	rdx, eax
	mov	r8, QWORD PTR [rsi+8]
	mov	rcx, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r8+rdx*8], rcx
seto bl
# 0 "" 2
#NO_APP
	test	bl, bl
	je	.L1369
	mov	ebx, eax
.L1372:
	lea	r12, [rcx+r10]
	sar	r12, 56
	test	rcx, rcx
	mov	rcx, -256
	mov	edx, 256
	cmovg	rcx, rdx
	movsx	r8, ebx
	sal	r8, 3
	mov	r10, r8
	add	r10, QWORD PTR [rsi+8]
	mov	rdx, r12
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r10], rdx
seto r15b
# 0 "" 2
#NO_APP
	lea	r10, [r12+rcx]
	add	ebx, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	ebx, edx
	jl	.L1371
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1369
.L1371:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+r8], rcx
seto r12b
# 0 "" 2
#NO_APP
	test	r12b, r12b
	jne	.L1372
.L1369:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1392
	vucomisd	xmm0, xmm7
	jne	.L1392
.L1366:
	add	r11, 8
	cmp	r11, QWORD PTR [rbp-328]
	jne	.L1374
.L1365:
	add	edi, 8
	cmp	r13d, edi
	jg	.L1382
	mov	r13, r14
	mov	r14d, DWORD PTR [rbp-312]
	jmp	.L1375
.L1345:
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1376
	call	__stack_chk_fail
.L1376:
	add	rsp, 320
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7612:
	.section	.gcc_except_table
.LLSDA7612:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7612-.LLSDACSB7612
.LLSDACSB7612:
.LLSDACSE7612:
	.text
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.9, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi4E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.9
	.section	.text._ZN15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,"axG",@progbits,_ZN15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,comdat
	.align 2
	.weak	_ZN15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.type	_ZN15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, @function
_ZN15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv:
.LFB7255:
	.cfi_startproc
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 64
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r12, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	lea	rbx, [rdi+32]
	lea	rax, [rdi+192]
	mov	QWORD PTR [rbp-112], rax
	lea	rax, [rbp-96]
	lea	r13, [rax+32]
	vxorpd	xmm6, xmm6, xmm6
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm6
	vmovapd	xmm4, xmm6
.L1404:
	mov	QWORD PTR [rbp-104], rbx
	vmovapd	ymm0, YMMWORD PTR [rbx]
	vmovupd	YMMWORD PTR [rbp-96], ymm0
	vzeroupper
	lea	r9, [rbp-96]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm8, xmm7
	vmovapd	xmm5, xmm7
.L1403:
	vmovsd	xmm0, QWORD PTR [r9]
	mov	rsi, QWORD PTR [r12]
	vucomisd	xmm0, xmm6
	jp	.L1407
	vucomisd	xmm0, xmm5
	je	.L1395
.L1407:
	vmovq	rcx, xmm0
	vmovq	rdi, xmm0
	shr	rdi, 52
	and	edi, 2047
	sub	rdi, 1023
	mov	edx, -1840700269
	mov	eax, edi
	imul	edx
	add	edx, edi
	sar	edx, 5
	sar	edi, 31
	sub	edx, edi
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm7
	jp	.L1397
	vucomisd	xmm0, xmm8
	je	.L1395
.L1397:
	mov	r14d, 256
.L1411:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r8, xmm0
	movsx	rdx, eax
	mov	rdi, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdi+rdx*8], rcx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L1398
	mov	r10d, eax
.L1401:
	add	r8, rcx
	sar	r8, 56
	test	rcx, rcx
	mov	rcx, -256
	cmovg	rcx, r14
	movsx	rdi, r10d
	sal	rdi, 3
	mov	r11, rdi
	add	r11, QWORD PTR [rsi+8]
	mov	rdx, r8
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r11], rdx
seto r15b
# 0 "" 2
#NO_APP
	add	r8, rcx
	add	r10d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r10d, edx
	jl	.L1400
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1398
.L1400:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+rdi], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	jne	.L1401
.L1398:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1411
	vucomisd	xmm0, xmm4
	jne	.L1411
.L1395:
	add	r9, 8
	cmp	r9, r13
	jne	.L1403
	vxorpd	xmm0, xmm0, xmm0
	mov	rax, QWORD PTR [rbp-104]
	vmovapd	YMMWORD PTR [rax], ymm0
	add	rbx, 32
	cmp	rbx, QWORD PTR [rbp-112]
	jne	.L1404
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1405
	call	__stack_chk_fail
.L1405:
	add	rsp, 64
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7255:
	.size	_ZN15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, .-_ZN15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.text
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.10, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.10:
.LFB7613:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7613
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 352
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r13, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	call	omp_get_thread_num
	mov	r14d, eax
	call	omp_get_num_threads
	mov	ebx, eax
	mov	DWORD PTR [rbp-340], eax
	mov	ecx, r14d
	lea	rax, [0+rcx*8]
	mov	rdi, rcx
	sal	rdi, 6
	sub	rdi, rax
	mov	QWORD PTR [rbp-376], rdi
	mov	QWORD PTR [rbp-368], rdi
	mov	rax, QWORD PTR [r13+8]
	add	rdi, QWORD PTR [rax]
	mov	QWORD PTR [rbp-304], rdi
	vxorpd	xmm0, xmm0, xmm0
	vmovapd	YMMWORD PTR [rbp-112], ymm0
	vmovapd	YMMWORD PTR [rbp-272], ymm0
	vmovapd	YMMWORD PTR [rbp-240], ymm0
	vmovapd	YMMWORD PTR [rbp-208], ymm0
	vmovapd	YMMWORD PTR [rbp-176], ymm0
	vmovapd	YMMWORD PTR [rbp-144], ymm0
	mov	eax, r14d
	sal	eax, 4
	mov	eax, eax
	lea	rsi, [0+rax*4]
	mov	QWORD PTR [rbp-352], rsi
	mov	rax, QWORD PTR [r13+16]
	add	rsi, QWORD PTR [rax]
	mov	DWORD PTR [rsi], 0
	movsx	rdi, DWORD PTR [r13+24]
	mov	esi, ebx
	mov	rax, rcx
	imul	rax, rdi
	cqo
	idiv	rsi
	mov	r8d, eax
	and	r8d, -8
	lea	eax, [r14+1]
	imul	rax, rdi
	cqo
	idiv	rsi
	and	eax, -8
	lea	r15d, [rax-1]
	cmp	r8d, r15d
	jl	.L1414
.L1443:
	lea	rdi, [rbp-304]
	call	_ZN15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	mov	rax, QWORD PTR [r13+8]
	mov	rdi, QWORD PTR [rbp-376]
	add	rdi, QWORD PTR [rax]
	call	_ZN16Superaccumulator9NormalizeEv
	mov	rax, QWORD PTR [r13+8]
	mov	QWORD PTR [rbp-360], rax
	mov	r13, QWORD PTR [r13+16]
	cmp	DWORD PTR [rbp-340], 1
	jbe	.L1413
	mov	ebx, 1
	mov	eax, 1
	mov	r15d, 1
.L1419:
	mov	rcx, QWORD PTR [rbp-352]
	add	rcx, QWORD PTR [r13+0]
	mov	edx, DWORD PTR [rcx]
	add	edx, 1
	mov	DWORD PTR [rcx], edx
	mov	edx, ebx
	mov	r12d, r15d
	mov	ecx, ebx
	sal	r12d, cl
	lea	ecx, [r12-1]
	test	ecx, r14d
	jne	.L1416
	or	eax, r14d
	cmp	DWORD PTR [rbp-340], eax
	jbe	.L1416
	mov	ecx, eax
	sal	ecx, 4
	mov	ecx, ecx
	mov	rsi, QWORD PTR [r13+0]
	lea	rcx, [rsi+rcx*4]
	mov	rsi, QWORD PTR [rbp-360]
	mov	rdi, QWORD PTR [rsi]
	mov	eax, eax
	lea	rsi, [0+rax*8]
	sal	rax, 6
	sub	rax, rsi
	lea	rsi, [rdi+rax]
	add	rdi, QWORD PTR [rbp-368]
	prefetcht0	[rcx]
	mov	eax, DWORD PTR [rcx]
	cmp	ebx, eax
	jle	.L1417
.L1449:
	rep nop
	mov	eax, DWORD PTR [rcx]
	cmp	edx, eax
	jg	.L1449
.L1417:
	call	_ZN16Superaccumulator10AccumulateERS_
.L1416:
	mov	eax, r12d
	add	ebx, 1
	cmp	DWORD PTR [rbp-340], r12d
	ja	.L1419
	jmp	.L1413
.L1414:
	lea	rax, [rbp-304]
	lea	rdi, [rax+160]
	vmovapd	ymm6, YMMWORD PTR _ZZ10constant8fILin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647EEDv8_fvE1u[rip]
	lea	rax, [rbp-336]
	add	rax, 32
	mov	QWORD PTR [rbp-360], rax
	vxorpd	xmm8, xmm8, xmm8
	vmovsd	xmm7, QWORD PTR .LC15[rip]
	mov	DWORD PTR [rbp-344], r14d
	mov	r14, r13
	mov	r13d, r15d
.L1450:
#APP
# 127 "longacc.cpp" 1
	# myloop
# 0 "" 2
#NO_APP
	movsx	rax, r8d
	add	rax, 4
	mov	rdx, QWORD PTR [r14]
	vmovapd	ymm2, YMMWORD PTR [rdx+rax*8]
	vmovapd	ymm3, YMMWORD PTR [rdx-32+rax*8]
	lea	rax, [rbp-304]
.L1422:
	mov	rdx, rax
	vmovapd	ymm1, YMMWORD PTR [rax+32]
	vaddpd	ymm0, ymm1, ymm3
	vandpd	ymm4, ymm1, ymm6
	vandpd	ymm5, ymm3, ymm6
	vcmppd	ymm4, ymm4, ymm5, 1
	vptest	ymm4, ymm3
	je	.L1420
	vblendvpd	ymm5, ymm1, ymm3, ymm4
	vblendvpd	ymm3, ymm3, ymm1, ymm4
	vmovapd	ymm1, ymm5
.L1420:
	vsubpd	ymm1, ymm1, ymm0
	vaddpd	ymm3, ymm1, ymm3
	vaddpd	ymm5, ymm0, ymm2
	vandpd	ymm1, ymm0, ymm6
	vandpd	ymm4, ymm2, ymm6
	vcmppd	ymm1, ymm1, ymm4, 1
	vptest	ymm1, ymm2
	je	.L1421
	vblendvpd	ymm4, ymm0, ymm2, ymm1
	vblendvpd	ymm2, ymm2, ymm0, ymm1
	vmovapd	ymm0, ymm4
.L1421:
	vsubpd	ymm0, ymm0, ymm5
	vaddpd	ymm2, ymm0, ymm2
	vmovapd	YMMWORD PTR [rdx+32], ymm5
	add	rax, 32
	cmp	rax, rdi
	jne	.L1422
	vxorpd	xmm0, xmm0, xmm0
	vcmppd	ymm0, ymm3, ymm0, 4
	vtestpd	ymm0, ymm0
	je	.L1423
	vmovupd	YMMWORD PTR [rbp-336], ymm3
	vzeroupper
	lea	r11, [rbp-336]
	vxorpd	xmm10, xmm10, xmm10
	vmovapd	xmm3, xmm10
	vmovapd	xmm4, xmm10
	vmovapd	xmm9, xmm10
	vmovapd	xmm5, xmm10
.L1432:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rsi, QWORD PTR [rbp-304]
	vucomisd	xmm0, xmm8
	jp	.L1447
	vucomisd	xmm0, xmm5
	je	.L1424
.L1447:
	vmovq	rcx, xmm0
	vmovq	r9, xmm0
	shr	r9, 52
	and	r9d, 2047
	sub	r9, 1023
	mov	edx, -1840700269
	mov	eax, r9d
	imul	edx
	add	edx, r9d
	sar	edx, 5
	sar	r9d, 31
	sub	edx, r9d
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm10
	jp	.L1459
	vucomisd	xmm0, xmm9
	je	.L1424
.L1459:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r10, xmm0
	movsx	rdx, eax
	mov	r9, QWORD PTR [rsi+8]
	mov	rcx, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r9+rdx*8], rcx
seto bl
# 0 "" 2
#NO_APP
	test	bl, bl
	je	.L1427
	mov	ebx, eax
.L1430:
	lea	r12, [rcx+r10]
	sar	r12, 56
	test	rcx, rcx
	mov	rcx, -256
	mov	edx, 256
	cmovg	rcx, rdx
	movsx	r9, ebx
	sal	r9, 3
	mov	r10, r9
	add	r10, QWORD PTR [rsi+8]
	mov	rdx, r12
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r10], rdx
seto r15b
# 0 "" 2
#NO_APP
	lea	r10, [r12+rcx]
	add	ebx, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	ebx, edx
	jl	.L1429
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1427
.L1429:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+r9], rcx
seto r12b
# 0 "" 2
#NO_APP
	test	r12b, r12b
	jne	.L1430
.L1427:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm7
	sub	eax, 1
	vucomisd	xmm0, xmm3
	jp	.L1459
	vucomisd	xmm0, xmm4
	jne	.L1459
.L1424:
	add	r11, 8
	cmp	r11, QWORD PTR [rbp-360]
	jne	.L1432
.L1423:
	vxorpd	xmm0, xmm0, xmm0
	vcmppd	ymm0, ymm2, ymm0, 4
	vtestpd	ymm0, ymm0
	je	.L1433
	vmovupd	YMMWORD PTR [rbp-336], ymm2
	vzeroupper
	lea	r11, [rbp-336]
	vxorpd	xmm9, xmm9, xmm9
	vmovapd	xmm11, xmm9
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm9
	vmovapd	xmm4, xmm9
	vmovapd	xmm10, xmm9
	vmovapd	xmm5, xmm9
.L1442:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rsi, QWORD PTR [rbp-304]
	vucomisd	xmm0, xmm9
	jp	.L1448
	vucomisd	xmm0, xmm5
	je	.L1434
.L1448:
	vmovq	rcx, xmm0
	vmovq	r9, xmm0
	shr	r9, 52
	and	r9d, 2047
	sub	r9, 1023
	mov	edx, -1840700269
	mov	eax, r9d
	imul	edx
	add	edx, r9d
	sar	edx, 5
	sar	r9d, 31
	sub	edx, r9d
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm11
	jp	.L1460
	vucomisd	xmm0, xmm10
	je	.L1434
.L1460:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r10, xmm0
	movsx	rdx, eax
	mov	r9, QWORD PTR [rsi+8]
	mov	rcx, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r9+rdx*8], rcx
seto bl
# 0 "" 2
#NO_APP
	test	bl, bl
	je	.L1437
	mov	ebx, eax
.L1440:
	lea	r12, [rcx+r10]
	sar	r12, 56
	test	rcx, rcx
	mov	rcx, -256
	mov	edx, 256
	cmovg	rcx, rdx
	movsx	r9, ebx
	sal	r9, 3
	mov	r10, r9
	add	r10, QWORD PTR [rsi+8]
	mov	rdx, r12
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r10], rdx
seto r15b
# 0 "" 2
#NO_APP
	lea	r10, [r12+rcx]
	add	ebx, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	ebx, edx
	jl	.L1439
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1437
.L1439:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+r9], rcx
seto r12b
# 0 "" 2
#NO_APP
	test	r12b, r12b
	jne	.L1440
.L1437:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1460
	vucomisd	xmm0, xmm4
	jne	.L1460
.L1434:
	add	r11, 8
	cmp	r11, QWORD PTR [rbp-360]
	jne	.L1442
.L1433:
	add	r8d, 8
	cmp	r13d, r8d
	jg	.L1450
	mov	r13, r14
	mov	r14d, DWORD PTR [rbp-344]
	jmp	.L1443
.L1413:
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1444
	call	__stack_chk_fail
.L1444:
	add	rsp, 352
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7613:
	.section	.gcc_except_table
.LLSDA7613:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7613-.LLSDACSB7613
.LLSDACSB7613:
.LLSDACSE7613:
	.text
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.10, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi5E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.10
	.section	.text._ZN15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,"axG",@progbits,_ZN15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,comdat
	.align 2
	.weak	_ZN15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.type	_ZN15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, @function
_ZN15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv:
.LFB7260:
	.cfi_startproc
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 64
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r12, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	lea	rbx, [rdi+32]
	lea	rax, [rdi+224]
	mov	QWORD PTR [rbp-112], rax
	lea	rax, [rbp-96]
	lea	r13, [rax+32]
	vxorpd	xmm6, xmm6, xmm6
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm6
	vmovapd	xmm4, xmm6
.L1472:
	mov	QWORD PTR [rbp-104], rbx
	vmovapd	ymm0, YMMWORD PTR [rbx]
	vmovupd	YMMWORD PTR [rbp-96], ymm0
	vzeroupper
	lea	r9, [rbp-96]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm8, xmm7
	vmovapd	xmm5, xmm7
.L1471:
	vmovsd	xmm0, QWORD PTR [r9]
	mov	rsi, QWORD PTR [r12]
	vucomisd	xmm0, xmm6
	jp	.L1475
	vucomisd	xmm0, xmm5
	je	.L1463
.L1475:
	vmovq	rcx, xmm0
	vmovq	rdi, xmm0
	shr	rdi, 52
	and	edi, 2047
	sub	rdi, 1023
	mov	edx, -1840700269
	mov	eax, edi
	imul	edx
	add	edx, edi
	sar	edx, 5
	sar	edi, 31
	sub	edx, edi
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm7
	jp	.L1465
	vucomisd	xmm0, xmm8
	je	.L1463
.L1465:
	mov	r14d, 256
.L1479:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r8, xmm0
	movsx	rdx, eax
	mov	rdi, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdi+rdx*8], rcx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L1466
	mov	r10d, eax
.L1469:
	add	r8, rcx
	sar	r8, 56
	test	rcx, rcx
	mov	rcx, -256
	cmovg	rcx, r14
	movsx	rdi, r10d
	sal	rdi, 3
	mov	r11, rdi
	add	r11, QWORD PTR [rsi+8]
	mov	rdx, r8
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r11], rdx
seto r15b
# 0 "" 2
#NO_APP
	add	r8, rcx
	add	r10d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r10d, edx
	jl	.L1468
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1466
.L1468:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+rdi], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	jne	.L1469
.L1466:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1479
	vucomisd	xmm0, xmm4
	jne	.L1479
.L1463:
	add	r9, 8
	cmp	r9, r13
	jne	.L1471
	vxorpd	xmm0, xmm0, xmm0
	mov	rax, QWORD PTR [rbp-104]
	vmovapd	YMMWORD PTR [rax], ymm0
	add	rbx, 32
	cmp	rbx, QWORD PTR [rbp-112]
	jne	.L1472
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1473
	call	__stack_chk_fail
.L1473:
	add	rsp, 64
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7260:
	.size	_ZN15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, .-_ZN15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.text
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.11, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.11:
.LFB7614:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7614
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 384
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r13, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	call	omp_get_thread_num
	mov	r14d, eax
	call	omp_get_num_threads
	mov	edi, eax
	mov	DWORD PTR [rbp-372], eax
	mov	edx, r14d
	lea	rax, [0+rdx*8]
	mov	rcx, rdx
	sal	rcx, 6
	sub	rcx, rax
	mov	QWORD PTR [rbp-408], rcx
	mov	QWORD PTR [rbp-400], rcx
	mov	rax, QWORD PTR [r13+8]
	add	rcx, QWORD PTR [rax]
	mov	QWORD PTR [rbp-336], rcx
	vxorpd	xmm0, xmm0, xmm0
	vmovapd	YMMWORD PTR [rbp-112], ymm0
	vmovapd	YMMWORD PTR [rbp-304], ymm0
	vmovapd	YMMWORD PTR [rbp-272], ymm0
	vmovapd	YMMWORD PTR [rbp-240], ymm0
	vmovapd	YMMWORD PTR [rbp-208], ymm0
	vmovapd	YMMWORD PTR [rbp-176], ymm0
	vmovapd	YMMWORD PTR [rbp-144], ymm0
	mov	eax, r14d
	sal	eax, 4
	mov	eax, eax
	lea	rsi, [0+rax*4]
	mov	QWORD PTR [rbp-384], rsi
	mov	rax, QWORD PTR [r13+16]
	add	rsi, QWORD PTR [rax]
	mov	DWORD PTR [rsi], 0
	movsx	rsi, DWORD PTR [r13+24]
	mov	ecx, edi
	mov	rax, rdx
	imul	rax, rsi
	cqo
	idiv	rcx
	mov	r8d, eax
	and	r8d, -8
	lea	eax, [r14+1]
	imul	rax, rsi
	cqo
	idiv	rcx
	and	eax, -8
	lea	r15d, [rax-1]
	cmp	r8d, r15d
	jl	.L1482
.L1511:
	lea	rdi, [rbp-336]
	call	_ZN15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	mov	rax, QWORD PTR [r13+8]
	mov	rdi, QWORD PTR [rbp-408]
	add	rdi, QWORD PTR [rax]
	call	_ZN16Superaccumulator9NormalizeEv
	mov	rax, QWORD PTR [r13+8]
	mov	QWORD PTR [rbp-392], rax
	mov	r13, QWORD PTR [r13+16]
	cmp	DWORD PTR [rbp-372], 1
	jbe	.L1481
	mov	ebx, 1
	mov	eax, 1
	mov	r15d, 1
.L1487:
	mov	rcx, QWORD PTR [rbp-384]
	add	rcx, QWORD PTR [r13+0]
	mov	edx, DWORD PTR [rcx]
	add	edx, 1
	mov	DWORD PTR [rcx], edx
	mov	edx, ebx
	mov	r12d, r15d
	mov	ecx, ebx
	sal	r12d, cl
	lea	ecx, [r12-1]
	test	ecx, r14d
	jne	.L1484
	or	eax, r14d
	cmp	DWORD PTR [rbp-372], eax
	jbe	.L1484
	mov	ecx, eax
	sal	ecx, 4
	mov	ecx, ecx
	mov	rsi, QWORD PTR [r13+0]
	lea	rcx, [rsi+rcx*4]
	mov	rsi, QWORD PTR [rbp-392]
	mov	rdi, QWORD PTR [rsi]
	mov	eax, eax
	lea	rsi, [0+rax*8]
	sal	rax, 6
	sub	rax, rsi
	lea	rsi, [rdi+rax]
	add	rdi, QWORD PTR [rbp-400]
	prefetcht0	[rcx]
	mov	eax, DWORD PTR [rcx]
	cmp	ebx, eax
	jle	.L1485
.L1517:
	rep nop
	mov	eax, DWORD PTR [rcx]
	cmp	edx, eax
	jg	.L1517
.L1485:
	call	_ZN16Superaccumulator10AccumulateERS_
.L1484:
	mov	eax, r12d
	add	ebx, 1
	cmp	DWORD PTR [rbp-372], r12d
	ja	.L1487
	jmp	.L1481
.L1482:
	lea	rax, [rbp-336]
	lea	rdi, [rax+224]
	vmovapd	ymm6, YMMWORD PTR _ZZ10constant8fILin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647EEDv8_fvE1u[rip]
	lea	rax, [rbp-368]
	add	rax, 32
	mov	QWORD PTR [rbp-392], rax
	vmovsd	xmm7, QWORD PTR .LC15[rip]
	mov	DWORD PTR [rbp-376], r14d
	mov	r14, r13
	mov	r13d, r15d
.L1518:
#APP
# 127 "longacc.cpp" 1
	# myloop
# 0 "" 2
#NO_APP
	movsx	rax, r8d
	add	rax, 4
	mov	rdx, QWORD PTR [r14]
	vmovapd	ymm4, YMMWORD PTR [rdx+rax*8]
	vmovapd	ymm5, YMMWORD PTR [rdx-32+rax*8]
	lea	rax, [rbp-336]
	add	rax, 32
.L1490:
	mov	rdx, rax
	vmovapd	ymm1, YMMWORD PTR [rax]
	vaddpd	ymm0, ymm1, ymm5
	vandpd	ymm2, ymm1, ymm6
	vandpd	ymm3, ymm5, ymm6
	vcmppd	ymm2, ymm2, ymm3, 1
	vptest	ymm2, ymm5
	je	.L1488
	vblendvpd	ymm3, ymm1, ymm5, ymm2
	vblendvpd	ymm5, ymm5, ymm1, ymm2
	vmovapd	ymm1, ymm3
.L1488:
	vsubpd	ymm1, ymm1, ymm0
	vaddpd	ymm5, ymm1, ymm5
	vaddpd	ymm3, ymm0, ymm4
	vandpd	ymm1, ymm0, ymm6
	vandpd	ymm2, ymm4, ymm6
	vcmppd	ymm1, ymm1, ymm2, 1
	vptest	ymm1, ymm4
	je	.L1489
	vblendvpd	ymm2, ymm0, ymm4, ymm1
	vblendvpd	ymm4, ymm4, ymm0, ymm1
	vmovapd	ymm0, ymm2
.L1489:
	vsubpd	ymm0, ymm0, ymm3
	vaddpd	ymm4, ymm0, ymm4
	vmovapd	YMMWORD PTR [rdx], ymm3
	add	rax, 32
	cmp	rax, rdi
	jne	.L1490
	vxorpd	xmm0, xmm0, xmm0
	vcmppd	ymm0, ymm5, ymm0, 4
	vtestpd	ymm0, ymm0
	je	.L1491
	vmovupd	YMMWORD PTR [rbp-368], ymm5
	vzeroupper
	lea	r11, [rbp-368]
	vxorpd	xmm5, xmm5, xmm5
	vmovapd	xmm10, xmm5
	vmovapd	xmm2, xmm5
	vmovapd	xmm3, xmm5
	vmovapd	xmm9, xmm5
	vmovapd	xmm8, xmm5
.L1500:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rsi, QWORD PTR [rbp-336]
	vucomisd	xmm0, xmm5
	jp	.L1515
	vucomisd	xmm0, xmm8
	je	.L1492
.L1515:
	vmovq	rcx, xmm0
	vmovq	r9, xmm0
	shr	r9, 52
	and	r9d, 2047
	sub	r9, 1023
	mov	edx, -1840700269
	mov	eax, r9d
	imul	edx
	add	edx, r9d
	sar	edx, 5
	sar	r9d, 31
	sub	edx, r9d
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm10
	jp	.L1527
	vucomisd	xmm0, xmm9
	je	.L1492
.L1527:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r10, xmm0
	movsx	rdx, eax
	mov	r9, QWORD PTR [rsi+8]
	mov	rcx, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r9+rdx*8], rcx
seto bl
# 0 "" 2
#NO_APP
	test	bl, bl
	je	.L1495
	mov	ebx, eax
.L1498:
	lea	r12, [rcx+r10]
	sar	r12, 56
	test	rcx, rcx
	mov	rcx, -256
	mov	edx, 256
	cmovg	rcx, rdx
	movsx	r9, ebx
	sal	r9, 3
	mov	r10, r9
	add	r10, QWORD PTR [rsi+8]
	mov	rdx, r12
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r10], rdx
seto r15b
# 0 "" 2
#NO_APP
	lea	r10, [r12+rcx]
	add	ebx, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	ebx, edx
	jl	.L1497
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1495
.L1497:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+r9], rcx
seto r12b
# 0 "" 2
#NO_APP
	test	r12b, r12b
	jne	.L1498
.L1495:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm7
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1527
	vucomisd	xmm0, xmm3
	jne	.L1527
.L1492:
	add	r11, 8
	cmp	r11, QWORD PTR [rbp-392]
	jne	.L1500
.L1491:
	vxorpd	xmm0, xmm0, xmm0
	vcmppd	ymm0, ymm4, ymm0, 4
	vtestpd	ymm0, ymm0
	je	.L1501
	vmovupd	YMMWORD PTR [rbp-368], ymm4
	vzeroupper
	lea	r11, [rbp-368]
	vxorpd	xmm5, xmm5, xmm5
	vmovapd	xmm10, xmm5
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm5
	vmovapd	xmm4, xmm5
	vmovapd	xmm9, xmm5
	vmovapd	xmm8, xmm5
.L1510:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rsi, QWORD PTR [rbp-336]
	vucomisd	xmm0, xmm5
	jp	.L1516
	vucomisd	xmm0, xmm8
	je	.L1502
.L1516:
	vmovq	rcx, xmm0
	vmovq	r9, xmm0
	shr	r9, 52
	and	r9d, 2047
	sub	r9, 1023
	mov	edx, -1840700269
	mov	eax, r9d
	imul	edx
	add	edx, r9d
	sar	edx, 5
	sar	r9d, 31
	sub	edx, r9d
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm10
	jp	.L1528
	vucomisd	xmm0, xmm9
	je	.L1502
.L1528:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r10, xmm0
	movsx	rdx, eax
	mov	r9, QWORD PTR [rsi+8]
	mov	rcx, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r9+rdx*8], rcx
seto bl
# 0 "" 2
#NO_APP
	test	bl, bl
	je	.L1505
	mov	ebx, eax
.L1508:
	lea	r12, [rcx+r10]
	sar	r12, 56
	test	rcx, rcx
	mov	rcx, -256
	mov	edx, 256
	cmovg	rcx, rdx
	movsx	r9, ebx
	sal	r9, 3
	mov	r10, r9
	add	r10, QWORD PTR [rsi+8]
	mov	rdx, r12
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r10], rdx
seto r15b
# 0 "" 2
#NO_APP
	lea	r10, [r12+rcx]
	add	ebx, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	ebx, edx
	jl	.L1507
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1505
.L1507:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+r9], rcx
seto r12b
# 0 "" 2
#NO_APP
	test	r12b, r12b
	jne	.L1508
.L1505:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1528
	vucomisd	xmm0, xmm4
	jne	.L1528
.L1502:
	add	r11, 8
	cmp	r11, QWORD PTR [rbp-392]
	jne	.L1510
.L1501:
	add	r8d, 8
	cmp	r13d, r8d
	jg	.L1518
	mov	r13, r14
	mov	r14d, DWORD PTR [rbp-376]
	jmp	.L1511
.L1481:
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1512
	call	__stack_chk_fail
.L1512:
	add	rsp, 384
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7614:
	.section	.gcc_except_table
.LLSDA7614:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7614-.LLSDACSB7614
.LLSDACSB7614:
.LLSDACSE7614:
	.text
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.11, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi6E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.11
	.section	.text._ZN15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,"axG",@progbits,_ZN15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,comdat
	.align 2
	.weak	_ZN15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.type	_ZN15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, @function
_ZN15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv:
.LFB7265:
	.cfi_startproc
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 64
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r12, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	lea	rbx, [rdi+32]
	lea	rax, [rdi+256]
	mov	QWORD PTR [rbp-112], rax
	lea	rax, [rbp-96]
	lea	r13, [rax+32]
	vxorpd	xmm6, xmm6, xmm6
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm6
	vmovapd	xmm4, xmm6
.L1540:
	mov	QWORD PTR [rbp-104], rbx
	vmovapd	ymm0, YMMWORD PTR [rbx]
	vmovupd	YMMWORD PTR [rbp-96], ymm0
	vzeroupper
	lea	r9, [rbp-96]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm8, xmm7
	vmovapd	xmm5, xmm7
.L1539:
	vmovsd	xmm0, QWORD PTR [r9]
	mov	rsi, QWORD PTR [r12]
	vucomisd	xmm0, xmm6
	jp	.L1543
	vucomisd	xmm0, xmm5
	je	.L1531
.L1543:
	vmovq	rcx, xmm0
	vmovq	rdi, xmm0
	shr	rdi, 52
	and	edi, 2047
	sub	rdi, 1023
	mov	edx, -1840700269
	mov	eax, edi
	imul	edx
	add	edx, edi
	sar	edx, 5
	sar	edi, 31
	sub	edx, edi
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm7
	jp	.L1533
	vucomisd	xmm0, xmm8
	je	.L1531
.L1533:
	mov	r14d, 256
.L1547:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r8, xmm0
	movsx	rdx, eax
	mov	rdi, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdi+rdx*8], rcx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L1534
	mov	r10d, eax
.L1537:
	add	r8, rcx
	sar	r8, 56
	test	rcx, rcx
	mov	rcx, -256
	cmovg	rcx, r14
	movsx	rdi, r10d
	sal	rdi, 3
	mov	r11, rdi
	add	r11, QWORD PTR [rsi+8]
	mov	rdx, r8
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r11], rdx
seto r15b
# 0 "" 2
#NO_APP
	add	r8, rcx
	add	r10d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r10d, edx
	jl	.L1536
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1534
.L1536:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+rdi], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	jne	.L1537
.L1534:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1547
	vucomisd	xmm0, xmm4
	jne	.L1547
.L1531:
	add	r9, 8
	cmp	r9, r13
	jne	.L1539
	vxorpd	xmm0, xmm0, xmm0
	mov	rax, QWORD PTR [rbp-104]
	vmovapd	YMMWORD PTR [rax], ymm0
	add	rbx, 32
	cmp	rbx, QWORD PTR [rbp-112]
	jne	.L1540
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1541
	call	__stack_chk_fail
.L1541:
	add	rsp, 64
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7265:
	.size	_ZN15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, .-_ZN15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.text
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.12, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.12:
.LFB7615:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7615
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 416
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r13, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	call	omp_get_thread_num
	mov	r14d, eax
	call	omp_get_num_threads
	mov	DWORD PTR [rbp-404], eax
	mov	eax, r14d
	lea	rdx, [0+rax*8]
	sal	rax, 6
	sub	rax, rdx
	mov	rsi, rax
	mov	QWORD PTR [rbp-440], rax
	mov	QWORD PTR [rbp-432], rax
	mov	rax, QWORD PTR [r13+8]
	add	rsi, QWORD PTR [rax]
	mov	QWORD PTR [rbp-368], rsi
	vxorpd	xmm0, xmm0, xmm0
	vmovapd	YMMWORD PTR [rbp-112], ymm0
	lea	rax, [rbp-336]
	lea	rdx, [rbp-112]
.L1550:
	vmovapd	YMMWORD PTR [rax], ymm0
	add	rax, 32
	cmp	rax, rdx
	jne	.L1550
	mov	eax, r14d
	sal	eax, 4
	mov	eax, eax
	sal	rax, 2
	mov	rsi, rax
	mov	QWORD PTR [rbp-416], rax
	mov	rax, QWORD PTR [r13+16]
	add	rsi, QWORD PTR [rax]
	mov	DWORD PTR [rsi], 0
	movsx	rsi, DWORD PTR [r13+24]
	mov	ecx, DWORD PTR [rbp-404]
	mov	eax, r14d
	imul	rax, rsi
	cqo
	idiv	rcx
	mov	r8d, eax
	and	r8d, -8
	lea	eax, [r14+1]
	imul	rax, rsi
	cqo
	idiv	rcx
	and	eax, -8
	lea	r15d, [rax-1]
	cmp	r8d, r15d
	jl	.L1551
.L1580:
	lea	rdi, [rbp-368]
	call	_ZN15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	mov	rax, QWORD PTR [r13+8]
	mov	rdi, QWORD PTR [rbp-440]
	add	rdi, QWORD PTR [rax]
	call	_ZN16Superaccumulator9NormalizeEv
	mov	rax, QWORD PTR [r13+8]
	mov	QWORD PTR [rbp-424], rax
	mov	r13, QWORD PTR [r13+16]
	cmp	DWORD PTR [rbp-404], 1
	jbe	.L1549
	mov	ebx, 1
	mov	eax, 1
	mov	r15d, 1
.L1556:
	mov	rcx, QWORD PTR [rbp-416]
	add	rcx, QWORD PTR [r13+0]
	mov	edx, DWORD PTR [rcx]
	add	edx, 1
	mov	DWORD PTR [rcx], edx
	mov	edx, ebx
	mov	r12d, r15d
	mov	ecx, ebx
	sal	r12d, cl
	lea	ecx, [r12-1]
	test	ecx, r14d
	jne	.L1553
	or	eax, r14d
	cmp	DWORD PTR [rbp-404], eax
	jbe	.L1553
	mov	ecx, eax
	sal	ecx, 4
	mov	ecx, ecx
	mov	rsi, QWORD PTR [r13+0]
	lea	rcx, [rsi+rcx*4]
	mov	rsi, QWORD PTR [rbp-424]
	mov	rdi, QWORD PTR [rsi]
	mov	eax, eax
	lea	rsi, [0+rax*8]
	sal	rax, 6
	sub	rax, rsi
	lea	rsi, [rdi+rax]
	add	rdi, QWORD PTR [rbp-432]
	prefetcht0	[rcx]
	mov	eax, DWORD PTR [rcx]
	cmp	ebx, eax
	jle	.L1554
.L1586:
	rep nop
	mov	eax, DWORD PTR [rcx]
	cmp	edx, eax
	jg	.L1586
.L1554:
	call	_ZN16Superaccumulator10AccumulateERS_
.L1553:
	mov	eax, r12d
	add	ebx, 1
	cmp	DWORD PTR [rbp-404], r12d
	ja	.L1556
	jmp	.L1549
.L1551:
	lea	rax, [rbp-368]
	lea	rdi, [rax+256]
	vmovapd	ymm6, YMMWORD PTR _ZZ10constant8fILin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647EEDv8_fvE1u[rip]
	lea	rax, [rbp-400]
	add	rax, 32
	mov	QWORD PTR [rbp-424], rax
	vmovsd	xmm7, QWORD PTR .LC15[rip]
	mov	DWORD PTR [rbp-408], r14d
	mov	r14, r13
	mov	r13d, r15d
.L1587:
#APP
# 127 "longacc.cpp" 1
	# myloop
# 0 "" 2
#NO_APP
	movsx	rax, r8d
	add	rax, 4
	mov	rdx, QWORD PTR [r14]
	vmovapd	ymm4, YMMWORD PTR [rdx+rax*8]
	vmovapd	ymm5, YMMWORD PTR [rdx-32+rax*8]
	lea	rax, [rbp-368]
	add	rax, 32
.L1559:
	mov	rdx, rax
	vmovapd	ymm1, YMMWORD PTR [rax]
	vaddpd	ymm0, ymm1, ymm5
	vandpd	ymm2, ymm1, ymm6
	vandpd	ymm3, ymm5, ymm6
	vcmppd	ymm2, ymm2, ymm3, 1
	vptest	ymm2, ymm5
	je	.L1557
	vblendvpd	ymm3, ymm1, ymm5, ymm2
	vblendvpd	ymm5, ymm5, ymm1, ymm2
	vmovapd	ymm1, ymm3
.L1557:
	vsubpd	ymm1, ymm1, ymm0
	vaddpd	ymm5, ymm1, ymm5
	vaddpd	ymm3, ymm0, ymm4
	vandpd	ymm1, ymm0, ymm6
	vandpd	ymm2, ymm4, ymm6
	vcmppd	ymm1, ymm1, ymm2, 1
	vptest	ymm1, ymm4
	je	.L1558
	vblendvpd	ymm2, ymm0, ymm4, ymm1
	vblendvpd	ymm4, ymm4, ymm0, ymm1
	vmovapd	ymm0, ymm2
.L1558:
	vsubpd	ymm0, ymm0, ymm3
	vaddpd	ymm4, ymm0, ymm4
	vmovapd	YMMWORD PTR [rdx], ymm3
	add	rax, 32
	cmp	rax, rdi
	jne	.L1559
	vxorpd	xmm0, xmm0, xmm0
	vcmppd	ymm0, ymm5, ymm0, 4
	vtestpd	ymm0, ymm0
	je	.L1560
	vmovupd	YMMWORD PTR [rbp-400], ymm5
	vzeroupper
	lea	r11, [rbp-400]
	vxorpd	xmm5, xmm5, xmm5
	vmovapd	xmm10, xmm5
	vmovapd	xmm2, xmm5
	vmovapd	xmm3, xmm5
	vmovapd	xmm9, xmm5
	vmovapd	xmm8, xmm5
.L1569:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rsi, QWORD PTR [rbp-368]
	vucomisd	xmm0, xmm5
	jp	.L1584
	vucomisd	xmm0, xmm8
	je	.L1561
.L1584:
	vmovq	rcx, xmm0
	vmovq	r9, xmm0
	shr	r9, 52
	and	r9d, 2047
	sub	r9, 1023
	mov	edx, -1840700269
	mov	eax, r9d
	imul	edx
	add	edx, r9d
	sar	edx, 5
	sar	r9d, 31
	sub	edx, r9d
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm10
	jp	.L1597
	vucomisd	xmm0, xmm9
	je	.L1561
.L1597:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r10, xmm0
	movsx	rdx, eax
	mov	r9, QWORD PTR [rsi+8]
	mov	rcx, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r9+rdx*8], rcx
seto bl
# 0 "" 2
#NO_APP
	test	bl, bl
	je	.L1564
	mov	ebx, eax
.L1567:
	lea	r12, [rcx+r10]
	sar	r12, 56
	test	rcx, rcx
	mov	rcx, -256
	mov	edx, 256
	cmovg	rcx, rdx
	movsx	r9, ebx
	sal	r9, 3
	mov	r10, r9
	add	r10, QWORD PTR [rsi+8]
	mov	rdx, r12
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r10], rdx
seto r15b
# 0 "" 2
#NO_APP
	lea	r10, [r12+rcx]
	add	ebx, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	ebx, edx
	jl	.L1566
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1564
.L1566:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+r9], rcx
seto r12b
# 0 "" 2
#NO_APP
	test	r12b, r12b
	jne	.L1567
.L1564:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm7
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1597
	vucomisd	xmm0, xmm3
	jne	.L1597
.L1561:
	add	r11, 8
	cmp	r11, QWORD PTR [rbp-424]
	jne	.L1569
.L1560:
	vxorpd	xmm0, xmm0, xmm0
	vcmppd	ymm0, ymm4, ymm0, 4
	vtestpd	ymm0, ymm0
	je	.L1570
	vmovupd	YMMWORD PTR [rbp-400], ymm4
	vzeroupper
	lea	r11, [rbp-400]
	vxorpd	xmm5, xmm5, xmm5
	vmovapd	xmm10, xmm5
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm5
	vmovapd	xmm4, xmm5
	vmovapd	xmm9, xmm5
	vmovapd	xmm8, xmm5
.L1579:
	vmovsd	xmm0, QWORD PTR [r11]
	mov	rsi, QWORD PTR [rbp-368]
	vucomisd	xmm0, xmm5
	jp	.L1585
	vucomisd	xmm0, xmm8
	je	.L1571
.L1585:
	vmovq	rcx, xmm0
	vmovq	r9, xmm0
	shr	r9, 52
	and	r9d, 2047
	sub	r9, 1023
	mov	edx, -1840700269
	mov	eax, r9d
	imul	edx
	add	edx, r9d
	sar	edx, 5
	sar	r9d, 31
	sub	edx, r9d
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm10
	jp	.L1598
	vucomisd	xmm0, xmm9
	je	.L1571
.L1598:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r10, xmm0
	movsx	rdx, eax
	mov	r9, QWORD PTR [rsi+8]
	mov	rcx, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r9+rdx*8], rcx
seto bl
# 0 "" 2
#NO_APP
	test	bl, bl
	je	.L1574
	mov	ebx, eax
.L1577:
	lea	r12, [rcx+r10]
	sar	r12, 56
	test	rcx, rcx
	mov	rcx, -256
	mov	edx, 256
	cmovg	rcx, rdx
	movsx	r9, ebx
	sal	r9, 3
	mov	r10, r9
	add	r10, QWORD PTR [rsi+8]
	mov	rdx, r12
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r10], rdx
seto r15b
# 0 "" 2
#NO_APP
	lea	r10, [r12+rcx]
	add	ebx, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	ebx, edx
	jl	.L1576
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1574
.L1576:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+r9], rcx
seto r12b
# 0 "" 2
#NO_APP
	test	r12b, r12b
	jne	.L1577
.L1574:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1598
	vucomisd	xmm0, xmm4
	jne	.L1598
.L1571:
	add	r11, 8
	cmp	r11, QWORD PTR [rbp-424]
	jne	.L1579
.L1570:
	add	r8d, 8
	cmp	r13d, r8d
	jg	.L1587
	mov	r13, r14
	mov	r14d, DWORD PTR [rbp-408]
	jmp	.L1580
.L1549:
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1581
	call	__stack_chk_fail
.L1581:
	add	rsp, 416
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7615:
	.section	.gcc_except_table
.LLSDA7615:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7615-.LLSDACSB7615
.LLSDACSB7615:
.LLSDACSE7615:
	.text
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.12, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi7E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.12
	.section	.text._ZN15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,"axG",@progbits,_ZN15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv,comdat
	.align 2
	.weak	_ZN15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.type	_ZN15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, @function
_ZN15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv:
.LFB7270:
	.cfi_startproc
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 64
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r12, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	lea	rbx, [rdi+32]
	lea	rax, [rdi+288]
	mov	QWORD PTR [rbp-112], rax
	lea	rax, [rbp-96]
	lea	r13, [rax+32]
	vxorpd	xmm6, xmm6, xmm6
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm6
	vmovapd	xmm4, xmm6
.L1610:
	mov	QWORD PTR [rbp-104], rbx
	vmovapd	ymm0, YMMWORD PTR [rbx]
	vmovupd	YMMWORD PTR [rbp-96], ymm0
	vzeroupper
	lea	r9, [rbp-96]
	vxorpd	xmm7, xmm7, xmm7
	vmovapd	xmm8, xmm7
	vmovapd	xmm5, xmm7
.L1609:
	vmovsd	xmm0, QWORD PTR [r9]
	mov	rsi, QWORD PTR [r12]
	vucomisd	xmm0, xmm6
	jp	.L1613
	vucomisd	xmm0, xmm5
	je	.L1601
.L1613:
	vmovq	rcx, xmm0
	vmovq	rdi, xmm0
	shr	rdi, 52
	and	edi, 2047
	sub	rdi, 1023
	mov	edx, -1840700269
	mov	eax, edi
	imul	edx
	add	edx, edi
	sar	edx, 5
	sar	edi, 31
	sub	edx, edi
	mov	eax, edx
	add	eax, DWORD PTR [rsi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rcx
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm7
	jp	.L1603
	vucomisd	xmm0, xmm8
	je	.L1601
.L1603:
	mov	r14d, 256
.L1617:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r8, xmm0
	movsx	rdx, eax
	mov	rdi, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdi+rdx*8], rcx
seto r10b
# 0 "" 2
#NO_APP
	test	r10b, r10b
	je	.L1604
	mov	r10d, eax
.L1607:
	add	r8, rcx
	sar	r8, 56
	test	rcx, rcx
	mov	rcx, -256
	cmovg	rcx, r14
	movsx	rdi, r10d
	sal	rdi, 3
	mov	r11, rdi
	add	r11, QWORD PTR [rsi+8]
	mov	rdx, r8
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r11], rdx
seto r15b
# 0 "" 2
#NO_APP
	add	r8, rcx
	add	r10d, 1
	mov	edx, DWORD PTR [rsi+4]
	add	edx, DWORD PTR [rsi]
	cmp	r10d, edx
	jl	.L1606
	mov	DWORD PTR [rsi+40], 4
	jmp	.L1604
.L1606:
	mov	rdx, QWORD PTR [rsi+8]
	mov	rcx, r8
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+rdi], rcx
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	jne	.L1607
.L1604:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1617
	vucomisd	xmm0, xmm4
	jne	.L1617
.L1601:
	add	r9, 8
	cmp	r9, r13
	jne	.L1609
	vxorpd	xmm0, xmm0, xmm0
	mov	rax, QWORD PTR [rbp-104]
	vmovapd	YMMWORD PTR [rax], ymm0
	add	rbx, 32
	cmp	rbx, QWORD PTR [rbp-112]
	jne	.L1610
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1611
	call	__stack_chk_fail
.L1611:
	add	rsp, 64
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7270:
	.size	_ZN15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv, .-_ZN15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	.text
	.type	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.13, @function
_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.13:
.LFB7616:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA7616
	lea	r10, [rsp+8]
	.cfi_def_cfa 10, 0
	and	rsp, -32
	push	QWORD PTR [r10-8]
	push	rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	mov	rbp, rsp
	push	r15
	push	r14
	push	r13
	push	r12
	push	r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	push	rbx
	sub	rsp, 448
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	mov	r13, rdi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-56], rax
	xor	eax, eax
	call	omp_get_thread_num
	mov	r14d, eax
	call	omp_get_num_threads
	mov	DWORD PTR [rbp-436], eax
	mov	eax, r14d
	lea	rdx, [0+rax*8]
	sal	rax, 6
	sub	rax, rdx
	mov	rbx, rax
	mov	QWORD PTR [rbp-472], rax
	mov	QWORD PTR [rbp-464], rax
	mov	rax, QWORD PTR [r13+8]
	add	rbx, QWORD PTR [rax]
	mov	QWORD PTR [rbp-400], rbx
	vxorpd	xmm0, xmm0, xmm0
	vmovapd	YMMWORD PTR [rbp-112], ymm0
	lea	rax, [rbp-368]
	lea	rdx, [rbp-112]
.L1620:
	vmovapd	YMMWORD PTR [rax], ymm0
	add	rax, 32
	cmp	rax, rdx
	jne	.L1620
	mov	eax, r14d
	sal	eax, 4
	mov	eax, eax
	sal	rax, 2
	mov	rbx, rax
	mov	QWORD PTR [rbp-448], rax
	mov	rax, QWORD PTR [r13+16]
	add	rbx, QWORD PTR [rax]
	mov	DWORD PTR [rbx], 0
	movsx	rsi, DWORD PTR [r13+24]
	mov	ecx, DWORD PTR [rbp-436]
	mov	eax, r14d
	imul	rax, rsi
	cqo
	idiv	rcx
	mov	r8d, eax
	and	r8d, -8
	lea	eax, [r14+1]
	imul	rax, rsi
	cqo
	idiv	rcx
	and	eax, -8
	lea	r15d, [rax-1]
	cmp	r8d, r15d
	jl	.L1621
.L1650:
	lea	rdi, [rbp-400]
	call	_ZN15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEE5FlushEv
	mov	rax, QWORD PTR [r13+8]
	mov	rdi, QWORD PTR [rbp-472]
	add	rdi, QWORD PTR [rax]
	call	_ZN16Superaccumulator9NormalizeEv
	mov	rax, QWORD PTR [r13+8]
	mov	QWORD PTR [rbp-456], rax
	mov	r13, QWORD PTR [r13+16]
	cmp	DWORD PTR [rbp-436], 1
	jbe	.L1619
	mov	ebx, 1
	mov	eax, 1
	mov	r15d, 1
.L1626:
	mov	rcx, QWORD PTR [rbp-448]
	add	rcx, QWORD PTR [r13+0]
	mov	edx, DWORD PTR [rcx]
	add	edx, 1
	mov	DWORD PTR [rcx], edx
	mov	edx, ebx
	mov	r12d, r15d
	mov	ecx, ebx
	sal	r12d, cl
	lea	ecx, [r12-1]
	test	ecx, r14d
	jne	.L1623
	or	eax, r14d
	cmp	DWORD PTR [rbp-436], eax
	jbe	.L1623
	mov	ecx, eax
	sal	ecx, 4
	mov	ecx, ecx
	mov	rsi, QWORD PTR [r13+0]
	lea	rcx, [rsi+rcx*4]
	mov	rdi, QWORD PTR [rbp-456]
	mov	rdi, QWORD PTR [rdi]
	mov	eax, eax
	lea	rsi, [0+rax*8]
	sal	rax, 6
	sub	rax, rsi
	lea	rsi, [rdi+rax]
	add	rdi, QWORD PTR [rbp-464]
	prefetcht0	[rcx]
	mov	eax, DWORD PTR [rcx]
	cmp	ebx, eax
	jle	.L1624
.L1656:
	rep nop
	mov	eax, DWORD PTR [rcx]
	cmp	edx, eax
	jg	.L1656
.L1624:
	call	_ZN16Superaccumulator10AccumulateERS_
.L1623:
	mov	eax, r12d
	add	ebx, 1
	cmp	DWORD PTR [rbp-436], r12d
	ja	.L1626
	jmp	.L1619
.L1621:
	lea	rax, [rbp-400]
	lea	rcx, [rax+288]
	vmovapd	ymm6, YMMWORD PTR _ZZ10constant8fILin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647EEDv8_fvE1u[rip]
	lea	rax, [rbp-432]
	add	rax, 32
	mov	QWORD PTR [rbp-456], rax
	vmovsd	xmm7, QWORD PTR .LC15[rip]
	mov	DWORD PTR [rbp-440], r14d
	mov	r12d, r15d
.L1657:
#APP
# 127 "longacc.cpp" 1
	# myloop
# 0 "" 2
#NO_APP
	movsx	rax, r8d
	add	rax, 4
	mov	rdx, QWORD PTR [r13+0]
	vmovapd	ymm4, YMMWORD PTR [rdx+rax*8]
	vmovapd	ymm5, YMMWORD PTR [rdx-32+rax*8]
	lea	rax, [rbp-400]
	add	rax, 32
.L1629:
	mov	rdx, rax
	vmovapd	ymm1, YMMWORD PTR [rax]
	vaddpd	ymm0, ymm1, ymm5
	vandpd	ymm2, ymm1, ymm6
	vandpd	ymm3, ymm5, ymm6
	vcmppd	ymm2, ymm2, ymm3, 1
	vptest	ymm2, ymm5
	je	.L1627
	vblendvpd	ymm3, ymm1, ymm5, ymm2
	vblendvpd	ymm5, ymm5, ymm1, ymm2
	vmovapd	ymm1, ymm3
.L1627:
	vsubpd	ymm1, ymm1, ymm0
	vaddpd	ymm5, ymm1, ymm5
	vaddpd	ymm3, ymm0, ymm4
	vandpd	ymm1, ymm0, ymm6
	vandpd	ymm2, ymm4, ymm6
	vcmppd	ymm1, ymm1, ymm2, 1
	vptest	ymm1, ymm4
	je	.L1628
	vblendvpd	ymm2, ymm0, ymm4, ymm1
	vblendvpd	ymm4, ymm4, ymm0, ymm1
	vmovapd	ymm0, ymm2
.L1628:
	vsubpd	ymm0, ymm0, ymm3
	vaddpd	ymm4, ymm0, ymm4
	vmovapd	YMMWORD PTR [rdx], ymm3
	add	rax, 32
	cmp	rax, rcx
	jne	.L1629
	vxorpd	xmm0, xmm0, xmm0
	vcmppd	ymm0, ymm5, ymm0, 4
	vtestpd	ymm0, ymm0
	je	.L1630
	vmovupd	YMMWORD PTR [rbp-432], ymm5
	vzeroupper
	lea	r14, [rbp-432]
	vxorpd	xmm8, xmm8, xmm8
	vmovapd	xmm9, xmm8
	vmovapd	xmm2, xmm8
	vmovapd	xmm3, xmm8
	vmovapd	xmm10, xmm8
	vmovapd	xmm5, xmm8
.L1639:
	vmovsd	xmm0, QWORD PTR [r14]
	mov	rdi, QWORD PTR [rbp-400]
	vucomisd	xmm0, xmm8
	jp	.L1654
	vucomisd	xmm0, xmm5
	je	.L1631
.L1654:
	vmovq	rsi, xmm0
	vmovq	r9, xmm0
	shr	r9, 52
	and	r9d, 2047
	sub	r9, 1023
	mov	edx, -1840700269
	mov	eax, r9d
	imul	edx
	add	edx, r9d
	sar	edx, 5
	sar	r9d, 31
	sub	edx, r9d
	mov	eax, edx
	add	eax, DWORD PTR [rdi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rsi
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm9
	jp	.L1667
	vucomisd	xmm0, xmm10
	je	.L1631
.L1667:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r10, xmm0
	movsx	rdx, eax
	mov	r9, QWORD PTR [rdi+8]
	mov	rsi, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r9+rdx*8], rsi
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	je	.L1634
	mov	r11d, eax
.L1637:
	lea	rbx, [rsi+r10]
	sar	rbx, 56
	test	rsi, rsi
	mov	rsi, -256
	mov	edx, 256
	cmovg	rsi, rdx
	movsx	r9, r11d
	sal	r9, 3
	mov	r10, r9
	add	r10, QWORD PTR [rdi+8]
	mov	rdx, rbx
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r10], rdx
seto r15b
# 0 "" 2
#NO_APP
	lea	r10, [rbx+rsi]
	add	r11d, 1
	mov	edx, DWORD PTR [rdi+4]
	add	edx, DWORD PTR [rdi]
	cmp	r11d, edx
	jl	.L1636
	mov	DWORD PTR [rdi+40], 4
	jmp	.L1634
.L1636:
	mov	rdx, QWORD PTR [rdi+8]
	mov	rsi, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+r9], rsi
seto bl
# 0 "" 2
#NO_APP
	test	bl, bl
	jne	.L1637
.L1634:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm7
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1667
	vucomisd	xmm0, xmm3
	jne	.L1667
.L1631:
	add	r14, 8
	cmp	r14, QWORD PTR [rbp-456]
	jne	.L1639
.L1630:
	vxorpd	xmm0, xmm0, xmm0
	vcmppd	ymm0, ymm4, ymm0, 4
	vtestpd	ymm0, ymm0
	je	.L1640
	vmovupd	YMMWORD PTR [rbp-432], ymm4
	vzeroupper
	lea	r14, [rbp-432]
	vxorpd	xmm8, xmm8, xmm8
	vmovapd	xmm9, xmm8
	vmovsd	xmm3, QWORD PTR .LC15[rip]
	vmovapd	xmm2, xmm8
	vmovapd	xmm4, xmm8
	vmovapd	xmm10, xmm8
	vmovapd	xmm5, xmm8
.L1649:
	vmovsd	xmm0, QWORD PTR [r14]
	mov	rdi, QWORD PTR [rbp-400]
	vucomisd	xmm0, xmm8
	jp	.L1655
	vucomisd	xmm0, xmm5
	je	.L1641
.L1655:
	vmovq	rsi, xmm0
	vmovq	r9, xmm0
	shr	r9, 52
	and	r9d, 2047
	sub	r9, 1023
	mov	edx, -1840700269
	mov	eax, r9d
	imul	edx
	add	edx, r9d
	sar	edx, 5
	sar	r9d, 31
	sub	edx, r9d
	mov	eax, edx
	add	eax, DWORD PTR [rdi]
	imul	edx, edx, -56
	sal	rdx, 52
	add	rdx, rsi
	vmovq	xmm0, rdx
	vucomisd	xmm0, xmm9
	jp	.L1668
	vucomisd	xmm0, xmm10
	je	.L1641
.L1668:
#APP
# 85 "mylibm.hpp" 1
	roundsd xmm1, xmm0, 0
# 0 "" 2
#NO_APP
	vcvtsd2si	r10, xmm0
	movsx	rdx, eax
	mov	r9, QWORD PTR [rdi+8]
	mov	rsi, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r9+rdx*8], rsi
seto r11b
# 0 "" 2
#NO_APP
	test	r11b, r11b
	je	.L1644
	mov	r11d, eax
.L1647:
	lea	rbx, [rsi+r10]
	sar	rbx, 56
	test	rsi, rsi
	mov	rsi, -256
	mov	edx, 256
	cmovg	rsi, rdx
	movsx	r9, r11d
	sal	r9, 3
	mov	r10, r9
	add	r10, QWORD PTR [rdi+8]
	mov	rdx, rbx
	sal	rdx, 56
	neg	rdx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r10], rdx
seto r15b
# 0 "" 2
#NO_APP
	lea	r10, [rbx+rsi]
	add	r11d, 1
	mov	edx, DWORD PTR [rdi+4]
	add	edx, DWORD PTR [rdi]
	cmp	r11d, edx
	jl	.L1646
	mov	DWORD PTR [rdi+40], 4
	jmp	.L1644
.L1646:
	mov	rdx, QWORD PTR [rdi+8]
	mov	rsi, r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+r9], rsi
seto bl
# 0 "" 2
#NO_APP
	test	bl, bl
	jne	.L1647
.L1644:
	vsubsd	xmm0, xmm0, xmm1
	vmulsd	xmm0, xmm0, xmm3
	sub	eax, 1
	vucomisd	xmm0, xmm2
	jp	.L1668
	vucomisd	xmm0, xmm4
	jne	.L1668
.L1641:
	add	r14, 8
	cmp	r14, QWORD PTR [rbp-456]
	jne	.L1649
.L1640:
	add	r8d, 8
	cmp	r12d, r8d
	jg	.L1657
	mov	r14d, DWORD PTR [rbp-440]
	jmp	.L1650
.L1619:
	mov	rax, QWORD PTR [rbp-56]
	xor	rax, QWORD PTR fs:40
	je	.L1651
	call	__stack_chk_fail
.L1651:
	add	rsp, 448
	pop	rbx
	pop	r10
	.cfi_def_cfa 10, 0
	pop	r12
	pop	r13
	pop	r14
	pop	r15
	pop	rbp
	lea	rsp, [r10-8]
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7616:
	.section	.gcc_except_table
.LLSDA7616:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7616-.LLSDACSB7616
.LLSDACSB7616:
.LLSDACSE7616:
	.text
	.size	_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.13, .-_Z12TimeOMPCacheI15FPExpansionVectI5Vec4dLi8E17FPExpansionTraitsILb0ELb0ELb0ELb0ELb0ELb1ELb0ELb0EEEEdPdiPm._omp_fn.13
	.section	.text._ZSt18generate_canonicalIdLm53ESt26linear_congruential_engineImLm16807ELm0ELm2147483647EEET_RT1_,"axG",@progbits,_ZSt18generate_canonicalIdLm53ESt26linear_congruential_engineImLm16807ELm0ELm2147483647EEET_RT1_,comdat
	.weak	_ZSt18generate_canonicalIdLm53ESt26linear_congruential_engineImLm16807ELm0ELm2147483647EEET_RT1_
	.type	_ZSt18generate_canonicalIdLm53ESt26linear_congruential_engineImLm16807ELm0ELm2147483647EEET_RT1_, @function
_ZSt18generate_canonicalIdLm53ESt26linear_congruential_engineImLm16807ELm0ELm2147483647EEET_RT1_:
.LFB7537:
	.cfi_startproc
	imul	rsi, QWORD PTR [rdi], 16807
	movabs	rdx, 8589934597
	mov	rax, rsi
	mul	rdx
	mov	rcx, rsi
	sub	rcx, rdx
	shr	rcx
	add	rdx, rcx
	shr	rdx, 30
	mov	rcx, rdx
	sal	rcx, 31
	sub	rcx, rdx
	sub	rsi, rcx
	mov	rdx, rsi
	mov	rax, rsi
	sub	rax, 1
	js	.L1671
	vxorpd	xmm0, xmm0, xmm0
	vcvtsi2sdq	xmm0, xmm0, rax
	jmp	.L1672
.L1671:
	mov	rcx, rax
	shr	rcx
	and	eax, 1
	or	rcx, rax
	vxorpd	xmm0, xmm0, xmm0
	vcvtsi2sdq	xmm0, xmm0, rcx
	vaddsd	xmm0, xmm0, xmm0
.L1672:
	vaddsd	xmm2, xmm0, QWORD PTR .LC14[rip]
	imul	rsi, rdx, 16807
	movabs	rdx, 8589934597
	mov	rax, rsi
	mul	rdx
	mov	rcx, rsi
	sub	rcx, rdx
	shr	rcx
	add	rdx, rcx
	shr	rdx, 30
	mov	rax, rdx
	sal	rax, 31
	sub	rax, rdx
	sub	rsi, rax
	mov	rdx, rsi
	mov	QWORD PTR [rdi], rsi
	sub	rdx, 1
	js	.L1673
	vxorpd	xmm0, xmm0, xmm0
	vcvtsi2sdq	xmm0, xmm0, rdx
	jmp	.L1674
.L1673:
	mov	rax, rdx
	shr	rax
	and	edx, 1
	or	rax, rdx
	vxorpd	xmm1, xmm1, xmm1
	vcvtsi2sdq	xmm1, xmm1, rax
	vaddsd	xmm0, xmm1, xmm1
.L1674:
	vmulsd	xmm0, xmm0, QWORD PTR .LC16[rip]
	vaddsd	xmm0, xmm0, xmm2
	vdivsd	xmm0, xmm0, QWORD PTR .LC17[rip]
	ret
	.cfi_endproc
.LFE7537:
	.size	_ZSt18generate_canonicalIdLm53ESt26linear_congruential_engineImLm16807ELm0ELm2147483647EEET_RT1_, .-_ZSt18generate_canonicalIdLm53ESt26linear_congruential_engineImLm16807ELm0ELm2147483647EEET_RT1_
	.section	.rodata.str1.1
.LC18:
	.string	"default"
	.text
	.globl	_Z14init_lognormalPdidd
	.type	_Z14init_lognormalPdidd, @function
_Z14init_lognormalPdidd:
.LFB6966:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA6966
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 5080
	.cfi_def_cfa_offset 5136
	mov	r13, rdi
	mov	ebx, esi
	vmovsd	QWORD PTR [rsp+16], xmm0
	vmovsd	QWORD PTR [rsp+24], xmm1
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+5064], rax
	xor	eax, eax
	lea	rdx, [rsp+46]
	mov	esi, OFFSET FLAT:.LC18
	lea	rdi, [rsp+48]
.LEHB98:
	call	_ZNSsC1EPKcRKSaIcE
.LEHE98:
	lea	rsi, [rsp+48]
	lea	rdi, [rsp+64]
.LEHB99:
	call	_ZNSt13random_device7_M_initERKSs
.LEHE99:
	mov	rax, QWORD PTR [rsp+48]
	lea	rdi, [rax-24]
	cmp	rdi, OFFSET FLAT:_ZNSs4_Rep20_S_empty_rep_storageE
	je	.L1676
	lea	rdx, [rax-8]
	mov	ecx, OFFSET FLAT:_ZL28__gthrw___pthread_key_createPjPFvPvE
	test	rcx, rcx
	je	.L1677
	mov	eax, -1
	lock xadd	DWORD PTR [rdx], eax
	jmp	.L1678
.L1677:
	mov	edx, DWORD PTR [rax-8]
	lea	ecx, [rdx-1]
	mov	DWORD PTR [rax-8], ecx
	mov	eax, edx
.L1678:
	test	eax, eax
	jg	.L1676
	lea	rsi, [rsp+47]
	call	_ZNSs4_Rep10_M_destroyERKSaIcE
.L1676:
	lea	rdi, [rsp+64]
.LEHB100:
	call	_ZNSt13random_device9_M_getvalEv
.LEHE100:
	mov	esi, eax
	movabs	rdx, 8589934597
	mov	rax, rsi
	mul	rdx
	mov	rcx, rsi
	sub	rcx, rdx
	shr	rcx
	add	rdx, rcx
	shr	rdx, 30
	mov	rax, rdx
	sal	rax, 31
	sub	rax, rdx
	sub	rsi, rax
	mov	rdx, rsi
	mov	eax, 1
	cmove	rdx, rax
	mov	QWORD PTR [rsp+48], rdx
	test	ebx, ebx
	je	.L1681
	sub	ebx, 1
	lea	r12, [rbx+1]
	mov	ebx, 0
	mov	r15d, 0
.L1685:
	movsx	rax, ebx
	lea	rbp, [r13+0+rax*8]
	test	r15b, r15b
	jne	.L1689
.L1694:
	lea	rdi, [rsp+48]
	call	_ZSt18generate_canonicalIdLm53ESt26linear_congruential_engineImLm16807ELm0ELm2147483647EEET_RT1_
	vaddsd	xmm0, xmm0, xmm0
	vsubsd	xmm2, xmm0, QWORD PTR .LC2[rip]
	vmovsd	QWORD PTR [rsp], xmm2
	lea	rdi, [rsp+48]
	call	_ZSt18generate_canonicalIdLm53ESt26linear_congruential_engineImLm16807ELm0ELm2147483647EEET_RT1_
	vaddsd	xmm0, xmm0, xmm0
	vsubsd	xmm3, xmm0, QWORD PTR .LC2[rip]
	vmovsd	QWORD PTR [rsp+8], xmm3
	vmovsd	xmm6, QWORD PTR [rsp]
	vmulsd	xmm1, xmm6, xmm6
	vmulsd	xmm0, xmm3, xmm3
	vaddsd	xmm4, xmm1, xmm0
	vmovq	r14, xmm4
	vucomisd	xmm4, QWORD PTR .LC2[rip]
	ja	.L1694
	vucomisd	xmm4, QWORD PTR .LC14[rip]
	jp	.L1692
	je	.L1694
.L1692:
	vmovq	xmm0, r14
	call	log
	vmulsd	xmm0, xmm0, QWORD PTR .LC19[rip]
	vmovq	xmm6, r14
	vdivsd	xmm0, xmm0, xmm6
	call	sqrt
	vmulsd	xmm7, xmm0, QWORD PTR [rsp]
	vmovq	r14, xmm7
	vmulsd	xmm0, xmm0, QWORD PTR [rsp+8]
	mov	r15d, 1
	jmp	.L1682
.L1689:
	vmovq	xmm0, r14
	mov	r15d, 0
.L1682:
	vaddsd	xmm0, xmm0, QWORD PTR .LC14[rip]
	vmulsd	xmm0, xmm0, QWORD PTR [rsp+24]
	vaddsd	xmm0, xmm0, QWORD PTR [rsp+16]
	call	exp
	vmovsd	QWORD PTR [rbp+0], xmm0
	add	rbx, 1
	cmp	rbx, r12
	jne	.L1685
.L1681:
	lea	rdi, [rsp+64]
	call	_ZNSt13random_device7_M_finiEv
	mov	rax, QWORD PTR [rsp+5064]
	xor	rax, QWORD PTR fs:40
	je	.L1688
	jmp	.L1695
.L1690:
	mov	rbx, rax
	mov	rax, QWORD PTR [rsp+48]
	lea	rdi, [rax-24]
	lea	rsi, [rsp+47]
	call	_ZNSs4_Rep10_M_disposeERKSaIcE
	mov	rdi, rbx
.LEHB101:
	call	_Unwind_Resume
.LEHE101:
.L1691:
	mov	rbx, rax
	lea	rdi, [rsp+64]
	call	_ZNSt13random_device7_M_finiEv
	mov	rdi, rbx
.LEHB102:
	call	_Unwind_Resume
.LEHE102:
.L1695:
	call	__stack_chk_fail
.L1688:
	add	rsp, 5080
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_def_cfa_offset 48
	pop	rbp
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE6966:
	.section	.gcc_except_table
.LLSDA6966:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6966-.LLSDACSB6966
.LLSDACSB6966:
	.uleb128 .LEHB98-.LFB6966
	.uleb128 .LEHE98-.LEHB98
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB99-.LFB6966
	.uleb128 .LEHE99-.LEHB99
	.uleb128 .L1690-.LFB6966
	.uleb128 0
	.uleb128 .LEHB100-.LFB6966
	.uleb128 .LEHE100-.LEHB100
	.uleb128 .L1691-.LFB6966
	.uleb128 0
	.uleb128 .LEHB101-.LFB6966
	.uleb128 .LEHE101-.LEHB101
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB102-.LFB6966
	.uleb128 .LEHE102-.LEHB102
	.uleb128 0
	.uleb128 0
.LLSDACSE6966:
	.text
	.size	_Z14init_lognormalPdidd, .-_Z14init_lognormalPdidd
	.type	_GLOBAL__sub_I__Z17randDoubleUniformv, @function
_GLOBAL__sub_I__Z17randDoubleUniformv:
.LFB7602:
	.cfi_startproc
	sub	rsp, 8
	.cfi_def_cfa_offset 16
	mov	edi, OFFSET FLAT:_ZStL8__ioinit
	call	_ZNSt8ios_base4InitC1Ev
	mov	edx, OFFSET FLAT:__dso_handle
	mov	esi, OFFSET FLAT:_ZStL8__ioinit
	mov	edi, OFFSET FLAT:_ZNSt8ios_base4InitD1Ev
	call	__cxa_atexit
	add	rsp, 8
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE7602:
	.size	_GLOBAL__sub_I__Z17randDoubleUniformv, .-_GLOBAL__sub_I__Z17randDoubleUniformv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__Z17randDoubleUniformv
	.section	.rodata
	.align 32
	.type	_ZZ10constant8fILin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647EEDv8_fvE1u, @object
	.size	_ZZ10constant8fILin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647EEDv8_fvE1u, 32
_ZZ10constant8fILin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647ELin1ELi2147483647EEDv8_fvE1u:
	.long	-1
	.long	2147483647
	.long	-1
	.long	2147483647
	.long	-1
	.long	2147483647
	.long	-1
	.long	2147483647
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	3862712991
	.long	1086856406
	.align 8
.LC1:
	.long	2057431941
	.long	1105178132
	.align 8
.LC2:
	.long	0
	.long	1072693248
	.align 8
.LC14:
	.long	0
	.long	0
	.align 8
.LC15:
	.long	0
	.long	1131413504
	.align 8
.LC16:
	.long	4286578688
	.long	1105199103
	.align 8
.LC17:
	.long	4278190080
	.long	1137704959
	.align 8
.LC19:
	.long	0
	.long	-1073741824
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 4.9.1-16ubuntu6) 4.9.1"
	.section	.note.GNU-stack,"",@progbits
