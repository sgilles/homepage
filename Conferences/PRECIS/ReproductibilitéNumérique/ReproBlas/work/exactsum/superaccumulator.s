	.file	"superaccumulator.cpp"
	.intel_syntax noprefix
	.section	.text.unlikely,"ax",@progbits
	.type	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.part.1, @function
_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.part.1:
.LFB4680:
	.cfi_startproc
	push	rax
	.cfi_def_cfa_offset 16
	mov	rax, QWORD PTR [rdi]
	add	rdi, QWORD PTR [rax-24]
	mov	esi, DWORD PTR [rdi+32]
	or	esi, 1
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
	pop	rdx
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE4680:
	.size	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.part.1, .-_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.part.1
	.text
	.align 2
	.p2align 4,,15
	.globl	_ZN16SuperaccumulatorC2Eii
	.type	_ZN16SuperaccumulatorC2Eii, @function
_ZN16SuperaccumulatorC2Eii:
.LFB4421:
	.cfi_startproc
	push	r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	mov	r8d, -1840700269
	add	esi, 55
	push	rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	push	rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	mov	rbx, rdi
	lea	edi, [rdx+55]
	mov	QWORD PTR [rbx+8], 0
	mov	eax, edi
	mov	QWORD PTR [rbx+16], 0
	imul	r8d
	mov	eax, esi
	mov	QWORD PTR [rbx+24], 0
	lea	ecx, [rdx+rdi]
	imul	r8d
	sar	edi, 31
	sar	ecx, 5
	sub	ecx, edi
	add	edx, esi
	sar	esi, 31
	mov	DWORD PTR [rbx], ecx
	sar	edx, 5
	sub	edx, esi
	add	ecx, edx
	mov	DWORD PTR [rbx+4], edx
	movsx	rdx, ecx
	test	rdx, rdx
	jne	.L11
	mov	QWORD PTR [rbx+24], 0
	xor	ebp, ebp
.L6:
	sub	ecx, 1
	mov	QWORD PTR [rbx+16], rbp
	mov	DWORD PTR [rbx+32], 0
	mov	DWORD PTR [rbx+36], ecx
	mov	DWORD PTR [rbx+40], 0
	mov	QWORD PTR [rbx+48], 255
	pop	rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	pop	rbp
	.cfi_def_cfa_offset 16
	pop	r12
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movabs	rax, 2305843009213693951
	cmp	rdx, rax
	ja	.L12
	lea	r12, [0+rdx*8]
	mov	rdi, r12
	call	_Znwm
	mov	rdx, r12
	xor	esi, esi
	lea	rbp, [rax+r12]
	mov	QWORD PTR [rbx+8], rax
	mov	rdi, rax
	mov	QWORD PTR [rbx+16], rax
	mov	QWORD PTR [rbx+24], rbp
	call	memset
	mov	ecx, DWORD PTR [rbx]
	add	ecx, DWORD PTR [rbx+4]
	jmp	.L6
.L12:
	call	_ZSt17__throw_bad_allocv
	.cfi_endproc
.LFE4421:
	.size	_ZN16SuperaccumulatorC2Eii, .-_ZN16SuperaccumulatorC2Eii
	.globl	_ZN16SuperaccumulatorC1Eii
	.set	_ZN16SuperaccumulatorC1Eii,_ZN16SuperaccumulatorC2Eii
	.align 2
	.p2align 4,,15
	.globl	_ZN16Superaccumulator10AccumulateEli
	.type	_ZN16Superaccumulator10AccumulateEli, @function
_ZN16Superaccumulator10AccumulateEli:
.LFB4423:
	.cfi_startproc
	mov	ecx, DWORD PTR [rdi]
	mov	eax, 56
	mov	r9d, 56
	mov	r11d, DWORD PTR [rdi+36]
	push	r14
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	push	r13
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
	push	r12
	.cfi_def_cfa_offset 32
	.cfi_offset 12, -32
	imul	ecx, eax
	push	rbp
	.cfi_def_cfa_offset 40
	.cfi_offset 6, -40
	push	rbx
	.cfi_def_cfa_offset 48
	.cfi_offset 3, -48
	add	ecx, edx
	mov	edx, 613566757
	mov	eax, ecx
	shr	eax, 3
	mul	edx
	mov	r10d, edx
	mov	r8d, edx
	imul	r10d, r9d
	mov	r9d, DWORD PTR [rdi+32]
	cmp	edx, r9d
	cmovle	r9d, edx
	mov	DWORD PTR [rdi+32], r9d
	lea	r9d, [rdx+2]
	cmp	r9d, r11d
	cmovge	r11d, r9d
	sub	ecx, r10d
	mov	DWORD PTR [rdi+36], r11d
	jne	.L14
	mov	edx, edx
	mov	rcx, QWORD PTR [rdi+8]
	mov	rax, rsi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx+rdx*8], rax
seto r9b
# 0 "" 2
#NO_APP
	test	r9b, r9b
	jne	.L66
.L63:
	pop	rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	pop	rbp
	.cfi_def_cfa_offset 32
	pop	r12
	.cfi_def_cfa_offset 24
	pop	r13
	.cfi_def_cfa_offset 16
	pop	r14
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	mov	r11, rsi
	mov	r8d, edx
	mov	r10, QWORD PTR [rdi+8]
	movabs	rax, 72057594037927935
	sal	r11, cl
	and	r11, rax
	mov	rax, r11
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r10+r8*8], rax
seto bl
# 0 "" 2
#NO_APP
	test	bl, bl
	jne	.L67
.L21:
	mov	eax, 56
	sub	eax, ecx
	mov	ecx, eax
	sar	rsi, cl
	test	rsi, rsi
	je	.L63
	movabs	r10, 72057594037927935
	add	edx, 1
	movsx	r11, edx
	and	r10, rsi
	lea	r8, [0+r11*8]
	mov	rax, r10
	mov	rcx, r8
	add	rcx, QWORD PTR [rdi+8]
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx], rax
seto bl
# 0 "" 2
#NO_APP
	test	bl, bl
	jne	.L68
.L27:
	sar	rsi, 56
	test	rsi, rsi
	je	.L63
	movabs	rax, 72057594037927935
	movsx	r10, r9d
	mov	rdx, QWORD PTR [rdi+8]
	and	rsi, rax
	mov	rax, rsi
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rdx+8+r8], rax
seto cl
# 0 "" 2
#NO_APP
	test	cl, cl
	je	.L63
	lea	rdx, [8+r10*8]
	mov	r8d, 256
	mov	r10, -256
.L34:
	add	rsi, rax
	mov	r11, r8
	sar	rsi, 56
	test	rax, rax
	mov	rax, QWORD PTR [rdi+8]
	cmovle	r11, r10
	mov	rcx, rsi
	sal	rcx, 56
	neg	rcx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax-8+rdx], rcx
seto bl
# 0 "" 2
#NO_APP
	mov	eax, DWORD PTR [rdi]
	add	r9d, 1
	add	rsi, r11
	add	eax, DWORD PTR [rdi+4]
	cmp	r9d, eax
	jge	.L65
	mov	rcx, rdx
	mov	rax, rsi
	add	rcx, QWORD PTR [rdi+8]
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx], rax
seto r11b
# 0 "" 2
#NO_APP
	add	rdx, 8
	test	r11b, r11b
	je	.L63
	jmp	.L34
.L67:
	lea	r8, [8+r8*8]
	mov	ebx, edx
	mov	ebp, 256
	mov	r12, -256
.L25:
	add	r11, rax
	mov	r13, rbp
	sar	r11, 56
	test	rax, rax
	mov	rax, QWORD PTR [rdi+8]
	cmovle	r13, r12
	mov	r10, r11
	sal	r10, 56
	neg	r10
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax-8+r8], r10
seto r14b
# 0 "" 2
#NO_APP
	mov	eax, DWORD PTR [rdi]
	add	ebx, 1
	add	r11, r13
	add	eax, DWORD PTR [rdi+4]
	cmp	ebx, eax
	jge	.L69
	mov	r10, r8
	mov	rax, r11
	add	r10, QWORD PTR [rdi+8]
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r10], rax
seto r13b
# 0 "" 2
#NO_APP
	add	r8, 8
	test	r13b, r13b
	je	.L21
	jmp	.L25
.L66:
	lea	rdx, [8+rdx*8]
	mov	r10, -256
	mov	r9d, 256
.L19:
	add	rsi, rax
	mov	r11, r9
	sar	rsi, 56
	test	rax, rax
	mov	rax, QWORD PTR [rdi+8]
	cmovle	r11, r10
	mov	rcx, rsi
	sal	rcx, 56
	neg	rcx
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax-8+rdx], rcx
seto bl
# 0 "" 2
#NO_APP
	mov	eax, DWORD PTR [rdi]
	add	r8d, 1
	add	rsi, r11
	add	eax, DWORD PTR [rdi+4]
	cmp	r8d, eax
	jge	.L65
	mov	rcx, rdx
	mov	rax, rsi
	add	rcx, QWORD PTR [rdi+8]
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rcx], rax
seto r11b
# 0 "" 2
#NO_APP
	add	rdx, 8
	test	r11b, r11b
	je	.L63
	jmp	.L19
.L68:
	lea	rcx, [8+r11*8]
	mov	rbp, -256
	mov	ebx, 256
.L31:
	add	r10, rax
	mov	r12, rbx
	sar	r10, 56
	test	rax, rax
	mov	rax, QWORD PTR [rdi+8]
	cmovle	r12, rbp
	mov	r11, r10
	sal	r11, 56
	neg	r11
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [rax-8+rcx], r11
seto r13b
# 0 "" 2
#NO_APP
	mov	eax, DWORD PTR [rdi]
	add	edx, 1
	add	r10, r12
	add	eax, DWORD PTR [rdi+4]
	cmp	edx, eax
	jge	.L70
	mov	r11, rcx
	mov	rax, r10
	add	r11, QWORD PTR [rdi+8]
#APP
# 185 "mylibm.hpp" 1
	xadd QWORD PTR [r11], rax
seto r12b
# 0 "" 2
#NO_APP
	add	rcx, 8
	test	r12b, r12b
	je	.L27
	jmp	.L31
.L65:
	mov	DWORD PTR [rdi+40], 4
	jmp	.L63
.L69:
	mov	DWORD PTR [rdi+40], 4
	jmp	.L21
.L70:
	mov	DWORD PTR [rdi+40], 4
	jmp	.L27
	.cfi_endproc
.LFE4423:
	.size	_ZN16Superaccumulator10AccumulateEli, .-_ZN16Superaccumulator10AccumulateEli
	.align 2
	.p2align 4,,15
	.globl	_ZN16Superaccumulator10AccumulateERS_
	.type	_ZN16Superaccumulator10AccumulateERS_, @function
_ZN16Superaccumulator10AccumulateERS_:
.LFB4424:
	.cfi_startproc
	push	r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	mov	rcx, QWORD PTR [rdi+48]
	push	rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	push	rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	mov	rdx, QWORD PTR [rsi+48]
	lea	eax, [rcx+1+rdx]
	cmp	eax, 255
	jle	.L72
	cmp	rcx, 127
	jg	.L73
	cmp	rcx, rdx
	mov	rax, rdx
	jge	.L73
.L74:
	cmp	rax, 127
	jg	.L78
	mov	rdx, QWORD PTR [rdi+48]
	cmp	rdx, rax
	jge	.L79
.L78:
	mov	ecx, DWORD PTR [rsi+32]
	cmp	ecx, DWORD PTR [rsi+36]
	jg	.L108
	mov	r10, QWORD PTR [rsi+8]
	movsx	rbx, ecx
	mov	QWORD PTR [rsi+48], 0
	sal	rbx, 3
	mov	r11d, DWORD PTR [rsi]
	lea	rax, [r10+rbx]
	mov	r9, QWORD PTR [rax]
	add	r11d, DWORD PTR [rsi+4]
	mov	rdx, r9
	sar	rdx, 56
	mov	r8, rdx
	sal	r8, 56
	sub	r9, r8
	mov	QWORD PTR [rax], r9
	lea	r9d, [rcx+1]
	cmp	r9d, r11d
	jge	.L81
	lea	rax, [r10+8+rbx]
	mov	ebx, r11d
	movsx	r9, r9d
	sub	ebx, ecx
	mov	ecx, ebx
	sub	ecx, 2
	add	r9, rcx
	lea	r9, [r10+8+r9*8]
	.p2align 4,,10
	.p2align 3
.L83:
	mov	rcx, rdx
	add	rax, 8
	add	rcx, QWORD PTR [rax-8]
	mov	rdx, rcx
	sar	rdx, 56
	mov	r8, rdx
	sal	r8, 56
	sub	rcx, r8
	mov	QWORD PTR [rax-8], rcx
	cmp	rax, r9
	jne	.L83
	mov	r9d, r11d
.L81:
	sub	r9d, 1
	mov	DWORD PTR [rsi+36], r9d
	movsx	r9, r9d
	add	QWORD PTR [r10+r9*8], r8
	mov	rdx, QWORD PTR [rdi+48]
	mov	rax, QWORD PTR [rsi+48]
.L79:
	lea	eax, [rdx+1+rax]
.L72:
	mov	r8d, DWORD PTR [rdi+32]
	cdqe
	mov	QWORD PTR [rdi+48], rax
	mov	eax, DWORD PTR [rdi+36]
	cmp	DWORD PTR [rsi+32], r8d
	cmovle	r8d, DWORD PTR [rsi+32]
	mov	DWORD PTR [rdi+32], r8d
	cmp	DWORD PTR [rsi+36], eax
	cmovge	eax, DWORD PTR [rsi+36]
	cmp	r8d, eax
	mov	DWORD PTR [rdi+36], eax
	jg	.L106
	mov	rbp, QWORD PTR [rsi+8]
	sub	eax, r8d
	movsx	rcx, r8d
	mov	r10, QWORD PTR [rdi+8]
	lea	rbx, [0+rcx*8]
	mov	r9d, eax
	lea	r11d, [r9+1]
	cmp	r11d, 19
	lea	rdx, [rbp+0+rbx]
	lea	rsi, [r10+rbx]
	jbe	.L86
	lea	rax, [rdx+16]
	cmp	rsi, rax
	lea	rax, [rsi+16]
	setae	r12b
	cmp	rdx, rax
	setae	al
	or	r12b, al
	je	.L86
	mov	r9d, r11d
	xor	eax, eax
	xor	ecx, ecx
	shr	r9d
	lea	r12d, [r9+r9]
.L89:
	vmovdqu	xmm1, XMMWORD PTR [rsi+rax]
	add	ecx, 1
	vmovdqu	xmm0, XMMWORD PTR [rdx+rax]
	vpaddq	xmm0, xmm1, xmm0
	vmovdqu	XMMWORD PTR [rsi+rax], xmm0
	add	rax, 16
	cmp	r9d, ecx
	ja	.L89
	lea	eax, [r8+r12]
	cmp	r11d, r12d
	je	.L90
	cdqe
	mov	rdx, QWORD PTR [rbp+0+rax*8]
	add	QWORD PTR [r10+rax*8], rdx
.L90:
	cmp	QWORD PTR [rdi+48], 127
	jg	.L95
	.p2align 4,,10
	.p2align 3
.L106:
	pop	rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	pop	rbp
	.cfi_def_cfa_offset 16
	pop	r12
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	mov	r11d, DWORD PTR [rdi+32]
	mov	rax, rdx
	cmp	r11d, DWORD PTR [rdi+36]
	jg	.L74
	mov	rbx, QWORD PTR [rdi+8]
	movsx	r9, r11d
	mov	QWORD PTR [rdi+48], 0
	mov	ebp, DWORD PTR [rdi]
	lea	r10d, [r11+1]
	sal	r9, 3
	lea	rax, [rbx+r9]
	mov	rcx, QWORD PTR [rax]
	add	ebp, DWORD PTR [rdi+4]
	mov	rdx, rcx
	sar	rdx, 56
	mov	r8, rdx
	sal	r8, 56
	sub	rcx, r8
	cmp	r10d, ebp
	mov	QWORD PTR [rax], rcx
	jge	.L75
	lea	rax, [rbx+8+r9]
	mov	r8d, ebp
	movsx	rcx, r10d
	sub	r8d, r11d
	sub	r8d, 2
	add	rcx, r8
	lea	r9, [rbx+8+rcx*8]
	.p2align 4,,10
	.p2align 3
.L77:
	add	rdx, QWORD PTR [rax]
	add	rax, 8
	mov	rcx, rdx
	sar	rdx, 56
	mov	r8, rdx
	sal	r8, 56
	sub	rcx, r8
	mov	QWORD PTR [rax-8], rcx
	cmp	rax, r9
	jne	.L77
	lea	r10d, [r10-1+rbp]
	sub	r10d, r11d
.L75:
	sub	r10d, 1
	mov	DWORD PTR [rdi+36], r10d
	movsx	r10, r10d
	add	QWORD PTR [rbx+r10*8], r8
	mov	rax, QWORD PTR [rsi+48]
	jmp	.L74
.L95:
	mov	QWORD PTR [rdi+48], 0
	mov	r11d, DWORD PTR [rdi]
	lea	ebp, [r8+1]
	mov	rax, QWORD PTR [rsi]
	add	r11d, DWORD PTR [rdi+4]
	mov	rdx, rax
	sar	rdx, 56
	mov	r9, rdx
	sal	r9, 56
	sub	rax, r9
	cmp	ebp, r11d
	mov	QWORD PTR [rsi], rax
	jge	.L92
	lea	ecx, [r11-2]
	movsx	rsi, ebp
	lea	rax, [r10+8+rbx]
	sub	ecx, r8d
	add	rcx, rsi
	lea	rsi, [r10+8+rcx*8]
	.p2align 4,,10
	.p2align 3
.L94:
	add	rdx, QWORD PTR [rax]
	add	rax, 8
	mov	rcx, rdx
	sar	rdx, 56
	mov	r9, rdx
	sal	r9, 56
	sub	rcx, r9
	mov	QWORD PTR [rax-8], rcx
	cmp	rax, rsi
	jne	.L94
	sub	r11d, 1
	sub	r11d, r8d
	add	ebp, r11d
.L92:
	sub	ebp, 1
	mov	DWORD PTR [rdi+36], ebp
	movsx	rbp, ebp
	add	QWORD PTR [r10+rbp*8], r9
	pop	rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	pop	rbp
	.cfi_def_cfa_offset 16
	pop	r12
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	mov	rdx, QWORD PTR [rdi+48]
	lea	eax, [rdx+1+rax]
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L86:
	add	rcx, r9
	mov	rax, rsi
	lea	r9, [rbp+8+rcx*8]
	.p2align 4,,10
	.p2align 3
.L91:
	mov	rcx, QWORD PTR [rdx]
	add	rdx, 8
	add	QWORD PTR [rax], rcx
	add	rax, 8
	cmp	rdx, r9
	jne	.L91
	jmp	.L90
	.cfi_endproc
.LFE4424:
	.size	_ZN16Superaccumulator10AccumulateERS_, .-_ZN16Superaccumulator10AccumulateERS_
	.align 2
	.p2align 4,,15
	.globl	_ZN16Superaccumulator5RoundEv
	.type	_ZN16Superaccumulator5RoundEv, @function
_ZN16Superaccumulator5RoundEv:
.LFB4425:
	.cfi_startproc
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	mov	rbp, rsp
	.cfi_def_cfa_register 6
	push	r14
	push	r13
	push	r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	mov	r12, rdi
	push	rbx
	and	rsp, -32
	sub	rsp, 16
	.cfi_offset 3, -48
	mov	esi, DWORD PTR [rdi+32]
	cmp	esi, DWORD PTR [rdi+36]
	jle	.L110
.L154:
	vxorpd	xmm0, xmm0, xmm0
.L192:
	lea	rsp, [rbp-32]
	pop	rbx
	pop	r12
	pop	r13
	pop	r14
	pop	rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	mov	QWORD PTR [rdi+48], 0
	mov	rdi, QWORD PTR [rdi+8]
	movsx	r10, esi
	mov	r9d, DWORD PTR [r12]
	lea	ebx, [rsi+1]
	sal	r10, 3
	mov	r11d, DWORD PTR [r12+4]
	lea	rax, [rdi+r10]
	mov	rcx, QWORD PTR [rax]
	add	r11d, r9d
	mov	rdx, rcx
	sar	rdx, 56
	mov	r8, rdx
	sal	r8, 56
	sub	rcx, r8
	cmp	ebx, r11d
	mov	QWORD PTR [rax], rcx
	jge	.L112
	lea	rax, [rdi+8+r10]
	mov	r8d, r11d
	movsx	rcx, ebx
	sub	r8d, esi
	sub	r8d, 2
	add	rcx, r8
	lea	r10, [rdi+8+rcx*8]
	.p2align 4,,10
	.p2align 3
.L114:
	add	rdx, QWORD PTR [rax]
	add	rax, 8
	mov	rcx, rdx
	sar	rdx, 56
	mov	r8, rdx
	sal	r8, 56
	sub	rcx, r8
	mov	QWORD PTR [rax-8], rcx
	cmp	rax, r10
	jne	.L114
	lea	ebx, [rbx-1+r11]
	sub	ebx, esi
.L112:
	sub	ebx, 1
	shr	rdx, 63
	movsx	rcx, ebx
	mov	DWORD PTR [r12+36], ebx
	mov	r14, rdx
	sal	rcx, 3
	lea	rax, [rdi+rcx]
	add	r8, QWORD PTR [rax]
	test	r8, r8
	mov	r13, r8
	mov	QWORD PTR [rax], r8
	jne	.L115
	cmp	esi, ebx
	jg	.L115
	lea	rax, [rdi-8+rcx]
	.p2align 4,,10
	.p2align 3
.L117:
	mov	r13, QWORD PTR [rax]
	sub	ebx, 1
	test	r13, r13
	jne	.L115
	sub	rax, 8
	cmp	esi, ebx
	jle	.L117
	xor	r13d, r13d
.L115:
	test	r14b, r14b
	jne	.L194
	test	ebx, ebx
	js	.L154
	vcvtsi2sd	xmm1, xmm1, r13
	mov	edi, ebx
	sub	edi, r9d
	mov	r8d, 56
	imul	edi, r8d
	vmovapd	xmm0, xmm1
	vmovsd	QWORD PTR [rsp+8], xmm1
	call	ldexp
	test	ebx, ebx
	vmovsd	xmm1, QWORD PTR [rsp+8]
	vmovapd	xmm2, xmm0
	je	.L192
.L122:
	vmovapd	xmm0, xmm1
	vmovsd	QWORD PTR [rsp+8], xmm2
	call	lrint
	mov	edi, ebx
	mov	esi, 56
	sub	edi, DWORD PTR [r12]
	sub	r13, rax
	vcvtsi2sd	xmm0, xmm0, r13
	imul	edi, esi
	call	ldexp
	mov	edx, DWORD PTR [r12+32]
	lea	esi, [rbx-1]
	vmovapd	xmm3, xmm0
	mov	rcx, QWORD PTR [r12+8]
	vmovsd	xmm2, QWORD PTR [rsp+8]
	cmp	edx, esi
	je	.L195
	test	r14b, r14b
	jne	.L125
	movsx	rax, edx
	mov	r9d, edx
	lea	r10, [rcx+rax*8]
	not	r9d
	mov	rax, r10
	add	r9d, ebx
	and	eax, 31
	shr	rax, 3
	neg	rax
	and	eax, 3
	cmp	r9d, eax
	cmovbe	eax, r9d
	sub	ebx, edx
	lea	r8d, [rbx-1]
	cmp	r8d, 10
	cmovbe	eax, r8d
	test	eax, eax
	mov	edi, eax
	je	.L161
	mov	rax, QWORD PTR [r10]
	lea	r11d, [rdx+1]
	cmp	edi, 1
	jbe	.L170
	movsx	r11, r11d
	or	rax, QWORD PTR [rcx+r11*8]
	lea	r11d, [rdx+2]
	cmp	edi, 2
	jbe	.L170
	movsx	r11, r11d
	or	rax, QWORD PTR [rcx+r11*8]
	lea	r11d, [rdx+3]
	cmp	edi, 3
	jbe	.L170
	movsx	r11, r11d
	or	rax, QWORD PTR [rcx+r11*8]
	lea	r11d, [rdx+4]
	cmp	edi, 4
	jbe	.L170
	movsx	r11, r11d
	or	rax, QWORD PTR [rcx+r11*8]
	lea	r11d, [rdx+5]
	cmp	edi, 5
	jbe	.L170
	movsx	r11, r11d
	or	rax, QWORD PTR [rcx+r11*8]
	lea	r11d, [rdx+6]
	cmp	edi, 6
	jbe	.L170
	movsx	r11, r11d
	or	rax, QWORD PTR [rcx+r11*8]
	lea	r11d, [rdx+7]
	cmp	edi, 7
	jbe	.L170
	movsx	r11, r11d
	or	rax, QWORD PTR [rcx+r11*8]
	lea	r11d, [rdx+8]
	cmp	edi, 8
	jbe	.L170
	movsx	r11, r11d
	or	rax, QWORD PTR [rcx+r11*8]
	lea	r11d, [rdx+9]
	cmp	edi, 9
	jbe	.L170
	movsx	r11, r11d
	add	edx, 10
	or	rax, QWORD PTR [rcx+r11*8]
.L128:
	cmp	r9d, edi
	je	.L144
.L127:
	mov	ebx, r8d
	mov	r13d, edi
	sub	ebx, edi
	mov	r11d, ebx
	shr	r11d, 2
	lea	r9d, [0+r11*4]
	test	r9d, r9d
	je	.L130
	lea	r8, [r10+r13*8]
	xor	edi, edi
	vpxor	xmm0, xmm0, xmm0
.L136:
	add	edi, 1
	vorps	ymm0, ymm0, YMMWORD PTR [r8]
	add	r8, 32
	cmp	r11d, edi
	ja	.L136
	vmovdqa	xmm1, xmm0
	vextractf128	xmm0, ymm0, 0x1
	vmovd	r10, xmm0
	vpextrq	rdi, xmm0, 1
	or	rdi, r10
	vpextrq	r8, xmm1, 1
	or	rax, rdi
	vmovd	rdi, xmm1
	add	edx, r9d
	or	rax, r8
	or	rax, rdi
	cmp	r9d, ebx
	je	.L191
	vzeroupper
.L130:
	movsx	rdi, edx
	or	rax, QWORD PTR [rcx+rdi*8]
	lea	edi, [rdx+1]
	cmp	edi, esi
	je	.L144
	movsx	rdi, edi
	add	edx, 2
	or	rax, QWORD PTR [rcx+rdi*8]
	cmp	edx, esi
	je	.L144
	movsx	rdx, edx
	or	rax, QWORD PTR [rcx+rdx*8]
.L144:
	xor	edx, edx
	test	rax, rax
	setne	dl
.L124:
	test	r14b, r14b
	je	.L146
	movabs	rax, 72057594037927936
	movsx	rdi, esi
	sub	rax, QWORD PTR [rcx+rdi*8]
.L147:
	sub	esi, DWORD PTR [r12]
	or	rax, rdx
	mov	ecx, 56
	vmovsd	QWORD PTR [rsp], xmm2
	vcvtsi2sd	xmm0, xmm0, rax
	vmovsd	QWORD PTR [rsp+8], xmm3
	mov	edi, esi
	imul	edi, ecx
	call	ldexp
	vmovsd	xmm3, QWORD PTR [rsp+8]
	vxorpd	xmm1, xmm1, xmm1
	vmovsd	xmm2, QWORD PTR [rsp]
	vucomisd	xmm3, xmm1
	jp	.L179
	je	.L148
.L179:
	xor	edx, edx
	vucomisd	xmm0, xmm1
	mov	eax, 1
	vaddsd	xmm0, xmm3, xmm0
	setp	dl
	cmove	rax, rdx
	vmovd	rsi, xmm0
	or	rax, rsi
	vmovd	xmm0, rax
.L148:
	test	r14b, r14b
	vaddsd	xmm0, xmm2, xmm0
	je	.L192
	vmovsd	xmm1, QWORD PTR .LC1[rip]
	vxorpd	xmm0, xmm0, xmm1
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L194:
	movabs	rax, 72057594037927935
	and	r13, rax
	cmp	r13, rax
	je	.L196
.L119:
	test	ebx, ebx
	js	.L154
	movabs	r13, 72057594037927936
	movsx	rax, ebx
	sub	r13, QWORD PTR [rdi+rax*8]
	mov	edi, ebx
	sub	edi, r9d
	mov	r9d, 56
	imul	edi, r9d
	vcvtsi2sd	xmm1, xmm1, r13
	vmovapd	xmm0, xmm1
	vmovsd	QWORD PTR [rsp+8], xmm1
	call	ldexp
	test	ebx, ebx
	vmovsd	xmm1, QWORD PTR [rsp+8]
	vmovapd	xmm2, xmm0
	jne	.L122
	vmovsd	xmm0, QWORD PTR .LC1[rip]
	lea	rsp, [rbp-32]
	pop	rbx
	vxorpd	xmm0, xmm0, xmm2
	pop	r12
	pop	r13
	pop	r14
	pop	rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	cmp	esi, ebx
	jg	.L119
	movsx	rax, ebx
	lea	rax, [rdi-8+rax*8]
.L120:
	mov	rdx, r13
	sub	ebx, 1
	and	rdx, QWORD PTR [rax]
	cmp	rdx, r13
	jne	.L119
	sub	rax, 8
	cmp	esi, ebx
	jg	.L119
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L125:
	movsx	rax, edx
	mov	r10d, edx
	lea	r11, [rcx+rax*8]
	not	r10d
	mov	rax, r11
	add	r10d, ebx
	sal	rax, 60
	shr	rax, 63
	cmp	r10d, eax
	cmovbe	eax, r10d
	sub	ebx, edx
	lea	r9d, [rbx-1]
	cmp	r9d, 6
	cmovbe	eax, r9d
	test	eax, eax
	mov	edi, eax
	je	.L172
	movabs	r8, 72057594037927936
	lea	ebx, [rdx+1]
	mov	rax, r8
	sub	rax, QWORD PTR [r11]
	cmp	edi, 1
	jbe	.L177
	movsx	rbx, ebx
	mov	r13, r8
	sub	r13, QWORD PTR [rcx+rbx*8]
	lea	ebx, [rdx+2]
	or	rax, r13
	cmp	edi, 2
	jbe	.L177
	movsx	rbx, ebx
	mov	r13, r8
	sub	r13, QWORD PTR [rcx+rbx*8]
	lea	ebx, [rdx+3]
	or	rax, r13
	cmp	edi, 3
	jbe	.L177
	movsx	rbx, ebx
	mov	r13, r8
	sub	r13, QWORD PTR [rcx+rbx*8]
	lea	ebx, [rdx+4]
	or	rax, r13
	cmp	edi, 4
	jbe	.L177
	movsx	rbx, ebx
	mov	r13, r8
	sub	r13, QWORD PTR [rcx+rbx*8]
	lea	ebx, [rdx+5]
	or	rax, r13
	cmp	edi, 5
	jbe	.L177
	movsx	rbx, ebx
	add	edx, 6
	sub	r8, QWORD PTR [rcx+rbx*8]
	or	rax, r8
.L139:
	cmp	r10d, edi
	je	.L144
.L138:
	sub	r9d, edi
	mov	r8d, edi
	mov	r10d, r9d
	shr	r9d
	mov	ebx, r9d
	add	ebx, ebx
	je	.L141
	vmovdqa	xmm4, XMMWORD PTR .LC2[rip]
	lea	r8, [r11+r8*8]
	xor	edi, edi
	vpxor	xmm1, xmm1, xmm1
.L145:
	vpsubq	xmm0, xmm4, XMMWORD PTR [r8]
	add	edi, 1
	add	r8, 16
	cmp	edi, r9d
	vpor	xmm1, xmm1, xmm0
	jb	.L145
	vpsrldq	xmm0, xmm1, 8
	add	edx, ebx
	vpor	xmm1, xmm0, xmm1
	vmovd	rdi, xmm1
	or	rax, rdi
	cmp	r10d, ebx
	je	.L144
.L141:
	movabs	rdi, 72057594037927936
	movsx	rdx, edx
	sub	rdi, QWORD PTR [rcx+rdx*8]
	or	rax, rdi
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L146:
	movsx	rax, esi
	mov	rax, QWORD PTR [rcx+rax*8]
	jmp	.L147
.L177:
	mov	edx, ebx
	jmp	.L139
.L170:
	mov	edx, r11d
	jmp	.L128
.L172:
	xor	eax, eax
	.p2align 4,,4
	jmp	.L138
.L161:
	xor	eax, eax
	.p2align 4,,4
	jmp	.L127
.L195:
	xor	edx, edx
	.p2align 4,,5
	jmp	.L124
.L191:
	vzeroupper
	.p2align 4,,4
	jmp	.L144
	.cfi_endproc
.LFE4425:
	.size	_ZN16Superaccumulator5RoundEv, .-_ZN16Superaccumulator5RoundEv
	.align 2
	.p2align 4,,15
	.globl	_ZN16Superaccumulator9NormalizeEv
	.type	_ZN16Superaccumulator9NormalizeEv, @function
_ZN16Superaccumulator9NormalizeEv:
.LFB4426:
	.cfi_startproc
	mov	ecx, DWORD PTR [rdi+32]
	xor	eax, eax
	cmp	ecx, DWORD PTR [rdi+36]
	jg	.L203
	mov	r9, QWORD PTR [rdi+8]
	movsx	r11, ecx
	mov	QWORD PTR [rdi+48], 0
	lea	r10d, [rcx+1]
	sal	r11, 3
	lea	rdx, [r9+r11]
	mov	r8, QWORD PTR [rdx]
	mov	rax, r8
	sar	rax, 56
	mov	rsi, rax
	sal	rsi, 56
	sub	r8, rsi
	mov	QWORD PTR [rdx], r8
	mov	r8d, DWORD PTR [rdi]
	add	r8d, DWORD PTR [rdi+4]
	cmp	r10d, r8d
	jge	.L199
	lea	rdx, [r9+8+r11]
	mov	r11d, r8d
	sub	r11d, ecx
	movsx	rcx, r10d
	lea	esi, [r11-2]
	add	rcx, rsi
	lea	r8, [r9+8+rcx*8]
	.p2align 4,,10
	.p2align 3
.L201:
	add	rax, QWORD PTR [rdx]
	add	rdx, 8
	mov	rcx, rax
	sar	rax, 56
	mov	rsi, rax
	sal	rsi, 56
	sub	rcx, rsi
	mov	QWORD PTR [rdx-8], rcx
	cmp	rdx, r8
	jne	.L201
	lea	r10d, [r10-1+r11]
.L199:
	sub	r10d, 1
	shr	rax, 63
	mov	DWORD PTR [rdi+36], r10d
	movsx	r10, r10d
	add	QWORD PTR [r9+r10*8], rsi
.L203:
	rep ret
	.cfi_endproc
.LFE4426:
	.size	_ZN16Superaccumulator9NormalizeEv, .-_ZN16Superaccumulator9NormalizeEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"Exact "
.LC4:
	.string	"Inexact "
.LC5:
	.string	"Overflow "
.LC6:
	.string	"??"
.LC7:
	.string	"+"
.LC8:
	.string	" "
	.text
	.align 2
	.p2align 4,,15
	.globl	_ZN16Superaccumulator4DumpERSo
	.type	_ZN16Superaccumulator4DumpERSo, @function
_ZN16Superaccumulator4DumpERSo:
.LFB4427:
	.cfi_startproc
	push	r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	mov	r15, rdi
	push	r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	push	r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	push	r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	mov	r12, rsi
	push	rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	push	rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	sub	rsp, 8
	.cfi_def_cfa_offset 64
	mov	eax, DWORD PTR [rdi+40]
	cmp	eax, 1
	je	.L206
	cmp	eax, 4
	je	.L207
	test	eax, eax
	je	.L219
	mov	edx, 2
	mov	esi, OFFSET FLAT:.LC6
	mov	rdi, r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.L209:
	mov	rax, QWORD PTR [r12]
	mov	rcx, r12
	mov	edx, DWORD PTR [r15]
	add	rcx, QWORD PTR [rax-24]
	add	edx, DWORD PTR [r15+4]
	mov	eax, DWORD PTR [rcx+24]
	and	eax, -75
	or	eax, 8
	sub	edx, 1
	mov	DWORD PTR [rcx+24], eax
	js	.L216
	movsx	rax, edx
	mov	edx, edx
	lea	rbx, [0+rax*8]
	sub	rax, rdx
	lea	r13, [-8+rax*8]
	.p2align 4,,10
	.p2align 3
.L212:
	mov	rax, QWORD PTR [r15+8]
	mov	esi, OFFSET FLAT:.LC7
	mov	rdi, r12
	mov	rax, QWORD PTR [rax+rbx]
	sub	rbx, 8
	mov	rbp, rax
	sar	rbp, 56
	mov	rdx, rbp
	sal	rdx, 56
	sub	rax, rdx
	mov	edx, 1
	mov	r14, rax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	mov	rsi, rbp
	mov	rdi, r12
	call	_ZNSo9_M_insertIlEERSoT_
	mov	edx, 1
	mov	esi, OFFSET FLAT:.LC8
	mov	rbp, rax
	mov	rdi, rax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	mov	rsi, r14
	mov	rdi, rbp
	call	_ZNSo9_M_insertIlEERSoT_
	cmp	rbx, r13
	jne	.L212
	mov	rax, QWORD PTR [r12]
	mov	rdx, r12
	add	rdx, QWORD PTR [rax-24]
	mov	eax, DWORD PTR [rdx+24]
.L210:
	mov	rbx, QWORD PTR [rdx+240]
	and	eax, -75
	or	eax, 2
	mov	DWORD PTR [rdx+24], eax
	test	rbx, rbx
	je	.L220
	cmp	BYTE PTR [rbx+56], 0
	je	.L214
	movzx	eax, BYTE PTR [rbx+67]
.L215:
	mov	rdi, r12
	movsx	esi, al
	call	_ZNSo3putEc
	add	rsp, 8
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	pop	rbx
	.cfi_def_cfa_offset 48
	mov	rdi, rax
	pop	rbp
	.cfi_def_cfa_offset 40
	pop	r12
	.cfi_def_cfa_offset 32
	pop	r13
	.cfi_def_cfa_offset 24
	pop	r14
	.cfi_def_cfa_offset 16
	pop	r15
	.cfi_def_cfa_offset 8
	jmp	_ZNSo5flushEv
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	mov	edx, 6
	mov	esi, OFFSET FLAT:.LC3
	mov	rdi, r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L214:
	mov	rdi, rbx
	call	_ZNKSt5ctypeIcE13_M_widen_initEv
	mov	rax, QWORD PTR [rbx]
	mov	esi, 10
	mov	rdi, rbx
	call	[QWORD PTR [rax+48]]
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L207:
	mov	edx, 9
	mov	esi, OFFSET FLAT:.LC5
	mov	rdi, r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L206:
	mov	edx, 8
	mov	esi, OFFSET FLAT:.LC4
	mov	rdi, r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.L209
.L216:
	mov	rdx, rcx
	jmp	.L210
.L220:
	.p2align 4,,8
	call	_ZSt16__throw_bad_castv
	.cfi_endproc
.LFE4427:
	.size	_ZN16Superaccumulator4DumpERSo, .-_ZN16Superaccumulator4DumpERSo
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.type	_GLOBAL__sub_I__ZN16SuperaccumulatorC2Eii, @function
_GLOBAL__sub_I__ZN16SuperaccumulatorC2Eii:
.LFB4678:
	.cfi_startproc
	sub	rsp, 8
	.cfi_def_cfa_offset 16
	mov	edi, OFFSET FLAT:_ZStL8__ioinit
	call	_ZNSt8ios_base4InitC1Ev
	mov	edx, OFFSET FLAT:__dso_handle
	mov	esi, OFFSET FLAT:_ZStL8__ioinit
	mov	edi, OFFSET FLAT:_ZNSt8ios_base4InitD1Ev
	add	rsp, 8
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit
	.cfi_endproc
.LFE4678:
	.size	_GLOBAL__sub_I__ZN16SuperaccumulatorC2Eii, .-_GLOBAL__sub_I__ZN16SuperaccumulatorC2Eii
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN16SuperaccumulatorC2Eii
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.align 16
.LC2:
	.quad	72057594037927936
	.quad	72057594037927936
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 4.8.2-19ubuntu1) 4.8.2"
	.section	.note.GNU-stack,"",@progbits
