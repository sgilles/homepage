/*
 *************************************************************************************
 Copyright (c) 2015, University of Perpignan
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************
*/


#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <malloc.h>
#include <math.h>
#include <unistd.h>
#include <limits.h>
#include <sys/types.h>
#include <fcntl.h>
#include <float.h>
#include <mpfr.h>
#include <rtnblas.h>

#define MPFR_RNDN GMP_RNDN

unsigned long long rdtsc();

double rand_double(double M) {
	return (M * (double) rand() / (double) RAND_MAX);
}

double rand_unif(double a, double b) {
	return (a + (b - a) * rand() / (double) RAND_MAX);
}

unsigned int rand_uint(unsigned int M) { //uniform_int(0,M+1)
	return((unsigned int) (((M + 1.0) * rand()) / (RAND_MAX + 1.0)));
}

void copyVector(unsigned int n, double *src, double *dest) {
	for (int i = 0; i < n; i++) dest[i] = src[i];
}

double genDot(double *x, double *y, double *C, unsigned int n, double exp_c) {
	unsigned int i, j, n2 = n / 2;
	double b, s, floatdot, floatabsdot;
	double c = pow(10, exp_c);
	b = log2(c);
	unsigned int b2 = b / 2;
	mpfr_set_default_prec(2100);
	mpfr_t dot, mpfrX, mpfrY, prod, absprod, absdot;
	mpfr_init(dot);
	mpfr_init(absdot);
	mpfr_init(prod);
	mpfr_init(absprod);
	mpfr_init(mpfrX);
	mpfr_init(mpfrY);
	mpfr_set_d(dot, 0, MPFR_RNDN);
	mpfr_set_d(absdot, 0, MPFR_RNDN);
	x[0] = rand_unif(-1, 1) * pow(2, b2 + 1);
	y[0] = rand_unif(-1, 1) * pow(2, b2 + 1);
	mpfr_set_d(mpfrX, x[0], MPFR_RNDN);
	mpfr_set_d(mpfrY, y[0], MPFR_RNDN);
	mpfr_mul(prod, mpfrX, mpfrY, MPFR_RNDN);
	mpfr_add(dot, dot, prod, MPFR_RNDN);
	mpfr_abs(absprod, prod, MPFR_RNDN);
	mpfr_add(absdot, absdot, absprod, MPFR_RNDN);
	x[n2] = rand_unif(-1, 1);
	y[n2] = rand_unif(-1, 1);
	mpfr_set_d(mpfrX, x[b2], MPFR_RNDN);
	mpfr_set_d(mpfrY, y[b2], MPFR_RNDN);
	mpfr_mul(prod, mpfrX, mpfrY, MPFR_RNDN);
	mpfr_add(dot, dot, prod, MPFR_RNDN);
	mpfr_abs(absprod, prod, MPFR_RNDN);
	mpfr_add(absdot, absdot, absprod, MPFR_RNDN);
	for (i = 1; i < n2 - 1; i++) {
		unsigned int exponent = (unsigned int) rand_unif(0, b2 + 1);
		x[i] = rand_unif(-1, 1) * pow(2, exponent);
		y[i] = rand_unif(-1, 1) * pow(2, exponent);
		mpfr_set_d(mpfrX, x[i], MPFR_RNDN);
		mpfr_set_d(mpfrY, y[i], MPFR_RNDN);
		mpfr_mul(prod, mpfrX, mpfrY, MPFR_RNDN);
		mpfr_add(dot, dot, prod, MPFR_RNDN);
		mpfr_abs(absprod, prod, MPFR_RNDN);
		mpfr_add(absdot, absdot, absprod, MPFR_RNDN);
	}
	double realexponent = b2;
	double step = b2 / (double) n2;
	for (i = n2; i < n; i++) {
		floatdot = mpfr_get_d(dot, MPFR_RNDN);
		x[i] = rand_unif(-1.0, 1.0) * pow(2, (unsigned int) realexponent);
		y[i] = (rand_unif(-1.0, 1.0) * pow(2, (unsigned int) realexponent) - floatdot) / x[i];
		mpfr_set_d(mpfrX, x[i], MPFR_RNDN);
		mpfr_set_d(mpfrY, y[i], MPFR_RNDN);
		mpfr_mul(prod, mpfrX, mpfrY, MPFR_RNDN);
		mpfr_add(dot, dot, prod, MPFR_RNDN);
		mpfr_abs(absprod, prod, MPFR_RNDN);
		mpfr_add(absdot, absdot, absprod, MPFR_RNDN);
		realexponent -= step;
	}
	for (i = 1; i < n; i++)
		if ((j = rand_uint(i)) != i) {
			b = x[j];
			x[j] = x[i];
			x[i] = b;
			b = y[j];
			y[j] = y[i];
			y[i] = b;
		}
	floatdot = mpfr_get_d(dot, MPFR_RNDN);
	floatabsdot = mpfr_get_d(absdot, MPFR_RNDN);
	*C = (2 * floatabsdot) / fabs(floatdot);
	return floatdot;
}

int main(int argc, char **argv) {
	unsigned int Cexp; 
	unsigned long n;
	double Cact, dot;
	int r;
	n = atoi(argv[1]);
	Cexp = atoi(argv[2]);
	FILE *file = fopen(argv[3], "wb");
	double *X = (double*) memalign(64, sizeof(double) * n);
	double *Y = (double*) memalign(64, sizeof(double) * n);
	srand(rdtsc());
	rtnblasInitialise();
	dot = genDot(X, Y, &Cact, n, Cexp);
	printf("Requested cond : 10^%d\nCond effective : %g\nDot : %g\n", Cexp, Cact, dot);
	r = fwrite(&n, sizeof(unsigned long), 1, file);
	r = fwrite(X, sizeof(double), n, file);
	r = fwrite(Y, sizeof(double), n, file);
	r = fwrite(&dot, sizeof(double), 1, file);
	r = fwrite(&Cact, sizeof(double), 1, file);
	free(X);
	free(Y);
	fclose(file);
	return EXIT_SUCCESS;
}

