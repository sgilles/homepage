/*
 *************************************************************************************
 Copyright (c) 2015, University of Perpignan
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************
*/

#include <immintrin.h>
#include <stdio.h>
#include <mkl_cblas.h>
#include <summation.h>
#include <rtnblas.h>
#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#ifdef STATS
static unsigned long exponentExtractCycles = 0;
static unsigned long reduceAndUnionCycles = 0;
static unsigned long localSumCycles = 0;
static unsigned long firstPass = 0;

void getRtnBlasStatistics(unsigned long *paramExponentExtractCycles, unsigned long *paramReduceAndUnionCycles, unsigned long *paramLocalSumCycles, unsigned long *paramFirstPass) {
	*paramExponentExtractCycles = exponentExtractCycles;
	*paramReduceAndUnionCycles = reduceAndUnionCycles;
	*paramLocalSumCycles = localSumCycles;
	*paramFirstPass = firstPass;
}
#endif

#ifdef MPI_PARALLEL
#include <mpi.h>
static int MPI_NUMBER_OF_THREADS;
static int myRank;
static MPI_Op MPI_QUAD_SUM;
#endif

#ifdef OMP_PARALLEL
#include <omp.h>
static unsigned int OMP_NUMBER_OF_THREADS;
#endif

#ifdef SSE
#define PDSIZE 2
#define PDVECTOR __m128d
#define addvector _mm_add_pd
#define subvector _mm_sub_pd
#define mulvector _mm_mul_pd
#define andvector _mm_and_pd
#define maxvector _mm_max_pd
#define broadcast(adress) _mm_broadcastsd_pd(*(__m128d*) adress)
#define ZEROES _mm_setzero_pd()
#elif defined(AVX) || defined(AVX2)
#define PDSIZE 4
#define PDVECTOR __m256d
#define addvector _mm256_add_pd
#define subvector _mm256_sub_pd
#define mulvector _mm256_mul_pd
#define andvector _mm256_and_pd
#define maxvector _mm256_max_pd
#define broadcast(adress) _mm256_broadcast_sd(adress)
#define ZEROES _mm256_setzero_pd()
#elif defined(AVX512)
#define PDSIZE 8
#define PDVECTOR __m512d
#define addvector _mm512_add_pd
#define subvector _mm512_sub_pd
#define mulvector _mm512_mul_pd
#define absvector _mm512_abs_pd
#define maxvector _mm512_max_pd
PDVECTOR _mm512_broadcastsd_pd(__m128d);
#ifdef MIC
#define broadcast(adress) _mm512_extload_pd (adress, _MM_UPCONV_PD_NONE, _MM_BROADCAST_1X8, 0)
#else
#define broadcast(adress) _mm512_broadcastsd_pd(*(__m128d*) adress)
#endif
#define ZEROES _mm512_setzero_pd()
#endif

#if defined(AVX2)
#define fmaddvector _mm256_fmadd_pd
#define fmsubvector _mm256_fmsub_pd
#elif defined(AVX512)
#define fmaddvector _mm512_fmadd_pd
#define fmsubvector _mm512_fmsub_pd
#endif

#define L1CACHE 32768
#define L2CACHE 262144
#define CACHELINE 8
#define FPSIZE 8
#define HYBRIDSUMSIZE 2048
#define ONLINEEXACTSIZE 4096
#define MAXTRANSSIZE 40

#ifndef AVX512
static unsigned long ABS_MASK = 0x7FFFFFFFFFFFFFFF;
static PDVECTOR ABS_MASK_VECTOR;
#endif

static unsigned long SPLIT_MASK = 0xFFFFFFFFFC000000;
static double SPLIT_FACTOR, eps, eta;
static PDVECTOR SPLIT_FACTOR_VECTOR;

void rtnblasInitialise() {
	summationInitialise();
	eps = pow(2, -53);
	SPLIT_FACTOR = pow(2, 27) + 1;
	SPLIT_FACTOR_VECTOR = broadcast(&SPLIT_FACTOR);

#ifndef AVX512
	ABS_MASK_VECTOR = broadcast((double*) &ABS_MASK);
#endif

#ifdef OMP_PARALLEL
	OMP_NUMBER_OF_THREADS = atoi(getenv("OMP_NUM_THREADS"));
#endif

#ifdef MPI_PARALLEL
	MPI_Comm_size(MPI_COMM_WORLD, &MPI_NUMBER_OF_THREADS);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
	MPI_Op_create((MPI_User_function *) ADD_QUAD, 1, &MPI_QUAD_SUM);
#endif

}

void initZero(unsigned int size, double* A) {
	int i;
	for (i = 0; i + PDSIZE - 1 < size; i += PDSIZE) {
		*(PDVECTOR*) A = ZEROES;
		A += PDSIZE;
	}
	for (; i < size; i++) {
		A[0] = 0;
		A++;
	}
}

void rtnblas_dcopy(unsigned int size, double* src, int IncX, double* dest, int IntY) {
	unsigned int i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		*(PDVECTOR*) dest = *(PDVECTOR*) src;
#ifndef AVX512
		*(PDVECTOR*) &dest[4] = *(PDVECTOR*) &src[4];
#ifdef SSE
		*(PDVECTOR*) &dest[2] = *(PDVECTOR*) &src[2];
		*(PDVECTOR*) &dest[6] = *(PDVECTOR*) &src[6];
#endif
#endif
		src += CACHELINE;
		dest += CACHELINE;
	}
	for (; i < size; i++) {
		dest[0] = src[0];
		src++;
		dest++;
	}
}

#ifdef SSE
#define splitScallers(X,High,Low,Temp)\
*(PDVECTOR*) &Temp[0] = mulvector(*(PDVECTOR*) &X[0], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[0] = subvector(*(PDVECTOR*) &Temp[0], subvector(*(PDVECTOR*) &Temp[0], *(PDVECTOR*) &X[0]));\
*(PDVECTOR*) &Low[0] = subvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &High[0]);\
*(PDVECTOR*) &Temp[2] = mulvector(*(PDVECTOR*) &X[2], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[2] = subvector(*(PDVECTOR*) &Temp[2], subvector(*(PDVECTOR*) &Temp[2], *(PDVECTOR*) &X[2]));\
*(PDVECTOR*) &Low[2] = subvector(*(PDVECTOR*) &X[2], *(PDVECTOR*) &High[2]);\
*(PDVECTOR*) &Temp[4] = mulvector(*(PDVECTOR*) &X[4], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[4] = subvector(*(PDVECTOR*) &Temp[4], subvector(*(PDVECTOR*) &Temp[4], *(PDVECTOR*) &X[4]));\
*(PDVECTOR*) &Low[4] = subvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &High[4]);\
*(PDVECTOR*) &Temp[6] = mulvector(*(PDVECTOR*) &X[6], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[6] = subvector(*(PDVECTOR*) &Temp[6], subvector(*(PDVECTOR*) &Temp[6], *(PDVECTOR*) &X[6]));\
*(PDVECTOR*) &Low[6] = subvector(*(PDVECTOR*) &X[6], *(PDVECTOR*) &High[6]);
#define twoProd(X,Y,mul,error,XH,XL,XT,YH,YL,YT,Temp)\
*(PDVECTOR*) &mul[0] = mulvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &mul[2] = mulvector(*(PDVECTOR*) &X[2], *(PDVECTOR*) &Y[2]);\
*(PDVECTOR*) &mul[4] = mulvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4]);\
*(PDVECTOR*) &mul[6] = mulvector(*(PDVECTOR*) &X[6], *(PDVECTOR*) &Y[6]);\
splitScallers(X,XH,XL,Temp)\
splitScallers(Y,YH,YL,Temp)\
*(PDVECTOR*) &XT[0] = subvector(mulvector(*(PDVECTOR*) &XH[0], *(PDVECTOR*) &YH[0]), *(PDVECTOR*) &mul[0]);\
*(PDVECTOR*) &YT[0] = addvector(mulvector(*(PDVECTOR*) &XH[0], *(PDVECTOR*) &YL[0]), *(PDVECTOR*) &XT[0]);\
*(PDVECTOR*) &XT[0] = addvector(mulvector(*(PDVECTOR*) &XL[0], *(PDVECTOR*) &YH[0]), *(PDVECTOR*) &YT[0]);\
*(PDVECTOR*) &error[0] = addvector(mulvector(*(PDVECTOR*) &XL[0], *(PDVECTOR*) &YL[0]), *(PDVECTOR*) &XT[0]);\
*(PDVECTOR*) &XT[2] = subvector(mulvector(*(PDVECTOR*) &XH[2], *(PDVECTOR*) &YH[2]), *(PDVECTOR*) &mul[2]);\
*(PDVECTOR*) &YT[2] = addvector(mulvector(*(PDVECTOR*) &XH[2], *(PDVECTOR*) &YL[2]), *(PDVECTOR*) &XT[2]);\
*(PDVECTOR*) &XT[2] = addvector(mulvector(*(PDVECTOR*) &XL[2], *(PDVECTOR*) &YH[2]), *(PDVECTOR*) &YT[2]);\
*(PDVECTOR*) &error[2] = addvector(mulvector(*(PDVECTOR*) &XL[2], *(PDVECTOR*) &YL[2]), *(PDVECTOR*) &XT[2]);\
*(PDVECTOR*) &XT[4] = subvector(mulvector(*(PDVECTOR*) &XH[4], *(PDVECTOR*) &YH[4]), *(PDVECTOR*) &mul[4]);\
*(PDVECTOR*) &YT[4] = addvector(mulvector(*(PDVECTOR*) &XH[4], *(PDVECTOR*) &YL[4]), *(PDVECTOR*) &XT[4]);\
*(PDVECTOR*) &XT[4] = addvector(mulvector(*(PDVECTOR*) &XL[4], *(PDVECTOR*) &YH[4]), *(PDVECTOR*) &YT[4]);\
*(PDVECTOR*) &error[4] = addvector(mulvector(*(PDVECTOR*) &XL[4], *(PDVECTOR*) &YL[4]), *(PDVECTOR*) &XT[4]);\
*(PDVECTOR*) &XT[6] = subvector(mulvector(*(PDVECTOR*) &XH[6], *(PDVECTOR*) &YH[6]), *(PDVECTOR*) &mul[6]);\
*(PDVECTOR*) &YT[6] = addvector(mulvector(*(PDVECTOR*) &XH[6], *(PDVECTOR*) &YL[6]), *(PDVECTOR*) &XT[6]);\
*(PDVECTOR*) &XT[6] = addvector(mulvector(*(PDVECTOR*) &XL[6], *(PDVECTOR*) &YH[6]), *(PDVECTOR*) &YT[6]);\
*(PDVECTOR*) &error[6] = addvector(mulvector(*(PDVECTOR*) &XL[6], *(PDVECTOR*) &YL[6]), *(PDVECTOR*) &XT[6]);
#elif defined(AVX)
#define splitScallers(X,High,Low,Temp)\
*(PDVECTOR*) &Temp[0] = mulvector(*(PDVECTOR*) &X[0], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[0] = subvector(*(PDVECTOR*) &Temp[0], subvector(*(PDVECTOR*) &Temp[0], *(PDVECTOR*) &X[0]));\
*(PDVECTOR*) &Low[0] = subvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &High[0]);\
*(PDVECTOR*) &Temp[4] = mulvector(*(PDVECTOR*) &X[4], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[4] = subvector(*(PDVECTOR*) &Temp[4], subvector(*(PDVECTOR*) &Temp[4], *(PDVECTOR*) &X[4]));\
*(PDVECTOR*) &Low[4] = subvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &High[4]);
#define twoProd(X,Y,mul,error,XH,XL,XT,YH,YL,YT,Temp)\
*(PDVECTOR*) &mul[0] = mulvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &mul[4] = mulvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4]);\
splitScallers(X,XH,XL,Temp)\
splitScallers(Y,YH,YL,Temp)\
*(PDVECTOR*) &XT[0] = subvector(mulvector(*(PDVECTOR*) &XH[0], *(PDVECTOR*) &YH[0]), *(PDVECTOR*) &mul[0]);\
*(PDVECTOR*) &YT[0] = addvector(mulvector(*(PDVECTOR*) &XH[0], *(PDVECTOR*) &YL[0]), *(PDVECTOR*) &XT[0]);\
*(PDVECTOR*) &XT[0] = addvector(mulvector(*(PDVECTOR*) &XL[0], *(PDVECTOR*) &YH[0]), *(PDVECTOR*) &YT[0]);\
*(PDVECTOR*) &error[0] = addvector(mulvector(*(PDVECTOR*) &XL[0], *(PDVECTOR*) &YL[0]), *(PDVECTOR*) &XT[0]);\
*(PDVECTOR*) &XT[4] = subvector(mulvector(*(PDVECTOR*) &XH[4], *(PDVECTOR*) &YH[4]), *(PDVECTOR*) &mul[4]);\
*(PDVECTOR*) &YT[4] = addvector(mulvector(*(PDVECTOR*) &XH[4], *(PDVECTOR*) &YL[4]), *(PDVECTOR*) &XT[4]);\
*(PDVECTOR*) &XT[4] = addvector(mulvector(*(PDVECTOR*) &XL[4], *(PDVECTOR*) &YH[4]), *(PDVECTOR*) &YT[4]);\
*(PDVECTOR*) &error[4] = addvector(mulvector(*(PDVECTOR*) &XL[4], *(PDVECTOR*) &YL[4]), *(PDVECTOR*) &XT[4]);
#elif defined(AVX2)
#define splitScallers(X,High,Low,Temp)\
*(PDVECTOR*) &Temp[0] = mulvector(*(PDVECTOR*) &X[0], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[0] = subvector(*(PDVECTOR*) &Temp[0], subvector(*(PDVECTOR*) &Temp[0], *(PDVECTOR*) &X[0]));\
*(PDVECTOR*) &Low[0] = subvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &High[0]);\
*(PDVECTOR*) &Temp[4] = mulvector(*(PDVECTOR*) &X[4], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[4] = subvector(*(PDVECTOR*) &Temp[4], subvector(*(PDVECTOR*) &Temp[4], *(PDVECTOR*) &X[4]));\
*(PDVECTOR*) &Low[4] = subvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &High[4]);
#define twoProd(X,Y,mul,error)\
*(PDVECTOR*) &mul[0] = mulvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &error[0] = fmsubvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0], *(PDVECTOR*) &mul[0]);\
*(PDVECTOR*) &mul[4] = mulvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4]);\
*(PDVECTOR*) &error[4] = fmsubvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4], *(PDVECTOR*) &mul[4]);
#elif defined(AVX512)
#define splitScallers(X,High,Low,Temp)\
*(PDVECTOR*) &Temp[0] = mulvector(*(PDVECTOR*) &X[0], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[0] = subvector(*(PDVECTOR*) &Temp[0], subvector(*(PDVECTOR*) &Temp[0], *(PDVECTOR*) &X[0]));\
*(PDVECTOR*) &Low[0] = subvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &High[0]);
#define twoProd(X,Y,mul,error)\
*(PDVECTOR*) &mul[0] = mulvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &error[0] = fmsubvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0], *(PDVECTOR*) &mul[0]);
#endif

#ifdef AVX512
#define hybridSumExponentVector(X,L,index1,index2)\
*(PDVECTOR*) exponent = absvector(*(PDVECTOR*) &X);\
*(__m512i*) &index[index1] = _mm512_srli_epi64(*(__m512i*) exponent, 52);\
*(PDVECTOR*) exponent = absvector(*(PDVECTOR*) &L);\
*(__m512i*) &index[index2] = _mm512_srli_epi64(*(__m512i*) exponent, 52);
#endif

#define hybridSumExponent(X,L,index1,index2)\
exponent[index1].data = fabs(X);\
index[index1] = exponent[index1].binary >> 52;\
index[index2] = index[index1] - 27;\
index[index2] = (index[index2] > 0) ? index[index2] : 0;

#define hybridSumAccumulate(H,L,index1,index2)\
C[index[index1]] += H;\
C[index[index2]] += L;

#define splitScaller(X,High,Low,Temp)\
Temp[0] = X[0] * SPLIT_FACTOR;\
High[0] = Temp[0] - (Temp[0] - X[0]);\
Low[0] = X[0] - High[0];

#if defined(AVX2) || (defined(AVX512) && not defined(MIC))
#define twoProdScaller(X,Y,mul,error)\
mul[0] = X[0] * Y[0];\
error[0] = X[0];\
*(__m128d*) &error[0] = _mm_fmadd_sd(*(__m128d*) &error[0], *(__m128d*) &Y[0], *(__m128d*) &mul[0]);
#else
#define twoProdScaller(X,Y,mul,error,XH,XL,XT,YH,YL,YT,Temp)\
mul[0] = X[0] * Y[0];\
splitScaller(X,XH,XL,Temp)\
splitScaller(Y,YH,YL,Temp)\
XT[0] = (XH[0] * YH[0]) - mul[0];\
YT[0] = (XH[0] * YL[0]) + XT[0];\
XT[0] = (XL[0] * YH[0]) + YT[0];\
error[0] = (XL[0] * YL[0]) + XT[0];
#endif

void ddotSequentialExtractHybridSum(unsigned long size, double* X, double* Y, double* C) {
	double __declspec(align(64)) res1[CACHELINE];
	double __declspec(align(64)) res2[CACHELINE];
	double __declspec(align(64)) XH[CACHELINE];
	double __declspec(align(64)) XL[CACHELINE];
	double __declspec(align(64)) YH[CACHELINE];
	double __declspec(align(64)) YL[CACHELINE];
	double __declspec(align(64)) temp[CACHELINE];
#if (not defined(AVX2) && not defined(AVX512)) || defined(MIC)
	double __declspec(align(64)) XT[CACHELINE];
	double __declspec(align(64)) YT[CACHELINE];
#endif
	union binaryDouble exponent[CACHELINE * 2];
	unsigned long index[CACHELINE * 4];
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		// PREFETCH DATA FOR NEXT LOOP
		__builtin_prefetch(&X[8]);
		__builtin_prefetch(&Y[8]);
		// TWOPROD
#if defined(AVX2) || defined(AVX512)
		twoProd(X,Y,res1,res2)
#else
		twoProd(X,Y,res1,res2,XH,XL,XT,YH,YL,YT,temp)
#endif
		// SPLIT RESULT
		splitScallers(res1,XH,XL,temp)
		splitScallers(res2,YH,YL,temp)
		// GET EXPONENT
#ifdef AVX512
		hybridSumExponentVector(XH[0],XL[0],0,16)
		hybridSumExponentVector(YH[0],YL[0],8,24)
#else
		hybridSumExponent(XH[0],XL[0],0,16)
		hybridSumExponent(XH[1],XL[1],1,17)
		hybridSumExponent(XH[2],XL[2],2,18)
		hybridSumExponent(XH[3],XL[3],3,19)
		hybridSumExponent(XH[4],XL[4],4,20)
		hybridSumExponent(XH[5],XL[5],5,21)
		hybridSumExponent(XH[6],XL[6],6,22)
		hybridSumExponent(XH[7],XL[7],7,23)
		hybridSumExponent(YH[0],YL[0],8,24)
		hybridSumExponent(YH[1],YL[1],9,25)
		hybridSumExponent(YH[2],YL[2],10,26)
		hybridSumExponent(YH[3],YL[3],11,27)
		hybridSumExponent(YH[4],YL[4],12,28)
		hybridSumExponent(YH[5],YL[5],13,29)
		hybridSumExponent(YH[6],YL[6],14,30)
		hybridSumExponent(YH[7],YL[7],15,31)
#endif
		// ACCUMULATION
		hybridSumAccumulate(XH[0],XL[0],0,16)
		hybridSumAccumulate(XH[1],XL[1],1,17)
		hybridSumAccumulate(XH[2],XL[2],2,18)
		hybridSumAccumulate(XH[3],XL[3],3,19)
		hybridSumAccumulate(XH[4],XL[4],4,20)
		hybridSumAccumulate(XH[5],XL[5],5,21)
		hybridSumAccumulate(XH[6],XL[6],6,22)
		hybridSumAccumulate(XH[7],XL[7],7,23)
		hybridSumAccumulate(YH[0],YL[0],8,24)
		hybridSumAccumulate(YH[1],YL[1],9,25)
		hybridSumAccumulate(YH[2],YL[2],10,26)
		hybridSumAccumulate(YH[3],YL[3],11,27)
		hybridSumAccumulate(YH[4],YL[4],12,28)
		hybridSumAccumulate(YH[5],YL[5],13,29)
		hybridSumAccumulate(YH[6],YL[6],14,30)
		hybridSumAccumulate(YH[7],YL[7],15,31)
		X += CACHELINE;
		Y += CACHELINE;
	}
	for (; i < size; i++) {
#if defined(AVX2) || (defined(AVX512) && not defined(MIC))
		twoProdScaller(X,Y,res1,res2)
#else
		twoProdScaller(X,Y,res1,res2,XH,XL,XT,YH,YL,YT,temp)
#endif
		splitScaller(res1,XH,XL,temp)
		splitScaller(res2,YH,YL,temp)
		hybridSumExponent(XH[0],XL[0],0,1)
		hybridSumExponent(YH[0],YL[0],2,3)
		hybridSumAccumulate(XH[0],XL[0],0,1)
		hybridSumAccumulate(YH[0],YL[0],2,3)
		X++;
		Y++;
	}
}

#define fastTwoAcc(var,i)\
temp[i] = C[index[i]] + var;\
C[index[i] + 1] += (var - (temp[i] - C[index[i]]));\
C[index[i]] = temp[i];

#ifdef AVX512
#define onlineExponentVector(index1,index2)\
*(PDVECTOR*) exponent = absvector(*(PDVECTOR*) &res1[index1]);\
*(__m512i*) &index[index1] = _mm512_slli_epi64(_mm512_srli_epi64(*(__m512i*) exponent, 52), 1);\
*(PDVECTOR*) exponent = absvector(*(PDVECTOR*) &res2[index1]);\
*(__m512i*) &index[index2] = _mm512_slli_epi64(_mm512_srli_epi64(*(__m512i*) exponent, 52), 1);
#endif

#define onlineExponent(index1,index2)\
exponent[index1].data = fabs(res1[index1]);\
index[index1] = (exponent[index1].binary >> 52) * 2;\
index[index2] = index[index1] - 106;\
index[index2] = (index[index2] > 0) ? index[index2] : 0;

#define onlineFasttwosum(index1,index2)\
fastTwoAcc(res1[index1],index1)\
fastTwoAcc(res2[index1],index2)

void ddotSequentialExtractOnlineExact(unsigned long size, double* X, double* Y, double* C) {
	double __declspec(align(64)) res1[CACHELINE];
	double __declspec(align(64)) res2[CACHELINE];
	double __declspec(align(64)) temp[CACHELINE * 2];

#if (not defined(AVX2) && not defined(AVX512)) || defined(MIC)
	double __declspec(align(64)) XH[CACHELINE];
	double __declspec(align(64)) XL[CACHELINE];
	double __declspec(align(64)) YH[CACHELINE];
	double __declspec(align(64)) YL[CACHELINE];
	double __declspec(align(64)) XT[CACHELINE];
	double __declspec(align(64)) YT[CACHELINE];
#endif

	union binaryDouble exponent[CACHELINE];
	unsigned long index[CACHELINE * 2];
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		// PREFETCH DATA FOR NEXT LOOP
		__builtin_prefetch(&X[8]);
		__builtin_prefetch(&Y[8]);
		// TWOPROD
#if defined(AVX2) || defined(AVX512)
		twoProd(X,Y,res1,res2)
#else
		twoProd(X,Y,res1,res2,XH,XL,XT,YH,YL,YT,temp)
#endif
		// EXPONENT AND ACCUMULATION
#ifdef AVX512
		onlineExponentVector(0,8)
#endif
		onlineExponent(0,8)
		onlineExponent(1,9)
		onlineExponent(2,10)
		onlineExponent(3,11)
		onlineExponent(4,12)
		onlineExponent(5,13)
		onlineExponent(6,14)
		onlineExponent(7,15)
		onlineFasttwosum(0,8)
		onlineFasttwosum(1,9)
		onlineFasttwosum(2,10)
		onlineFasttwosum(3,11)
		onlineFasttwosum(4,12)
		onlineFasttwosum(5,13)
		onlineFasttwosum(6,14)
		onlineFasttwosum(7,15)
		X += CACHELINE;
		Y += CACHELINE;
	}
	for (; i < size; i++) {
#if defined(AVX2) || (defined(AVX512) && not defined(MIC))
		twoProdScaller(X,Y,res1,res2)
#else
		twoProdScaller(X,Y,res1,res2,XH,XL,XT,YH,YL,YT,temp)
#endif
		onlineExponent(0,1)
		onlineFasttwosum(0,1)
		X++;
		Y++;
	}
}

void sequentialVectorTwoProd(unsigned long size, unsigned long gsize, double *X, double *Y, double *res) {
#if (not defined(AVX2) && not defined(AVX512)) || defined(MIC)
	double __declspec(align(64)) XH[CACHELINE];
	double __declspec(align(64)) XL[CACHELINE];
	double __declspec(align(64)) YH[CACHELINE];
	double __declspec(align(64)) YL[CACHELINE];
	double __declspec(align(64)) XT[CACHELINE];
	double __declspec(align(64)) YT[CACHELINE];
	double __declspec(align(64)) temp[CACHELINE];
#endif
	double *res2 = res;
	double *res1 = &res[gsize];
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&X[8]);
		__builtin_prefetch(&Y[8]);
		__builtin_prefetch(&res1[8], 1);
		__builtin_prefetch(&res2[8], 1);
#if defined(AVX2) || defined(AVX512)
		twoProd(X,Y,res1,res2)
#else
		twoProd(X,Y,res1,res2,XH,XL,XT,YH,YL,YT,temp)
#endif
		X += CACHELINE;
		Y += CACHELINE;
		res1 += CACHELINE;
		res2 += CACHELINE;
	}
	for (; i < size; i++) {
#if defined(AVX2) || (defined(AVX512) && not defined(MIC))
		twoProdScaller(X,Y,res1,res2)
#else
		twoProdScaller(X,Y,res1,res2,XH,XL,XT,YH,YL,YT,temp)
#endif
		X++;
		Y++;
		res1++;
		res2++;
	}
}

#if defined(SSE)
#define TwoSum(X,Y,Xt,Yt)\
*(PDVECTOR*) &Xt[0] = addvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &Yt[0] = subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &X[0]);\
*(PDVECTOR*) &Y[0] = addvector(subvector(*(PDVECTOR*) &X[0], subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &Yt[0])),\
			subvector(*(PDVECTOR*) &Y[0], *(PDVECTOR*) &Yt[0]));\
*(PDVECTOR*) &X[0] = *(PDVECTOR*) &Xt[0];\
*(PDVECTOR*) &Xt[2] = addvector(*(PDVECTOR*) &X[2], *(PDVECTOR*) &Y[2]);\
*(PDVECTOR*) &Yt[2] = subvector(*(PDVECTOR*) &Xt[2], *(PDVECTOR*) &X[2]);\
*(PDVECTOR*) &Y[2] = addvector(subvector(*(PDVECTOR*) &X[2], subvector(*(PDVECTOR*) &Xt[2], *(PDVECTOR*) &Yt[2])),\
			subvector(*(PDVECTOR*) &Y[2], *(PDVECTOR*) &Yt[2]));\
*(PDVECTOR*) &X[2] = *(PDVECTOR*) &Xt[2];\
*(PDVECTOR*) &Xt[4] = addvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4]);\
*(PDVECTOR*) &Yt[4] = subvector(*(PDVECTOR*) &Xt[4], *(PDVECTOR*) &X[4]);\
*(PDVECTOR*) &Y[4] = addvector(subvector(*(PDVECTOR*) &X[4], subvector(*(PDVECTOR*) &Xt[4], *(PDVECTOR*) &Yt[4])),\
			subvector(*(PDVECTOR*) &Y[4], *(PDVECTOR*) &Yt[4]));\
*(PDVECTOR*) &X[4] = *(PDVECTOR*) &Xt[4];\
*(PDVECTOR*) &Xt[6] = addvector(*(PDVECTOR*) &X[6], *(PDVECTOR*) &Y[6]);\
*(PDVECTOR*) &Yt[6] = subvector(*(PDVECTOR*) &Xt[6], *(PDVECTOR*) &X[6]);\
*(PDVECTOR*) &Y[6] = addvector(subvector(*(PDVECTOR*) &X[6], subvector(*(PDVECTOR*) &Xt[6], *(PDVECTOR*) &Yt[6])),\
			subvector(*(PDVECTOR*) &Y[6], *(PDVECTOR*) &Yt[6]));\
*(PDVECTOR*) &X[6] = *(PDVECTOR*) &Xt[6];
#elif defined(AVX) || defined(AVX2)
#define TwoSum(X,Y,Xt,Yt)\
*(PDVECTOR*) &Xt[0] = addvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &Yt[0] = subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &X[0]);\
*(PDVECTOR*) &Y[0] = addvector(subvector(*(PDVECTOR*) &X[0], subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &Yt[0])),\
			subvector(*(PDVECTOR*) &Y[0], *(PDVECTOR*) &Yt[0]));\
*(PDVECTOR*) &X[0] = *(PDVECTOR*) &Xt[0];\
*(PDVECTOR*) &Xt[4] = addvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4]);\
*(PDVECTOR*) &Yt[4] = subvector(*(PDVECTOR*) &Xt[4], *(PDVECTOR*) &X[4]);\
*(PDVECTOR*) &Y[4] = addvector(subvector(*(PDVECTOR*) &X[4], subvector(*(PDVECTOR*) &Xt[4], *(PDVECTOR*) &Yt[4])),\
			subvector(*(PDVECTOR*) &Y[4], *(PDVECTOR*) &Yt[4]));\
*(PDVECTOR*) &X[4] = *(PDVECTOR*) &Xt[4];
#elif defined(AVX512)
#define TwoSum(X,Y,Xt,Yt)\
*(PDVECTOR*) &Xt[0] = addvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &Yt[0] = subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &X[0]);\
*(PDVECTOR*) &Y[0] = addvector(subvector(*(PDVECTOR*) &X[0], subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &Yt[0])),\
			subvector(*(PDVECTOR*) &Y[0], *(PDVECTOR*) &Yt[0]));\
*(PDVECTOR*) &X[0] = *(PDVECTOR*) &Xt[0];
#endif

void sequentialVectorTwoProdTrans(unsigned long size, double *X, double *Y, double *res, unsigned long *count) {
#if (not defined(AVX2) && not defined(AVX512)) || defined(MIC)
	double __declspec(align(64)) XH[CACHELINE];
	double __declspec(align(64)) XL[CACHELINE];
	double __declspec(align(64)) YH[CACHELINE];
	double __declspec(align(64)) YL[CACHELINE];
	double __declspec(align(64)) temp[CACHELINE];
#endif
	double __declspec(align(64)) XT[CACHELINE];
	double __declspec(align(64)) YT[CACHELINE];
	double __declspec(align(64)) res1[CACHELINE];
	double __declspec(align(64)) res2[CACHELINE];
	double __declspec(align(64)) S[CACHELINE] = {0};
	double __declspec(align(64)) St[CACHELINE] = {0};
	*count = 0;
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&X[8]);
		__builtin_prefetch(&Y[8]);
		__builtin_prefetch(&res1[8], 1);
		__builtin_prefetch(&res2[8], 1);
#if defined(AVX2) || defined(AVX512)
		twoProd(X,Y,res1,res2)
#else
		twoProd(X,Y,res1,res2,XH,XL,XT,YH,YL,YT,temp)
#endif
		TwoSum(S,res1,XT,YT);
		TwoSum(St,res1,XT,YT);
		TwoSum(St,res2,XT,YT);
		for (unsigned int j = 0; j < CACHELINE; j++) {
			if (res1[j] != 0) {
				res[*count] = res1[j];
				(*count)++;
			}
			if (res2[j] != 0) {
				res[*count] = res2[j];
				(*count)++;
			}
		}
		X += CACHELINE;
		Y += CACHELINE;
	}
	for (unsigned int j = 0; j < CACHELINE; j++) {
		if (S[j] != 0) {
			res[*count] = S[j];
			(*count)++;
		}
		if (St[j] != 0) {
			res[*count] = St[j];
			(*count)++;
		}
	}
	for (; i < size; i++) {
#if defined(AVX2) || (defined(AVX512) && not defined(MIC))
		twoProdScaller(X,Y,res1,res2)
#else
		twoProdScaller(X,Y,res1,res2,XH,XL,XT,YH,YL,YT,temp)
#endif
		res[*count] = res1[0];
		(*count)++;
		res[*count] = res2[0];
		(*count)++;
		X++;
		Y++;
	}
}

#define reduceVector(vector,result)\
result = 0;\
for (unsigned int cnt = 0; cnt < PDSIZE; cnt++) result += ((double*) &vector)[cnt];

double sequentialTwoProdSum(unsigned long size, unsigned long gsize, double *X, double *Y, double *res) {
#if (not defined(AVX2) && not defined(AVX512)) || defined(MIC)
	double __declspec(align(64)) XH[CACHELINE];
	double __declspec(align(64)) XL[CACHELINE];
	double __declspec(align(64)) YH[CACHELINE];
	double __declspec(align(64)) YL[CACHELINE];
	double __declspec(align(64)) XT[CACHELINE];
	double __declspec(align(64)) YT[CACHELINE];
	double __declspec(align(64)) temp[CACHELINE];
#endif
	double *res1 = res;
	double *res2 = &res[gsize];
	PDVECTOR absSum = ZEROES;
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&X[8]);
		__builtin_prefetch(&Y[8]);
		__builtin_prefetch(&res1[8], 1);
		__builtin_prefetch(&res2[8], 1);
#if defined(AVX2) || defined(AVX512)
		twoProd(X,Y,res1,res2)
#else
		twoProd(X,Y,res1,res2,XH,XL,XT,YH,YL,YT,temp)
#endif

#ifdef SSE
		absSum = addvector(absSum, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res1[0]));
		absSum = addvector(absSum, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res1[2]));
		absSum = addvector(absSum, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res1[4]));
		absSum = addvector(absSum, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res1[6]));
#elif defined(AVX2) || defined(AVX)
		absSum = addvector(absSum, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res1[0]));
		absSum = addvector(absSum, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res1[4]));
#elif defined(AVX512)
		absSum = addvector(absSum, absvector(*(PDVECTOR*) &res1[0]));
#endif
		X += CACHELINE;
		Y += CACHELINE;
		res1 += CACHELINE;
		res2 += CACHELINE;
	}
	for (; i < size; i++) {
#if defined(AVX2) || (defined(AVX512) && not defined(MIC))
		twoProdScaller(X,Y,res1,res2)
#else
		twoProdScaller(X,Y,res1,res2,XH,XL,XT,YH,YL,YT,temp)
#endif
		((double*) &absSum)[0] += fabs(res1[0]);
		X++;
		Y++;
		res1++;
		res2++;
	}
	double reduceVector(absSum, sum);
	return sum;
}

double ddotSequentialAccSum(unsigned long size, double *X, double *Y) {
	double *res = (double*) memalign(64, 2 * size * sizeof(double));
	double absSum = sequentialTwoProdSum(size, size, X, Y, res) / (1 - (size * eps));
#ifdef STATS
	firstPass = rdtsc();
#endif
	double S = sequentialAccSumCallBack(2 * size, res, nextPower2(absSum));
	free(res);
	return S;
}

double ddotSequentialFastAccSum(unsigned long size, double *X, double *Y) {
	double *res = (double*) memalign(64, 2 * size * sizeof(double));
	double absSum = sequentialTwoProdSum(size, size, X, Y, res) / (1 - (size * eps));
#ifdef STATS
	firstPass = rdtsc();
#endif
	double S = sequentialFastAccSumCallBack(2 * size, res, nextPower2(absSum));
	free(res);
	return S;
}

void sequentialdasum(unsigned long size, double *X, double *sum, double *finalerror, double *maxError) {
	double __declspec(align(64)) result[CACHELINE] = {0};
	double __declspec(align(64)) error[CACHELINE] = {0};
	double __declspec(align(64)) temp1[CACHELINE] = {0};
	double __declspec(align(64)) temp2[CACHELINE] = {0};
	double __declspec(align(64)) data[CACHELINE];
	double __declspec(align(64)) maxErrorV[CACHELINE] = {0};
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&X[40]);
#if defined(AVX512)
		*(PDVECTOR*) &data[0] = absvector(*(PDVECTOR*) &X[0]);
		*(PDVECTOR*) &X[0] = *(PDVECTOR*) &data[0];
#elif defined(AVX) || defined(AVX2) || defined(SSE)
		*(PDVECTOR*) &data[0] = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &X[0]);
		*(PDVECTOR*) &data[4] = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &X[4]);
		*(PDVECTOR*) &X[0] = *(PDVECTOR*) &data[0];
		*(PDVECTOR*) &X[4] = *(PDVECTOR*) &data[4];
#endif
#if defined(SSE)
		*(PDVECTOR*) &data[2] = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &X[2]);
		*(PDVECTOR*) &data[6] = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &X[6]);
		*(PDVECTOR*) &X[2] = *(PDVECTOR*) &data[2];
		*(PDVECTOR*) &X[6] = *(PDVECTOR*) &data[6];
#endif

		TwoSum(result, data, temp1, temp2)

		*(PDVECTOR*) &error[0] = addvector(*(PDVECTOR*) &error[0], *(PDVECTOR*) &data[0]);
		*(PDVECTOR*) &maxErrorV[0] = maxvector(*(PDVECTOR*) &error[0], *(PDVECTOR*) &maxErrorV[0]);
#if defined(AVX) || defined(AVX2) || defined(SSE)
		*(PDVECTOR*) &error[4] = addvector(*(PDVECTOR*) &error[4], *(PDVECTOR*) &data[4]);
		*(PDVECTOR*) &maxErrorV[4] = maxvector(*(PDVECTOR*) &error[4], *(PDVECTOR*) &maxErrorV[4]);
#endif
#if defined(SSE)
		*(PDVECTOR*) &error[2] = addvector(*(PDVECTOR*) &error[2], *(PDVECTOR*) &data[2]);
		*(PDVECTOR*) &error[6] = addvector(*(PDVECTOR*) &error[6], *(PDVECTOR*) &data[6]);
		*(PDVECTOR*) &maxErrorV[2] = maxvector(*(PDVECTOR*) &error[2], *(PDVECTOR*) &maxErrorV[2]);
		*(PDVECTOR*) &maxErrorV[6] = maxvector(*(PDVECTOR*) &error[6], *(PDVECTOR*) &maxErrorV[6]);
#endif
		X += CACHELINE;
	}
	double tserr;
	for (; i < size; i++) {
		twoSum(result[0], fabs(X[0]), &result[0], &tserr);
		error[0] += tserr;
		maxErrorV[0] = (maxErrorV[0] > error[0]) ? maxErrorV[0] : error[0];
		X++;
	}
	for (unsigned int j = 1; j < CACHELINE; j++) {
		twoSum(result[0], result[j], &result[0], &tserr);
		error[0] += tserr;
		maxErrorV[0] = (maxErrorV[0] > error[0]) ? maxErrorV[0] : error[0];
		error[0] += error[j];
		maxErrorV[0] = (maxErrorV[0] > error[0]) ? maxErrorV[0] : error[0];
	}
	*sum = result[0];
	*finalerror = error[0];
	*maxError = ulp(maxErrorV[0]) / 2;
}

double sequentialdot(unsigned long size, double *X, double *Y) {
	if (size < L2CACHE / (FPSIZE * 4)) return rtnblas_facs_ddot(size, X, 1, Y, 1);
	return rtnblas_ol_ddot(size, X, 1, Y, 1);
}

void sequentialTransSmallDot(unsigned long* size, double *X, double *Y, double *res) {
	unsigned long count;
	sequentialVectorTwoProdTrans(*size, X, Y, res, &count);
	sequentialTransSmallVector(&count, res);
	*size = count;
}

double ddotSequentialIFastSum(unsigned long size, double *X, double *Y) {
	double *res = (double*) memalign(64, 2 * size * sizeof(double));
	unsigned long count;
	sequentialVectorTwoProdTrans(size, X, Y, res, &count);
	double S = iFastSumCallBack(&count, res, 0);
	free(res);
	return S;
}

double axpbySequentialOnlineExact(
	unsigned int size, double *A, int IncA, double *X,
	int IncX, double Y, double alpha, double beta, double* C
) {
	ddotSequentialExtractOnlineExact(size, A, X, C);
	unsigned long count = ONLINEEXACTSIZE;
	eliminateZeros(&count, C);
	sequentialTransSmallVector(&count, C);
	double *res = (double*) memalign(64, count * sizeof(double));
	double __declspec(align(64)) valpha[CACHELINE] = {alpha};
#if (not defined(AVX2) && not defined(AVX512)) || defined(MIC)
	double __declspec(align(64)) XH[CACHELINE];
	double __declspec(align(64)) XL[CACHELINE];
	double __declspec(align(64)) YH[CACHELINE];
	double __declspec(align(64)) YL[CACHELINE];
	double __declspec(align(64)) XT[CACHELINE];
	double __declspec(align(64)) YT[CACHELINE];
	double __declspec(align(64)) temp[CACHELINE];
#endif
	double *CT = C;
	double *resT = res;
	int i = 0;
	for (; i + CACHELINE - 1 < count; i += CACHELINE) {
#if defined(AVX2) || defined(AVX512)
		twoProd(CT,valpha,resT,C)
#else
		twoProd(CT,valpha,resT,C,XH,XL,XT,YH,YL,YT,temp)
#endif
		CT += CACHELINE;
		resT += CACHELINE;
	}
	for (; i < count; i++) {
#if defined(AVX2) || (defined(AVX512) && not defined(MIC))
		twoProdScaller(CT,valpha,resT,CT)
#else
		twoProdScaller(CT,valpha,resT,CT,XH,XL,XT,YH,YL,YT,temp)
#endif
		CT++;
		resT++;
	}
	memcpy(&C[count], res, count * sizeof(double));
	double *YR = &Y;
	double *betaT = &beta;
	CT = &C[count * 2];
	resT = &C[count * 2 + 1];
#if defined(AVX2) || (defined(AVX512) && not defined(MIC))
	twoProdScaller(YR,betaT,resT,CT)
#else
	twoProdScaller(YR,betaT,resT,CT,XH,XL,XT,YH,YL,YT,temp)
#endif
	free(res);
	count = count * 2 + 2;
	return iFastSumCallBack(&count, C, 0);
}

double axpbySequentialHybridSum(
	unsigned int size, double *A, int IncA, double *X,
	int IncX, double Y, double alpha, double beta, double* C
) {
	ddotSequentialExtractHybridSum(size, A, X, C);
	unsigned long count = HYBRIDSUMSIZE;
	eliminateZeros(&count, C);
	sequentialTransSmallVector(&count, C);
	double *res = (double*) memalign(64, count * sizeof(double));
	double __declspec(align(64)) valpha[CACHELINE] = {alpha};
#if (not defined(AVX2) && not defined(AVX512)) || defined(MIC)
	double __declspec(align(64)) XH[CACHELINE];
	double __declspec(align(64)) XL[CACHELINE];
	double __declspec(align(64)) YH[CACHELINE];
	double __declspec(align(64)) YL[CACHELINE];
	double __declspec(align(64)) XT[CACHELINE];
	double __declspec(align(64)) YT[CACHELINE];
	double __declspec(align(64)) temp[CACHELINE];
#endif
	double *CT = C;
	double *resT = res;
	int i = 0;
	for (; i + CACHELINE - 1 < count; i += CACHELINE) {
#if defined(AVX2) || defined(AVX512)
		twoProd(CT,valpha,resT,C)
#else
		twoProd(CT,valpha,resT,C,XH,XL,XT,YH,YL,YT,temp)
#endif
		CT += CACHELINE;
		resT += CACHELINE;
	}
	for (; i < count; i++) {
#if defined(AVX2) || (defined(AVX512) && not defined(MIC))
		twoProdScaller(CT,valpha,resT,CT)
#else
		twoProdScaller(CT,valpha,resT,CT,XH,XL,XT,YH,YL,YT,temp)
#endif
		CT++;
		resT++;
	}
	memcpy(&C[count], res, count * sizeof(double));
	double *YR = &Y;
	double *betaT = &beta;
	CT = &C[count * 2];
	resT = &C[count * 2 + 1];
#if defined(AVX2) || (defined(AVX512) && not defined(MIC))
	twoProdScaller(YR,betaT,resT,CT)
#else
	twoProdScaller(YR,betaT,resT,CT,XH,XL,XT,YH,YL,YT,temp)
#endif
	free(res);
	count = count * 2 + 2;
	return iFastSumCallBack(&count, C, 0);
}

double axpbySequentialIFastSum(
	unsigned int size, double *A, int IncA, double *X,
	int IncX, double Y, double alpha, double beta, double* C
) {
	unsigned long count = size;
	sequentialTransSmallDot(&count, A, X, C);
	double __declspec(align(64)) valpha[CACHELINE] = {alpha};
#if (not defined(AVX2) && not defined(AVX512)) || defined(MIC)
	double __declspec(align(64)) XH[CACHELINE];
	double __declspec(align(64)) XL[CACHELINE];
	double __declspec(align(64)) YH[CACHELINE];
	double __declspec(align(64)) YL[CACHELINE];
	double __declspec(align(64)) XT[CACHELINE];
	double __declspec(align(64)) YT[CACHELINE];
	double __declspec(align(64)) temp[CACHELINE];
#endif
	double *CT = C;
	double *resT = &C[MAXTRANSSIZE];
	int i = 0;
	for (; i + CACHELINE - 1 < count; i += CACHELINE) {
#if defined(AVX2) || defined(AVX512)
		twoProd(CT,valpha,resT,C)
#else
		twoProd(CT,valpha,resT,C,XH,XL,XT,YH,YL,YT,temp)
#endif
		CT += CACHELINE;
		resT += CACHELINE;
	}
	for (; i < count; i++) {
#if defined(AVX2) || (defined(AVX512) && not defined(MIC))
		twoProdScaller(CT,valpha,resT,CT)
#else
		twoProdScaller(CT,valpha,resT,CT,XH,XL,XT,YH,YL,YT,temp)
#endif
		CT++;
		resT++;
	}
	rtnblas_dcopy(count, &C[MAXTRANSSIZE], 1, &C[count], 1);
	double *YR = &Y;
	double *betaT = &beta;
	CT = &C[count * 2];
	resT = &C[count * 2 + 1];
#if defined(AVX2) || (defined(AVX512) && not defined(MIC))
	twoProdScaller(YR,betaT,resT,CT)
#else
	twoProdScaller(YR,betaT,resT,CT,XH,XL,XT,YH,YL,YT,temp)
#endif
	count = count * 2 + 2;
	return iFastSumCallBack(&count, C, 0);
}

void dgemvSequentialOnlineExact(
		int layout, int trans, unsigned int m, unsigned int n, double alpha, double *A,
		unsigned int lda, double *X, int IncX, double beta, double *Y, int IncY
	) {
	double C[ONLINEEXACTSIZE] __attribute__ ((aligned(64)));
	for (unsigned int i = 0; i < m; i++) {
		initZero(ONLINEEXACTSIZE, C);
		Y[i] = axpbySequentialOnlineExact(n, A, 1, X, IncX, Y[i], alpha, beta, C);
		A += lda;
		
	}
}

void dgemvSequentialHybridSum(
		int layout, int trans, unsigned int m, unsigned int n, double alpha, double *A,
		unsigned int lda, double *X, int IncX, double beta, double *Y, int IncY
	) {
	double C[HYBRIDSUMSIZE] __attribute__ ((aligned(64)));
	for (unsigned int i = 0; i < m; i++) {
		initZero(HYBRIDSUMSIZE, C);
		Y[i] = axpbySequentialHybridSum(n, A, 1, X, IncX, Y[i], alpha, beta, C);
		A += lda;
	}
}

void dgemvSequentialIFastSum(
		int layout, int trans, unsigned int m, unsigned int n, double alpha, double *A,
		unsigned int lda, double *X, int IncX, double beta, double *Y, int IncY
	) {
	unsigned int Csize = 2 * n;
	Csize = (82 < Csize) ? Csize : 82;
	double *C = (double*) memalign(64, Csize * sizeof(double));
	for (unsigned int i = 0; i < m; i++) {
		Y[i] = axpbySequentialIFastSum(n, A, 1, X, IncX, Y[i], alpha, beta, C);
		A += lda;
	}
	free(C);
}

double axmxSequentialOnlineExact(double *A, int IncA, double *X, int IncX, unsigned int i, double Xi) {
	double C[ONLINEEXACTSIZE + 1] __attribute__ ((aligned(64))) = {0};
	ddotSequentialExtractOnlineExact(i, A, X, C);
	unsigned long count = ONLINEEXACTSIZE;
	eliminateZeros(&count, C);
	C[count] = -Xi;
	count++;
	return iFastSumCallBack(&count, C, 0);
}

double axmxSequentialHybridSum(double *A, int IncA, double *X, int IncX, unsigned int i, double Xi) {
	double C[HYBRIDSUMSIZE + 1] __attribute__ ((aligned(64))) = {0};
	ddotSequentialExtractHybridSum(i, A, X, C);
	unsigned long count = HYBRIDSUMSIZE;
	eliminateZeros(&count, C);
	C[count] = -Xi;
	count++;
	return iFastSumCallBack(&count, C, 0);
}

double axmxSequentialIFastSum(double *A, int IncA, double *X, int IncX, unsigned int i, double Xi) {
	double *C = (double*) memalign(64, ((i * 2) + 1) * sizeof(double));
	unsigned long count;
	sequentialVectorTwoProdTrans(i, A, X, C, &count);
	C[count] = -Xi;
	count++;
	double S = iFastSumCallBack(&count, C, 0);
	free(C);
	return S;
}

double axmxSequentialFastAccSum(double *A, int IncA, double *X, int IncX, unsigned int i, double Xi) {
	double *C = (double*) memalign(64, (i * 2 + 1) * sizeof(double));
	double absSum = sequentialTwoProdSum(i, i, A, X, C) / (1 - (i * eps));
	C[i * 2] = -Xi;
	absSum += fabs(Xi);
	double S = sequentialFastAccSumCallBack(i * 2 + 1, C, absSum);
	free(C);
	return S;
}

double axmxSequentialAccSum(double *A, int IncA, double *X, int IncX, unsigned int i, double Xi) {
	double *C = (double*) memalign(64, (i * 2 + 1) * sizeof(double));
	double absSum = sequentialTwoProdSum(i, i, A, X, C) / (1 - (i * eps));
	C[i * 2] = -Xi;
	absSum += fabs(Xi);
	double S = sequentialAccSumCallBack(i * 2 + 1, C, nextPower2(absSum));
	free(C);
	return S;
}

void dtrsvLowerSequentialOnlineExact(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	X[0] = X[0] / A[0];
	A += lda;
	for (unsigned int i = 1; i < n; i++) {
		X[i] = axmxSequentialOnlineExact(A, 1, X, IncX, i, X[i]) / -A[i];
		A += lda;
	}
}

void dtrsvLowerSequentialHybridSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	X[0] = X[0] / A[0];
	A += lda;
	for (unsigned int i = 1; i < n; i++) {
		X[i] = axmxSequentialHybridSum(A, 1, X, IncX, i, X[i]) / -A[i];
		A += lda;
	}
}

void dtrsvLowerSequentialIFastSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	X[0] = X[0] / A[0];
	A += lda;
	for (unsigned int i = 1; i < n; i++) {
		X[i] = axmxSequentialIFastSum(A, 1, X, IncX, i, X[i]) / -A[i];
		A += lda;
	}
}

void dtrsvLowerSequentialFastAccSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	X[0] = X[0] / A[0];
	A += lda;
	for (unsigned int i = 1; i < n; i++) {
		X[i] = axmxSequentialFastAccSum(A, 1, X, IncX, i, X[i]) / -A[i];
		A += lda;
	}
}

void dtrsvLowerSequentialAccSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	X[0] = X[0] / A[0];
	A += lda;
	for (unsigned int i = 1; i < n; i++) {
		X[i] = axmxSequentialAccSum(A, 1, X, IncX, i, X[i]) / -A[i];
		A += lda;
	}
}

void dtrsvLowerSequentialDynamic(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	X[0] = X[0] / A[0];
	A += lda;
	unsigned int i;
	unsigned int border = (n < 1000) ? n : 1000;
	for (i = 1; i < border; i++) {
		X[i] = axmxSequentialIFastSum(A, 1, X, IncX, i, X[i]) / -A[i];
		A += lda;
	}
	for (; i < n; i++) {
		X[i] = axmxSequentialHybridSum(A, 1, X, IncX, i, X[i]) / -A[i];
		A += lda;
	}
}

void dtrsvUpperSequentialOnlineExact(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += (lda - 1) * n;
	X[n - 1] = X[n - 1] / A[n - 1];
	A -= lda;
	for (unsigned int i = 1; i < n; i++) {
		X[n - i - 1] = axmxSequentialOnlineExact(&A[n - i], 1, &X[n - i], IncX, i, X[n - i - 1]) / -A[n - i - 1];
		A -= lda;
	}
}


void dtrsvUpperSequentialHybridSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += (lda - 1) * n;
	X[n - 1] = X[n - 1] / A[n - 1];
	A -= lda;
	for (unsigned int i = 1; i < n; i++) {
		X[n - i - 1] = axmxSequentialHybridSum(&A[n - i], 1, &X[n - i], IncX, i, X[n - i - 1]) / -A[n - i - 1];
		A -= lda;
	}
}

void dtrsvUpperSequentialIFastSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += (lda - 1) * n;
	X[n - 1] = X[n - 1] / A[n - 1];
	A -= lda;
	for (unsigned int i = 1; i < n; i++) {
		X[n - i - 1] = axmxSequentialIFastSum(&A[n - i], 1, &X[n - i], IncX, i, X[n - i - 1]) / -A[n - i - 1];
		A -= lda;
	}
}

void dtrsvUpperSequentialFastAccSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += (lda - 1) * n;
	X[n - 1] = X[n - 1] / A[n - 1];
	A -= lda;
	for (unsigned int i = 1; i < n; i++) {
		X[n - i - 1] = axmxSequentialFastAccSum(&A[n - i], 1, &X[n - i], IncX, i, X[n - i - 1]) / -A[n - i - 1];
		A -= lda;
	}
}

void dtrsvUpperSequentialAccSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += (lda - 1) * n;
	X[n - 1] = X[n - 1] / A[n - 1];
	A -= lda;
	for (unsigned int i = 1; i < n; i++) {
		X[n - i - 1] = axmxSequentialAccSum(&A[n - i], 1, &X[n - i], IncX, i, X[n - i - 1]) / -A[n - i - 1];
		A -= lda;
	}
}

void dtrsvUpperSequentialDynamic(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += (lda - 1) * n;
	X[n - 1] = X[n - 1] / A[n - 1];
	A -= lda;
	unsigned int i;
	unsigned int border = (n < 1000) ? n : 1000;
	for (i = 1; i < border; i++) {
		X[n - i - 1] = axmxSequentialIFastSum(&A[n - i], 1, &X[n - i], IncX, i, X[n - i - 1]) / -A[n - i - 1];
		A -= lda;
	}
	for (; i < n; i++) {
		X[n - i - 1] = axmxSequentialHybridSum(&A[n - i], 1, &X[n - i], IncX, i, X[n - i - 1]) / -A[n - i - 1];
		A -= lda;
	}
}

void dtrsvLowerDiagSequentialOnlineExact(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += lda;
	for (unsigned int i = 1; i < n; i++) {
		X[i] = -axmxSequentialOnlineExact(A, 1, X, IncX, i, X[i]);
		A += lda;
	}
}

void dtrsvLowerDiagSequentialHybridSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += lda;
	for (unsigned int i = 1; i < n; i++) {
		X[i] = -axmxSequentialHybridSum(A, 1, X, IncX, i, X[i]);
		A += lda;
	}
}

void dtrsvLowerDiagSequentialIFastSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += lda;
	for (unsigned int i = 1; i < n; i++) {
		X[i] = -axmxSequentialIFastSum(A, 1, X, IncX, i, X[i]);
		A += lda;
	}
}

void dtrsvLowerDiagSequentialFastAccSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += lda;
	for (unsigned int i = 1; i < n; i++) {
		X[i] = -axmxSequentialFastAccSum(A, 1, X, IncX, i, X[i]);
		A += lda;
	}
}

void dtrsvLowerDiagSequentialAccSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += lda;
	for (unsigned int i = 1; i < n; i++) {
		X[i] = -axmxSequentialAccSum(A, 1, X, IncX, i, X[i]);
		A += lda;
	}
}

void dtrsvLowerDiagSequentialDynamic(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += lda;
	unsigned int i;
	unsigned int border = (n < 1000) ? n : 1000;
	for (i = 1; i < border; i++) {
		X[i] = -axmxSequentialAccSum(A, 1, X, IncX, i, X[i]);
		A += lda;
	}
	for (; i < n; i++) {
		X[i] = -axmxSequentialHybridSum(A, 1, X, IncX, i, X[i]);
		A += lda;
	}
}

void dtrsvUpperDiagSequentialOnlineExact(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += (lda - 2) * n;
	for (unsigned int i = 1; i < n; i++) {
		X[n - i - 1] = -axmxSequentialOnlineExact(&A[n - i], 1, &X[n - i], IncX, i, X[n - i - 1]);
		A -= lda;
	}
}

void dtrsvUpperDiagSequentialHybridSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += (lda - 2) * n;
	for (unsigned int i = 1; i < n; i++) {
		X[n - i - 1] = -axmxSequentialHybridSum(&A[n - i], 1, &X[n - i], IncX, i, X[n - i - 1]);
		A -= lda;
	}
}

void dtrsvUpperDiagSequentialIFastSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += (lda - 2) * n;
	for (unsigned int i = 1; i < n; i++) {
		X[n - i - 1] = -axmxSequentialIFastSum(&A[n - i], 1, &X[n - i], IncX, i, X[n - i - 1]);
		A -= lda;
	}
}

void dtrsvUpperDiagSequentialFastAccSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += (lda - 2) * n;
	for (unsigned int i = 1; i < n; i++) {
		X[n - i - 1] = -axmxSequentialFastAccSum(&A[n - i], 1, &X[n - i], IncX, i, X[n - i - 1]);
		A -= lda;
	}
}

void dtrsvUpperDiagSequentialAccSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += (lda - 2) * n;
	for (unsigned int i = 1; i < n; i++) {
		X[n - i - 1] = -axmxSequentialAccSum(&A[n - i], 1, &X[n - i], IncX, i, X[n - i - 1]);
		A -= lda;
	}
}

void dtrsvUpperDiagSequentialDynamic(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	A += (lda - 2) * n;
	unsigned int i;
	unsigned int border = (n < 1000) ? n : 1000;
	for (i = 1; i < border; i++) {
		X[n - i - 1] = -axmxSequentialIFastSum(&A[n - i], 1, &X[n - i], IncX, i, X[n - i - 1]);
		A -= lda;
	}
	for (; i < n; i++) {
		X[n - i - 1] = -axmxSequentialHybridSum(&A[n - i], 1, &X[n - i], IncX, i, X[n - i - 1]);
		A -= lda;
	}
}

//========================================================================================================================
//============================================== END OF SEQUENTIAL PART ==================================================
//========================================================================================================================

#ifdef OMP_PARALLEL
void ddotOmpExtractHybridSum(unsigned long size, double* X, double* Y, double* C, unsigned long *sharedIndex) {
#ifdef STATS
	int first = 1;
#endif
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
	unsigned int LOCAL_OMP_NUMBER_OF_THREADS = size / localSizeCacheLine;
	LOCAL_OMP_NUMBER_OF_THREADS += !!(size % localSizeCacheLine);
	int sizes[LOCAL_OMP_NUMBER_OF_THREADS];
#pragma omp parallel for default(shared)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize = localSizeCacheLine;
		if (i + localSizeCacheLine > size) localSize = size - i;
		double Ct[HYBRIDSUMSIZE] __attribute__ ((aligned(64))) = {0};
		ddotSequentialExtractHybridSum(localSize, &X[i], &Y[i], Ct);
		unsigned long interSize = HYBRIDSUMSIZE;
		eliminateZeros(&interSize, Ct);
#ifdef STATS
#pragma omp critical
		if (first) {
			first = 0;
			exponentExtractCycles = rdtsc();
		}
#endif
		sequentialTransSmallVector(&interSize, Ct);
		unsigned int threadId = omp_get_thread_num();
		sizes[threadId] = interSize;
		memcpy(&C[threadId * MAXTRANSSIZE], Ct, interSize * sizeof(double));
	}
	*sharedIndex = sizes[0];
	for (unsigned int i = 1; i < LOCAL_OMP_NUMBER_OF_THREADS; i++) {
		memcpy(&C[*sharedIndex], &C[i * MAXTRANSSIZE], sizes[i] * sizeof(double));
		(*sharedIndex) += sizes[i];
	}
}

void ddotOmpExtractOnlineExact(unsigned long size, double* X, double* Y, double* C, unsigned long *sharedIndex) {
#ifdef STATS
	int first = 1;
#endif
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
	unsigned int LOCAL_OMP_NUMBER_OF_THREADS = size / localSizeCacheLine;
	LOCAL_OMP_NUMBER_OF_THREADS += !!(size % localSizeCacheLine);
	int sizes[LOCAL_OMP_NUMBER_OF_THREADS];
#pragma omp parallel for default(shared)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize = localSizeCacheLine;
		if (i + localSizeCacheLine > size) localSize = size - i;
		double Ct[ONLINEEXACTSIZE] __attribute__ ((aligned(64))) = {0};
		ddotSequentialExtractOnlineExact(localSize, &X[i], &Y[i], Ct);
		unsigned long interSize = ONLINEEXACTSIZE;
		eliminateZeros(&interSize, Ct);
#ifdef STATS
#pragma omp critical
		if (first) {
			first = 0;
			exponentExtractCycles = rdtsc();
		}
#endif
		sequentialTransSmallVector(&interSize, Ct);
		unsigned int threadId = omp_get_thread_num();
		sizes[threadId] = interSize;
		memcpy(&C[threadId * MAXTRANSSIZE], Ct, interSize * sizeof(double));
	}
	*sharedIndex = sizes[0];
	for (unsigned int i = 1; i < LOCAL_OMP_NUMBER_OF_THREADS; i++) {
		memcpy(&C[*sharedIndex], &C[i * MAXTRANSSIZE], sizes[i] * sizeof(double));
		(*sharedIndex) += sizes[i];
	}
}

double ompTwoProdSum(unsigned long size, double *X, double *Y, double *res) {
	double S = 0;
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
#pragma omp parallel for default(shared) reduction(+:S)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize = localSizeCacheLine;
		if (i + localSizeCacheLine > size) localSize = size - i;
		S = sequentialTwoProdSum(localSize, size, &X[i], &Y[i], &res[i]);
	}
	return S;
}

double ddotOmpAccSum(unsigned long size, double *X, double *Y) {
	double *res = (double*) memalign(64, 2 * size * sizeof(double));
	double absSum = ompTwoProdSum(size, X, Y, res) / (1 - (size * eps));
	double S = ompAccSumCallBack(2 * size, res, nextPower2(absSum));
	free(res);
	return S;
}

double ddotOmpIFastSum(unsigned long size, double *X, double *Y) {
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
	unsigned int LOCAL_OMP_NUMBER_OF_THREADS = size / localSizeCacheLine;
	LOCAL_OMP_NUMBER_OF_THREADS += !!(size % localSizeCacheLine);
	int sizes[LOCAL_OMP_NUMBER_OF_THREADS];
	double *C = (double*) memalign(64, MAXTRANSSIZE * LOCAL_OMP_NUMBER_OF_THREADS * sizeof(double));
	initZero(MAXTRANSSIZE * LOCAL_OMP_NUMBER_OF_THREADS, C);
#pragma omp parallel for default(shared)
	for (unsigned long start = 0; start < size; start += localSizeCacheLine) {
		unsigned long localSize = localSizeCacheLine;
		if (start + localSizeCacheLine > size) localSize = size - start;
		double *Ct = (double*) memalign(64, localSize * 2 * sizeof(double));
		unsigned long interSize = localSize;
		sequentialTransSmallDot(&interSize, &X[start], &Y[start], Ct);
		unsigned int threadId = omp_get_thread_num();
		sizes[threadId] = interSize;
		memcpy(&C[threadId * MAXTRANSSIZE], Ct, interSize * sizeof(double));
		free(Ct);
	}
	unsigned long sharedIndex = sizes[0];
	for (unsigned int i = 1; i < LOCAL_OMP_NUMBER_OF_THREADS; i++) {
		memcpy(&C[sharedIndex], &C[i * MAXTRANSSIZE], sizes[i] * sizeof(double));
		sharedIndex += sizes[i];
	}
	double S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	return S;
}

void ompdasum(unsigned long size, double *X, double *sum, double *finalerror, double *maxError) {
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
	unsigned int LOCAL_OMP_NUMBER_OF_THREADS = size / localSizeCacheLine;
	LOCAL_OMP_NUMBER_OF_THREADS += !!(size % localSizeCacheLine);
	double results[LOCAL_OMP_NUMBER_OF_THREADS * 2];
	double maxE = 0;
#pragma omp parallel for default(shared) reduction(max:maxE)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize = localSizeCacheLine;
		if (i + localSizeCacheLine > size) localSize = size - i;
		double S = 0;
		double E = 0;
		sequentialdasum(localSize, &X[i], &S, &E, &maxE);
		unsigned int threadId = omp_get_thread_num();
		results[threadId] = S;
		results[threadId + LOCAL_OMP_NUMBER_OF_THREADS] = E;
	}
	for (unsigned int i = 0; i < LOCAL_OMP_NUMBER_OF_THREADS; i++) {
		*finalerror += results[i + LOCAL_OMP_NUMBER_OF_THREADS];
		twoSum(*sum, results[i], sum, &results[i]);
		*finalerror += results[i];
	}
	*maxError = maxE;
}

double ompdot(unsigned long size, double *X, double *Y) {
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
	unsigned int LOCAL_OMP_NUMBER_OF_THREADS = size / localSizeCacheLine;
	LOCAL_OMP_NUMBER_OF_THREADS += !!(size % localSizeCacheLine);
	int sizes[LOCAL_OMP_NUMBER_OF_THREADS];
	double *C = (double*) memalign(64, MAXTRANSSIZE * OMP_NUMBER_OF_THREADS * sizeof(double));
	initZero(MAXTRANSSIZE * LOCAL_OMP_NUMBER_OF_THREADS, C);
#pragma omp parallel for default(shared)
	for (unsigned long start = 0; start < size; start += localSizeCacheLine) {
		unsigned long localSize = localSizeCacheLine;
		if (start + localSizeCacheLine > size) localSize = size - start;
		unsigned int threadId = omp_get_thread_num();
#ifdef MIC
		if (localSize < L2CACHE / (FPSIZE * 4)) {
#else
		if (localSize < L2CACHE / (FPSIZE * 4)) {
#endif
			double *Ct = (double*) memalign(64, localSize * 2 * sizeof(double));
			unsigned long interSize = localSize;
			sequentialTransSmallDot(&interSize, &X[start], &Y[start], Ct);
			sizes[threadId] = interSize;
			memcpy(&C[threadId * MAXTRANSSIZE], Ct, interSize * sizeof(double));
			free(Ct);
		}
#ifndef MIC
		else if (localSize < L2CACHE / (FPSIZE * 4)) {
			double Ct[ONLINEEXACTSIZE] __attribute__ ((aligned(64))) = {0};
			ddotSequentialExtractOnlineExact(localSize, &X[start], &Y[start], Ct);
			unsigned long interSize = ONLINEEXACTSIZE;
			eliminateZeros(&interSize, Ct);
			sequentialTransSmallVector(&interSize, Ct);
			sizes[threadId] = interSize;
			memcpy(&C[threadId * MAXTRANSSIZE], Ct, interSize * sizeof(double));
		}
#endif
		else {
			double Ct[HYBRIDSUMSIZE] __attribute__ ((aligned(64))) = {0};
			ddotSequentialExtractHybridSum(localSize, &X[start], &Y[start], Ct);
			unsigned long interSize = HYBRIDSUMSIZE;
			eliminateZeros(&interSize, Ct);
			sequentialTransSmallVector(&interSize, Ct);
			sizes[threadId] = interSize;
			memcpy(&C[threadId * MAXTRANSSIZE], Ct, interSize * sizeof(double));
		}
	}
	unsigned long sharedIndex = sizes[0];
	for (unsigned int i = 1; i < LOCAL_OMP_NUMBER_OF_THREADS; i++) {
		memcpy(&C[sharedIndex], &C[i * MAXTRANSSIZE], sizes[i] * sizeof(double));
		sharedIndex += sizes[i];
	}
	double S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	return S;
}

void ompVectorTwoProd(unsigned long size, double *X, double *Y, double *res) {
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
#pragma omp parallel for default(shared)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize = localSizeCacheLine;
		if (i + localSizeCacheLine > size) localSize = size - i;
		sequentialVectorTwoProd(localSize, size, &X[i], &Y[i], &res[i]);
	}
}

void ompTransSmallDot(unsigned long* size, double *X, double *Y, double *res) {
	ompVectorTwoProd(*size, X, Y, res);
	unsigned long count = *size * 2;
	ompTransSmallVector(&count, res);
	*size = count;
}

void dgemvOmpOnlineExact(
		int layout, int trans, unsigned int m, unsigned int n, double alpha, double *A,
		unsigned int lda, double *X, int IncX, double beta, double *Y, int IncY
	) {
	unsigned long localM = m / OMP_NUMBER_OF_THREADS;
	localM += !!(m / OMP_NUMBER_OF_THREADS);
#pragma omp parallel for default(shared)
	for (unsigned int i = 0; i < m; i += localM) {
		unsigned long minM = localM;
		if (i + localM > m) minM = m - i;
		dgemvSequentialOnlineExact(layout, trans, minM, n, alpha, &A[i * lda], lda, X, IncX, beta, &Y[i], IncY);
	}
}

void dgemvOmpHybridSum(
		int layout, int trans, unsigned int m, unsigned int n, double alpha, double *A,
		unsigned int lda, double *X, int IncX, double beta, double *Y, int IncY
	) {
	unsigned long localM = m / OMP_NUMBER_OF_THREADS;
	localM += !!(m / OMP_NUMBER_OF_THREADS);
#pragma omp parallel for default(shared)
	for (unsigned int i = 0; i < m; i += localM) {
		unsigned long minM = localM;
		if (i + localM > m) minM = m - i;
		dgemvSequentialHybridSum(layout, trans, minM, n, alpha, &A[i * lda], lda, X, IncX, beta, &Y[i], IncY);
	}
}

void dgemvOmpIFastSum(
		int layout, int trans, unsigned int m, unsigned int n, double alpha, double *A,
		unsigned int lda, double *X, int IncX, double beta, double *Y, int IncY
	) {
	unsigned long localM = m / OMP_NUMBER_OF_THREADS;
	localM += !!(m / OMP_NUMBER_OF_THREADS);
#pragma omp parallel for default(shared)
	for (unsigned int i = 0; i < m; i += localM) {
		unsigned long minM = localM;
		if (i + localM > m) minM = m - i;
		dgemvSequentialIFastSum(layout, trans, minM, n, alpha, &A[i * lda], lda, X, IncX, beta, &Y[i], IncY);
	}
}

double axmxSequentialHybridSumContinue(double *A, int IncA, double *X, int IncX, unsigned int i, double Xi, double *C) {
	ddotSequentialExtractHybridSum(i, A, X, C);
	unsigned long count = HYBRIDSUMSIZE;
	eliminateZeros(&count, C);
	C[count] = -Xi;
	count++;
	return iFastSumCallBack(&count, C, 0);
}

void dtrsvLowerSequentialHybridSumContinue(unsigned int n, double* A, unsigned int lda, double* X, int IncX, double **vectorC) {
	for (unsigned int i = 0; i < n; i++) {
		X[i] = axmxSequentialHybridSumContinue(A, 1, X, IncX, i, X[i], vectorC[i]) / -A[i];
		A += lda;
	}
}

void dtrsvLowerGemvHybridSum(unsigned int m, unsigned int n, double *A, unsigned int lda, double *X, int IncX, double **C) {
#pragma omp parallel for default(shared) schedule(guided)
	for (unsigned int line = 0; line < m; line++) ddotSequentialExtractHybridSum(n, &A[line * lda], X, C[line]);
}

void dtrsvLowerOmpHybridSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	unsigned int blockSize = 60;
	double** vectorC = (double**) memalign(64, blockSize * sizeof(double*));
	for (unsigned int i = 0; i < blockSize; i++) vectorC[i] = (double*) memalign(64, (HYBRIDSUMSIZE + 1) * sizeof(double));
	for (unsigned int currentBlock = 0; currentBlock < n; currentBlock += blockSize) {
		for (unsigned int i = 0; i < blockSize; i++) for (unsigned int j = 0; j < HYBRIDSUMSIZE + 1; j++) vectorC[i][j] = 0;
		unsigned int minBlockSize = n - currentBlock;
		if (minBlockSize > blockSize) minBlockSize = blockSize;
		if (currentBlock == 0) dtrsvLowerSequentialHybridSumContinue(minBlockSize, A, lda, X, IncX, vectorC);
		else {
			dtrsvLowerGemvHybridSum(minBlockSize, currentBlock, &A[currentBlock * lda], lda, X, IncX, vectorC);
			dtrsvLowerSequentialHybridSumContinue(minBlockSize, &A[currentBlock * lda + currentBlock], lda, &X[currentBlock],IncX, vectorC);
		}
	}
	for (unsigned int i = 0; i < blockSize; i++) free(vectorC[i]);
	free(vectorC);
}

double axmxSequentialOnlineExactContinue(double *A, int IncA, double *X, int IncX, unsigned int i, double Xi, double *C) {
	ddotSequentialExtractOnlineExact(i, A, X, C);
	unsigned long count = ONLINEEXACTSIZE;
	eliminateZeros(&count, C);
	C[count] = -Xi;
	count++;
	return iFastSumCallBack(&count, C, 0);
}

void dtrsvLowerSequentialOnlineExactContinue(unsigned int n, double* A, unsigned int lda, double* X, int IncX, double **vectorC) {
	for (unsigned int i = 0; i < n; i++) {
		X[i] = axmxSequentialOnlineExactContinue(A, 1, X, IncX, i, X[i], vectorC[i]) / -A[i];
		A += lda;
	}
}

void dtrsvLowerGemvOnlineExact(unsigned int m, unsigned int n, double *A, unsigned int lda, double *X, int IncX, double **C) {
#pragma omp parallel for default(shared) schedule(guided)
	for (unsigned int line = 0; line < m; line++) ddotSequentialExtractOnlineExact(n, &A[line * lda], X, C[line]);
}

void dtrsvLowerOmpOnlineExact(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	unsigned int blockSize = 60;
	double** vectorC = (double**) memalign(64, blockSize * sizeof(double*));
	for (unsigned int i = 0; i < blockSize; i++) vectorC[i] = (double*) memalign(64, (ONLINEEXACTSIZE + 1) * sizeof(double));
	for (unsigned int currentBlock = 0; currentBlock < n; currentBlock += blockSize) {
		for (unsigned int i = 0; i < blockSize; i++) for (unsigned int j = 0; j < ONLINEEXACTSIZE + 1; j++) vectorC[i][j] = 0;
		unsigned int minBlockSize = n - currentBlock;
		if (minBlockSize > blockSize) minBlockSize = blockSize;
		if (currentBlock == 0) dtrsvLowerSequentialOnlineExactContinue(minBlockSize, A, lda, X, IncX, vectorC);
		else {
			dtrsvLowerGemvOnlineExact(minBlockSize, currentBlock, &A[currentBlock * lda], lda, X, IncX, vectorC);
			dtrsvLowerSequentialOnlineExactContinue(minBlockSize, &A[currentBlock * lda + currentBlock], lda, &X[currentBlock], IncX, vectorC);
		}
	}
	for (unsigned int i = 0; i < blockSize; i++) free(vectorC[i]);
	free(vectorC);
}

double axmxSequentialIFastSumContinue(double *A, int IncA, double *X, int IncX, unsigned int i, double Xi, double *C, unsigned int start) {
	unsigned long count;
	sequentialVectorTwoProdTrans(i, A, X, &C[start], &count);
	C[start + count] = -Xi;
	count += start + 1;
	return iFastSumCallBack(&count, C, 0);
}

void dtrsvLowerSequentialIFastSumContinue(
	unsigned int n, double* A, unsigned int lda, double* X, int IncX, double **vectorC, unsigned long *count
) {
	for (unsigned int i = 0; i < n; i++) {
		X[i] = axmxSequentialIFastSumContinue(A, 1, X, IncX, i, X[i], vectorC[i], count[i]) / -A[i];
		A += lda;
	}
}

void dtrsvLowerGemvIFastSum(
	unsigned int m, unsigned int n, double *A, unsigned int lda, double *X, int IncX, double **C, unsigned long *count
) {
#pragma omp parallel for default(shared) schedule(guided)
	for (unsigned int line = 0; line < m; line++) {
		count[line] = n;
		sequentialTransSmallDot(&count[line], &A[line * lda], X, C[line]);
	}
}

void dtrsvLowerOmpIFastSum(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	unsigned int blockSize = 60;
	double** vectorC = (double**) memalign(64, blockSize * sizeof(double*));
	unsigned long* count = (unsigned long*) memalign(64, blockSize * sizeof(unsigned long));
	for (unsigned int i = 0; i < blockSize; i++) vectorC[i] = (double*) memalign(64, (n * 2 + 1) * sizeof(double));
	for (unsigned int currentBlock = 0; currentBlock < n; currentBlock += blockSize) {
		for (unsigned int i = 0; i < blockSize; i++) {
			for (unsigned int j = 0; j < n * 2 + 1; j++) vectorC[i][j] = 0;
			count[i] = 0;
		}
		unsigned int minBlockSize = n - currentBlock;
		if (minBlockSize > blockSize) minBlockSize = blockSize;
		if (currentBlock == 0) dtrsvLowerSequentialIFastSumContinue(minBlockSize, A, lda, X, IncX, vectorC, count);
		else {
			dtrsvLowerGemvIFastSum(minBlockSize, currentBlock, &A[currentBlock * lda], lda, X, IncX, vectorC, count);
			dtrsvLowerSequentialIFastSumContinue(
				minBlockSize, &A[currentBlock * lda + currentBlock], lda, &X[currentBlock], IncX, vectorC, count
			);
		}
	}
	for (unsigned int i = 0; i < blockSize; i++) free(vectorC[i]);
	free(vectorC);
	free(count);
}
#endif

//========================================================================================================================
//=========================================== END OF OPENMP PARALLEL PART ================================================
//========================================================================================================================

#if defined(MPI_PARALLEL) && not defined(OMP_PARALLEL)
void ddotMpiExtractHybridSum(unsigned long size, double* X, double* Y, double* C, unsigned long *sharedIndex) {
	*sharedIndex = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	ddotSequentialExtractHybridSum(localSize, X, Y, C);
	unsigned long interSize = HYBRIDSUMSIZE;
	eliminateZeros(&interSize, C);
#ifdef STATS
	exponentExtractCycles = rdtsc();
#endif
	sequentialTransSmallVector(&interSize, C);
	if (myRank == 0) {
		*sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&C[*sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			*sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(C, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
}

void ddotMpiExtractOnlineExact(unsigned long size, double* X, double* Y, double* C, unsigned long *sharedIndex) {
	*sharedIndex = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	ddotSequentialExtractOnlineExact(localSize, X, Y, C);
	unsigned long interSize = ONLINEEXACTSIZE;
	eliminateZeros(&interSize, C);
#ifdef STATS
	exponentExtractCycles = rdtsc();
#endif
	sequentialTransSmallVector(&interSize, C);
	if (myRank == 0) {
		*sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&C[*sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			*sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(C, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
}

double mpiTwoProdSum(unsigned long size, double *X, double *Y, double *res) {
	double S = 0;
	double glS = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	S = sequentialTwoProdSum(localSize, localSize, X, Y, res);
	MPI_Allreduce(&S, &glS, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	return glS;
}

double ddotMpiAccSum(unsigned long size, double *X, double *Y) {
	double *res = (double*) memalign(64, 2 * size * sizeof(double));
	double absSum = mpiTwoProdSum(size, X, Y, res) / (1 - (size * eps));
	double S = mpiAccSumCallBack(2 * size, res, nextPower2(absSum));
	free(res);
	return S;
}

double ddotMpiIFastSum(unsigned long size, double *X, double *Y) {
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	double *C = (double*) memalign(64, localSize * 2 * sizeof(double));
	unsigned long sharedIndex = 0;
	unsigned long interSize = localSize;
	sequentialTransSmallDot(&interSize, X, Y, C);
	if (myRank == 0) {
		sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&C[sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(C, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
	double S;
	if (!myRank) S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	MPI_Bcast(&S, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	return S;
}

void mpidasum(unsigned long size, double *X, double *sum, double *finalerror) {
	double S[2] = {0};
	double glS[2];
	double maxE = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	sequentialdasum(localSize, X, &S[0], &S[1], &maxE);
	MPI_Allreduce(&S[0], &glS[0], 2, MPI_DOUBLE, MPI_QUAD_SUM, MPI_COMM_WORLD);
	*sum = glS[0];
	*finalerror = glS[1];
}

double mpidot(unsigned long size, double *X, double *Y) {
	unsigned long sharedIndex = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	unsigned long interSize;
	double *C;
	if (localSize < L1CACHE / (FPSIZE * 4)) {
		interSize = 2 * localSize;
		C = (double*) memalign(64, interSize * sizeof(double));
		sequentialTransSmallDot(&interSize, X, Y, C);
	}
	else if (localSize < L2CACHE / (FPSIZE * 4)) {
		C = (double*) memalign(64, HYBRIDSUMSIZE * sizeof(double));
		initZero(HYBRIDSUMSIZE, C);
		ddotSequentialExtractHybridSum(localSize, X, Y, C);
		interSize = HYBRIDSUMSIZE;
		eliminateZeros(&interSize, C);
		sequentialTransSmallVector(&interSize, C);
	}
	else {
		C = (double*) memalign(64, ONLINEEXACTSIZE * sizeof(double));
		initZero(ONLINEEXACTSIZE, C);
		ddotSequentialExtractOnlineExact(localSize, X, Y, C);
		interSize = ONLINEEXACTSIZE;
		eliminateZeros(&interSize, C);
		sequentialTransSmallVector(&interSize, C);
	}
	if (myRank == 0) {
		sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&C[sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(C, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
	double S;
	if (!myRank) S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	MPI_Bcast(&S, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	return S;
}
#endif

#ifdef MPI_PARALLEL
double ddotMpiClassic(unsigned long size, double *X, int IncX, double *Y, int IncY) {
	double gdot = 0;
	double ldot = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	ldot = cblas_ddot(localSize, X, IncX, Y, IncY);
#ifdef STATS
	localSumCycles = rdtsc();
#endif
	MPI_Allreduce(&ldot, &gdot, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	return gdot;
}
#endif

//========================================================================================================================
//============================================ END OF MPI PARALLEL PART ==================================================
//========================================================================================================================

#if defined(MPI_PARALLEL) && defined(OMP_PARALLEL)
void ddotHybridExtractHybridSum(unsigned long size, double* X, double* Y, double* C, unsigned long *sharedIndex) {
	*sharedIndex = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	unsigned long interSize = HYBRIDSUMSIZE;
	ddotOmpExtractHybridSum(localSize, X, Y, C, &interSize);
	sequentialTransSmallVector(&interSize, C);
	if (myRank == 0) {
		*sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&C[*sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			*sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(C, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
}

void ddotHybridExtractOnlineExact(unsigned long size, double* X, double* Y, double* C, unsigned long *sharedIndex) {
	*sharedIndex = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	unsigned long interSize = ONLINEEXACTSIZE;
	ddotOmpExtractOnlineExact(localSize, X, Y, C, &interSize);
	sequentialTransSmallVector(&interSize, C);
	if (myRank == 0) {
		*sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&C[*sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			*sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(C, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
}

double hybridTwoProdSum(unsigned long size, double *X, double *Y, double *res) {
	double S = 0;
	double glS = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	S = ompTwoProdSum(localSize, X, Y, res);
	MPI_Allreduce(&S, &glS, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	return glS;
}

double ddotHybridAccSum(unsigned long size, double *X, double *Y) {
	double *res = (double*) memalign(64, 2 * size * sizeof(double));
	double absSum = hybridTwoProdSum(size, X, Y, res) / (1 - (size * eps));
	double S = hybridAccSumCallBack(2 * size, res, nextPower2(absSum));
	free(res);
	return S;
}

double ddotHybridIFastSum(unsigned long size, double *X, double *Y) {
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	double *C = (double*) memalign(64, localSize * 2 * sizeof(double));
	unsigned long sharedIndex = 0;
	unsigned long interSize = localSize;
	ompTransSmallDot(&interSize, X, Y, C);
	if (myRank == 0) {
		sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&C[sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(C, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
	double S;
	if (!myRank) S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	MPI_Bcast(&S, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	return S;
}

void hybriddasum(unsigned long size, double *X, double *sum, double *finalerror) {
	double S[2] = {0};
	double glS[2];
	double maxE;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	ompdasum(localSize, X, &S[0], &S[1], &maxE);
	MPI_Allreduce(&S[0], &glS[0], 2, MPI_DOUBLE, MPI_QUAD_SUM, MPI_COMM_WORLD);
	*sum = glS[0];
	*finalerror = glS[1];
}

double hybriddot(unsigned long size, double *X, double *Y) {
	unsigned long sharedIndex = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	unsigned long interSize;
	double* C;
	if (localSize < L1CACHE / (FPSIZE * 4)) {
		interSize = 2 * localSize;
		C = (double*) memalign(64, interSize * sizeof(double));
		ompTransSmallDot(&interSize, X, Y, C);
	}
	else if (localSize < L2CACHE / (FPSIZE * 4)) {
		interSize = MAXTRANSSIZE * MPI_NUMBER_OF_THREADS;
		C = (double*) memalign(64, interSize * sizeof(double));
		ddotOmpExtractHybridSum(localSize, X, Y, C, &interSize);
		sequentialTransSmallVector(&interSize, C);
	}
	else {
		interSize = MAXTRANSSIZE * MPI_NUMBER_OF_THREADS;
		C = (double*) memalign(64, interSize * sizeof(double));
		ddotOmpExtractOnlineExact(localSize, X, Y, C, &interSize);
		sequentialTransSmallVector(&interSize, C);
	}
	if (myRank == 0) {
		sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&C[sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(C, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
	double S;
	if (!myRank) S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	MPI_Bcast(&S, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	return S;
}
#endif

//========================================================================================================================
//=========================================== END OF Hybrid PARALLEL PART ================================================
//========================================================================================================================

double rtnblas_hs_ddot(unsigned long size, double* X, int IncX, double* Y, int IncY) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	unsigned int Csize = (MPI_NUMBER_OF_THREADS > OMP_NUMBER_OF_THREADS) ? MPI_NUMBER_OF_THREADS : OMP_NUMBER_OF_THREADS;
	Csize *= MAXTRANSSIZE;
	double *C = (double*) memalign(64, Csize * sizeof(double));
	unsigned long sharedIndex;
	ddotHybridExtractHybridSum(size, X, Y, C, &sharedIndex);
#ifdef STATS
	reduceAndUnionCycles = rdtsc();
#endif
	double S;
	if (!myRank) S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	MPI_Bcast(&S, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	return S;
#else
	unsigned int Csize = MAXTRANSSIZE * MPI_NUMBER_OF_THREADS;
	Csize = (Csize > HYBRIDSUMSIZE) ? Csize : HYBRIDSUMSIZE;
	double *C = (double*) memalign(64, Csize * sizeof(double));
	unsigned long sharedIndex;
	ddotMpiExtractHybridSum(size, X, Y, C, &sharedIndex);
#ifdef STATS
	reduceAndUnionCycles = rdtsc();
#endif
	double S;
	if (!myRank) S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	MPI_Bcast(&S, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	return S;
#endif
#elif defined(OMP_PARALLEL)
	double *C = (double*) memalign(64, MAXTRANSSIZE * OMP_NUMBER_OF_THREADS * sizeof(double));
	unsigned long sharedIndex;
	ddotOmpExtractHybridSum(size, X, Y, C, &sharedIndex);
#ifdef STATS
	reduceAndUnionCycles = rdtsc();
#endif
	double S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	return S;
#else
	double C[HYBRIDSUMSIZE] __attribute__ ((aligned(64))) = {0};
	ddotSequentialExtractHybridSum(size, X, Y, C);
	unsigned long interSize = HYBRIDSUMSIZE;
	eliminateZeros(&interSize, C);
#ifdef STATS
	exponentExtractCycles = rdtsc();
#endif
	return iFastSum(interSize, C);
#endif
}

double rtnblas_ol_ddot(unsigned long size, double* X, int IncX, double* Y, int IncY) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	unsigned int Csize = (MPI_NUMBER_OF_THREADS > OMP_NUMBER_OF_THREADS) ? MPI_NUMBER_OF_THREADS : OMP_NUMBER_OF_THREADS;
	Csize *= MAXTRANSSIZE;
	double *C = (double*) memalign(64, Csize * sizeof(double));
	unsigned long sharedIndex;
	ddotHybridExtractOnlineExact(size, X, Y, C, &sharedIndex);
#ifdef STATS
	reduceAndUnionCycles = rdtsc();
#endif
	double S;
	if (!myRank) S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	MPI_Bcast(&S, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	return S;
#else
	unsigned int Csize = MAXTRANSSIZE * MPI_NUMBER_OF_THREADS;
	Csize = (Csize > ONLINEEXACTSIZE) ? Csize : ONLINEEXACTSIZE;
	double *C = (double*) memalign(64, Csize * sizeof(double));
	unsigned long sharedIndex;
	ddotMpiExtractOnlineExact(size, X, Y, C, &sharedIndex);
#ifdef STATS
	reduceAndUnionCycles = rdtsc();
#endif
	double S;
	if (!myRank) S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	MPI_Bcast(&S, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	return S;
#endif
#elif defined(OMP_PARALLEL)
	double *C = (double*) memalign(64, MAXTRANSSIZE * OMP_NUMBER_OF_THREADS * sizeof(double));
	unsigned long sharedIndex;
	ddotOmpExtractOnlineExact(size, X, Y, C, &sharedIndex);
#ifdef STATS
	reduceAndUnionCycles = rdtsc();
#endif
	double S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	return S;
#else
	double C[ONLINEEXACTSIZE] __attribute__ ((aligned(64))) = {0};
	ddotSequentialExtractOnlineExact(size, X, Y, C);
	unsigned long interSize = ONLINEEXACTSIZE;
	eliminateZeros(&interSize, C);
#ifdef STATS
	exponentExtractCycles = rdtsc();
#endif
	return iFastSum(interSize, C);
#endif
}

double rtnblas_acs_ddot(unsigned long size, double *X, int IncX, double *Y, int IncY) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	return ddotHybridAccSum(size, X, Y);
#else
	return ddotMpiAccSum(size, X, Y);
#endif
#elif defined(OMP_PARALLEL)
	return ddotOmpAccSum(size, X, Y);
#else
	return ddotSequentialAccSum(size, X, Y);
#endif
}

double rtnblas_ifs_ddot(unsigned long size, double *X, int IncX, double *Y, int IncY) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	return ddotHybridIFastSum(size, X, Y);
#else
	return ddotMpiIFastSum(size, X, Y);
#endif
#elif defined(OMP_PARALLEL)
	return ddotOmpIFastSum(size, X, Y);
#else
	return ddotSequentialIFastSum(size, X, Y);
#endif
}

double rtnblas_facs_ddot(unsigned long size, double *X, int IncX, double *Y, int IncY) {
	return ddotSequentialFastAccSum(size, X, Y);
}

double rtnblas_dasum(unsigned long size, double *X, int IncX) {
	double sum = 0, finalerror = 0, maxError = 0;
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	hybriddasum(size, X, &sum, &finalerror);
#else
	mpidasum(size, X, &sum, &finalerror);
#endif
#elif defined(OMP_PARALLEL)
	ompdasum(size, X, &sum, &finalerror, &maxError);
#else
	sequentialdasum(size, X, &sum, &finalerror, &maxError);
#endif
	twoSum(sum, finalerror, &sum, &finalerror);
	double perror = finalerror + maxError * size;
	perror += ulp(perror);
	double nerror = finalerror - maxError * size;
	nerror -= ulp(nerror);
	if (sum + perror == sum + nerror) return sum;
	else return dynamicSum(size, X);
}

double rtnblas_ddot(unsigned long size, double *X, int IncX, double *Y, int IncY) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	return hybriddot(size, X, Y);
#else
	return mpidot(size, X, Y);
#endif
#elif defined(OMP_PARALLEL)
	return ompdot(size, X, Y);
#else
	return sequentialdot(size, X, Y);
#endif
}

double classic_ddot(unsigned long size, double *X, int IncX, double *Y, int IncY) {
#ifdef MPI_PARALLEL
	return ddotMpiClassic(size, X, IncX, Y, IncY);
#else
	return cblas_ddot(size, X, IncX, Y, IncY);
#endif
}

double rtnblas_ol_dnrm2(unsigned long size, double *X, int IncX) {
	return sqrt(rtnblas_ol_ddot(size, X, IncX, X, IncX));
}

double rtnblas_hs_dnrm2(unsigned long size, double *X, int IncX) {
	return sqrt(rtnblas_hs_ddot(size, X, IncX, X, IncX));
}

double rtnblas_acs_dnrm2(unsigned long size, double *X, int IncX) {
	return sqrt(rtnblas_acs_ddot(size, X, IncX, X, IncX));
}

double rtnblas_facs_dnrm2(unsigned long size, double *X, int IncX) {
	return sqrt(rtnblas_facs_ddot(size, X, IncX, X, IncX));
}

double rtnblas_ifs_dnrm2(unsigned long size, double *X, int IncX) {
	return sqrt(rtnblas_ifs_ddot(size, X, IncX, X, IncX));
}

double rtnblas_dnrm2(unsigned long size, double *X, int IncX) {
	return sqrt(rtnblas_ddot(size, X, IncX, X, IncX));
}

double classic_dnrm2(unsigned long size, double *X, int IncX) {
	return sqrt(classic_ddot(size, X, IncX, X, IncX));
}

void rtnblas_ol_dgemv(
		int layout, int trans, unsigned int m, unsigned int n, double alpha, double *A,
		unsigned int lda, double *X, int IncX, double beta, double *Y, int IncY
	) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
#else
#endif
#elif defined(OMP_PARALLEL)
	dgemvOmpOnlineExact(layout, trans, m, n, alpha, A, lda, X, IncX, beta, Y, IncY);
#else
	dgemvSequentialOnlineExact(layout, trans, m, n, alpha, A, lda, X, IncX, beta, Y, IncY);
#endif
}

void rtnblas_hs_dgemv(
		int layout, int trans, unsigned int m, unsigned int n, double alpha, double *A,
		unsigned int lda, double *X, int IncX, double beta, double *Y, int IncY
	) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
#else
#endif
#elif defined(OMP_PARALLEL)
	dgemvOmpHybridSum(layout, trans, m, n, alpha, A, lda, X, IncX, beta, Y, IncY);
#else
	dgemvSequentialHybridSum(layout, trans, m, n, alpha, A, lda, X, IncX, beta, Y, IncY);
#endif
}

void rtnblas_ifs_dgemv(
		int layout, int trans, unsigned int m, unsigned int n, double alpha, double *A,
		unsigned int lda, double *X, int IncX, double beta, double *Y, int IncY
	) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
#else
#endif
#elif defined(OMP_PARALLEL)
	dgemvOmpIFastSum(layout, trans, m, n, alpha, A, lda, X, IncX, beta, Y, IncY);
#else
	dgemvSequentialIFastSum(layout, trans, m, n, alpha, A, lda, X, IncX, beta, Y, IncY);
#endif
}

void rtnblas_dgemv(
		int layout, int trans, unsigned int m, unsigned int n, double alpha, double *A,
		unsigned int lda, double *X, int IncX, double beta, double *Y, int IncY
	) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
#else
#endif
#elif defined(OMP_PARALLEL)
	if (n < 2000) dgemvOmpIFastSum(layout, trans, m, n, alpha, A, lda, X, IncX, beta, Y, IncY);
	else dgemvOmpHybridSum(layout, trans, m, n, alpha, A, lda, X, IncX, beta, Y, IncY);
#else
	if (n < 2000) dgemvSequentialIFastSum(layout, trans, m, n, alpha, A, lda, X, IncX, beta, Y, IncY);
	else dgemvSequentialHybridSum(layout, trans, m, n, alpha, A, lda, X, IncX, beta, Y, IncY);
#endif
}

double axmxSequentialOnlineExactDoubleDivision(double *A, int IncA, double *X, int IncX, unsigned int i, double Xi, double *divisionError) {
	double C[ONLINEEXACTSIZE + 1] __attribute__ ((aligned(64))) = {0};
	ddotSequentialExtractOnlineExact(i, A, X, C);
	ddotSequentialExtractOnlineExact(i, A, divisionError, C);
	unsigned long count = ONLINEEXACTSIZE;
	eliminateZeros(&count, C);
	C[count] = -Xi;
	count++;
	return iFastSumCallBack(&count, C, 0);
}

void twoProdFunction(double x, double y, double* result, double* error) {
	*result = x * y;
	double xh, xl, yh, yl;
	split(x, &xh, &xl);
	split(y, &yh, &yl);
	*error = xl * yl - (((*result - xh * yh) - xl * yh) - xh * yl);
}

void twoDiv(double x, double y, double* result, double* error) {
	*result = x / y;
	double v, w;
	twoProdFunction(*result, y, &v, &w);
	*error = (x - v - w) / y;
}

void dtrsvLowerSequentialOnlineExactDoubleDivision(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	double* divisionError = (double*) memalign(64, n * sizeof(double));
	twoDiv(X[0], A[0], &X[0], &divisionError[0]);
	A += lda;
	for (unsigned int i = 1; i < n; i++) {
		X[i] = axmxSequentialOnlineExactDoubleDivision(A, 1, X, IncX, i, X[i], divisionError);
		twoDiv(-X[i], A[i], &X[i], &divisionError[i]);
		A += lda;
	}
}

void rtnblas_ol_dtrsv(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
#else
#endif
#elif defined(OMP_PARALLEL)
	if (uplo == CblasLower && diag == CblasNonUnit) dtrsvLowerOmpOnlineExact(n, A, lda, X, IncX);
#else
	if (uplo == CblasLower && diag == CblasNonUnit) dtrsvLowerSequentialOnlineExactDoubleDivision(n, A, lda, X, IncX);
	if (uplo == CblasLower && diag == CblasUnit) dtrsvLowerDiagSequentialOnlineExact(n, A, lda, X, IncX);
	if (uplo == CblasUpper && diag == CblasNonUnit) dtrsvUpperSequentialOnlineExact(n, A, lda, X, IncX);
	if (uplo == CblasUpper && diag == CblasUnit) dtrsvUpperDiagSequentialOnlineExact(n, A, lda, X, IncX);
#endif
}

void rtnblas_hs_dtrsv(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
#else
#endif
#elif defined(OMP_PARALLEL)
	if (uplo == CblasLower && diag == CblasNonUnit) dtrsvLowerSequentialHybridSum(n, A, lda, X, IncX);
#else
	if (uplo == CblasLower && diag == CblasNonUnit) dtrsvLowerSequentialHybridSum(n, A, lda, X, IncX);
	if (uplo == CblasLower && diag == CblasUnit) dtrsvLowerDiagSequentialHybridSum(n, A, lda, X, IncX);
	if (uplo == CblasUpper && diag == CblasNonUnit) dtrsvUpperSequentialHybridSum(n, A, lda, X, IncX);
	if (uplo == CblasUpper && diag == CblasUnit) dtrsvUpperDiagSequentialHybridSum(n, A, lda, X, IncX);
#endif
}

void rtnblas_ifs_dtrsv(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
#else
#endif
#elif defined(OMP_PARALLEL)
	if (uplo == CblasLower && diag == CblasNonUnit) dtrsvLowerOmpIFastSum(n, A, lda, X, IncX);
#else
	if (uplo == CblasLower && diag == CblasNonUnit) dtrsvLowerSequentialIFastSum(n, A, lda, X, IncX);
	if (uplo == CblasLower && diag == CblasUnit) dtrsvLowerDiagSequentialIFastSum(n, A, lda, X, IncX);
	if (uplo == CblasUpper && diag == CblasNonUnit) dtrsvUpperSequentialIFastSum(n, A, lda, X, IncX);
	if (uplo == CblasUpper && diag == CblasUnit) dtrsvUpperDiagSequentialIFastSum(n, A, lda, X, IncX);
#endif
}

void rtnblas_acs_dtrsv(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
#else
#endif
#elif defined(OMP_PARALLEL)
#else
	if (uplo == CblasLower && diag == CblasNonUnit) dtrsvLowerSequentialAccSum(n, A, lda, X, IncX);
	if (uplo == CblasLower && diag == CblasUnit) dtrsvLowerDiagSequentialAccSum(n, A, lda, X, IncX);
	if (uplo == CblasUpper && diag == CblasNonUnit) dtrsvUpperSequentialAccSum(n, A, lda, X, IncX);
	if (uplo == CblasUpper && diag == CblasUnit) dtrsvUpperDiagSequentialAccSum(n, A, lda, X, IncX);
#endif
}

void rtnblas_facs_dtrsv(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
#else
#endif
#elif defined(OMP_PARALLEL)
#else
	if (uplo == CblasLower && diag == CblasNonUnit) dtrsvLowerSequentialFastAccSum(n, A, lda, X, IncX);
	if (uplo == CblasLower && diag == CblasUnit) dtrsvLowerDiagSequentialFastAccSum(n, A, lda, X, IncX);
	if (uplo == CblasUpper && diag == CblasNonUnit) dtrsvUpperSequentialFastAccSum(n, A, lda, X, IncX);
	if (uplo == CblasUpper && diag == CblasUnit) dtrsvUpperDiagSequentialFastAccSum(n, A, lda, X, IncX);
#endif
}

void rtnblas_dtrsv(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
#else
#endif
#elif defined(OMP_PARALLEL)
#else
	if (uplo == CblasLower && diag == CblasNonUnit) dtrsvLowerSequentialDynamic(n, A, lda, X, IncX);
	if (uplo == CblasLower && diag == CblasUnit) dtrsvLowerDiagSequentialDynamic(n, A, lda, X, IncX);
	if (uplo == CblasUpper && diag == CblasNonUnit) dtrsvUpperSequentialDynamic(n, A, lda, X, IncX);
	if (uplo == CblasUpper && diag == CblasUnit) dtrsvUpperDiagSequentialDynamic(n, A, lda, X, IncX);
#endif
}

