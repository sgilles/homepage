double axmxSequentialOnlineExactDoubleDivision(double *A, int IncA, double *X, int IncX, unsigned int i, double Xi, double *divisionError) {
	double C[ONLINEEXACTSIZE + 1] __attribute__ ((aligned(64))) = {0};
	ddotSequentialExtractOnlineExact(i, A, X, C);
	ddotSequentialExtractOnlineExact(i, A, divisionError, C);
	unsigned long count = ONLINEEXACTSIZE;
	eliminateZeros(&count, C);
	C[count] = -Xi;
	count++;
	return iFastSumCallBack(&count, C, 0);
}

void twoProdFunction(double x, double y, double* result, double* error) {
	*result = x * y;
	double xh, xl, yh, yl;
	split(x, &xh, &xl);
	split(y, &yh, &yl);
	*error = xl * yl - (((*result - xh * yh) - xl * yh) - xh * yl);
}

void twoDiv(double x, double y, double* result, double* error) {
	*result = x / y;
	double v, w;
	twoProdFunction(*result, y, &v, &w);
	*error = (x - v - w) / y;
}

void dtrsvLowerSequentialOnlineExactDoubleDivision(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	double* divisionError = (double*) memalign(64, n * sizeof(double));
	twoDiv(X[0], A[0], &X[0], &divisionError[0]);
	A += lda;
	for (unsigned int i = 1; i < n; i++) {
		X[i] = axmxSequentialOnlineExactDoubleDivision(A, 1, X, IncX, i, X[i], divisionError);
		twoDiv(-X[i], A[i], &X[i], &divisionError[i]);
		A += lda;
	}
}


