void ddotSequentialExtractOnlineExact(unsigned long n, double* X, double* Y, double* C) {

	union binaryDouble exponent[2];
	double temp[2], res[2];
	unsigned int index[2];

	for (unsigned long i = 0; i < n; i++) {

		twoProd(X[i], Y[i], &res[0], &res[1]);

		exponent[0].data = fabs(res[0]);
		exponent[1].data = fabs(res[1]);

		index[0] = (exponent[0].binary >> 52) * 2;
		index[1] = (exponent[1].binary >> 52) * 2;

		twoSum(res[0], C[index[0]], &C[index[0]], &temp[0]);
		twoSum(res[1], C[index[1]], &C[index[1]], &temp[1]);

		twoSum(temp[0], C[index[0] + 1], &C[index[0] + 1], &temp[0]);
		twoSum(temp[1], C[index[1] + 1], &C[index[1] + 1], &temp[1]);

	}

}

