void ddotSequentialExtractHybridSum(unsigned long n, double* X, double* Y, double* C) {

	double temp[8], XH[8], XL[8], XT[8], YH[8], YL[8], YT[8], res1[8], res2[8];

	union binaryDouble exponent[16];
	unsigned long index[32];

	for (unsigned long i = 0; i < n; i += 8) {

		// Prefetch les données pour l'Itération suivante de la boucle
		__builtin_prefetch(&X[8]);
		__builtin_prefetch(&Y[8]);

		// RTN(X * Y)

		*(__m256d*) &res1[0] = _mm256_mul_pd(*(__m256d*) &X[0], *(__m256d*) &Y[0]);
		*(__m256d*) &res1[4] = _mm256_mul_pd(*(__m256d*) &X[4], *(__m256d*) &Y[4]);

		// Split de X et Y

		// Split de X

		*(__m256d*) &temp[0] = _mm256_mul_pd(*(__m256d*) &X[0], _M256_SPLIT_FACTOR);
		*(__m256d*) &temp[4] = _mm256_mul_pd(*(__m256d*) &X[4], _M256_SPLIT_FACTOR);

		*(__m256d*) &XH[0] = _mm256_sub_pd(*(__m256d*) &temp[0], *(__m256d*) &X[0]);
		*(__m256d*) &XH[4] = _mm256_sub_pd(*(__m256d*) &temp[4], *(__m256d*) &X[4]);

		*(__m256d*) &XH[0] = _mm256_sub_pd(*(__m256d*) &temp[0], *(__m256d*) &XH[0]);
		*(__m256d*) &XH[4] = _mm256_sub_pd(*(__m256d*) &temp[4], *(__m256d*) &XH[4]);

		*(__m256d*) &XL[0] = _mm256_sub_pd(*(__m256d*) &X[0], *(__m256d*) &XH[0]);
		*(__m256d*) &XL[4] = _mm256_sub_pd(*(__m256d*) &X[4], *(__m256d*) &XH[4]);

		// Split de Y

		*(__m256d*) &temp[0] = _mm256_mul_pd(*(__m256d*) &Y[0], _M256_SPLIT_FACTOR);
		*(__m256d*) &temp[4] = _mm256_mul_pd(*(__m256d*) &Y[4], _M256_SPLIT_FACTOR);

		*(__m256d*) &YH[0] = _mm256_sub_pd(*(__m256d*) &temp[0], *(__m256d*) &Y[0]);
		*(__m256d*) &YH[4] = _mm256_sub_pd(*(__m256d*) &temp[4], *(__m256d*) &Y[4]);

		*(__m256d*) &YH[0] = _mm256_sub_pd(*(__m256d*) &temp[0], *(__m256d*) &YH[0]);
		*(__m256d*) &YH[4] = _mm256_sub_pd(*(__m256d*) &temp[4], *(__m256d*) &YH[4]);

		*(__m256d*) &YL[0] = _mm256_sub_pd(*(__m256d*) &Y[0], *(__m256d*) &YH[0]);
		*(__m256d*) &YL[4] = _mm256_sub_pd(*(__m256d*) &Y[4], *(__m256d*) &YH[4]);

		// Round-off error of multiplication

		*(__m256d*) &XT[0] = _mm256_mul_pd(*(__m256d*) &XH[0], *(__m256d*) &YH[0]);
		*(__m256d*) &XT[4] = _mm256_mul_pd(*(__m256d*) &XH[4], *(__m256d*) &YH[4]);

		*(__m256d*) &XT[0] = _mm256_sub_pd(*(__m256d*) &XT[0], *(__m256d*) &res1[0]);
		*(__m256d*) &XT[4] = _mm256_sub_pd(*(__m256d*) &XT[4], *(__m256d*) &res1[4]);

		*(__m256d*) &YT[0] = _mm256_mul_pd(*(__m256d*) &XH[0], *(__m256d*) &YL[0]);
		*(__m256d*) &YT[4] = _mm256_mul_pd(*(__m256d*) &XH[4], *(__m256d*) &YL[4]);

		*(__m256d*) &YT[0] = _mm256_add_pd(*(__m256d*) &YT[0], *(__m256d*) &XT[0]);
		*(__m256d*) &YT[4] = _mm256_add_pd(*(__m256d*) &YT[4], *(__m256d*) &XT[4]);

		*(__m256d*) &XT[0] = _mm256_mul_pd(*(__m256d*) &XL[0], *(__m256d*) &YH[0]);
		*(__m256d*) &XT[4] = _mm256_mul_pd(*(__m256d*) &XL[4], *(__m256d*) &YH[4]);

		*(__m256d*) &XT[0] = _mm256_add_pd(*(__m256d*) &XT[0], *(__m256d*) &YT[0]);
		*(__m256d*) &XT[4] = _mm256_add_pd(*(__m256d*) &XT[4], *(__m256d*) &YT[4]);

		*(__m256d*) &res2[0] = _mm256_mul_pd(*(__m256d*) &XL[0], *(__m256d*) &YL[0]);
		*(__m256d*) &res2[4] = _mm256_mul_pd(*(__m256d*) &XL[4], *(__m256d*) &YL[4]);

		*(__m256d*) &res2[0] = _mm256_add_pd(*(__m256d*) &res2[0], *(__m256d*) &XT[0]);
		*(__m256d*) &res2[4] = _mm256_add_pd(*(__m256d*) &res2[4], *(__m256d*) &XT[4]);

		// Split de res1 et res2

		// Split de res1

		*(__m256d*) &temp[0] = _mm256_mul_pd(*(__m256d*) &res1[0], _M256_SPLIT_FACTOR);
		*(__m256d*) &temp[4] = _mm256_mul_pd(*(__m256d*) &res1[4], _M256_SPLIT_FACTOR);

		*(__m256d*) &XH[0] = _mm256_sub_pd(*(__m256d*) &temp[0], *(__m256d*) &res1[0]);
		*(__m256d*) &XH[4] = _mm256_sub_pd(*(__m256d*) &temp[4], *(__m256d*) &res1[4]);

		*(__m256d*) &XH[0] = _mm256_sub_pd(*(__m256d*) &temp[0], *(__m256d*) &XH[0]);
		*(__m256d*) &XH[4] = _mm256_sub_pd(*(__m256d*) &temp[4], *(__m256d*) &XH[4]);

		*(__m256d*) &XL[0] = _mm256_sub_pd(*(__m256d*) &res1[0], *(__m256d*) &XH[0]);
		*(__m256d*) &XL[4] = _mm256_sub_pd(*(__m256d*) &res1[4], *(__m256d*) &XH[4]);

		// Split de res2

		*(__m256d*) &temp[0] = _mm256_mul_pd(*(__m256d*) &res2[0], _M256_SPLIT_FACTOR);
		*(__m256d*) &temp[4] = _mm256_mul_pd(*(__m256d*) &res2[4], _M256_SPLIT_FACTOR);

		*(__m256d*) &YH[0] = _mm256_sub_pd(*(__m256d*) &temp[0], *(__m256d*) &res2[0]);
		*(__m256d*) &YH[4] = _mm256_sub_pd(*(__m256d*) &temp[4], *(__m256d*) &res2[4]);

		*(__m256d*) &YH[0] = _mm256_sub_pd(*(__m256d*) &temp[0], *(__m256d*) &YH[0]);
		*(__m256d*) &YH[4] = _mm256_sub_pd(*(__m256d*) &temp[4], *(__m256d*) &YH[4]);

		*(__m256d*) &YL[0] = _mm256_sub_pd(*(__m256d*) &res2[0], *(__m256d*) &YH[0]);
		*(__m256d*) &YL[4] = _mm256_sub_pd(*(__m256d*) &res2[4], *(__m256d*) &YH[4]);

		// Extraction de l'exposant

		exponent[0].data = fabs(XH[0]);
		exponent[1].data = fabs(XH[1]);
		exponent[2].data = fabs(XH[2]);
		exponent[3].data = fabs(XH[3]);
		exponent[4].data = fabs(XH[4]);
		exponent[5].data = fabs(XH[5]);
		exponent[6].data = fabs(XH[6]);
		exponent[7].data = fabs(XH[7]);
		exponent[8].data = fabs(YH[0]);
		exponent[9].data = fabs(YH[1]);
		exponent[10].data = fabs(YH[2]);
		exponent[11].data = fabs(YH[3]);
		exponent[12].data = fabs(YH[4]);
		exponent[13].data = fabs(YH[5]);
		exponent[14].data = fabs(YH[6]);
		exponent[15].data = fabs(YH[7]);

		index[0] = exponent[0].binary >> 52;
		index[1] = exponent[1].binary >> 52;
		index[2] = exponent[2].binary >> 52;
		index[3] = exponent[3].binary >> 52;
		index[4] = exponent[4].binary >> 52;
		index[5] = exponent[5].binary >> 52;
		index[6] = exponent[6].binary >> 52;
		index[7] = exponent[7].binary >> 52;
		index[8] = exponent[8].binary >> 52;
		index[9] = exponent[9].binary >> 52;
		index[10] = exponent[10].binary >> 52;
		index[11] = exponent[11].binary >> 52;
		index[12] = exponent[12].binary >> 52;
		index[13] = exponent[13].binary >> 52;
		index[14] = exponent[14].binary >> 52;
		index[15] = exponent[15].binary >> 52;

		index[16] = index[0] - 27;
		index[17] = index[1] - 27;
		index[18] = index[2] - 27;
		index[19] = index[3] - 27;
		index[20] = index[4] - 27;
		index[21] = index[5] - 27;
		index[22] = index[6] - 27;
		index[23] = index[7] - 27;
		index[24] = index[8] - 27;
		index[25] = index[9] - 27;
		index[26] = index[10] - 27;
		index[27] = index[11] - 27;
		index[28] = index[12] - 27;
		index[29] = index[13] - 27;
		index[30] = index[14] - 27;
		index[31] = index[15] - 27;

		index[16] = (index[16] > 0) ? index[16] : 0;
		index[17] = (index[17] > 0) ? index[17] : 0;
		index[18] = (index[18] > 0) ? index[18] : 0;
		index[19] = (index[19] > 0) ? index[19] : 0;
		index[20] = (index[20] > 0) ? index[20] : 0;
		index[21] = (index[21] > 0) ? index[21] : 0;
		index[22] = (index[22] > 0) ? index[22] : 0;
		index[23] = (index[23] > 0) ? index[23] : 0;
		index[24] = (index[24] > 0) ? index[24] : 0;
		index[25] = (index[25] > 0) ? index[25] : 0;
		index[26] = (index[26] > 0) ? index[26] : 0;
		index[27] = (index[27] > 0) ? index[27] : 0;
		index[28] = (index[28] > 0) ? index[28] : 0;
		index[29] = (index[29] > 0) ? index[29] : 0;
		index[30] = (index[30] > 0) ? index[30] : 0;
		index[31] = (index[31] > 0) ? index[31] : 0;

		C[index[0]] += XH[0];
		C[index[1]] += XH[1];
		C[index[2]] += XH[2];
		C[index[3]] += XH[3];
		C[index[4]] += XH[4];
		C[index[5]] += XH[5];
		C[index[6]] += XH[6];
		C[index[7]] += XH[7];
		C[index[8]] += YH[0];
		C[index[9]] += YH[1];
		C[index[10]] += YH[2];
		C[index[11]] += YH[3];
		C[index[12]] += YH[4];
		C[index[13]] += YH[5];
		C[index[14]] += YH[6];
		C[index[15]] += YH[7];
		C[index[16]] += XL[0];
		C[index[17]] += XL[1];
		C[index[18]] += XL[2];
		C[index[19]] += XL[3];
		C[index[20]] += XL[4];
		C[index[21]] += XL[5];
		C[index[22]] += XL[6];
		C[index[23]] += XL[7];
		C[index[24]] += YL[0];
		C[index[25]] += YL[1];
		C[index[26]] += YL[2];
		C[index[27]] += YL[3];
		C[index[28]] += YL[4];
		C[index[29]] += YL[5];
		C[index[30]] += YL[6];
		C[index[31]] += YL[7];

		X += 8;
		Y += 8;

	}

}

