void ddotSequentialExtractHybridSum(unsigned long n, double* X, double* Y, double* C) {

	double XH, XL, YH, YL, res1, res2;

	union binaryDouble exponent[4];
	unsigned long index[4];

	for (unsigned long i = 0; i < n; i++) {

		twoProd(X[i], Y[i], &res1, &res2);

		split(res1, &XH, &XL);
		split(res2, &YH, &YL);

		exponent[0].data = fabs(XH);
		exponent[1].data = fabs(YH);
		exponent[2].data = fabs(XL);
		exponent[3].data = fabs(YL);

		index[0] = exponent[0].binary >> 52;
		index[1] = exponent[1].binary >> 52;
		index[2] = exponent[2].binary >> 52;
		index[3] = exponent[3].binary >> 52;

		C[index[0]] += XH;
		C[index[1]] += YH;
		C[index[2]] += XL;
		C[index[3]] += YL;

	}

}

