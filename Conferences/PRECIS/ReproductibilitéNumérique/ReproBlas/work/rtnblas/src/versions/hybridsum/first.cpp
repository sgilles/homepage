void ddotSequentialExtractHybridSum(unsigned long n, double* X, double* Y, double* C) {

	double temp[8], XH[8], XL[8], XT[8], YH[8], YL[8], YT[8], res1[8], res2[8];

	union binaryDouble exponent[16];
	unsigned long index[32];

	for (unsigned long i = 0; i < n; i += 8) {

		// Prefetch les données pour l'Itération suivante de la boucle
		__builtin_prefetch(&X[8]);
		__builtin_prefetch(&Y[8]);

		// RTN(X * Y)

		res1[0] = X[0] * Y[0];
		res1[1] = X[1] * Y[1];
		res1[2] = X[2] * Y[2];
		res1[3] = X[3] * Y[3];
		res1[4] = X[4] * Y[4];
		res1[5] = X[5] * Y[5];
		res1[6] = X[6] * Y[6];
		res1[7] = X[7] * Y[7];

		// Split de X et Y

		// Split de X

		temp[0] = X[0] * SPLIT_FACTOR;
		temp[1] = X[1] * SPLIT_FACTOR;
		temp[2] = X[2] * SPLIT_FACTOR;
		temp[3] = X[3] * SPLIT_FACTOR;
		temp[4] = X[4] * SPLIT_FACTOR;
		temp[5] = X[5] * SPLIT_FACTOR;
		temp[6] = X[6] * SPLIT_FACTOR;
		temp[7] = X[7] * SPLIT_FACTOR;

		XH[0] = temp[0] - X[0];
		XH[1] = temp[1] - X[1];
		XH[2] = temp[2] - X[2];
		XH[3] = temp[3] - X[3];
		XH[4] = temp[4] - X[4];
		XH[5] = temp[5] - X[5];
		XH[6] = temp[6] - X[6];
		XH[7] = temp[7] - X[7];

		XH[0] = temp[0] - XH[0];
		XH[1] = temp[1] - XH[1];
		XH[2] = temp[2] - XH[2];
		XH[3] = temp[3] - XH[3];
		XH[4] = temp[4] - XH[4];
		XH[5] = temp[5] - XH[5];
		XH[6] = temp[6] - XH[6];
		XH[7] = temp[7] - XH[7];

		XL[0] = X[0] - XH[0];
		XL[1] = X[1] - XH[1];
		XL[2] = X[2] - XH[2];
		XL[3] = X[3] - XH[3];
		XL[4] = X[4] - XH[4];
		XL[5] = X[5] - XH[5];
		XL[6] = X[6] - XH[6];
		XL[7] = X[7] - XH[7];

		// Split de Y

		temp[0] = Y[0] * SPLIT_FACTOR;
		temp[1] = Y[1] * SPLIT_FACTOR;
		temp[2] = Y[2] * SPLIT_FACTOR;
		temp[3] = Y[3] * SPLIT_FACTOR;
		temp[4] = Y[4] * SPLIT_FACTOR;
		temp[5] = Y[5] * SPLIT_FACTOR;
		temp[6] = Y[6] * SPLIT_FACTOR;
		temp[7] = Y[7] * SPLIT_FACTOR;

		YH[0] = temp[0] - Y[0];
		YH[1] = temp[1] - Y[1];
		YH[2] = temp[2] - Y[2];
		YH[3] = temp[3] - Y[3];
		YH[4] = temp[4] - Y[4];
		YH[5] = temp[5] - Y[5];
		YH[6] = temp[6] - Y[6];
		YH[7] = temp[7] - Y[7];

		YH[0] = temp[0] - YH[0];
		YH[1] = temp[1] - YH[1];
		YH[2] = temp[2] - YH[2];
		YH[3] = temp[3] - YH[3];
		YH[4] = temp[4] - YH[4];
		YH[5] = temp[5] - YH[5];
		YH[6] = temp[6] - YH[6];
		YH[7] = temp[7] - YH[7];

		YL[0] = Y[0] - YH[0];
		YL[1] = Y[1] - YH[1];
		YL[2] = Y[2] - YH[2];
		YL[3] = Y[3] - YH[3];
		YL[4] = Y[4] - YH[4];
		YL[5] = Y[5] - YH[5];
		YL[6] = Y[6] - YH[6];
		YL[7] = Y[7] - YH[7];

		// Round-off error of multiplication

		XT[0] = XH[0] * YH[0];
		XT[1] = XH[1] * YH[1];
		XT[2] = XH[2] * YH[2];
		XT[3] = XH[3] * YH[3];
		XT[4] = XH[4] * YH[4];
		XT[5] = XH[5] * YH[5];
		XT[6] = XH[6] * YH[6];
		XT[7] = XH[7] * YH[7];

		XT[0] -= res1[0];
		XT[1] -= res1[1];
		XT[2] -= res1[2];
		XT[3] -= res1[3];
		XT[4] -= res1[4];
		XT[5] -= res1[5];
		XT[6] -= res1[6];
		XT[7] -= res1[7];

		YT[0] = XH[0] * YL[0];
		YT[1] = XH[1] * YL[1];
		YT[2] = XH[2] * YL[2];
		YT[3] = XH[3] * YL[3];
		YT[4] = XH[4] * YL[4];
		YT[5] = XH[5] * YL[5];
		YT[6] = XH[6] * YL[6];
		YT[7] = XH[7] * YL[7];

		YT[0] += XT[0];
		YT[1] += XT[1];
		YT[2] += XT[2];
		YT[3] += XT[3];
		YT[4] += XT[4];
		YT[5] += XT[5];
		YT[6] += XT[6];
		YT[7] += XT[7];

		XT[0] = XL[0] * YH[0];
		XT[1] = XL[1] * YH[1];
		XT[2] = XL[2] * YH[2];
		XT[3] = XL[3] * YH[3];
		XT[4] = XL[4] * YH[4];
		XT[5] = XL[5] * YH[5];
		XT[6] = XL[6] * YH[6];
		XT[7] = XL[7] * YH[7];

		XT[0] += YT[0];
		XT[1] += YT[1];
		XT[2] += YT[2];
		XT[3] += YT[3];
		XT[4] += YT[4];
		XT[5] += YT[5];
		XT[6] += YT[6];
		XT[7] += YT[7];

		res2[0] = XL[0] * YL[0];
		res2[1] = XL[1] * YL[1];
		res2[2] = XL[2] * YL[2];
		res2[3] = XL[3] * YL[3];
		res2[4] = XL[4] * YL[4];
		res2[5] = XL[5] * YL[5];
		res2[6] = XL[6] * YL[6];
		res2[7] = XL[7] * YL[7];

		res2[0] += XT[0];
		res2[1] += XT[1];
		res2[2] += XT[2];
		res2[3] += XT[3];
		res2[4] += XT[4];
		res2[5] += XT[5];
		res2[6] += XT[6];
		res2[7] += XT[7];

		// Split de res1 et res2

		// Split de res1

		temp[0] = res1[0] * SPLIT_FACTOR;
		temp[1] = res1[1] * SPLIT_FACTOR;
		temp[2] = res1[2] * SPLIT_FACTOR;
		temp[3] = res1[3] * SPLIT_FACTOR;
		temp[4] = res1[4] * SPLIT_FACTOR;
		temp[5] = res1[5] * SPLIT_FACTOR;
		temp[6] = res1[6] * SPLIT_FACTOR;
		temp[7] = res1[7] * SPLIT_FACTOR;

		XH[0] = temp[0] - res1[0];
		XH[1] = temp[1] - res1[1];
		XH[2] = temp[2] - res1[2];
		XH[3] = temp[3] - res1[3];
		XH[4] = temp[4] - res1[4];
		XH[5] = temp[5] - res1[5];
		XH[6] = temp[6] - res1[6];
		XH[7] = temp[7] - res1[7];

		XH[0] = temp[0] - XH[0];
		XH[1] = temp[1] - XH[1];
		XH[2] = temp[2] - XH[2];
		XH[3] = temp[3] - XH[3];
		XH[4] = temp[4] - XH[4];
		XH[5] = temp[5] - XH[5];
		XH[6] = temp[6] - XH[6];
		XH[7] = temp[7] - XH[7];

		XL[0] = res1[0] - XH[0];
		XL[1] = res1[1] - XH[1];
		XL[2] = res1[2] - XH[2];
		XL[3] = res1[3] - XH[3];
		XL[4] = res1[4] - XH[4];
		XL[5] = res1[5] - XH[5];
		XL[6] = res1[6] - XH[6];
		XL[7] = res1[7] - XH[7];

		// Split de res2

		temp[0] = res2[0] * SPLIT_FACTOR;
		temp[1] = res2[1] * SPLIT_FACTOR;
		temp[2] = res2[2] * SPLIT_FACTOR;
		temp[3] = res2[3] * SPLIT_FACTOR;
		temp[4] = res2[4] * SPLIT_FACTOR;
		temp[5] = res2[5] * SPLIT_FACTOR;
		temp[6] = res2[6] * SPLIT_FACTOR;
		temp[7] = res2[7] * SPLIT_FACTOR;

		YH[0] = temp[0] - res2[0];
		YH[1] = temp[1] - res2[1];
		YH[2] = temp[2] - res2[2];
		YH[3] = temp[3] - res2[3];
		YH[4] = temp[4] - res2[4];
		YH[5] = temp[5] - res2[5];
		YH[6] = temp[6] - res2[6];
		YH[7] = temp[7] - res2[7];

		YH[0] = temp[0] - YH[0];
		YH[1] = temp[1] - YH[1];
		YH[2] = temp[2] - YH[2];
		YH[3] = temp[3] - YH[3];
		YH[4] = temp[4] - YH[4];
		YH[5] = temp[5] - YH[5];
		YH[6] = temp[6] - YH[6];
		YH[7] = temp[7] - YH[7];

		YL[0] = res2[0] - YH[0];
		YL[1] = res2[1] - YH[1];
		YL[2] = res2[2] - YH[2];
		YL[3] = res2[3] - YH[3];
		YL[4] = res2[4] - YH[4];
		YL[5] = res2[5] - YH[5];
		YL[6] = res2[6] - YH[6];
		YL[7] = res2[7] - YH[7];

		// Extraction de l'exposant

		exponent[0].data = fabs(XH[0]);
		exponent[1].data = fabs(XH[1]);
		exponent[2].data = fabs(XH[2]);
		exponent[3].data = fabs(XH[3]);
		exponent[4].data = fabs(XH[4]);
		exponent[5].data = fabs(XH[5]);
		exponent[6].data = fabs(XH[6]);
		exponent[7].data = fabs(XH[7]);
		exponent[8].data = fabs(YH[0]);
		exponent[9].data = fabs(YH[1]);
		exponent[10].data = fabs(YH[2]);
		exponent[11].data = fabs(YH[3]);
		exponent[12].data = fabs(YH[4]);
		exponent[13].data = fabs(YH[5]);
		exponent[14].data = fabs(YH[6]);
		exponent[15].data = fabs(YH[7]);

		index[0] = exponent[0].binary >> 52;
		index[1] = exponent[1].binary >> 52;
		index[2] = exponent[2].binary >> 52;
		index[3] = exponent[3].binary >> 52;
		index[4] = exponent[4].binary >> 52;
		index[5] = exponent[5].binary >> 52;
		index[6] = exponent[6].binary >> 52;
		index[7] = exponent[7].binary >> 52;
		index[8] = exponent[8].binary >> 52;
		index[9] = exponent[9].binary >> 52;
		index[10] = exponent[10].binary >> 52;
		index[11] = exponent[11].binary >> 52;
		index[12] = exponent[12].binary >> 52;
		index[13] = exponent[13].binary >> 52;
		index[14] = exponent[14].binary >> 52;
		index[15] = exponent[15].binary >> 52;

		index[16] = index[0] - 27;
		index[17] = index[1] - 27;
		index[18] = index[2] - 27;
		index[19] = index[3] - 27;
		index[20] = index[4] - 27;
		index[21] = index[5] - 27;
		index[22] = index[6] - 27;
		index[23] = index[7] - 27;
		index[24] = index[8] - 27;
		index[25] = index[9] - 27;
		index[26] = index[10] - 27;
		index[27] = index[11] - 27;
		index[28] = index[12] - 27;
		index[29] = index[13] - 27;
		index[30] = index[14] - 27;
		index[31] = index[15] - 27;

		index[16] = (index[16] > 0) ? index[16] : 0;
		index[17] = (index[17] > 0) ? index[17] : 0;
		index[18] = (index[18] > 0) ? index[18] : 0;
		index[19] = (index[19] > 0) ? index[19] : 0;
		index[20] = (index[20] > 0) ? index[20] : 0;
		index[21] = (index[21] > 0) ? index[21] : 0;
		index[22] = (index[22] > 0) ? index[22] : 0;
		index[23] = (index[23] > 0) ? index[23] : 0;
		index[24] = (index[24] > 0) ? index[24] : 0;
		index[25] = (index[25] > 0) ? index[25] : 0;
		index[26] = (index[26] > 0) ? index[26] : 0;
		index[27] = (index[27] > 0) ? index[27] : 0;
		index[28] = (index[28] > 0) ? index[28] : 0;
		index[29] = (index[29] > 0) ? index[29] : 0;
		index[30] = (index[30] > 0) ? index[30] : 0;
		index[31] = (index[31] > 0) ? index[31] : 0;

		C[index[0]] += XH[0];
		C[index[1]] += XH[1];
		C[index[2]] += XH[2];
		C[index[3]] += XH[3];
		C[index[4]] += XH[4];
		C[index[5]] += XH[5];
		C[index[6]] += XH[6];
		C[index[7]] += XH[7];
		C[index[8]] += YH[0];
		C[index[9]] += YH[1];
		C[index[10]] += YH[2];
		C[index[11]] += YH[3];
		C[index[12]] += YH[4];
		C[index[13]] += YH[5];
		C[index[14]] += YH[6];
		C[index[15]] += YH[7];
		C[index[16]] += XL[0];
		C[index[17]] += XL[1];
		C[index[18]] += XL[2];
		C[index[19]] += XL[3];
		C[index[20]] += XL[4];
		C[index[21]] += XL[5];
		C[index[22]] += XL[6];
		C[index[23]] += XL[7];
		C[index[24]] += YL[0];
		C[index[25]] += YL[1];
		C[index[26]] += YL[2];
		C[index[27]] += YL[3];
		C[index[28]] += YL[4];
		C[index[29]] += YL[5];
		C[index[30]] += YL[6];
		C[index[31]] += YL[7];

		X += 8;
		Y += 8;

	}

}


