/*
 *************************************************************************************
 Copyright (c) 2016, University of Perpignan
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************
*/


#ifndef RTNBLAS_IMPL_HPP
#define RTNBLAS_IMPL_HPP


// Writing your own STL container
namespace simd{

  template<class V>
  class vb64
  {
  private:
    V data;
  public:
    static const unsigned int vb64_size = sizeof(V)/sizeof(double);

    vb64() { }
    vb64(vb64 &v): data(v.data) { }
    vb64(V v): data(v) {}
    vb64(double d) { _mm_set1_pd(d); }

    __inline V __attribute__((__gnu_inline__, __always_inline__, __artificial__))
    operator+=(V v) { this.data = _mm_add_pd(this.data, v.data); return *this; }
    __inline V __attribute__((__gnu_inline__, __always_inline__, __artificial__))
    operator-=(V v) { this.data = _mm_sub_pd(this.data, v.data); return *this; }
    __inline V __attribute__((__gnu_inline__, __always_inline__, __artificial__))
    operator*=(V v) { this.data = _mm_mul_pd(this.data, v.data); return *this; }
  };

  __inline vb64 __attribute__((__gnu_inline__, __always_inline__, __artificial__))
  operator+(vb64 v1, vb64 v2) { vb64 res(v1); res += v2; return res; }
  __inline vb64 __attribute__((__gnu_inline__, __always_inline__, __artificial__))
  operator-(vb64 v1, vb64 v2) { vb64 res(v1); res -= v2; return res; }
  __inline vb64 __attribute__((__gnu_inline__, __always_inline__, __artificial__))
  operator*(vb64 v1, vb64 v2) { vb64 res(v1); res *= v2; return res; }
 
}

#endif
