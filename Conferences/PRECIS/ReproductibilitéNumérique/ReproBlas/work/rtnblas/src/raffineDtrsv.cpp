double dgemvTrsvLineSequentialIFastSum(unsigned int size, double *A, int IncA, double *X, int IncX, double Y) {
	double *C = (double*) memalign(64, (size * 2 + 1) * sizeof(double));
	unsigned long count;
	sequentialVectorTwoProdTrans(size, A, X, C, &count);
	C[count] = -Y;
	double S = iFastSum(count + 1, C);
	free(C);
	return S;
}

double dgemvTrsvLineSequentialOnlineExact(unsigned int size, double *A, int IncA, double *X, int IncX, double Y) {
	double C[ONLINEEXACTSIZE + 1] __attribute__ ((aligned(64))) = {0};
	ddotSequentialExtractOnlineExact(size, A, X, C);
	C[ONLINEEXACTSIZE] = -Y;
	return iFastSum(ONLINEEXACTSIZE + 1, C);
}

double dgemvTrsvLineSequentialHybridSum(unsigned int size, double *A, int IncA, double *X, int IncX, double Y) {
	double C[HYBRIDSUMSIZE + 1] __attribute__ ((aligned(64))) = {0};
	ddotSequentialExtractHybridSum(size, A, X, C);
	C[HYBRIDSUMSIZE] = -Y;
	return iFastSum(HYBRIDSUMSIZE + 1, C);
}

double dgemvTrsvLineSequentialAccSum(unsigned int size, double *A, int IncA, double *X, int IncX, double Y) {
	double *C = (double*) memalign(64, (size * 2 + 1) * sizeof(double));
	double absSum = sequentialTwoProdSum(size, size, A, X, C) / (1 - (size * eps));
	C[size * 2] = -Y;
	absSum += fabs(Y);
	double S = sequentialAccSumCallBack(size * 2 + 1, C, nextPower2(absSum));
	free(C);
	return S;
}

double dgemvTrsvLineSequentialFastAccSum(unsigned int size, double *A, int IncA, double *X, int IncX, double Y) {
	double *C = (double*) memalign(64, (size * 2 + 1) * sizeof(double));
	double absSum = sequentialTwoProdSum(size, size, A, X, C) / (1 - (size * eps));
	C[size * 2] = -Y;
	absSum += fabs(Y);
	double S = sequentialFastAccSumCallBack(size * 2 + 1, C, absSum);
	free(C);
	return S;
}

void dgemvTrsvSequentialOnlineExact(int layout, unsigned int n, double *A,unsigned int lda, double *X, int IncX, double *Y, int IncY) {
	for (unsigned int i = 0; i < n; i++) {
		Y[i] = dgemvTrsvLineSequentialOnlineExact(n, A, 1, X, IncX, Y[i]);
		A += lda;
	}
}

void dgemvTrsvSequentialHybridSum(int layout, unsigned int n, double *A,unsigned int lda, double *X, int IncX, double *Y, int IncY) {
	for (unsigned int i = 0; i < n; i++) {
		Y[i] = dgemvTrsvLineSequentialHybridSum(n, A, 1, X, IncX, Y[i]);
		A += lda;
	}
}

void dgemvTrsvSequentialIFastSum(int layout, unsigned int n, double *A,unsigned int lda, double *X, int IncX, double *Y, int IncY) {
	for (unsigned int i = 0; i < n; i++) {
		Y[i] = dgemvTrsvLineSequentialIFastSum(n, A, 1, X, IncX, Y[i]);
		A += lda;
	}
}

void dgemvTrsvSequentialFastAccSum(int layout, unsigned int n, double *A,unsigned int lda, double *X, int IncX, double *Y, int IncY) {
	for (unsigned int i = 0; i < n; i++) {
		Y[i] = dgemvTrsvLineSequentialFastAccSum(n, A, 1, X, IncX, Y[i]);
		A += lda;
	}
}

void dgemvTrsvSequentialAccSum(int layout, unsigned int n, double *A,unsigned int lda, double *X, int IncX, double *Y, int IncY) {
	for (unsigned int i = 0; i < n; i++) {
		Y[i] = dgemvTrsvLineSequentialAccSum(n, A, 1, X, IncX, Y[i]);
		A += lda;
	}
}

void dgemvTrsvSequentialDynamic(int layout, unsigned int n, double *A,unsigned int lda, double *X, int IncX, double *Y, int IncY) {
	if (n < 1000) {
		for (unsigned int i = 0; i < n; i++) {
			Y[i] = dgemvTrsvLineSequentialIFastSum(n, A, 1, X, IncX, Y[i]);
			A += lda;
		}
	}
	else {
		for (unsigned int i = 0; i < n; i++) {
			Y[i] = dgemvTrsvLineSequentialHybridSum(n, A, 1, X, IncX, Y[i]);
			A += lda;
		}
	}
}

void raffineDtrsvSequentialIFastSum(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	double* b = (double*) memalign(64, n * sizeof(double));
	double* d = (double*) memalign(64, n * sizeof(double));
	memcpy(b, X, n * sizeof(double));
	memcpy(d, b, n * sizeof(double));
	dtrsvSequentialIFastSum(layout, uplo, trans, diag, n, A, lda, X, IncX);
	dgemvTrsvSequentialIFastSum(layout, n, A, lda, X, IncX, d, 1);
	int correct = 1;
	for (unsigned int i = 0; i < n; i++) if (d[i] >= (ulp(b[i]) / 2)) correct = 0;
	if (correct) return;
	while (!correct) {
		dtrsvSequentialIFastSum(layout, uplo, trans, diag, n, A, lda, d, IncX);
		for (unsigned int i = 0; i < n; i++) X[i] -= d[i];
		memcpy(d, b, n * sizeof(double));
		dgemvTrsvSequentialIFastSum(layout, n, A, lda, X, IncX, d, 1);
		int correct = 1;
		for (unsigned int i = 0; i < n; i++) if (d[i] >= (ulp(b[i]) / 2)) correct = 0;
	}
	free(b);
	free(d);
}

void raffineDtrsvSequentialHybridSum(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	double* b = (double*) memalign(64, n * sizeof(double));
	double* d = (double*) memalign(64, n * sizeof(double));
	memcpy(b, X, n * sizeof(double));
	memcpy(d, b, n * sizeof(double));
	dtrsvSequentialHybridSum(layout, uplo, trans, diag, n, A, lda, X, IncX);
	dgemvTrsvSequentialHybridSum(layout, n, A, lda, X, IncX, d, 1);
	int correct = 1;
	for (unsigned int i = 0; i < n; i++) if (d[i] >= (ulp(b[i]) / 2)) correct = 0;
	if (correct) return;
	while (!correct) {
		dtrsvSequentialHybridSum(layout, uplo, trans, diag, n, A, lda, d, IncX);
		for (unsigned int i = 0; i < n; i++) X[i] -= d[i];
		memcpy(d, b, n * sizeof(double));
		dgemvTrsvSequentialHybridSum(layout, n, A, lda, X, IncX, d, 1);
		int correct = 1;
		for (unsigned int i = 0; i < n; i++) if (d[i] >= (ulp(b[i]) / 2)) correct = 0;
	}
	free(b);
	free(d);
}

void raffineDtrsvSequentialOnlineExact(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	double* b = (double*) memalign(64, n * sizeof(double));
	double* d = (double*) memalign(64, n * sizeof(double));
	memcpy(b, X, n * sizeof(double));
	memcpy(d, b, n * sizeof(double));
	dtrsvSequentialOnlineExact(layout, uplo, trans, diag, n, A, lda, X, IncX);
	dgemvTrsvSequentialOnlineExact(layout, n, A, lda, X, IncX, d, 1);
	int correct = 1;
	for (unsigned int i = 0; i < n; i++) if (d[i] >= (ulp(b[i]) / 2)) correct = 0;
	if (correct) return;
	while (!correct) {
		dtrsvSequentialOnlineExact(layout, uplo, trans, diag, n, A, lda, d, IncX);
		for (unsigned int i = 0; i < n; i++) X[i] -= d[i];
		memcpy(d, b, n * sizeof(double));
		dgemvTrsvSequentialOnlineExact(layout, n, A, lda, X, IncX, d, 1);
		int correct = 1;
		for (unsigned int i = 0; i < n; i++) if (d[i] >= (ulp(b[i]) / 2)) correct = 0;
	}
	free(b);
	free(d);
}


void raffineDtrsvSequentialAccSum(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	double* b = (double*) memalign(64, n * sizeof(double));
	double* d = (double*) memalign(64, n * sizeof(double));
	memcpy(b, X, n * sizeof(double));
	memcpy(d, b, n * sizeof(double));
	dtrsvSequentialAccSum(layout, uplo, trans, diag, n, A, lda, X, IncX);
	dgemvTrsvSequentialAccSum(layout, n, A, lda, X, IncX, d, 1);
	int correct = 1;
	for (unsigned int i = 0; i < n; i++) if (d[i] >= (ulp(b[i]) / 2)) correct = 0;
	if (correct) return;
	while (!correct) {
		dtrsvSequentialAccSum(layout, uplo, trans, diag, n, A, lda, d, IncX);
		for (unsigned int i = 0; i < n; i++) X[i] -= d[i];
		memcpy(d, b, n * sizeof(double));
		dgemvTrsvSequentialAccSum(layout, n, A, lda, X, IncX, d, 1);
		int correct = 1;
		for (unsigned int i = 0; i < n; i++) if (d[i] >= (ulp(b[i]) / 2)) correct = 0;
	}
	free(b);
	free(d);
}

void raffineDtrsvSequentialFastAccSum(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	double* b = (double*) memalign(64, n * sizeof(double));
	double* d = (double*) memalign(64, n * sizeof(double));
	memcpy(b, X, n * sizeof(double));
	memcpy(d, b, n * sizeof(double));
	dtrsvSequentialFastAccSum(layout, uplo, trans, diag, n, A, lda, X, IncX);
	dgemvTrsvSequentialFastAccSum(layout, n, A, lda, X, IncX, d, 1);
	int correct = 1;
	for (unsigned int i = 0; i < n; i++) if (d[i] >= (ulp(b[i]) / 2)) correct = 0;
	if (correct) return;
	while (!correct) {
		dtrsvSequentialFastAccSum(layout, uplo, trans, diag, n, A, lda, d, IncX);
		for (unsigned int i = 0; i < n; i++) X[i] -= d[i];
		memcpy(d, b, n * sizeof(double));
		dgemvTrsvSequentialFastAccSum(layout, n, A, lda, X, IncX, d, 1);
		int correct = 1;
		for (unsigned int i = 0; i < n; i++) if (d[i] >= (ulp(b[i]) / 2)) correct = 0;
	}
	free(b);
	free(d);
}

void raffineDtrsvSequentialDynamic(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	double* b = (double*) memalign(64, n * sizeof(double));
	double* d = (double*) memalign(64, n * sizeof(double));
	memcpy(b, X, n * sizeof(double));
	memcpy(d, b, n * sizeof(double));
	dtrsvSequentialDynamic(layout, uplo, trans, diag, n, A, lda, X, IncX);
	dgemvTrsvSequentialDynamic(layout, n, A, lda, X, IncX, d, 1);
	int correct = 1;
	for (unsigned int i = 0; i < n; i++) if (d[i] >= (ulp(b[i]) / 2)) correct = 0;
	if (correct) return;
	while (!correct) {
		dtrsvSequentialDynamic(layout, uplo, trans, diag, n, A, lda, d, IncX);
		for (unsigned int i = 0; i < n; i++) X[i] -= d[i];
		memcpy(d, b, n * sizeof(double));
		dgemvTrsvSequentialDynamic(layout, n, A, lda, X, IncX, d, 1);
		int correct = 1;
		for (unsigned int i = 0; i < n; i++) if (d[i] >= (ulp(b[i]) / 2)) correct = 0;
	}
	free(b);
	free(d);
}

void rtnblas_ol_dtrsv(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
#else
#endif
#elif defined(OMP_PARALLEL)
#else
	raffineDtrsvSequentialOnlineExact(layout, uplo, trans, diag, n, A, lda, X, IncX);
#endif
}

void rtnblas_hs_dtrsv(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
#else
#endif
#elif defined(OMP_PARALLEL)
#else
	raffineDtrsvSequentialHybridSum(layout, uplo, trans, diag, n, A, lda, X, IncX);
#endif
}

void rtnblas_ifs_dtrsv(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
#else
#endif
#elif defined(OMP_PARALLEL)
#else
	raffineDtrsvSequentialIFastSum(layout, uplo, trans, diag, n, A, lda, X, IncX);
#endif
}

void rtnblas_acs_dtrsv(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
#else
#endif
#elif defined(OMP_PARALLEL)
#else
	raffineDtrsvSequentialAccSum(layout, uplo, trans, diag, n, A, lda, X, IncX);
#endif
}

void rtnblas_facs_dtrsv(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
#else
#endif
#elif defined(OMP_PARALLEL)
#else
	raffineDtrsvSequentialFastAccSum(layout, uplo, trans, diag, n, A, lda, X, IncX);
#endif
}

void rtnblas_dtrsv(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
#else
#endif
#elif defined(OMP_PARALLEL)
#else
	raffineDtrsvSequentialDynamic(layout, uplo, trans, diag, n, A, lda, X, IncX);
#endif
}


