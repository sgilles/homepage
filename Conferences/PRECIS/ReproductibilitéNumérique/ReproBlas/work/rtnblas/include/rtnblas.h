/*
 *************************************************************************************
 Copyright (c) 2015, University of Perpignan
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************
*/


#ifndef RTNBLAS_H_INCLUDED
#define RTNBLAS_H_INCLUDED

void rtnblasInitialise();

double rtnblas_hs_ddot(unsigned long int, double*, int, double*, int);
double rtnblas_ol_ddot(unsigned long int, double*, int, double*, int);
double rtnblas_facs_ddot(unsigned long int, double*, int, double*, int);
double rtnblas_acs_ddot(unsigned long int, double*, int, double*, int);
double rtnblas_ifs_ddot(unsigned long int, double*, int, double*, int);
double rtnblas_ddot(unsigned long int, double*, int, double*, int);
double classic_ddot(unsigned long, double*, int, double*, int);

double rtnblas_hs_dnrm2(unsigned long int, double*, int);
double rtnblas_ol_dnrm2(unsigned long int, double*, int);
double rtnblas_facs_dnrm2(unsigned long, double*, int);
double rtnblas_acs_dnrm2(unsigned long, double*, int);
double rtnblas_ifs_dnrm2(unsigned long, double*, int);
double rtnblas_dnrm2(unsigned long, double*, int);
double classic_dnrm2(unsigned long, double*, int);

double rtnblas_dasum(unsigned long, double*, int);

void rtnblas_ol_dgemv(int, int, unsigned int, unsigned int, double, double*, unsigned int, double*, int, double, double*, int);
void rtnblas_hs_dgemv(int, int, unsigned int, unsigned int, double, double*, unsigned int, double*, int, double, double*, int);
void rtnblas_ifs_dgemv(int, int, unsigned int, unsigned int, double, double*, unsigned int, double*, int, double, double*, int);
void rtnblas_dgemv(int, int, unsigned int, unsigned int, double, double*, unsigned int, double*, int, double, double*, int);

void rtnblas_ol_dtrsv(int, int, int, int, unsigned int, double*, unsigned int, double*, int);
void rtnblas_hs_dtrsv(int, int, int, int, unsigned int, double*, unsigned int, double*, int);
void rtnblas_ifs_dtrsv(int, int, int, int, unsigned int, double*, unsigned int, double*, int);
void rtnblas_facs_dtrsv(int, int, int, int, unsigned int, double*, unsigned int, double*, int);
void rtnblas_acs_dtrsv(int, int, int, int, unsigned int, double*, unsigned int, double*, int);
void rtnblas_dtrsv(int, int, int, int, unsigned int, double*, unsigned int, double*, int);

void rtnblas_ol_floatdtrsv(int, int, int, int, unsigned int, double*, unsigned int, double*, int);
void rtnblas_hs_floatdtrsv(int, int, int, int, unsigned int, double*, unsigned int, double*, int);
void rtnblas_ifs_floatdtrsv(int, int, int, int, unsigned int, double*, unsigned int, double*, int);
void rtnblas_facs_floatdtrsv(int, int, int, int, unsigned int, double*, unsigned int, double*, int);
void rtnblas_acs_floatdtrsv(int, int, int, int, unsigned int, double*, unsigned int, double*, int);
void rtnblas_floatdtrsv(int, int, int, int, unsigned int, double*, unsigned int, double*, int);

void sequentialVectorTwoProdTrans(unsigned long, double*, double*, double*, unsigned long*);
void ddotSequentialExtractHybridSum(unsigned long, double*, double*, double*);

#ifdef STATS
void getRtnBlasStatistics(unsigned long*, unsigned long*, unsigned long*, unsigned long*);
#endif

#endif

