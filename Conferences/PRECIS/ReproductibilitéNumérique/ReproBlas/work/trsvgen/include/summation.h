/*
 *************************************************************************************
 Copyright (c) 2015, University of Perpignan
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************
*/


#ifndef SUMMATION_H_INCLUDED
#define SUMMATION_H_INCLUDED

#define RDTSC

#include <immintrin.h>

#ifdef STATISTICS
#include <summation_statistics.h>
#endif

unsigned long int ABS_MASK = 0x7FFFFFFFFFFFFFFF;
double SPLIT_FACTOR, eps, eta;
__m256d _M256_SPLIT_FACTOR;
__m256d _M256_ABS_MASK;

union binaryDouble {
	double data;
	unsigned long int binary;
};

union binaryVector {
	__m256d data;
	unsigned long int binary[4];
};

unsigned long long rdtsc();
void summationInitialise();
double round3(double S0, double S1, int s);
void split(double A, double* high, double* low);
void twoSum(double a, double b, double* A, double* B);
double naiveSum(unsigned long int size, double *A);
double optimizedSum(unsigned long int size, double *A);
void extractOnlineExact(unsigned long  int sizeA, double* A, double* C);
void extractHybridSum(unsigned long  int sizeA, double* A, double* C);
double iFastSum(unsigned long  int size, double* A);
double onlineExact(unsigned long  int size, double* A);
double hybridSum(unsigned long  int size, double* A);
double accSumCallBack(unsigned long  int size, double* A, double sigma);
double accSum(unsigned long int size, double* A);
double fastAccSumCallBack(unsigned long  int size, double* A, double sigma);
double fastAccSum(unsigned long int size, double* A);

#endif

