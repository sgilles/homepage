/*
 *************************************************************************************
 Copyright (c) 2015, University of Perpignan
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************
*/

#ifndef __SUM__
#define __SUM__

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include "dp_tools.h"

/* Maximal value for K */
#define KMAX 8

/* Recursive summation algorithm. */
double Sum(double *p, unsigned int n);

/* Recursive summation algorithm with the computation
   of an error bound: a priori error bound and running
   error bound respectively. */
double Sum_aeb(double *beta, double *p, unsigned int n);
double Sum_reb(double *beta, double *p, unsigned int n);

/* Compensated summation algorithm from Rump, Ogita and Oishi's
   paper "Accurate sum and dot-product". As accurate as if computed
   in doubled working precision, with a final rounding back to
   double precision. */
double Sum2(double *p, unsigned int n);

/* Idem as Sum2 but in tripled working precision. */
double Sum3(double *p, unsigned int n);

/* Compensated summation algorithm from Rump, Ogita and Oishi's
   paper "Accurate sum and dot-product". As accurate as if computed
   in K-fold working precision, with a final rounding back to
   double precision. Overwrites the input vector.*/
double SumKIn(double *p, unsigned int n, unsigned int K);
double SumKInBound(double *bound, double *p, unsigned int n, unsigned int K);

/* Idem as SumKIn but do not overwrite the input vector. */
double SumK(double *p, unsigned int n, unsigned int K);

/* Idem as SumKIn but proceed in a "vertical" manner. */
double SumKvert(double *p, unsigned int n, unsigned int K);

/*double-double summation */
double DDSum(double *p, unsigned int n);
double DDSum2(double *x, unsigned int n);
double DDSum3(double *x, unsigned int n);

/* Accurate summation algorithm from Rump, Ogita and Oishi's
   paper "Accurate floating point summation". 
   Faithful rounding (1 ulp). Overwrites the input vector. */
double AccSumIn(double *p, unsigned int n);

/* Idem as AccSumIn but do not overwrite the input vector. */
double AccSum(double *p, unsigned int n);

/* Fast Accurate summation algorithm from Rump, 
   paper "Ultimately Fast Accurate floating point summation". 
   Faithful rounding (1 ulp). Overwrites the input vector. */
double FastAccSumIn(double *p, unsigned int n);

/* Idem as AccSumIn but do not overwrite the input vector. */
double FastAccSum(double *p, unsigned int n);

/* Zhu and Hayes, SISC 2009. 
Correct rounding (1/2 ulp). Overwrites the input vector. */
double iFastSumIn(double *p, unsigned int n);

/* Idem as iFastSumIn but do not overwrite the input vector. */
double iFastSum(double *p, unsigned int n);

/* Zhu and Hayes, SISC 2009. 
Correct rounding (1/2 ulp). Overwrites the input vector.  */
double HybridSum(double *p, unsigned int n);

//double VHybridSum(double *p, unsigned int n);
//double V2HybridSum(double *p, unsigned int n);

/* Zhu and Hayes, TOMS 2010.
Correct rounding (1/2 ulp). Overwrites the input vector.  */
double OnlineExactTwoSum(double *p, unsigned int n);
double OnlineExactFastTwoSum(double *p, unsigned int n);

double VOnlineExactSum(double *p, unsigned int n);

/* ------------------------------------------------------------- */
/* Dot product */
/* ------------------------------------------------------------- */
/* double Dot(double *x, double *y, unsigned int n); */
/* double Dot2(double *x, double *y, unsigned int n); */
/* double Dot2_aeb(double *beta, double *x, double *y, unsigned int n); */
/* double Dot2_reb(double *beta, double *x, double *y, unsigned int n); */
/* double AccDot(double *x, double *y, unsigned int n); */
/* double CompErrorDot(double *x, double *y, unsigned int n, double r); */
/* double GenDot(double *x, double *y, double *C, unsigned int n, double c); */
/* double RandDot(double *x, double *y, double *C, unsigned int n); */
/* double RandDot01(double *x, double *y, double *C, unsigned int n); */

#endif
