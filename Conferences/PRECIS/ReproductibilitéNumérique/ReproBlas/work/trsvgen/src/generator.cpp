/*
 *************************************************************************************
 Copyright (c) 2015, University of Perpignan
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************
*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>
#include <rtnblas.h>
#include <mkl.h>
#include <mpfr.h>

void mpfr_trsv(unsigned int size, double* A, double *x, double *b) {
	mpfr_set_default_prec(2500);
	mpfr_t* mpfrX = (mpfr_t*) malloc(size * sizeof(mpfr_t));
	mpfr_t mpfrA;
	mpfr_init(mpfrA);
	for (unsigned int i = 0; i < size; i++) {
		mpfr_init(mpfrX[i]);
		mpfr_set_d(mpfrX[i], b[i], MPFR_RNDN);
	}
	for (unsigned int line = 0; line < size; line++) {
		for (unsigned int column = 0; column < line; column++) {
			mpfr_set_d(mpfrA, A[column], MPFR_RNDN);
			mpfr_mul(mpfrA, mpfrA, mpfrX[column], MPFR_RNDN);
			mpfr_sub(mpfrX[line], mpfrX[line], mpfrA, MPFR_RNDN);
		}
		mpfr_div_d(mpfrX[line], mpfrX[line], A[line], MPFR_RNDN);
		A += size;
		x[line] = mpfr_get_d(mpfrX[line], MPFR_RNDN);
	}
}

int main(int argc, char **argv) {
	int r = 1;
	double C;
	unsigned long n = 0;
	FILE *fileRead = fopen(argv[1], "rb");
	FILE *fileWrite = fopen(argv[2], "wb");
	r = fread(&n, sizeof(unsigned long), 1, fileRead);
	double *A = (double*) memalign(64, sizeof(double) * n * n);
	double *x = (double*) memalign(64, sizeof(double) * n);
	double *b = (double*) memalign(64, sizeof(double) * n);
	r = fread(A, sizeof(double), n * n, fileRead);
	r = fread(b, sizeof(double), n, fileRead);
	r = fread(&C, sizeof(double), 1, fileRead);
	rtnblas_dgemv(CblasRowMajor, CblasNoTrans, n, n, 1, A, n, x, 1, 0, b, 1);
	mpfr_trsv(n, A, x, b);
	r = fwrite(&n, sizeof(unsigned long), 1, fileWrite);
	r = fwrite(A, sizeof(double), n * n, fileWrite);
	r = fwrite(x, sizeof(double), n, fileWrite);
	r = fwrite(b, sizeof(double), n, fileWrite);
	r = fwrite(&C, sizeof(double), 1, fileWrite);
	r = fwrite(&C, sizeof(double), 1, fileWrite);
	free(A);
	free(x);
	free(b);
	fclose(fileRead);
	fclose(fileWrite);
	return EXIT_SUCCESS;
}

