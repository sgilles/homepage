/*
 *************************************************************************************
 Copyright (c) 2015, University of Perpignan
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************
*/


#ifndef SUMMATION_H_INCLUDED
#define SUMMATION_H_INCLUDED

#define RDTSC

#include <immintrin.h>

#ifdef MPI_PARALLEL
#include <mpi.h>
#endif

unsigned long long rdtsc();

union binaryDouble {
	double data;
	unsigned long binary;
};

union binaryVector {
	__m256d data;
	unsigned long binary[4];
};

#ifdef STATS
void getSummationStatistics(unsigned long *paramLocalSumCycles);
#endif

double nextPower2(double);
double ufp(double);
double ulp(double);
void split(double A, double* high, double* low);

void addIFP(double*, int*, double*, int*, double*, int*);
void extractVectorOneReduction(unsigned long size, double* A, double* M);
void extractVectorOneReductionLight(unsigned long size, double* A, double* M);
void extractVectorOneReductionIR(unsigned long size, double* A, double* M);
double oneReduction(unsigned long, double*);

void twoSum(double, double, double*, double*);
void summationInitialise();

#ifdef MPI_PARALLEL
void ADD_QUAD(double *invec, double *inoutvec, int *len, MPI_Datatype *datatype);
#endif


void eliminateZeros(unsigned long* size, double* C);
void transVector(unsigned long*, double*);


double optimizedSum(unsigned long, double*);
double absSum(unsigned long, double*);
double onlineExact(unsigned long, double*);
double hybridSum(unsigned long, double*);
double fastAccSum(unsigned long, double*);
double accSum(unsigned long, double*);
double iFastSum(unsigned long, double*);
double dynamicSum(unsigned long, double*);
double sumK2(unsigned long, double*);
double sumK3(unsigned long, double*);


double fastReprodSum(unsigned long, double*);
double reprodSum(unsigned long, double*);


void sequentialTransSmallVector(unsigned long*, double*);
void ompTransSmallVector(unsigned long*, double*);
double sequentialAbsMax(unsigned long, double*);
double ompAbsMax(unsigned long, double*);
double mpiAbsMax(unsigned long, double*);
double hybridAbsMax(unsigned long, double*);


double sequentialAccSumCallBack(unsigned long, double*, double);
double ompAccSumCallBack(unsigned long, double*, double);
double mpiAccSumCallBack(unsigned long, double*, double);
double hybridAccSumCallBack(unsigned long, double*, double);


double sequentialFastAccSumCallBack(unsigned long, double*, double);


double sequentialFastReprodSumCallBack(unsigned long, double*, double);
double ompFastReprodSumCallBack(unsigned long, double*, double);
double mpiFastReprodSumCallBack(unsigned long, double*, double);
double hybridFastReprodSumCallBack(unsigned long, double*, double);


double sequentialReprodSumCallBack(unsigned long, double*, double);
double ompReprodSumCallBack(unsigned long, double*, double);
double mpiReprodSumCallBack(unsigned long, double*, double);
double hybridReprodSumCallBack(unsigned long, double*, double);


double iFastSumCallBack(unsigned long*, double*, int);


#endif

