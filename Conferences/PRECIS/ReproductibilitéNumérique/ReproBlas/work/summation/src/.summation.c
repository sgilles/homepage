
//=========================================================================================================================================
// Name        : preciseBLAS.c
// Author      : CHOHRA Chems-eddine
// Version     : 1.0
// Copyright   : C.C
// Description : BLAS précises et performantes
//=========================================================================================================================================

#include <math.h>
#include <malloc.h>
#include <stdlib.h>
#include <immintrin.h>
#include <cblas.h>
#include <summation.h>

#if defined(__i386__)

unsigned long long rdtsc(void) {
	unsigned long long int x;
	__asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
	return x;
}

#elif defined(__x86_64__)

unsigned long long rdtsc(void) {
	unsigned hi, lo;
	__asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
	return ((unsigned long long) lo) | (((unsigned long long) hi) << 32);
}

#endif

void summationInitialise() {
	SPLIT_FACTOR = pow(2, 27) + 1;
	eps = pow(2, -53);
	eta = pow(2, -1074);
	_M256_SPLIT_FACTOR = _mm256_broadcast_sd(&SPLIT_FACTOR);
	_M256_ABS_MASK = _mm256_broadcast_sd((double*) &ABS_MASK);
}

//=========================================================================================================================================
// Une fonction qui fait le split de deux flottants
// Entrée : Un flottant A
// Sorties : high=A[53:27] and low=A[26:0]
//=========================================================================================================================================
void split(double A, double* high, double* low) {
	double temp = A * SPLIT_FACTOR;
	*high = temp - (temp - A);
	*low = A - *high;
}

//=========================================================================================================================================
// Une fonction qui réalise la transformation exacte de la somme de deux scallers
// Entrées : a, b
// Sorties : A=a+b[haut], B=a+b[bas]
//=========================================================================================================================================
void twoSum(double a, double b, double* A, double* B) {
	*A = a + b;
	*B = *A - a;
	*B = (a - (*A - *B)) + (b - *B);
}

//=========================================================================================================================================
// Une fonction qui réalise la transformation exacte de la somme de deux scallers
// Entrées : a, b
// Sorties : A=a+b[haut], B=a+b[bas]
//=========================================================================================================================================
void fastTwoSum(double a, double b, double* A, double* B) {
	*A = a + b;
	a = *A - a;
	*B = b - a;
}

//=========================================================================================================================================
// Une fonction qui renvoie l'ulp d'un flottant
// Entrées : Un flottant x
// Retourne : l'ulp de x
//=========================================================================================================================================
double ulp(double x) {
	x = fabs(x);
	double temp = x * eps;
	temp = (temp + x) - x;
	if (temp == 0) return x * eps * 2;
	else return temp;
}

//=========================================================================================================================================
// Une fonction qui renvoie la puissance de 2 suivante d'un flottant
// Entrées : Un flottant x
// Retourne : la puissance de 2 suivante de x
//=========================================================================================================================================
double nextPower2(double x) {
	register double intermediate = x / eps;
	x = fabs((intermediate - x) - intermediate);
	return x;
}

//=========================================================================================================================================
// Une fonction qui renvoie la somme optimisée d'un vecteur donné A
// Entrées : Un vecteur A
// Retourne : La somme des elements de A
//=========================================================================================================================================
double naiveSum(unsigned long int size, double *A) {
	double S = 0;
	for (unsigned long int i = 0; i < size; i++) S += A[i];
	return S;
}

//=========================================================================================================================================
// Une fonction qui renvoie la somme optimisée d'un vecteur donné A
// Entrées : Un vecteur A
// Retourne : La somme des elements de A
//=========================================================================================================================================
double optimizedSum(unsigned long int size, double *A) {

	// Préchargement des données de la première itération de la boucle
	__builtin_prefetch(A, 0, _MM_HINT_NTA);
	__builtin_prefetch(&A[8], 0, _MM_HINT_NTA);

	__m256d SP[4] = {_mm256_setzero_pd()};

	for (unsigned long int i = 0; i < size; i += 16) { // Déroulement par 4

		// Préchargement des données de la prochaine itération de la boucle
		__builtin_prefetch(&A[16], 0, _MM_HINT_NTA);
		__builtin_prefetch(&A[24], 0, _MM_HINT_NTA);

		// Itération 1
		SP[0] = _mm256_add_pd(SP[0], *(__m256d*) A);

		// Itération 2
		SP[1] = _mm256_add_pd(SP[1], *(__m256d*) &A[4]);

		// Itération 3
		SP[2] = _mm256_add_pd(SP[2], *(__m256d*) &A[8]);

		// Itération 4
		SP[3] = _mm256_add_pd(SP[3], *(__m256d*) &A[12]);

		A += 16;
	}

	// Réduction des vecteurs
	SP[0] = _mm256_add_pd(SP[0], SP[1]);
	SP[2] = _mm256_add_pd(SP[2], SP[3]);
	SP[0] = _mm256_add_pd(SP[0], SP[2]);

	// Réduction des scalers
	return ((SP[0][0] + SP[0][1]) + (SP[0][2] + SP[0][3]));
}

//=========================================================================================================================================
// Une fonction qui calcule l'arrondi de la somme de 3 nombres
// Entrée : deux flottants S0 et S1 et un boolean s
// retourn la somme 
//=========================================================================================================================================
double round3(double S0, double S1, int s) {
	if ((fabs(S1) == ulp(S0) / 2) && ((S1 > 0) == s)) return S0 + (S1 * 1.1);
	else return S0;
}

//=========================================================================================================================================
// Une fonction qui accumule les elements d'un vecteur selon l'exposent
// Entrées : A : Vecteurs de taille "n"
// Sortie : C : Un vecteur de taille "2048"
//=========================================================================================================================================
void extractHybridSum(unsigned long int sizeA, double* A, double* C) {
	__m256d res[2];
	union binaryVector exponent[2];
	
	for (unsigned long int i = 0; i < sizeA; i += 4) {
		__m256d temp = *(__m256d*) &A[i] * _M256_SPLIT_FACTOR;
		res[0] = temp - (temp - *(__m256d*) &A[i]);
		res[1] = *(__m256d*) &A[i] - res[0];
		exponent[0].data = _mm256_and_pd(res[0], _M256_ABS_MASK);
		C[exponent[0].binary[0] >> 52] += res[0][0];
		C[exponent[0].binary[1] >> 52] += res[0][1];
		C[exponent[0].binary[2] >> 52] += res[0][2];
		C[exponent[0].binary[3] >> 52] += res[0][3];
		exponent[1].data = _mm256_and_pd(res[1], _M256_ABS_MASK);
		C[exponent[1].binary[0] >> 52] += res[1][0];
		C[exponent[1].binary[1] >> 52] += res[1][1];
		C[exponent[1].binary[2] >> 52] += res[1][2];
		C[exponent[1].binary[3] >> 52] += res[1][3];
	}

#ifdef STATISTICS
	flopSummationStatistics += sizeA * 6;
#endif

}

//=========================================================================================================================================
// Une fonction qui accumule les elements d'un vecrteur selon l'exposent
// Entrées : A : Vecteurs de taille "n"
// Sortie : C : Un vecteur de taille "4096"
//=========================================================================================================================================
void extractOnlineExact(unsigned long  int sizeA, double* A, double* C) {

	union binaryVector exponent;

	unsigned long int index;

	for (unsigned long  int i = 0; i < sizeA; i += 4) {
		double Ctemp, Atemp;
		exponent.data = _mm256_and_pd(*(__m256d*) &A[i], _M256_ABS_MASK);
		index = (exponent.binary[0] >> 52) * 2;
		Ctemp = C[index] + A[i];
		Atemp = Ctemp - C[index];
		A[i] = (C[index] - (Ctemp - Atemp)) + (A[i] - Atemp);
		C[index] = Ctemp;
		C[index + 1] += A[i];
		index = (exponent.binary[1] >> 52) * 2;
		Ctemp = C[index] + A[i + 1];
		Atemp = Ctemp - C[index];
		A[i + 1] = (C[index] - (Ctemp - Atemp)) + (A[i + 1] - Atemp);
		C[index] = Ctemp;
		C[index + 1] += A[i + 1];
		index = (exponent.binary[2] >> 52) * 2;
		Ctemp = C[index] + A[i + 2];
		Atemp = Ctemp - C[index];
		A[i + 2] = (C[index] - (Ctemp - Atemp)) + (A[i + 2] - Atemp);
		C[index] = Ctemp;
		C[index + 1] += A[i + 2];
		index = (exponent.binary[3] >> 52) * 2;
		Ctemp = C[index] + A[i + 3];
		Atemp = Ctemp - C[index];
		A[i + 3] = (C[index] - (Ctemp - Atemp)) + (A[i + 3] - Atemp);
		C[index] = Ctemp;
		C[index + 1] += A[i + 3];
	}

#ifdef STATISTICS
	flopSummationStatistics += sizeA * 7;
#endif

}

//=========================================================================================================================================
// Implémentation de l'algorithme ExtractVector
// Entrée : Un vecteur A et un scaler sigma
// Retourn : La somme de la partie supérieure de vecteur A
//=========================================================================================================================================
double extractVector(double *A, unsigned long int sizeA, double sigma) {
	__m256d SP = _mm256_setzero_pd();
	__m256d m256sigma = _mm256_broadcast_sd(&sigma);
	for (unsigned long int i = 0; i < sizeA; i += 4) {
		register __m256d q;
		q = (m256sigma + *(__m256d*) &A[i]) - m256sigma;
		*(__m256d*) &A[i] = *(__m256d*) &A[i] - q;
		SP = q + SP;
	}
	return ((SP[0] + SP[1]) + (SP[2] + SP[3]));
}

//=========================================================================================================================================
// Implémentation de l'algorithme transform
// Entrée : Un vecteur A
// Retourn : La somme "exacte" des valeurs de vecteur A
//=========================================================================================================================================
double transform(double *A, unsigned long int sizeA, double sigma, double* rest) {

	// calculer M
	register double M = nextPower2(sizeA + 2);

	// Initialisation de la somme
	double S = 0;
	double SP = 0;

	while (fabs(S) < sigma * M * M / eps) { // Itérer jusqu'à ce que la majoration de la somme soit négligeable

		// Itérer sur le vecteur A en utilisant l'algorithme ExtractVector
		SP += extractVector(A, sizeA, sigma);

		// Accumulation des resultats et recupération de l'erreur de calcul
		fastTwoSum(S, SP, &S, &SP);

		// Calculer le nouveau sigma
		sigma *= eps * M;

#ifdef STATISTICS
		flopSummationStatistics += sizeA * 4 + 5;
#endif

	}
	*rest = SP;
	return S;
}

//=========================================================================================================================================
// Une fonction qui renvoie la somme exacte d'un vecteur donné
// Entrées : Un vecteur A 
// Retourne : La somme exacte des valeurs de ce vecteur
//=========================================================================================================================================
double iFastSumCallBack(unsigned long int* size, double* A, unsigned long int Rc) {
	register double S = 0;
	for (unsigned long int i = 0; i < *size; i++) {
		double Stemp = S + A[i];
		double Atemp = Stemp - S;
		A[i] = (S - (Stemp - Atemp)) + (A[i] - Atemp);
		S = Stemp;
	}

#ifdef STATISTICS
	flopSummationStatistics += *size * 6;
#endif

	while (1) {
		unsigned long int count = 0;
		register double St = 0;
		register double Sm = 0;
		for (unsigned long int i = 0; i < *size; i++) {
			double Stemp = St + A[i];
			double Atemp = Stemp - St;
			A[count] = (St - (Stemp - Atemp)) + (A[i] - Atemp);
			St = Stemp;
			if (A[count] != 0) {
				count++;
				double X = fabs(St);
				if (Sm < X) Sm = X;
			}
		}
		double Em = (ulp(Sm) / 2) * (count - 1);
		double Stemp = S + St;
		double Sttemp = Stemp - S;
		A[count] = (S - (Stemp - Sttemp)) + (St - Sttemp);
		S = Stemp;
		St = A[count];
		*size = count;

#ifdef STATISTICS
		flopSummationStatistics += *size * 6 + 13;
#endif

		if (Rc > 0) return S;
		double Wp, Wm, E1, E2;
		Wp = St + Em;
		E1 = Wp - St;
		E1 = (St - (Wp - E1)) + (Em - E1);
		Em = -Em;
		Wp = St + Em;
		E1 = Wp - St;
		E1 = (St - (Wp - E1)) + (Em - E1);

#ifdef STATISTICS
		flopSummationStatistics += *size * 6 + 32;
#endif

		if (((Wp + S != S) || (round3(S, Wp, E1 > 0) != S)) || ((Wm + S != S) || (round3(S, Wm, E2 > 0) != S))) {
			double S1 = iFastSumCallBack(size, A, 1);
			double Stemp = S + S1;
			double S1temp = Stemp - S;
			S1 = (S - (Stemp - S1temp)) + (S1 - S1temp);
			S = Stemp;
			double S2 = iFastSumCallBack(size, A, 1);

#ifdef STATISTICS
			flopSummationStatistics += *size * 16;
#endif

			return round3(S, S1, S2 > 0);
		}
		return S;
	}
}

double iFastSum(unsigned long int size, double* A) {
	return iFastSumCallBack(&size, A, 0);
}

//=========================================================================================================================================
// Implémentation de l'algorithme OnlineExact
// Entrée : Un vecteur A
// Retourn : La somme "exacte" des valeurs de vecteur A
//=========================================================================================================================================
double onlineExact(unsigned long int size, double* A) {
	__m256d C[1024] = {_mm256_setzero_pd()};
	extractOnlineExact(size, A, (double*) &C);

#ifdef DETAILED
	cyclesTransformationSummationStatistics = rdtsc();
	absSumExtractionSummationStatistics = cblas_dasum(4096, (double*) C, 1);
#endif

	return iFastSum(4096, (double*) &C);
}

//=========================================================================================================================================
// Implémentation de l'algorithme HybridSum
// Entrée : Un vecteur A
// Retourne : La somme "exacte" des valeurs de vecteur A
//=========================================================================================================================================
double hybridSum(unsigned long int size, double* A) {
	__m256d C[512] = {_mm256_setzero_pd()};
	extractHybridSum(size, A, (double*) &C);

#ifdef DETAILED
	cyclesTransformationSummationStatistics = rdtsc();
	absSumExtractionSummationStatistics = cblas_dasum(2048, (double*) C, 1);
#endif

	return iFastSum(2048, (double*) &C);
}

//=========================================================================================================================================
// Implémentation de l'algorithme AccSum
// Entrée : Un vecteur A et un scaler sigma
// Retourn : La somme "exacte" des valeurs de vecteur A
//=========================================================================================================================================
double accSumCallBack(unsigned long int size, double* A, double sigma) {
	if (sigma == 0) {
		unsigned long int index = cblas_idamax(size, A, 1);
		sigma = nextPower2(fabs(A[index]) * size);
	}
	register double S;
	double SP;
	S = transform(A, size, sigma, &SP);
	return (S + (SP + optimizedSum(size, A)));
}

double accSum(unsigned long int size, double* A) {
	return accSumCallBack(size, A, 0);
}

//=========================================================================================================================================
// Implémentation de l'algorithme FastAccSum
// Entrée : Un vecteur A et un scaler sigma
// Retourn : La somme "exacte" des valeurs de vecteur A
//=========================================================================================================================================
double fastAccSumCallBack(unsigned long int size, double* A, double T) {

	// Initialisation
	if (T == 0) T = cblas_dasum(size, A, 1) / (1 - (size * eps)); 
	double lastNormal = eta / eps;
	if (T <= lastNormal) return optimizedSum(size, A);
	double fi, Teta, t, tpr = 0;

	// Itérer jusqu'à l'obtention de l'arrondi fidèl
	do {
		double sigma0 = (2 * T) / (1 - ((3 * size) + 1) * eps);
		double sigma = sigma0;
		double sigmapr;

		// L'algorithm extractVectorNew de rhump
		for (unsigned long int i = 0; i < size; i++) {
			sigmapr = A[i] + sigma;
			double tmp = (sigmapr - sigma);
			A[i] = A[i] - tmp;
			sigma = sigmapr;
		}
		Teta = sigmapr - sigma0;

		// Calculer la somme
		t = tpr;
		tpr += Teta;

		// test de zero
		if (tpr == 0) return fastAccSumCallBack(size, A, 0);

		// calculer ufp(sigma0)
		double q = sigma0 / (2 * eps);
		double u = fabs((q / (1 - eps)) - q);

		// calculer fi et T pour le test de fin de boucle
		fi = ((2 * size * (size + 2) * eps) * u) / (1 - (5 * eps));
		T = 2 * size * eps * u;

	} while ((fabs(tpr) < fi) && (4 * T > lastNormal));

	// retourner l'arrondi fidèl
	double Teta2 = (t - tpr) + Teta;
	return tpr + (Teta2 + optimizedSum(size, A));

}

double fastAccSum(unsigned long int size, double* A) {
	return fastAccSumCallBack(size, A, 0);
}


