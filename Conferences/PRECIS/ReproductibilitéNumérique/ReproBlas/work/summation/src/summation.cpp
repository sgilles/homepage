/*
 *************************************************************************************
 Copyright (c) 2015, University of Perpignan
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************
*/

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <stdlib.h>
#include <immintrin.h>
#include <mkl_cblas.h>
#include <summation.h>
#include <fenv.h>
#include <unistd.h>

#ifdef STATS
static unsigned long localSumCycles = 0;

void getSummationStatistics(unsigned long *paramLocalSumCycles) {
	*paramLocalSumCycles = localSumCycles;
}
#endif

#define ONEREDUCTIONK 3
#define CACHELINE 8
#define FPSIZE 8
#define MAXTRANSSIZE 40
#define ONEREDUCTIONW 40
#define MANTISSASIZE 53
#define EXPONENTPOSITIVERANGESIZE 1024
#define HYBRIDSUMSIZE 2048
#define ONLINEEXACTSIZE 4096
#define L1CACHE 32768
#define L2CACHE 262144

#ifdef MPI_PARALLEL
#include <mpi.h>
static int MPI_NUMBER_OF_THREADS;
static int myRank;
static MPI_Op MPI_ADD_IFP;

typedef struct {
	double F[ONEREDUCTIONK];
	int P[ONEREDUCTIONK];
} IFP;

static void ADD_IFP(int *invec, int *inoutvec, int *len, MPI_Datatype *datatype) {
	IFP *inIFP = (IFP*) invec;
	IFP *outIFP = (IFP*) inoutvec;
	addIFP(outIFP->F, outIFP->P, inIFP->F, inIFP->P, outIFP->F, outIFP->P);
}

static MPI_Op MPI_QUAD_SUM;

void ADD_QUAD(double *invec, double *inoutvec, int *len, MPI_Datatype *datatype) {
	for (int i = 0; i < *len; i += 2) {
		inoutvec[1] += invec[1];
		twoSum(inoutvec[0], invec[0], &inoutvec[0], &invec[1]);
		inoutvec[1] += invec[1];
		inoutvec += 2;
		invec += 2;
	}
}
#endif

#ifdef OMP_PARALLEL
#include <omp.h>
static unsigned int OMP_NUMBER_OF_THREADS;
#endif

#ifdef SSE
#define PDSIZE 2
#define PDVECTOR __m128d
#define addvector _mm_add_pd
#define subvector _mm_sub_pd
#define mulvector _mm_mul_pd
#define andvector _mm_and_pd
#define orvector _mm_or_pd
#define maxvector _mm_max_pd
#define broadcast(adress) _mm_broadcastsd_pd(*(__m128d*) adress)
#define ZEROES _mm_setzero_pd()
#elif defined(AVX) || defined(AVX2)
#define PDSIZE 4
#define PDVECTOR __m256d
#define addvector _mm256_add_pd
#define subvector _mm256_sub_pd
#define mulvector _mm256_mul_pd
#define andvector _mm256_and_pd
#define orvector _mm256_or_pd
#define maxvector _mm256_max_pd
#define broadcast(adress) _mm256_broadcast_sd(adress)
#define ZEROES _mm256_setzero_pd()
#elif defined(AVX512)
#define PDSIZE 8
#define PDVECTOR __m512d
#define addvector _mm512_add_pd
#define subvector _mm512_sub_pd
#define mulvector _mm512_mul_pd
#define absvector _mm512_abs_pd
#define maxvector _mm512_max_pd
PDVECTOR _mm512_broadcastsd_pd(__m128d);
#ifdef MIC
#define orvector(A,B) _mm512_or_si512(*(__m512i*) &A, *(__m512i*) &B)
#define broadcast(adress) _mm512_extload_pd (adress, _MM_UPCONV_PD_NONE, _MM_BROADCAST_1X8, 0)
#else
#define orvector _mm512_or_pd
#define broadcast(adress) _mm512_broadcastsd_pd(*(__m128d*) adress)
#endif
#define ZEROES _mm512_setzero_pd()
#endif

#if defined(AVX2)
#define fmaddvector _mm256_fmadd_pd
#define fmsubvector _mm256_fmsub_pd
#elif defined(AVX512)
#define fmaddvector _mm512_fmadd_pd
#define fmsubvector _mm512_fmsub_pd
#endif

#ifndef AVX512
static unsigned long ABS_MASK = 0x7FFFFFFFFFFFFFFF;
#endif

static unsigned long ONE_MASK = 1;
static double SPLIT_FACTOR, eps, eta;
static PDVECTOR SPLIT_FACTOR_VECTOR;
static PDVECTOR ABSMASKVECTOR;
static PDVECTOR ONEMASKVECTOR;

#define reduceVector(vector,result)\
result = 0;\
for (unsigned int cnt = 0; cnt < PDSIZE; cnt++) result += ((double*) &vector)[cnt];

#define reduceMax(vector,result)\
result = 0;\
for (unsigned int cnt = 0; cnt < PDSIZE; cnt++) result = (result < ((double*) &vector)[cnt]) ? ((double*) &vector)[cnt] : result;

#if defined(__i386__)
unsigned long long rdtsc() {
	unsigned long long int x;
	__asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
	return x;
}
#elif defined(__x86_64__)
unsigned long long rdtsc() {
	unsigned hi, lo;
	__asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
	return ((unsigned long long) lo) | (((unsigned long long) hi) << 32);
}
#endif

void summationInitialise() {
	SPLIT_FACTOR = pow(2, 27) + 1;
	eps = pow(2, -53);
	eta = pow(2, -1074);
	SPLIT_FACTOR_VECTOR = broadcast(&SPLIT_FACTOR);
	ONEMASKVECTOR = broadcast((double*) &ONE_MASK);

#ifndef AVX512
	ABSMASKVECTOR = broadcast((double*) &ABS_MASK);
#endif

#ifdef OMP_PARALLEL
	OMP_NUMBER_OF_THREADS = atoi(getenv("OMP_NUM_THREADS"));
#endif

#ifdef MPI_PARALLEL
	MPI_Comm_size(MPI_COMM_WORLD, &MPI_NUMBER_OF_THREADS);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
	MPI_Op_create((MPI_User_function *) ADD_IFP, 1, &MPI_ADD_IFP);
#endif

}

//=========================================================================================================================================
// Une fonction qui fait le split de deux flottants
// Entrée : Un flottant A
// Sorties : high=A[53:27] and low=A[26:0]
//=========================================================================================================================================
void split(double A, double* high, double* low) {
	double temp = A * SPLIT_FACTOR;
	*high = temp - (temp - A);
	*low = A - *high;
}

//=========================================================================================================================================
// Une fonction qui réalise la transformation exacte de la somme de deux scallers
// Entrées : a, b
// Sorties : A=a+b[haut], B=a+b[bas]
//=========================================================================================================================================
void twoSum(double a, double b, double* A, double* B) {
	*A = a + b;
	*B = *A - a;
	*B = (a - (*A - *B)) + (b - *B);
}

//=========================================================================================================================================
// Une fonction qui réalise la transformation exacte de la somme de deux scallers
// Entrées : a, b
// Sorties : A=a+b[haut], B=a+b[bas]
//=========================================================================================================================================
void fastTwoSum(double a, double b, double* A, double* B) {
	*A = a + b;
	a = *A - a;
	*B = b - a;
}

//=========================================================================================================================================
// Une fonction qui renvoie l'ulp d'un flottant
// Entrées : Un flottant x
// Retourne : l'ulp de x
//=========================================================================================================================================
double ulp(double x) {
	x = fabs(x);
	double temp = x * eps;
	temp = (temp + x) - x;
	if (temp == 0) return x * eps * 2;
	else return temp;
}

//=========================================================================================================================================
// Une fonction qui renvoie l'ufp d'un flottant
// Entrées : Un flottant x
// Retourne : l'ufp de x
//=========================================================================================================================================
double ufp(double x) {
	*(unsigned long*) &x = *(unsigned long*) &x & 0xFFC0000000000000;
	return x;
//	double q = x / (2 * eps) + x;
//	return fabs(q - (val * q));
}

//=========================================================================================================================================
// Une fonction qui renvoie la puissance de 2 suivante d'un flottant
// Entrées : Un flottant x
// Retourne : la puissance de 2 suivante de x
//=========================================================================================================================================
double nextPower2(double x) {
	double intermediate = x / eps;
	x = fabs((intermediate - x) - intermediate);
	return x;
}

//=========================================================================================================================================
// Une fonction qui calcule le signe d'un nombre flottant
// Entrée : un nombre flottant S
// retourn le signe de S
//=========================================================================================================================================
int sign(double S) {
	if (S > 0.0) return 1;
	else if (S == 0.0) return 0;
	else return -1;
}

unsigned int isHalfUlp(double S) {
	union binaryDouble bn;
	bn.data = S;
	if ((bn.binary & 0x000FFFFFFFFFFFFF) == 0) return 1;
	return 0;
}

inline double magnify(double S) {
	union binaryDouble bn;
	bn.data = S;
	bn.binary = (bn.binary) | 1;
	return bn.data;
}

//=========================================================================================================================================
// Une fonction qui calcule l'arrondi de la somme de 3 nombres
// Entrée : deux flottants S0 et S1 et un boolean s
// retourn la somme
//=========================================================================================================================================
double round3(double S0, double S1, double S2) {
	if (((sign(S1) == sign(S2))) && (isHalfUlp(S1)))
		return magnify(S1) + S0;
	return S0;
}

void addIFP(double *S1, int *C1, double *S2, int *C2, double *S, int *C) {
	if (S1[0] < S2[0]) addIFP(S2, C2, S1, C1, S, C);
	else if (S1[0] == S1[0] && S2[0] == S2[0]) {
		while (ufp(S2[0]) < ufp(S1[0])) {
			for (unsigned int k = 2; k > 0; k--) {
				S2[k] = S2[k - 1];
				C2[k] = C2[k - 1];
			}
			S2[0] = 1.5 * pow(2, ONEREDUCTIONW) * ufp(S2[0]);
			C2[0] = 0;
		}
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			S[k] = S1[k] + (S2[k] - 1.5 * ufp(S2[k]));
			C[k] = C1[k] + C2[k];
			double ufpk = ufp(S[k]);
			if (S[k] >= 1.75 * ufpk) {
				S[k] -= 0.25 * ufpk;
				C[k]++;
			}
			else if (S[k] < 1.25 * ufpk) {
				S[k] += 0.5 * ufpk;
				C[k] -= 2;
			}
			else if (S[k] <= 1.5 * ufpk) {
				S[k] += 0.25 * ufpk;
				C[k]--;
			}
		}
	}
	else {
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) S[k] = NAN;
	}
}

void eliminateZeros(unsigned long* size, double* C) {
	unsigned int interSize = 0;
	for (unsigned int i = 0; i < *size; i++) if (C[i]) {
		C[interSize] = C[i];
		interSize++;
	}
	*size = interSize;
}

//=========================================================================================================================================
// Une fonction qui renvoie la somme optimisée d'un vecteur donné A
// Entrées : Un vecteur A
// Retourne : La somme des elements de A
//=========================================================================================================================================
#ifdef SSE
#define accumulate(vector)\
vector = addvector(vector, *(PDVECTOR*) &A[0]);\
vector = addvector(vector, *(PDVECTOR*) &A[2]);\
vector = addvector(vector, *(PDVECTOR*) &A[4]);\
vector = addvector(vector, *(PDVECTOR*) &A[6]);
#elif defined(AVX) || defined(AVX2)
#define accumulate(vector);\
vector = addvector(vector, *(PDVECTOR*) &A[0]);\
vector = addvector(vector, *(PDVECTOR*) &A[4]);
#elif defined(AVX512)
#define accumulate(vector);\
vector = addvector(vector, *(PDVECTOR*) &A[0]);
#endif
double sequentialOptimizedSum(unsigned long size, double *A) {
	PDVECTOR SP = ZEROES;
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&A[40], 0, _MM_HINT_T0);
		accumulate(SP)
		A += CACHELINE;
	}
	double reduceVector(SP,result)
	for (; i < size; i++) {
		result += A[0];
		A++;
	}
	return result;
}

//=========================================================================================================================================
// Une fonction qui renvoie le max de valeur absolue d'un vecteur donné A
// Entrées : Un vecteur A
// Retourne : Le max des valeurs absolues de A
//=========================================================================================================================================
#ifdef SSE
#define setmax(vector)\
vector = maxvector(vector, andvector(ABSMASKVECTOR, *(PDVECTOR*) &A[0]));\
vector = maxvector(vector, andvector(ABSMASKVECTOR, *(PDVECTOR*) &A[2]));\
vector = maxvector(vector, andvector(ABSMASKVECTOR, *(PDVECTOR*) &A[4]));\
vector = maxvector(vector, andvector(ABSMASKVECTOR, *(PDVECTOR*) &A[6]));
#elif defined(AVX) || defined(AVX2)
#define setmax(vector);\
vector = maxvector(vector, andvector(ABSMASKVECTOR, *(PDVECTOR*) &A[0]));\
vector = maxvector(vector, andvector(ABSMASKVECTOR, *(PDVECTOR*) &A[4]));
#elif defined(AVX512)
#define setmax(vector);\
vector = maxvector(vector, absvector(*(PDVECTOR*) &A[0]));
#endif
double sequentialAbsMax(unsigned long size, double *A) {
	PDVECTOR SP = ZEROES;
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&A[40], 0, _MM_HINT_T0);
		setmax(SP)
		A += CACHELINE;
	}
	double reduceMax(SP,maxAbs)
	for (; i < size; i++) {
		maxAbs = (maxAbs < fabs(A[0])) ? fabs(A[0]) : maxAbs;
		A++;
	}
	return maxAbs;
}

//=========================================================================================================================================
// Une fonction qui renvoie la somme des valeurs absolues d'un vecteur donné A
// Entrées : Un vecteur A
// Retourne : La somme des valeurs absolues de A
//=========================================================================================================================================
double sequentialAbsSum(unsigned long size, double *A) {
	return cblas_dasum(size, A, 1);
}

#ifdef AVX512
#define hybridSumVectorExp(unroll,unroll8)\
*(PDVECTOR*) exponent = absvector(*(PDVECTOR*) &res[unroll]);\
*(__m512i*) &index[unroll] = _mm512_srli_epi64(*(__m512i*) exponent, 52);\
*(PDVECTOR*) exponent = absvector(*(PDVECTOR*) &res[unroll8]);\
*(__m512i*) &index[unroll8] = _mm512_srli_epi64(*(__m512i*) exponent, 52);
#endif

#define hybridSumExp(unroll,unroll8)\
exponent[unroll].data = fabs(res[unroll]);\
index[unroll] = exponent[unroll].binary >> 52;\
index[unroll8] = index[unroll] - 27;\
index[unroll8] = (index[unroll8] > 0) ? index[unroll8] : 0;

#define hybridSumAcc(unroll,unroll8)\
C[index[unroll]] += res[unroll];\
C[index[unroll8]] += res[unroll8];

#ifdef AVX512
#define hybridSumVectorSplit(unroll, unroll8)\
*(PDVECTOR*) &temp[unroll] = mulvector(*(PDVECTOR*) &A[unroll], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &res[unroll] = subvector(*(PDVECTOR*) &temp[unroll], subvector(*(PDVECTOR*) &temp[unroll], *(PDVECTOR*) &A[unroll]));\
*(PDVECTOR*) &res[unroll8] = subvector(*(PDVECTOR*) &A[unroll], *(PDVECTOR*) &res[unroll]);
#endif

#define hybridSumSplit(unroll, unroll8)\
temp[unroll] = A[unroll] * SPLIT_FACTOR;\
res[unroll] = temp[unroll] - (temp[unroll] - A[unroll]);\
res[unroll8] = A[unroll] - res[unroll];
//=========================================================================================================================================
// Une fonction qui accumule les elements d'un vecteur selon l'exposent
// Entrées : A : Vecteurs de taille "n"
// Sortie : C : Un vecteur de taille "2048"
//=========================================================================================================================================
void sequentialExtractHybridSum(unsigned long size, double* A, double* C) {
	double __attribute__ ((aligned(64))) res[CACHELINE * 2], __attribute__ ((aligned(64))) temp[CACHELINE];
	union binaryDouble __attribute__ ((aligned(64))) exponent[CACHELINE];
	unsigned long long index[CACHELINE * 2];
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		// PREFETCH DATA FOR NEXT LOOP
		__builtin_prefetch(&A[40], 0, _MM_HINT_NTA);
		// SPLIT
#ifdef AVX512
		hybridSumVectorSplit(0,8)
#else
		hybridSumSplit(0,8)
		hybridSumSplit(1,9)
		hybridSumSplit(2,10)
		hybridSumSplit(3,11)
		hybridSumSplit(4,12)
		hybridSumSplit(5,13)
		hybridSumSplit(6,14)
		hybridSumSplit(7,15)
#endif
		// EXPONENT AND ACCUMULATION
#ifdef AVX512
		hybridSumVectorExp(0,8)
#else
		hybridSumExp(0,8)
		hybridSumExp(1,9)
		hybridSumExp(2,10)
		hybridSumExp(3,11)
		hybridSumExp(4,12)
		hybridSumExp(5,13)
		hybridSumExp(6,14)
		hybridSumExp(7,15)
#endif
		hybridSumAcc(0,8)
		hybridSumAcc(1,9)
		hybridSumAcc(2,10)
		hybridSumAcc(3,11)
		hybridSumAcc(4,12)
		hybridSumAcc(5,13)
		hybridSumAcc(6,14)
		hybridSumAcc(7,15)
		A += CACHELINE;
	}
	for (; i < size; i++) {
		hybridSumSplit(0,1)
		hybridSumExp(0,1)
		hybridSumAcc(0,1)
		A++;
	}
}

#define onlineExactTwoSum(unroll)\
Ctemp[unroll] = C[index[unroll]] + A[unroll];\
Atemp[unroll] = Ctemp[unroll] - C[index[unroll]];\
C[index[unroll]] = Ctemp[unroll];\
C[index[unroll] + 1] += (A[unroll] - Atemp[unroll]);

#ifdef AVX512
#define onlineExactExponentVector(unroll)\
*(PDVECTOR*) exponent = absvector(*(PDVECTOR*) &A[unroll]);\
*(__m512i*) index = _mm512_slli_epi64(_mm512_srli_epi64(*(__m512i*) exponent, 52), 1);
#endif

#define onlineExactExponent(unroll)\
exponent[unroll].data = fabs(A[unroll]);\
index[unroll] = (exponent[unroll].binary >> 52) * 2;

//=========================================================================================================================================
// Une fonction qui accumule les elements d'un vecrteur selon l'exposent
// Entrées : A : Vecteurs de taille "n"
// Sortie : C : Un vecteur de taille "4096"
//=========================================================================================================================================
void sequentialExtractOnlineExact(unsigned long size, double* A, double* C) {
	union binaryDouble __attribute__ ((aligned(64))) exponent[CACHELINE];
	double Atemp[CACHELINE], Ctemp[CACHELINE];
	unsigned long __attribute__ ((aligned(64))) index[CACHELINE];
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		// Prefetching pour l'itération suivante
		__builtin_prefetch(&A[40], 1, _MM_HINT_T0);
		// Extraction d'exposant
#ifdef AVX512
		onlineExactExponentVector(0)
#else
		onlineExactExponent(0)
		onlineExactExponent(1)
		onlineExactExponent(2)
		onlineExactExponent(3)
		onlineExactExponent(4)
		onlineExactExponent(5)
		onlineExactExponent(6)
		onlineExactExponent(7)
#endif
		// FastTwoSum
		onlineExactTwoSum(0)
		onlineExactTwoSum(1)
		onlineExactTwoSum(2)
		onlineExactTwoSum(3)
		onlineExactTwoSum(4)
		onlineExactTwoSum(5)
		onlineExactTwoSum(6)
		onlineExactTwoSum(7)
		A += CACHELINE;
	}
	for (; i < size; i++) {
		onlineExactExponent(0)
		onlineExactTwoSum(0)
		A++;
	}
}

//=========================================================================================================================================
// Implémentation de l'algorithme AccSum
// Entrée : Un vecteur A et un scaler sigma
// Retourn : La somme "exacte" des valeurs de vecteur A
//=========================================================================================================================================
#define extractscallers(q,sigma,A,S)\
q = subvector(addvector(sigma,*(PDVECTOR*) A), sigma);\
S = addvector(q,S);\
*(PDVECTOR*) A = subvector(*(PDVECTOR*) A, q);
double sequentialFirstExtractVector(unsigned long long size, double *A, double sigma, double M, double *SP) {
	PDVECTOR VS = ZEROES;
	PDVECTOR VSP = ZEROES;
	PDVECTOR sigmaSVector = broadcast(&sigma);
	double sigmaS = sigma;
	sigma *= M * eps;
	PDVECTOR sigmaSPVector = broadcast(&sigma);
	PDVECTOR qS = sigmaSVector;
	PDVECTOR qSP = sigmaSPVector;
	unsigned long i;
	for (i = 0; i + PDSIZE - 1 < size; i += PDSIZE) {
		extractscallers(qS,sigmaSVector,A,VS)
		extractscallers(qSP,sigmaSPVector,A,VSP)
		A += PDSIZE;
	}
	reduceVector(VSP,*SP)
	double reduceVector(VS,S)
	double dq;
	for (; i < size; i++) {
		dq = (sigmaS + A[0]) - sigmaS;
		S += dq;
		A[0] -= dq;
		dq = (sigma + A[0]) - sigma;
		*SP += dq;
		A[0] -= dq;
		A++;
	}
	return S;
}

double sequentialExtractVector(unsigned long long size, double *A, double sigma) {
	PDVECTOR VSP = ZEROES;
	PDVECTOR sigmavector = broadcast(&sigma);
	PDVECTOR q;
	unsigned long i;
	for (i = 0; i + PDSIZE - 1 < size; i += PDSIZE) {
		extractscallers(q,sigmavector,A,VSP)
		A += PDSIZE;
	}
	double dq;
	double reduceVector(VSP,S)
	for (; i < size; i++) {
		dq = (sigma + A[0]) - sigma;
		S += dq;
		A[0] -= dq;
		A++;
	}
	return S;
}

double sequentialAccSumCallBack(unsigned long size, double* A, double sigma) {
	if (sigma == 0)	sigma = nextPower2(sequentialAbsSum(size, A));
	if (sigma == 0) return 0;
	double M = nextPower2(size + 2);
	double S = 0;
	double SP = 0;
	S = sequentialFirstExtractVector(size, A, sigma, M, &SP);
	fastTwoSum(S, SP, &S, &SP);
	sigma *= eps * eps * M * M;
	while (fabs(S) < M * sigma) {
		SP += sequentialExtractVector(size, A, sigma);
		fastTwoSum(S, SP, &S, &SP);
		sigma *= eps * M;
	}
	return S + (SP + sequentialOptimizedSum(size, A));
}

//=========================================================================================================================================
// Implémentation de l'algorithme FastAccSum
// Entrée : Un vecteur A et un scaler sigma
// Retourn : La somme "exacte" des valeurs de vecteur A
//=========================================================================================================================================
#define extractscallersnew(sigmapr,A,sigma)\
sigmapr = addvector(*(PDVECTOR*) A, sigma);\
*(PDVECTOR*) A = subvector(*(PDVECTOR*) A,subvector(sigmapr, sigma));\
sigma = sigmapr;
double sequentialFirstExtractVectorNew(unsigned long long size, unsigned long long gsize, double *A, double sigma, double *SP, double *TCont) {
	double u, Tmin, fi, T;
	u = ufp(sigma);
	fi = ((2 * gsize * (gsize + 2) * eps) * u) / (1 - (5 * eps));
	T = 2 * gsize * eps * u;
	Tmin = (((3 / 2) + (4 * eps)) * (gsize * eps)) * sigma;
	if (T > Tmin) T = Tmin;
	double sigmaSP = (2 * T) / (1 - ((3 * gsize) + 1) * eps);
	*TCont = sigmaSP;
	PDVECTOR sigmaSVector = broadcast(&sigma);
	PDVECTOR sigmaprSVector = sigmaSVector;
	PDVECTOR sigmaSPVector = broadcast(&sigmaSP);
	PDVECTOR sigmaprSPVector = sigmaSPVector;
	unsigned long i;
	for (i = 0; i + PDSIZE - 1 < size; i += PDSIZE) {
		extractscallersnew(sigmaprSVector,A,sigmaSVector)
		extractscallersnew(sigmaprSPVector,A,sigmaSPVector)
		A += PDSIZE;
	}
	for (; i < size; i++) {
		((double*) &sigmaprSVector)[0] = A[0] + ((double*) &sigmaSVector)[0];
		A[0] = A[0] - (((double*) &sigmaprSVector)[0] - ((double*) &sigmaSVector)[0]);
		((double*) &sigmaSVector)[0] = ((double*) &sigmaprSVector)[0];
		((double*) &sigmaprSPVector)[0] = A[0] + ((double*) &sigmaSPVector)[0];
		A[0] = A[0] - (((double*) &sigmaprSPVector)[0] - ((double*) &sigmaSPVector)[0]);
		((double*) &sigmaSPVector)[0] = ((double*) &sigmaprSPVector)[0];
		A++;
	}
	sigmaSPVector = broadcast(&sigmaSP);
	sigmaprSPVector = subvector(sigmaprSPVector, sigmaSPVector);
	sigmaSVector = broadcast(&sigma);
	sigmaprSVector = subvector(sigmaprSVector, sigmaSVector);
	reduceVector(sigmaprSPVector,*SP)
	double reduceVector(sigmaprSVector,S)
	return S;
}

double sequentialExtractVectorNew(unsigned long long size, double *A, double sigma) {
	PDVECTOR sigmavector = broadcast(&sigma);
	PDVECTOR sigmaprvector;
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += PDSIZE) {
		extractscallersnew(sigmaprvector,A,sigmavector)
		A += PDSIZE;
	}
	for (; i < size; i++) {
		((double*) &sigmaprvector)[0] = A[0] + ((double*) &sigmavector)[0];
		A[0] = A[0] - (((double*) &sigmaprvector)[0] - ((double*) &sigmavector)[0]);
		((double*) &sigmavector)[0] = ((double*) &sigmaprvector)[0];
		A++;
	}
	sigmavector = broadcast(&sigma);
	sigmaprvector = subvector(sigmaprvector, sigmavector);
	double reduceVector(sigmaprvector,S)
	return S;
}

double sequentialFastAccSumCallBack(unsigned long size, double* A, double T) {
	if (T == 0) {
		T = sequentialAbsSum(size, A) / (1 - (size * eps));
	}
	double lastNormal = eta / eps;
	if (T <= lastNormal) {
		return sequentialOptimizedSum(size, A);
	}
	double Teta, sigma, tpr, t, q, u, Tmin, fi;
	sigma = (2 * T) / (1 - ((3 * size) + 1) * eps);
	tpr = sequentialFirstExtractVectorNew(size, size, A, sigma, &Teta, &sigma);
	t = tpr;
	tpr += Teta;
	u = ufp(sigma);
	fi = ((2 * size * (size + 2) * eps) * u) / (1 - (5 * eps));
	T = 2 * size * eps * u;
	Tmin = (((3 / 2) + (4 * eps)) * (size * eps)) * sigma;
	if (T > Tmin) T = Tmin;
	while ((fabs(tpr) < fi) && (4 * T > lastNormal)) {
		sigma = (2 * T) / (1 - ((3 * size) + 1) * eps);
		Teta = sequentialExtractVectorNew(size, A, sigma);
		t = tpr;
		tpr += Teta;
		if (tpr == 0) return sequentialFastAccSumCallBack(size, A, 0);
		u = ufp(sigma);
		fi = ((2 * size * (size + 2) * eps) * u) / (1 - (5 * eps));
		T = 2 * size * eps * u;
		Tmin = (((3 / 2) + (4 * eps)) * (size * eps)) * sigma;
		if (T > Tmin) T = Tmin;
	}
	double Teta2 = (t - tpr) + Teta;
	return tpr + (Teta2 + sequentialOptimizedSum(size, A));
}

//=========================================================================================================================================
// Une fonction qui renvoie la somme exacte d'un vecteur donné
// Entrées : Un vecteur A
// Retourne : La somme exacte des valeurs de ce vecteur
//=========================================================================================================================================
#define ifasttwosum(S,index,St,At)\
St = addvector(S, *(PDVECTOR*) &A[index]);\
At = subvector(St, S);\
*(PDVECTOR*) &A[index] = addvector(subvector(S, subvector(St, At)), subvector(*(PDVECTOR*) &A[index], At));\
S = St;

#define keepnonzeroerrors(index)\
for (unsigned int cnt = index; cnt < index + PDSIZE; cnt++)\
	if (A[cnt] != 0) {\
		A[count] = A[cnt];\
		count++;\
		double X = fabs(((double*) &VSt)[0]);\
		if (Sm < X) Sm = X;\
	}

double firstDistilation(unsigned long *size, double* A, double *Em, double *St) {
	*St = 0;
	double S = 0;
	unsigned long i = 0;
	unsigned long count = 0;
	double Sm = 0;
	PDVECTOR VStemp, VAtemp, VSttemp, VAttemp, VS = ZEROES;
	PDVECTOR VSt = ZEROES;
	if (*size > PDSIZE) {
		for (i = 0; i + PDSIZE - 1 < *size; i += PDSIZE) {
			ifasttwosum(VS,i,VStemp,VAtemp)
			ifasttwosum(VSt,i,VSttemp,VAttemp)
			keepnonzeroerrors(i)
		}
		for (unsigned int j = 0; j < PDSIZE; j++) {
			twoSum(S, ((double*) &VS)[j], &S, &A[count]);
			twoSum(*St, A[count], St, &A[count]);
			if (A[count] != 0) {
				count++;
				double X = fabs(*St);
				if (Sm < X) Sm = X;
			}
			twoSum(*St, ((double*) &VSt)[j], St, &A[count]);
			if (A[count] != 0) {
				count++;
				double X = fabs(*St);
				if (Sm < X) Sm = X;
			}
		}
	}
	for (; i < *size; i++) {
		twoSum(S, A[i], &S, &A[count]);
		twoSum(*St, A[count], St, &A[count]);
		if (A[count] != 0) {
			count++;
			double X = fabs(*St);
			if (Sm < X) Sm = X;
		}
	}
	*Em = (ulp(Sm) / 2) * count;
	twoSum(S, *St, &S, St);
	A[count] = *St;
	count++;
	*size = count;
	return S;
}

double distilation(unsigned long *size, double* A, double *Em) {
	double St = 0;
	unsigned long i = 0;
	unsigned long count = 0;
	double Sm = 0;
	PDVECTOR VStemp, VAtemp;
	PDVECTOR VSt = ZEROES;
	if (*size > PDSIZE) {
		for (i = 0; i + PDSIZE - 1 < *size; i += PDSIZE) {
			ifasttwosum(VSt,i,VStemp,VAtemp)
			keepnonzeroerrors(i)
		}
		for (unsigned int j = 0; j < PDSIZE; j++) {
			twoSum(St, ((double*) &VSt)[j], &St, &A[count]);
			if (A[count] != 0) {
				count++;
				double X = fabs(St);
				if (Sm < X) Sm = X;
			}
		}
	}
	for (;i < *size; i++) {
		twoSum(St, A[i], &St, &A[count]);
		if (A[count] != 0) {
			count++;
			double X = fabs(St);
			if (Sm < X) Sm = X;
		}
	}
	*Em = (ulp(Sm) / 2) * count;
	*size = count;
	return St;
}

double iFastSumCallBack(unsigned long *size, double* A, int Rc) {
	double Em, S, St;
	S = firstDistilation(size, A, &Em, &St);
	if (Em == 0 || Em < ulp(S)) {
		if (Rc > 0) return S;
		double W1, W2, E1, E2;
		twoSum(St, Em, &W1, &E1);
		twoSum(St, -Em, &W2, &E2);
		if (((W1 + S != S) || (round3(S, W1, E1) != S)) && ((W2 + S != S) || (round3(S, W2, E2) != S))) {
			double S1 = iFastSumCallBack(size, A, 1);
			twoSum(S, S1, &S, &S1);
			double S2 = iFastSumCallBack(size, A, 1);
			return round3(S, S1, S2);
		}
		return S;
	}
	while (1) {
		St = distilation(size, A, &Em);
		twoSum(S, St, &S, &St);
		A[*size] = St;
		(*size)++;
		if (Em == 0 || Em < ulp(S)) {
			if (Rc > 0) return S;
			double W1, W2, E1, E2;
			twoSum(St, Em, &W1, &E1);
			twoSum(St, -Em, &W2, &E2);
			if (((W1 + S != S) || (round3(S, W1, E1) != S)) && ((W2 + S != S) || (round3(S, W2, E2) != S))) {
				double S1 = iFastSumCallBack(size, A, 1);
				twoSum(S, S1, &S, &S1);
				double S2 = iFastSumCallBack(size, A, 1);
				return round3(S, S1, S2);
			}
			return S;
		}
	}
}

//=========================================================================================================================================
// Implémentation de l'algorithme reproducible sum de diep et demmel
// Entrée : Un vecteur A et un scaler sigma
// Retourn : La somme "reproductible" des valeurs de vecteur A
//=========================================================================================================================================
#define reprodextractscallers(q,sigma,A,S,sigmat,St)\
extractscallers(q,sigma,A,S)\
q = subvector(addvector(sigmat,*(PDVECTOR*) A), sigmat);\
St = addvector(q,St);
void sequentialReprodSumCallBack(unsigned long size, unsigned long gsize, double* A, double sigma, double *fFold, double *sFold) {
	PDVECTOR q;
	PDVECTOR St = ZEROES;
	PDVECTOR Sts = ZEROES;
	double M = nextPower2(sigma);
	PDVECTOR MVector = broadcast(&M);
	sigma = (gsize * 2 * eps * M) / (1 - (2 * gsize * eps));
	double MF = M;
	M = nextPower2(sigma);
	PDVECTOR MVectors = broadcast(&M);
	double a[PDSIZE] __attribute__ ((aligned(64))) = {0};
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&A[CACHELINE], 0, _MM_HINT_T0);
		*(PDVECTOR*) a = *(PDVECTOR*) A;
		reprodextractscallers(q,MVector,a,St,MVectors,Sts)
		A += PDSIZE;
#if defined(AVX) || defined(AVX2) || defined(SSE)
		*(PDVECTOR*) a = *(PDVECTOR*) A;
		reprodextractscallers(q,MVector,a,St,MVectors,Sts)
		A += PDSIZE;
#endif
#if defined(SSE)
		*(PDVECTOR*) a = *(PDVECTOR*) A;
		reprodextractscallers(q,MVector,a,St,MVectors,Sts)
		A += PDSIZE;
		*(PDVECTOR*) a = *(PDVECTOR*) A;
		reprodextractscallers(q,MVector,a,St,MVectors,Sts)
		A += PDSIZE;
#endif
	}
	reduceVector(St,*fFold)
	reduceVector(Sts,*sFold)
	double dq, da;
	for (; i < size; i++) {
		da = A[0];
		dq = (MF + da) - MF;
		*fFold += dq;
		da -= dq;
		dq = (M + da) - M;
		*sFold += dq;
		A++;
	}
}

double sequentialReprodSumCallBack(unsigned long size, double* A, double sigma) {
	if (sigma == 0) {
		double max = sequentialAbsMax(size, A);
		sigma = (max * size) / (1 - (2 * size * eps));
	}
	double fFold, sFold;
	sequentialReprodSumCallBack(size, size, A, sigma, &fFold, &sFold);
	return fFold + sFold;
}

//=========================================================================================================================================
// Implémentation de l'algorithme fast reproducible sum de diep et demmel
// Entrée : Un vecteur A et un scaler sigma
// Retourn : La somme "reproductible" des valeurs de vecteur A
//=========================================================================================================================================
#define reprodextractscallersnew(sigma,A,sigmapr,sigmas)\
extractscallersnew(sigma,A,sigmapr)\
sigmas = addvector(sigmas,*(PDVECTOR*) A);
void sequentialFastReprodSumCallBack(unsigned long size, unsigned long gsize, double* A, double sigma, double *fFold, double *sFold) {
	int originalRoundingMode = fegetround();
	fesetround(FE_TOWARDZERO);
	PDVECTOR St, Sts;
	double M = 3 * nextPower2(sigma);
	PDVECTOR Minit = broadcast(&M);
	PDVECTOR Mprev = Minit;
	PDVECTOR MVector = Minit;
	sigma = (gsize * 4 * eps * (M / 3)) / (1 - (4 * (gsize + 1) * eps));
	M = 3 * nextPower2(sigma);
	PDVECTOR Minits = broadcast(&M);
	PDVECTOR MVectors = Minits;
	double a[PDSIZE] __attribute__ ((aligned(64))) = {0};
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&A[CACHELINE], 0, _MM_HINT_T0);
		*(PDVECTOR*) a = *(PDVECTOR*) A;
		reprodextractscallersnew(MVector,a,Mprev,MVectors)
		A += PDSIZE;
#if defined(AVX) || defined(AVX2) || defined(SSE)
		*(PDVECTOR*) a = *(PDVECTOR*) A;
		reprodextractscallersnew(MVector,a,Mprev,MVectors)
		A += PDSIZE;
#endif
#if defined(SSE)
		*(PDVECTOR*) a = *(PDVECTOR*) A;
		reprodextractscallersnew(MVector,a,Mprev,MVectors)
		A += PDSIZE;
		*(PDVECTOR*) a = *(PDVECTOR*) A;
		reprodextractscallersnew(MVector,a,Mprev,MVectors)
		A += PDSIZE;
#endif
	}
	double da;
	for (; i < size; i++) {
		da = A[0];
		((double*) &Mprev)[0] = da + ((double*) &MVector)[0];
		da = da - (((double*) &Mprev)[0] - ((double*) &MVector)[0]);
		((double*) &MVector)[0] = ((double*) &Mprev)[0];
		((double*) &MVectors)[0] = da + ((double*) &MVectors)[0];
		A++;
	}
	St = subvector(MVector, Minit);
	Sts = subvector(MVectors, Minits);
	reduceVector(St,*fFold)
	reduceVector(Sts,*sFold)
	fesetround(originalRoundingMode);
}

double sequentialFastReprodSumCallBack(unsigned long size, double* A, double sigma) {
	if (sigma == 0) {
		double max = sequentialAbsMax(size, A);
		sigma = (max * size) / (1 - (4 * (size + 1) * eps));
	}
	double fFold, sFold;
	sequentialFastReprodSumCallBack(size, size, A, sigma, &fFold, &sFold);
	return fFold + sFold;
}

//=========================================================================================================================================
// Trasnsforamtion d'un vecteur A
// Entrée : Un vecteur A
// Retourn : Le vecteur A reduit
//=========================================================================================================================================
void sequentialTransSmallVector(unsigned long *size, double* A) {
	unsigned int i = 0;
	double B[40];
	while (*size > 1) {
		B[i] = iFastSumCallBack(size, A, 0);
		i++;
	}
	if (*size == 1) {
		B[i] = A[0];
		i++;
	}
	memcpy(A, B, i * sizeof(double));
	*size = i;
}

void sequentialTransBigVector(unsigned long *size, double* A) {
	double C[ONLINEEXACTSIZE] __attribute__ ((aligned(64))) = {0};
	sequentialExtractOnlineExact(*size, A, C);
	*size = ONLINEEXACTSIZE;
	eliminateZeros(size, C);
	sequentialTransSmallVector(size, C);
	memcpy(A, C, *size * sizeof(double));
}

// Transformer un vecteur a des flottant qui ne se chevauchent pas
void sequentialTransVector(unsigned long *size, double* A) {
	if (*size * FPSIZE <= L1CACHE) sequentialTransSmallVector(size, A);
	else sequentialTransBigVector(size, A);
}

double sequentialDynamicSum(unsigned long size, double *A) {
	if (size * FPSIZE < L2CACHE) return fastAccSum(size, A);
	else return onlineExact(size, A);
}

#ifdef MIC
#define extractscallersonereduction(t,A,a,M)\
a = *(PDVECTOR*) A;\
for (unsigned int k = 0; k < ONEREDUCTIONK - 1; k++) {\
*(__m512i*) &t = orvector(ONEMASKVECTOR, a);\
t = addvector(t, M[k]);\
a = subvector(a, subvector(t, M[k]));\
M[k] = t;\
}\
*(__m512i*) &t = orvector(ONEMASKVECTOR, a);\
M[ONEREDUCTIONK - 1] = addvector(t, M[ONEREDUCTIONK - 1]);\
A += PDSIZE;
#else
#define extractscallersonereduction(t,A,a,M)\
a = *(PDVECTOR*) A;\
for (unsigned int k = 0; k < ONEREDUCTIONK - 1; k++) {\
t = orvector(ONEMASKVECTOR, a);\
t = addvector(t, M[k]);\
a = subvector(a, subvector(t, M[k]));\
M[k] = t;\
}\
t = orvector(ONEMASKVECTOR, a);\
M[ONEREDUCTIONK - 1] = addvector(t, M[ONEREDUCTIONK - 1]);\
A += PDSIZE;
#endif

void extractVectorOneReduction(unsigned long size, double* A, double* M) {
	double a;
	double T;
	PDVECTOR pda;
	PDVECTOR pdT;
	double ufpM[ONEREDUCTIONK];
	PDVECTOR pdM[ONEREDUCTIONK];
	for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
		ufpM[k] = 1.5 * ufp(M[k]);
		pdM[k] = broadcast(&ufpM[k]);
	}
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&A[CACHELINE], 0, _MM_HINT_T0);
		extractscallersonereduction(pdT, A, pda, pdM)
#if defined(AVX) || defined(AVX2) || defined(SSE)
		extractscallersonereduction(pdT, A, pda, pdM)
#endif
#if defined(SSE)
		extractscallersonereduction(pdT, A, pda, pdM)
		extractscallersonereduction(pdT, A, pda, pdM)
#endif
	}
	for(; i < size; i++) {
		a = A[0];
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			*(unsigned long*) &T = *(unsigned long*) &a | 1;
			T += M[k];
			a -= (T - M[k]);
			M[k] = T;
		}
		A++;
	}
	for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
		for (unsigned int i = 0; i < PDSIZE; i++) {
			M[k] += ((double*) &pdM[k])[i] - ufpM[k];
		}
	}
}

#ifdef MIC
#define extractscallersonereductionlight(t,A,a,M)\
a = *(PDVECTOR*) A;\
for (unsigned int k = 0; k < ONEREDUCTIONK - 2; k++) {\
*(__m512i*) &t = orvector(ONEMASKVECTOR, a);\
t = addvector(t, M[k]);\
a = subvector(a, subvector(t, M[k]));\
M[k] = t;\
}\
*(__m512i*) &t = orvector(ONEMASKVECTOR, a);\
M[ONEREDUCTIONK - 2] = addvector(t, M[ONEREDUCTIONK - 2]);\
A += PDSIZE;
#else
#define extractscallersonereductionlight(t,A,a,M)\
a = *(PDVECTOR*) A;\
for (unsigned int k = 0; k < ONEREDUCTIONK - 2; k++) {\
t = orvector(ONEMASKVECTOR, a);\
t = addvector(t, M[k]);\
a = subvector(a, subvector(t, M[k]));\
M[k] = t;\
}\
t = orvector(ONEMASKVECTOR, a);\
M[ONEREDUCTIONK - 2] = addvector(t, M[ONEREDUCTIONK - 2]);\
A += PDSIZE;
#endif

void extractVectorOneReductionLight(unsigned long size, double* A, double* M) {
	double a;
	double T;
	PDVECTOR pda;
	PDVECTOR pdT;
	double ufpM[ONEREDUCTIONK - 1];
	PDVECTOR pdM[ONEREDUCTIONK - 1];
	for (unsigned int k = 0; k < ONEREDUCTIONK - 1; k++) {
		ufpM[k] = 1.5 * ufp(M[k]);
		pdM[k] = broadcast(&ufpM[k]);
	}
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&A[CACHELINE], 0, _MM_HINT_T0);
		extractscallersonereductionlight(pdT, A, pda, pdM)
#if defined(AVX) || defined(AVX2) || defined(SSE)
		extractscallersonereductionlight(pdT, A, pda, pdM)
#endif
#if defined(SSE)
		extractscallersonereductionlight(pdT, A, pda, pdM)
		extractscallersonereductionlight(pdT, A, pda, pdM)
#endif
	}
	for(; i < size; i++) {
		a = A[0];
		for (unsigned int k = 0; k < ONEREDUCTIONK - 1; k++) {
			*(unsigned long*) &T = *(unsigned long*) &a | 1;
			T += M[k];
			a -= (T - M[k]);
			M[k] = T;
		}
		A++;
	}
	for (unsigned int k = 0; k < ONEREDUCTIONK - 1; k++) {
		for (unsigned int i = 0; i < PDSIZE; i++) {
			M[k] += (((double*) &pdM[k])[i] - ufpM[k]);
		}
	}
}

void extractVectorOneReductionIR(unsigned long size, double* A, double* M) {
	double a;
	double T;
	PDVECTOR pda;
	PDVECTOR pdT;
	double ufpM[ONEREDUCTIONK + 1];
	PDVECTOR pdM[ONEREDUCTIONK + 1];
	for (unsigned int k = 0; k < ONEREDUCTIONK + 1; k++) {
		ufpM[k] = 1.5 * ufp(M[k]);
		pdM[k] = broadcast(&ufpM[k]);
	}
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&A[CACHELINE], 0, _MM_HINT_T0);
		extractscallersonereduction(pdT, A, pda, pdM)
#if defined(AVX) || defined(AVX2) || defined(SSE)
		extractscallersonereduction(pdT, A, pda, pdM)
#endif
#if defined(SSE)
		extractscallersonereduction(pdT, A, pda, pdM)
		extractscallersonereduction(pdT, A, pda, pdM)
#endif
	}
	for(; i < size; i++) {
		a = A[0];
		for (unsigned int k = 0; k < ONEREDUCTIONK + 1; k++) {
			*(unsigned long*) &T = *(unsigned long*) &a | 1;
			T += M[k];
			a -= (T - M[k]);
			M[k] = T;
		}
		A++;
	}
	for (unsigned int k = 0; k < ONEREDUCTIONK + 1; k++) {
		for (unsigned int i = 0; i < PDSIZE; i++) {
			M[k] += ((double*) &pdM[k])[i] - ufpM[k];
		}
	}
}

void sequentialOneReduction(unsigned long size, double* A, double* result, int* propagate) {
	int Msize = EXPONENTPOSITIVERANGESIZE / ONEREDUCTIONW;
	for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
		result[k] = 1.5 * pow(2.0, (ONEREDUCTIONK - k + 1 - (double) Msize) * ONEREDUCTIONW);
	}
	unsigned int step = pow(2, ((MANTISSASIZE - ONEREDUCTIONW) - 2));
	for (unsigned long start = 0; start < size; start += step) {
		__builtin_prefetch(&A[step], 0, _MM_HINT_T1);
		double localSize = (step < size - start) ? step : size - start;
		double max = sequentialAbsMax(localSize, A);
		while ((ulp(result[0]) * pow(2, ONEREDUCTIONW)) <= max) {
			for (unsigned int k = ONEREDUCTIONK - 1; k > 0; k--) {
				result[k] = result[k - 1];
				propagate[k] = propagate[k - 1];
			}
			result[0] = 1.5 * pow(2, ONEREDUCTIONW) * ufp(result[0]);
			propagate[0] = 0;
		}
		extractVectorOneReduction(localSize, A, result);
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			double ufpk = ufp(result[k]);
			if (result[k] >= 1.75 * ufpk) {
				result[k] -= 0.25 * ufpk;
				propagate[k]++;
			}
			else if (result[k] < 1.25 * ufpk) {
				result[k] += 0.5 * ufpk;
				propagate[k] -= 2;
			}
			else if (result[k] < 1.5 * ufpk) {
				result[k] += 0.25 * ufpk;
				propagate[k]--;
			}
		}
		A += step;
	}
}

#if defined(SSE)
#define TwoSum(X,Y,Xt,Yt)\
*(PDVECTOR*) &Xt[0] = addvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &Yt[0] = subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &X[0]);\
*(PDVECTOR*) &Y[0] = addvector(subvector(*(PDVECTOR*) &X[0], subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &Yt[0])),\
			subvector(*(PDVECTOR*) &Y[0], *(PDVECTOR*) &Yt[0]));\
*(PDVECTOR*) &X[0] = *(PDVECTOR*) &Xt[0];\
*(PDVECTOR*) &Xt[2] = addvector(*(PDVECTOR*) &X[2], *(PDVECTOR*) &Y[2]);\
*(PDVECTOR*) &Yt[2] = subvector(*(PDVECTOR*) &Xt[2], *(PDVECTOR*) &X[2]);\
*(PDVECTOR*) &Y[2] = addvector(subvector(*(PDVECTOR*) &X[2], subvector(*(PDVECTOR*) &Xt[2], *(PDVECTOR*) &Yt[2])),\
			subvector(*(PDVECTOR*) &Y[2], *(PDVECTOR*) &Yt[2]));\
*(PDVECTOR*) &X[2] = *(PDVECTOR*) &Xt[2];\
*(PDVECTOR*) &Xt[4] = addvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4]);\
*(PDVECTOR*) &Yt[4] = subvector(*(PDVECTOR*) &Xt[4], *(PDVECTOR*) &X[4]);\
*(PDVECTOR*) &Y[4] = addvector(subvector(*(PDVECTOR*) &X[4], subvector(*(PDVECTOR*) &Xt[4], *(PDVECTOR*) &Yt[4])),\
			subvector(*(PDVECTOR*) &Y[4], *(PDVECTOR*) &Yt[4]));\
*(PDVECTOR*) &X[4] = *(PDVECTOR*) &Xt[4];\
*(PDVECTOR*) &Xt[6] = addvector(*(PDVECTOR*) &X[6], *(PDVECTOR*) &Y[6]);\
*(PDVECTOR*) &Yt[6] = subvector(*(PDVECTOR*) &Xt[6], *(PDVECTOR*) &X[6]);\
*(PDVECTOR*) &Y[6] = addvector(subvector(*(PDVECTOR*) &X[6], subvector(*(PDVECTOR*) &Xt[6], *(PDVECTOR*) &Yt[6])),\
			subvector(*(PDVECTOR*) &Y[6], *(PDVECTOR*) &Yt[6]));\
*(PDVECTOR*) &X[6] = *(PDVECTOR*) &Xt[6];
#elif defined(AVX) || defined(AVX2)
#define TwoSum(X,Y,Xt,Yt)\
*(PDVECTOR*) &Xt[0] = addvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &Yt[0] = subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &X[0]);\
*(PDVECTOR*) &Y[0] = addvector(subvector(*(PDVECTOR*) &X[0], subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &Yt[0])),\
			subvector(*(PDVECTOR*) &Y[0], *(PDVECTOR*) &Yt[0]));\
*(PDVECTOR*) &X[0] = *(PDVECTOR*) &Xt[0];\
*(PDVECTOR*) &Xt[4] = addvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4]);\
*(PDVECTOR*) &Yt[4] = subvector(*(PDVECTOR*) &Xt[4], *(PDVECTOR*) &X[4]);\
*(PDVECTOR*) &Y[4] = addvector(subvector(*(PDVECTOR*) &X[4], subvector(*(PDVECTOR*) &Xt[4], *(PDVECTOR*) &Yt[4])),\
			subvector(*(PDVECTOR*) &Y[4], *(PDVECTOR*) &Yt[4]));\
*(PDVECTOR*) &X[4] = *(PDVECTOR*) &Xt[4];
#elif defined(AVX512)
#define TwoSum(X,Y,Xt,Yt)\
*(PDVECTOR*) &Xt[0] = addvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &Yt[0] = subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &X[0]);\
*(PDVECTOR*) &Y[0] = addvector(subvector(*(PDVECTOR*) &X[0], subvector(*(PDVECTOR*) &Xt[0], *(PDVECTOR*) &Yt[0])),\
			subvector(*(PDVECTOR*) &Y[0], *(PDVECTOR*) &Yt[0]));\
*(PDVECTOR*) &X[0] = *(PDVECTOR*) &Xt[0];
#endif
void sequentialSumK2(unsigned long size, double *X, double *sum, double *finalerror) {
	double __declspec(align(64)) result[CACHELINE] = {0};
	double __declspec(align(64)) error[CACHELINE] = {0};
	double __declspec(align(64)) temp1[CACHELINE] = {0};
	double __declspec(align(64)) temp2[CACHELINE] = {0};
	double __declspec(align(64)) data[CACHELINE];
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&X[40]);
		*(PDVECTOR*) &data[0] = *(PDVECTOR*) &X[0];
#if defined(AVX) || defined(AVX2) || defined(SSE)
		*(PDVECTOR*) &data[4] = *(PDVECTOR*) &X[4];
#endif
#if defined(SSE)
		*(PDVECTOR*) &data[2] = *(PDVECTOR*) &X[2];
		*(PDVECTOR*) &data[6] = *(PDVECTOR*) &X[6];
#endif

		TwoSum(result, data, temp1, temp2)

		*(PDVECTOR*) &error[0] = addvector(*(PDVECTOR*) &error[0], *(PDVECTOR*) &data[0]);
#if defined(AVX) || defined(AVX2) || defined(SSE)
		*(PDVECTOR*) &error[4] = addvector(*(PDVECTOR*) &error[4], *(PDVECTOR*) &data[4]);
#endif
#if defined(SSE)
		*(PDVECTOR*) &error[2] = addvector(*(PDVECTOR*) &error[2], *(PDVECTOR*) &data[2]);
		*(PDVECTOR*) &error[6] = addvector(*(PDVECTOR*) &error[6], *(PDVECTOR*) &data[6]);
#endif
		X += CACHELINE;
	}
	double tserr;
	for (; i < size; i++) {
		twoSum(result[0], X[0], &result[0], &tserr);
		error[0] += tserr;
		X++;
	}
	for (unsigned int j = 1; j < CACHELINE; j++) {
		twoSum(result[0], result[j], &result[0], &tserr);
		error[0] += tserr;
		error[0] += error[j];
	}
	*sum = result[0];
	*finalerror = error[0];
}

void sequentialSumK3(unsigned long size, double *X, double *sum1, double *sum2, double *finalerror) {
	double __declspec(align(64)) result1[CACHELINE] = {0};
	double __declspec(align(64)) result2[CACHELINE] = {0};
	double __declspec(align(64)) error[CACHELINE] = {0};
	double __declspec(align(64)) temp1[CACHELINE] = {0};
	double __declspec(align(64)) temp2[CACHELINE] = {0};
	double __declspec(align(64)) data[CACHELINE];
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&X[40]);
		*(PDVECTOR*) &data[0] = *(PDVECTOR*) &X[0];
#if defined(AVX) || defined(AVX2) || defined(SSE)
		*(PDVECTOR*) &data[4] = *(PDVECTOR*) &X[4];
#endif
#if defined(SSE)
		*(PDVECTOR*) &data[2] = *(PDVECTOR*) &X[2];
		*(PDVECTOR*) &data[6] = *(PDVECTOR*) &X[6];
#endif

		TwoSum(result1, data, temp1, temp2)
		TwoSum(result2, data, temp1, temp2)

		*(PDVECTOR*) &error[0] = addvector(*(PDVECTOR*) &error[0], *(PDVECTOR*) &data[0]);
#if defined(AVX) || defined(AVX2) || defined(SSE)
		*(PDVECTOR*) &error[4] = addvector(*(PDVECTOR*) &error[4], *(PDVECTOR*) &data[4]);
#endif
#if defined(SSE)
		*(PDVECTOR*) &error[2] = addvector(*(PDVECTOR*) &error[2], *(PDVECTOR*) &data[2]);
		*(PDVECTOR*) &error[6] = addvector(*(PDVECTOR*) &error[6], *(PDVECTOR*) &data[6]);
#endif
		X += CACHELINE;
	}
	double tserr1, tserr2;
	for (; i < size; i++) {
		twoSum(result1[0], X[0], &result1[0], &tserr1);
		twoSum(result2[0], tserr1, &result2[0], &tserr1);
		error[0] += tserr1;
		X++;
	}
	for (unsigned int j = 1; j < CACHELINE; j++) {
		twoSum(result1[0], result1[j], &result1[0], &tserr1);
		twoSum(result2[0], tserr1, &result2[0], &tserr1);
		error[0] += tserr1;
		twoSum(result2[0], result2[j], &result2[0], &tserr2);
		error[0] += tserr2;
		error[0] += error[j];
	}
	*sum1 = result1[0];
	*sum2 = result2[0];
	*finalerror = error[0];
}

//========================================================================================================================
//============================================== END OF SEQUENTIAL PART ==================================================
//========================================================================================================================

#ifdef OMP_PARALLEL

/*
double ompOptimizedSum(unsigned long size, double *A) {
	double S = 0;
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
#pragma omp parallel for default(shared) reduction(+:S)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		S += sequentialOptimizedSum(localSize, &A[i]);
	}
	return S;
}
*/

double ompOptimizedSum(unsigned long size, double *A) {
	double S = 0;
#pragma omp parallel for schedule(static) default(shared) reduction(+:S)
	for (unsigned long i = 0; i < size; i++) {
		S += A[i];
	}
	return S;
}

double ompAbsSum(unsigned long size, double *A) {
	double S = 0;
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
#pragma omp parallel for default(shared) reduction(+:S)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		S += cblas_dasum(localSize, &A[i], 1);
	}
	return S;
}

double ompAbsMax(unsigned long size, double *A) {
	double max = 0;
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
#pragma omp parallel for default(shared) reduction(max:max)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		double mx = sequentialAbsMax(localSize, &A[i]);
		max = (max > mx) ? max : mx;
	}
	return max;
}

void ompExtractHybridSum(unsigned long size, double* A, double* C, unsigned long *sharedIndex) {
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
	unsigned int LOCAL_OMP_NUMBER_OF_THREADS = size / localSizeCacheLine;
	LOCAL_OMP_NUMBER_OF_THREADS += !!(size % localSizeCacheLine);
	int sizes[LOCAL_OMP_NUMBER_OF_THREADS];
#pragma omp parallel for default(shared)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		double Ct[HYBRIDSUMSIZE] __attribute__ ((aligned(64))) = {0};
		sequentialExtractHybridSum(localSize, &A[i], Ct);
		unsigned long interSize = HYBRIDSUMSIZE;
		eliminateZeros(&interSize, Ct);
		sequentialTransSmallVector(&interSize, Ct);
		unsigned int threadId = omp_get_thread_num();
		sizes[threadId] = interSize;
		memcpy(&C[threadId * MAXTRANSSIZE], Ct, interSize * sizeof(double));
	}
	*sharedIndex = sizes[0];
	for (unsigned int i = 1; i < LOCAL_OMP_NUMBER_OF_THREADS; i++) {
		memcpy(&C[*sharedIndex], &C[i * MAXTRANSSIZE], sizes[i] * sizeof(double));
		(*sharedIndex) += sizes[i];
	}
}

void ompExtractOnlineExact(unsigned long size, double* A, double* C, unsigned long *sharedIndex) {
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
	unsigned int LOCAL_OMP_NUMBER_OF_THREADS = size / localSizeCacheLine;
	LOCAL_OMP_NUMBER_OF_THREADS += !!(size % localSizeCacheLine);
	int sizes[LOCAL_OMP_NUMBER_OF_THREADS];
#pragma omp parallel for default(shared)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		double Ct[ONLINEEXACTSIZE] __attribute__ ((aligned(64))) = {0};
		sequentialExtractOnlineExact(localSize, &A[i], Ct);
		unsigned long interSize = ONLINEEXACTSIZE;
		eliminateZeros(&interSize, Ct);
		sequentialTransSmallVector(&interSize, Ct);
		unsigned int threadId = omp_get_thread_num();
		sizes[threadId] = interSize;
		memcpy(&C[threadId * MAXTRANSSIZE], Ct, interSize * sizeof(double));
	}
	*sharedIndex = sizes[0];
	for (unsigned int i = 1; i < LOCAL_OMP_NUMBER_OF_THREADS; i++) {
		memcpy(&C[*sharedIndex], &C[i * MAXTRANSSIZE], sizes[i] * sizeof(double));
		(*sharedIndex) += sizes[i];
	}
}

double ompFirstExtractVector(unsigned long size, double *A, double sigma, double M, double *SP) {
	double S = 0;
	double S2 = 0;
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
#pragma omp parallel for default(shared) reduction(+:S,S2)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize = 0;
		double localS2 = 0;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		S += sequentialFirstExtractVector(localSize, &A[i], sigma, M, &localS2);
		S2 += localS2;
	}
	*SP = S2;
	return S;
}

double ompExtractVector(unsigned long size, double *A, double sigma) {
	double S = 0;
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
#pragma omp parallel for default(shared) reduction(+:S)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		S += sequentialExtractVector(localSize, &A[i], sigma);
	}
	return S;
}

double ompAccSumCallBack(unsigned long size, double* A, double sigma) {
	double S = 0;
	double M = nextPower2(size + 2);
	double SP = 0;
	if (sigma == 0)	sigma = nextPower2(ompAbsSum(size, A));
	if (sigma == 0) return 0;
	S = ompFirstExtractVector(size, A, sigma, M, &SP);
	fastTwoSum(S, SP, &S, &SP);
	sigma *= eps * eps * M * M;
	while (fabs(S) < M * sigma) {
		SP += ompExtractVector(size, A, sigma);
		fastTwoSum(S, SP, &S, &SP);
		sigma *= eps * M;
	}
	return S + (SP + ompOptimizedSum(size, A));
}

/*
double ompFastAccSumCallBack(unsigned long size, double* A, double T) {
	double Teta, Teta2, sigma, t, u, Tmin, fi, tpr = 0;
	double lastNormal = eta / eps;
	unsigned long localSize = size / OMP_NUMBER_OF_THREADS;
	if (T == 0) {
		T = ompAbsSum(size, A) / (1 - (size * eps));
	}
	if (T <= lastNormal) {
		return ompOptimizedSum(size, A);
	}
	sigma = (2 * T) / (1 - ((3 * size) + 1) * eps);
#pragma omp parallel for default(shared) reduction(+:tpr,Teta)
	for (unsigned long i = 0; i < size; i += localSize) tpr = sequentialFirstExtractVectorNew(localSize, size, &A[i], sigma, &Teta, &sigma);
	t = tpr;
	tpr += Teta;
	u = ufp(sigma);
	fi = ((2 * size * (size + 2) * eps) * u) / (1 - (5 * eps));
	T = 2 * size * eps * u;
	Tmin = (((3 / 2) + (4 * eps)) * (size * eps)) * sigma;
	if (T > Tmin) T = Tmin;
	if (T == 0) T = ompAbsSum(size, A) / (1 - (size * eps));
	do {
		sigma = (2 * T) / (1 - ((3 * size) + 1) * eps);
#pragma omp parallel for default(shared) reduction(+:Teta)
		for (unsigned long i = 0; i < size; i += localSize) Teta = sequentialExtractVectorNew(localSize, &A[i], sigma);
		t = tpr;
		tpr += Teta;
		u = ufp(sigma);
		fi = ((2 * size * (size + 2) * eps) * u) / (1 - (5 * eps));
		T = 2 * size * eps * u;
		Tmin = (((3 / 2) + (4 * eps)) * (size * eps)) * sigma;
		if (T > Tmin) T = Tmin;
	} while ((fabs(tpr) < fi) && (4 * T > lastNormal));
	Teta2 = (t - tpr) + Teta;
	return tpr + (Teta2 + ompOptimizedSum(size, A));
}
*/

double ompIFastSum(unsigned long size, double *A) {
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
	unsigned int LOCAL_OMP_NUMBER_OF_THREADS = size / localSizeCacheLine;
	LOCAL_OMP_NUMBER_OF_THREADS += !!(size % localSizeCacheLine);
	int sizes[LOCAL_OMP_NUMBER_OF_THREADS];
	double *C = (double*) memalign(64, MAXTRANSSIZE * LOCAL_OMP_NUMBER_OF_THREADS * sizeof(double));
	for (unsigned int j = 0; j < MAXTRANSSIZE * LOCAL_OMP_NUMBER_OF_THREADS; j++) C[j] = 0;
#pragma omp parallel for default(shared)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		unsigned long interSize = localSize;
		sequentialTransSmallVector(&interSize, &A[i]);
		unsigned int threadId = omp_get_thread_num();
		sizes[threadId] = interSize;
		memcpy(&C[threadId * MAXTRANSSIZE], &A[i], interSize * sizeof(double));
	}
	unsigned long sharedIndex = sizes[0];
	for (unsigned int i = 1; i < LOCAL_OMP_NUMBER_OF_THREADS; i++) {
		memcpy(&C[sharedIndex], &C[i * MAXTRANSSIZE], sizes[i] * sizeof(double));
		sharedIndex += sizes[i];
	}
	double S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	return S;
}

void ompReprodSumCallBack(unsigned long size, unsigned long gsize, double* A, double sigma, double *fFold, double *sFold) {
	double firstfold = 0;
	double secondfold = 0;
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
#ifdef STATS
	int first = 1;
#endif
#pragma omp parallel for default(shared) reduction(+:firstfold,secondfold)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		double localFirst = 0;
		double localSecond = 0;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		sequentialReprodSumCallBack(localSize, gsize, &A[i], sigma, &localFirst, &localSecond);
		firstfold += localFirst;
		secondfold += localSecond;
#ifdef STATS
#pragma omp critical
		if (first) {
			first = 0;
			localSumCycles = rdtsc();
		}
#endif
	}
	*fFold = firstfold;
	*sFold = secondfold;
}

double ompReprodSumCallBack(unsigned long size, double* A, double sigma) {
	double max;
	double fFold, sFold;
	if (sigma == 0) {
		max = ompAbsMax(size, A);
		sigma = (max * size) / (1 - (2 * size * eps));
	}
	ompReprodSumCallBack(size, size, A, sigma, &fFold, &sFold);
	return fFold + sFold;
}

void ompFastReprodSumCallBack(unsigned long size, unsigned long gsize, double* A, double sigma, double *fFold, double *sFold) {
	double firstfold = 0;
	double secondfold = 0;
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
#ifdef STATS
	int first = 1;
#endif
#pragma omp parallel for default(shared) reduction(+:firstfold,secondfold)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		double localFirst = 0;
		double localSecond = 0;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		sequentialFastReprodSumCallBack(localSize, gsize, &A[i], sigma, &localFirst, &localSecond);
		firstfold += localFirst;
		secondfold += localSecond;
#ifdef STATS
#pragma omp critical
		if (first) {
			first = 0;
			localSumCycles = rdtsc();
		}
#endif
	}
	*fFold = firstfold;
	*sFold = secondfold;
}

double ompFastReprodSumCallBack(unsigned long size, double* A, double sigma) {
	double max;
	double fFold, sFold;
	if (sigma == 0) {
		max = ompAbsMax(size, A);
		sigma = (max * size) / (1 - (4 * (size + 1) * eps));
	}
	ompFastReprodSumCallBack(size, size, A, sigma, &fFold, &sFold);
	return fFold + sFold;
}

double ompDynamicSum(unsigned long size, double *A) {
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
	unsigned int LOCAL_OMP_NUMBER_OF_THREADS = size / localSizeCacheLine;
	LOCAL_OMP_NUMBER_OF_THREADS += !!(size % localSizeCacheLine);
	int sizes[LOCAL_OMP_NUMBER_OF_THREADS];
	double *C = (double*) memalign(64, MAXTRANSSIZE * OMP_NUMBER_OF_THREADS * sizeof(double));
	for (unsigned int j = 0; j < MAXTRANSSIZE * OMP_NUMBER_OF_THREADS; j++) C[j] = 0;
#pragma omp parallel for default(shared)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		unsigned long interSize = localSize;
		sequentialTransVector(&interSize, &A[i]);
		unsigned int threadId = omp_get_thread_num();
		sizes[threadId] = interSize;
		memcpy(&C[threadId * MAXTRANSSIZE], &A[i], interSize * sizeof(double));
	}
	unsigned long sharedIndex = sizes[0];
	for (unsigned int i = 1; i < LOCAL_OMP_NUMBER_OF_THREADS; i++) {
		memcpy(&C[sharedIndex], &C[i * MAXTRANSSIZE], sizes[i] * sizeof(double));
		sharedIndex += sizes[i];
	}
	double S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	return S;
}

void ompTransVector(unsigned long* size, double *A) {
	unsigned long localSizeCacheLine = *size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(*size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
	unsigned int LOCAL_OMP_NUMBER_OF_THREADS = *size / localSizeCacheLine;
	int sizes[LOCAL_OMP_NUMBER_OF_THREADS];
	LOCAL_OMP_NUMBER_OF_THREADS += !!(*size % localSizeCacheLine);
	double *C = (double*) memalign(64, MAXTRANSSIZE * OMP_NUMBER_OF_THREADS * sizeof(double));
	for (unsigned int j = 0; j < MAXTRANSSIZE * OMP_NUMBER_OF_THREADS; j++) C[j] = 0;
#pragma omp parallel for default(shared)
	for (unsigned long i = 0; i < *size; i += localSizeCacheLine) {
		unsigned long localSize;
		if (i + localSizeCacheLine > *size) localSize = *size - i;
		else localSize = localSizeCacheLine;
		unsigned long interSize = localSize;
		sequentialTransVector(&interSize, &A[i]);
		unsigned int threadId = omp_get_thread_num();
		sizes[threadId] = interSize;
		memcpy(&C[threadId * MAXTRANSSIZE], &A[i], interSize * sizeof(double));
	}
	unsigned long sharedIndex = sizes[0];
	for (unsigned int i = 1; i < LOCAL_OMP_NUMBER_OF_THREADS; i++) {
		memcpy(&C[sharedIndex], &C[i * MAXTRANSSIZE], sizes[i] * sizeof(double));
		sharedIndex += sizes[i];
	}
	*size = sharedIndex;
	sequentialTransSmallVector(size, C);
	memcpy(A, C, *size * sizeof(double));
	free(C);
}

void ompTransSmallVector(unsigned long* size, double *A) {
	unsigned long localSizeCacheLine = *size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(*size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
	unsigned int LOCAL_OMP_NUMBER_OF_THREADS = *size / localSizeCacheLine;
	int sizes[LOCAL_OMP_NUMBER_OF_THREADS];
	LOCAL_OMP_NUMBER_OF_THREADS += !!(*size % localSizeCacheLine);
	double *C = (double*) memalign(64, MAXTRANSSIZE * OMP_NUMBER_OF_THREADS * sizeof(double));
	for (unsigned int j = 0; j < MAXTRANSSIZE * OMP_NUMBER_OF_THREADS; j++) C[j] = 0;
#pragma omp parallel for default(shared)
	for (unsigned long i = 0; i < *size; i += localSizeCacheLine) {
		unsigned long localSize;
		if (i + localSizeCacheLine > *size) localSize = *size - i;
		else localSize = localSizeCacheLine;
		unsigned long interSize = localSize;
		sequentialTransSmallVector(&interSize, &A[i]);
		unsigned int threadId = omp_get_thread_num();
		sizes[threadId] = interSize;
		memcpy(&C[threadId * MAXTRANSSIZE], &A[i], interSize * sizeof(double));
	}
	unsigned long sharedIndex = sizes[0];
	for (unsigned int i = 1; i < LOCAL_OMP_NUMBER_OF_THREADS; i++) {
		memcpy(&C[sharedIndex], &C[i * MAXTRANSSIZE], sizes[i] * sizeof(double));
		sharedIndex += sizes[i];
	}
	sequentialTransSmallVector(&sharedIndex, C);
	memcpy(A, C, sharedIndex * sizeof(double));
	*size = sharedIndex;
	free(C);
}

void ompOneReduction(unsigned long size, double* A, double* result, int* propagate) {
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
	unsigned int LOCAL_OMP_NUMBER_OF_THREADS = size / localSizeCacheLine;
	LOCAL_OMP_NUMBER_OF_THREADS += !!(size % localSizeCacheLine);
	double results[LOCAL_OMP_NUMBER_OF_THREADS][ONEREDUCTIONK];
	int propagates[LOCAL_OMP_NUMBER_OF_THREADS][ONEREDUCTIONK];
#pragma omp parallel for default(shared)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		double localResult[ONEREDUCTIONK] = {0};
		int localPropagate[ONEREDUCTIONK] = {0};
		sequentialOneReduction(localSize, &A[i], localResult, localPropagate);
		unsigned int threadId = omp_get_thread_num();
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			results[threadId][k] = localResult[k];
			propagates[threadId][k] = localPropagate[k];
		}
	}
	for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
		result[k] = results[0][k];
		propagate[k] = propagates[0][k];
	}
	for (unsigned int i = 1; i < LOCAL_OMP_NUMBER_OF_THREADS; i++) addIFP(result, propagate, results[i], propagates[i], result, propagate);
}

void ompSumK2(unsigned long size, double *X, double *sum, double *finalerror) {
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
	unsigned int LOCAL_OMP_NUMBER_OF_THREADS = size / localSizeCacheLine;
	LOCAL_OMP_NUMBER_OF_THREADS += !!(size % localSizeCacheLine);
	double results[LOCAL_OMP_NUMBER_OF_THREADS * 2];
#pragma omp parallel for default(shared)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		double S = 0;
		double E = 0;
		sequentialSumK2(localSize, &X[i], &S, &E);
		unsigned int threadId = omp_get_thread_num();
		results[threadId] = S;
		results[threadId + LOCAL_OMP_NUMBER_OF_THREADS] = E;
	}
	for (unsigned int i = 0; i < LOCAL_OMP_NUMBER_OF_THREADS; i++) {
		*finalerror += results[i + LOCAL_OMP_NUMBER_OF_THREADS];
		twoSum(*sum, results[i], sum, &results[i]);
		*finalerror += results[i];
	}
}

void ompSumK3(unsigned long size, double *X, double *sum1, double *sum2, double *finalerror) {
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
	unsigned int LOCAL_OMP_NUMBER_OF_THREADS = size / localSizeCacheLine;
	LOCAL_OMP_NUMBER_OF_THREADS += !!(size % localSizeCacheLine);
	double results[LOCAL_OMP_NUMBER_OF_THREADS * 3];
#pragma omp parallel for default(shared)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		double S1 = 0;
		double S2 = 0;
		double E = 0;
		sequentialSumK3(localSize, &X[i], &S1, &S2, &E);
		unsigned int threadId = omp_get_thread_num();
		results[threadId] = S1;
		results[threadId + LOCAL_OMP_NUMBER_OF_THREADS] = S2;
		results[threadId + 2 * LOCAL_OMP_NUMBER_OF_THREADS] = E;
	}
	for (unsigned int i = 0; i < LOCAL_OMP_NUMBER_OF_THREADS; i++) {
		*finalerror += results[i + 2 * LOCAL_OMP_NUMBER_OF_THREADS];
		twoSum(*sum2, results[i + LOCAL_OMP_NUMBER_OF_THREADS], sum2, &results[i + LOCAL_OMP_NUMBER_OF_THREADS]);
		*finalerror += results[i + LOCAL_OMP_NUMBER_OF_THREADS];
		twoSum(*sum1, results[i], sum1, &results[i]);
		twoSum(*sum2, results[i], sum2, &results[i]);
		*finalerror += results[i];
	}
}
#endif

//========================================================================================================================
//=========================================== END OF OPENMP PARALLEL PART ================================================
//========================================================================================================================

#if defined(MPI_PARALLEL) && not defined(OMP_PARALLEL)
double mpiOptimizedSum(unsigned long size, double *A) {
	double S, glS;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	S = sequentialOptimizedSum(localSize, A);
	MPI_Allreduce(&S, &glS, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	return glS;
}

double mpiAbsSum(unsigned long size, double *A) {
	double S, glS;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	S = cblas_dasum(localSize, A, 1);
	MPI_Allreduce(&S, &glS, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	return S;
}

double mpiAbsMax(unsigned long size, double *A) {
	double maxAbs, glMax;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	maxAbs = sequentialAbsMax(localSize, A);
	MPI_Allreduce(&maxAbs, &glMax, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
	return glMax;
}

double mpiFirstExtractVector(unsigned long size, double *A, double sigma, double M, double *SP) {
	double S[2];
	double glS[2];
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	S[0] = sequentialFirstExtractVector(localSize, A, sigma, M, &S[1]);
	MPI_Allreduce(&S[0], &glS[0], 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	*SP = glS[1];
	return glS[0];
}

double mpiExtractVector(unsigned long size, double *A, double sigma) {
	double S;
	double glS;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	S = sequentialExtractVector(localSize, A, sigma);
	MPI_Allreduce(&S, &glS, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	return glS;
}

double mpiAccSumCallBack(unsigned long size, double* A, double sigma) {
	double S = 0;
	double M = nextPower2(size + 2);
	double SP = 0;
	if (sigma == 0)	sigma = nextPower2(mpiAbsSum(size, A));
	if (sigma == 0) return 0;
	S = mpiFirstExtractVector(size, A, sigma, M, &SP);
	fastTwoSum(S, SP, &S, &SP);
	sigma *= eps * eps * M * M;
	while (fabs(S) < M * sigma) {
		SP += mpiExtractVector(size, A, sigma);
		fastTwoSum(S, SP, &S, &SP);
		sigma *= eps * M;
	}
	return S + (SP + mpiOptimizedSum(size, A));
}

void mpiExtractHybridSum(unsigned long size, double* A, double* C, unsigned long *sharedIndex) {
	*sharedIndex = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	sequentialExtractHybridSum(localSize, A, C);
	unsigned long interSize = HYBRIDSUMSIZE;
	eliminateZeros(&interSize, C);
	sequentialTransSmallVector(&interSize, C);
	if (myRank == 0) {
		*sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&C[*sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			*sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(C, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
}

void mpiExtractOnlineExact(unsigned long size, double* A, double* C, unsigned long *sharedIndex) {
	*sharedIndex = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	sequentialExtractOnlineExact(localSize, A, C);
	unsigned long interSize = ONLINEEXACTSIZE;
	eliminateZeros(&interSize, C);
	sequentialTransSmallVector(&interSize, C);
	if (myRank == 0) {
		*sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&C[*sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			*sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(C, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
}

void mpiReprodSumCallBack(unsigned long size, double* A, double sigma, double *fFold, double *sFold) {
	double S[2];
	double glS[2];
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	sequentialReprodSumCallBack(localSize, size, A, sigma, &S[0], &S[1]);
#ifdef STATS
	localSumCycles = rdtsc();
#endif
	MPI_Allreduce(&S, &glS, 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	*fFold = glS[0];
	*sFold = glS[1];
}

double mpiReprodSumCallBack(unsigned long size, double* A, double sigma) {
	double max;
	double fFold, sFold;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	if (sigma == 0) {
		max = mpiAbsMax(size, A);
		sigma = (max * size) / (1 - (2 * size * eps));
	}
	mpiReprodSumCallBack(size, A, sigma, &fFold, &sFold);
	return fFold + sFold;
}

void mpiFastReprodSumCallBack(unsigned long size, double* A, double sigma, double *fFold, double *sFold) {
	double S[2];
	double glS[2];
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	sequentialFastReprodSumCallBack(localSize, size, A, sigma, &S[0], &S[1]);
#ifdef STATS
	localSumCycles = rdtsc();
#endif
	MPI_Allreduce(&S, &glS, 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	*fFold = glS[0];
	*sFold = glS[1];
}

double mpiFastReprodSumCallBack(unsigned long size, double* A, double sigma) {
	double max;
	double fFold, sFold;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	if (sigma == 0) {
		max = mpiAbsMax(size, A);
		sigma = (max * size) / (1 - (4 * (size + 1) * eps));
	}
	mpiFastReprodSumCallBack(size, A, sigma, &fFold, &sFold);
	return fFold + sFold;
}

double mpiDynamicSum(unsigned long size, double *A) {
	unsigned long sharedIndex = 0;
	double *C = (double*) memalign(64, MAXTRANSSIZE * MPI_NUMBER_OF_THREADS * sizeof(double));
	for (unsigned int j = 0; j < MAXTRANSSIZE * MPI_NUMBER_OF_THREADS; j++) C[j] = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	unsigned long interSize = localSize;
	sequentialTransVector(&interSize, A);
	if (myRank == 0) {
		sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&C[sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(C, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
	double S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	return S;
}

double mpiIFastSum(unsigned long size, double *A) {
	unsigned long sharedIndex = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	unsigned long interSize = localSize;
	sequentialTransSmallVector(&interSize, A);
	if (myRank == 0) {
		sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&A[sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(A, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
	return iFastSumCallBack(&sharedIndex, A, 0);
}

void mpiTransVector(unsigned long* size, double *A) {
	unsigned long sharedIndex = 0;
	double *C = (double*) memalign(64, MAXTRANSSIZE * MPI_NUMBER_OF_THREADS * sizeof(double));
	for (unsigned int j = 0; j < MAXTRANSSIZE * MPI_NUMBER_OF_THREADS; j++) C[j] = 0;
	unsigned long localSize = *size / MPI_NUMBER_OF_THREADS;
	unsigned long interSize = localSize;
	sequentialTransVector(&interSize, A);
	if (myRank == 0) {
		sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&C[sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(C, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
	sequentialTransSmallVector(size, C);
	memcpy(A, C, *size * sizeof(double));
	free(C);
}

void mpiOneReduction(unsigned long size, double* A, double* result, int* propagate) {
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	IFP localIFP;
	IFP globalIFP;
	sequentialOneReduction(localSize, A, &localIFP.F[0], &localIFP.P[0]);
	MPI_Allreduce((int*) &localIFP, (int*) &globalIFP, 3 * ONEREDUCTIONK, MPI_INT, MPI_ADD_IFP, MPI_COMM_WORLD);
	for (unsigned int i = 0; i < ONEREDUCTIONK; i++) {
		result[i] = globalIFP.F[i];
		propagate[i] = globalIFP.P[i];
	}
}

void mpiSumK2(unsigned long size, double *X, double *sum, double *finalerror) {
	double S[2] = {0};
	double glS[2];
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	sequentialSumK2(localSize, X, &S[0], &S[1]);
	MPI_Allreduce(&S[0], &glS[0], 2, MPI_DOUBLE, MPI_QUAD_SUM, MPI_COMM_WORLD);
	*sum = glS[0];
	*finalerror = glS[1];
}
#endif

//========================================================================================================================
//============================================= END OF MPI PARALLEL PART =================================================
//========================================================================================================================

#if defined(MPI_PARALLEL) && defined(OMP_PARALLEL)
double hybridOptimizedSum(unsigned long size, double *A) {
	double S, glS;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	S = ompOptimizedSum(localSize, A);
	MPI_Allreduce(&S, &glS, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	return glS;
}

double hybridAbsSum(unsigned long size, double *A) {
	double S, glS;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	S = ompAbsSum(localSize, A);
	MPI_Allreduce(&S, &glS, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	return glS;
}

double hybridAbsMax(unsigned long size, double *A) {
	double maxAbs, glMax;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	maxAbs = ompAbsMax(localSize, A);
	MPI_Allreduce(&maxAbs, &glMax, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
	return glMax;
}

double hybridFirstExtractVector(unsigned long size, double *A, double sigma, double M, double *SP) {
	double S[2];
	double glS[2];
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	S[0] = ompFirstExtractVector(localSize, A, sigma, M, &S[1]);
	MPI_Allreduce(&S[0], &glS[0], 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	*SP = glS[1];
	return glS[0];
}

double hybridExtractVector(unsigned long size, double *A, double sigma) {
	double S;
	double glS;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	S = ompExtractVector(localSize, A, sigma);
	MPI_Allreduce(&S, &glS, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	return glS;
}

double hybridAccSumCallBack(unsigned long size, double* A, double sigma) {
	double S = 0;
	double M = nextPower2(size + 2);
	double SP = 0;
	if (sigma == 0)	sigma = nextPower2(hybridAbsSum(size, A));
	if (sigma == 0) return 0;
	S = hybridFirstExtractVector(size, A, sigma, M, &SP);
	fastTwoSum(S, SP, &S, &SP);
	sigma *= eps * eps * M * M;
	while (fabs(S) < M * sigma) {
		SP += hybridExtractVector(size, A, sigma);
		fastTwoSum(S, SP, &S, &SP);
		sigma *= eps * M;
	}
	return S + (SP + hybridOptimizedSum(size, A));
}

void hybridExtractHybridSum(unsigned long size, double* A, double* C, unsigned long *sharedIndex) {
	*sharedIndex = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	unsigned long interSize;
	ompExtractHybridSum(localSize, A, C, &interSize);
	sequentialTransSmallVector(&interSize, C);
	if (myRank == 0) {
		*sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&C[*sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			*sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(C, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
}

void hybridExtractOnlineExact(unsigned long size, double* A, double* C, unsigned long *sharedIndex) {
	*sharedIndex = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	unsigned long interSize;
	ompExtractOnlineExact(localSize, A, C, &interSize);
	sequentialTransSmallVector(&interSize, C);
	if (myRank == 0) {
		*sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&C[*sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			*sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(C, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
}

void hybridReprodSumCallBack(unsigned long size, double* A, double sigma, double *fFold, double *sFold) {
	double S[2];
	double glS[2];
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	ompReprodSumCallBack(localSize, size, A, sigma, &S[0], &S[1]);
	MPI_Allreduce(&S, &glS, 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	*fFold = glS[0];
	*sFold = glS[1];
}

double hybridReprodSumCallBack(unsigned long size, double* A, double sigma) {
	double max;
	double fFold, sFold;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	if (sigma == 0) {
		max = hybridAbsMax(size, A);
		sigma = (max * size) / (1 - (2 * size * eps));
	}
	hybridReprodSumCallBack(size, A, sigma, &fFold, &sFold);
	return fFold + sFold;
}

void hybridFastReprodSumCallBack(unsigned long size, double* A, double sigma, double *fFold, double *sFold) {
	double S[2];
	double glS[2];
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	ompFastReprodSumCallBack(localSize, size, A, sigma, &S[0], &S[1]);
	MPI_Allreduce(&S, &glS, 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	*fFold = glS[0];
	*sFold = glS[1];
}

double hybridFastReprodSumCallBack(unsigned long size, double* A, double sigma) {
	double max;
	double fFold, sFold;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	if (sigma == 0) {
		max = hybridAbsMax(size, A);
		sigma = (max * size) / (1 - (4 * (size + 1) * eps));
	}
	hybridFastReprodSumCallBack(size, A, sigma, &fFold, &sFold);
	return fFold + sFold;
}

double hybridDynamicSum(unsigned long size, double *A) {
	unsigned long sharedIndex = 0;
	double *C = (double*) memalign(64, MAXTRANSSIZE * MPI_NUMBER_OF_THREADS * sizeof(double));
	for (unsigned int j = 0; j < MAXTRANSSIZE * MPI_NUMBER_OF_THREADS; j++) C[j] = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	unsigned long interSize = localSize;
	ompTransVector(&interSize, A);
	if (myRank == 0) {
		sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&C[sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(C, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
	double S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	return S;
}

double hybridIFastSum(unsigned long size, double *A) {
	unsigned long sharedIndex = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	unsigned long interSize = localSize;
	ompTransSmallVector(&interSize, A);
	if (myRank == 0) {
		sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&A[sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(A, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
	return iFastSumCallBack(&sharedIndex, A, 0);
}

void hybridTransVector(unsigned long* size, double *A) {
	unsigned long sharedIndex = 0;
	double *C = (double*) memalign(64, MAXTRANSSIZE * MPI_NUMBER_OF_THREADS * sizeof(double));
	for (unsigned int j = 0; j < MAXTRANSSIZE * MPI_NUMBER_OF_THREADS; j++) C[j] = 0;
	unsigned long localSize = *size / MPI_NUMBER_OF_THREADS;
	unsigned long interSize = localSize;
	ompTransVector(&interSize, A);
	if (myRank == 0) {
		sharedIndex += interSize;
		MPI_Status state;
		int messageSize;
		for (int proc = 1; proc < MPI_NUMBER_OF_THREADS; proc++) {
			MPI_Probe(proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			MPI_Get_count(&state, MPI_DOUBLE, &messageSize);
			MPI_Recv(&C[sharedIndex], messageSize, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, &state);
			sharedIndex += interSize;
		}
	}
	else {
		MPI_Send(C, interSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
	sequentialTransSmallVector(size, C);
	memcpy(A, C, *size * sizeof(double));
	free(C);
}

void hybridOneReduction(unsigned long size, double* A, double* result, int* propagate) {
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	IFP localIFP;
	IFP globalIFP;
	ompOneReduction(localSize, A, &localIFP.F[0], &localIFP.P[0]);
	MPI_Allreduce((int*) &localIFP, (int*) &globalIFP, 3 * ONEREDUCTIONK, MPI_INT, MPI_ADD_IFP, MPI_COMM_WORLD);
	for (unsigned int i = 0; i < ONEREDUCTIONK; i++) {
		result[i] = globalIFP.F[i];
		propagate[i] = globalIFP.P[i];
	}
}

void hybridSumK2(unsigned long size, double *X, double *sum, double *finalerror) {
	double S[2] = {0};
	double glS[2];
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	ompSumK2(localSize, X, &S[0], &S[1]);
	MPI_Allreduce(&S[0], &glS[0], 2, MPI_DOUBLE, MPI_QUAD_SUM, MPI_COMM_WORLD);
	*sum = glS[0];
	*finalerror = glS[1];
}
#endif

//========================================================================================================================
//=========================================== END OF HYBRID PARALLEL PART ================================================
//========================================================================================================================

double optimizedSum(unsigned long size, double *A) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	return hybridOptimizedSum(size, A);
#else
	return mpiOptimizedSum(size, A);
#endif
#elif defined(OMP_PARALLEL)
	return ompOptimizedSum(size, A);
#else
	return sequentialOptimizedSum(size, A);
#endif
}

double absSum(unsigned long size, double *A) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	return hybridAbsSum(size, A);
#else
	return mpiAbsSum(size, A);
#endif
#elif defined(OMP_PARALLEL)
	return ompAbsSum(size, A);
#else
	return sequentialAbsSum(size, A);
#endif
}

double absMax(unsigned long size, double *A) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	return hybridAbsMax(size, A);
#else
	return mpiAbsMax(size, A);
#endif
#elif defined(OMP_PARALLEL)
	return ompAbsMax(size, A);
#else
	return sequentialAbsMax(size, A);
#endif
}

// Appel de l'implémentation de OnlineExact
double onlineExact(unsigned long size, double* A) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	unsigned int Csize = (MPI_NUMBER_OF_THREADS > OMP_NUMBER_OF_THREADS) ? MPI_NUMBER_OF_THREADS : OMP_NUMBER_OF_THREADS;
	Csize *= MAXTRANSSIZE;
	double *C = (double*) memalign(64, Csize * sizeof(double));
	for (unsigned int j = 0; j < Csize; j++) C[j] = 0;
	unsigned long sharedIndex;
	hybridExtractOnlineExact(size, A, C, &sharedIndex);
	double S;
	if (!myRank) S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	MPI_Bcast(&S, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	return S;
#else
	unsigned int Csize = MAXTRANSSIZE * MPI_NUMBER_OF_THREADS;
	Csize = (Csize > ONLINEEXACTSIZE) ? Csize : ONLINEEXACTSIZE;
	double *C = (double*) memalign(64, Csize * sizeof(double));
	for (unsigned int j = 0; j < Csize; j++) C[j] = 0;
	unsigned long sharedIndex;
	mpiExtractOnlineExact(size, A, C, &sharedIndex);
	double S;
	if (!myRank) S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	MPI_Bcast(&S, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	return S;
#endif
#elif defined(OMP_PARALLEL)
	double *C = (double*) memalign(64, MAXTRANSSIZE * OMP_NUMBER_OF_THREADS * sizeof(double));
	for (unsigned int j = 0; j < MAXTRANSSIZE * OMP_NUMBER_OF_THREADS; j++) C[j] = 0;
	unsigned long sharedIndex;
	ompExtractOnlineExact(size, A, C, &sharedIndex);
	double S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	return S;
#else
	double C[ONLINEEXACTSIZE] __attribute__ ((aligned(64))) = {0};
	sequentialExtractOnlineExact(size, A, C);
	unsigned long interSize = ONLINEEXACTSIZE;
	eliminateZeros(&interSize, C);
	return iFastSum(interSize, C);
#endif
}

// Appel de l'implémentation de HybridSum
double hybridSum(unsigned long size, double* A) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	unsigned int Csize = (MPI_NUMBER_OF_THREADS > OMP_NUMBER_OF_THREADS) ? MPI_NUMBER_OF_THREADS : OMP_NUMBER_OF_THREADS;
	Csize *= MAXTRANSSIZE;
	double *C = (double*) memalign(64, Csize * sizeof(double));
	for (unsigned int j = 0; j < Csize; j++) C[j] = 0;
	unsigned long sharedIndex;
	hybridExtractHybridSum(size, A, C, &sharedIndex);
	double S;
	if (!myRank) S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	MPI_Bcast(&S, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	return S;
#else
	unsigned int Csize = MAXTRANSSIZE * MPI_NUMBER_OF_THREADS;
	Csize = (Csize > HYBRIDSUMSIZE) ? Csize : HYBRIDSUMSIZE;
	double *C = (double*) memalign(64, Csize * sizeof(double));
	for (unsigned int j = 0; j < Csize; j++) C[j] = 0;
	unsigned long sharedIndex;
	mpiExtractHybridSum(size, A, C, &sharedIndex);
	double S;
	if (!myRank) S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	MPI_Bcast(&S, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	return S;
#endif
#elif defined(OMP_PARALLEL)
	double *C = (double*) memalign(64, MAXTRANSSIZE * OMP_NUMBER_OF_THREADS * sizeof(double));
	for (unsigned int j = 0; j < MAXTRANSSIZE * OMP_NUMBER_OF_THREADS; j++) C[j] = 0;
	unsigned long sharedIndex;
	ompExtractHybridSum(size, A, C, &sharedIndex);
	double S = iFastSumCallBack(&sharedIndex, C, 0);
	free(C);
	return S;
#else
	double C[HYBRIDSUMSIZE] __attribute__ ((aligned(64))) = {0};
	sequentialExtractHybridSum(size, A, C);
	unsigned long interSize = HYBRIDSUMSIZE;
	eliminateZeros(&interSize, C);
	return iFastSum(interSize, C);
#endif
}

// Appel de l'implémentation de iFastSum
double iFastSum(unsigned long size, double* A) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	return hybridIFastSum(size, A);
#else
	return mpiIFastSum(size, A);
#endif
#elif defined(OMP_PARALLEL)
	return ompIFastSum(size, A);
#else
	return iFastSumCallBack(&size, A, 0);
#endif
}

// Appel de l'implémentation de AccSum
double accSum(unsigned long size, double* A) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	return hybridAccSumCallBack(size, A, 0);
#else
	return mpiAccSumCallBack(size, A, 0);
#endif
#elif defined(OMP_PARALLEL)
	return ompAccSumCallBack(size, A, 0);
#else
	return sequentialAccSumCallBack(size, A, 0);
#endif
}

// Appel de l'implémentation de FastAccSum
double fastAccSum(unsigned long size, double* A) {
	return sequentialFastAccSumCallBack(size, A, 0);
}

// Appel de l'implémentation de ReprodSum
double reprodSum(unsigned long size, double* A) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	return hybridReprodSumCallBack(size, A, 0);
#else
	return mpiReprodSumCallBack(size, A, 0);
#endif
#elif defined(OMP_PARALLEL)
	return ompReprodSumCallBack(size, A, 0);
#else
	return sequentialReprodSumCallBack(size, A, 0);
#endif
}

// Appel de l'implémentation de FastReprodSum
double fastReprodSum(unsigned long size, double* A) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	return hybridFastReprodSumCallBack(size, A, 0);
#else
	return mpiFastReprodSumCallBack(size, A, 0);
#endif
#elif defined(OMP_PARALLEL)
	return ompFastReprodSumCallBack(size, A, 0);
#else
	return sequentialFastReprodSumCallBack(size, A, 0);
#endif
}

// Appel de l'implémentation de OneReduction
double oneReduction(unsigned long size, double* A) {
	double result[ONEREDUCTIONK] = {0};
	int propagate[ONEREDUCTIONK] = {0};
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	hybridOneReduction(size, A, result, propagate);
#else
	mpiOneReduction(size, A, result, propagate);
#endif
#elif defined(OMP_PARALLEL)
	ompOneReduction(size, A, result, propagate);
#else
	sequentialOneReduction(size, A, result, propagate);
#endif
	double finalResult = 0;
	for (unsigned int k = 0; k < ONEREDUCTIONK; k++) finalResult += result[k] - 1.5 * ufp(result[k]) + 0.25 * propagate[k] * ufp(result[k]);
	return finalResult;
}

// Choisir l'algorithme de summation selon la taille
double dynamicSum(unsigned long size, double *A) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	return hybridDynamicSum(size, A);
#else
	return mpiDynamicSum(size, A);
#endif
#elif defined(OMP_PARALLEL)
	return ompDynamicSum(size, A);
#else
	return sequentialDynamicSum(size, A);
#endif
}

// Choisir l'algorithme de summation selon la taille
void transVector(unsigned long* size, double *A) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	hybridTransVector(size, A);
#else
	mpiTransVector(size, A);
#endif
#elif defined(OMP_PARALLEL)
	ompTransVector(size, A);
#else
	sequentialTransVector(size, A);
#endif
}

double sumK2(unsigned long size, double *X) {
	double sum = 0, finalerror = 0;
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	hybridSumK2(size, X, &sum, &finalerror);
#else
	mpiSumK2(size, X, &sum, &finalerror);
#endif
#elif defined(OMP_PARALLEL)
	ompSumK2(size, X, &sum, &finalerror);
#else
	sequentialSumK2(size, X, &sum, &finalerror);
#endif
	return sum + finalerror;
}

double sumK3(unsigned long size, double *X) {
	double sum1 = 0, sum2 = 0, finalerror = 0;
#if defined(OMP_PARALLEL)
	ompSumK3(size, X, &sum1, &sum2, &finalerror);
#else
	sequentialSumK3(size, X, &sum1, &sum2, &finalerror);
#endif
	return sum1 + sum2 + finalerror;
}

