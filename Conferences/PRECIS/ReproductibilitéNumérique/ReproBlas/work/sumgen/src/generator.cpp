/*
 *************************************************************************************
 Copyright (c) 2015, University of Perpignan
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************
*/


#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <malloc.h>
#include <math.h>
#include <unistd.h>
#include <limits.h>
#include <sys/types.h>
#include <fcntl.h>
#include <float.h>
#include <mpfr.h>

#define MPFR_RNDN GMP_RNDN

double rand_double(double M) {
	return (M * (double) rand() / (double) RAND_MAX);
}

double rand_unif(double a, double b) {
	return (a + (b - a) * rand() / (double) RAND_MAX);
}

unsigned int rand_uint(unsigned int M) { //uniform_int(0,M+1)
	return((unsigned int) (((M + 1.0) * rand()) / (RAND_MAX + 1.0)));
}

void copyVector(unsigned int n, double *src, double *dest) {
	for (int i = 0; i < n; i++) dest[i] = src[i];
}

double genSum(double *x, unsigned int n, double exp_c, double* C) {
	unsigned int i, j, n2 = n/2;
	double b, s;
	double c = pow(10, exp_c);
	b = log2(c);
	x[0] = (2.0 * rand_double(1) - 1);
	x[1] = ldexp(2.0 * rand_double(1) - 1, nearbyint(b) + 1);
	for (i = 2; i < n2; i++) x[i] = ldexp(rand_unif(-1.0, 1.0), nearbyint(rand_unif(0.0, 1.0) * b));
	mpfr_set_default_prec(2100);
	mpfr_t sum, elt, abselt, abssum;
	mpfr_init(sum);
	mpfr_init(elt);
	mpfr_init(abselt);
	mpfr_init(abssum);
	mpfr_set_d(sum, 0, MPFR_RNDN);
	mpfr_set_d(abssum, 0, MPFR_RNDN);
	for (i = 0; i < n2; i++) {
		mpfr_set_d(elt, x[i], MPFR_RNDN);
		mpfr_set_d(abselt, fabs(x[i]), MPFR_RNDN);
		mpfr_add(abssum, abssum, abselt, MPFR_RNDN);
		mpfr_add(sum, sum, elt, MPFR_RNDN);
	}
	for (i = n2; i < n; i++) {
		double exactsum = mpfr_get_d(sum, MPFR_RNDN);
		x[i] = ldexp(rand_unif(-1.0, 1.0), nearbyint(b - (i - n2) * b / (n - 1 - n2))) - exactsum;
		mpfr_set_d(elt, x[i], MPFR_RNDN);
		mpfr_set_d(abselt, fabs(x[i]), MPFR_RNDN);
		mpfr_add(abssum, abssum, abselt, MPFR_RNDN);
		mpfr_add(sum, sum, elt, MPFR_RNDN);
	}
	for (i = 1; i < n; i++)
		if ((j = rand_uint(i)) != i) {
			b = x[j];
			x[j] = x[i];
			x[i] = b;
		}
	b = mpfr_get_d(abssum, MPFR_RNDN);
	s = mpfr_get_d(sum, MPFR_RNDN);
	*C = b / fabs(s);
	return(s);
}

int main(int argc, char **argv) {
	unsigned int Cexp; 
	unsigned long n;
	double Cact, sum;
	int r;
	n = atoi(argv[1]);
	Cexp = atoi(argv[2]);
	FILE *file = fopen(argv[3], "wb");
	double *tab = (double*) memalign(64, sizeof(double) * n);
	srand(time(0));
	sum = genSum(tab, n, Cexp, &Cact);
	printf("Requested cond : 10^%d\nCond effective : %g\nSum : %g\n", Cexp, Cact, sum);
	r = fwrite(&n, sizeof(unsigned long), 1, file);
	r = fwrite(tab, sizeof(double), n, file);
	r = fwrite(&sum, sizeof(double), 1, file);
	r = fwrite(&Cact, sizeof(double), 1, file);
	free(tab);
	fclose(file);
	return EXIT_SUCCESS;
}

