#include "sum.h"
//#include <emmintrin.h>
#include <emmintrin.h>
//#include <smmintrin.h>

/* ----------------------------------------------------------------- */
/* --- Summation algorithms ---------------------------------------- */
/* ----------------------------------------------------------------- */

double Sum(double *p, unsigned int n) {
  double r = p[0];
  int i;
  
  for(i=1; i<n; i++) r += p[i];
  return(r);
}

double Sum_aeb(double *beta, double *p, unsigned int n) {
  double r = p[0], b;
  int i;
  
  b = fabs(p[0]);
  for(i=1; i<n; i++) {
    b += fabs(p[i]);
    r += p[i];
  }
  if(beta != NULL) *beta = gam(2*n+1)*b+ETA_DBL;
  return(r);
}

double Sum_reb(double *beta, double *p, unsigned int n) {
  double r = p[0], b;
  int i;
  
  b = 0.0;
  for(i=1; i<n; i++) {
    r += p[i];
    b += fabs(r);
  }
  if(beta != NULL) *beta = (URD_DBL*b)/(1-n*URD_DBL)+3*ETA_DBL;
  return(r);
}
 
double Sum2(double *p, unsigned int n) {
  double s, s_, c, t;
  unsigned int i;
  
  s = c = 0.0;
  for(i=0; i<n; ++i) {
    s_ = s + p[i];
    t = s_ - s;
    c += (s -  (s_ - t)) + (p[i] - t);
    s = s_;
  }
  return(s+c);
}

double Sum3(double *p, unsigned int n) {
  double s1, s2, s3, e, u, v;
  unsigned int i;
  
  s1 = s2 = s3 = 0.0;
  for(i=0; i<n; ++i) {
    u = s1 + p[i];
    v = u - s1;
    e = (s1 -  (u - v)) + (p[i] - v);
    s1 = u;
    
    u = s2 + e;
    v = u - s2;
    s3 += (s2 -  (u - v)) + (e - v);
    s2 = u;
  }
  u = s1 + s2;
  v = u - s1;
  s3 += (s1 -  (u - v)) + (s2 - v);
  return(u+s3);
}

double SumKIn(double *p, unsigned int n, unsigned int K) {
  unsigned int i, j;
  double v;
  
  for(i = 1; i < K; i++) {
    for(j = 1; j < n; j++) {
      TwoSumIn(p[j], p[j-1], p[j-1]);
    }
  }
  v = p[0]; for(j = 1; j < n; j++) v += p[j];
  return(v);
}

double SumKInBound(double *bound, double *p, unsigned int n, unsigned int K) {
  unsigned int i, j;
  double v, delta, beta;
  
  for(i = 1; i < K; i++) {
    for(j = 1; j < n; j++) {
      TwoSumIn(p[j], p[j-1], p[j-1]);
    }
  }
  v = p[0];
  beta = fabs(p[0]);
  for(j = 1; j < n-1; j++) {
    v += p[j];
    beta += fabs(p[j]);
  }
  TwoSumIn(v, delta, p[n-1]);
  beta = gam(2*n) * (beta + fabs(p[n-1]));
  delta = fabs(delta);
  *bound = delta+(beta+(EPS_DBL*delta+2*ETA_DBL));
  return(v);
}

double SumK(double *p, unsigned int n, unsigned int K) {
  double *q, r;
  
  q = (double *)malloc(sizeof(double)*n);
  memcpy(q, p, sizeof(double)*n);
  r = SumKIn(q, n, K);
  free(q);
  return(r);
}

double SumKvert(double *p, unsigned int n, unsigned int K) {
  double q[KMAX], e;
  int i, j;

  if( K > KMAX ) {
    fprintf(stderr, "WARNING, %s, %d: K > KMAX.\n", __FILE__, __LINE__);
    exit(-1);
  }

  for(i=0; i<K; i++) {
    e = p[i];
    for(j=0; j<i; j++) {
      TwoSumIn(q[j], e, e);
    }
    q[i] = e;
  }
  for(i=K; i<n; i++) {
    e = p[i];
    for(j=0; j<K-1; j++) {
      TwoSumIn(q[j], e, e);
    }
    q[K-1] += e;
  }
  for(i=0; i<K-2; i++) {
    e = q[i];
    for(j=i+1; j<K-1; j++) {
      TwoSumIn(q[j], e, e);
    }
    q[K-1] += e;
  }
  return(q[K-2] + q[K-1]);
}
/* ------------------------------------------------------------- */
double DDSum(double *p, unsigned int n) {
  double e = 0.0, s=p[0], s_l = 0.0, z, copy;
  unsigned int i;
  for (i=1; i<n; i++) {
    copy = s;
    s += p[i];
    z = s - copy;
    e = (copy - (s - z)) + (p[i] - z);
    //AddTwo(s, p[i], &s, &e);
    s_l +=e;
    
    copy = s;
    s = s + s_l;
    s_l = s_l - (s - copy);
    //		FastAddTwoSC(s, s_l, &s, &s_l);
  }
  return s;
}
/* ------------------------------------------------------------- */
double DDSum2(double *x, unsigned int n){
  double s, s_, t, t2, t3, t4, t5, e,  s_l=0.0;
  unsigned int i;
  s = x[0];
  for(i=1; i<n; i++){
    s_ = s;          //a
    s = s + x[i];    //b
    t = s - s_;      //c
    t2 = s - t;      //d
    t3 = x[i] - t;   //e
    t4 = s_ - t2;    //f
    t5 = t4 + t3;    //g
    s_l = s_l + t5;  //h
    s_ = s;          //i
    s = s + s_l;     //j
    e = s_ - s;      //k
    s_l = s_l + e;   //l
  }
  return s;
}
/* ------------------------------------------------------------- */
double DDSum3(double *x, unsigned int n){
  double s, s_, t, t2, t3, e,  s_l=0.0;
  unsigned int i;

  s = x[0];
  for(i=1; i<n; i++){
    s_ = s;          //a
    s = s + x[i];    //b
    t = s - s_;      //c
    t2 = s - t;      //d
    t3 = x[i] - t;   //e
    t2 = s_ - t2;    //f
    t3 = t2 + t3;    //g
    s_l = s_l + t3;  //h
    s_ = s;          //i
    s = s + s_l;     //j
    e = s_ - s;      //k
    s_l = s_l + e;   //l
  }
  return s;
}

/* ----------------------------------------------------------------- */
/* --- Accurate summation algorithm -------------------------------- */
/* ----------------------------------------------------------------- */

double AccSumIn(double *p, unsigned int n) {
  double mu, Ms, sigma, phi, factor;
  double q, t, tau, tau1, tau2, tt;
  int i;
      
  mu = fabs(p[0]);
  for(i=1; i<n; i++) {
    if( fabs(p[i]) > mu ) mu = fabs(p[i]); 
  }
  if(mu == 0.0) return(0.0);
  NextPowerTwo(Ms, n+2); /*Ms=NextPowerTwo(n+2)*/
  NextPowerTwo(sigma, mu); 
  sigma *= Ms; /*sigma=Ms*NextPowerTwo(mu)*/
  phi = URD_DBL*Ms;
  factor = URD_DBL*Ms*Ms;
  //  factor = EPS_DBL*Ms*Ms;

  t = 0.0;
  int m = 0;
  while(1) {
    //    printf("-- acc sum : mu=%g Ms=%g sigma=%g, phi=%g \n", mu, Ms, sigma, phi);
    m = m + 1;
    //printf(" --AccSum : m = %d \n", m);
    tau = 0.0;
    for(i=0; i<n; i++) {
      tt = (sigma + p[i]);
      q = tt - sigma;
      p[i] -= q; tau += q;
    }
    tau1 = t + tau;
     
    if((fabs(tau1) >= factor*sigma) || (sigma <= LBD_DBL)) {
      q = p[0]; for(i=1; i<n; i++) q += p[i];
      tau2 = tau - (tau1 - t);
      return(tau1 + (tau2 + q));
    }
    t = tau1;
    if(t == 0.0) {
      //      printf("\n AccSum : cancellation \n");
      return(AccSumIn(p, n));
    } 
    sigma = phi*sigma;
  }
  return(0.0);
}

double AccSum(double *p, unsigned int n) {
  double *q, r;
  
  q = (double *)malloc(sizeof(double)*n);
  memcpy(q, p, sizeof(double)*n);
  r = AccSumIn(q, n);
  free(q);
  return(r);
}

/* ----------------------------------------------------------------- */
/* --- Fast Accurate summation algorithm --------------------------- */
/* ----------------------------------------------------------------- */
double FastAccSum(double *p, unsigned int n) {
  double *q, r;
  
  q = (double *)malloc(sizeof(double)*n);
  memcpy(q, p, sizeof(double)*n);
  r = FastAccSumIn(q, n);
  free(q);
  return r;
}


double FastAccSumIn(double *p, unsigned int n) {
  double res=0,abssum=0.0,sum=0,t=0,t_prime=0;
  double sigma=0 , sigmaO=0 , sigma_prime=0 , T=0 , q=0 , tau=0, phi=0, tau2=0,u=0;
  double c9=ETA_DBL/URD_DBL;
  int i=0;
  
  //  printf("lim : %g", lim);
  for(i=0;i<n;i++){
    abssum += fabs(p[i]);
    //    printf("p(%i)= %g, abssum %g\n", i, p[i], abssum);
  }
  T=abssum/(1. - n*URD_DBL);
  //printf("T= %g\n",T);
  if ((T) <= (c9)){  //c9 dans .m Rump (????) 
    //    printf("    DOUTE   \n");
    for(i=0;i<n;i++)
      {	
	sum += p[i];
      }
    return sum;
  }
  //
  t_prime=0;
  int m = 0;
  while(1)
    {
      m = m + 1;
      //printf(" FAS m = %d\n", m);
      sigmaO = (double)(2.*T)/(1.-(3.*n+1.)*URD_DBL);
      sigma = sigmaO;
      //      printf("sigma %g, sigma0 %g \n", sigma, sigmaO);
      for(i=0;i<n;i++){
	sigma_prime = sigma + p[i]; 
	q = sigma_prime - sigma;
	p[i] = p[i] - q;
	sigma = sigma_prime;
      }
      tau = sigma_prime - sigmaO;
      //printf("tau=%g\n",tau);
      t = t_prime;
      t_prime = t + tau;
      if (t_prime==0) {
	//	printf("\n -- cancellation dans FastAccSum\n");
	return res=FastAccSumIn(p,n);
      }
      
      q = (double)(sigmaO/(2.*URD_DBL));
      u = (double)(fabs(q/(1.-URD_DBL) - q));
      phi = (double)((2*n*(n+2)*URD_DBL)*u)/(1.-5.*URD_DBL);
      T = (double)(fmin( ((1.5 + 4*URD_DBL)*(n*URD_DBL))*(sigmaO) ,  2*n*URD_DBL*u  ));

      //      printf("\n t prime : %g  phi : %g \n", t_prime, phi);

      
      if ((fabs(t_prime) >= phi) || (4*T <= c9)){
	tau2 = (t - t_prime) + tau;
	sum=0;
	for(i=0;i<n;i++)
	  {
	    sum+=p[i];
	  }
	res = t_prime + (tau2 +(sum));
	return res;
      }
    }
  //  printf("m=%d\n",m);
} 

/* ------------------------------------------------------------- */
typedef union{ double d; long int nd;} nb_d1;		
//en fait peut etre il serait mieux d'utiliser int64 mais faut voir si ca a un impact sur perform
//typedef struct t_n_double { unsigned signe: 1; int exposant: 11; unsigned m1: 26; unsigned m2: 26; } n_double;
//typedef union{ double d; n_double nd;} nb_d2;		
typedef union{ double d; int64_t nd;} nb_d3;		


#if defined __x86_64__
int exposant(double x) {	//renvoi exposant tel qu'il est code en machine
	nb_d1 temp;
	temp.d = x;
	return (((temp.nd)>>52)&0x7ff);//-1022; 
}
#else 

int exposant(double x) {
	nb_d3 temp;
	temp.d = x;
	return (((temp.nd)>>52)&0x7ff);
}
#endif	
/* ------------------------------------------------------------- */
double ulp(double x) {
	double r = pow(2, exposant(x)-1022 - 53);
	if ((isnan(r)) || (!isfinite(r))) return 0;
	else return r;
}
/* ------------------------------------------------------------- */
double half_ulp(double x) {
	double r = pow(2, exposant(x)-1022 - 54);
	if ((isnan(r)) || (!isfinite(r))) return 0;
	return r;
}
/* ------------------------------------------------------------- */
int Sign(double s) {
	if (s>0.0) return 1;
	else if (s==0.0) return 0;
	else return -1;
}
/* ------------------------------------------------------------- */
double Magnify(double s1) {
	nb_d1 cp;
	cp.d = s1;
	cp.nd = (cp.nd)|1;
	return cp.d;
}
/* ------------------------------------------------------------- */
unsigned int is_half_ulp(double x) {
	nb_d1 cp;
	cp.d = x;
	if ((cp.nd&0x000fffffffffffff) == 0) return 1;
	return 0;
}
/* ------------------------------------------------------------- */
double Round3(double s0, double s1, double s2) {
	//rounds s0+s1+s2 where fl(s0+s1) = s0 and fl(s1|s2)=s1
	if (((Sign(s1)==Sign(s2)))  && (is_half_ulp(s1)))
		return Magnify(s1)+s0;
	return s0;
}
/* ------------------------------------------------------------- */

/* ------------------------------------------------------------- */
double iFastSumIn(double *p, unsigned int n) {
  double s = 0.0, s_temp, s_m, s1, s2, e_m, w1, e1, w2, e2, z, copy, sum;
  static unsigned int rc = 0;
  unsigned int count, i, loop=1;
  for (i=0; i<n; i++) {	//accumulate first approximation
    sum = s + p[i];
    //		AddTwo(s, p[i], &s, &p[i]); 	
    copy = s;
    s += p[i];
    z = s - copy;
    p[i] = (copy - ( s - z)) + (p[i] - z);
  }
  //  printf("first approximation: %.16e\n", s);	
		
  while (1) {		//main loop
    count = 0;
    s_temp = 0.0;  //temporary sum
    s_m = 0;
    loop++;
    //        printf("-- loop %i ---\n", loop);
    for (i=0; i<n; i++) {
      //printf("loop=%i, %.16e\n", loop, p[i]);
      
      //(s_t, p[count]) = AddTwo(s_t, p[i])
      copy = s_temp;
      s_temp += p[i];
      z = s_temp - copy;
      p[count] = (copy - ( s_temp - z)) + (p[i] - z);
      
      if (p[count]!=0) {
	count++;
	s_m = (s_m>fabs(s_temp)?s_m:fabs(s_temp));
      }
    }
    e_m = count*half_ulp(s_m); //weak upper bound on magnitude of the sum of the errors
    // (s, s_t) = AddTwo(s, s_t)
    copy = s;
    s += s_temp;
    z = s - copy;
    s_temp = (copy - ( s - z)) + (s_temp - z); 
    // s has been corrected by s_temp, new s_temp is the residual error
   
    p[count] = s_temp; //pas vu dans KP
    n = count+1; // a verifier

    //printf("loop: %i, n (nb err/=0): %2i, half_ulp(s): %e, error upper bound e_m : %e\n", loop, n, half_ulp(s), e_m);
    //printf("correction %i: %.16e\n", loop, s);	
    //printf("local error: p[count]=%.16e\n", s_temp);

    if ((e_m==0) || (e_m<half_ulp(s))) {
      //printf("analyse erreur residuelle: e_m=%.16e, half_up_s=%.16e\n", e_m, half_ulp(s));
      if (rc>0) {
	//printf("sortie derniere passe recursive\n");
	rc = 0;
	return s;
      }
      //AddTwo(s_temp, e_m, &w1, &e1); 			
      w1 = s_temp + e_m;
      z = w1 - s_temp;
      e1 = (s_temp - ( w1 - z)) + (e_m - z);

      //AddTwo(s_temp, -e_m, &w2, &e2);
      w2 = sum;
      w2 = s_temp + (-e_m);
      z = w2 - s_temp;
      e2 = (s_temp - ( w2 - z)) + ((-e_m) - z);
      
      //printf("w1:%.5e, e1:%.5e, w2:%.5e, e2:%.5e\n", w1, e1, w2, e2);

      if (((w1+s)!= s) || ((w2+s)!=s) || (Round3(s, w1, e1)!=s) || (Round3(s, w2, e2)!=s)){
	
	//printf("recursive iFS1\n");
	//for (int i=0; i<n; i++)
	  //printf("%e\n", p[i]);
	  //printf("----\n");
	rc = 1;
	s1 = iFastSumIn(p, n);
	
	//AddTwo(s, s1, &s, &s1);
	copy = s;
	s += s1;
	z = s - copy;
	s1 = (copy - ( s - z)) + (s1 - z);
	
	//printf("recursive iFS2\n");
	s2 = iFastSumIn(p, n);

	//printf("round3\n---\n");
	rc = 0;
	s = Round3(s, s1, s2);
      }
      //	printf("loop: %d\n", loop);
      return s;		
    }
  }
}
/* ------------------------------------------------------------- */
/* // version avec parametre de controle de la recursion : 1 si appel recursif, O sinon */
/* double iFastSumRec(double *p, unsigned int n, unsigned int rc) { */
/*   double s = 0.0, s_temp, s_m, s1, s2, e_m, w1, e1, w2, e2, z, copy, sum,b; */
/*   //  static unsigned int rc = 0; */
/*   unsigned int count, i, loop=1; */
/*   for (i=0; i<n; i++) {	//accumulate first approximation */
/*     sum = s + p[i]; */
/*     //		AddTwo(s, p[i], &s, &p[i]); 	 */
/*     copy = s; */
/*     s += p[i]; */
/*     z = s - copy; */
/*     p[i] = (copy - ( s - z)) + (p[i] - z); */
/*   } */
/*   printf("first approximation: %.16e\n", s);	 */
		
/*   while (1) {		//main loop */
/*     count = 0; */
/*     s_temp = 0.0;  //temporary sum */
/*     s_m = 0; */
/*     loop++; */
/*     printf("-- loop %i ---\n", loop); */
/*     for (i=0; i<n; i++) { */
/*       printf("loop=%i, %.16e\n", loop, p[i]); */
/*       //(s_t, p[count]) = AddTwo(s_t, p[i]) */
/*       copy = s_temp; */
/*       s_temp += p[i]; */
/*       z = s_temp - copy; */
/*       p[count] = (copy - ( s_temp - z)) + (p[i] - z); */
      
/*       if (p[count]!=0) { */
/* 	count++; */
/* 	s_m = (s_m>fabs(s_temp)?s_m:fabs(s_temp)); */
/*       } */
/*     } */
/*     e_m = count*half_ulp(s_m); //weak upper bound on magnitude of the sum of the errors */
/*     // (s, s_t) = AddTwo(s, s_t) */
/*     copy = s; */
/*     s += s_temp; */
/*     z = s - copy; */
/*     s_temp = (copy - ( s - z)) + (s_temp - z);  */
/*     // s has been corrected by s_temp, new s_temp is the residual error */
   
/*     p[count] = s_temp; //pas vu dans KP */
/*     n = count+1; // a verifier */

/*     printf("loop: %i, n (nb err/=0): %2i, half_ulp(s): %e, error upper bound e_m : %e\n", loop, n, half_ulp(s), e_m); */
/*     printf("correction %i: %.16e\n", loop, s);	 */
/*     printf("local error: p[count]=%.16e\n", s_temp); */

/*     if ((e_m==0) || (e_m<half_ulp(s))) { */
/*       printf("analyse erreur residuelle: e_m=%.16e, half_up_s=%.16e\n", e_m, half_ulp(s)); */
/*       if (rc>0) { */
/* 	printf("sortie derniere passe recursive\n"); */
/* 	//	rc=0; */
/* 	return s; */
/*       } */
/*       //AddTwo(s_temp, e_m, &w1, &e1); 			 */
/*       w1 = s_temp + e_m; */
/*       z = w1 - s_temp; */
/*       e1 = (s_temp - ( w1 - z)) + (e_m - z); */

/*       //AddTwo(s_temp, -e_m, &w2, &e2); */
/*       w2 = sum; */
/*       w2 = s_temp + (-e_m); */
/*       z = w2 - s_temp; */
/*       e2 = (s_temp - ( w2 - z)) + ((-e_m) - z); */
      
/*       printf("w1:%.5e, e1:%.5e, w2:%.5e, e2:%.5e\n", w1, e1, w2, e2); */

/*       if (((w1+s)!= s) || ((w2+s)!=s) || (Round3(s, w1, e1)!=s) || (Round3(s, w2, e2)!=s)) */
/* 	{ */
/* 	rc=1; */

/* 	printf("recursive iFS1\n"); */
/* 	for (int i=0; i<n; i++) */
/* 	  printf("%e\n", p[i]); */
/* 	printf("----\n"); */

/* 	s1 = iFastSumRec(p, n, 1); */

/* 	//AddTwo(s, s1, &s, &s1); */
/* 	copy = s; */
/* 	s += s1; */
/* 	z = s - copy; */
/* 	s1 = (copy - ( s - z)) + (s1 - z); */

/* 	printf("recursive iFS2\n"); */
/* 	s2 = iFastSumRec(p, n, 1); */

/* 	//rc=0; */
/* 	printf("round3\n---\n"); */
/* 	s = Round3(s, s1, s2); */
/*       } */
/*       //	printf("loop: %d\n", loop); */
/*       return s;		 */
/*     } */
/*   } */
/* } */
/* ------------------------------------------------------------- */
//double iFastSumIn(double *p, unsigned int n) {
//  double r;
//  r = iFastSumRec(p, n, 0); //
//  return r;
//}
/* ------------------------------------------------------------- */
double iFastSum(double *p, unsigned int n) {
  double *q, r;
  q = (double *)malloc(sizeof(double)*n);
  memcpy(q, p, sizeof(double)*n);
  r = iFastSumIn(q, n);
  free(q);
  return r;
}
/* ------------------------------------------------------------- */
double HybridSum(double *p, unsigned int n) {
  const unsigned int N = 1<<26; //base (2) a la puissance (partie entiere inferieure(longeur mantisse/2))
  const unsigned int b_l = 1<<11; //base (2) a la puissance (longeur de l'exposant)
  //const unsigned int b_l_m1 = 1<<10; // biais + 1 
  double *a1, *a2, *temp, hi, lo, r, ashift, t;
  a1 = (double *)malloc(sizeof(double)*b_l);
  a2 = (double *)malloc(sizeof(double)*b_l);
  memset(a1, 0, sizeof(double)*b_l);		//set all # in a1 to 0
  unsigned int  m=n<N?n:N, k, z;	// m -> si trop des nombres a sommer, il seront 'decoupes' en plusiers 'lots', m etant la taille de ce 'lot'
  int j;
  while (1) {
    for (k=0; k<m; k++) {
      ashift = p[k] * _splitter_;		//split p[k] into two #, each with 1/2 of it's mantissa
      t = ashift - p[k];
      hi = ashift - t;
      lo = p[k] - hi;
      //i++;		
      j = exposant(hi);  //add two split parts to corresponding accumulator a1[j]
      //printf("k=%d hi=%g j=%d",k,hi,j);
      a1[j] += hi;
      j = exposant(lo);
      //      printf("lo=%g j=%d hi-lo=%g \n",lo,j,hi-lo);
      a1[j] += lo;
    }
    n -=m;
    if (n==0) goto exit_boucle;	
    memset(a2, 0, sizeof(double)*b_l);			//set all # in a2 to 0
    for (k=0; k<b_l; k++) {
      ashift = a1[k] * _splitter_;
      t = ashift - a1[k];
      hi = ashift - t;
      lo = a1[k] - hi;
      j = exposant(hi);  //add two split parts to corresponding accumulator a1[j]
      a2[j] += hi;
      j = exposant(lo);
      a2[j] += lo;				
    }
    temp = a1;	//swap arrays
    a1 = a2;
    a2 = temp;
    z = N - b_l;
    m = n<z?n:z;
  }
 exit_boucle:
  r = iFastSum(a1, b_l);
  free(a1); free(a2);
  return r;
}

/* ------------------------------------------------------------- */
double VHybridSum(double *p, unsigned int n) {
  const unsigned int N = 1<<26; //base (2) a la puissance (partie entiere inferieure(longeur mantisse/2))
  const unsigned int b_l = 1<<11; //base (2) a la puissance (longeur de l'exposant)
  //const unsigned int b_l_m1 = 1<<10; // biais + 1 
  double *a1, *a2, *temp, hi, lo, r, ashift, t;
  a1 = (double *)malloc(sizeof(double)*b_l);
  a2 = (double *)malloc(sizeof(double)*b_l);
  memset(a1, 0, sizeof(double)*b_l);		//set all # in a1 to 0
  unsigned int i = 0, m=n<N?n:N, k, z;	// m -> si trop des nombres a sommer, il seront 'decoupes' en plusiers 'lots', m etant la taille de ce 'lot'
  int j;
  int jhi1,jhi2;
  int jlo1,jlo2;

  __m128d ashift_m128;
  __m128d pk_m128;
  __m128d _splitter__m128;
  __m128d hi_m128;
  __m128d lo_m128;
  __m128d t_m128;
  __m128i and_m128;

  double tmphi;
  double tmplo;
  /* __m128d tmpha12_m128; */
  /* __m128d tmpla12_m128; */

  //  and_m128 = _mm_set_epi64(0x7ff,0x7ff);
  and_m128 = _mm_set_epi32(0x0, 0x7ff, 0x0, 0x7ff);
  union { __m128i i; __m128d d; } j_m128;

  _splitter__m128 = _mm_set_pd(134217729.0, 134217729.0);

  while (1) {
    for (k=0; k<m-1; k=k+2) {
      pk_m128 = _mm_load_pd(&p[k]); // Load p[k]|p[k+1]
      //      ashift_m128 = pk_m128 * _splitter__m128;		//split p[k] into two #, each with 1/2 of it's mantissa
      ashift_m128 = _mm_mul_pd(pk_m128, _splitter__m128);
      //      t_m128 = ashift_m128 - pk_m128;
      t_m128 = _mm_sub_pd(ashift_m128, pk_m128);
      //      hi_m128 = ashift_m128 - t_m128;
      hi_m128 = _mm_sub_pd(ashift_m128, t_m128);
      //      lo_m128 = pk_m128 - hi_m128;
      lo_m128 = _mm_sub_pd(pk_m128, hi_m128);
      i += 2;		
      //      j = exposant(hi);  //add two split parts to corresponding accumulator a1[j]
      //	return (((temp.nd)>>52)&0x7ff);//-1022; 
      j_m128.d = hi_m128;
      j_m128.i = _mm_srli_epi64(j_m128.i, 52);
      j_m128.i = _mm_and_si128(j_m128.i, and_m128);

      //      j1 = _mm_extract_epi32(j_m128.i,0);
      //      j2 = _mm_extract_epi32(j_m128.i,2);
      jhi1 = _mm_cvtsi128_si32(j_m128.i);
      jhi2 = _mm_cvtsi128_si32(_mm_unpackhi_epi64(j_m128.i,j_m128.i));

      //      a1[j] += hi;
      ///////////////////////////////////////      _mm_store_pd(&tmphi[0], hi_m128);
      /* a1[j1] += tmpa[0]; */
      /* a1[j2] += tmpa[1]; */

      //      j = exposant(lo);
      j_m128.d = lo_m128;
      j_m128.i = _mm_srli_epi64(j_m128.i, 52);
      j_m128.i = _mm_and_si128(j_m128.i, and_m128);

      /* j1 = _mm_extract_epi32(j_m128.i,0); */
      /* j2 = _mm_extract_epi32(j_m128.i,2); */
      jlo1 = _mm_cvtsi128_si32(j_m128.i);
      jlo2 = _mm_cvtsi128_si32(_mm_unpackhi_epi64(j_m128.i,j_m128.i));

      //      a1[j] += lo;
      //      _mm_store_pd(&tmplo[0], lo_m128);
      /* a1[j1] += tmpa[0]; */
      /* a1[j2] += tmpa[1]; */

      _mm_storel_pd(&tmphi, hi_m128);
      _mm_storel_pd(&tmplo, lo_m128);
      a1[jhi1] += tmphi;
      a1[jlo1] += tmplo;

      _mm_storeh_pd(&tmphi, hi_m128);
      _mm_storeh_pd(&tmplo, lo_m128);
      a1[jhi2] += tmphi;
      a1[jlo2] += tmplo;

      /* tmpha12_m128 = _mm_loadl_pd(tmpha12_m128, &a1[jhi1]); */
      /* tmpha12_m128 = _mm_loadh_pd(tmpha12_m128, &a1[jhi2]); */

      /* tmpha12_m128 = _mm_add_pd(tmpha12_m128, hi_m128); */

      /* _mm_storel_pd(&a1[jhi1], tmpha12_m128); */
      /* _mm_storeh_pd(&a1[jhi2], tmpha12_m128); */

      /* a1[jhi2] += tmphi[1]; */
      /* a1[jlo2] += tmplo[1]; */

      /* _mm_storeh_pd(&a1[jhi2], hi_m128); */
      /* _mm_storeh_pd(&a1[jlo2], lo_m128); */

      /* tmpla12_m128 = _mm_loadl_pd(tmpla12_m128, &a1[jlo1]); */
      /* tmpla12_m128 = _mm_loadh_pd(tmpla12_m128, &a1[jlo2]); */

      /* tmpla12_m128 = _mm_add_pd(tmpla12_m128, lo_m128); */

      /* _mm_storel_pd(&a1[jlo1], tmpla12_m128); */
      /* _mm_storeh_pd(&a1[jlo2], tmpla12_m128); */


    }
    for (; k<m; k=k+1) {
      ashift = p[k] * _splitter_;		//split p[k] into two #, each with 1/2 of it's mantissa
      t = ashift - p[k];
      hi = ashift - t;
      lo = p[k] - hi;
      i++;		
      j = exposant(hi);  //add two split parts to corresponding accumulator a1[j]
      a1[j] += hi;
      j = exposant(lo);
      a1[j] += lo;
    }
    n -=m;
    if (n==0) goto exit_boucle;	
    memset(a2, 0, sizeof(double)*b_l);			//set all # in a2 to 0
    for (k=0; k<b_l; k++) {
      ashift = a1[k] * _splitter_;
      t = ashift - a1[k];
      hi = ashift - t;
      lo = a1[k] - hi;
      j = exposant(hi);  //add two split parts to corresponding accumulator a1[j]
      a2[j] += hi;
      j = exposant(lo);
      a2[j] += lo;				
    }
    temp = a1;	//swap arrays
    a1 = a2;
    a2 = temp;
    z = N - b_l;
    m = n<z?n:z;
  }
 exit_boucle:
  r = iFastSum(a1, b_l);
  free(a1); free(a2);
  return r;
}

/* ------------------------------------------------------------- */
//double V2HybridSum(double *p, unsigned int n) {
  /* const unsigned int N = 1<<26; //base (2) a la puissance (partie entiere inferieure(longeur mantisse/2)) */
  /* const unsigned int b_l = 1<<11; //base (2) a la puissance (longeur de l'exposant) */
  /* //const unsigned int b_l_m1 = 1<<10; // biais + 1  */
  /* double *a1, *a2, *temp, hi, lo, r, ashift, t; */
  /* a1 = (double *)malloc(sizeof(double)*b_l); */
  /* a2 = (double *)malloc(sizeof(double)*b_l); */
  /* memset(a1, 0, sizeof(double)*b_l);		//set all # in a1 to 0 */

  /* __m128d *a1_m128; */
  /* __m128d *a2_m128; */

 /*  a1_m128 = (__m128 *)aligned_malloc(sizeof(__m128)*b_l);  */
 /*  a2_m128 = (__m128 *)aligned_malloc(sizeof(__m128)*b_l);  */

 /*  unsigned int i = 0, m=n<N?n:N, k, z;	// m -> si trop des nombres a sommer, il seront 'decoupes' en plusiers 'lots', m etant la taille de ce 'lot' */
 /*  int j; */
 /*  int jhi1,jhi2; */
 /*  int jlo1,jlo2; */

 /*  __m128d ashift_m128; */
 /*  __m128d pk_m128; */
 /*  __m128d _splitter__m128; */
 /*  __m128d hi_m128; */
 /*  __m128d lo_m128; */
 /*  __m128d t_m128; */
 /*  __m128i and_m128; */

 /*  double tmphi; */
 /*  double tmplo; */
 /*  and_m128 = _mm_set_epi32(0x0, 0x7ff, 0x0, 0x7ff); */
 /*  union { __m128i i; __m128d d; } j_m128; */

 /*  _splitter__m128 = _mm_set_pd(134217729.0, 134217729.0); */

 /*  while (1) { */
 /*    for (k=0; k<m-1; k=k+2) { */
 /*      pk_m128 = _mm_load_pd(&p[k]); // Load p[k]|p[k+1] */
 /*      //      ashift_m128 = pk_m128 * _splitter__m128;		//split p[k] into two #, each with 1/2 of it's mantissa */
 /*      ashift_m128 = _mm_mul_pd(pk_m128, _splitter__m128); */
 /*      //      t_m128 = ashift_m128 - pk_m128; */
 /*      t_m128 = _mm_sub_pd(ashift_m128, pk_m128); */
 /*      //      hi_m128 = ashift_m128 - t_m128; */
 /*      hi_m128 = _mm_sub_pd(ashift_m128, t_m128); */
 /*      //      lo_m128 = pk_m128 - hi_m128; */
 /*      lo_m128 = _mm_sub_pd(pk_m128, hi_m128); */
 /*      i += 2;		 */
 /*      //      j = exposant(hi);  //add two split parts to corresponding accumulator a1[j] */
 /*      //	return (((temp.nd)>>52)&0x7ff);//-1022;  */
 /*      j_m128.d = hi_m128; */
 /*      j_m128.i = _mm_srli_epi64(j_m128.i, 52); */
 /*      j_m128.i = _mm_and_si128(j_m128.i, and_m128); */

 /*      jhi1 = _mm_cvtsi128_si32(j_m128.i); */
 /*      jhi2 = _mm_cvtsi128_si32(_mm_unpackhi_epi64(j_m128.i,j_m128.i)); */

 /*      //      j = exposant(lo); */
 /*      j_m128.d = lo_m128; */
 /*      j_m128.i = _mm_srli_epi64(j_m128.i, 52); */
 /*      j_m128.i = _mm_and_si128(j_m128.i, and_m128); */

 /*      jlo1 = _mm_cvtsi128_si32(j_m128.i); */
 /*      jlo2 = _mm_cvtsi128_si32(_mm_unpackhi_epi64(j_m128.i,j_m128.i)); */

 /*      //      a1[j] += hi; */
 /*      //      a1[j] += lo; */
 /*      _mm_storel_pd(&tmphi, hi_m128); */
 /*      _mm_storel_pd(&tmplo, lo_m128); */
 /*      a1[jhi1] += tmphi; */
 /*      a1[jlo1] += tmplo; */

 /*      _mm_storeh_pd(&tmphi, hi_m128); */
 /*      _mm_storeh_pd(&tmplo, lo_m128); */
 /*      a1[jhi2] += tmphi; */
 /*      a1[jlo2] += tmplo; */

 /*    } */
 /*    for (; k<m; k=k+1) { */
 /*      ashift = p[k] * _splitter_;		//split p[k] into two #, each with 1/2 of it's mantissa */
 /*      t = ashift - p[k]; */
 /*      hi = ashift - t; */
 /*      lo = p[k] - hi; */
 /*      i++;		 */
 /*      j = exposant(hi);  //add two split parts to corresponding accumulator a1[j] */
 /*      a1[j] += hi; */
 /*      j = exposant(lo); */
 /*      a1[j] += lo; */
 /*    } */
 /*    n -=m; */
 /*    if (n==0) goto exit_boucle;	 */
 /*    memset(a2, 0, sizeof(double)*b_l);			//set all # in a2 to 0 */
 /*    for (k=0; k<b_l; k++) { */
 /*      ashift = a1[k] * _splitter_; */
 /*      t = ashift - a1[k]; */
 /*      hi = ashift - t; */
 /*      lo = a1[k] - hi; */
 /*      j = exposant(hi);  //add two split parts to corresponding accumulator a1[j] */
 /*      a2[j] += hi; */
 /*      j = exposant(lo); */
 /*      a2[j] += lo;	 */
 /*    } */
 /*    temp = a1;	//swap arrays */
 /*    a1 = a2; */
 /*    a2 = temp; */
 /*    z = N - b_l; */
 /*    m = n<z?n:z; */
 /*  } */
 /* exit_boucle: */
 /*  r = iFastSum(a1, b_l); */
 /*  free(a1); free(a2); */
 /*  return r; */
//  return 0.0;
//}


/* ------------------------------------------------------------- */
/* Original version from Zhu-Hayes paper */
/* ------------------------------------------------------------- */
/* double OnlineExactSum(double *p, unsigned int n) { */
/*   int N = 1<<26; */
/*   int b_l = 1<<11; //base (2) a la puissance (longeur de l'exposant)  */
/*   //	int b_l_m1 = 1<<10;  */
/*   double *a, *a1, *a2, *b1, *b2, *temp, r, z, copy; */
/*   a1 = (double *)malloc(sizeof(double)*b_l); */
/*   a2 = (double *)malloc(sizeof(double)*b_l); */
/*   b1 = (double *)malloc(sizeof(double)*b_l); */
/*   b2 = (double *)malloc(sizeof(double)*b_l); */
/*   a = (double *)malloc(sizeof(double)*b_l*2); */
/*   a1 = memset(a1, 0, sizeof(double)*b_l);	//initialize all the numbers in a1 and a2 to be zero */
/*   a2 = memset(a2, 0, sizeof(double)*b_l);	//initialize all the numbers in a1 and a2 to be zero */
/*   unsigned int i = 0, k, l, count = 0; */
/*   int j; */
/*   for (k=0; k<n; k++) { */
/*     j = exposant(p[k]);  */

/*     // TwoSum */
/*     copy = a1[j]; */
/*     a1[j] += p[k]; */
/*     z = a1[j] - copy; */
/*     a2[j] += (copy - (a1[j] - z)) + (p[k] - z); */

/*     /\* // FastTwoSum BUGGED *\/ */
/*     /\* copy = a1[j]; *\/ */
/*     /\* a1[j] += p[k]; *\/ */
/*     /\* int tmp; *\/ */
/*     /\* if (fabs(copy) > fabs(p[k])) *\/ */
/*     /\*   { tmp = p[k]; p[k]= copy; copy=tmp; } *\/ */
/*     /\* z = a1[j] - copy; *\/ */
/*     /\* //    a2[j] += (copy - (a1[j] - z)) + (p[k] - z); *\/ */
/*     /\* a2[j] += p[k] - z; *\/ */

/*     i++; */


/*     if (i>=N) {	 */
/*       b1 = memset(b1, 0, sizeof(double)*b_l); */
/*       b2 = memset(b2, 0, sizeof(double)*b_l); */
/*       for (l = 0; l<b_l; l++) {		//for each number y in a1 and a2 */
/* 	j = exposant(a1[l]); */
/* 	copy = b1[j]; */
/* 	b1[j] += a1[l]; */
/* 	z = b1[j] - copy; */
/* 	b2[j] += (copy - (b1[j] - z)) + (a1[l] - z); */
/*       } */
/*       for (l = 0; l<b_l; l++) {		 */
/* 	j = exposant(a2[l]); */
/* 	copy = b1[j]; */
/* 	b1[j] += a2[l]; */
/* 	z = b1[j] - copy; */
/* 	b2[j] += (copy - (b1[j] - z)) + (a2[l] - z); */
/*       }					//end for each ... */
/*       temp = a1;		//swap array pointers a1 and a2 with b1 and b2 respectively */
/*       a1 = b1; */
/*       b1 = temp; */
/*       temp = a2;		 */
/*       a2 = b2; */
/*       b2 = temp; */
/*       i = b_l<<1; */
/*     } */
/*   }	 */
/*   for (l = 0; l<b_l; l++) {	//concatenate a1 and a2 to be a and remove zeros */
/*     if (a1[l]!=0.0) { a[count]= a1[l]; count++;} */
/*   } */
/*   for (l = 0; l<b_l; l++) {	 */
/*     if (a2[l]!=0.0) { a[count]= a2[l]; count++;} */
/*   } */

/*   // observation cond(a) */
/*   /\* double sum_abs = 0.0; *\/ */
/*   /\* for (l=0; l<count; l++) { *\/ */
/*   /\*   sum_abs += fabs(a[l]);} *\/ */
/*   //  printf("OLE: appel iFastSum\n"); */
  
/*   r = iFastSum(a,  count); */
  
/*   //  printf("OLE : count=%i, cond[a]= %e\n", count, sum_abs/fabs(r)); */
  
/*   free(a); free(a1); free(a2); free(b1); free(b2);		 */
/*   return r;} */
  

/* ------------------------------------------------------------- */
/* Modified version closer to Zhu-Hayes' HybridSum               */
/* Sum length n is now an explicit parameter  
/* ------------------------------------------------------------- */
double OnlineExactTwoSum(double *p, unsigned int n) {
  int N = 1<<26;
  int b_l = 1<<11; //base (2) a la puissance (longeur de l'exposant) 
  //	int b_l_m1 = 1<<10; 
  double *a, *a1, *a2, *b1, *b2, *temp, r, z, copy;
  a1 = (double *)malloc(sizeof(double)*b_l);
  a2 = (double *)malloc(sizeof(double)*b_l);
  b1 = (double *)malloc(sizeof(double)*b_l);
  b2 = (double *)malloc(sizeof(double)*b_l);
  a = (double *)malloc(sizeof(double)*b_l*2);
  a1 = memset(a1, 0, sizeof(double)*b_l);	//initialize all the numbers in a1 and a2 to be zero
  a2 = memset(a2, 0, sizeof(double)*b_l);	//initialize all the numbers in a1 and a2 to be zero
  unsigned int i = 0, k, l, count = 0;
  //  unsigned int i = 0;
  int j;
  int m = n<N?n:N;
  k = 0;
  while(i<n)
    {
      //      for (k=0; k<m; k++) {
      for (; i<m; i++) {
	j = exposant(p[k]); 
	
	copy = a1[j];
	a1[j] += p[k];
	z = a1[j] - copy;
	a2[j] += (copy - (a1[j] - z)) + (p[k] - z);
	k++;
	//	i++;
      }
      if (i>=N) {	
	b1 = memset(b1, 0, sizeof(double)*b_l);
	b2 = memset(b2, 0, sizeof(double)*b_l);
	for (l = 0; l<b_l; l++) {		//for each number y in a1 and a2
	  j = exposant(a1[l]);
	  copy = b1[j];
	  b1[j] += a1[l];
	  z = b1[j] - copy;
	  b2[j] += (copy - (b1[j] - z)) + (a1[l] - z);
	}
	for (l = 0; l<b_l; l++) {		
	  j = exposant(a2[l]);
	  copy = b1[j];
	  b1[j] += a2[l];
	  z = b1[j] - copy;
	  b2[j] += (copy - (b1[j] - z)) + (a2[l] - z);
	}					//end for each ...
	temp = a1;		//swap array pointers a1 and a2 with b1 and b2 respectively
	a1 = b1;
	b1 = temp;
	temp = a2;		
	a2 = b2;
	b2 = temp;
	i = b_l<<1;
      }
    }	
  for (l = 0; l<b_l; l++) {	//concatenate a1 and a2 to be a and remove zeros
    if (a1[l]!=0.0) { a[count]= a1[l]; count++;}
  }
  for (l = 0; l<b_l; l++) {	
    if (a2[l]!=0.0) { a[count]= a2[l]; count++;}
  }

  // observation cond(a)
  /* double sum_abs = 0.0; */
  /* for (l=0; l<count; l++) { */
  /*   sum_abs += fabs(a[l]);} */
  //  printf("OLE: appel iFastSum\n");
  
  r = iFastSum(a,  count);
  
  //  printf("OLE : count=%i, cond[a]= %e\n", count, sum_abs/fabs(r));
  
  free(a); free(a1); free(a2); free(b1); free(b2);		
  return r;}

/* ------------------------------------------------------------- */
double OnlineExactFastTwoSum(double *p, unsigned int n) {
  int N = 1<<26;
  int b_l = 1<<11; //base (2) a la puissance (longeur de l'exposant) 
  //	int b_l_m1 = 1<<10; 
  double *a, *a1, *a2, *b1, *b2, *temp;
  double r, z, c, copy;
  a1 = (double *)malloc(sizeof(double)*b_l);
  a2 = (double *)malloc(sizeof(double)*b_l);
  b1 = (double *)malloc(sizeof(double)*b_l);
  b2 = (double *)malloc(sizeof(double)*b_l);
  a = (double *)malloc(sizeof(double)*b_l*2);
  a1 = memset(a1, 0, sizeof(double)*b_l);	//initialize all the numbers in a1 and a2 to be zero
  a2 = memset(a2, 0, sizeof(double)*b_l);	//initialize all the numbers in a1 and a2 to be zero
  unsigned int i = 0, k, l, count = 0;
  //  unsigned int i = 0;
  int j;
  int m = n<N?n:N;
  k = 0;
  while(i<n)
    {
      //      for (k=0; k<m; k++) {
      for (; i<m; i++) {
	j = exposant(p[k]); 
	c = a1[j];
	if (fabs(c)>fabs(p[k]))
	  {
	    a1[j] += p[k];
	    z = a1[j] - c;
	    a2[j] += p[k] - z;
	  } 
	else 
	  {   
	    a1[j] += p[k];
	    z = a1[j] - p[k];
	    a2[j] += c - z;
	  }
	k++;
	//	i++;
      }
	
	if (i>=N) {	
	  b1 = memset(b1, 0, sizeof(double)*b_l);
	  b2 = memset(b2, 0, sizeof(double)*b_l);
	  for (l = 0; l<b_l; l++) {		//for each number y in a1 and a2
	    j = exposant(a1[l]);
	    copy = b1[j];
	    b1[j] += a1[l];
	    z = b1[j] - copy;
	    b2[j] += (copy - (b1[j] - z)) + (a1[l] - z);
	  }
	  for (l = 0; l<b_l; l++) {		
	    j = exposant(a2[l]);
	    copy = b1[j];
	    b1[j] += a2[l];
	    z = b1[j] - copy;
	    b2[j] += (copy - (b1[j] - z)) + (a2[l] - z);
	  }					//end for each ...
	  temp = a1;		//swap array pointers a1 and a2 with b1 and b2 respectively
	  a1 = b1;
	  b1 = temp;
	  temp = a2;		
	  a2 = b2;
	  b2 = temp;
	  i = b_l<<1;
	}
      }	
      for (l = 0; l<b_l; l++) {	//concatenate a1 and a2 to be a and remove zeros
	if (a1[l]!=0.0) { a[count]= a1[l]; count++;}
      }
      for (l = 0; l<b_l; l++) {	
	if (a2[l]!=0.0) { a[count]= a2[l]; count++;}
      }
      
  // observation cond(a)
  /* double sum_abs = 0.0; */
  /* for (l=0; l<count; l++) { */
  /*   sum_abs += fabs(a[l]);} */
  //  printf("OLE: appel iFastSum\n");
  
  r = iFastSum(a,  count);
  
  //  printf("OLE : count=%i, cond[a]= %e\n", count, sum_abs/fabs(r));
  
  free(a); free(a1); free(a2); free(b1); free(b2);		
  return r;}
  
/* ------------------------------------------------------------- */
double VOnlineExactSum(double *p, unsigned int n) {
  int N = 1<<26;
  int b_l = 1<<11; //base (2) a la puissance (longeur de l'exposant) 
  //	int b_l_m1 = 1<<10; 
  double *a, *a1, *a2, *b1, *b2, *temp, r, z, copy;
  a1 = (double *)malloc(sizeof(double)*b_l);
  a2 = (double *)malloc(sizeof(double)*b_l);
  b1 = (double *)malloc(sizeof(double)*b_l);
  b2 = (double *)malloc(sizeof(double)*b_l);
  a = (double *)malloc(sizeof(double)*b_l*2);
  a1 = memset(a1, 0, sizeof(double)*b_l);	//initialize all the numbers in a1 and a2 to be zero
  a2 = memset(a2, 0, sizeof(double)*b_l);	//initialize all the numbers in a1 and a2 to be zero
  unsigned int i = 0, k, l, count = 0;
  int j;
  int j1,j2;

  union { __m128i i; __m128d d; } j_m128;

  __m128d pk_m128;
  __m128d copy_m128;
  __m128d a1_j1_j2_m128;
  __m128d a2_j1_j2_m128;
  __m128d z_m128;
  __m128i and_m128;

  double tmpa1[2];
  double tmpa2[2];

  and_m128 = _mm_set_epi32(0x0, 0x7ff, 0x0, 0x7ff);

  for (k=0; k<n-1; k=k+2) {
    j = exposant(p[k]); 
    /*
    copy = a1[j];
    a1[j] += p[k];
    z = a1[j] - copy;
    a2[j] += (copy - (a1[j] - z)) + (p[k] - z);
    */
    // exposant
    

    pk_m128 = _mm_load_pd(&p[k]);

    j_m128.d = pk_m128;
    j_m128.i = _mm_srli_epi64(j_m128.i, 52);
    j_m128.i = _mm_and_si128(j_m128.i, and_m128);
    j1 = _mm_cvtsi128_si32(j_m128.i);
    j2 = _mm_cvtsi128_si32(_mm_unpackhi_epi64(j_m128.i,j_m128.i));

    //    a1_j1_j2_m128 = _mm_set_pd(a1[j1],a1[j2]);
    a1_j1_j2_m128 = _mm_set_pd(a1[j2],a1[j1]);
    copy_m128 = a1_j1_j2_m128;

    a1_j1_j2_m128 = _mm_add_pd(a1_j1_j2_m128, pk_m128);
    z_m128 = _mm_sub_pd(a1_j1_j2_m128, copy_m128);

    a2_j1_j2_m128 = _mm_set_pd(a2[j2],a2[j1]);
    a2_j1_j2_m128 = _mm_add_pd(a2_j1_j2_m128,
			       _mm_add_pd(
					  _mm_sub_pd(copy_m128, _mm_sub_pd(a1_j1_j2_m128, z_m128) ),
					  _mm_sub_pd(pk_m128, z_m128)
					  )
			       );

    /* _mm_store_pd(&tmpa1[0], a1_j1_j2_m128); */
    /* _mm_store_pd(&tmpa2[0], a2_j1_j2_m128); */
    _mm_storel_pd(&a1[j1], a1_j1_j2_m128);
    _mm_storeh_pd(&a2[j1], a1_j1_j2_m128);
    /* a1[j1] = tmpa1[0]; */
    /* a2[j1] = tmpa2[0]; */

    a1[j2] = tmpa1[1];
    a2[j2] = tmpa2[1];
    
    //    i ++;
    i = i + 2;

    if (i>=N) {	
      b1 = memset(b1, 0, sizeof(double)*b_l);
      b2 = memset(b2, 0, sizeof(double)*b_l);
      for (l = 0; l<b_l; l++) {		//for each number y in a1 and a2
	j = exposant(a1[l]);
	copy = b1[j];
	b1[j] += a1[l];
	z = b1[j] - copy;
	b2[j] += (copy - (b1[j] - z)) + (a1[l] - z);
      }
      for (l = 0; l<b_l; l++) {		
	j = exposant(a2[l]);
	copy = b1[j];
	b1[j] += a2[l];
	z = b1[j] - copy;
	b2[j] += (copy - (b1[j] - z)) + (a2[l] - z);
      }					//end for each ...
      temp = a1;		//swap array pointers a1 and a2 with b1 and b2 respectively
      a1 = b1;
      b1 = temp;
      temp = a2;		
      a2 = b2;
      b2 = temp;
      i = b_l<<1;
    }

    /* //    a1[j1] += tmpa1[0]; */
    /* a1[j2] += tmpa1[1]; */
    /* //    a2[j1] += tmpa2[0]; */
    /* a2[j2] += tmpa2[1]; */
    /* i++; */

    /* if (i>=N) {	 */
    /*   b1 = memset(b1, 0, sizeof(double)*b_l); */
    /*   b2 = memset(b2, 0, sizeof(double)*b_l); */
    /*   for (l = 0; l<b_l; l++) {		//for each number y in a1 and a2 */
    /* 	j = exposant(a1[l]); */
    /* 	copy = b1[j]; */
    /* 	b1[j] += a1[l]; */
    /* 	z = b1[j] - copy; */
    /* 	b2[j] += (copy - (b1[j] - z)) + (a1[l] - z); */
    /*   } */
    /*   for (l = 0; l<b_l; l++) {		 */
    /* 	j = exposant(a2[l]); */
    /* 	copy = b1[j]; */
    /* 	b1[j] += a2[l]; */
    /* 	z = b1[j] - copy; */
    /* 	b2[j] += (copy - (b1[j] - z)) + (a2[l] - z); */
    /*   }					//end for each ... */
    /*   temp = a1;		//swap array pointers a1 and a2 with b1 and b2 respectively */
    /*   a1 = b1; */
    /*   b1 = temp; */
    /*   temp = a2;		 */
    /*   a2 = b2; */
    /*   b2 = temp; */
    /*   i = b_l<<1; */
    /* } */

  }	
  for (; k<n; k++) {
    j = exposant(p[k]); 

    copy = a1[j];
    a1[j] += p[k];
    z = a1[j] - copy;
    a2[j] += (copy - (a1[j] - z)) + (p[k] - z);

    i ++;
    if (i>=N) {	
      b1 = memset(b1, 0, sizeof(double)*b_l);
      b2 = memset(b2, 0, sizeof(double)*b_l);
      for (l = 0; l<b_l; l++) {		//for each number y in a1 and a2
	j = exposant(a1[l]);
	copy = b1[j];
	b1[j] += a1[l];
	z = b1[j] - copy;
	b2[j] += (copy - (b1[j] - z)) + (a1[l] - z);
      }
      for (l = 0; l<b_l; l++) {		
	j = exposant(a2[l]);
	copy = b1[j];
	b1[j] += a2[l];
	z = b1[j] - copy;
	b2[j] += (copy - (b1[j] - z)) + (a2[l] - z);
      }					//end for each ...
      temp = a1;		//swap array pointers a1 and a2 with b1 and b2 respectively
      a1 = b1;
      b1 = temp;
      temp = a2;		
      a2 = b2;
      b2 = temp;
      i = b_l<<1;
    }
  }	
  for (l = 0; l<b_l; l++) {	//concatenate a1 and a2 to be a and remove zeros
    if (a1[l]!=0.0) { a[count]= a1[l]; count++;}
  }
  for (l = 0; l<b_l; l++) {	
    if (a2[l]!=0.0) { a[count]= a2[l]; count++;}
  }

  // observation cond(a)
  double sum_abs = 0.0;
  for (l=0; l<count; l++) {
    sum_abs += fabs(a[l]);}
  //  printf("OLE: appel iFastSum\n");
  
  r = iFastSum(a,  count);
  
  //  printf("OLE : count=%i, cond[a]= %e\n", count, sum_abs/fabs(r));
  
  free(a); free(a1); free(a2); free(b1); free(b2);		
  return r;}




/* ------------------------------------------------------------- */
/* ------------------------------------------------------------- */
//int AddTwo(double a, double b, double *sum, double *error) {  //Knuth
//int FastAddTwo(double a, double b, double *sum, double *error) {
//int FastAddTwoSC(double a, double b, double *sum, double *error) {
	//sans comparaison - suppose a>b
/*
int FastAddTwoCumul(double a, double b, double *sum, double *error) {
	*sum = a + b;
	if (fabs(a) > fabs(b)) {
		*error += b - (*sum - a);
	} else {
		*error += a - (*sum - b);
	}
	return EXIT_SUCCESS;
}
*/

/* ----------------------------------------------------------------- */
/* --- Dot product algorithms -------------------------------------- */
/* ----------------------------------------------------------------- */

/* double Dot(double *x, double *y, unsigned int n) { */
/*   double s = x[0] * y[0]; */
/*   int i; */

/*   for(i=1; i<n; i++) s += (x[i] * y[i]); */
/*   return(s); */
/* } */

/* double Dot2(double *x, double *y, unsigned int n) { */
/*   double p, s, c, pi, sig; */
/*   int i; */

/*   TwoProd(s, c, x[0], y[0]); */
/*   for(i=1; i<n; i++) { */
/*     TwoProd(p, pi, x[i], y[i]); */
/*     TwoSumIn(s, sig, p); */
/*     c += (pi + sig); */
/*   } */
/*   return(s+c); */
/* } */

/* double Dot2_aeb(double *beta, double *x, double *y, unsigned int n) { */
/*   double p, s, c, b, alpha, pi, sig, t; */
/*   int i; */

/*   TwoProd(s, c, x[0], y[0]); */
/*   b = fabs(c); */
/*   for(i=1; i<n; i++) { */
/*     TwoProd(p, pi, x[i], y[i]); */
/*     TwoSumIn(s, sig, p); */
/*     t = pi+sig; */
/*     c += t; */
/*     b += fabs(t); */
/*   } */
/*   s += c; */
/*   t = (n*URD_DBL)/(1-2*n*URD_DBL); */
/*   alpha = (URD_DBL*fabs(s) + (t*b + 6*LBD_DBL)); */
/*   *beta = alpha/(1-2*URD_DBL); */
/*   return(s); */
/* } */

/* double Dot2_reb(double *beta, double *x, double *y, unsigned int n) { */
/*   double p, s, c, b, alpha, pi, sig, t; */
/*   int i; */

/*   TwoProd(s, c, x[0], y[0]); */
/*   TwoProd(p, pi, x[1], y[1]); */
/*   TwoSumIn(s, sig, p); */
/*   t = pi+sig; */
/*   c += t; */
/*   b = fabs(c) + fabs(t); */
/*   for(i=2; i<n; i++) { */
/*     TwoProd(p, pi, x[i], y[i]); */
/*     TwoSumIn(s, sig, p); */
/*     t = pi+sig; */
/*     c += t; */
/*     b += fabs(c) + fabs(t); */
/*   } */
/*   TwoSumIn(s, t, c); */
/*   t = s*URD_DBL; */
/*   alpha = (URD_DBL*b)/(1-(n+2)*URD_DBL) + 3*ETA_DBL; */
/*   *beta = (fabs(t)+alpha)/(1-3*URD_DBL) + ETA_DBL; */
/*   return(s); */
/* } */

/* /\* ----------------------------------------------------------------- *\/ */
/* /\* --- Dot-product algorithms with Bailey's double-double ---------- *\/ */
/* /\* ----------------------------------------------------------------- *\/ */

/* #if defined(__ARCH_ia64__) && defined(__ICC__) */
/* double DotDD(double *x, double *y, unsigned int n) { */
/*   double ph, pl, sh, sl, s1, s2, t1, t2, u, v; */
/*   int i; */

/*   sh = sl = 0.0; */
/*   for(i=0; i<n; i++) { */
/*     /\*TwoProd(ph, pl, x[i], y[i]);*\/ */
/*     ph = x[i] * y[i]; pl = x[i] * y[i] - ph; */

/*     /\* TwoSum(s1, s2, sh, ph); s2 += t1; TwoSum(t1, t2, sl, pl); *\/ */
/*     s1 = sh + ph;                 t1 = sl + pl; */
/*     u = s1 - sh;                  v = t1 - sl; */
/*     s2 = ((sh-(s1-u))+(ph-u))+t1; t2 = (sl-(t1-v))+(pl-v); */

/*     /\* FastTwoSum(t1, s2, s1, s2); t2 += s2 *\/ */
/*     t1 = s1 + s2; */
/*     t2 += (s1 - t1) + s2; */

/*     /\*FastTwoSum(sh, sl, t1, t2);*\/ */
/*     sh = t1 + t2; */
/*     sl = (t1 - sh) + t2; */
/*   } */
/*   return(sh); */
/* } */
/* #else */
/* double DotDD(double *x, double *y, unsigned int n) { */
/*   double s, t, h, r, s1, s2, t1, t2; */
/*   int i; */
/*   s = t = 0; */
/*   for(i=0; i<n; i++) { */
/*     TwoProd(h, r, x[i], y[i]); */
/*     TwoSum(s1, s2, s, h); */
/*     TwoSum(t1, t2, t, r); */
/*     s2 += t1; */
/*     FastTwoSum(t1, s2, s1, s2); */
/*     t2 += s2; */
/*     FastTwoSum(s, t, t1, t2); */
/*   } */
/*   return(s); */
/* } */
/* #endif */

/* /\* ----------------------------------------------------------------- *\/ */
/* /\* --- Accurate dot-product algorithms ----------------------------- *\/ */
/* /\* ----------------------------------------------------------------- *\/ */

/* double AccDot(double *x, double *y, unsigned int n) { */
/*   double r, *p; */
/*   int i, j; */

/*   p = (double*)malloc(sizeof(double)*2*n); */
/*   for(i=0, j=0; i<n; i++, j+=2) { */
/*     TwoProd(p[j], p[j+1], x[i], y[i]); */
/*   } */
/*   r = AccSumIn(p, 2*n); */
/*   free(p); */
/*   return(r); */
/* } */

/* double CompErrorDot(double *x, double *y, unsigned int n, double r) { */
/*   double e, *p; */
/*   int i, j; */

/*   p = (double*)malloc(sizeof(double)*(2*n+1)); */
/*   for(i=0, j=0; i<n; i++, j+=2) { */
/*     TwoProd(p[j], p[j+1], x[i], y[i]); */
/*   } */
/*   p[2*n] = -r; */
/*   e = fabs(AccSumIn(p, 2*n+1)); */
/*   free(p); */
/*   return(e); */
/* } */

/* double GenDot(double *x, double *y, double *C, unsigned int n, double c) { */
/*   unsigned int i, j, n2 = n/2; */
/*   double e, b, s; */

/*   b = log2(c)/2-2.0; */
/*   x[0] = rand_unif(-1.0, 1.0); */
/*   y[0] = rand_unif(-1.0, 1.0); */
/*   x[1] = ldexp(rand_unif(-1.0, 1.0), nearbyint(b)+1); */
/*   y[1] = ldexp(rand_unif(-1.0, 1.0), nearbyint(b)+1); */
/*   for(i=2; i<n2; i++) { */
/*     x[i] = ldexp(rand_unif(-1.0, 1.0), nearbyint(rand_double(1)*b)); */
/*     y[i] = ldexp(rand_unif(-1.0, 1.0), nearbyint(rand_double(1)*b)); */
/*   } */

/*   for(i=n2; i<n; i++) { */
/*     e = nearbyint(b-(i-n2)*b/(n-1-n2)); */
/*     x[i] = ldexp(rand_unif(-1.0, 1.0), e); */
/*     y[i] = ldexp(rand_unif(-1.0, 1.0), e) - AccDot(x, y, i) / x[i]; */
/*   } */

/*   for(i=1; i<n; i++) { */
/*     if((j = rand_uint(i)) != i) { */
/*       b = x[j]; x[j] = x[i]; x[i] = b; */
/*       b = y[j]; y[j] = y[i]; y[i] = b; */
/*     } */
/*   } */

/*   b = fabs(x[0] * y[0]); for(i=1; i<n; i++) b += fabs(x[i] * y[i]); */
/*   s = AccDot(x, y, n); */
/*   *C = b / fabs(s); */

/*   return(s); */
/* } */

/* double RandDot(double *x, double *y, double *C, unsigned int n) { */
/*   double b, s; */
/*   unsigned int i; */

/*   for(i=0; i<n; i++) { */
/*     x[i] = rand_unif(-1.0, 1.0); */
/*     y[i] = rand_unif(-1.0, 1.0); */
/*   } */

/*   b = fabs(x[0] * y[0]); for(i=1; i<n; i++) b += fabs(x[i] * y[i]); */
/*   s = AccDot(x, y, n); */
/*   *C = b / fabs(s);   */
  
/*   return(s); */
/* } */

/* double RandDot01(double *x, double *y, double *C, unsigned int n) { */
/*   double b, s; */
/*   unsigned int i; */

/*   for(i=0; i<n; i++) { */
/*     x[i] = rand_unif(0.0, 1.0); */
/*     y[i] = rand_unif(0.0, 1.0); */
/*   } */

/*   b = fabs(x[0] * y[0]); for(i=1; i<n; i++) b += fabs(x[i] * y[i]); */
/*   s = AccDot(x, y, n); */
/*   *C = b / fabs(s);   */
  
/*   return(s); */
/* } */

