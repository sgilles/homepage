#include "gensum.h"

/* ------------------------------------------------------------- */
double rand_unif_m1p1(void) {
  // uniform double in [-1,1]: m1=minus one, p1= plus one
  // a random sign, a random mantissa and a random exponent
  int sign = pow(-1,rand()%2);
  double mant = (double)sign*(double)rand()/(double)RAND_MAX;
  int expo = -(rand()%1023 +1); // in [-1023,-1]
  return ldexp(mant, expo);
}
/* ------------------------------------------------------------- */
double rand_double(double M) { // uniform(0,M)
  return (M*(double)rand()/(double)RAND_MAX);
}
/* ------------------------------------------------------------- */
double rand_unif(double a, double b) { //uniform(a,b)
  return(a+(b-a)*rand()/(double)RAND_MAX);
}
/* ------------------------------------------------------------- */
int rand_int(int a, int b) { //uniform_int(a,b)
  return(int)(rand()%(b-a) + a);
}
/* ------------------------------------------------------------- */
unsigned int rand_uint(unsigned int M) { //uniform_int(0,M+1)
  return((unsigned int)(((M+1.0)*rand()) / (RAND_MAX+1.0)));
}
/* ------------------------------------------------------------- */
typedef union{ double d; long int nd;} nb_d1;		
//en fait peut etre il serait mieux d'utiliser int64 mais faut voir si ca a un impact sur perform
//typedef struct t_n_double { unsigned signe: 1; int exposant: 11; unsigned m1: 26; unsigned m2: 26; } n_double;
//typedef union{ double d; n_double nd;} nb_d2;		
typedef union{ double d; int64_t nd;} nb_d3;		

#if defined __x86_64__
inline int exposant(double x) {	//renvoi exposant tel qu'il est code en machine
	nb_d1 temp;
	temp.d = x;
	return (((temp.nd)>>52)&0x7ff);//-1022; 
}
#else 

inline int exposant(double x) {
	nb_d3 temp;
	temp.d = x;
	return (((temp.nd)>>52)&0x7ff);
}
#endif	
/* ----------------------------------------------------------------- */
/* --- Generator of ill-conditionned sums -------------------------- */
/* ----------------------------------------------------------------- */

double GenSum(double *x, double *C, unsigned int n, double c) {
  unsigned int i, j, n2 = n/2;
  double b, s;

  b = log2(c);
  x[0] = (2.0*rand_double(1)-1);
  x[1] = ldexp( 2.0*rand_double(1)-1, nearbyint(b)+1 );
  for(i=2; i<n2; i++)
    x[i] = ldexp( rand_unif(-1.0, 1.0), nearbyint(rand_unif(-1.0, 1.0)*b) );

  for(i=n2; i<n; i++)
    x[i] = ldexp( rand_unif(-1.0, 1.0), nearbyint(b-(i-n2)*b/(n-1-n2)) ) - AccSum(x, i);
  for(i=1; i<n; i++)
    if((j = rand_uint(i)) != i) { b = x[j]; x[j] = x[i]; x[i] = b; }

  b = fabs(x[0]); for(i=1; i<n; i++) b += fabs(x[i]);
  s = AccSum(x, n);
  *C = b / fabs(s);

  return(s);
}
/* ------------------------------------------------------------- */
double GenSum2(double *x, double *C, unsigned int n, double exp_c) {
  unsigned int i, j, n2 = n/2;
  double b, s;

  double c = pow(10, exp_c);
  b = log2(c);
  x[0] = (2.0*rand_double(1)-1);
  x[1] = ldexp( 2.0*rand_double(1)-1, nearbyint(b)+1 );
  for(i=2; i<n2; i++)
    x[i] = ldexp( rand_unif(-1.0, 1.0), nearbyint(rand_unif(0.0, 1.0)*b) );

  for(i=n2; i<n; i++)
    x[i] = ldexp( rand_unif(-1.0, 1.0), nearbyint(b-(i-n2)*b/(n-1-n2)) ) - AccSum(x, i);
  for(i=1; i<n; i++)
    if((j = rand_uint(i)) != i) { b = x[j]; x[j] = x[i]; x[i] = b; }

  b = fabs(x[0]); for(i=1; i<n; i++) b += fabs(x[i]);
  s = AccSum(x, n);
  *C = b / fabs(s);

  return(s);
}
/* ------------------------------------------------------------- */
double GenSumDelta(double *x, double *C, unsigned int n, double exp_c, int delta){
  // same as GenSum2 with larger exponant range delta  
  unsigned int i, j, n2 = n/2, n4= n/4, n34= 3*n/4;
  double b, s;
  int Emax= delta/2; //to balance generated exponents while delaying 
                     //overflow for large value of delta and n

  if (n%4!=0){
    printf("STOP: GenSumRumpDelta: n has to be a multiple of 4\n");
    return (0);
  }

  double c = pow(10, exp_c);
  b = log2(c);
  int e1 = Emax - nearbyint(b);
  x[0] = ldexp( 2.0*rand_double(1.)-1, Emax); // Max = unif(-1,1)*2^Emax
  x[1] = ldexp( 2.0*rand_double(1.)-1, e1); // Min = unif(-1,1)*2^e1
  // random between Emax and e1
  for(i=2; i<n4; i++){
    int e=rand_int(e1, Emax);
    //    x[i] = ldexp( rand_unif(-1.0, 1.0), nearbyint(rand_unif(0.0, 1.0)*b) );
    x[i] = ldexp( 2.0*rand_double(1.)-1, e );
    printf("step1: i=%d, rand_int=%d, x = %le \n",i, e, x[i]);
  } 
  //
  int exp_step= (int)((double)e1/(double)n4); // step for linear recursion in exponant range Emax..Emax-b 
  printf("exp_step:%d \n", exp_step);
  for(i=n4; i<n2; i++){
    int e= Emax- i*exp_step;
    //    x[i] = ldexp( rand_unif(-1.0, 1.0), nearbyint(rand_unif(0.0, 1.0)*b) );
    x[i] = ldexp( 2.0*rand_double(1.)-1, nearbyint(e) ) - AccSum(x, i); // to cancel
    printf("step2: i=%d, exp=%d, x=%le \n",i, e, x[i]);
  } 
  // random with small exponent : in e1 .. e2
  int e2 = Emax - delta;
  printf("e1=%d, e2=%d \n", e1, e2);
  for(i=n2; i<n34; i++){
    int ee=rand_int(e2, e1);
    //    x[i] = ldexp( rand_unif(-1.0, 1.0), nearbyint(rand_unif(0.0, 1.0)*b) );
    x[i] = ldexp( 2.0*rand_double(1.)-1, ee );
    printf("step3: i=%d, exp=%d, x=%le \n",i, ee, x[i]);
  } 
  //
  int esmall_step= (int)(abs(e2-e1)/n4); // step for linear recursion in exponant range e1..e2 
  for(i=n34; i<n; i++){
    int ee= e1 + i*esmall_step; 
    x[i] = ldexp( 2.0*rand_double(1)-1, nearbyint(ee) ) - AccSum(x, i);
    printf("step4: i=%d, exp=%d, x=%le \n",i, ee, x[i]);
  }
  // shake
  for(i=1; i<n; i++)
    if((j = rand_uint(i)) != i) { b = x[j]; x[j] = x[i]; x[i] = b; }
  
  b = fabs(x[0]); 
  for(i=1; i<n; i++) b += fabs(x[i]);
  s = AccSum(x, n);
  *C = b / fabs(s);
  
/*   for (int i=0; i<n; i++){		 */
/*     frexp(x[i], &exp); */
/*     exp_min = exp<exp_min?exp:exp_min; */
/*     exp_max = exp>exp_max?exp:exp_max; */
/*     //    printf("i=%i, exp=%i, x= %e \n", i, exp, x[i]); */
/*   }	 */
/*   act_delta = exp_max - exp_min; */

  printf("s=%e \n", s);
  return(s);
}
/* ------------------------------------------------------------- */
double GenSumDeltaUnif(double *x, double *C, unsigned int n, int delta) {
  // random exponent in [-delta/2, delta/2] but not necessary ill conditionned sums
  // x and 1/x are stored 
  unsigned int i, j;
  double mant, b, s;
  int delta2 = delta/2;
  int sign, exp;

  for(i=0; i<n; i+=4){
    sign = pow(-1,rand()%2);
    mant = (double)sign*((double)rand()/(double)RAND_MAX + 1.); // in pm[1,2]
    do {
      exp = (rand()%delta2); // in [-delta/2,-1]
      //      printf("%d %e %d\n",i, mant, exp);
    }  while (exp > 996); //to avoid overflow in split (HybridSum) 
    x[i] = ldexp(mant, exp);
    x[i+1] = 1./x[i];         // in [1,delta/2]
    x[i+2] = -1.0000000000001*x[i];   //next two almost cancel with the two previous
    x[i+3] = -1.0000000000001*x[i+1];
    //x[i+2] = -x[i];   //next two cancel with the two previous
    //x[i+3] = -x[i+1]; //but let's trust the shake to avoid full perfect cancellation
    
    //    printf("%d %e %e %e %e\n", i, x[i], x[i+1], x[i+2], x[i+3]);
  }

  //shake
  for(i=1; i<n; i++)
    if((j = rand_uint(i)) != i) { b = x[j]; x[j] = x[i]; x[i] = b; }
  
  b = fabs(x[0]); 
  for(i=1; i<n; i++) b += fabs(x[i]);
  printf("sum_abs = %e\n",b);
  s = OnlineExactTwoSum(x, n);
  //s = OnlineExactSum(x, n);
  //s = HybridSum(x, n);
  //s = AccSum(x, n);
  //s = FastAccSum(x, n);
  //s = Sum2(x, n);
  *C = b / fabs(s);
  //*C = 1.0000000;
  //return(b);
  return(s);
}
/* ------------------------------------------------------------- */
double GenSumDeltaDirac(double *x, double *C, unsigned int n, int delta) {
  // NO random exponent in [-delta/2, delta/2]: (n-1) are -delta/2 and 1 is delta/2  
  // x and 1/x are stored 
  unsigned int i, j;
  double mant, mant1, b, s;
  int sign,  exp;

  int delta2 = delta/2;
  int safe_delta2 = delta2>996?996:delta2;
  //to avoid overflow in split (HybridSum) 

  for(i=0; i<n-1; i+=2){
    do { 
      sign = pow(-1,rand()%2);
      mant = (double)sign*((double)rand()/(double)RAND_MAX + 1.0);
      //exp = -(rand()%delta2 +1); // in [-delta/2,-1]
      x[i] = ldexp(mant, safe_delta2);
      mant1 = frexp(x[i], &exp); //mant1 in [0.5,1] grrr!!!
      //printf("%d %e %e %e %d %d \n", i, mant, mant1, x[i], exp, safe_delta2);
    }
    while (exp-1 != safe_delta2); //frexp breaks the normalization rule  
    x[i+1] = -1.0000000000001*x[i];   //almost cancel with the previous
    //printf("%d %d %d\n", i, exposant(x[i]), exposant(x[i+1]));
    //printf("%d %e %e\n", i, x[i], x[i+1]);
  }
  x[n-1] = 1./x[n-2];         // 
  //printf("%d %d \n", n-1, exposant(x[n-1]));  
  //  printf("%d %e \n", n-1, x[n-1]));

  //shake
  for(i=1; i<n; i++)
    if((j = rand_uint(i)) != i) { b = x[j]; x[j] = x[i]; x[i] = b; }
  
  b = fabs(x[0]); 

  //  if (no_cond == 0)
  //{
      for(i=1; i<n; i++) b += fabs(x[i]);
      s = OnlineExactTwoSum(x, n);
      //      s = OnlineExactSum(x, n);
      *C = b / fabs(s);
      //}
      //else
      //s = OnlineExactSum(x, n);
  

  // actual delta
  //  delta = exp_max-exp_min;

  return(s);
}
/* /\* ----------------------------------------------------------------- *\/ */
/* /\* --- lecture des fichiers generees par generator.c --------------- *\/ */
/* /\* ----------------------------------------------------------------- *\/ */


double **rand_nbs(char * nfich, unsigned int n_voulu) { //renvoi tableau des n_voulu nombres flottants
	int n, nb_vect, exp_max, exp_min;
	unsigned int i, j, k;
	int r;
	int fich = open(nfich, O_RDONLY);
	if (fich==-1) {printf("erreur ouverture fichier\n"); exit(1);}
	//structure de fichier: unsigned int (taille de vecteur), unsigned int (nombre des vecteurs), double (conditionnement minimal de lot), double*taille_vect*nb_vect (nombres ranges par vecteurs)
	r = read(fich, &n, sizeof(unsigned int)); if (r!=sizeof(unsigned int)) {printf("erreur lecture fichier1\n"); exit(1);}
	r = read(fich, &nb_vect, sizeof(unsigned int));  if (r!=sizeof(unsigned int)) {printf("erreur lecture fichier2\n"); exit(1);}
	r = lseek(fich, sizeof(double), SEEK_CUR); if (r!=(sizeof(double)+2*sizeof(unsigned int))) {printf("erreur lecture fichier3\n"); exit(1);}
	if ((n_voulu%(n)!=0) ||((n)>n_voulu)) {printf("erreur \n"); exit(1);}
	double **tab= (double **)malloc(sizeof(double)*nb_vect);
	if (tab ==NULL) {printf("erreur allocation4\n"); exit(1);}
	for (i = 0; i<nb_vect; i++) {
		tab[i] = (double *)malloc(sizeof(double)*n_voulu);
		if (tab[i] == NULL) {printf("erreur allocation5\n"); exit(1);}
	}

	for (i=0; i<nb_vect; i++) {
		exp_min = 1024; exp_max = -1024;
		for (j=0; j<n; j++) {
			r = read(fich, &tab[i][j], sizeof(double));
			exp_max = exp_max>(exposant(tab[i][j])-1022)?exp_max:(exposant(tab[i][j])-1022);
			exp_min = exp_min<(exposant(tab[i][j])-1022)?exp_min:(exposant(tab[i][j])-1022);
			//printf("i: %u, j: %u --> %e\n", i, j, tab[i][j]);
			if (r!=sizeof(double)) {printf("erreur lecture fichier6\n"); exit(1);}
			for (k=1; k<(n_voulu/n); k++) {
				tab[i][n*k+j] = tab [i][j];
			}
		}
	//	printf("vecteur %d ->delta: %d\n", i, exp_max - exp_min);
	}
	printf("tableau generé: %d vecteurs de taille %d (doubles)\n", nb_vect, n_voulu);
	close(fich);
	return tab;
}

/*---------------------------------------------------------------------------------*/
double **rand_nbs_delta(char * nfich, unsigned int n_voulu) { //renvoi tableau des n_voulu nombres flottants
	unsigned int n, nb_vect, delta;
	double cond;
	unsigned int i, j, k;
	int r;
	int fich = open(nfich, O_RDONLY);
	if (fich==-1) {printf("erreur ouverture fichier\n"); exit(1);}
	//structure de fichier: unsigned int (taille de vecteur), unsigned int (nombre des vecteurs), double (conditionnement minimal de lot), double*taille_vect*nb_vect (nombres ranges par vecteurs)
	r = read(fich, &n, sizeof(unsigned int)); if (r!=sizeof(unsigned int)) {printf("erreur lecture fichier1\n"); exit(1);}
	r = read(fich, &nb_vect, sizeof(unsigned int));  if (r!=sizeof(unsigned int)) {printf("erreur lecture fichier2\n"); exit(1);}
	r = read(fich, &delta, sizeof(unsigned int)); if (r!=sizeof(int)) {printf("erreur lecture fichier3\n"); exit(1);}
	r = read(fich, &cond, sizeof(double)); if (r!=sizeof(double)) {printf("erreur lecture fichier3\n"); exit(1);}
	if ((n_voulu%(n)!=0) ||((n)>n_voulu)) {printf("erreur \n"); exit(1);}
	double **tab= (double **)malloc(sizeof(double)*nb_vect);
	if (tab ==NULL) {printf("erreur allocation4\n"); exit(1);}
	for (i = 0; i<nb_vect; i++) {
		tab[i] = (double *)malloc(sizeof(double)*n_voulu);
		if (tab[i] == NULL) {printf("erreur allocation5\n"); exit(1);}
	}

	for (i=0; i<nb_vect; i++) {
		for (j=0; j<n; j++) {
			r = read(fich, &tab[i][j], sizeof(double));

			//printf("i: %u, j: %u --> %e\n", i, j, tab[i][j]);
			if (r!=sizeof(double)) {printf("erreur lecture fichier6\n"); exit(1);}
			for (k=1; k<(n_voulu/n); k++) {
				tab[i][n*k+j] = tab [i][j];
			}
		}
		//printf("vecteur %d ->delta: %d, conditionnement: % e\n", i, delta, cond);
	}
	printf("tableau generé: %d vecteurs de taille %d (doubles)\n", nb_vect, n_voulu);
	close(fich);
	return tab;
}
/*---------------------------------------------------------------------------------*/
void free_tab_nbs(double ** tab, unsigned int nb_vect) {
    int i;
    for (i=0; i<nb_vect; i++) {
        free(tab[i]);
        tab[i]=NULL;
    }
    free(tab);
    tab=NULL;
}

int lect_param_fich_data(char *nfich, unsigned int *n, unsigned int *nb_vect, double *cond) {
	int r;
	int fich = open(nfich, O_RDONLY);
	if (fich==-1) {printf("erreur ouverture fichier\n"); exit(1);}
	r = read(fich, n, sizeof(unsigned int)); if (r!=sizeof(unsigned int)) {printf("erreur lecture fichier\n"); exit(1);}
	r = read(fich, nb_vect, sizeof(unsigned int));  if (r!=sizeof(unsigned int)) {printf("erreur lecture fichier\n"); exit(1);}
	r = read(fich, cond, sizeof(double));  if (r!=sizeof(double)) {printf("erreur lecture fichier\n"); exit(1);}
	printf("fichier lu: %d vecteurs de taille %d (doubles)\nconditionnement minimal dans ce lot est de %e\n", *nb_vect, *n, *cond);
	close(fich);
	return EXIT_SUCCESS;
}

/***************************************************************************************/
int lect_param_fich_data_delta(char *nfich, unsigned int *n, unsigned int *nb_vect, unsigned int * delta, double *cond) {
	int r;
	int fich = open(nfich, O_RDONLY);
	if (fich==-1) {printf("erreur ouverture fichier\n"); exit(1);}
	r = read(fich, n, sizeof(unsigned int)); if (r!=sizeof(unsigned int)) {printf("erreur lecture fichier\n"); exit(1);}
	r = read(fich, nb_vect, sizeof(unsigned int));  if (r!=sizeof(unsigned int)) {printf("erreur lecture fichier\n"); exit(1);}
	r = read(fich, delta, sizeof(unsigned int));  if (r!=sizeof(int)) {printf("erreur lecture fichier\n"); exit(1);}
	r = read(fich, cond, sizeof(double));  if (r!=sizeof(double)) {printf("erreur lecture fichier\n"); exit(1);}
	printf("fichier lu: %d vecteurs de taille %d (doubles)\ndelta min de ce lot est: %d, conditionnement minimal %e\n", *nb_vect, *n, *delta, *cond);
	close(fich);
	return EXIT_SUCCESS;
}

