%{
# *************************************************************************************
# Copyright (c) 2015, University of Perpignan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.
# *************************************************************************************
#
%}

clear all;
graphics_toolkit("gnuplot");

addpath('scripts');

vars;
testresultscpu;
[values, indices] = sort(results(:, 5) .* results(:, 6));
results = results(indices, :);
[values, indices] = sort(results(:, 7));
results = results(indices, :);

compareAlgos(0, compilers, 0, links, algos, codes, 8, 1, 16, "AVX", AVX, results, colors, 10, 10);
close all;
compareAlgos(0, compilers, 0, links, algos, codes, 8, 1, 1, "AVX", AVX, results, colors, 10, 10);
close all;

%{
comparePrecision(0, compilers, 0, links, algos, codes, 1, 1, 1000, "AVX", AVX, results, colors);
close all;
comparePrecisionCores(0, compilers, 0, links, algos, codes, 8, 1, 1000000, "AVX", AVX, results, colors);
close all;
comparePrecisionMachines(0, compilers, 0, links, ClassicDotAVX, 8, 1, 1000000, instructionSets, instructionSetsCodes, results, colors);
close all;
%}

