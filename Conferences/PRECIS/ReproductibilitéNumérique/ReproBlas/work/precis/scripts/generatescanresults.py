#
# *************************************************************************************
# Copyright (c) 2015, University of Perpignan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.
# *************************************************************************************
#


import os
import shlex
import re
import subprocess
import math
import sys

from optparse import OptionParser

compiler = "icc"
instructionset = "AVX"
link = "static"
linkoctave = "staticlink"

def generateResult(cond, size, algo, thomp, thmpi):
	cmdline = ""
	times = " 8 "
	if (algo == "ExactSum"):
		os.environ['OMP_NUM_THREADS'] = str(thomp)
		for ee in [0, 1]:
			for expsize in [2, 3, 4, 5, 6, 7, 8]:
				cmdline = "../exactsum/longacc " + str(expsize) + " " + str(ee) + " " + str(size) + " " + str(cond) + times
				printstring = "\"" + compiler + ", " + linkoctave + ", Expansion" + str(expsize);
				if ee:
					printstring = printstring + "EE";
				printstring = printstring + ", " + str(cond) + ", " + str(thmpi) + ", " + str(thomp) + "\" "
				cmdline += printstring + instructionset
				subprocess.call(cmdline, shell=True)
				sys.stderr.write(cmdline + "\n")
	else:
		if (algo == "iFastSum" or algo == "AccSum" or algo == "FastAccSum" or algo == "HybridSum" or algo == "OnlineExact" or algo == "ReprodSum" or algo == "FastReprodSum" or algo == "ClassicSum" or algo == "Rsum" or algo == "OneReduction" or algo == "SumK2" or algo == "SumK3" or algo == "Rasum" or algo == "exactdasum" or algo == "Reprodasum" or algo == "FastReprodasum" or algo == "Classicasum" or algo == "Classicnrm2" or algo == "Rnrm2" or algo == "FastReprodnrm2"):
			filedirectory = "sum"
		elif (algo == "iFastSumDot" or algo == "AccSumDot" or algo == "FastAccSumDot" or algo == "HybridSumDot" or algo == "OnlineExactDot" or algo == "ReprodDot" or algo == "FastReprodDot" or algo == "ClassicDot" or algo == "Rdot" or algo == "OneReductionDot" or algo == "OneReductionTwoProdDot"):
			filedirectory = "dot"
		elif (algo == "iFastSumTrsv" or algo == "AccSumTrsv" or algo == "FastAccSumTrsv" or algo == "HybridSumTrsv" or algo == "OnlineExactTrsv" or algo == "ReprodTrsv" or algo == "FastReprodTrsv" or algo == "ClassicTrsv" or algo == "Rtrsv" or algo == "OneReductionTrsv"):
			filedirectory = "trsv"
		else:
			filedirectory = "gemv"
		if thmpi == 1:
			if thomp == 1:
				cmdline = "./run/bin/" + compiler + "/" + link + "/sequential/blas.out " + algo + " " + str(size) + " " + str(cond) + " " + filedirectory + times
			else:
				os.environ['OMP_NUM_THREADS'] = str(thomp)
				cmdline = "./run/bin/" + compiler + "/" + link + "/omp/blas.out " + algo + " " + str(size) + " " + str(cond) + " " + filedirectory + times
		else:
			if thomp == 1:
				cmdline = "mpirun -np " + str(thmpi) + " ./run/bin/" + compiler + "/" + link + "/mpi/blas.out " + algo + " " + str(size) + " " + str(cond) + " " + filedirectory + times
			else:
				os.environ['OMP_NUM_THREADS'] = str(thomp)
				cmdline = "mpirun -np " + str(thmpi) + " ./run/bin/" + compiler + "/" + link + "/hybrid/blas.out " + algo + " " + str(size) + " " + str(cond) + " " + filedirectory + times
		printstring = "\"" + compiler + ", " + linkoctave + ", " + algo + ", " + str(cond) + ", " + str(thmpi) + ", " + str(thomp) + "\" "
		cmdline += printstring + instructionset
		sys.stderr.write(cmdline + "\n")
		subprocess.call(cmdline, shell=True)

def generateResults(conds, sizes, algos, OMP_THREADS, MPI_THREADS):
	sys.stdout.flush();
	for thomp in OMP_THREADS:
		for thmpi in MPI_THREADS:
			for algo in algos:
				for cond in conds:
					for size in sizes:
						generateResult(cond, size, algo, thomp, thmpi)

if __name__ == '__main__':
	sys.stdout.write("results = [\n")
	sizes = map(lambda x: 10**x, range(3, 8)) + map(lambda x: 2 * (10**x), range(3, 8)) + map(lambda x: 5 * (10**x), range(3, 7))
#	sizes = map(lambda x: 200 * x, range(1, 16))
	sizes.sort()
	conds = list(range(8, 11, 8))
	OMP_THREADS = [1]
	MPI_THREADS = map(lambda x: 2**x, range(1))
#	algos = ["OneReductionDot", "OneReductionTwoProdDot"]
	algos = ["SumK2", "SumK3", "AccSum", "FastAccSum", "iFastSum", "OnlineExact", "HybridSum"]
	generateResults(conds, sizes, algos, OMP_THREADS, MPI_THREADS)
	conds = list(range(32, 33, 8))
	algos = ["AccSum", "FastAccSum", "iFastSum", "OnlineExact", "HybridSum", "Classicasum", "ClassicDot", "Classicnrm2", "Rasum", "Rdot", "Rnrm2"]
	generateResults(conds, sizes, algos, OMP_THREADS, MPI_THREADS)
	OMP_THREADS = [16]
	algos = ["Classicasum", "ClassicDot", "Classicnrm2", "Rasum", "Rdot", "Rnrm2", "FastReprodasum", "FastReprodDot", "FastReprodnrm2"]
	generateResults(conds, sizes, algos, OMP_THREADS, MPI_THREADS)
	sys.stdout.write("]\n")

