#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys
import os
import shlex
import subprocess

import re
from optparse import OptionParser

import gemv_parameters as parameters

__version__ = '0.1'

def create_data_collection(filename=""):
    fname=filename+".data"
    tmpfd = open(fname,"w")

    ### Reading data
    print filename
    f = open(filename, "r")
    pattern = re.compile("(.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*)")
    #    print(pattern)
    
    collection = []
    line = f.readline()
    
    while (line != ""):
        m = re.match(pattern,line)

        if m != None:
            collection.append(m)
        line = f.readline()                                   
        
    f.close()
    tmpfd.close()
    return collection
    
def create_graph(filename="", collec=None):

    datafilename = filename+".data"
    fname = filename+".gp"
    f = open(fname, "w")
    
    f.write("\n")

    f.write('set term dumb')
    f.write("\n")
    # f.write('set boxwidth 0.9 absolute')
    # f.write("\n")
    # f.write('set style fill   solid 1.00 border -1')
    # f.write("\n")

    #    set style data histograms
    #    set style histogram cluster
    # f.write('set style data histograms')
    # f.write("\n")

    # f.write('set style histogram clustered gap 1 title  offset character 0, 0, 0')
    # f.write("\n")
    f.write("set datafile missing '-'")
    f.write("\n")
    # f.write('set style data linespoints')
    # f.write("\n")
    # f.write('set xtics border in scale 1,0.5 nomirror rotate by -45  offset character 0, 0, 0 ')
    # f.write("\n")

    f.write('set log x')
    f.write("\n")
    f.write('set xlabel "size"')
    f.write("\n")
    f.write('set ylabel "cycles/size"')
    f.write("\n")

    f.write('set title "Results" ')
    f.write("\n")
    f.write('set yrange [ 0.00000 : * ] noreverse nowriteback')
    f.write("\n")
    f.write('set xrange ['+str(parameters.sizes[0])+':'+str(parameters.sizes[len(parameters.sizes)-1])+']')
    f.write("\n")
    f.write("\n")

    gpcommand = "plot "    
    # for col in range(2,len(parameters.algos)+2):
    #         for i in range(len(parameters.OMP_THREADS)):
    #             if col==2 and i == 0:
    #                 gpcommand = gpcommand + "'"+datafilename+"' using (1):($"+str(col)+"/$1) index "+str(i)+" with linespoints title '"+str(parameters.algos[col-2])+"-"+str(parameters.OMP_THREADS[i])+"'"
    #             else:
    #                 gpcommand = gpcommand + ",\\\n\t'' using 1:($"+str(col)+"/$1) index "+str(i)+" with linespoints title '"+str(parameters.algos[col-2])+"-"+str(parameters.OMP_THREADS[i])+"'"
    # for col in range(2,len(parameters.algos)+2):
    #         for i in range(len(parameters.OMP_THREADS)):
    #             if col==2 and i == 0:
    #                 gpcommand = gpcommand + "'"+datafilename+"' using (1):($"+str(col)+") index "+str(i)+" with linespoints title '"+str(parameters.algos[col-2])+"-"+str(parameters.OMP_THREADS[i])+"'"
    #             else:
    #                 gpcommand = gpcommand + ",\\\n\t'' using 1:($"+str(col)+") index "+str(i)+" with linespoints title '"+str(parameters.algos[col-2])+"-"+str(parameters.OMP_THREADS[i])+"'"
    for col in range(2,len(parameters.algos)+2):
            for i in range(len(parameters.OMP_THREADS)):
                if col==2 and i == 0:
                    gpcommand = gpcommand + "'"+datafilename+"' using 1:"+str(col)+" index "+str(i+1)+" with linespoints title '"+str(parameters.algos[col-2])+"-"+str(parameters.OMP_THREADS[i])+"'"
                else:
                    gpcommand = gpcommand + ",\\\n\t'' using 1:"+str(col)+" index "+str(i+1)+" with linespoints title '"+str(parameters.algos[col-2])+"-"+str(parameters.OMP_THREADS[i])+"'"
    f.write(gpcommand)
    f.write("\n")
    f.write("\n")
    f.write('set term postscript')
    f.write("\n")
    f.write('set output "'+filename+'.eps"')
    f.write("\n")
    f.write("replot")
    f.write("\n")
    f.close()

    fname = filename+".data"
    f = open(fname, "w")

    jus=25
    f.write(str("Aglos").ljust(jus))
    for algo in parameters.algos:
        f.write((str(algo).strip()).ljust(jus))

    f.write("\n")
    for cond in parameters.conds:
        # f.write(str(cond).ljust(jus))
        # f.write("\n")        
        for thomp in parameters.OMP_THREADS:
            #f.write("\n")
            f.write("\n")
#            f.write(("#Threads:"+str(thomp)).ljust(jus))
            f.write("\n")
            for size in parameters.sizes:
                f.write(str(size).ljust(jus))
                for algo in parameters.algos:
                    for c in collec:
                        if algo==str(c.group(3)).strip() and str(cond)==str(c.group(4)).strip() and str(size)==str(c.group(7)).strip() and str(thomp)==str(c.group(6)).strip():
                            f.write((str(float(c.group(10))/size).strip()).ljust(jus))
#                            f.write("")
                f.write("\n")
    f.write("\n")        
    f.close()
    
def main():    
    parser = OptionParser()
    
    (options, args) = parser.parse_args()
    arguments = ""
    for args in parser.rargs:
        arguments = arguments + " " + args

    collec = create_data_collection("testresultscpu.m")
    create_graph("testresultscpu.m", collec)

if __name__ == '__main__':
    main()



