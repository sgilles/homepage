#
# *************************************************************************************
# Copyright (c) 2015, University of Perpignan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.
# *************************************************************************************
#


import os
import shlex
import re
import subprocess
import math
import sys

from optparse import OptionParser

#import parameters as parameters
import gemv_parameters as parameters

compiler = "icc"
instructionset = "AVX"
link = "static"
linkoctave = "staticlink"
binary= "blas.out"

def generateResult(cond, size, algo, thomp, thmpi):
	cmdline = ""
	times = " 8 "
	if (algo == "ExactSum"):
		os.environ['OMP_NUM_THREADS'] = str(thomp)
		for ee in [0, 1]:
			for expsize in [2, 3, 4, 5, 6, 7, 8]:
				cmdline = "../exactsum/longacc " + str(expsize) + " " + str(ee) + " " + str(size) + " " + str(cond) + times
				printstring = "\"" + compiler + ", " + linkoctave + ", Expansion" + str(expsize);
				if ee:
					printstring = printstring + "EE";
				printstring = printstring + ", " + str(cond) + ", " + str(thmpi) + ", " + str(thomp) + "\" "
				cmdline += printstring + instructionset
				subprocess.call(cmdline, shell=True)
				sys.stderr.write(cmdline + "\n")
	else:
		if (algo == "iFastSum" or algo == "AccSum" or algo == "FastAccSum" or algo == "HybridSum" or algo == "OnlineExact" or algo == "ReprodSum" or algo == "FastReprodSum" or algo == "ClassicSum" or algo == "Rsum" or algo == "OneReduction" or algo == "SumK2" or algo == "SumK3" or algo == "Rasum" or algo == "exactdasum" or algo == "Reprodasum" or algo == "FastReprodasum" or algo == "OneReductionasum" or algo == "Classicasum" or algo == "Classicnrm2" or algo == "Rnrm2" or algo == "FastReprodnrm2" or algo == "OneReductionnrm2" or algo == "exsum2" or algo == "exsum3" or algo == "exsum4" or algo == "exsum8" or algo == "exsum2EE" or algo == "exsum3EE" or algo == "exsum4EE" or algo == "exsum8EE" or algo == "RSum"):
			filedirectory = "sum"
		elif (algo == "iFastSumDot" or algo == "AccSumDot" or algo == "FastAccSumDot" or algo == "HybridSumDot" or algo == "OnlineExactDot" or algo == "ReprodDot" or algo == "FastReprodDot" or algo == "ClassicDot" or algo == "ClassicDotCompatible" or algo == "ClassicDotSSE2" or algo == "ClassicDotSSE3" or algo == "ClassicDotSSSE3" or algo == "ClassicDotSSE4_1" or algo == "ClassicDotSSE4_2" or algo == "ClassicDotAVX" or algo == "ClassicDotAVX2" or algo == "Rdot" or algo == "OneReductionDot" or algo == "OneReductionTwoProdDot" or algo == "dot16" or algo == "dot8"):
			filedirectory = "dot"
		elif (algo == "iFastSumTrsv" or algo == "AccSumTrsv" or algo == "FastAccSumTrsv" or algo == "HybridSumTrsv" or algo == "OnlineExactTrsv" or algo == "ReprodTrsv" or algo == "FastReprodTrsv" or algo == "ClassicTrsv" or algo == "ClassicTrsvCompatible" or algo == "ClassicTrsvSSE2" or algo == "ClassicTrsvSSE3" or algo == "ClassicTrsvSSSE3" or algo == "ClassicTrsvSSE4_1" or algo == "ClassicTrsvSSE4_2" or algo == "ClassicTrsvAVX" or algo == "ClassicTrsvAVX2" or algo == "Rtrsv" or algo == "OneReductionTrsv" or algo == "OneReductionIRTrsv" or algo == "TestReprodTrsv" or algo == "TestNonReprodTrsv" or algo == "XBLASTrsv"):
			filedirectory = "trsv"
		elif (algo == "ClassicGemm" or algo == "ClassicGemmCompatible" or algo == "ClassicGemmSSE2" or algo == "ClassicGemmSSE3" or algo == "ClassicGemmSSSE3" or algo == "ClassicGemmSSE4_1" or algo == "ClassicGemmSSE4_2" or algo == "ClassicGemmAVX" or algo == "ClassicGemmAVX2"):
			filedirectory = "gemm"
		else:
			filedirectory = "gemv"
		if thmpi == 1:
			if thomp == 1:
				cmdline = "./run/bin/" + compiler + "/" + link + "/sequential/"+binary+" " + algo + " " + str(size) + " " + str(cond) + " " + filedirectory + times
			else:
				os.environ['OMP_NUM_THREADS'] = str(thomp)
				cmdline = "./run/bin/" + compiler + "/" + link + "/omp/"+binary+" " + algo + " " + str(size) + " " + str(cond) + " " + filedirectory + times
		else:
			if thomp == 1:
				cmdline = "mpirun -np " + str(thmpi) + " ./run/bin/" + compiler + "/" + link + "/mpi/"+binary+" " + algo + " " + str(size) + " " + str(cond) + " " + filedirectory + times
			else:
				os.environ['OMP_NUM_THREADS'] = '1'
				cmdline = "mpirun -np " + str(thmpi) + " ./run/bin/" + compiler + "/" + link + "/hybrid/"+binary+" " + algo + " " + str(size) + " " + str(cond) + " " + filedirectory + times
		printstring = "\"" + compiler + ", " + linkoctave + ", " + algo + ", " + str(cond) + ", " + str(thmpi) + ", " + str(thomp) + "\" "
		cmdline += printstring + instructionset
		sys.stderr.write(cmdline + "\n")
		subprocess.call(cmdline, shell=True)

def generateResults(conds, sizes, algos, OMP_THREADS, MPI_THREADS):
	sys.stdout.flush();
	for thmpi in MPI_THREADS:
		for thomp in OMP_THREADS:
			for algo in algos:
				for cond in conds:
					for size in sizes:
						generateResult(cond, size, algo, thomp, thmpi)

if __name__ == '__main__':
	sys.stdout.write("results = [\n")
#	algos = ["ClassicTrsv", "OneReductionTrsv", "OneReductionIRTrsv", "HybridSumTrsv", "XBLASTrsv"]
#	algos = ["ClassicDot", "RDot"]
	generateResults(parameters.conds, parameters.sizes, parameters.algos, parameters.OMP_THREADS, parameters.MPI_THREADS)
	sys.stdout.write("]\n")

