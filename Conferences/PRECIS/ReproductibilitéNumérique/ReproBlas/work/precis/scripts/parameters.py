### Parameters for all scripts

# -----------------------------------------------------------------------------------------------------
# PRECIS 2017 EXO 2
# -----------------------------------------------------------------------------------------------------
OMP_THREADS = [1]
MPI_THREADS = [1] #map(lambda x: 2**x, range(1))

algos = ["ClassicSum", "SumK2", "SumK3", "ReprodSum", "iFastSum", "OnlineExact", "HybridSum"]
sizes = map(lambda x: 10**x, range(4, 8))
sizes.sort()
#conds = list(range(6, 13, 6))
conds = list(range(6, 7, 6))

# -----------------------------------------------------------------------------------------------------
# PRECIS 2017 EXO 3-1
# -----------------------------------------------------------------------------------------------------
# OMP_THREADS = [1]
# MPI_THREADS = [1]
# algos = ["ClassicDot", "OneReductionDot", "Rdot"]
# sizes = map(lambda x: 10**x, range(4, 8))
# sizes.sort()
# #conds = list(range(6, 13, 6))
# conds = list(range(6, 7, 6))

# -----------------------------------------------------------------------------------------------------
# PRECIS 2017 EXO 3-2
# -----------------------------------------------------------------------------------------------------
# OMP_THREADS = [4,8,16]
# MPI_THREADS = [1]
# #algos = ["ClassicDot", "OneReductionDot", "Rdot", "OnlineExactDot", "HybridSumDot"]
# algos = ["ClassicDot", "Rdot"]
# sizes = map(lambda x: 10**x, range(5, 8))
# sizes.sort()
# #conds = list(range(6, 13, 6))
# conds = list(range(6, 7, 6))
