%{
# *************************************************************************************
# Copyright (c) 2015, University of Perpignan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.
# *************************************************************************************
#
%}

SSE         = 0;
AVX         = 1;
AVX2        = 2;
AVX512      = 3;
KNC         = 4;

icc         = 0;
gcc         = 1;

staticlink  = 0;
dynamiclink = 1;

ClassicSum         = 000;
Classicasum        = 001;
OneReduction       = 010;
FastReprodSum      = 011;
ReprodSum          = 012;
FastReprodasum     = 014;
Reprodasum         = 015;
OneReductionasum   = 016;
OnlineExact        = 020;
HybridSum          = 021;
FastAccSum         = 022;
AccSum             = 023;
iFastSum           = 024;
Rsum               = 025;
Rasum              = 026;
SumK2              = 032;
SumK3              = 033;
Expansion2         = 052;
Expansion3         = 053;
Expansion4         = 054;
Expansion5         = 055;
Expansion6         = 056;
Expansion7         = 057;
Expansion8         = 058;
Expansion2EE       = 062;
Expansion3EE       = 063;
Expansion4EE       = 064;
Expansion5EE       = 065;
Expansion6EE       = 066;
Expansion7EE       = 067;
Expansion8EE       = 068;

ClassicDot           = 100;
ClassicDotCompatible = 101;
ClassicDotSSE2       = 102;
ClassicDotSSE3       = 103;
ClassicDotSSSE3      = 104;
ClassicDotSSE4_1     = 105;
ClassicDotSSE4_2     = 106;
ClassicDotAVX        = 107;
ClassicDotAVX2       = 108;
OneReductionDot      = 110;
FastReprodDot        = 111;
ReprodDot            = 112;
OnlineExactDot       = 120;
HybridSumDot         = 121;
FastAccSumDot        = 122;
AccSumDot            = 123;
iFastSumDot          = 124;
Rdot                 = 125;

Classicnrm2      = 200;
OneReductionnrm2 = 210;
FastReprodnrm2   = 211;
Reprodnrm2       = 212;
OnlineExactnrm2  = 220;
HybridSumnrm2    = 221;
FastAccSumnrm2   = 222;
AccSumnrm2       = 223;
iFastSumnrm2     = 224;
Rnrm2            = 225;

ClassicGemv      = 300;
OneReductionGemv = 310;
FastReprodGemv   = 311;
ReprodGemv       = 312;
OnlineExactGemv  = 320;
HybridSumGemv    = 321;
FastAccSumGemv   = 322;
AccSumGemv       = 323;
iFastSumGemv     = 324;
Rgemv            = 325;

ClassicTrsv           = 400;
ClassicTrsvCompatible = 401;
ClassicTrsvSSE2       = 402;
ClassicTrsvSSE3       = 403;
ClassicTrsvSSSE3      = 404;
ClassicTrsvSSE4_1     = 405;
ClassicTrsvSSE4_2     = 406;
ClassicTrsvAVX        = 407;
ClassicTrsvAVX2       = 408;
OneReductionTrsv      = 410;
FastReprodTrsv        = 411;
ReprodTrsv            = 412;
OnlineExactTrsv       = 420;
HybridSumTrsv         = 421;
FastAccSumTrsv        = 422;
AccSumTrsv            = 423;
iFastSumTrsv          = 424;
Rtrsv                 = 425;
XBLASTrsv             = 430;
OneReductionIRTrsv    = 440;

OnlineExactFloatTrsv  = 520;
HybridSumFloatTrsv    = 521;
FastAccSumFloatTrsv   = 522;
AccSumFloatTrsv       = 523;
iFastSumFloatTrsv     = 524;
RfloatTrsv            = 525;

ClassicGemm           = 600;
ClassicGemmCompatible = 601;
ClassicGemmSSE2       = 602;
ClassicGemmSSE3       = 603;
ClassicGemmSSSE3      = 604;
ClassicGemmSSE4_1     = 605;
ClassicGemmSSE4_2     = 606;
ClassicGemmAVX        = 607;
ClassicGemmAVX2       = 608;

mpiprocess = 0:7;
mpiprocess = 2 .^ mpiprocess;
mpiprocess = mpiprocess';
ompthreads = [1; 2; 4; 8; 16];

colors = ["-+k"; "-.*b"; "--xg"; ":oy"; "-.dr"; "--xy"; "--.c"; ":sk"];
compilers = ["icc"; "gcc"];
links = ["staticlink"; "dynamiclink"];

instructionSets = ["Machine A"; "Machine B"];
instructionSetsCodes = [AVX; AVX2];

algos = ["ClassicTrsv"; "OneReductionTrsv"; "OneReductionIRTrsv"];
codes = [ClassicTrsv; OneReductionTrsv; OneReductionIRTrsv];

%{
instructionsSets = ["SSE"; "AVX"; "AVX2"; "AVX512"]
instructionsSetsCodes = [SSE; AVX; AVX2; AVX512]
%}

