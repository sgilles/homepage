#
# *************************************************************************************
# Copyright (c) 2015, University of Perpignan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.
# *************************************************************************************
#


import os
import shlex
import re
import subprocess
import math
import sys

from optparse import OptionParser

compiler = "icc"
instructionset = "KNC"
link = "static"
linkoctave = "staticlink"

def generateResult(cond, size, algo, thomp):
	cmdline = ""
	times = " 128 "
	if (algo == "iFastSum" or algo == "AccSum" or algo == "FastAccSum" or algo == "HybridSum" or algo == "OnlineExact" or algo == "ReprodSum" or algo == "FastReprodSum" or algo == "ClassicSum" or algo == "Rsum" or algo == "OneReduction" or algo == "SumK2" or algo == "SumK3" or algo == "Rasum" or algo == "exactdasum" or algo == "Reprodasum" or algo == "OneReductionasum" or algo == "FastReprodasum" or algo == "Classicasum" or algo == "Classicnrm2" or algo == "Rnrm2" or algo == "FastReprodnrm2" or algo == "OneReductionnrm2" or algo == "exsum2" or algo == "exsum3" or algo == "exsum4" or algo == "exsum8" or algo == "exsum2EE" or algo == "exsum3EE" or algo == "exsum4EE" or algo == "exsum8EE" or algo == "HybridSumnrm2" or algo == "iFastSumnrm2" or algo == "OnlineExactnrm2"):
		filedirectory = "sum"
	elif (algo == "iFastSumDot" or algo == "AccSumDot" or algo == "FastAccSumDot" or algo == "HybridSumDot" or algo == "OnlineExactDot" or algo == "ReprodDot" or algo == "FastReprodDot" or algo == "ClassicDot" or algo == "ClassicDotCompatible" or algo == "ClassicDotSSE2" or algo == "ClassicDotSSE3" or algo == "ClassicDotSSSE3" or algo == "ClassicDotSSE4_1" or algo == "ClassicDotSSE4_2" or algo == "ClassicDotAVX" or algo == "ClassicDotAVX2" or algo == "Rdot" or algo == "OneReductionDot" or algo == "OneReductionTwoProdDot" or algo == "dot16" or algo == "dot8"):
		filedirectory = "dot"
	elif (algo == "iFastSumTrsv" or algo == "AccSumTrsv" or algo == "FastAccSumTrsv" or algo == "HybridSumTrsv" or algo == "OnlineExactTrsv" or algo == "ReprodTrsv" or algo == "FastReprodTrsv" or algo == "ClassicTrsv" or algo == "Rtrsv" or algo == "OneReductionTrsv"):
		filedirectory = "trsv"
	else:
		filedirectory = "gemv"
	programtorun = "./run/bin/" + compiler + "/" + link + "/mic/blas.out "
	programargs = algo + " " + str(size) + " " + str(cond) + " " + filedirectory + times + compiler + "," + linkoctave + "," + algo + "," + str(cond) + ",1," + str(thomp) + " " + instructionset + " " + str(thomp)
	cmdline = "micnativeloadex " + programtorun + " -a \"" + programargs + "\" -d 0"
	sys.stderr.write(cmdline + "\n")
	subprocess.call(cmdline, shell=True)

def generateResults(conds, sizes, algos, OMP_THREADS):
	sys.stdout.flush();
	for thomp in OMP_THREADS:
		for cond in conds:
			for algo in algos:
				for size in sizes:
					generateResult(cond, size, algo, thomp)

if __name__ == '__main__':
	sys.stdout.write("results = [\n")
#	sizes = map(lambda x: 10**x, range(3, 4))
#	sizes = map(lambda x: 5 * 10**x, range(5, 6))
	sizes = map(lambda x: 10**x, range(3, 9)) + map(lambda x: 2 * (10**x), range(3, 9)) + map(lambda x: 5 * (10**x), range(3, 8))
#	sizes = map(lambda x: 200 * x, range(1, 16))
	sizes.sort()
	conds = list(range(16, 17, 8))
	OMP_THREADS = list(range(240, 241, 1))
	algos = ["Rasum"]
#	algos = ["linearSolver"]
	generateResults(conds, sizes, algos, OMP_THREADS)
	sys.stdout.write("]\n")

