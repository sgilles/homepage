%{
# *************************************************************************************
# Copyright (c) 2015, University of Perpignan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.
# *************************************************************************************
#
%}
function ok = compareConditions(compiler, compilers, link, links, algos, codes, sizeCompare, process, threads, instructionSet, instructionSetCode, results, colors, col)
algoresults = results(:,[3, 4, col]);
conds = algoresults(algoresults(:,1) == codes(1), 2);
runtime = algoresults(algoresults(:,1) == codes(1), 3);
plot(conds, runtime, 'LineWidth', 1, strcat(colors(1,:), ";", algos(1, :), ";"));
figtitle = strcat("compare conditions for:", compilers(compiler + 1, :), ",\ ", links(link + 1, :), ",\ size=", int2str(sizeCompare), ",\ process=", int2str(process), ",\ threads=", int2str(threads), ",\ ", instructionSet);
title(figtitle);
hold on;
xlabel('log10(condition)','FontSize',16,'FontWeight','bold');
ylabel("runtime(cycles)",'FontSize',16,'FontWeight','bold');
legend({"OnlineExactDot","HybridSumDot","AccSumDot","FastAccSumDot"},'FontSize',18,'FontWeight','bold');
legend('Location','northwest');
set(gca,'FontSize',14);
axis([7 33 0 40000000]);
for algo = 2:size(algos, 1)
	conds = algoresults(algoresults(:,1) == codes(algo), 2);
	runtime = algoresults(algoresults(:,1) == codes(algo), 3);
	plot(conds, runtime, 'LineWidth', 1, strcat(colors(algo,:), ";", algos(algo, :), ";"));
end
print(strcat("figures/withtitle/condscompare-", compilers(compiler + 1,:), "-", links(link + 1,:), "-", int2str(sizeCompare), "-", int2str(process), "-", int2str(threads), "-", instructionSet, ".eps"), '-depsc2');
title("");
print(strcat("figures/notitle/condscompare-", compilers(compiler + 1,:), "-", links(link + 1,:), "-", int2str(sizeCompare), "-", int2str(process), "-", int2str(threads), "-", instructionSet, ".eps"), '-depsc2');

