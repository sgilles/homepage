/*
 *************************************************************************************
 Copyright (c) 2015, University of Perpignan
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************
*/


#include <math.h>
#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>
#include <mkl.h>
#include <immintrin.h>
#include <rtnblas.h>
//#include <reprodblas.h>
#include <summation.h>
#include <string.h>
#include <string>
#include <iostream>
//#include <linearalgebra.h>
//#include <blas_extended.h>

#ifdef OMP_PARALLEL
#include <omp.h>
#endif

#include "init.hpp"

int main(int argc, char** argv) {
  if (argc > 4) {
    double *X, *Y, *results;
    double *tempX, *tempY, *tempResults;
    double result;
    unsigned long size = atoi(argv[2]);
    
    if (!strcmp(argv[4], "dot")) {
      X = (double*) memalign(64, size * sizeof(double));
      Y = (double*) memalign(64, size * sizeof(double));
    }
    
    //linearAlgebraInitialise();
    char filename[100] = "";
    
    if (size > 100000) sprintf(filename, "./data%s/c%ss100000.dat", argv[4], argv[3]);
    else sprintf(filename, "./data%s/c%ss%s.dat", argv[4], argv[3], argv[2]);
    
    if (!strcmp(argv[4], "dot")) initGeneratedDotVector(X, Y, filename, size, &result);
    if (!strcmp(argv[4], "sum")) initGeneratedSumVector(X, filename, size, &result);
    
    if (!strcmp(argv[4], "dot")) {
      tempX = (double*) memalign(64, size * sizeof(double));
      tempY = (double*) memalign(64, size * sizeof(double));
      memcpy(tempX, X, size * sizeof(double));
      memcpy(tempY, Y, size * sizeof(double));
    }
    if (!strcmp(argv[4], "sum")) {
      tempX = (double*) memalign(64, size * sizeof(double));
      memcpy(tempX, X, size * sizeof(double));
    }
    
    unsigned int  iter = atoi(argv[5]);
    unsigned long cycles, reducecycles;
    unsigned long mincachecycles = 0;
    unsigned long meancachecycles = 0;
    unsigned long minmemorycycles = 0;
    unsigned long meanmemorycycles = 0;
    double s;
    double *sresults;
    
    if (!strcmp(argv[1], "ClassicDot")) {
      mkl_cbwr_set(MKL_CBWR_OFF);
      //      timingdot(classic_ddot)
      //#define timingdot(call)			\
      minmemorycycles = pow(2, 62);
      for (int i = 0; i < iter; i++) {
	streamVector(size, tempX, X);
	streamVector(size, tempY, Y);
	cycles = rdtsc();
	s = classic_ddot(size, X, 1, Y, 1);
	cycles = rdtsc() - cycles;
	minmemorycycles = (cycles < minmemorycycles) ? cycles : minmemorycycles;
	meanmemorycycles += cycles;
      }
      meanmemorycycles /= iter;

      // mincachecycles = pow(2, 62);					\
      // for (int i = 0; i < iter; i++) {					\
      // 	cycles = rdtsc();						\
      // 	//	s = call(size, X, 1, Y, 1);				\
      // 	s = classic_ddot(size, X, 1, Y, 1);					\
      // 	cycles = rdtsc() - cycles;					\
      // 	mincachecycles = (cycles < mincachecycles) ? cycles : mincachecycles; \
      // 	meancachecycles += cycles;					\
      // }									\
      // meancachecycles /= iter;      
    }

    if (!strcmp(argv[1], "Rdot")) {
      mkl_cbwr_set(MKL_CBWR_OFF);
      //      timingdot(classic_ddot)
      //#define timingdot(call)			\
      minmemorycycles = pow(2, 62);
      for (int i = 0; i < iter; i++) {
	streamVector(size, tempX, X);
	streamVector(size, tempY, Y);
	cycles = rdtsc();
	s = rtnblas_ddot(size, X, 1, Y, 1);
	cycles = rdtsc() - cycles;
	minmemorycycles = (cycles < minmemorycycles) ? cycles : minmemorycycles;
	meanmemorycycles += cycles;
      }
      meanmemorycycles /= iter;
      // mincachecycles = pow(2, 62);					\
      // for (int i = 0; i < iter; i++) {					\
      // 	cycles = rdtsc();						\
      // 	//	s = call(size, X, 1, Y, 1);				\
      // 	s = rtnblas_ddot(size, X, 1, Y, 1);					\
      // 	cycles = rdtsc() - cycles;					\
      // 	mincachecycles = (cycles < mincachecycles) ? cycles : mincachecycles; \
      // 	meancachecycles += cycles;					\
      // }									\
      // meancachecycles /= iter;
      
      
      //      timingdot(rtnblas_ddot)
	}

    // if (!strcmp(argv[1], "ClassicSum")) {
    //   timingsum(optimizedSum(size, X))
    // 	}
    // if (!strcmp(argv[1], "HybridSum")) {
    //   timingsum(hybridSum(size, X))
    // 	}
    // if (!strcmp(argv[1], "OnlineExact")) {
    //   timingsum(onlineExact(size, X))
    // 	}
    // if (!strcmp(argv[1], "AccSum")) {
    //   timingsum(accSum(size, X))
    // 	}
    // if (!strcmp(argv[1], "FastAccSum")) {
    //   timingsum(fastAccSum(size, X))
    // 	}
    // if (!strcmp(argv[1], "iFastSum")) {
    //   timingsum(iFastSum(3, X))
    // 	}
    // if (!strcmp(argv[1], "SumK2")) {
    //   timingsum(sumK2(size, X))
    // 	}
    // if (!strcmp(argv[1], "SumK3")) {
    //   timingsum(sumK3(size, X))
    // 	}
    // if (!strcmp(argv[1], "Rsum")) {
    //   timingsum(dynamicSum(size, X))
    // 	}

    // if (!strcmp(argv[1], "Classicasum")) {
    //   timingsum(absSum(size, X))
    // 	}
    // if (!strcmp(argv[1], "Rasum")) {
    //   timingsum(rtnblas_dasum(size, X, 1))
    // 	}

    // if (!strcmp(argv[4], "gemv")) {
    //   s = 0;
    //   result = 0;
    //   double diff = 0;
    //   for (unsigned int i = 0; i < size; i++) {
    // 	diff += fabs(results[i] - sresults[i]);
    // 	result += fabs(results[i]);
    //   }
    //   result = diff / result;
    // }
    // if (!strcmp(argv[4], "gemm")) {
    //   s = 0;
    //   result = 0;
    //   double diff = 0;
    //   for (unsigned int i = 0; i < size * size; i++) {
    // 	diff += fabs(results[i] - sresults[i]);
    // 	result += fabs(results[i]);
    //   }
    //   result = diff / result;
    // }


    printf(
	   "%s, %s, %.16g, %.16g, %lld, %lld, %lld, %lld, %s",
	   argv[6], argv[2], s, result, mincachecycles, meancachecycles, minmemorycycles, meanmemorycycles, argv[7]
	   );
    printf("\n");
  }
  else {
    fprintf(stderr, "Usage error : %s\nRun %s algo data\nalgo:the algorithme to use\ndata:the file of data", argv[0], argv[0]);
  }
  return 0;
}

