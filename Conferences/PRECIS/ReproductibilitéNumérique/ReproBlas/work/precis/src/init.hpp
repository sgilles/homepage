#define timingsum(call)\
minmemorycycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	streamVector(size, tempX, X);\
	cycles = rdtsc();\
	s = call;\
	cycles = rdtsc() - cycles;\
	minmemorycycles = (cycles < minmemorycycles) ? cycles : minmemorycycles;\
	meanmemorycycles += cycles;\
}\
meanmemorycycles /= iter;\
mincachecycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	copyVector(size, tempX, X);\
	cycles = rdtsc();\
	s = call;\
	cycles = rdtsc() - cycles;\
	mincachecycles = (cycles < mincachecycles) ? cycles : mincachecycles;\
	meancachecycles += cycles;\
}\
meancachecycles /= iter;
#define timingdot(call)\
minmemorycycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	streamVector(size, tempX, X);\
	streamVector(size, tempY, Y);\
	cycles = rdtsc();\
	s = call(size, X, 1, Y, 1);\
	cycles = rdtsc() - cycles;\
	minmemorycycles = (cycles < minmemorycycles) ? cycles : minmemorycycles;\
	meanmemorycycles += cycles;\
}\
meanmemorycycles /= iter;\
mincachecycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	cycles = rdtsc();\
	s = call(size, X, 1, Y, 1);\
	cycles = rdtsc() - cycles;\
	mincachecycles = (cycles < mincachecycles) ? cycles : mincachecycles;\
	meancachecycles += cycles;\
}\
meancachecycles /= iter;
#define timinggemv(call)\
minmemorycycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	streamVector(size * size, tempX, X);\
	streamVector(size, tempY, Y);\
	streamVector(size, tempResults, sresults);\
	cycles = rdtsc();\
	call(CblasRowMajor, CblasNoTrans, size, size, 1, X, size, Y, 1, 1, sresults, 1);\
	cycles = rdtsc() - cycles;\
	minmemorycycles = (cycles < minmemorycycles) ? cycles : minmemorycycles;\
	meanmemorycycles += cycles;\
}\
meanmemorycycles /= iter;\
mincachecycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	copyVector(size, tempResults, sresults);\
	cycles = rdtsc();\
	call(CblasRowMajor, CblasNoTrans, size, size, 1, X, size, Y, 1, 1, sresults, 1);\
	cycles = rdtsc() - cycles;\
	mincachecycles = (cycles < mincachecycles) ? cycles : mincachecycles;\
	meancachecycles += cycles;\
}\
meancachecycles /= iter;
#define timinggemm(call)\
minmemorycycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	streamVector(size * size, tempX, X);\
	streamVector(size * size, tempY, Y);\
	streamVector(size * size, tempResults, sresults);\
	cycles = rdtsc();\
	call(CblasRowMajor, CblasNoTrans, CblasNoTrans, size, size, size, 1, X, size, Y, size, 1, sresults, size);\
	cycles = rdtsc() - cycles;\
	minmemorycycles = (cycles < minmemorycycles) ? cycles : minmemorycycles;\
	meanmemorycycles += cycles;\
}\
meanmemorycycles /= iter;\
mincachecycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	copyVector(size * size, tempResults, sresults);\
	cycles = rdtsc();\
	call(CblasRowMajor, CblasNoTrans, CblasNoTrans, size, size, size, 1, X, size, Y, size, 1, sresults, size);\
	cycles = rdtsc() - cycles;\
	mincachecycles = (cycles < mincachecycles) ? cycles : mincachecycles;\
	meancachecycles += cycles;\
}\
meancachecycles /= iter;


// double verifiedexactdasum(unsigned long size, double* X) {
// 	for (unsigned long i = 0; i < size; i++) {
// 		X[i] = fabs(X[i]);
// 	}
// 	return dynamicSum(size, X);
// }

void initGeneratedSumVector(double* X, char* filename, unsigned long size, double* sum) {
	int r;
	double cond;
	unsigned long fileSize;
	FILE* file;
	file = fopen(filename, "rb");
	if (file == NULL) {
		printf("Can not open file %s\n", filename);
		exit(1);
	}
	r = fread(&fileSize, sizeof(unsigned long), 1, file);
	if (r == 0) {
		printf("Can not read size from file %s\n", filename);
		exit(1);
	}
	r = fread(X, sizeof(double), fileSize, file);
	if (r == 0) {
		printf("Can not read vector X from file %s\n", filename);
		exit(1);
	}
	r = fread(sum, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read sum from file %s\n", filename);
		exit(1);
	}
	r = fread(&cond, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read condition from file %s\n", filename);
		exit(1);
	}
	unsigned long times = size / fileSize;
	*sum *= times;
	for (unsigned long index = fileSize; index < size; index++) {
		unsigned long firstIndex = index % fileSize;
		X[index] = X[firstIndex];
	}
	fclose(file);
}

void initGeneratedDotVector(double* X, double* Y, char* filename, unsigned long size, double* dot) {
	int r;
	double cond;
	unsigned long fileSize;
	FILE* file;
	file = fopen(filename, "rb");
	if (file == NULL) {
		printf("Can not open file %s\n", filename);
		exit(1);
	}
	r = fread(&fileSize, sizeof(unsigned long), 1, file);
	if (r == 0) {
		printf("Can not read size from file %s\n", filename);
		exit(1);
	}
	r = fread(X, sizeof(double), fileSize, file);
	if (r == 0) {
		printf("Can not read the vector X from file %s\n", filename);
		exit(1);
	}
	r = fread(Y, sizeof(double), fileSize, file);
	if (r == 0) {
		printf("Can not read the vector Y from file %s\n", filename);
		exit(1);
	}
	r = fread(dot, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read the result from file %s\n", filename);
		exit(1);
	}
	r = fread(&cond, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read the condition from file %s\n", filename);
		exit(1);
	}
	fclose(file);
	for (unsigned long index = fileSize; index < size; index++) {
		unsigned long firstIndex = index % fileSize;
		X[index] = X[firstIndex];
		Y[index] = Y[firstIndex];
	}
	unsigned long times = size / fileSize;
	*dot *= times;
}

void initGeneratedGemvVector(double* X, double* Y, char* filename, unsigned long size, double* result) {
	int r;
	unsigned long fileSize;
	double Cmin, Cmax;
	unsigned int lines = (100 < size) ? 100 : size;
	unsigned int columns = size;
	FILE* file;
	file = fopen(filename, "rb");
	if (file == NULL) {
		printf("Can not open file %s\n", filename);
		exit(1);
	}
	r = fread(&fileSize, sizeof(unsigned long), 1, file);
	if (r == 0) {
		printf("Can not read size from file %s\n", filename);
		exit(1);
	}
	r = fread(X, sizeof(double), lines * columns, file);
	if (r == 0) {
		printf("Can not read matrix X from file %s\n", filename);
		exit(1);
	}
	r = fread(Y, sizeof(double), columns, file);
	if (r == 0) {
		printf("Can not read vector Y from file %s\n", filename);
		exit(1);
	}
	r = fread(result, sizeof(double), lines, file);
	if (r == 0) {
		printf("Can not read results from file %s\n", filename);
		exit(1);
	}
	r = fread(&Cmin, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read minimal condition from file %s\n", filename);
		exit(1);
	}
	r = fread(&Cmax, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read maximal condition from file %s\n", filename);
		exit(1);
	}
	for (unsigned long line = lines; line < size; line += lines) {
		memcpy(&X[line * columns], X, lines * columns * sizeof(double));
		memcpy(&result[line], result, lines * sizeof(double));
	}
	fclose(file);
}



void initGeneratedGemmVector(double* X, double* Y, char* filename, unsigned long size, double* result) {
	int r;
	unsigned long fileSize;
	double Cmin, Cmax;
	unsigned int lines = (100 < size) ? 100 : size;
	unsigned int columns = size;
	FILE* file;
	file = fopen(filename, "rb");
	if (file == NULL) {
		printf("Can not open file %s\n", filename);
		exit(1);
	}
	r = fread(&fileSize, sizeof(unsigned long), 1, file);
	if (r == 0) {
		printf("Can not read size from file %s\n", filename);
		exit(1);
	}
	r = fread(X, sizeof(double), lines * columns, file);
	if (r == 0) {
		printf("Can not read matrix X from file %s\n", filename);
		exit(1);
	}
	r = fread(Y, sizeof(double), columns, file);
	if (r == 0) {
		printf("Can not read vector Y from file %s\n", filename);
		exit(1);
	}
	r = fread(result, sizeof(double), lines, file);
	if (r == 0) {
		printf("Can not read results from file %s\n", filename);
		exit(1);
	}
	r = fread(&Cmin, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read minimal condition from file %s\n", filename);
		exit(1);
	}
	r = fread(&Cmax, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read maximal condition from file %s\n", filename);
		exit(1);
	}
	for (unsigned long line = lines; line < size; line += lines) {
		memcpy(&X[line * columns], X, lines * columns * sizeof(double));
		memcpy(&result[line], result, lines * sizeof(double));
	}
	for (unsigned long line = 1; line < size; line++) {
		memcpy(&Y[line * size], Y, size * sizeof(double));
		memcpy(&result[line * size], result, size * sizeof(double));
	}
	for (unsigned long line = 0; line < size; line++)
		for (unsigned long column = 0; column < size; column++) {
			Y[line * size + column] = Y[line * size + line];
			result[line * size + column] = result[line * size + line];
		}
	fclose(file);
}

// Utilisation de l'instruction _mm_stream_pd qui compatible à partir de SSE2
void allstreamVector(unsigned int n, double *src, double *dest) {
	for (int i = 0; i < n - 1; i += 2) _mm_stream_pd(&dest[i], *(__m128d*) &src[i]);
}

void streamVector(unsigned int n, double *src, double *dest) {
	for (int i = 0; i < n - 1; i += 2) _mm_stream_pd(&dest[i], *(__m128d*) &src[i]);
}

void copyVector(unsigned int n, double *src, double *dest) {
	for (int i = 0; i < n; i++) dest[i] = src[i];
}
