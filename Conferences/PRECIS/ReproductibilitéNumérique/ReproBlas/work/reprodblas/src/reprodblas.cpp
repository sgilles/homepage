/*
 *************************************************************************************
 Copyright (c) 2015, University of Perpignan
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************
*/

#include <immintrin.h>
#include <summation.h>
#include <reprodblas.h>
#include <rtnblas.h>
#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <fenv.h>
#include <mkl.h>

#ifdef STATS
static unsigned long localSumCycles = 0;
static unsigned long localMaxCycles = 0;
static unsigned long reduceMaxCycles = 0;

void getReprodBlasStatistics(unsigned long *paramLocalMaxCycles, unsigned long *paramReduceMaxCycles, unsigned long *paramLocalSumCycles) {
	*paramLocalSumCycles = localSumCycles;
	*paramLocalMaxCycles = localMaxCycles;
	*paramReduceMaxCycles = reduceMaxCycles;
}
#endif

#define ONEREDUCTIONK 3
#define CACHELINE 8
#define ONEREDUCTIONW 40
#define MANTISSASIZE 53
#define EXPONENTPOSITIVERANGESIZE 1024

typedef struct {
	double F[ONEREDUCTIONK];
	int P[ONEREDUCTIONK];
} IFP;

#ifdef MPI_PARALLEL
#include <mpi.h>
static int MPI_NUMBER_OF_THREADS;
static int myRank;
static MPI_Op MPI_ADD_IFP;

static void ADD_IFP(int *invec, int *inoutvec, int *len, MPI_Datatype *datatype) {
	IFP *inIFP = (IFP*) invec;
	IFP *outIFP = (IFP*) inoutvec;
	addIFP(outIFP->F, outIFP->P, inIFP->F, inIFP->P, outIFP->F, outIFP->P);
}
#endif

#ifdef OMP_PARALLEL
#include <omp.h>
static int OMP_NUMBER_OF_THREADS;
#endif

#ifdef SSE
#define PDSIZE 2
#define PDVECTOR __m128d
#define mulvector _mm_mul_pd
#define subvector _mm_sub_pd
#define addvector _mm_add_pd
#define andvector _mm_and_pd
#define maxvector _mm_max_pd
#define broadcast(adress) _mm_broadcastsd_pd(*(__m128d*) adress)
#define ZEROES _mm_setzero_pd()
#elif defined(AVX) || defined(AVX2)
#define PDSIZE 4
#define PDVECTOR __m256d
#define mulvector _mm256_mul_pd
#define subvector _mm256_sub_pd
#define addvector _mm256_add_pd
#define andvector _mm256_and_pd
#define maxvector _mm256_max_pd
#define broadcast(adress) _mm256_broadcast_sd(adress)
#define ZEROES _mm256_setzero_pd()
#elif defined(AVX512)
#define PDSIZE 8
#define PDVECTOR __m512d
#define mulvector _mm512_mul_pd
#define subvector _mm512_sub_pd
#define addvector _mm512_add_pd
#define absvector _mm512_abs_pd
#define maxvector _mm512_max_pd
PDVECTOR _mm512_broadcastsd_pd(__m128d);
#ifdef MIC
#define broadcast(adress) _mm512_extload_pd (adress, _MM_UPCONV_PD_NONE, _MM_BROADCAST_1X8, 0)
#else
#define broadcast(adress) _mm512_broadcastsd_pd(*(__m128d*) adress)
#endif
#define ZEROES _mm512_setzero_pd()
#endif

#if defined(AVX2)
#define fmaddvector _mm256_fmadd_pd
#define fmsubvector _mm256_fmsub_pd
#elif defined(AVX512)
#define fmaddvector _mm512_fmadd_pd
#define fmsubvector _mm512_fmsub_pd
#endif

#define reduceMax(vector,result)\
result = 0;\
for (unsigned int cnt = 0; cnt < PDSIZE; cnt++) result = (result < ((double*) &vector)[cnt]) ? ((double*) &vector)[cnt] : result;

static unsigned long ABS_MASK = 0x7FFFFFFFFFFFFFFF;
static PDVECTOR ABS_MASK_VECTOR;

static unsigned long SPLIT_MASK = 0xFFFFFFFFFC000000;
static double eps;
static double SPLIT_FACTOR;
static PDVECTOR SPLIT_FACTOR_VECTOR;

void reprodblasInitialise() {
	summationInitialise();
	ABS_MASK_VECTOR = broadcast((double*) &ABS_MASK);
	eps = pow(2, -53);
	SPLIT_FACTOR = pow(2, 27) + 1;
	SPLIT_FACTOR_VECTOR = broadcast(&SPLIT_FACTOR);

#ifdef OMP_PARALLEL
	OMP_NUMBER_OF_THREADS = atoi(getenv("OMP_NUM_THREADS"));
#endif

#ifdef MPI_PARALLEL
	MPI_Comm_size(MPI_COMM_WORLD, &MPI_NUMBER_OF_THREADS);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
	MPI_Op_create((MPI_User_function *) ADD_IFP, 1, &MPI_ADD_IFP);
#endif

}

double sequentialMultMax(unsigned long int size, double *X, int IncX, double *Y, int IncY, double *res) {
	PDVECTOR AbsMaxVector = ZEROES;
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&X[32]);
		__builtin_prefetch(&Y[32]);
		__builtin_prefetch(&res[32], 1);
		*(PDVECTOR*) &res[0] = mulvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);
#if defined(AVX) || defined(AVX2) || defined(SSE)
		*(PDVECTOR*) &res[4] = mulvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4]);
#endif
#if defined(SSE)
		*(PDVECTOR*) &res[2] = mulvector(*(PDVECTOR*) &X[2], *(PDVECTOR*) &Y[2]);
		*(PDVECTOR*) &res[6] = mulvector(*(PDVECTOR*) &X[6], *(PDVECTOR*) &Y[6]);
#endif
#if defined(AVX512)
		AbsMaxVector = maxvector(*(PDVECTOR*) &AbsMaxVector, absvector(*(PDVECTOR*) &res[0]));
#elif defined(AVX) || defined(AVX2) || defined(SSE)
		AbsMaxVector = maxvector(*(PDVECTOR*) &AbsMaxVector, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res[0]));
		AbsMaxVector = maxvector(*(PDVECTOR*) &AbsMaxVector, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res[4]));
#endif
#if defined(SSE)
		AbsMaxVector = maxvector(*(PDVECTOR*) &AbsMaxVector, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res[0]));
		AbsMaxVector = maxvector(*(PDVECTOR*) &AbsMaxVector, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res[2]));
		AbsMaxVector = maxvector(*(PDVECTOR*) &AbsMaxVector, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res[4]));
		AbsMaxVector = maxvector(*(PDVECTOR*) &AbsMaxVector, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res[6]));
#endif
		X += CACHELINE;
		Y += CACHELINE;
		res += CACHELINE;
	}
	double reduceMax(AbsMaxVector, maxAbs);
	double absMax;
	for (;i < size; i++) {
		res[0] = X[0] * Y[0];
		absMax = fabs(res[0]);
		maxAbs = (maxAbs > absMax) ? maxAbs : absMax;
		X++;
		Y++;
		res++;
	}
	return maxAbs;
}

#ifdef SSE
#define splitScallers(X,High,Low,Temp)\
*(PDVECTOR*) &Temp[0] = mulvector(*(PDVECTOR*) &X[0], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[0] = subvector(*(PDVECTOR*) &Temp[0], subvector(*(PDVECTOR*) &Temp[0], *(PDVECTOR*) &X[0]));\
*(PDVECTOR*) &Low[0] = subvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &High[0]);\
*(PDVECTOR*) &Temp[2] = mulvector(*(PDVECTOR*) &X[2], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[2] = subvector(*(PDVECTOR*) &Temp[2], subvector(*(PDVECTOR*) &Temp[2], *(PDVECTOR*) &X[2]));\
*(PDVECTOR*) &Low[2] = subvector(*(PDVECTOR*) &X[2], *(PDVECTOR*) &High[2]);\
*(PDVECTOR*) &Temp[4] = mulvector(*(PDVECTOR*) &X[4], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[4] = subvector(*(PDVECTOR*) &Temp[4], subvector(*(PDVECTOR*) &Temp[4], *(PDVECTOR*) &X[4]));\
*(PDVECTOR*) &Low[4] = subvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &High[4]);\
*(PDVECTOR*) &Temp[6] = mulvector(*(PDVECTOR*) &X[6], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[6] = subvector(*(PDVECTOR*) &Temp[6], subvector(*(PDVECTOR*) &Temp[6], *(PDVECTOR*) &X[6]));\
*(PDVECTOR*) &Low[6] = subvector(*(PDVECTOR*) &X[6], *(PDVECTOR*) &High[6]);
#define twoProd(X,Y,mul,error,XH,XL,XT,YH,YL,YT,Temp)\
*(PDVECTOR*) &mul[0] = mulvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &mul[2] = mulvector(*(PDVECTOR*) &X[2], *(PDVECTOR*) &Y[2]);\
*(PDVECTOR*) &mul[4] = mulvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4]);\
*(PDVECTOR*) &mul[6] = mulvector(*(PDVECTOR*) &X[6], *(PDVECTOR*) &Y[6]);\
splitScallers(X,XH,XL,Temp)\
splitScallers(Y,YH,YL,Temp)\
*(PDVECTOR*) &XT[0] = subvector(mulvector(*(PDVECTOR*) &XH[0], *(PDVECTOR*) &YH[0]), *(PDVECTOR*) &mul[0]);\
*(PDVECTOR*) &YT[0] = addvector(mulvector(*(PDVECTOR*) &XH[0], *(PDVECTOR*) &YL[0]), *(PDVECTOR*) &XT[0]);\
*(PDVECTOR*) &XT[0] = addvector(mulvector(*(PDVECTOR*) &XL[0], *(PDVECTOR*) &YH[0]), *(PDVECTOR*) &YT[0]);\
*(PDVECTOR*) &error[0] = addvector(mulvector(*(PDVECTOR*) &XL[0], *(PDVECTOR*) &YL[0]), *(PDVECTOR*) &XT[0]);\
*(PDVECTOR*) &XT[2] = subvector(mulvector(*(PDVECTOR*) &XH[2], *(PDVECTOR*) &YH[2]), *(PDVECTOR*) &mul[2]);\
*(PDVECTOR*) &YT[2] = addvector(mulvector(*(PDVECTOR*) &XH[2], *(PDVECTOR*) &YL[2]), *(PDVECTOR*) &XT[2]);\
*(PDVECTOR*) &XT[2] = addvector(mulvector(*(PDVECTOR*) &XL[2], *(PDVECTOR*) &YH[2]), *(PDVECTOR*) &YT[2]);\
*(PDVECTOR*) &error[2] = addvector(mulvector(*(PDVECTOR*) &XL[2], *(PDVECTOR*) &YL[2]), *(PDVECTOR*) &XT[2]);\
*(PDVECTOR*) &XT[4] = subvector(mulvector(*(PDVECTOR*) &XH[4], *(PDVECTOR*) &YH[4]), *(PDVECTOR*) &mul[4]);\
*(PDVECTOR*) &YT[4] = addvector(mulvector(*(PDVECTOR*) &XH[4], *(PDVECTOR*) &YL[4]), *(PDVECTOR*) &XT[4]);\
*(PDVECTOR*) &XT[4] = addvector(mulvector(*(PDVECTOR*) &XL[4], *(PDVECTOR*) &YH[4]), *(PDVECTOR*) &YT[4]);\
*(PDVECTOR*) &error[4] = addvector(mulvector(*(PDVECTOR*) &XL[4], *(PDVECTOR*) &YL[4]), *(PDVECTOR*) &XT[4]);\
*(PDVECTOR*) &XT[6] = subvector(mulvector(*(PDVECTOR*) &XH[6], *(PDVECTOR*) &YH[6]), *(PDVECTOR*) &mul[6]);\
*(PDVECTOR*) &YT[6] = addvector(mulvector(*(PDVECTOR*) &XH[6], *(PDVECTOR*) &YL[6]), *(PDVECTOR*) &XT[6]);\
*(PDVECTOR*) &XT[6] = addvector(mulvector(*(PDVECTOR*) &XL[6], *(PDVECTOR*) &YH[6]), *(PDVECTOR*) &YT[6]);\
*(PDVECTOR*) &error[6] = addvector(mulvector(*(PDVECTOR*) &XL[6], *(PDVECTOR*) &YL[6]), *(PDVECTOR*) &XT[6]);
#elif defined(AVX)
#define splitScallers(X,High,Low,Temp)\
*(PDVECTOR*) &Temp[0] = mulvector(*(PDVECTOR*) &X[0], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[0] = subvector(*(PDVECTOR*) &Temp[0], subvector(*(PDVECTOR*) &Temp[0], *(PDVECTOR*) &X[0]));\
*(PDVECTOR*) &Low[0] = subvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &High[0]);\
*(PDVECTOR*) &Temp[4] = mulvector(*(PDVECTOR*) &X[4], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[4] = subvector(*(PDVECTOR*) &Temp[4], subvector(*(PDVECTOR*) &Temp[4], *(PDVECTOR*) &X[4]));\
*(PDVECTOR*) &Low[4] = subvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &High[4]);
#define twoProd(X,Y,mul,error,XH,XL,XT,YH,YL,YT,Temp)\
*(PDVECTOR*) &mul[0] = mulvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &mul[4] = mulvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4]);\
splitScallers(X,XH,XL,Temp)\
splitScallers(Y,YH,YL,Temp)\
*(PDVECTOR*) &XT[0] = subvector(mulvector(*(PDVECTOR*) &XH[0], *(PDVECTOR*) &YH[0]), *(PDVECTOR*) &mul[0]);\
*(PDVECTOR*) &YT[0] = addvector(mulvector(*(PDVECTOR*) &XH[0], *(PDVECTOR*) &YL[0]), *(PDVECTOR*) &XT[0]);\
*(PDVECTOR*) &XT[0] = addvector(mulvector(*(PDVECTOR*) &XL[0], *(PDVECTOR*) &YH[0]), *(PDVECTOR*) &YT[0]);\
*(PDVECTOR*) &error[0] = addvector(mulvector(*(PDVECTOR*) &XL[0], *(PDVECTOR*) &YL[0]), *(PDVECTOR*) &XT[0]);\
*(PDVECTOR*) &XT[4] = subvector(mulvector(*(PDVECTOR*) &XH[4], *(PDVECTOR*) &YH[4]), *(PDVECTOR*) &mul[4]);\
*(PDVECTOR*) &YT[4] = addvector(mulvector(*(PDVECTOR*) &XH[4], *(PDVECTOR*) &YL[4]), *(PDVECTOR*) &XT[4]);\
*(PDVECTOR*) &XT[4] = addvector(mulvector(*(PDVECTOR*) &XL[4], *(PDVECTOR*) &YH[4]), *(PDVECTOR*) &YT[4]);\
*(PDVECTOR*) &error[4] = addvector(mulvector(*(PDVECTOR*) &XL[4], *(PDVECTOR*) &YL[4]), *(PDVECTOR*) &XT[4]);
#elif defined(AVX2)
#define splitScallers(X,High,Low,Temp)\
*(PDVECTOR*) &Temp[0] = mulvector(*(PDVECTOR*) &X[0], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[0] = subvector(*(PDVECTOR*) &Temp[0], subvector(*(PDVECTOR*) &Temp[0], *(PDVECTOR*) &X[0]));\
*(PDVECTOR*) &Low[0] = subvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &High[0]);\
*(PDVECTOR*) &Temp[4] = mulvector(*(PDVECTOR*) &X[4], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[4] = subvector(*(PDVECTOR*) &Temp[4], subvector(*(PDVECTOR*) &Temp[4], *(PDVECTOR*) &X[4]));\
*(PDVECTOR*) &Low[4] = subvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &High[4]);
#define twoProd(X,Y,mul,error)\
*(PDVECTOR*) &mul[0] = mulvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &error[0] = fmsubvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0], *(PDVECTOR*) &mul[0]);\
*(PDVECTOR*) &mul[4] = mulvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4]);\
*(PDVECTOR*) &error[4] = fmsubvector(*(PDVECTOR*) &X[4], *(PDVECTOR*) &Y[4], *(PDVECTOR*) &mul[4]);
#elif defined(AVX512)
#define splitScallers(X,High,Low,Temp)\
*(PDVECTOR*) &Temp[0] = mulvector(*(PDVECTOR*) &X[0], SPLIT_FACTOR_VECTOR);\
*(PDVECTOR*) &High[0] = subvector(*(PDVECTOR*) &Temp[0], subvector(*(PDVECTOR*) &Temp[0], *(PDVECTOR*) &X[0]));\
*(PDVECTOR*) &Low[0] = subvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &High[0]);
#define twoProd(X,Y,mul,error)\
*(PDVECTOR*) &mul[0] = mulvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0]);\
*(PDVECTOR*) &error[0] = fmsubvector(*(PDVECTOR*) &X[0], *(PDVECTOR*) &Y[0], *(PDVECTOR*) &mul[0]);
#endif

#define splitScaller(X,High,Low,Temp)\
Temp[0] = X[0] * SPLIT_FACTOR;\
High[0] = Temp[0] - (Temp[0] - X[0]);\
Low[0] = X[0] - High[0];

#if defined(AVX2) || (defined(AVX512) && not defined(MIC))
#define twoProdScaller(X,Y,mul,error)\
mul[0] = X[0] * Y[0];\
error[0] = X[0];\
*(__m128d*) &error[0] = _mm_fmadd_sd(*(__m128d*) &error[0], *(__m128d*) &Y[0], *(__m128d*) &mul[0]);
#else
#define twoProdScaller(X,Y,mul,error,XH,XL,XT,YH,YL,YT,Temp)\
mul[0] = X[0] * Y[0];\
splitScaller(X,XH,XL,Temp)\
splitScaller(Y,YH,YL,Temp)\
XT[0] = (XH[0] * YH[0]) - mul[0];\
YT[0] = (XH[0] * YL[0]) + XT[0];\
XT[0] = (XL[0] * YH[0]) + YT[0];\
error[0] = (XL[0] * YL[0]) + XT[0];
#endif

double sequentialTwoProdMax(unsigned long size, unsigned long gsize, double *X, double *Y, double *res1, double *res2) {
#if (not defined(AVX2) && not defined(AVX512)) || defined(MIC)
	double __declspec(align(64)) XH[CACHELINE];
	double __declspec(align(64)) XL[CACHELINE];
	double __declspec(align(64)) YH[CACHELINE];
	double __declspec(align(64)) YL[CACHELINE];
	double __declspec(align(64)) XT[CACHELINE];
	double __declspec(align(64)) YT[CACHELINE];
	double __declspec(align(64)) temp[CACHELINE];
#endif
	PDVECTOR absMax = ZEROES;
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&X[8]);
		__builtin_prefetch(&Y[8]);
		__builtin_prefetch(&res1[8], 1);
		__builtin_prefetch(&res2[8], 1);
#if defined(AVX2) || defined(AVX512)
		twoProd(X,Y,res1,res2)
#else
		twoProd(X,Y,res1,res2,XH,XL,XT,YH,YL,YT,temp)
#endif

#ifdef SSE
		absMax = maxvector(absMax, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res1[0]));
		absMax = maxvector(absMax, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res1[2]));
		absMax = maxvector(absMax, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res1[4]));
		absMax = maxvector(absMax, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res1[6]));
#elif defined(AVX2) || defined(AVX)
		absMax = maxvector(absMax, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res1[0]));
		absMax = maxvector(absMax, andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &res1[4]));
#elif defined(AVX512)
		absMax = maxvector(absMax, absvector(*(PDVECTOR*) &res1[0]));
#endif
		X += CACHELINE;
		Y += CACHELINE;
		res1 += CACHELINE;
		res2 += CACHELINE;
	}
	double reduceMax(absMax, max);
	double actMax;
	for (; i < size; i++) {
#if defined(AVX2) || (defined(AVX512) && not defined(MIC))
		twoProdScaller(X,Y,res1,res2)
#else
		twoProdScaller(X,Y,res1,res2,XH,XL,XT,YH,YL,YT,temp)
#endif
		actMax = fabs(res1[0]);
		max = (actMax > max) ? actMax : max;
		X++;
		Y++;
		res1++;
		res2++;
	}
	return max;
}

double sequential_rs_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY) {
	double *res = (double*) memalign(64, size * sizeof(double));
	double maxAbs = sequentialMultMax(size, X, IncX, Y, IncY, res);
#ifdef STATS
	localMaxCycles = rdtsc();
#endif
	double sigma = (maxAbs * size) / (1 - (2 * size * eps));
	double S = sequentialReprodSumCallBack(size, res, sigma);
	free(res);
	return S;
}

double sequential_frs_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY) {
	double *res = (double*) memalign(64, size * sizeof(double));
	double maxAbs = sequentialMultMax(size, X, IncX, Y, IncY, res);
#ifdef STATS
	localMaxCycles = rdtsc();
#endif
	double sigma = (maxAbs * size) / (1 - (2 * size * eps));
	double S = sequentialFastReprodSumCallBack(size, res, sigma);
	free(res);
	return S;
}

void sequential_or_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY, double* result, int* propagate) {
	double Msize = EXPONENTPOSITIVERANGESIZE / ONEREDUCTIONW;
	for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
		result[k] = 1.5 * pow(2, (ONEREDUCTIONK - k + 1 - Msize) * ONEREDUCTIONW);
		propagate[k] = 0;
	}
	unsigned int step = pow(2, ((MANTISSASIZE - ONEREDUCTIONW) - 3));
	double* res = (double*) memalign(64, step * sizeof(double));
	for (unsigned long start = 0; start < size; start += step) {
		__builtin_prefetch(&X[step], 0, _MM_HINT_T1);
		__builtin_prefetch(&Y[step], 0, _MM_HINT_T1);
		double localSize = (step < size - start) ? step : size - start;
		double max = sequentialMultMax(localSize, X, IncX, Y, IncY, res);
		while ((ulp(result[0]) * pow(2, ONEREDUCTIONW)) <= max) {
			for (unsigned int k = ONEREDUCTIONK - 1; k > 0; k--) {
				result[k] = result[k - 1];
				propagate[k] = propagate[k - 1];
			}
			result[0] = 1.5 * pow(2, ONEREDUCTIONW) * ufp(result[0]);
			propagate[0] = 0;
		}
		extractVectorOneReduction(localSize, res, result);
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			double ufpk = ufp(result[k]);
			if (result[k] >= 1.75 * ufpk) {
				result[k] -= 0.25 * ufpk;
				propagate[k]++;
			}
			else if (result[k] < 1.25 * ufpk) {
				result[k] += 0.5 * ufpk;
				propagate[k] -= 2;
			}
			else if (result[k] < 1.5 * ufpk) {
				result[k] += 0.25 * ufpk;
				propagate[k]--;
			}
		}
		X += step;
		Y += step;
	}
	free(res);
}

void sequential_orlight_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY, double* result, int* propagate) {
	double Msize = EXPONENTPOSITIVERANGESIZE / ONEREDUCTIONW;
	for (unsigned int k = 0; k < ONEREDUCTIONK - 1; k++) {
		result[k] = 1.5 * pow(2, (ONEREDUCTIONK - k - Msize) * ONEREDUCTIONW);
		propagate[k] = 0;
	}
	unsigned int step = pow(2, ((MANTISSASIZE - ONEREDUCTIONW) - 3));
	double* res = (double*) memalign(64, step * sizeof(double));
	for (unsigned long start = 0; start < size; start += step) {
		__builtin_prefetch(&X[step], 0, _MM_HINT_T1);
		__builtin_prefetch(&Y[step], 0, _MM_HINT_T1);
		double localSize = (step < size - start) ? step : size - start;
		double max = sequentialMultMax(localSize, X, IncX, Y, IncY, res);
		while ((ulp(result[0]) * pow(2, ONEREDUCTIONW)) <= max) {
			for (unsigned int k = ONEREDUCTIONK - 2; k > 0; k--) {
				result[k] = result[k - 1];
				propagate[k] = propagate[k - 1];
			}
			result[0] = 1.5 * pow(2, ONEREDUCTIONW) * ufp(result[0]);
			propagate[0] = 0;
		}
		extractVectorOneReductionLight(localSize, res, result);
		for (unsigned int k = 0; k < ONEREDUCTIONK - 1; k++) {
			double ufpk = ufp(result[k]);
			if (result[k] >= 1.75 * ufpk) {
				result[k] -= 0.25 * ufpk;
				propagate[k]++;
			}
			else if (result[k] < 1.25 * ufpk) {
				result[k] += 0.5 * ufpk;
				propagate[k] -= 2;
			}
			else if (result[k] < 1.5 * ufpk) {
				result[k] += 0.25 * ufpk;
				propagate[k]--;
			}
		}
		X += step;
		Y += step;
	}
	free(res);
}

void sequential_or_ddotcontinue(unsigned long int size, double *X, int IncX, double *Y, int IncY, double* result, int* propagate) {
	double Msize = EXPONENTPOSITIVERANGESIZE / ONEREDUCTIONW;
	unsigned int step = pow(2, ((MANTISSASIZE - ONEREDUCTIONW) - 3));
	double* res = (double*) memalign(64, step * sizeof(double));
	for (unsigned long start = 0; start < size; start += step) {
		__builtin_prefetch(&X[step], 0, _MM_HINT_T1);
		__builtin_prefetch(&Y[step], 0, _MM_HINT_T1);
		double localSize = (step < size - start) ? step : size - start;
		double max = sequentialMultMax(localSize, X, IncX, Y, IncY, res);
		while ((ulp(result[0]) * pow(2, ONEREDUCTIONW)) <= max) {
			for (unsigned int k = ONEREDUCTIONK - 1; k > 0; k--) {
				result[k] = result[k - 1];
				propagate[k] = propagate[k - 1];
			}
			result[0] = 1.5 * pow(2, ONEREDUCTIONW) * ufp(result[0]);
			propagate[0] = 0;
		}
		extractVectorOneReduction(localSize, res, result);
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			double ufpk = ufp(result[k]);
			if (result[k] >= 1.75 * ufpk) {
				result[k] -= 0.25 * ufpk;
				propagate[k]++;
			}
			else if (result[k] < 1.25 * ufpk) {
				result[k] += 0.5 * ufpk;
				propagate[k] -= 2;
			}
			else if (result[k] < 1.5 * ufpk) {
				result[k] += 0.25 * ufpk;
				propagate[k]--;
			}
		}
		X += step;
		Y += step;
	}
	free(res);
}

void sequential_orlight_ddotcontinue(unsigned long int size, double *X, int IncX, double *Y, int IncY, double* result, int* propagate) {
	double Msize = EXPONENTPOSITIVERANGESIZE / ONEREDUCTIONW;
	unsigned int step = pow(2, ((MANTISSASIZE - ONEREDUCTIONW) - 3));
	double* res = (double*) memalign(64, step * sizeof(double));
	for (unsigned long start = 0; start < size; start += step) {
		__builtin_prefetch(&X[step], 0, _MM_HINT_T1);
		__builtin_prefetch(&Y[step], 0, _MM_HINT_T1);
		double localSize = (step < size - start) ? step : size - start;
		double max = sequentialMultMax(localSize, X, IncX, Y, IncY, res);
		while ((ulp(result[0]) * pow(2, ONEREDUCTIONW)) <= max) {
			for (unsigned int k = ONEREDUCTIONK - 2; k > 0; k--) {
				result[k] = result[k - 1];
				propagate[k] = propagate[k - 1];
			}
			result[0] = 1.5 * pow(2, ONEREDUCTIONW) * ufp(result[0]);
			propagate[0] = 0;
		}
		extractVectorOneReductionLight(localSize, res, result);
		for (unsigned int k = 0; k < ONEREDUCTIONK - 1; k++) {
			double ufpk = ufp(result[k]);
			if (result[k] >= 1.75 * ufpk) {
				result[k] -= 0.25 * ufpk;
				propagate[k]++;
			}
			else if (result[k] < 1.25 * ufpk) {
				result[k] += 0.5 * ufpk;
				propagate[k] -= 2;
			}
			else if (result[k] < 1.5 * ufpk) {
				result[k] += 0.25 * ufpk;
				propagate[k]--;
			}
		}
		X += step;
		Y += step;
	}
	free(res);
}

void sequential_ortp_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY, double* result, int* propagate) {
	double Msize = EXPONENTPOSITIVERANGESIZE / ONEREDUCTIONW;
	for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
		result[k] = 1.5 * pow(2, (ONEREDUCTIONK - k + 1 - Msize) * ONEREDUCTIONW);
		propagate[k] = 0;
	}
	unsigned int step = pow(2, ((MANTISSASIZE - ONEREDUCTIONW) - 4));
	double* res1 = (double*) memalign(64, step * sizeof(double));
	double* res2 = (double*) memalign(64, step * sizeof(double));
	for (unsigned long start = 0; start < size; start += step) {
		__builtin_prefetch(&X[step], 0, _MM_HINT_T1);
		__builtin_prefetch(&Y[step], 0, _MM_HINT_T1);
		double localSize = (step < size - start) ? step : size - start;
		double max = sequentialTwoProdMax(localSize, localSize, X, Y, res1, res2);
		while ((ulp(result[0]) * pow(2, ONEREDUCTIONW)) <= max) {
			for (unsigned int k = ONEREDUCTIONK - 1; k > 0; k--) {
				result[k] = result[k - 1];
				propagate[k] = propagate[k - 1];
			}
			result[0] = 1.5 * pow(2, ONEREDUCTIONW) * ufp(result[0]);
			propagate[0] = 0;
		}
		extractVectorOneReduction(localSize, res1, result);
		extractVectorOneReduction(localSize, res2, result);
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			double ufpk = ufp(result[k]);
			if (result[k] >= 1.75 * ufpk) {
				result[k] -= 0.25 * ufpk;
				propagate[k]++;
			}
			else if (result[k] < 1.25 * ufpk) {
				result[k] += 0.5 * ufpk;
				propagate[k] -= 2;
			}
			else if (result[k] < 1.5 * ufpk) {
				result[k] += 0.25 * ufpk;
				propagate[k]--;
			}
		}
		X += step;
		Y += step;
	}
	free(res1);
	free(res2);
}

void sequential_ortp_ddotcontinue(unsigned long int size, double *X, int IncX, double *Y, int IncY, double* result, int* propagate) {
	double Msize = EXPONENTPOSITIVERANGESIZE / ONEREDUCTIONW;
	unsigned int step = pow(2, ((MANTISSASIZE - ONEREDUCTIONW) - 4));
	double* res1 = (double*) memalign(64, step * sizeof(double));
	double* res2 = (double*) memalign(64, step * sizeof(double));
	for (unsigned long start = 0; start < size; start += step) {
		__builtin_prefetch(&X[step], 0, _MM_HINT_T1);
		__builtin_prefetch(&Y[step], 0, _MM_HINT_T1);
		double localSize = (step < size - start) ? step : size - start;
		double max = sequentialTwoProdMax(localSize, localSize, X, Y, res1, res2);
		while ((ulp(result[0]) * pow(2, ONEREDUCTIONW)) <= max) {
			for (unsigned int k = ONEREDUCTIONK - 1; k > 0; k--) {
				result[k] = result[k - 1];
				propagate[k] = propagate[k - 1];
			}
			result[0] = 1.5 * pow(2, ONEREDUCTIONW) * ufp(result[0]);
			propagate[0] = 0;
		}
		extractVectorOneReduction(localSize, res1, result);
		extractVectorOneReduction(localSize, res2, result);
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			double ufpk = ufp(result[k]);
			if (result[k] >= 1.75 * ufpk) {
				result[k] -= 0.25 * ufpk;
				propagate[k]++;
			}
			else if (result[k] < 1.25 * ufpk) {
				result[k] += 0.5 * ufpk;
				propagate[k] -= 2;
			}
			else if (result[k] < 1.5 * ufpk) {
				result[k] += 0.25 * ufpk;
				propagate[k]--;
			}
		}
		X += step;
		Y += step;
	}
	free(res1);
	free(res2);
}

#define reduceVector(vector,result)\
result = 0;\
for (unsigned int cnt = 0; cnt < PDSIZE; cnt++) result += ((double*) &vector)[cnt];

#define extractscallers(q,sigma,A,S)\
q = subvector(addvector(sigma,*(PDVECTOR*) A), sigma);\
S = addvector(q,S);\
*(PDVECTOR*) A = subvector(*(PDVECTOR*) A, q);
#define reprodextractscallers(q,sigma,A,S,sigmat,St)\
extractscallers(q,sigma,A,S)\
q = subvector(addvector(sigmat,*(PDVECTOR*) A), sigmat);\
St = addvector(q,St);
void sequentialReprodasumCallBack(unsigned long size, unsigned long gsize, double* A, double sigma, double *fFold, double *sFold) {
	PDVECTOR q;
	PDVECTOR St = ZEROES;
	PDVECTOR Sts = ZEROES;
	double M = nextPower2(sigma);
	PDVECTOR MVector = broadcast(&M);
	sigma = (gsize * 2 * eps * M) / (1 - (2 * gsize * eps));
	double MF = M;
	M = nextPower2(sigma);
	PDVECTOR MVectors = broadcast(&M);
	double a[PDSIZE] __attribute__ ((aligned(64))) = {0};
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&A[CACHELINE], 0, _MM_HINT_T0);
#if defined(AVX512)
		*(PDVECTOR*) a = absvector(*(PDVECTOR*) A);
#else
		*(PDVECTOR*) a = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) A);
#endif
		reprodextractscallers(q,MVector,a,St,MVectors,Sts)
		A += PDSIZE;
#if defined(AVX) || defined(AVX2) || defined(SSE)
		*(PDVECTOR*) a = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) A);
		reprodextractscallers(q,MVector,a,St,MVectors,Sts)
		A += PDSIZE;
#endif
#if defined(SSE)
		*(PDVECTOR*) a = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) A);
		reprodextractscallers(q,MVector,a,St,MVectors,Sts)
		A += PDSIZE;
		*(PDVECTOR*) a = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) A);
		reprodextractscallers(q,MVector,a,St,MVectors,Sts)
		A += PDSIZE;
#endif
	}
	reduceVector(St,*fFold)
	reduceVector(Sts,*sFold)
	double dq, da;
	for (; i < size; i++) {
		da = A[0];
		dq = (MF + da) - MF;
		*fFold += dq;
		da -= dq;
		dq = (M + da) - M;
		*sFold += dq;
		A++;
	}
}

double sequentialReprodasumCallBack(unsigned long size, double* A, double sigma) {
	if (sigma == 0) {
		double max = sequentialAbsMax(size, A);
		sigma = (max * size) / (1 - (2 * size * eps));
	}
	double fFold, sFold;
	sequentialReprodasumCallBack(size, size, A, sigma, &fFold, &sFold);
	return fFold + sFold;
}

#define extractscallersnew(sigmapr,A,sigma)\
sigmapr = addvector(*(PDVECTOR*) A, sigma);\
*(PDVECTOR*) A = subvector(*(PDVECTOR*) A,subvector(sigmapr, sigma));\
sigma = sigmapr;
#define reprodextractscallersnew(sigma,A,sigmapr,sigmas)\
extractscallersnew(sigma,A,sigmapr)\
sigmas = addvector(sigmas,*(PDVECTOR*) A);
void sequentialFastReprodasumCallBack(unsigned long size, unsigned long gsize, double* A, double sigma, double *fFold, double *sFold) {
	int originalRoundingMode = fegetround();
	fesetround(FE_TOWARDZERO);
	PDVECTOR St, Sts;
	double M = 3 * nextPower2(sigma);
	PDVECTOR Minit = broadcast(&M);
	PDVECTOR Mprev = Minit;
	PDVECTOR MVector = Minit;
	sigma = (gsize * 4 * eps * (M / 3)) / (1 - (4 * (gsize + 1) * eps));
	M = 3 * nextPower2(sigma);
	PDVECTOR Minits = broadcast(&M);
	PDVECTOR MVectors = Minits;
	double a[PDSIZE] __attribute__ ((aligned(64))) = {0};
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&A[CACHELINE], 0, _MM_HINT_T0);
#if defined(AVX512)
		*(PDVECTOR*) a = absvector(*(PDVECTOR*) A);
#else
		*(PDVECTOR*) a = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) A);
#endif
		reprodextractscallersnew(MVector,a,Mprev,MVectors)
		A += PDSIZE;
#if defined(AVX) || defined(AVX2) || defined(SSE)
		*(PDVECTOR*) a = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) A);
		reprodextractscallersnew(MVector,a,Mprev,MVectors)
		A += PDSIZE;
#endif
#if defined(SSE)
		*(PDVECTOR*) a = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) A);
		reprodextractscallersnew(MVector,a,Mprev,MVectors)
		A += PDSIZE;
		*(PDVECTOR*) a = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) A);
		reprodextractscallersnew(MVector,a,Mprev,MVectors)
		A += PDSIZE;
#endif
	}
	double da;
	for (; i < size; i++) {
		da = A[0];
		((double*) &Mprev)[0] = da + ((double*) &MVector)[0];
		da = da - (((double*) &Mprev)[0] - ((double*) &MVector)[0]);
		((double*) &MVector)[0] = ((double*) &Mprev)[0];
		((double*) &MVectors)[0] = da + ((double*) &MVectors)[0];
		A++;
	}
	St = subvector(MVector, Minit);
	Sts = subvector(MVectors, Minits);
	reduceVector(St,*fFold)
	reduceVector(Sts,*sFold)
	fesetround(originalRoundingMode);
}

double sequentialFastReprodasumCallBack(unsigned long size, double* A, double sigma) {
	if (sigma == 0) {
		double max = sequentialAbsMax(size, A);
		sigma = (max * size) / (1 - (4 * (size + 1) * eps));
	}
	double fFold, sFold;
	sequentialFastReprodasumCallBack(size, size, A, sigma, &fFold, &sFold);
	return fFold + sFold;
}

#ifdef SSE
#define putmax(vector)\
*(PDVECTOR*) &absA[0] = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &A[0]);\
*(PDVECTOR*) &absA[2] = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &A[2]);\
*(PDVECTOR*) &absA[4] = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &A[4]);\
*(PDVECTOR*) &absA[6] = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &A[6]);\
vector = maxvector(vector, *(PDVECTOR*) &absA[0]);\
vector = maxvector(vector, *(PDVECTOR*) &absA[2]);\
vector = maxvector(vector, *(PDVECTOR*) &absA[4]);\
vector = maxvector(vector, *(PDVECTOR*) &absA[6]);
#elif defined(AVX) || defined(AVX2)
#define putmax(vector);\
*(PDVECTOR*) &absA[0] = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &A[0]);\
*(PDVECTOR*) &absA[4] = andvector(ABS_MASK_VECTOR, *(PDVECTOR*) &A[4]);\
vector = maxvector(vector, *(PDVECTOR*) &absA[0]);\
vector = maxvector(vector, *(PDVECTOR*) &absA[4]);
#elif defined(AVX512)
#define putmax(vector);\
*(PDVECTOR*) &absA[0] = absvector(*(PDVECTOR*) &A[0]);\
vector = maxvector(vector, *(PDVECTOR*) &absA[0]);
#endif
double reprodasumAbsMax(unsigned long size, double *A, double *absA) {
	PDVECTOR SP = ZEROES;
	unsigned long i;
	for (i = 0; i + CACHELINE - 1 < size; i += CACHELINE) {
		__builtin_prefetch(&A[40], 0, _MM_HINT_T0);
		__builtin_prefetch(&absA[40], 0, _MM_HINT_T0);
		putmax(SP)
		A += CACHELINE;
		absA += CACHELINE;
	}
	double reduceMax(SP,maxAbs)
	for (; i < size; i++) {
		absA[0] = fabs(A[0]);
		maxAbs = (maxAbs < absA[0]) ? absA[0] : maxAbs;
		absA++;
		A++;
	}
	return maxAbs;
}

void sequential_or_dasum(unsigned long size, double* A, int IncA, double* result, int* propagate) {
	int Msize = EXPONENTPOSITIVERANGESIZE / ONEREDUCTIONW;
	for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
		result[k] = 1.5 * pow(2.0, (ONEREDUCTIONK - k + 1 - (double) Msize) * ONEREDUCTIONW);
	}
	unsigned int step = pow(2, ((MANTISSASIZE - ONEREDUCTIONW) - 2));
	double *absA = (double*) memalign(64, step * sizeof(double));
	for (unsigned long start = 0; start < size; start += step) {
		__builtin_prefetch(&A[step], 0, _MM_HINT_T1);
		double localSize = (step < size - start) ? step : size - start;
		double max = reprodasumAbsMax(localSize, A, absA);
		while ((ulp(result[0]) * pow(2, ONEREDUCTIONW)) <= max) {
			for (unsigned int k = ONEREDUCTIONK - 1; k > 0; k--) {
				result[k] = result[k - 1];
				propagate[k] = propagate[k - 1];
			}
			result[0] = 1.5 * pow(2, ONEREDUCTIONW) * ufp(result[0]);
			propagate[0] = 0;
		}
		extractVectorOneReduction(localSize, absA, result);
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			double ufpk = ufp(result[k]);
			if (result[k] >= 1.75 * ufpk) {
				result[k] -= 0.25 * ufpk;
				propagate[k]++;
			}
			else if (result[k] < 1.25 * ufpk) {
				result[k] += 0.5 * ufpk;
				propagate[k] -= 2;
			}
			else if (result[k] < 1.5 * ufpk) {
				result[k] += 0.25 * ufpk;
				propagate[k]--;
			}
		}
		A += step;
	}
}

void dtrsvLowerSequentialOneReduction(unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	IFP tempindexed;
	X[0] /= A[0];
	A += lda;
	for (int line = 1; line < n; line++) {
		sequential_orlight_ddot(line, A, 1, X, 1, &tempindexed.F[0], &tempindexed.P[0]);
		double max = fabs(X[line]);
		while ((ulp(tempindexed.F[0]) * pow(2, ONEREDUCTIONW)) <= max) {
			for (unsigned int k = ONEREDUCTIONK - 2; k > 0; k--) {
				tempindexed.F[k] = tempindexed.F[k - 1];
				tempindexed.P[k] = tempindexed.P[k - 1];
			}
			tempindexed.F[0] = 1.5 * pow(2, ONEREDUCTIONW) * ufp(tempindexed.F[0]);
			tempindexed.P[0] = 0;
		}
		X[line] = -X[line];
		for (unsigned int k = 0; k < ONEREDUCTIONK - 1; k++) {
			double T;
			*(unsigned long*) &T = *(unsigned long*) &X[line] | 1;
			T += tempindexed.F[k];
			X[line] -= (T - tempindexed.F[k]);
			tempindexed.F[k] = T;
		}		
		X[line] = 0;
		for (unsigned int k = 0; k < ONEREDUCTIONK - 1; k++)
			X[line] += tempindexed.F[k] - 1.5 * ufp(tempindexed.F[k]) + 0.25 * tempindexed.P[k] * ufp(tempindexed.F[k]);
		X[line] /= -A[line];
		A += lda;
	}
}

void dtrsvLowerSequentialOneReductionSolveAndRefine(unsigned int n, double* A, unsigned int lda, double* X, int IncX, double* d, int Incd) {
	IFP tempindexed;
	for (int line = 0; line < n; line++) {
		sequential_ortp_ddot(line, A, 1, X, 1, &tempindexed.F[0], &tempindexed.P[0]);
		double max = fabs(X[line]);
		while ((ulp(tempindexed.F[0]) * pow(2, ONEREDUCTIONW)) <= max) {
			for (unsigned int k = ONEREDUCTIONK - 1; k > 0; k--) {
				tempindexed.F[k] = tempindexed.F[k - 1];
				tempindexed.P[k] = tempindexed.P[k - 1];
			}
			tempindexed.F[0] = 1.5 * pow(2, ONEREDUCTIONW) * ufp(tempindexed.F[0]);
			tempindexed.P[0] = 0;
		}
		X[line] = -X[line];
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			double T;
			*(unsigned long*) &T = *(unsigned long*) &X[line] | 1;
			T += tempindexed.F[k];
			X[line] -= (T - tempindexed.F[k]);
			tempindexed.F[k] = T;
		}
		X[line] = 0;
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++)
			X[line] += tempindexed.F[k] - 1.5 * ufp(tempindexed.F[k]) + 0.25 * tempindexed.P[k] * ufp(tempindexed.F[k]);
		X[line] /= -A[line];
#if defined(AVX2) || (defined(AVX512) && not defined(MIC))
		double mul, error;
		twoProdScaller((&X[line]),(&A[line]),(&mul), (&error))
#else
		double mul, error, XH, XL, XT, YH, YL, YT, tempd;
		twoProdScaller((&X[line]),(&A[line]),(&mul),(&error),(&XH),(&XL),(&XT),(&YH),(&YL),(&YT),(&tempd))
#endif
		max = fabs(mul);
		while ((ulp(tempindexed.F[0]) * pow(2, ONEREDUCTIONW)) <= max) {
			for (unsigned int k = ONEREDUCTIONK - 1; k > 0; k--) {
				tempindexed.F[k] = tempindexed.F[k - 1];
				tempindexed.P[k] = tempindexed.P[k - 1];
			}
			tempindexed.F[0] = 1.5 * pow(2, ONEREDUCTIONW) * ufp(tempindexed.F[0]);
			tempindexed.P[0] = 0;
		}
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			double T;
			*(unsigned long*) &T = *(unsigned long*) &mul | 1;
			T += tempindexed.F[k];
			mul -= (T - tempindexed.F[k]);
			tempindexed.F[k] = T;
		}		
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			double T;
			*(unsigned long*) &T = *(unsigned long*) &error | 1;
			T += tempindexed.F[k];
			error -= (T - tempindexed.F[k]);
			tempindexed.F[k] = T;
		}
		d[line] = 0;
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++)
			d[line] += tempindexed.F[k] - 1.5 * ufp(tempindexed.F[k]) + 0.25 * tempindexed.P[k] * ufp(tempindexed.F[k]);
		d[line] = -d[line];
		A += lda;
	}
}

void dtrsvLowerSequentialOneReductionContinue(
	unsigned int n, double* A, unsigned int lda, 
	double* X, int IncX, IFP* bindexed, int incbindexed
) {
	for (int line = 0; line < n; line++) {
		sequential_orlight_ddotcontinue(line, A, 1, X, 1, &bindexed[line].F[0], &bindexed[line].P[0]);
		double max = fabs(X[line]);
		int Msize = EXPONENTPOSITIVERANGESIZE / ONEREDUCTIONW;
		while ((ulp(bindexed[line].F[0]) * pow(2, ONEREDUCTIONW)) <= max) {
			for (unsigned int k = ONEREDUCTIONK - 2; k > 0; k--) {
				bindexed[line].F[k] = bindexed[line].F[k - 1];
				bindexed[line].P[k] = bindexed[line].P[k - 1];
			}
			bindexed[line].F[0] = 1.5 * pow(2, ONEREDUCTIONW) * ufp(bindexed[line].F[0]);
			bindexed[line].P[0] = 0;
		}
		X[line] = -X[line];
		for (unsigned int k = 0; k < ONEREDUCTIONK - 1; k++) {
			double T;
			*(unsigned long*) &T = *(unsigned long*) &X[line] | 1;
			T += bindexed[line].F[k];
			X[line] -= (T - bindexed[line].F[k]);
			bindexed[line].F[k] = T;
		}		
		X[line] = 0;
		for (unsigned int k = 0; k < ONEREDUCTIONK - 1; k++)
			X[line] += bindexed[line].F[k] - 1.5 * ufp(bindexed[line].F[k]) + 0.25 * bindexed[line].P[k] * ufp(bindexed[line].F[k]);
		X[line] /= -A[line];
		A += lda;
	}
}

void dtrsvLowerSequentialOneReductionContinueSolveAndRefine(
	unsigned int n, double* A, unsigned int lda, double* X,
	int IncX, double* d, int Incd, IFP* bindexed, int incbindexed
) {
	for (int line = 0; line < n; line++) {
		sequential_ortp_ddotcontinue(line, A, 1, X, 1, &bindexed[line].F[0], &bindexed[line].P[0]);
		double max = fabs(X[line]);
		int Msize = EXPONENTPOSITIVERANGESIZE / ONEREDUCTIONW;
		while ((ulp(bindexed[line].F[0]) * pow(2, ONEREDUCTIONW)) <= max) {
			for (unsigned int k = ONEREDUCTIONK - 1; k > 0; k--) {
				bindexed[line].F[k] = bindexed[line].F[k - 1];
				bindexed[line].P[k] = bindexed[line].P[k - 1];
			}
			bindexed[line].F[0] = 1.5 * pow(2, ONEREDUCTIONW) * ufp(bindexed[line].F[0]);
			bindexed[line].P[0] = 0;
		}
		X[line] = -X[line];
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			double T;
			*(unsigned long*) &T = *(unsigned long*) &X[line] | 1;
			T += bindexed[line].F[k];
			X[line] -= (T - bindexed[line].F[k]);
			bindexed[line].F[k] = T;
		}		
		X[line] = 0;
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++)
			X[line] += bindexed[line].F[k] - 1.5 * ufp(bindexed[line].F[k]) + 0.25 * bindexed[line].P[k] * ufp(bindexed[line].F[k]);
		X[line] /= -A[line];
#if defined(AVX2) || (defined(AVX512) && not defined(MIC))
		double mul, error;
		twoProdScaller((&X[line]),(&A[line]),(&mul), (&error))
#else
		double mul, error, XH, XL, XT, YH, YL, YT, tempd;
		twoProdScaller((&X[line]),(&A[line]),(&mul),(&error),(&XH),(&XL),(&XT),(&YH),(&YL),(&YT),(&tempd))
#endif
		max = fabs(mul);
		while ((ulp(bindexed[line].F[0]) * pow(2, ONEREDUCTIONW)) <= max) {
			for (unsigned int k = ONEREDUCTIONK - 1; k > 0; k--) {
				bindexed[line].F[k] = bindexed[line].F[k - 1];
				bindexed[line].P[k] = bindexed[line].P[k - 1];
			}
			bindexed[line].F[0] = 1.5 * pow(2, ONEREDUCTIONW) * ufp(bindexed[line].F[0]);
			bindexed[line].P[0] = 0;
		}
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			double T;
			*(unsigned long*) &T = *(unsigned long*) &mul | 1;
			T += bindexed[line].F[k];
			mul -= (T - bindexed[line].F[k]);
			bindexed[line].F[k] = T;
		}		
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			double T;
			*(unsigned long*) &T = *(unsigned long*) &error | 1;
			T += bindexed[line].F[k];
			error -= (T - bindexed[line].F[k]);
			bindexed[line].F[k] = T;
		}		
		d[line] = 0;
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++)
			d[line] += bindexed[line].F[k] - 1.5 * ufp(bindexed[line].F[k]) + 0.25 * bindexed[line].P[k] * ufp(bindexed[line].F[k]);
		d[line] = -d[line];
		A += lda;
	}
}

void sequentialOneReductionGemvIndexed(
	unsigned int m, unsigned int n, double *A, unsigned int lda,
	double *X, int IncX, double beta, IFP *bindexed, int Incbindexed
) {
	if (beta == 0) {
		for (unsigned int line = 0; line < m; line++) {
			sequential_or_ddot(n, A, 1, X, 1, &bindexed[line].F[0], &bindexed[line].P[0]);
			A += lda;
		}
	}
	else {
		for (unsigned int line = 0; line < m; line++) {
			sequential_or_ddotcontinue(n, A, 1, X, 1, &bindexed[line].F[0], &bindexed[line].P[0]);
			A += lda;
		}
	}
}

void sequentialOneReductionGemv(
	unsigned int m, unsigned int n, double alpha, double *A,
	unsigned int lda, double *X, int IncX, double beta, double *Y, int IncY
) {
	IFP *bindexed = (IFP*) memalign(64, n * sizeof(IFP));
	for (unsigned int line = 0; line < m; line++) {
		sequential_or_ddot(n, &A[line * lda], 1, X, 1, &bindexed[line].F[0], &bindexed[line].P[0]);
		double Ax = 0;
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++)
			Ax += bindexed[line].F[k] - 1.5 * ufp(bindexed[line].F[k]) + 0.25 * bindexed[line].P[k] * ufp(bindexed[line].F[k]);
		Y[line] = alpha * Ax + beta * Y[line];
	}
}

void sequentialOneReductionGemvIRLower(
	unsigned int m, unsigned int n, double *A, unsigned int lda, double *X, int IncX, double *Y, int IncY
) {
	IFP *bindexed = (IFP*) memalign(64, n * sizeof(IFP));
	for (unsigned int line = 0; line < m; line++) {
		sequential_ortp_ddot(line + 1, &A[line * lda], 1, X, 1, &bindexed[line].F[0], &bindexed[line].P[0]);
		double max = fabs(Y[line]);
		int Msize = EXPONENTPOSITIVERANGESIZE / ONEREDUCTIONW;
		while ((ulp(bindexed[line].F[0]) * pow(2, ONEREDUCTIONW)) <= max) {
			for (unsigned int k = ONEREDUCTIONK - 1; k > 0; k--) {
				bindexed[line].F[k] = bindexed[line].F[k - 1];
				bindexed[line].P[k] = bindexed[line].P[k - 1];
			}
			bindexed[line].F[0] = 1.5 * pow(2, ONEREDUCTIONW) * ufp(bindexed[line].F[0]);
			bindexed[line].P[0] = 0;
		}
		Y[line] = -Y[line];
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			double T;
			*(unsigned long*) &T = *(unsigned long*) &Y[line] | 1;
			T += bindexed[line].F[k];
			Y[line] -= (T - bindexed[line].F[k]);
			bindexed[line].F[k] = T;
		}		
		Y[line] = 0;
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++)
			Y[line] += bindexed[line].F[k] - 1.5 * ufp(bindexed[line].F[k]) + 0.25 * bindexed[line].P[k] * ufp(bindexed[line].F[k]);
		Y[line] = -Y[line];
	}
}

double dgemvTrsvLineSequentialIFastSum(unsigned int size, double *A, int IncA, double *X, int IncX, double Y) {
	double *C = (double*) memalign(64, (size * 2 + 1) * sizeof(double));
	unsigned long count;
	sequentialVectorTwoProdTrans(size, A, X, C, &count);
	C[count] = -Y;
	count++;
	double S = iFastSumCallBack(&count, C, 0);
	free(C);
	return S;
}

double dgemvTrsvLineSequentialHybridSum(unsigned int size, double *A, int IncA, double *X, int IncX, double Y) {
	double C[2048 + 1] __attribute__ ((aligned(64))) = {0};
	ddotSequentialExtractHybridSum(size, A, X, C);
	C[2048] = -Y;
	unsigned long tmpSize = 2048 + 1;
	return iFastSumCallBack(&tmpSize, C, 0);
}

//========================================================================================================================
//============================================== END OF SEQUENTIAL PART ==================================================
//========================================================================================================================

#ifdef OMP_PARALLEL
double ompMultMax(unsigned long int size, double *X, int IncX, double *Y, int IncY, double *res) {
	double globalMax = 0;
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
#ifdef STATS
	int first = 1;
#endif
#pragma omp parallel for default(shared) reduction(max:globalMax)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		double gmax = sequentialMultMax(localSize, &X[i], IncX, &Y[i], IncY, &res[i]);
		globalMax = (globalMax > gmax) ? globalMax : gmax;
#ifdef STATS
#pragma omp critical
		if (first) {
			first = 0;
			localMaxCycles = rdtsc();
		}
#endif
	}
	return globalMax;
}

double omp_rs_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY) {
	double *res = (double*) memalign(64, size * sizeof(double));
	double maxAbs = ompMultMax(size, X, IncX, Y, IncY, res);
#ifdef STATS
	reduceMaxCycles = rdtsc();
#endif
	double sigma = (maxAbs * size) / (1 - (2 * size * eps));
	double S = ompReprodSumCallBack(size, res, sigma);
	free(res);
	return S;
}

double omp_frs_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY) {
	double *res = (double*) memalign(64, size * sizeof(double));
	double maxAbs = ompMultMax(size, X, IncX, Y, IncY, res);
#ifdef STATS
	reduceMaxCycles = rdtsc();
#endif
	double sigma = (maxAbs * size) / (1 - (2 * size * eps));
	double S = ompFastReprodSumCallBack(size, res, sigma);
	free(res);
	return S;
}

void omp_or_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY, double* result, int* propagate) {
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
	unsigned int LOCAL_OMP_NUMBER_OF_THREADS = size / localSizeCacheLine;
	LOCAL_OMP_NUMBER_OF_THREADS += !!(size % localSizeCacheLine);
	double results[LOCAL_OMP_NUMBER_OF_THREADS][ONEREDUCTIONK];
	int propagates[LOCAL_OMP_NUMBER_OF_THREADS][ONEREDUCTIONK];
#ifdef STATS
	int first = 1;
#endif
#pragma omp parallel for default(shared)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		double localResult[ONEREDUCTIONK] = {0};
		int localPropagate[ONEREDUCTIONK] = {0};
		sequential_or_ddot(localSize, &X[i], IncX, &Y[i], IncY, localResult, localPropagate);
#ifdef STATS
#pragma omp critical
		{
			if (first) localSumCycles = rdtsc();
			first = 0;
		}
#endif
		unsigned int threadId = omp_get_thread_num();
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			results[threadId][k] = localResult[k];
			propagates[threadId][k] = localPropagate[k];
		}
	}
	for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
		result[k] = results[0][k];
		propagate[k] = propagates[0][k];
	}
	for (unsigned int i = 1; i < LOCAL_OMP_NUMBER_OF_THREADS; i++) addIFP(result, propagate, results[i], propagates[i], result, propagate);
}

void omp_ortp_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY, double* result, int* propagate) {
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
	unsigned long LOCAL_OMP_NUMBER_OF_THREADS = size / localSizeCacheLine;
	LOCAL_OMP_NUMBER_OF_THREADS += !!(size % localSizeCacheLine);
	double results[LOCAL_OMP_NUMBER_OF_THREADS][ONEREDUCTIONK];
	int propagates[LOCAL_OMP_NUMBER_OF_THREADS][ONEREDUCTIONK];
#ifdef STATS
	int first = 1;
#endif
#pragma omp parallel for default(shared)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		double localResult[ONEREDUCTIONK] = {0};
		int localPropagate[ONEREDUCTIONK] = {0};
		sequential_ortp_ddot(localSize, &X[i], IncX, &Y[i], IncY, localResult, localPropagate);
#ifdef STATS
#pragma omp critical
		{
			if (first) localSumCycles = rdtsc();
			first = 0;
		}
#endif
		unsigned int threadId = omp_get_thread_num();
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			results[threadId][k] = localResult[k];
			propagates[threadId][k] = localPropagate[k];
		}
	}
	for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
		result[k] = results[0][k];
		propagate[k] = propagates[0][k];
	}
	for (unsigned int i = 1; i < LOCAL_OMP_NUMBER_OF_THREADS; i++) addIFP(result, propagate, results[i], propagates[i], result, propagate);
}

void ompReprodasumCallBack(unsigned long size, unsigned long gsize, double* A, double sigma, double *fFold, double *sFold) {
	double firstfold = 0;
	double secondfold = 0;
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
#ifdef STATS
	int first = 1;
#endif
#pragma omp parallel for default(shared) reduction(+:firstfold,secondfold)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		double localFirst = 0;
		double localSecond = 0;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		sequentialReprodasumCallBack(localSize, gsize, &A[i], sigma, &localFirst, &localSecond);
		firstfold += localFirst;
		secondfold += localSecond;
#ifdef STATS
#pragma omp critical
		if (first) {
			first = 0;
			localSumCycles = rdtsc();
		}
#endif
	}
	*fFold = firstfold;
	*sFold = secondfold;
}

double ompReprodasumCallBack(unsigned long size, double* A, double sigma) {
	double max;
	double fFold, sFold;
	if (sigma == 0) {
		max = ompAbsMax(size, A);
		sigma = (max * size) / (1 - (2 * size * eps));
	}
	ompReprodasumCallBack(size, size, A, sigma, &fFold, &sFold);
	return fFold + sFold;
}

void ompFastReprodasumCallBack(unsigned long size, unsigned long gsize, double* A, double sigma, double *fFold, double *sFold) {
	double firstfold = 0;
	double secondfold = 0;
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
#ifdef STATS
	int first = 1;
#endif
#pragma omp parallel for default(shared) reduction(+:firstfold,secondfold)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		double localFirst = 0;
		double localSecond = 0;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		sequentialFastReprodasumCallBack(localSize, gsize, &A[i], sigma, &localFirst, &localSecond);
		firstfold += localFirst;
		secondfold += localSecond;
#ifdef STATS
#pragma omp critical
		if (first) {
			first = 0;
			localSumCycles = rdtsc();
		}
#endif
	}
	*fFold = firstfold;
	*sFold = secondfold;
}

double ompFastReprodasumCallBack(unsigned long size, double* A, double sigma) {
	double max;
	double fFold, sFold;
	if (sigma == 0) {
		max = ompAbsMax(size, A);
		sigma = (max * size) / (1 - (4 * (size + 1) * eps));
	}
	ompFastReprodasumCallBack(size, size, A, sigma, &fFold, &sFold);
	return fFold + sFold;
}

void omp_or_dasum(unsigned long int size, double *X, int IncX, double* result, int* propagate) {
	unsigned long localSizeCacheLine = size / OMP_NUMBER_OF_THREADS;
	localSizeCacheLine += !!(size % OMP_NUMBER_OF_THREADS);
	localSizeCacheLine += (7 - ((localSizeCacheLine - 1) % CACHELINE));
	unsigned int LOCAL_OMP_NUMBER_OF_THREADS = size / localSizeCacheLine;
	LOCAL_OMP_NUMBER_OF_THREADS += !!(size % localSizeCacheLine);
	double results[LOCAL_OMP_NUMBER_OF_THREADS][ONEREDUCTIONK];
	int propagates[LOCAL_OMP_NUMBER_OF_THREADS][ONEREDUCTIONK];
#ifdef STATS
	unsigned int first = 1;
#endif
#pragma omp parallel for default(shared)
	for (unsigned long i = 0; i < size; i += localSizeCacheLine) {
		unsigned long localSize;
		if (i + localSizeCacheLine > size) localSize = size - i;
		else localSize = localSizeCacheLine;
		double localResult[ONEREDUCTIONK] = {0};
		int localPropagate[ONEREDUCTIONK] = {0};
		sequential_or_dasum(localSize, &X[i], IncX, localResult, localPropagate);
#ifdef STATS
#pragma omp critical
		if (first) {
			localSumCycles = rdtsc();
			first = 0;
		}
#endif
		unsigned int threadId = omp_get_thread_num();
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			results[threadId][k] = localResult[k];
			propagates[threadId][k] = localPropagate[k];
		}
	}
	for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
		result[k] = results[0][k];
		propagate[k] = propagates[0][k];
	}
	for (unsigned int i = 1; i < LOCAL_OMP_NUMBER_OF_THREADS; i++) addIFP(result, propagate, results[i], propagates[i], result, propagate);
}

void ompOneReductionGemvIndexed(
	unsigned int m, unsigned int n, double *A, unsigned int lda,
	double *X, int IncX, double beta, IFP *bindexed, int Incbindexed
) {
	if (beta == 0) {
#pragma omp parallel for default(shared) schedule(guided)
		for (unsigned int line = 0; line < m; line++) {
			sequential_orlight_ddot(n, &A[line * lda], 1, X, 1, &bindexed[line].F[0], &bindexed[line].P[0]);
		}
	}
	else {
#pragma omp parallel for default(shared) schedule(guided)
		for (unsigned int line = 0; line < m; line++) {
			sequential_orlight_ddotcontinue(n, &A[line * lda], 1, X, 1, &bindexed[line].F[0], &bindexed[line].P[0]);
		}
	}
}

void ompOneReductionGemvIndexedSolveAndRefine(
	unsigned int m, unsigned int n, double *A, unsigned int lda,
	double *X, int IncX, double beta, IFP *bindexed, int Incbindexed
) {
	if (beta == 0) {
#pragma omp parallel for default(shared) schedule(guided)
		for (unsigned int line = 0; line < m; line++) {
			sequential_ortp_ddot(n, &A[line * lda], 1, X, 1, &bindexed[line].F[0], &bindexed[line].P[0]);
		}
	}
	else {
#pragma omp parallel for default(shared) schedule(guided)
		for (unsigned int line = 0; line < m; line++) {
			sequential_ortp_ddotcontinue(n, &A[line * lda], 1, X, 1, &bindexed[line].F[0], &bindexed[line].P[0]);
		}
	}
}

void ompOneReductionGemv(
	unsigned int m, unsigned int n, double alpha, double *A,
	unsigned int lda, double *X, int IncX, double beta, double *Y, int IncY
) {
	IFP *bindexed = (IFP*) memalign(64, n * sizeof(IFP));
#pragma omp parallel for default(shared) schedule(guided)
	for (unsigned int line = 0; line < m; line++) {
		sequential_or_ddot(n, &A[line * lda], 1, X, 1, &bindexed[line].F[0], &bindexed[line].P[0]);
		double Ax = 0;
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++)
			Ax += bindexed[line].F[k] - 1.5 * ufp(bindexed[line].F[k]) + 0.25 * bindexed[line].P[k] * ufp(bindexed[line].F[k]);
		Y[line] = alpha * Ax + beta * Y[line];
	}
}

void dtrsvLowerOmpOneReductionRecursive(
	unsigned int n, double* A, unsigned int lda, double* X,
	int IncX, IFP* bindexed, int Incbindexed, int first
) {
	unsigned int blockSize = 480;
	if (n <= blockSize) {
		if (first) dtrsvLowerSequentialOneReduction(n, A, lda, X, IncX);
		else dtrsvLowerSequentialOneReductionContinue(n, A, lda, X, IncX, bindexed, Incbindexed);
	}
	else {
		unsigned int halfn = n / 2;
		if (halfn % blockSize) halfn += blockSize - halfn % blockSize;
		dtrsvLowerOmpOneReductionRecursive(halfn, A, lda, X, IncX, bindexed, Incbindexed, first);
		ompOneReductionGemvIndexed(n - halfn, halfn, &A[halfn * lda], lda, X, IncX, !first, &bindexed[halfn], Incbindexed);
		dtrsvLowerOmpOneReductionRecursive(n - halfn, &A[halfn * lda + halfn], lda, &X[halfn], IncX, &bindexed[halfn], Incbindexed, 0);
	}
}

void dtrsvLowerOmpOneReductionRecursiveSolveAndRefine(
	unsigned int n, double* A, unsigned int lda, double* X, int IncX,
	double* d, int Incd, IFP* bindexed, int Incbindexed, int first
) {
	unsigned int blockSize = 480;
	if (n <= blockSize) {
		if (first) dtrsvLowerSequentialOneReductionSolveAndRefine(n, A, lda, X, IncX, d, Incd);
		else dtrsvLowerSequentialOneReductionContinueSolveAndRefine(n, A, lda, X, IncX, d, Incd, bindexed, Incbindexed);
	}
	else {
		unsigned int halfn = n / 2;
		if (halfn % blockSize) halfn += blockSize - halfn % blockSize;
		dtrsvLowerOmpOneReductionRecursiveSolveAndRefine(halfn, A, lda, X, IncX, d, Incd, bindexed, Incbindexed, first);
		ompOneReductionGemvIndexedSolveAndRefine(n - halfn, halfn, &A[halfn * lda], lda, X, IncX, !first, &bindexed[halfn], Incbindexed);
		dtrsvLowerOmpOneReductionRecursiveSolveAndRefine(
			n - halfn, &A[halfn * lda + halfn], lda, &X[halfn], IncX, &d[halfn], IncX, &bindexed[halfn], Incbindexed, 0
		);
	}
}

void ompOneReductionGemvIR(
	unsigned int m, unsigned int n, double alpha, double *A,
	unsigned int lda, double *X, int IncX, double beta, double *Y, int IncY
) {
	IFP *bindexed = (IFP*) memalign(64, n * sizeof(IFP));
#pragma omp parallel for default(shared) schedule(guided)
	for (unsigned int line = 0; line < m; line++) {
		sequential_or_ddot(line, &A[line * lda], 1, X, 1, &bindexed[line].F[0], &bindexed[line].P[0]);
		double Ax = 0;
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++)
			Ax += bindexed[line].F[k] - 1.5 * ufp(bindexed[line].F[k]) + 0.25 * bindexed[line].P[k] * ufp(bindexed[line].F[k]);
		Y[line] = alpha * Ax + beta * Y[line];
	}
}

void ompOneReductionGemvIRLower(
	unsigned int m, unsigned int n, double *A, unsigned int lda, double *X, int IncX, double *Y, int IncY
) {
	IFP *bindexed = (IFP*) memalign(64, n * sizeof(IFP));
#pragma omp parallel for default(shared) schedule(guided)
	for (unsigned int line = 0; line < m; line++) {
		sequential_ortp_ddot(line + 1, &A[line * lda], 1, X, 1, &bindexed[line].F[0], &bindexed[line].P[0]);
		double max = fabs(Y[line]);
		int Msize = EXPONENTPOSITIVERANGESIZE / ONEREDUCTIONW;
		while ((ulp(bindexed[line].F[0]) * pow(2, ONEREDUCTIONW)) <= max) {
			for (unsigned int k = ONEREDUCTIONK - 1; k > 0; k--) {
				bindexed[line].F[k] = bindexed[line].F[k - 1];
				bindexed[line].P[k] = bindexed[line].P[k - 1];
			}
			bindexed[line].F[0] = 1.5 * pow(2, ONEREDUCTIONW) * ufp(bindexed[line].F[0]);
			bindexed[line].P[0] = 0;
		}
		Y[line] = -Y[line];
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++) {
			double T;
			*(unsigned long*) &T = *(unsigned long*) &Y[line] | 1;
			T += bindexed[line].F[k];
			Y[line] -= (T - bindexed[line].F[k]);
			bindexed[line].F[k] = T;
		}		
		Y[line] = 0;
		for (unsigned int k = 0; k < ONEREDUCTIONK; k++)
			Y[line] += bindexed[line].F[k] - 1.5 * ufp(bindexed[line].F[k]) + 0.25 * bindexed[line].P[k] * ufp(bindexed[line].F[k]);
		Y[line] = -Y[line];
	}
}
#endif


//========================================================================================================================
//================================================== END OF OMP PART =====================================================
//========================================================================================================================

#if defined(MPI_PARALLEL) && not defined(OMP_PARALLEL)
double mpiMultMax(unsigned long size, double *X, int IncX, double *Y, int IncY, double *res) {
	double max = 0;
	double glmax = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	max = sequentialMultMax(localSize, X, IncX, Y, IncY, res);
#ifdef STATS
	localMaxCycles = rdtsc();
#endif
	MPI_Allreduce(&max, &glmax, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
	return glmax;
}

double mpi_rs_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY) {
	double *res = (double*) memalign(64, size * sizeof(double));
	double maxAbs = mpiMultMax(size, X, IncX, Y, IncY, res);
#ifdef STATS
	reduceMaxCycles = rdtsc();
#endif
	double sigma = (maxAbs * size) / (1 - (2 * size * eps));
	double S = mpiReprodSumCallBack(size, res, sigma);
	free(res);
	return S;
}

double mpi_frs_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY) {
	double *res = (double*) memalign(64, size * sizeof(double));
	double maxAbs = mpiMultMax(size, X, IncX, Y, IncY, res);
#ifdef STATS
	reduceMaxCycles = rdtsc();
#endif
	double sigma = (maxAbs * size) / (1 - (2 * size * eps));
	double S = mpiFastReprodSumCallBack(size, res, sigma);
	free(res);
	return S;
}

void mpi_or_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY, double* result, int* propagate) {
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	IFP localIFP;
	IFP globalIFP;
	sequential_or_ddot(localSize, X, IncX, Y, IncY, &localIFP.F[0], &localIFP.P[0]);
#ifdef STATS
	localSumCycles = rdtsc();
#endif
	MPI_Allreduce((int*) &localIFP.F[0], (int*) &globalIFP.F[0], 3 * ONEREDUCTIONK, MPI_INT, MPI_ADD_IFP, MPI_COMM_WORLD);
	for (unsigned int i = 0; i < ONEREDUCTIONK; i++) {
		result[i] = globalIFP.F[i];
		propagate[i] = globalIFP.P[i];
	}
}

void mpi_ortp_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY, double* result, int* propagate) {
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	IFP localIFP;
	IFP globalIFP;
	sequential_ortp_ddot(localSize, X, IncX, Y, IncY, &localIFP.F[0], &localIFP.P[0]);
#ifdef STATS
	localSumCycles = rdtsc();
#endif
	MPI_Allreduce((int*) &localIFP.F[0], (int*) &globalIFP.F[0], 3 * ONEREDUCTIONK, MPI_INT, MPI_ADD_IFP, MPI_COMM_WORLD);
	for (unsigned int i = 0; i < ONEREDUCTIONK; i++) {
		result[i] = globalIFP.F[i];
		propagate[i] = globalIFP.P[i];
	}
}

void mpi_or_dasum(unsigned long int size, double *X, int IncX, double* result, int* propagate) {
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	IFP localIFP;
	IFP globalIFP;
	sequential_or_dasum(localSize, X, IncX, &localIFP.F[0], &localIFP.P[0]);
#ifdef STATS
	localSumCycles = rdtsc();
#endif
	MPI_Allreduce((int*) &localIFP.F[0], (int*) &globalIFP.F[0], 3 * ONEREDUCTIONK, MPI_INT, MPI_ADD_IFP, MPI_COMM_WORLD);
	for (unsigned int i = 0; i < ONEREDUCTIONK; i++) {
		result[i] = globalIFP.F[i];
		propagate[i] = globalIFP.P[i];
	}
}

void mpiFastReprodasumCallBack(unsigned long size, double* A, double sigma, double *fFold, double *sFold) {
	double S[2];
	double glS[2];
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	sequentialFastReprodasumCallBack(localSize, size, A, sigma, &S[0], &S[1]);
#ifdef STATS
	localSumCycles = rdtsc();
#endif
	MPI_Allreduce(&S, &glS, 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	*fFold = glS[0];
	*sFold = glS[1];
}

double mpiFastReprodasumCallBack(unsigned long size, double* A, double sigma) {
	double max;
	double fFold, sFold;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	if (sigma == 0) {
		max = mpiAbsMax(size, A);
		sigma = (max * size) / (1 - (4 * (size + 1) * eps));
	}
	mpiFastReprodasumCallBack(size, A, sigma, &fFold, &sFold);
	return fFold + sFold;
}

void mpiReprodasumCallBack(unsigned long size, double* A, double sigma, double *fFold, double *sFold) {
	double S[2];
	double glS[2];
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	sequentialReprodasumCallBack(localSize, size, A, sigma, &S[0], &S[1]);
#ifdef STATS
	localSumCycles = rdtsc();
#endif
	MPI_Allreduce(&S, &glS, 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	*fFold = glS[0];
	*sFold = glS[1];
}

double mpiReprodasumCallBack(unsigned long size, double* A, double sigma) {
	double max;
	double fFold, sFold;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	if (sigma == 0) {
		max = mpiAbsMax(size, A);
		sigma = (max * size) / (1 - (2 * size * eps));
	}
	mpiReprodasumCallBack(size, A, sigma, &fFold, &sFold);
	return fFold + sFold;
}
#endif

//========================================================================================================================
//================================================== END OF MPI PART =====================================================
//========================================================================================================================

#if defined(MPI_PARALLEL) && defined(OMP_PARALLEL)
double hybridMultMax(unsigned long size, double *X, int IncX, double *Y, int IncY, double *res) {
	double max = 0;
	double glmax = 0;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	max = ompMultMax(localSize, X, IncX, Y, IncY, res);
	MPI_Allreduce(&max, &glmax, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
	return glmax;
}

double hybrid_rs_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY) {
	double *res = (double*) memalign(64, size * sizeof(double));
	double maxAbs = hybridMultMax(size, X, IncX, Y, IncY, res);
#ifdef STATS
	reduceMaxCycles = rdtsc();
#endif
	double sigma = (maxAbs * size) / (1 - (2 * size * eps));
	double S = hybridReprodSumCallBack(size, res, sigma);
	free(res);
	return S;
}

double hybrid_frs_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY) {
	double *res = (double*) memalign(64, size * sizeof(double));
	double maxAbs = hybridMultMax(size, X, IncX, Y, IncY, res);
#ifdef STATS
	reduceMaxCycles = rdtsc();
#endif
	double sigma = (maxAbs * size) / (1 - (2 * size * eps));
	double S = hybridFastReprodSumCallBack(size, res, sigma);
	free(res);
	return S;
}

void hybrid_or_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY, double* result, int* propagate) {
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	IFP localIFP;
	IFP globalIFP;
	omp_or_ddot(localSize, X, IncX, Y, IncY, &localIFP.F[0], &localIFP.P[0]);
	MPI_Allreduce((int*) &localIFP, (int*) &globalIFP, 3 * ONEREDUCTIONK, MPI_INT, MPI_ADD_IFP, MPI_COMM_WORLD);
	for (unsigned int i = 0; i < ONEREDUCTIONK; i++) {
		result[i] = globalIFP.F[i];
		propagate[i] = globalIFP.P[i];
	}
}

void hybrid_ortp_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY, double* result, int* propagate) {
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	IFP localIFP;
	IFP globalIFP;
	omp_ortp_ddot(localSize, X, IncX, Y, IncY, &localIFP.F[0], &localIFP.P[0]);
	MPI_Allreduce((int*) &localIFP, (int*) &globalIFP, 3 * ONEREDUCTIONK, MPI_INT, MPI_ADD_IFP, MPI_COMM_WORLD);
	for (unsigned int i = 0; i < ONEREDUCTIONK; i++) {
		result[i] = globalIFP.F[i];
		propagate[i] = globalIFP.P[i];
	}
}

void hybrid_or_dasum(unsigned long int size, double *X, int IncX, double* result, int* propagate) {
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	IFP localIFP;
	IFP globalIFP;
	omp_or_dasum(localSize, X, IncX, &localIFP.F[0], &localIFP.P[0]);
	MPI_Allreduce((int*) &localIFP, (int*) &globalIFP, 3 * ONEREDUCTIONK, MPI_INT, MPI_ADD_IFP, MPI_COMM_WORLD);
	for (unsigned int i = 0; i < ONEREDUCTIONK; i++) {
		result[i] = globalIFP.F[i];
		propagate[i] = globalIFP.P[i];
	}
}

void hybridReprodasumCallBack(unsigned long size, double* A, double sigma, double *fFold, double *sFold) {
	double S[2];
	double glS[2];
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	ompReprodasumCallBack(localSize, size, A, sigma, &S[0], &S[1]);
	MPI_Allreduce(&S, &glS, 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	*fFold = glS[0];
	*sFold = glS[1];
}

double hybridReprodasumCallBack(unsigned long size, double* A, double sigma) {
	double max;
	double fFold, sFold;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	if (sigma == 0) {
		max = hybridAbsMax(size, A);
		sigma = (max * size) / (1 - (2 * size * eps));
	}
	hybridReprodasumCallBack(size, A, sigma, &fFold, &sFold);
	return fFold + sFold;
}

void hybridFastReprodasumCallBack(unsigned long size, double* A, double sigma, double *fFold, double *sFold) {
	double S[2];
	double glS[2];
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	ompFastReprodasumCallBack(localSize, size, A, sigma, &S[0], &S[1]);
	MPI_Allreduce(&S, &glS, 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	*fFold = glS[0];
	*sFold = glS[1];
}

double hybridFastReprodasumCallBack(unsigned long size, double* A, double sigma) {
	double max;
	double fFold, sFold;
	unsigned long localSize = size / MPI_NUMBER_OF_THREADS;
	if (sigma == 0) {
		max = hybridAbsMax(size, A);
		sigma = (max * size) / (1 - (4 * (size + 1) * eps));
	}
	hybridFastReprodasumCallBack(size, A, sigma, &fFold, &sFold);
	return fFold + sFold;
}
#endif

//========================================================================================================================
//================================================ END OF HYBRID PART ====================================================
//========================================================================================================================

double reprodblas_rs_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	return hybrid_rs_ddot(size, X, IncX, Y, IncY);
#else
	return mpi_rs_ddot(size, X, IncX, Y, IncY);
#endif
#elif defined(OMP_PARALLEL)
	return omp_rs_ddot(size, X, IncX, Y, IncY);
#else
	return sequential_rs_ddot(size, X, IncX, Y, IncY);
#endif
}

double reprodblas_frs_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	return hybrid_frs_ddot(size, X, IncX, Y, IncY);
#else
	return mpi_frs_ddot(size, X, IncX, Y, IncY);
#endif
#elif defined(OMP_PARALLEL)
	return omp_frs_ddot(size, X, IncX, Y, IncY);
#else
	return sequential_frs_ddot(size, X, IncX, Y, IncY);
#endif
}

double reprodblas_or_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY) {
	double result[ONEREDUCTIONK] = {0};
	int propagate[ONEREDUCTIONK] = {0};
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	hybrid_or_ddot(size, X, IncX, Y, IncY, result, propagate);
#else
	mpi_or_ddot(size, X, IncX, Y, IncY, result, propagate);
#endif
#elif defined(OMP_PARALLEL)
	omp_or_ddot(size, X, IncX, Y, IncY, result, propagate);
#else
	sequential_or_ddot(size, X, IncX, Y, IncY, result, propagate);
#endif
	double finalResult = 0;
	for (unsigned int k = 0; k < ONEREDUCTIONK; k++)
		finalResult += result[k] - 1.5 * ufp(result[k]) + 0.25 * propagate[k] * ufp(result[k]);
	return finalResult;
}

double reprodblas_ortp_ddot(unsigned long int size, double *X, int IncX, double *Y, int IncY) {
	double result[ONEREDUCTIONK] = {0};
	int propagate[ONEREDUCTIONK] = {0};
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	hybrid_ortp_ddot(size, X, IncX, Y, IncY, result, propagate);
#else
	mpi_ortp_ddot(size, X, IncX, Y, IncY, result, propagate);
#endif
#elif defined(OMP_PARALLEL)
	omp_ortp_ddot(size, X, IncX, Y, IncY, result, propagate);
#else
	sequential_ortp_ddot(size, X, IncX, Y, IncY, result, propagate);
#endif
	double finalResult = 0;
	for (unsigned int k = 0; k < ONEREDUCTIONK; k++)
		finalResult += result[k] - 1.5 * ufp(result[k]) + 0.25 * propagate[k] * ufp(result[k]);
	return finalResult;
}

double reprodblas_rs_dnrm2(unsigned long size, double *X, int IncX) {
	return sqrt(reprodblas_rs_ddot(size, X, IncX, X, IncX));
}

double reprodblas_frs_dnrm2(unsigned long size, double *X, int IncX) {
	return sqrt(reprodblas_frs_ddot(size, X, IncX, X, IncX));
}

double reprodblas_or_dnrm2(unsigned long size, double *X, int IncX) {
	return sqrt(reprodblas_or_ddot(size, X, IncX, X, IncX));
}

double reprodblas_ortp_dnrm2(unsigned long size, double *X, int IncX) {
	return sqrt(reprodblas_ortp_ddot(size, X, IncX, X, IncX));
}

double reprodblas_rs_dasum(unsigned long size, double* A, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	return hybridReprodasumCallBack(size, A, 0);
#else
	return mpiReprodasumCallBack(size, A, 0);
#endif
#elif defined(OMP_PARALLEL)
	return ompReprodasumCallBack(size, A, 0);
#else
	return sequentialReprodasumCallBack(size, A, 0);
#endif
}

// Appel de l'implémentation de FastReprodSum
double reprodblas_frs_dasum(unsigned long size, double* A, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	return hybridFastReprodasumCallBack(size, A, 0);
#else
	return mpiFastReprodasumCallBack(size, A, 0);
#endif
#elif defined(OMP_PARALLEL)
	return ompFastReprodasumCallBack(size, A, 0);
#else
	return sequentialFastReprodasumCallBack(size, A, 0);
#endif
}

double reprodblas_or_dasum(unsigned long int size, double *X, int IncX) {
	double result[ONEREDUCTIONK] = {0};
	int propagate[ONEREDUCTIONK] = {0};
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	hybrid_or_dasum(size, X, IncX, result, propagate);
#else
	mpi_or_dasum(size, X, IncX, result, propagate);
#endif
#elif defined(OMP_PARALLEL)
	omp_or_dasum(size, X, IncX, result, propagate);
#else
	sequential_or_dasum(size, X, IncX, result, propagate);
#endif
	double finalResult = 0;
	for (unsigned int k = 0; k < ONEREDUCTIONK; k++)
		finalResult += result[k] - 1.5 * ufp(result[k]) + 0.25 * propagate[k] * ufp(result[k]);
	return finalResult;
}

void reprodblas_or_dtrsv(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
#else
#endif
#elif defined(OMP_PARALLEL)
	IFP* bindexed = (IFP*) malloc(n * sizeof(IFP));
	dtrsvLowerOmpOneReductionRecursive(n, A, lda, X, IncX, bindexed, 1, 1);
#else
	dtrsvLowerSequentialOneReduction(n, A, lda, X, IncX);
#endif
}

void reprodblas_orir_dtrsv(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	// lol
#else
	// lol
#endif
#elif defined(OMP_PARALLEL)
	IFP* bindexed = (IFP*) malloc(n * sizeof(IFP));
	double* b = (double*) memalign(64, n * sizeof(double));
	double* d = (double*) memalign(64, n * sizeof(double));
	memcpy(b, X, n * sizeof(double));
	dtrsvLowerOmpOneReductionRecursiveSolveAndRefine(n, A, lda, X, IncX, d, 1, bindexed, 1, 1);
	dtrsvLowerOmpOneReductionRecursive(n, A, lda, d, 1, bindexed, 1, 1);
	cblas_daxpby(n, 1, d, 1, 1, X, 1);
	double normNew = sequentialAbsMax(n, d);
	double normOld;
	do {
		normOld = normNew;
		memcpy(d, b, n * sizeof(double));
		ompOneReductionGemvIRLower(n, n, A, lda, X, IncX, d, 1);
		dtrsvLowerOmpOneReductionRecursive(n, A, lda, d, 1, bindexed, 1, 1);
		cblas_daxpby(n, 1, d, 1, 1, X, 1);
		normNew = reprodblas_or_dasum(n, d, 1);
	} while ((normNew / sequentialAbsMax(n, X) > 2 * eps) && (2 * normNew < normOld));
	free(b);
	free(d);
	free(bindexed);
#else
	double* b = (double*) memalign(64, n * sizeof(double));
	double* d = (double*) memalign(64, n * sizeof(double));
	memcpy(b, X, n * sizeof(double));
	dtrsvLowerSequentialOneReductionSolveAndRefine(n, A, lda, X, IncX, d, 1);
	dtrsvLowerSequentialOneReduction(n, A, lda, d, 1);
	cblas_daxpby(n, 1, d, 1, 1, X, 1);
	double normNew = sequentialAbsMax(n, d);
	double normOld;
	do {
		normOld = normNew;
		memcpy(d, b, n * sizeof(double));
		sequentialOneReductionGemvIRLower(n, n, A, lda, X, IncX, d, 1);
		dtrsvLowerSequentialOneReduction(n, A, lda, d, 1);
		cblas_daxpby(n, 1, d, 1, 1, X, 1);
		normNew = reprodblas_or_dasum(n, d, 1);
	} while ((normNew / sequentialAbsMax(n, X) > 2 * eps) && (2 * normNew < normOld));
	free(b);
	free(d);
#endif
}

/*
void reprodblas_orir_dtrsv(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	// lol
#else
	// lol
#endif
#elif defined(OMP_PARALLEL)
	IFP* bindexed = (IFP*) malloc(n * sizeof(IFP));
	double* b = (double*) memalign(64, n * sizeof(double));
	double* d = (double*) memalign(64, n * sizeof(double));
	memcpy(b, X, n * sizeof(double));
	dtrsvLowerOmpOneReductionRecursiveSolveAndRefine(n, A, lda, X, IncX, d, 1, bindexed, 1, 1);
	double denominator = 1 / reprodblas_or_dasum(n, b, 1);
	double normNew = reprodblas_or_dasum(n, d, 1) * denominator;
	double normOld = normNew * 2;
	while (normOld > normNew) {
		normOld = normNew;
		dtrsvLowerOmpOneReductionRecursive(n, A, lda, d, 1, bindexed, 1, 1);
		cblas_daxpby(n, 1, d, 1, 1, X, 1);
		memcpy(d, b, n * sizeof(double));
		ompOneReductionGemvIRLower(n, n, A, lda, X, IncX, d, 1);
		normNew = reprodblas_or_dasum(n, d, 1) * denominator;
	}
	free(b);
	free(d);
	free(bindexed);
#else
	double* b = (double*) memalign(64, n * sizeof(double));
	double* d = (double*) memalign(64, n * sizeof(double));
	memcpy(b, X, n * sizeof(double));
	dtrsvLowerSequentialOneReductionSolveAndRefine(n, A, lda, X, IncX, d, 1);
	double denominator = 1 / reprodblas_or_dasum(n, b, 1);
	double normNew = reprodblas_or_dasum(n, d, 1) * denominator;
	double normOld = normNew * 2;
	while (normOld > normNew) {
		normOld = normNew;
		dtrsvLowerSequentialOneReduction(n, A, lda, d, 1);
		cblas_daxpby(n, 1, d, 1, 1, X, 1);
		memcpy(d, b, n * sizeof(double));
		sequentialOneReductionGemvIRLower(n, n, A, lda, X, IncX, d, 1);
		normNew = reprodblas_or_dasum(n, d, 1) * denominator;
	}
	free(b);
	free(d);
#endif
}
*/

void reprodblas_orsr_dtrsv(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	// lol
#else
	// lol
#endif
#elif defined(OMP_PARALLEL)
	IFP* bindexed = (IFP*) malloc(n * sizeof(IFP));
	double* b = (double*) memalign(64, n * sizeof(double));
	double* d = (double*) memalign(64, n * sizeof(double));
	memcpy(b, X, n * sizeof(double));
	dtrsvLowerOmpOneReductionRecursiveSolveAndRefine(n, A, lda, X, IncX, d, 1, bindexed, 1, 1);
	dtrsvLowerOmpOneReductionRecursive(n, A, lda, d, 1, bindexed, 1, 1);
	cblas_daxpby(n, 1, d, 1, 1, X, 1);
	free(b);
	free(d);
	free(bindexed);
#else
	double* b = (double*) memalign(64, n * sizeof(double));
	double* d = (double*) memalign(64, n * sizeof(double));
	memcpy(b, X, n * sizeof(double));
	dtrsvLowerSequentialOneReductionSolveAndRefine(n, A, lda, X, IncX, d, 1);
	dtrsvLowerSequentialOneReduction(n, A, lda, d, 1);
	cblas_daxpby(n, 1, d, 1, 1, X, 1);
	free(b);
	free(d);
#endif
}

/*
void reprodblas_orirtest_dtrsv(int layout, int uplo, int trans, int diag, unsigned int n, double* A, unsigned int lda, double* X, int IncX) {
	double* b = (double*) memalign(64, n * sizeof(double));
	double* d = (double*) memalign(64, n * sizeof(double));
	memcpy(b, X, n * sizeof(double));
	memcpy(d, X, n * sizeof(double));
	cblas_dtrsv(CblasRowMajor, CblasLower, CblasNoTrans, CblasNonUnit, n, A, lda, X, IncX);
	rtnblas_dgemv(CblasRowMajor, CblasNoTrans, n, n, 1, A, n, X, 1, -1, d, 1);
	double denominator = 1 / cblas_dasum(n, b, 1);
	double normNew = cblas_dasum(n, d, 1) * denominator;
	double normOld = normNew * 2;
	int i = 0;
	while (i < 20) {
		normOld = normNew;
		cblas_dtrsv(CblasRowMajor, CblasLower, CblasNoTrans, CblasNonUnit, n, A, lda, d, 1);
		cblas_daxpby(n, 1, d, 1, 1, X, 1);
		memcpy(d, b, n * sizeof(double));
		rtnblas_dgemv(CblasRowMajor, CblasNoTrans, n, n, 1, A, n, X, 1, -1, d, 1);
		normNew = cblas_dasum(n, d, 1) * denominator;
		i++;
	}
	free(b);
	free(d);
}
*/

void reprodblas_or_dgemv(
	int layout, int trans, unsigned int m, unsigned int n, double alpha, double *A,
	unsigned int lda, double *X, int IncX, double beta, double *Y, int IncY
) {
#ifdef MPI_PARALLEL
#ifdef OMP_PARALLEL
	// lol
#else
	// lol
#endif
#elif defined(OMP_PARALLEL)
	ompOneReductionGemv(m, n, alpha, A, lda, X, IncX, beta, Y, IncY);
#else
	sequentialOneReductionGemv(m, n, alpha, A, lda, X, IncX, beta, Y, IncY);
#endif
}

void reprodblas_dgemv(
	int layout, int trans, unsigned int m, unsigned int n, double alpha, double *A,
	unsigned int lda, double *X, int IncX, double beta, double *Y, int IncY
) {
	reprodblas_or_dgemv(layout, trans, m, n, alpha, A, lda, X, IncX, beta, Y, IncY);
}
