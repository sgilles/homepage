/*
 *************************************************************************************
 Copyright (c) 2015, University of Perpignan
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************
*/


#ifndef REORODBLAS_H_INCLUDED
#define REORODBLAS_H_INCLUDED

void reprodblasInitialise();

double reprodblas_rs_ddot(unsigned long, double*, int, double*, int);
double reprodblas_frs_ddot(unsigned long, double*, int, double*, int);
double reprodblas_or_ddot(unsigned long, double*, int, double*, int);
double reprodblas_ortp_ddot(unsigned long, double*, int, double*, int);

double reprodblas_rs_dnrm2(unsigned long, double*, int);
double reprodblas_frs_dnrm2(unsigned long, double*, int);
double reprodblas_or_dnrm2(unsigned long, double*, int);
double reprodblas_ortp_dnrm2(unsigned long, double*, int);

double reprodblas_rs_dasum(unsigned long, double*, int);
double reprodblas_frs_dasum(unsigned long, double*, int);
double reprodblas_or_dasum(unsigned long, double*, int);

void reprodblas_or_dtrsv(int, int, int, int, unsigned int, double*, unsigned int, double*, int);
void reprodblas_orir_dtrsv(int, int, int, int, unsigned int, double*, unsigned int, double*, int);
void reprodblas_orsr_dtrsv(int, int, int, int, unsigned int, double*, unsigned int, double*, int);

void reprodblas_or_dgemv(int, int, unsigned int, unsigned int, double, double*, unsigned int, double*, int, double, double*, int);
void reprodblas_dgemv(int, int, unsigned int, unsigned int, double, double*, unsigned int, double*, int, double, double*, int);

#ifdef STATS
void getReprodBlasStatistics(unsigned long*, unsigned long*, unsigned long*);
#endif

#endif

