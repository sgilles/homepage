/*
 *************************************************************************************
 Copyright (c) 2015, University of Perpignan
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************
*/


#include <math.h>
#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>
#include <mkl.h>
#include <immintrin.h>
#include <rtnblas.h>
#include <reprodblas.h>
#include <summation.h>
#include <string.h>
#include <string>
#include <iostream>
#include <linearalgebra.h>
#include <blas_extended.h>

#ifdef OMP_PARALLEL
#include <omp.h>
#endif

#ifdef MPI_PARALLEL
#include <mpi.h>
static int myRank;
static int MPI_NUMBER_OF_THREADS;
#endif

#ifndef STATS
#ifdef MPI_PARALLEL
#define timingsum(call)\
minmemorycycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	streamVector(size, tempX, X);\
	MPI_Barrier(MPI_COMM_WORLD);\
	cycles = rdtsc();\
	s = call;\
	cycles = rdtsc() - cycles;\
	MPI_Allreduce(&cycles, &reducecycles, 1, MPI_UNSIGNED_LONG, MPI_MAX, MPI_COMM_WORLD);\
	cycles = reducecycles;\
	minmemorycycles = (cycles < minmemorycycles) ? cycles : minmemorycycles;\
	meanmemorycycles += cycles;\
}\
meanmemorycycles /= iter;\
mincachecycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	copyVector(size, tempX, X);\
	MPI_Barrier(MPI_COMM_WORLD);\
	cycles = rdtsc();\
	s = call;\
	cycles = rdtsc() - cycles;\
	MPI_Allreduce(&cycles, &reducecycles, 1, MPI_UNSIGNED_LONG, MPI_MAX, MPI_COMM_WORLD);\
	cycles = reducecycles;\
	mincachecycles = (cycles < mincachecycles) ? cycles : mincachecycles;\
	meancachecycles += cycles;\
}\
meancachecycles /= iter;
#define timingdot(call)\
minmemorycycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	streamVector(size, tempX, X);\
	streamVector(size, tempY, Y);\
	MPI_Barrier(MPI_COMM_WORLD);\
	cycles = rdtsc();\
	s = call(size, X, 1, Y, 1);\
	cycles = rdtsc() - cycles;\
	MPI_Allreduce(&cycles, &reducecycles, 1, MPI_UNSIGNED_LONG, MPI_MAX, MPI_COMM_WORLD);\
	cycles = reducecycles;\
	minmemorycycles = (cycles < minmemorycycles) ? cycles : minmemorycycles;\
	meanmemorycycles += cycles;\
}\
meanmemorycycles /= iter;\
mincachecycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	MPI_Barrier(MPI_COMM_WORLD);\
	cycles = rdtsc();\
	s = call(size, X, 1, Y, 1);\
	cycles = rdtsc() - cycles;\
	MPI_Allreduce(&cycles, &reducecycles, 1, MPI_UNSIGNED_LONG, MPI_MAX, MPI_COMM_WORLD);\
	cycles = reducecycles;\
	mincachecycles = (cycles < mincachecycles) ? cycles : mincachecycles;\
	meancachecycles += cycles;\
}\
meancachecycles /= iter;
#define timinggemv(call)\
minmemorycycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	streamVector(size * lines, tempX, X);\
	allstreamVector(size, tempY, Y);\
	streamVector(lines, tempResults, sresults);\
	MPI_Barrier(MPI_COMM_WORLD);\
	cycles = rdtsc();\
	call(CblasRowMajor, CblasNoTrans, lines, size, 1, X, size, Y, 1, 1, sresults, 1);\
	cycles = rdtsc() - cycles;\
	MPI_Allreduce(&cycles, &reducecycles, 1, MPI_UNSIGNED_LONG, MPI_MAX, MPI_COMM_WORLD);\
	cycles = reducecycles;\
	minmemorycycles = (cycles < minmemorycycles) ? cycles : minmemorycycles;\
	meanmemorycycles += cycles;\
}\
meanmemorycycles /= iter;\
mincachecycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	copyVector(lines, tempResults, sresults);\
	MPI_Barrier(MPI_COMM_WORLD);\
	cycles = rdtsc();\
	call(CblasRowMajor, CblasNoTrans, lines, size, 1, X, size, Y, 1, 1, sresults, 1);\
	cycles = rdtsc() - cycles;\
	MPI_Allreduce(&cycles, &reducecycles, 1, MPI_UNSIGNED_LONG, MPI_MAX, MPI_COMM_WORLD);\
	cycles = reducecycles;\
	mincachecycles = (cycles < mincachecycles) ? cycles : mincachecycles;\
	meancachecycles += cycles;\
}\
meancachecycles /= iter;



#else
#define timingsum(call)\
minmemorycycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	streamVector(size, tempX, X);\
	cycles = rdtsc();\
	s = call;\
	cycles = rdtsc() - cycles;\
	minmemorycycles = (cycles < minmemorycycles) ? cycles : minmemorycycles;\
	meanmemorycycles += cycles;\
}\
meanmemorycycles /= iter;\
mincachecycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	copyVector(size, tempX, X);\
	cycles = rdtsc();\
	s = call;\
	cycles = rdtsc() - cycles;\
	mincachecycles = (cycles < mincachecycles) ? cycles : mincachecycles;\
	meancachecycles += cycles;\
}\
meancachecycles /= iter;
#define timingdot(call)\
minmemorycycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	streamVector(size, tempX, X);\
	streamVector(size, tempY, Y);\
	cycles = rdtsc();\
	s = call(size, X, 1, Y, 1);\
	cycles = rdtsc() - cycles;\
	minmemorycycles = (cycles < minmemorycycles) ? cycles : minmemorycycles;\
	meanmemorycycles += cycles;\
}\
meanmemorycycles /= iter;\
mincachecycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	cycles = rdtsc();\
	s = call(size, X, 1, Y, 1);\
	cycles = rdtsc() - cycles;\
	mincachecycles = (cycles < mincachecycles) ? cycles : mincachecycles;\
	meancachecycles += cycles;\
}\
meancachecycles /= iter;
#define timinggemv(call)\
minmemorycycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	streamVector(size * size, tempX, X);\
	streamVector(size, tempY, Y);\
	streamVector(size, tempResults, sresults);\
	cycles = rdtsc();\
	call(CblasRowMajor, CblasNoTrans, size, size, 1, X, size, Y, 1, 1, sresults, 1);\
	cycles = rdtsc() - cycles;\
	minmemorycycles = (cycles < minmemorycycles) ? cycles : minmemorycycles;\
	meanmemorycycles += cycles;\
}\
meanmemorycycles /= iter;\
mincachecycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	copyVector(size, tempResults, sresults);\
	cycles = rdtsc();\
	call(CblasRowMajor, CblasNoTrans, size, size, 1, X, size, Y, 1, 1, sresults, 1);\
	cycles = rdtsc() - cycles;\
	mincachecycles = (cycles < mincachecycles) ? cycles : mincachecycles;\
	meancachecycles += cycles;\
}\
meancachecycles /= iter;
#define timinggemm(call)\
minmemorycycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	streamVector(size * size, tempX, X);\
	streamVector(size * size, tempY, Y);\
	streamVector(size * size, tempResults, sresults);\
	cycles = rdtsc();\
	call(CblasRowMajor, CblasNoTrans, CblasNoTrans, size, size, size, 1, X, size, Y, size, 1, sresults, size);\
	cycles = rdtsc() - cycles;\
	minmemorycycles = (cycles < minmemorycycles) ? cycles : minmemorycycles;\
	meanmemorycycles += cycles;\
}\
meanmemorycycles /= iter;\
mincachecycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	copyVector(size * size, tempResults, sresults);\
	cycles = rdtsc();\
	call(CblasRowMajor, CblasNoTrans, CblasNoTrans, size, size, size, 1, X, size, Y, size, 1, sresults, size);\
	cycles = rdtsc() - cycles;\
	mincachecycles = (cycles < mincachecycles) ? cycles : mincachecycles;\
	meancachecycles += cycles;\
}\
meancachecycles /= iter;
#define timingtrsv(call)\
minmemorycycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	streamVector(size * size, tempX, X);\
	streamVector(size, tempY, Y);\
	cycles = rdtsc();\
	call(CblasRowMajor, CblasLower, CblasNoTrans, CblasNonUnit, size, X, size, Y, 1);\
	cycles = rdtsc() - cycles;\
	minmemorycycles = (cycles < minmemorycycles) ? cycles : minmemorycycles;\
	meanmemorycycles += cycles;\
}\
meanmemorycycles /= iter;\
mincachecycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	copyVector(size, tempY, Y);\
	cycles = rdtsc();\
	call(CblasRowMajor, CblasLower, CblasNoTrans, CblasNonUnit, size, X, size, Y, 1);\
	cycles = rdtsc() - cycles;\
	mincachecycles = (cycles < mincachecycles) ? cycles : mincachecycles;\
	meancachecycles += cycles;\
}\
meancachecycles /= iter;
#define timingxblastrsv(call)\
minmemorycycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	streamVector(size * size, tempX, X);\
	streamVector(size, tempY, Y);\
	cycles = rdtsc();\
	call;\
	cycles = rdtsc() - cycles;\
	minmemorycycles = (cycles < minmemorycycles) ? cycles : minmemorycycles;\
	meanmemorycycles += cycles;\
}\
meanmemorycycles /= iter;\
mincachecycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	copyVector(size, tempY, Y);\
	cycles = rdtsc();\
	call;\
	cycles = rdtsc() - cycles;\
	mincachecycles = (cycles < mincachecycles) ? cycles : mincachecycles;\
	meancachecycles += cycles;\
}\
meancachecycles /= iter;
#define timingsolver(call)\
minmemorycycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	streamVector(size * size, tempX, X);\
	streamVector(size, results, sresults);\
	cycles = rdtsc();\
	call(size, X, sresults);\
	cycles = rdtsc() - cycles;\
	minmemorycycles = (cycles < minmemorycycles) ? cycles : minmemorycycles;\
	meanmemorycycles += cycles;\
}\
meanmemorycycles /= iter;\
mincachecycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	copyVector(size * size, tempX, X);\
	copyVector(size, results, sresults);\
	cycles = rdtsc();\
	call(size, X, sresults);\
	cycles = rdtsc() - cycles;\
	mincachecycles = (cycles < mincachecycles) ? cycles : mincachecycles;\
	meancachecycles += cycles;\
}\
meancachecycles /= iter;
#endif
#endif

#ifdef STATS
#ifdef MPI_PARALLEL
#define timingstatsdot(call, stats)\
mincycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	streamVector(size, tempX, X);\
	streamVector(size, tempY, Y);\
	MPI_Barrier(MPI_COMM_WORLD);\
	begincycles = rdtsc();\
	s = call(size, X, 1, Y, 1);\
	cycles = rdtsc() - begincycles;\
	MPI_Allreduce(&cycles, &reducecycles, 1, MPI_UNSIGNED_LONG, MPI_MAX, MPI_COMM_WORLD);\
	cycles = reducecycles;\
	if (mincycles > cycles) {\
		mincycles = cycles;\
		stats;\
		MPI_Allreduce(&begincycles, &reducecycles, 1, MPI_UNSIGNED_LONG, MPI_MIN, MPI_COMM_WORLD);\
		begincycles = reducecycles;\
		MPI_Allreduce(&exponentExtractCycles, &reducecycles, 1, MPI_UNSIGNED_LONG, MPI_MIN, MPI_COMM_WORLD);\
		if (reducecycles) exponentExtractCycles = reducecycles - begincycles;\
		MPI_Bcast(&reduceAndUnionCycles, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);\
		if (reduceAndUnionCycles) reduceAndUnionCycles -= begincycles;\
		MPI_Allreduce(&localMaxCycles, &reducecycles, 1, MPI_UNSIGNED_LONG, MPI_MIN, MPI_COMM_WORLD);\
		if (reducecycles) localMaxCycles = reducecycles - begincycles;\
		MPI_Allreduce(&reduceMaxCycles, &reducecycles, 1, MPI_UNSIGNED_LONG, MPI_MAX, MPI_COMM_WORLD);\
		if (reducecycles) reduceMaxCycles = reducecycles - begincycles;\
		MPI_Allreduce(&localSumCycles, &reducecycles, 1, MPI_UNSIGNED_LONG, MPI_MIN, MPI_COMM_WORLD);\
		if (reducecycles) localSumCycles = reducecycles - begincycles;\
	}\
}
#else
#define timingstatsdot(call, stats)\
mincycles = pow(2, 62);\
for (int i = 0; i < iter; i++) {\
	streamVector(size, tempX, X);\
	streamVector(size, tempY, Y);\
	begincycles = rdtsc();\
	s = call(size, X, 1, Y, 1);\
	cycles = rdtsc() - begincycles;\
	if (mincycles > cycles) {\
		mincycles = cycles;\
		stats;\
		if (reduceMaxCycles) reduceMaxCycles -= begincycles;\
		if (exponentExtractCycles) exponentExtractCycles -= begincycles;\
		if (localSumCycles) localSumCycles -= begincycles;\
		if (localMaxCycles) localMaxCycles -= begincycles;\
		if (reduceAndUnionCycles) reduceAndUnionCycles -= begincycles;\
		if (firstPass) firstPass -= begincycles;\
	}\
}
#endif
#endif

double verifiedexactdasum(unsigned long size, double* X) {
	for (unsigned long i = 0; i < size; i++) {
		X[i] = fabs(X[i]);
	}
	return dynamicSum(size, X);
}

void initGeneratedSumVector(double* X, char* filename, unsigned long size, double* sum) {
#ifdef MPI_PARALLEL
	int r;
	double cond;
	double *tmpX;
	unsigned long fileSize;
	FILE* file;
	unsigned int add;
	if (!myRank) {
		file = fopen(filename, "rb");
		if (file == NULL) {
			printf("Can not open file %s\n", filename);
			exit(1);
		}
		r = fread(&fileSize, sizeof(unsigned long), 1, file);
		if (r == 0) {
			printf("Can not read size from file %s\n", filename);
			exit(1);
		}
		add = (MPI_NUMBER_OF_THREADS - (size % MPI_NUMBER_OF_THREADS)) % MPI_NUMBER_OF_THREADS;
		tmpX = (double*) memalign(64, (size + add) * sizeof(double));
		r = fread(tmpX, sizeof(double), fileSize, file);
		if (r == 0) {
			printf("Can not read vector X from file %s\n", filename);
			exit(1);
		}
		r = fread(sum, sizeof(double), 1, file);
		if (r == 0) {
			printf("Can not read sum from file %s\n", filename);
			exit(1);
		}
		r = fread(&cond, sizeof(double), 1, file);  
		if (r == 0) {
			printf("Can not read condition from file %s\n", filename);
			exit(1);
		}
		fclose(file);
		for (unsigned long index = fileSize; index < size; index++) {
			unsigned long firstIndex = index % fileSize;
			tmpX[index] = tmpX[firstIndex];
		}
		for (unsigned long index = size; index < size + add; index++) {
			tmpX[index] = 0;
		}
		unsigned long times = size / fileSize;
		*sum *= times;
	}
	int localSize = (size + add) / MPI_NUMBER_OF_THREADS;
	MPI_Bcast(sum, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Scatter(tmpX, localSize, MPI_DOUBLE, X, localSize, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	free(tmpX);
#else
	int r;
	double cond;
	unsigned long fileSize;
	FILE* file;
	file = fopen(filename, "rb");
	if (file == NULL) {
		printf("Can not open file %s\n", filename);
		exit(1);
	}
	r = fread(&fileSize, sizeof(unsigned long), 1, file);
	if (r == 0) {
		printf("Can not read size from file %s\n", filename);
		exit(1);
	}
	r = fread(X, sizeof(double), fileSize, file);
	if (r == 0) {
		printf("Can not read vector X from file %s\n", filename);
		exit(1);
	}
	r = fread(sum, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read sum from file %s\n", filename);
		exit(1);
	}
	r = fread(&cond, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read condition from file %s\n", filename);
		exit(1);
	}
	unsigned long times = size / fileSize;
	*sum *= times;
	for (unsigned long index = fileSize; index < size; index++) {
		unsigned long firstIndex = index % fileSize;
		X[index] = X[firstIndex];
	}
	fclose(file);
#endif
}

void initGeneratedDotVector(double* X, double* Y, char* filename, unsigned long size, double* dot) {
#ifdef MPI_PARALLEL
	int r;
	double cond;
	double *tmpX, *tmpY;
	unsigned long fileSize;
	unsigned int add;
	FILE* file;
	if (!myRank) {
		file = fopen(filename, "rb");
		if (file == NULL) {
			printf("Can not open file %s\n", filename);
			exit(1);
		}
		r = fread(&fileSize, sizeof(unsigned long), 1, file);
		if (r == 0) {
			printf("Can not read the size from file %s\n", filename);
			exit(1);
		}
		add = (MPI_NUMBER_OF_THREADS - (size % MPI_NUMBER_OF_THREADS)) % MPI_NUMBER_OF_THREADS;
		tmpX = (double*) memalign(64, (size + add) * sizeof(double));
		tmpY = (double*) memalign(64, (size + add) * sizeof(double));
		r = fread(tmpX, sizeof(double), fileSize, file);
		if (r == 0) {
			printf("Can not read file the vector X from file %s\n", filename);
			exit(1);
		}
		r = fread(tmpY, sizeof(double), fileSize, file);
		if (r == 0) {
			printf("Can not read file the vector Y from file %s\n", filename);
			exit(1);
		}
		r = fread(dot, sizeof(double), 1, file);
		if (r == 0) {
			printf("Can not read the result from file %s\n", filename);
			exit(1);
		}
		r = fread(&cond, sizeof(double), 1, file);  
		if (r == 0) {
			printf("Can not read the condition from file %s\n", filename);
			exit(1);
		}
		fclose(file);
		for (unsigned long index = fileSize; index < size; index++) {
			unsigned long firstIndex = index % fileSize;
			tmpX[index] = tmpX[firstIndex];
			tmpY[index] = tmpY[firstIndex];
		}
		for (unsigned long index = size; index < size + add; index++) {
			tmpX[index] = 0;
			tmpY[index] = 0;
		}
		unsigned long times = size / fileSize;
		*dot *= times;
	}
	int localSize = (size + add) / MPI_NUMBER_OF_THREADS;
	MPI_Bcast(dot, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Scatter(tmpX, localSize, MPI_DOUBLE, X, localSize, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Scatter(tmpY, localSize, MPI_DOUBLE, Y, localSize, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	free(tmpX);
	free(tmpY);
#else
	int r;
	double cond;
	unsigned long fileSize;
	FILE* file;
	file = fopen(filename, "rb");
	if (file == NULL) {
		printf("Can not open file %s\n", filename);
		exit(1);
	}
	r = fread(&fileSize, sizeof(unsigned long), 1, file);
	if (r == 0) {
		printf("Can not read size from file %s\n", filename);
		exit(1);
	}
	r = fread(X, sizeof(double), fileSize, file);
	if (r == 0) {
		printf("Can not read the vector X from file %s\n", filename);
		exit(1);
	}
	r = fread(Y, sizeof(double), fileSize, file);
	if (r == 0) {
		printf("Can not read the vector Y from file %s\n", filename);
		exit(1);
	}
	r = fread(dot, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read the result from file %s\n", filename);
		exit(1);
	}
	r = fread(&cond, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read the condition from file %s\n", filename);
		exit(1);
	}
	fclose(file);
	for (unsigned long index = fileSize; index < size; index++) {
		unsigned long firstIndex = index % fileSize;
		X[index] = X[firstIndex];
		Y[index] = Y[firstIndex];
	}
	unsigned long times = size / fileSize;
	*dot *= times;
#endif
}

void initGeneratedGemvVector(double* X, double* Y, char* filename, unsigned long size, double* result) {
#ifdef MPI_PARALLEL
	int r;
	double Cmin, Cmax;
	double *tmpX, *tmpResult;
	unsigned long fileSize;
	FILE* file;
	if (!myRank) {
		file = fopen(filename, "rb");
		if (file == NULL) {
			printf("Can not open file %s\n", filename);
			exit(1);
		}
		r = fread(&fileSize, sizeof(unsigned long), 1, file);
		if (r == 0) {
			printf("Can not read size from file %s\n", filename);
			exit(1);
		}
	}
	unsigned long columns = size;
	unsigned int lines = (100 < size) ? 100 : size;
	unsigned int add = (MPI_NUMBER_OF_THREADS - (size % MPI_NUMBER_OF_THREADS)) % MPI_NUMBER_OF_THREADS;
	tmpX = (double*) memalign(64, (size + add) * size * sizeof(double));
	tmpResult = (double*) memalign(64, size * sizeof(double));
	if (!myRank) {
		r = fread(tmpX, sizeof(double), columns * lines, file);
		if (r == 0) {
			printf("Can not read matrix X from file %s\n", filename);
			exit(1);
		}
		r = fread(Y, sizeof(double), columns, file);
		if (r == 0) {
			printf("Can not read vector Y from file %s\n", filename);
			exit(1);
		}
		r = fread(tmpResult, sizeof(double), lines, file);
		if (r == 0) {
			printf("Can not read results from file %s\n", filename);
			exit(1);
		}
		r = fread(&Cmin, sizeof(double), 1, file);  
		if (r == 0) {
			printf("Can not read minimal condition from file %s\n", filename);
			exit(1);
		}
		r = fread(&Cmax, sizeof(double), 1, file);  
		if (r == 0) {
			printf("Can not read maximal condition from file %s\n", filename);
			exit(1);
		}
		fclose(file);
		for (unsigned long line = lines; line < size; line += lines) {
			memcpy(&tmpX[line * columns], tmpX, lines * columns * sizeof(double));
			memcpy(&tmpResult[line], tmpResult, lines * sizeof(double));
		}
		for (unsigned long line = size; line < size + add; line++) {
			tmpResult[line] = 0;
			for (unsigned long column = 0; column < size; column++) {
				tmpX[line * columns + column] = 0;
			}
		}
	}
	int localSize = (size + add) / MPI_NUMBER_OF_THREADS;
	MPI_Bcast(Y, columns, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Scatter(tmpX, localSize * columns, MPI_DOUBLE, X, localSize, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Scatter(tmpResult, localSize, MPI_DOUBLE, result, localSize, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	free(tmpX);
	free(tmpResult);
#else
	int r;
	unsigned long fileSize;
	double Cmin, Cmax;
	unsigned int lines = (100 < size) ? 100 : size;
	unsigned int columns = size;
	FILE* file;
	file = fopen(filename, "rb");
	if (file == NULL) {
		printf("Can not open file %s\n", filename);
		exit(1);
	}
	r = fread(&fileSize, sizeof(unsigned long), 1, file);
	if (r == 0) {
		printf("Can not read size from file %s\n", filename);
		exit(1);
	}
	r = fread(X, sizeof(double), lines * columns, file);
	if (r == 0) {
		printf("Can not read matrix X from file %s\n", filename);
		exit(1);
	}
	r = fread(Y, sizeof(double), columns, file);
	if (r == 0) {
		printf("Can not read vector Y from file %s\n", filename);
		exit(1);
	}
	r = fread(result, sizeof(double), lines, file);
	if (r == 0) {
		printf("Can not read results from file %s\n", filename);
		exit(1);
	}
	r = fread(&Cmin, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read minimal condition from file %s\n", filename);
		exit(1);
	}
	r = fread(&Cmax, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read maximal condition from file %s\n", filename);
		exit(1);
	}
	for (unsigned long line = lines; line < size; line += lines) {
		memcpy(&X[line * columns], X, lines * columns * sizeof(double));
		memcpy(&result[line], result, lines * sizeof(double));
	}
	fclose(file);
#endif
}

void initGeneratedTrsvVector(double* A, double* b, char* filename, unsigned long size, double* x) {
	int r;
	unsigned long fileSize;
	double Cmin, Cmax;
	unsigned int lines = size;
	unsigned int columns = size;
	FILE* file;
	file = fopen(filename, "rb");
	if (file == NULL) {
		printf("Can not open file %s\n", filename);
		exit(1);
	}
	r = fread(&fileSize, sizeof(unsigned long), 1, file);
	if (r == 0) {
		printf("Can not read file %s\n", filename);
		exit(1);
	}
	r = fread(A, sizeof(double), lines * columns, file);
	if (r == 0) {
		printf("Can not read file %s\n", filename);
		exit(1);
	}
	r = fread(x, sizeof(double), columns, file);
	if (r == 0) {
		printf("Can not read file %s\n", filename);
		exit(1);
	}
	r = fread(b, sizeof(double), lines, file);
	if (r == 0) {
		printf("Can not read file %s\n", filename);
		exit(1);
	}
	r = fread(&Cmin, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read file %s\n", filename);
		exit(1);
	}
	r = fread(&Cmax, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read file %s\n", filename);
		exit(1);
	}
	fclose(file);
}

/*
void initGeneratedTrsvVector(double* A, double* b, char* filename, unsigned long size, double* x) {
	int r;
	unsigned long fileSize;
	double Cmin, Cmax;
	unsigned int lines = 15000;
	unsigned int columns = 15000;
	double* tempA = (double*) memalign(64, lines * columns * sizeof(double));
	double* tempX = (double*) memalign(64, columns * sizeof(double));
	double* tempB = (double*) memalign(64, lines * sizeof(double));
	FILE* file;
	filename = "./datatrsv/c7s15000.dat";
	file = fopen(filename, "rb");
	if (file == NULL) {
		printf("Can not open file 1 %s\n", filename);
		exit(1);
	}
	r = fread(&fileSize, sizeof(unsigned long), 1, file);
	if (r == 0) {
		printf("Can not read file 2 %s\n", filename);
		exit(1);
	}
	r = fread(tempA, sizeof(double), lines * columns, file);
	if (r == 0) {
		printf("Can not read file 3 %s\n", filename);
		exit(1);
	}
	r = fread(tempX, sizeof(double), columns, file);
	if (r == 0) {
		printf("Can not read file 4 %s\n", filename);
		exit(1);
	}
	r = fread(tempB, sizeof(double), lines, file);
	if (r == 0) {
		printf("Can not read file 5 %s\n", filename);
		exit(1);
	}
	r = fread(&Cmin, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read file 6 %s\n", filename);
		exit(1);
	}
	r = fread(&Cmax, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read file 7 %s\n", filename);
		exit(1);
	}
	memcpy(x, tempX, size * sizeof(double));
	memcpy(b, tempB, size * sizeof(double));
	for (unsigned int i = 0; i < size; i++) memcpy(&A[i * size], &tempA[i * columns], size * sizeof(double));
	fclose(file);
}
*/

void initGeneratedGemmVector(double* X, double* Y, char* filename, unsigned long size, double* result) {
	int r;
	unsigned long fileSize;
	double Cmin, Cmax;
	unsigned int lines = (100 < size) ? 100 : size;
	unsigned int columns = size;
	FILE* file;
	file = fopen(filename, "rb");
	if (file == NULL) {
		printf("Can not open file %s\n", filename);
		exit(1);
	}
	r = fread(&fileSize, sizeof(unsigned long), 1, file);
	if (r == 0) {
		printf("Can not read size from file %s\n", filename);
		exit(1);
	}
	r = fread(X, sizeof(double), lines * columns, file);
	if (r == 0) {
		printf("Can not read matrix X from file %s\n", filename);
		exit(1);
	}
	r = fread(Y, sizeof(double), columns, file);
	if (r == 0) {
		printf("Can not read vector Y from file %s\n", filename);
		exit(1);
	}
	r = fread(result, sizeof(double), lines, file);
	if (r == 0) {
		printf("Can not read results from file %s\n", filename);
		exit(1);
	}
	r = fread(&Cmin, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read minimal condition from file %s\n", filename);
		exit(1);
	}
	r = fread(&Cmax, sizeof(double), 1, file);
	if (r == 0) {
		printf("Can not read maximal condition from file %s\n", filename);
		exit(1);
	}
	for (unsigned long line = lines; line < size; line += lines) {
		memcpy(&X[line * columns], X, lines * columns * sizeof(double));
		memcpy(&result[line], result, lines * sizeof(double));
	}
	for (unsigned long line = 1; line < size; line++) {
		memcpy(&Y[line * size], Y, size * sizeof(double));
		memcpy(&result[line * size], result, size * sizeof(double));
	}
	for (unsigned long line = 0; line < size; line++)
		for (unsigned long column = 0; column < size; column++) {
			Y[line * size + column] = Y[line * size + line];
			result[line * size + column] = result[line * size + line];
		}
	fclose(file);
}

// Utilisation de l'instruction _mm_stream_pd qui compatible à partir de SSE2
void allstreamVector(unsigned int n, double *src, double *dest) {
	for (int i = 0; i < n - 1; i += 2) _mm_stream_pd(&dest[i], *(__m128d*) &src[i]);
}

void streamVector(unsigned int n, double *src, double *dest) {
#ifdef MPI_PARALLEL
	n /= MPI_NUMBER_OF_THREADS;
#endif
	for (int i = 0; i < n - 1; i += 2) _mm_stream_pd(&dest[i], *(__m128d*) &src[i]);
}

void copyVector(unsigned int n, double *src, double *dest) {
#ifdef MPI_PARALLEL
	n /= MPI_NUMBER_OF_THREADS;
#endif
	for (int i = 0; i < n; i++) dest[i] = src[i];
}

/*
void testNonReprod_dtrsv(
	CBLAS_LAYOUT layout, CBLAS_UPLO uplo, CBLAS_TRANSPOSE trans, CBLAS_DIAG diag,
	unsigned int n, double* A, unsigned int lda, double* X, int IncX
) {
	unsigned int blockSize = 1200;
	unsigned int NumberOfBlocks = (n / blockSize) + !!(n % blockSize);
	omp_lock_t* XwriteLock = (omp_lock_t*) malloc(NumberOfBlocks * sizeof(omp_lock_t));
	for (unsigned int i = 0; i < NumberOfBlocks; i++) omp_init_lock(&XwriteLock[i]);
	omp_lock_t* GemvLock = (omp_lock_t*) malloc(NumberOfBlocks * sizeof(omp_lock_t));
	for (unsigned int i = 0; i < NumberOfBlocks; i++) {
		omp_init_lock(&GemvLock[i]);
		omp_set_lock(&GemvLock[i]);
	}
	omp_lock_t* TrsvLock = (omp_lock_t*) malloc(NumberOfBlocks * NumberOfBlocks * sizeof(omp_lock_t));
	for (unsigned int i = 0; i < NumberOfBlocks * NumberOfBlocks; i++) {
		omp_init_lock(&TrsvLock[i]);
		omp_set_lock(&TrsvLock[i]);
	}
#pragma omp parallel
	{
#pragma omp single
		{
			for (unsigned int step = 0; step < n; step += blockSize) {
				unsigned int minBlockSize = n - step;
				minBlockSize = minBlockSize < blockSize ? minBlockSize : blockSize;
#pragma omp task
{
				for (unsigned int i = 0; i < step / blockSize; i++)
					omp_set_lock(&TrsvLock[(step / blockSize) * NumberOfBlocks + i]);
				cblas_dtrsv(layout, uplo, trans, diag, minBlockSize, &A[step + step * lda], lda, &X[step], IncX);
				omp_unset_lock(&GemvLock[step / blockSize]);
}
				for (unsigned int verticalStep = step + minBlockSize; verticalStep < n; verticalStep += blockSize) {
					unsigned int minVerticalBlockSize = n - verticalStep;
					minVerticalBlockSize = minVerticalBlockSize < blockSize ? minVerticalBlockSize : blockSize;
					double* tempB = (double *) memalign(64, minVerticalBlockSize * sizeof(double));
#pragma omp task
					{
						omp_set_lock(&GemvLock[step / blockSize]);
						omp_unset_lock(&GemvLock[step / blockSize]);
						cblas_dgemv(
							CblasRowMajor, CblasNoTrans, minVerticalBlockSize, minBlockSize,
							-1, &A[step + verticalStep * lda], lda, &X[step], 1, 0, tempB, 1
						);
						omp_set_lock(&XwriteLock[step / blockSize]);
						cblas_daxpy(minVerticalBlockSize, 1, tempB, 1, &X[verticalStep], 1);
						omp_unset_lock(&XwriteLock[step / blockSize]);
						omp_unset_lock(&TrsvLock[(verticalStep / blockSize) * NumberOfBlocks + step / blockSize]);
					}
				}
			}
		}
	}
}
*/

int main(int argc, char** argv) {
	if (argc > 4) {
#ifdef MIC
		setenv("OMP_NUM_THREADS", argv[8], 1);
		setenv("KMP_AFFINITY", "scatter", 1);
#endif

#ifdef MPI_PARALLEL
		MPI_Init(&argc, &argv);
		MPI_Comm_size(MPI_COMM_WORLD, &MPI_NUMBER_OF_THREADS);
		MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
#endif

		double *X, *Y, *results;
		double *tempX, *tempY, *tempResults;
		double result;
		unsigned long size = atoi(argv[2]);

#ifdef MPI_PARALLEL
		unsigned int add = (MPI_NUMBER_OF_THREADS - (size % MPI_NUMBER_OF_THREADS)) % MPI_NUMBER_OF_THREADS;
		unsigned long lines = size + add;

		if (!strcmp(argv[4], "gemm")) {
			X = (double*) memalign(64, size * size * sizeof(double));
			Y = (double*) memalign(64, size * size * sizeof(double));
			results = (double*) memalign(64, size * size * sizeof(double));
			tempResults = (double*) memalign(64, size * size * sizeof(double));
			for (unsigned long i = 0; i < size * size; i++) tempResults[i] = 0;
		}
		if (!strcmp(argv[4], "gemv")) {
			Y = (double*) memalign(64, size * sizeof(double));
			X = (double*) memalign(64, lines * size * sizeof(double));
			results = (double*) memalign(64, lines * sizeof(double));
			tempResults = (double*) memalign(64, lines * sizeof(double));
			for (unsigned long i = 0; i < lines; i++) tempResults[i] = 0;
		}
		if (!strcmp(argv[4], "trsv")) {
			X = (double*) memalign(64, size * size * sizeof(double));
			Y = (double*) memalign(64, size * sizeof(double));
			results = (double*) memalign(64, size * sizeof(double));
		}
		if (!strcmp(argv[4], "dot")) {
			X = (double*) memalign(64, lines * sizeof(double));
			Y = (double*) memalign(64, lines * sizeof(double));
		}
		if (!strcmp(argv[4], "sum")) {
			X = (double*) memalign(64, lines * sizeof(double));
		}
#else
		if (!strcmp(argv[4], "gemm")) {
			X = (double*) memalign(64, size * size * sizeof(double));
			Y = (double*) memalign(64, size * size * sizeof(double));
			results = (double*) memalign(64, size * size * sizeof(double));
			tempResults = (double*) memalign(64, size * size * sizeof(double));
			for (unsigned long i = 0; i < size * size; i++) tempResults[i] = 0;
		}
		if (!strcmp(argv[4], "gemv")) {
			Y = (double*) memalign(64, size * sizeof(double));
			X = (double*) memalign(64, size * size * sizeof(double));
			results = (double*) memalign(64, size * sizeof(double));
			tempResults = (double*) memalign(64, size * sizeof(double));
			for (unsigned long i = 0; i < size; i++) tempResults[i] = 0;
		}
		if (!strcmp(argv[4], "trsv")) {
			X = (double*) memalign(64, size * size * sizeof(double));
			Y = (double*) memalign(64, size * sizeof(double));
			results = (double*) memalign(64, size * sizeof(double));
		}
		if (!strcmp(argv[4], "dot")) {
			X = (double*) memalign(64, size * sizeof(double));
			Y = (double*) memalign(64, size * sizeof(double));
		}
		if (!strcmp(argv[4], "sum")) {
			X = (double*) memalign(64, size * sizeof(double));
		}
#endif

		linearAlgebraInitialise();
		char filename[100] = "";

#ifdef MIC
		if (size > 100000) sprintf(filename, "/home/cchohra/data%s/c%ss100000.dat", argv[4], argv[3]);
		else sprintf(filename, "/home/cchohra/data%s/c%ss%s.dat", argv[4], argv[3], argv[2]);
#else
		if (size > 100000) sprintf(filename, "./data%s/c%ss100000.dat", argv[4], argv[3]);
		else sprintf(filename, "./data%s/c%ss%s.dat", argv[4], argv[3], argv[2]);
#endif

		if (!strcmp(argv[4], "gemm")) initGeneratedGemmVector(X, Y, filename, size, results);
		if (!strcmp(argv[4], "gemv")) initGeneratedGemvVector(X, Y, filename, size, results);
		if (!strcmp(argv[4], "trsv")) initGeneratedTrsvVector(X, Y, filename, size, results);
		if (!strcmp(argv[4], "dot")) initGeneratedDotVector(X, Y, filename, size, &result);
		if (!strcmp(argv[4], "sum")) initGeneratedSumVector(X, filename, size, &result);

#ifdef MPI_PARALLEL
		if (!strcmp(argv[4], "gemm")) {
			tempX = (double*) memalign(64, size * size * sizeof(double));
			tempY = (double*) memalign(64, size * size * sizeof(double));
			memcpy(tempX, X, size * size * sizeof(double));
			memcpy(tempY, Y, size * size * sizeof(double));
		}
		if (!strcmp(argv[4], "gemv")) {
			tempX = (double*) memalign(64, lines * size * sizeof(double));
			tempY = (double*) memalign(64, size * sizeof(double));
			memcpy(tempX, X, lines * size * sizeof(double));
			memcpy(tempY, Y, size * sizeof(double));
		}
		if (!strcmp(argv[4], "trsv")) {
			tempX = (double*) memalign(64, size * size * sizeof(double));
			tempY = (double*) memalign(64, size * sizeof(double));
			memcpy(tempX, X, size * size * sizeof(double));
			memcpy(tempY, Y, size * sizeof(double));
		}
		if (!strcmp(argv[4], "dot")) {
			tempX = (double*) memalign(64, lines * sizeof(double));
			tempY = (double*) memalign(64, lines * sizeof(double));
			memcpy(tempX, X, lines * sizeof(double));
			memcpy(tempY, Y, lines * sizeof(double));
		}
		if (!strcmp(argv[4], "sum")) {
			tempX = (double*) memalign(64, lines * sizeof(double));
			memcpy(tempX, X, lines * sizeof(double));
		}
#else
		if (!strcmp(argv[4], "gemm")) {
			tempX = (double*) memalign(64, size * size * sizeof(double));
			tempY = (double*) memalign(64, size * size * sizeof(double));
			memcpy(tempX, X, size * size * sizeof(double));
			memcpy(tempY, Y, size * size * sizeof(double));
		}
		if (!strcmp(argv[4], "gemv")) {
			tempX = (double*) memalign(64, size * size * sizeof(double));
			tempY = (double*) memalign(64, size * sizeof(double));
			memcpy(tempX, X, size * size * sizeof(double));
			memcpy(tempY, Y, size * sizeof(double));
		}
		if (!strcmp(argv[4], "trsv")) {
			tempX = (double*) memalign(64, size * size * sizeof(double));
			tempY = (double*) memalign(64, size * sizeof(double));
			memcpy(tempX, X, size * size * sizeof(double));
			memcpy(tempY, Y, size * sizeof(double));
		}
		if (!strcmp(argv[4], "dot")) {
			tempX = (double*) memalign(64, size * sizeof(double));
			tempY = (double*) memalign(64, size * sizeof(double));
			memcpy(tempX, X, size * sizeof(double));
			memcpy(tempY, Y, size * sizeof(double));
		}
		if (!strcmp(argv[4], "sum")) {
			tempX = (double*) memalign(64, size * sizeof(double));
			memcpy(tempX, X, size * sizeof(double));
		}
#endif

#if defined(STATS)
		unsigned int iter = atoi(argv[5]);
		unsigned long begincycles, reducecycles, minbegincycles, cycles, mincycles = 0;
		unsigned long exponentExtractCycles = 0;
		unsigned long reduceAndUnionCycles = 0;
		unsigned long reduceMaxCycles = 0;
		unsigned long localSumCycles = 0;
		unsigned long localMaxCycles = 0;
		unsigned long firstPass = 0;
		double s;
		if (!strcmp(argv[1], "OneReductionDot")) {
			timingstatsdot(
				reprodblas_or_ddot,
				getReprodBlasStatistics(&localMaxCycles, &reduceMaxCycles, &localSumCycles)
			)
		}
		if (!strcmp(argv[1], "OneReductionTwoProdDot")) {
			timingstatsdot(
				reprodblas_ortp_ddot,
				getReprodBlasStatistics(&localMaxCycles, &reduceMaxCycles, &localSumCycles)
			)
		}
		if (!strcmp(argv[1], "FastReprodDot")) {
			timingstatsdot(
				reprodblas_frs_ddot,
				getReprodBlasStatistics(&localMaxCycles, &reduceMaxCycles, &localSumCycles); getSummationStatistics(&localSumCycles)
			)
		}
		if (!strcmp(argv[1], "ReprodDot")) {
			timingstatsdot(
				reprodblas_rs_ddot,
				getReprodBlasStatistics(&localMaxCycles, &reduceMaxCycles, &localSumCycles); getSummationStatistics(&localSumCycles)
			)
		}
		if (!strcmp(argv[1], "OnlineExactDot")) {
			timingstatsdot(
				rtnblas_ol_ddot,
				getRtnBlasStatistics(&exponentExtractCycles, &reduceAndUnionCycles, &localSumCycles, &firstPass)
			)
		}
		if (!strcmp(argv[1], "HybridSumDot")) {
			timingstatsdot(
				rtnblas_hs_ddot,
				getRtnBlasStatistics(&exponentExtractCycles, &reduceAndUnionCycles, &localSumCycles, &firstPass)
			)
		}
		if (!strcmp(argv[1], "FastAccSumDot")) {
			timingstatsdot(
				rtnblas_facs_ddot,
				getRtnBlasStatistics(&exponentExtractCycles, &reduceAndUnionCycles, &localSumCycles, &firstPass)
			)
		}
		if (!strcmp(argv[1], "ClassicDot")) {
			timingstatsdot(
				classic_ddot,
				getRtnBlasStatistics(&exponentExtractCycles, &reduceAndUnionCycles, &localSumCycles, &firstPass)
			)
		}

#ifdef MPI_PARALLEL
		if (!myRank) {
#endif

#if defined(MPI_PARALLEL) || defined(OMP_PARALLEL)
			printf(
				"%s, %s, %g, %g, %lld, %lld, %lld, %lld, %lld, %lld, %lld, %s\n",
				argv[6], argv[2], s, result, mincycles, reduceAndUnionCycles, exponentExtractCycles,
				localSumCycles, reduceMaxCycles, localMaxCycles, firstPass, argv[7]
			);
#else
			if (!strcmp(argv[1], "OnlineExactDot") || !strcmp(argv[1], "HybridSumDot"))
				printf(
					"%s, %s, %g, %g, %lld, %lld, %lld, %lld, %lld, %lld, %lld, %s\n",
					argv[6], argv[2], s, result, mincycles, reduceAndUnionCycles, exponentExtractCycles,
					localSumCycles, reduceMaxCycles, localMaxCycles, firstPass, argv[7]
				);
			else
				printf(
					"%s, %s, %g, %g, %lld, %lld, %lld, %lld, %lld, %lld, %lld, %s\n",
					argv[6], argv[2], s, result, localSumCycles, reduceAndUnionCycles, exponentExtractCycles,
					mincycles, reduceMaxCycles, localMaxCycles, firstPass, argv[7]
				);
#endif

#ifdef MPI_PARALLEL
		}
#endif

#else
		unsigned int  iter = atoi(argv[5]);
		unsigned long cycles, reducecycles;
		unsigned long mincachecycles = 0;
		unsigned long meancachecycles = 0;
		unsigned long minmemorycycles = 0;
		unsigned long meanmemorycycles = 0;
		double s;
		double *sresults;

#ifdef MPI_PARALLEL
		if (!strcmp(argv[4], "gemm")) {
			sresults = (double*) memalign(64, size * size * sizeof(double));
			copyVector(size * size, tempResults, sresults);\
		}
		if (!strcmp(argv[4], "gemv")) {
			sresults = (double*) memalign(64, lines * sizeof(double));
			copyVector(lines, tempResults, sresults);\
		}
#else
		if (!strcmp(argv[4], "gemm")) {
			sresults = (double*) memalign(64, size * size * sizeof(double));
			copyVector(size * size, tempResults, sresults);\
		}
		if (!strcmp(argv[4], "gemv")) {
			sresults = (double*) memalign(64, size * sizeof(double));
			copyVector(size, tempResults, sresults);\
		}
#endif

#ifndef MPI_PARALLEL
		if (!strcmp(argv[1], "linearSolver")) {
			timingsolver(solveSystem)
		}
		if (!strcmp(argv[1], "ClassicGemm")) {
			timinggemm(cblas_dgemm)
		}
		if (!strcmp(argv[1], "ClassicGemmCompatible")) {
			mkl_cbwr_set(MKL_CBWR_COMPATIBLE);
			timinggemm(cblas_dgemm)
		}
		if (!strcmp(argv[1], "ClassicGemmSSE2")) {
			mkl_cbwr_set(MKL_CBWR_SSE2);
			timinggemm(cblas_dgemm)
		}
		if (!strcmp(argv[1], "ClassicGemmSSE3")) {
			mkl_cbwr_set(MKL_CBWR_SSE3);
			timinggemm(cblas_dgemm)
		}
		if (!strcmp(argv[1], "ClassicGemmSSSE3")) {
			mkl_cbwr_set(MKL_CBWR_SSSE3);
			timinggemm(cblas_dgemm)
		}
		if (!strcmp(argv[1], "ClassicGemmSSE4_1")) {
			mkl_cbwr_set(MKL_CBWR_SSE4_1);
			timinggemm(cblas_dgemm)
		}
		if (!strcmp(argv[1], "ClassicGemmSSE4_2")) {
			mkl_cbwr_set(MKL_CBWR_SSE4_2);
			timinggemm(cblas_dgemm)
		}
		if (!strcmp(argv[1], "ClassicGemmAVX")) {
			mkl_cbwr_set(MKL_CBWR_AVX);
			timinggemm(cblas_dgemm)
		}
		if (!strcmp(argv[1], "ClassicGemmAVX2")) {
			mkl_cbwr_set(MKL_CBWR_AVX2);
			timinggemm(cblas_dgemm)
		}

		if (!strcmp(argv[1], "ClassicGemv")) {
			timinggemv(cblas_dgemv)
		}
		if (!strcmp(argv[1], "OnlineExactGemv")) {
			timinggemv(rtnblas_ol_dgemv)
		}
		if (!strcmp(argv[1], "HybridSumGemv")) {
			timinggemv(rtnblas_hs_dgemv)
		}
		if (!strcmp(argv[1], "iFastSumGemv")) {
			timinggemv(rtnblas_ifs_dgemv)
		}
		if (!strcmp(argv[1], "Rgemv")) {
			timinggemv(rtnblas_dgemv)
		}
		if (!strcmp(argv[1], "OneReductionGemv")) {
			timinggemv(reprodblas_or_dgemv)
		}

		if (!strcmp(argv[1], "ClassicTrsv")) {
			timingtrsv(cblas_dtrsv)
		}
		if (!strcmp(argv[1], "ClassicTrsvCompatible")) {
			mkl_cbwr_set(MKL_CBWR_COMPATIBLE);
			timingtrsv(cblas_dtrsv)
		}
		if (!strcmp(argv[1], "ClassicTrsvSSE2")) {
			mkl_cbwr_set(MKL_CBWR_SSE2);
			timingtrsv(cblas_dtrsv)
		}
		if (!strcmp(argv[1], "ClassicTrsvSSE3")) {
			mkl_cbwr_set(MKL_CBWR_SSE3);
			timingtrsv(cblas_dtrsv)
		}
		if (!strcmp(argv[1], "ClassicTrsvSSSE3")) {
			mkl_cbwr_set(MKL_CBWR_SSSE3);
			timingtrsv(cblas_dtrsv)
		}
		if (!strcmp(argv[1], "ClassicTrsvSSE4_1")) {
			mkl_cbwr_set(MKL_CBWR_SSE4_1);
			timingtrsv(cblas_dtrsv)
		}
		if (!strcmp(argv[1], "ClassicTrsvSSE4_2")) {
			mkl_cbwr_set(MKL_CBWR_SSE4_2);
			timingtrsv(cblas_dtrsv)
		}
		if (!strcmp(argv[1], "ClassicTrsvAVX")) {
			mkl_cbwr_set(MKL_CBWR_AVX);
			timingtrsv(cblas_dtrsv)
		}
		if (!strcmp(argv[1], "ClassicTrsvAVX2")) {
			mkl_cbwr_set(MKL_CBWR_AVX2);
			timingtrsv(cblas_dtrsv)
		}
		if (!strcmp(argv[1], "OnlineExactTrsv")) {
			timingtrsv(rtnblas_ol_dtrsv)
		}
		if (!strcmp(argv[1], "HybridSumTrsv")) {
			timingtrsv(rtnblas_hs_dtrsv)
		}
		if (!strcmp(argv[1], "iFastSumTrsv")) {
			timingtrsv(rtnblas_ifs_dtrsv)
		}
		if (!strcmp(argv[1], "FastAccSumTrsv")) {
			timingtrsv(rtnblas_facs_dtrsv)
		}
		if (!strcmp(argv[1], "AccSumTrsv")) {
			timingtrsv(rtnblas_acs_dtrsv)
		}
		if (!strcmp(argv[1], "Rtrsv")) {
			timingtrsv(rtnblas_dtrsv)
		}
		if (!strcmp(argv[1], "OneReductionTrsv")) {
			timingtrsv(reprodblas_or_dtrsv)
		}
		if (!strcmp(argv[1], "OneReductionIRTrsv")) {
			timingtrsv(reprodblas_orir_dtrsv)
		}
		if (!strcmp(argv[1], "XBLASTrsv")) {
			timingxblastrsv(BLAS_dtrsv_x(
				blas_rowmajor, blas_lower, blas_no_trans, blas_non_unit_diag, size, 1, X, size, Y, 1, blas_prec_extra
			))
		}
#endif

		if (!strcmp(argv[1], "ClassicDot")) {
			mkl_cbwr_set(MKL_CBWR_OFF);
			timingdot(classic_ddot)
		}
		if (!strcmp(argv[1], "ClassicDotCompatible")) {
			mkl_cbwr_set(MKL_CBWR_COMPATIBLE);
			timingdot(classic_ddot)
		}
		if (!strcmp(argv[1], "ClassicDotSSE2")) {
			mkl_cbwr_set(MKL_CBWR_SSE2);
			timingdot(classic_ddot)
		}
		if (!strcmp(argv[1], "ClassicDotSSE3")) {
			mkl_cbwr_set(MKL_CBWR_SSE3);
			timingdot(classic_ddot)
		}
		if (!strcmp(argv[1], "ClassicDotSSSE3")) {
			mkl_cbwr_set(MKL_CBWR_SSSE3);
			timingdot(classic_ddot)
		}
		if (!strcmp(argv[1], "ClassicDotSSE4_1")) {
			mkl_cbwr_set(MKL_CBWR_SSE4_1);
			timingdot(classic_ddot)
		}
		if (!strcmp(argv[1], "ClassicDotSSE4_2")) {
			mkl_cbwr_set(MKL_CBWR_SSE4_2);
			timingdot(classic_ddot)
		}
		if (!strcmp(argv[1], "ClassicDotAVX")) {
			mkl_cbwr_set(MKL_CBWR_AVX);
			timingdot(classic_ddot)
		}
		if (!strcmp(argv[1], "ClassicDotAVX2")) {
			mkl_cbwr_set(MKL_CBWR_AVX2);
			timingdot(classic_ddot)
		}
		if (!strcmp(argv[1], "OnlineExactDot")) {
			timingdot(rtnblas_ol_ddot)
		}
		if (!strcmp(argv[1], "HybridSumDot")) {
			timingdot(rtnblas_hs_ddot)
		}
		if (!strcmp(argv[1], "AccSumDot")) {
			timingdot(rtnblas_acs_ddot)
		}
		if (!strcmp(argv[1], "FastAccSumDot")) {
			timingdot(rtnblas_facs_ddot)
		}
		if (!strcmp(argv[1], "iFastSumDot")) {
			timingdot(rtnblas_ifs_ddot)
		}
		if (!strcmp(argv[1], "Rdot")) {
			timingdot(rtnblas_ddot)
		}
		if (!strcmp(argv[1], "ReprodDot")) {
			timingdot(reprodblas_rs_ddot)
		}
		if (!strcmp(argv[1], "FastReprodDot")) {
			timingdot(reprodblas_frs_ddot)
		}
		if (!strcmp(argv[1], "OneReductionDot")) {
			timingdot(reprodblas_or_ddot)
		}
		if (!strcmp(argv[1], "OneReductionTwoProdDot")) {
			timingdot(reprodblas_ortp_ddot)
		}

		if (!strcmp(argv[1], "ClassicSum")) {
			timingsum(optimizedSum(size, X))
		}
		if (!strcmp(argv[1], "HybridSum")) {
			timingsum(hybridSum(size, X))
		}
		if (!strcmp(argv[1], "OnlineExact")) {
			timingsum(onlineExact(size, X))
		}
		if (!strcmp(argv[1], "AccSum")) {
			timingsum(accSum(size, X))
		}
		if (!strcmp(argv[1], "FastAccSum")) {
			timingsum(fastAccSum(size, X))
		}
		if (!strcmp(argv[1], "iFastSum")) {
			timingsum(iFastSum(3, X))
		}
		if (!strcmp(argv[1], "SumK2")) {
			timingsum(sumK2(size, X))
		}
		if (!strcmp(argv[1], "SumK3")) {
			timingsum(sumK3(size, X))
		}
		if (!strcmp(argv[1], "Rsum")) {
			timingsum(dynamicSum(size, X))
		}
		if (!strcmp(argv[1], "ReprodSum")) {
			timingsum(reprodSum(size, X))
		}
		if (!strcmp(argv[1], "FastReprodSum")) {
			timingsum(fastReprodSum(size, X))
		}
		if (!strcmp(argv[1], "OneReduction")) {
			timingsum(oneReduction(size, X))
		}
		if (!strcmp(argv[1], "Classicasum")) {
			timingsum(absSum(size, X))
		}
		if (!strcmp(argv[1], "Rasum")) {
			timingsum(rtnblas_dasum(size, X, 1))
		}
		if (!strcmp(argv[1], "exactdasum")) {
			timingsum(verifiedexactdasum(size, X))
		}
		if (!strcmp(argv[1], "Reprodasum")) {
			timingsum(reprodblas_rs_dasum(size, X, 1))
		}
		if (!strcmp(argv[1], "FastReprodasum")) {
			timingsum(reprodblas_frs_dasum(size, X, 1))
		}
		if (!strcmp(argv[1], "OneReductionasum")) {
			timingsum(reprodblas_or_dasum(size, X, 1))
		}

/*
#ifdef OMP_PARALLEL
		if (!strcmp(argv[1], "exsum2")) {
			timingsum(exsum(size, X, 1, 2))
		}
		if (!strcmp(argv[1], "exsum3")) {
			timingsum(exsum(size, X, 1, 3))
		}
		if (!strcmp(argv[1], "exsum4")) {
			timingsum(exsum(size, X, 1, 4))
		}
		if (!strcmp(argv[1], "exsum8")) {
			timingsum(exsum(size, X, 1, 8))
		}
		if (!strcmp(argv[1], "exsum2EE")) {
			timingsum(exsum(size, X, 1, 2, true))
		}
		if (!strcmp(argv[1], "exsum3EE")) {
			timingsum(exsum(size, X, 1, 3, true))
		}
		if (!strcmp(argv[1], "exsum4EE")) {
			timingsum(exsum(size, X, 1, 4, true))
		}
		if (!strcmp(argv[1], "exsum8EE")) {
			timingsum(exsum(size, X, 1, 8, true))
		}
#endif
*/

		if (!strcmp(argv[1], "cblasnrm2")) {
			timingsum(cblas_dnrm2(size, X, 1))
		}
		if (!strcmp(argv[1], "Classicnrm2")) {
			timingsum(classic_dnrm2(size, X, 1))
		}
		if (!strcmp(argv[1], "OnlineExactnrm2")) {
			timingsum(rtnblas_ol_dnrm2(size, X, 1))
		}
		if (!strcmp(argv[1], "HybridSumnrm2")) {
			timingsum(rtnblas_hs_dnrm2(size, X, 1))
		}
		if (!strcmp(argv[1], "AccSumnrm2")) {
			timingsum(rtnblas_acs_dnrm2(size, X, 1))
		}
		if (!strcmp(argv[1], "FastAccSumnrm2")) {
			timingsum(rtnblas_facs_dnrm2(size, X, 1))
		}
		if (!strcmp(argv[1], "iFastSumnrm2")) {
			timingsum(rtnblas_ifs_dnrm2(size, X, 1))
		}
		if (!strcmp(argv[1], "Rnrm2")) {
			timingsum(rtnblas_dnrm2(size, X, 1))
		}
		if (!strcmp(argv[1], "Reprodnrm2")) {
			timingsum(reprodblas_rs_dnrm2(size, X, 1))
		}
		if (!strcmp(argv[1], "FastReprodnrm2")) {
			timingsum(reprodblas_frs_dnrm2(size, X, 1))
		}
		if (!strcmp(argv[1], "OneReductionnrm2")) {
			timingsum(reprodblas_or_dnrm2(size, X, 1))
		}

#ifdef MPI_PARALLEL
		if (!myRank) {
#endif

			if (!strcmp(argv[4], "gemv")) {
				s = 0;
				result = 0;
				double diff = 0;
				for (unsigned int i = 0; i < size; i++) {
					diff += fabs(results[i] - sresults[i]);
					result += fabs(results[i]);
				}
				result = diff / result;
			}
			if (!strcmp(argv[4], "trsv")) {
				s = 0;
				result = 0;
				double diff = 0;
				rtnblas_dgemv(CblasRowMajor, CblasNoTrans, size, size, 1, X, size, Y, 1, -1, tempY, 1);
				for (unsigned int i = 0; i < size; i++) {
					diff += fabs(tempY[i]);
					result += fabs(Y[i]);
				}
				s = diff / result;
				result = 0;
				diff = 0;
				for (unsigned int i = 0; i < size; i++) {
					diff += fabs(results[i] - Y[i]);
					result += fabs(results[i]);
				}
				result = diff / result;
			}
			if (!strcmp(argv[4], "gemm")) {
				s = 0;
				result = 0;
				double diff = 0;
				for (unsigned int i = 0; i < size * size; i++) {
					diff += fabs(results[i] - sresults[i]);
					result += fabs(results[i]);
				}
				result = diff / result;
			}
			printf(
				"%s, %s, %.16g, %.16g, %lld, %lld, %lld, %lld, %s",
				argv[6], argv[2], s, result, mincachecycles, meancachecycles, minmemorycycles, meanmemorycycles, argv[7]
			);
#ifndef MIC
			printf("\n");
#endif

#ifdef MPI_PARALLEL
		}
#endif

#endif

#ifdef MPI_PARALLEL
		MPI_Finalize();
#endif

	}
	else {
		fprintf(stderr, "Usage error : %s\nRun %s algo data\nalgo:the algorithme to use\ndata:the file of data", argv[0], argv[0]);
	}
	return 0;
}

