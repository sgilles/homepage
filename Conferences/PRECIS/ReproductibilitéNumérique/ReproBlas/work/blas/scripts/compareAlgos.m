%{
# *************************************************************************************
# Copyright (c) 2015, University of Perpignan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.
# *************************************************************************************
#
%}
function ok = compareAlgos(compiler, compilers, link, links, algos, codes, condition, process, threads, instructionSet, instructionSetCode, results, colors, top, col)
algoresults = results(results(:,1) == compiler & results(:,2) == link & results(:,4) == condition & results(:,5) == process & results(:,6) == threads & results(:,14) == instructionSetCode, [3, 7, col]);
sizes = algoresults(algoresults(:,1) == codes(1), 2);
refruntime = algoresults(algoresults(:,1) == codes(1), 3);
log10sizes = log10(sizes);
plot(sizes, refruntime ./ refruntime, 'LineWidth', 1, strcat(colors(1,:), ";", algos(1, :), ";"));
figtitle = strcat("compare algos for:", compilers(compiler + 1, :), ",\ ", links(link + 1, :), ",\ cond=", int2str(condition), ",\ process=", int2str(process), ",\ threads=", int2str(threads), ",\ ", instructionSet);
title(figtitle);
hold on;
xlabel('Size of the Input Vectors','FontSize',14);
ylabel('Runtime in Cycles / Size','FontSize',14);
set(gca,'FontSize',14);
axis([0 16000]);
for algo = 2:size(algos, 1)
	sizes = algoresults(algoresults(:,1) == codes(algo), 2);
	runtime = algoresults(algoresults(:,1) == codes(algo), 3)
	log10sizes = log10(sizes);
	plot(sizes, runtime ./ refruntime, 'LineWidth', 1, strcat(colors(algo,:), ";", algos(algo, :), ";"));
end
print(strcat("figures/withtitle/alogoscompare-", compilers(compiler + 1,:), "-", links(link + 1,:), "-", int2str(condition), "-", int2str(process), "-", int2str(threads), "-", instructionSet, ".eps"), '-depsc2');
title("");
print(strcat("figures/notitle/alogoscompare-", compilers(compiler + 1,:), "-", links(link + 1,:), "-", int2str(condition), "-", int2str(process), "-", int2str(threads), "-", instructionSet, ".eps"), '-depsc2');

