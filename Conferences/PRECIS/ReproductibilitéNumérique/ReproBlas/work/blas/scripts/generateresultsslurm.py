#
# *************************************************************************************
# Copyright (c) 2015, University of Perpignan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.
# *************************************************************************************
#


import os
import shlex
import re
import subprocess

from optparse import OptionParser

compiler = "icc"
instructionset = "AVX2"
link = "static"
linkoctave = "staticlink"

def generateSlurm(cond, size, algo, thomp, thmpi):
	if (algo == "iFastSum" or algo == "AccSum" or algo == "FastAccSum" or algo == "HybridSum" or algo == "OnlineExact" or algo == "ReprodSum" or algo == "FastReprodSum" or algo == "ClassicSum" or algo == "DynamicSum"):
		filedirectory = "sum"
	else:
		filedirectory = "dot"
	times = " 32 "
	nodes = 0
	tsknode = 0
	if thmpi == 1:
		nodes = 1
		tsknode = 1
	else:
		nodes = thmpi / 2
		tsknode = 2
	allthreads = thomp * thmpi
	if thmpi <= 128:
		if int(size) >= (thmpi * thomp * 50):
			if thmpi == 1 or thomp == 8 or thomp == 12:
				slurmfiletarget = "./resultsslurm/" + algo + "-" + str(cond) + "-" + str(thmpi) + "-" + str(thomp) + "-" + str(size) + ".slurm"
				if not os.path.exists(os.path.dirname(slurmfiletarget)):
					os.makedirs(os.path.dirname(slurmfiletarget))
				slurmfile = open(slurmfiletarget, 'w')
				slurmfile.write("#!/bin/bash \n")
				slurmfile.write("#SBATCH --nodes=" + str(nodes) + " \n")
				slurmfile.write("#SBATCH --ntasks=" + str(thmpi) + " \n")
				slurmfile.write("#SBATCH --ntasks-per-node=" + str(tsknode) + " \n")
				slurmfile.write("#SBATCH --threads-per-core=1 \n")
				slurmfile.write("#SBATCH --cpus-per-task=" + str(thomp) + " \n")
				slurmfile.write("#SBATCH -J rtnblas \n")
				slurmfile.write("#SBATCH --time=01:00:00 \n")
				slurmfile.write("module purge \n")
				slurmfile.write("module load intel/15.0.0.090 bullxmpi/1.2.8.3 \n")
				slurmfile.write("export OMP_NUM_THREADS=" + str(thomp) + " \n")
				slurmfile.write("export KMP_AFFINITY=granularity=fine,compact,1,0 \n")

				if thmpi == 1:
					if thomp == 1:
						cmdline = "../run/bin/" + compiler + "/" + link + "/sequential/blas.out " + algo + " " + str(size) + " " + str(cond) + " " + filedirectory + times
					else:
						cmdline = "../run/bin/" + compiler + "/" + link + "/omp/blas.out " + algo + " " + str(size) + " " + str(cond) + " " + filedirectory + times
				else:
					if thomp == 1:
						cmdline = "../run/bin/" + compiler + "/" + link + "/mpi/blas.out " + algo + " " + str(size) + " " + str(cond) + " " + filedirectory + times
					else:
						cmdline = "../run/bin/" + compiler + "/" + link + "/hybrid/blas.out " + algo + " " + str(size) + " " + str(cond) + " " + filedirectory + times

				printstring = "\"" + compiler + ", " + linkoctave + ", " + algo + ", " + str(cond) + ", " + str(thmpi) + ", " + str(thomp) + "\" "
				cmdlineecho = "echo srun --mpi=openmpi -K1 -m block -c " + str(thomp) + " --resv-ports -n $SLURM_NTASKS " + cmdline + printstring + instructionset + "\n"
				slurmfile.write(cmdlineecho)
				cmdline = "srun --mpi=openmpi -K1 -m block -c " + str(thomp) + " --resv-ports -n $SLURM_NTASKS " + cmdline + printstring + instructionset + " >> ../testresults.m\n"
				slurmfile.write(cmdline)

def generateSlurms(conds, sizes, algos, OMP_THREADS, MPI_THREADS):
	for cond in conds:
		for size in sizes:
			for algo in algos:
				for thomp in OMP_THREADS:
					for thmpi in MPI_THREADS:
						generateSlurm(cond, size, algo, thomp, thmpi)

if __name__ == '__main__':
	sizes_int = map(lambda x: 10**x, range(3, 9)) + map(lambda x: 2 * (10**x), range(3, 8)) + map(lambda x: 5 * (10**x), range(3, 8))
	sizes_int.sort()
	sizes = list(map(str,sizes_int))
	conds = list(range(8, 33, 8))
	OMP_THREADS = [1, 2, 4, 8, 12]
	MPI_THREADS = map(lambda x: 2**x, range(0, 11))
	algos = ["ClassicDot", "OneReductionDot", "FastReprodDot", "ReprodDot", "OnlineExactDot", "HybridSumDot"]
	print("Begin of generation")
	generateSlurms(conds, sizes, algos, OMP_THREADS, MPI_THREADS)
	print("End of generation")

