%{
# *************************************************************************************
# Copyright (c) 2015, University of Perpignan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.
# *************************************************************************************
#
%}

clear all;

addpath('scripts');

vars;
scantestresults;
%teststats;

[values, indices] = sort(results(:, 5) .* results(:, 6));
results = results(indices, :);

[values, indices] = sort(results(:, 7));
results = results(indices, :);

close all;

algos = ["SumK2"; "SumK3"; "OnlineExact"; "HybridSum"; "FastAccSum"; "AccSum"; "iFastSum"]
codes = [SumK2; SumK3; OnlineExact; HybridSum; FastAccSum; AccSum; iFastSum]
colors = ["-k"; "-k"; "-r"; "-g"; "-b"; "-c"; "-m"];
compareAlgos2(0, compilers, 0, links, algos, codes, 8, 1, 1, "AVX", AVX, results, colors, 60, 12, "Fig21a")

close all;

algos = ["OnlineExact"; "HybridSum"; "FastAccSum"; "AccSum"; "iFastSum"]
codes = [OnlineExact; HybridSum; FastAccSum; AccSum; iFastSum]
colors = ["-r"; "-g"; "-b"; "-c"; "-m"];
compareAlgos2(0, compilers, 0, links, algos, codes, 32, 1, 1, "AVX", AVX, results, colors, 60, 12, "Fig21b")

close all;

algos = ["Classicnrm2"; "Rnrm2"]
codes = [Classicnrm2; Rnrm2]
colors = ["-g"; "-r"];
compareAlgos2(0, compilers, 0, links, algos, codes, 32, 1, 1, "AVX", AVX, results, colors, 30, 12, "Fig31e")

close all;

algos = ["ClassicDot"; "Rdot"]
codes = [ClassicDot; Rdot]
colors = ["-g"; "-r"];
compareAlgos2(0, compilers, 0, links, algos, codes, 32, 1, 1, "AVX", AVX, results, colors, 30, 12, "Fig31c")

close all;

algos = ["Classicasum"; "Rasum"]
codes = [Classicasum; Rasum]
colors = ["-g"; "-r"];
compareAlgos2(0, compilers, 0, links, algos, codes, 32, 1, 1, "AVX", AVX, results, colors, 4, 12, "Fig31a")

close all;

algos = ["Classicnrm2"; "FastReprodnrm2"; "Rnrm2"]
codes = [Classicnrm2; FastReprodnrm2; Rnrm2]
colors = ["-g"; "-b"; "-r"];
compareAlgos2(0, compilers, 0, links, algos, codes, 32, 1, 16, "AVX", AVX, results, colors, 4, 12, "Fig31f")

close all;

algos = ["ClassicDot"; "FastReprodDot"; "Rdot"]
codes = [ClassicDot; FastReprodDot; Rdot]
colors = ["-g"; "-b"; "-r"];
compareAlgos2(0, compilers, 0, links, algos, codes, 32, 1, 16, "AVX", AVX, results, colors, 4, 12, "Fig31d")

close all;

algos = ["Classicasum"; "FastReprodasum"; "Rasum"]
codes = [Classicasum; FastReprodasum; Rasum]
colors = ["-g"; "-b"; "-r"];
compareAlgos2(0, compilers, 0, links, algos, codes, 32, 1, 16, "AVX", AVX, results, colors, 4, 12, "Fig31b")

close all;

