%{
# *************************************************************************************
# Copyright (c) 2015, University of Perpignan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.
# *************************************************************************************
#
%}
function ok = ompscalability(compiler, compilers, link, links, name, code, condition, ompthreads, instructionSet, instructionSetCode, results, colors, top, col)
algoresults = results(results(:,1) == compiler & results(:,2) == link & results(:,3) == code & results(:,4) == condition & results(:,5) == 1 & results(:,14) == instructionSetCode & results(:,7) >= 100000, [6, 7, col]);
sizes = algoresults(algoresults(:,1) == ompthreads(1), 2);
runtime = algoresults(algoresults(:,1) == ompthreads(1), 3);
log10sizes = log10(sizes);
plot(log10sizes, runtime ./ (sizes), 'LineWidth', 1, strcat(colors(1,:), ";threads=60;"));
hold on;
figtitle = strcat("omp scalability for:", compilers(compiler + 1, :), ",\ ", links(link + 1, :), ",\ ", name, ",\ cond=", int2str(condition), ",\ ", instructionSet);
title(figtitle);
xlabel('size');
ylabel("runtime(cycles) / size");
axis([4 9 0 5]);
for threads = 2:size(ompthreads, 1)
	sizes = algoresults(algoresults(:,1) == ompthreads(threads), 2);
	runtime = algoresults(algoresults(:,1) == ompthreads(threads), 3);
	log10sizes = log10(sizes);
	plot(log10sizes, runtime ./ (sizes), 'LineWidth', 1, strcat(colors(threads,:), ";threads=", int2str(ompthreads(threads)), ";"));
end
print(strcat("figures/withtitle/ompscal-", compilers(compiler + 1,:), "-", links(link + 1,:), "-", name, "-", int2str(condition), "-", instructionSet, ".eps"), '-depsc2');
title('');
print(strcat("figures/notitle/ompscal-", compilers(compiler + 1,:), "-", links(link + 1,:), "-", name, "-", int2str(condition), "-", instructionSet, ".eps"), '-depsc2');

