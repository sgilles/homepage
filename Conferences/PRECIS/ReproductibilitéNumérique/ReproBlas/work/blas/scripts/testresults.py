#
# *************************************************************************************
# Copyright (c) 2015, University of Perpignan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.
# *************************************************************************************
#


import os
import shlex
import re
import subprocess

from optparse import OptionParser

compiler = "icc"
link = "static"

def testResult(cond, size, algo, thomp, thmpi):
	cmdline = ""
	times = " 32 "
	if (algo == "iFastSum" or algo == "AccSum" or algo == "FastAccSum" or algo == "HybridSum" or algo == "OnlineExact" or algo == "ReprodSum" or algo == "FastReprodSum" or algo == "ClassicSum" or algo == "DynamicSum" or algo == "OneReduction"):
		filedirectory = "sum"
	else:
		filedirectory = "dot"

	if thmpi == 1:
		if thomp == 1:
			cmdline = "./run/test/" + compiler + "/" + link + "/sequential/blas.out " + algo + " " + str(size) + " " + str(cond) + " " + filedirectory + times
		else:
			os.environ['OMP_NUM_THREADS'] = str(thomp)
			cmdline = "./run/test/" + compiler + "/" + link + "/omp/blas.out " + algo + " " + str(size) + " " + str(cond) + " " + filedirectory + times
	else:
		if thomp == 1:
			cmdline = "mpirun -np " + str(thmpi) + " ./run/test/" + compiler + "/" + link + "/mpi/blas.out " + algo + " " + str(size) + " " + str(cond) + " " + filedirectory + times
		else:
			os.environ['OMP_NUM_THREADS'] = str(thomp)
			cmdline = "mpirun -np " + str(thmpi) + " ./run/test/" + compiler + "/" + link + "/hybrid/blas.out " + algo + " " + str(size) + " " + str(cond) + " " + filedirectory + times
	print(cmdline)
	subprocess.call(cmdline + str(thmpi) + str(thomp), shell=True)

def testResults(conds, sizes, algos, OMP_THREADS, MPI_THREADS):
	for cond in conds:
		for size in sizes:
			for algo in algos:
				for thomp in OMP_THREADS:
					for thmpi in MPI_THREADS:
						testResult(cond, size, algo, thomp, thmpi)

if __name__ == '__main__':
	sizes_int = map(lambda x: 10**x, range(3, 5)) + map(lambda x: 2 * (10**x), range(3, 4)) + map(lambda x: 5 * (10**x), range(3, 4))
	sizes_int.sort()
	sizes = list(map(str,sizes_int))
	conds = list(range(16, 17, 8))
	OMP_THREADS = [1]
	MPI_THREADS = [1]
	algos = ["StaticScheduleDot", "DynamicScheduleDot", "OnlineExactDot"]
	testResults(conds, sizes, algos, OMP_THREADS, MPI_THREADS)

