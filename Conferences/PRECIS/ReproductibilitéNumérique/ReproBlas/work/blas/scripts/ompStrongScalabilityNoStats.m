%{
# *************************************************************************************
# Copyright (c) 2015, University of Perpignan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.
# *************************************************************************************
#
%}
function ok = ompStrongScalabilityNoStats(compiler, compilers, link, links, name, code, condition, sizeScalability, ompthreads, instructionSet, instructionSetCode, results, col)
algoresults = results(results(:,1) == compiler & results(:,2) == link & results(:,3) == code & results(:,4) == condition & results(:,5) == 1 & results(:,7) == sizeScalability & results(:,14) == instructionSetCode, [6, col]);
runtime = algoresults(:,2);
bar(runtime);
set(gca, 'XTickLabel', ompthreads);
xlabel('threads');
ylabel("runtime(cycles)");
figtitle = strcat("omp strong scalability no stats for:", compilers(compiler + 1, :), ",\ ", links(link + 1, :), ",\ ", name, ",\ cond=", int2str(condition), ",\ size=", int2str(sizeScalability), ",\ ", instructionSet);
title(figtitle);
print(strcat("figures/withtitle/ompstrongscal-", compilers(compiler + 1,:), "-", links(link + 1,:), "-", name, "-", int2str(condition), "-", int2str(sizeScalability), "-", instructionSet, ".eps"), '-depsc2');
title('');
print(strcat("figures/notitle/ompstrongscal-", compilers(compiler + 1,:), "-", links(link + 1,:), "-", name, "-", int2str(condition), "-", int2str(sizeScalability), "-", instructionSet, ".eps"), '-depsc2');

